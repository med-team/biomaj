#!/usr/bin/perl

# History:
# Fix ticket 2013082410000041: manage db host

use Getopt::Long;
use FindBin qw($Bin);

my $BIOMAJ_ROOT="/usr/share/biomaj";

my $MIGRATIONDIR=$BIOMAJ_ROOT."/sql/migration";

# get config path
my $globalproperties;
open my $generalconffile, '<', $BIOMAJ_ROOT."/general.conf" or die $!;
while(<$generalconffile>) {
   chomp;
   if($_ =~m/workflows\.dir/) {
   (my $key, my $value) = split /=/, $_;
   $globalproperties = $value;
   last;
   }
}
close($generalconffile);

my $GLOBAL_PROPERTIES = $globalproperties."/global.properties";

sub printWarning {
        warn "You need to configure the database access before executing the upgrade\n";
        warn "Execute the script manually once configuration is done\n";
        printUsage();
        die;
}


sub printUsage {
	print " ***************************************************************************************\n";
	print "Before running the migration, please check that database configuration is correctly set \n";
	print "and that the database schema and user are correctly created.\n";
	print "To upgrade the database, the script must be run with the following arguments:\n";
	print "  For a first install, all arguments are mandatory, for an upgrade, only some parameters are required.\n";
	print "  - First install: [ -dbuser DBUSER -dbpwd DBPASSWORD -dbhost DBHOST -db (mysql|hsql)]\n";
	print "  - From v1.1 : 	  [ -admin LOGIN -adminpwd PASSWORD -adminemail  ADMINEMAIL]\n";
	print "\n";
	print "To get the current version, execute: biomaj --version\n";
}

my $admin = "admin";
my $adminpwd = "admin";
my $adminemail = 'no-reply@none';
my $help=0;

my $db = undef;
my $dbuser = undef;
my $dbpwd = undef;
my $dbhost = undef;

my $USAGE = "/usr/share/biomaj/migration/update.sh -admin USER -adminpwd USERPASSWORD";

# Get those via db_get
$result = GetOptions (	"h" => \$help,
						# From v1.1 to v1.2
						"admin=s" => \$admin,    # string
                      	"adminemail=s" => \$adminemail,    # string
                      	"adminpwd=s"   => \$adminpwd,
                      	"dbuser=s"   => \$dbuser,
                      	"dbpwd=s"   => \$dbpwd,
			"dbhost=s"   => \$dbhost,
                      	"db=s"   => \$db
                      	);      # string);  

if($help==1) {
	printUsage();
	exit;
}



# read configuration
my %config;
open my $configfile, '<', $GLOBAL_PROPERTIES or die $!;
while(<$configfile>) {
   chomp; 
   if($_ !~m/#/ && $_ !~/^$/) {
   (my $key, my $value) = split /=/, $_;
   $config{$key} = join '=', $value;
   }

}
close($configfile);


# Read config file to get db data
$db = $config{'database.type'} unless defined($db);
$dbuser = $config{'database.login'} unless defined($dbuser);
$dbpwd = $config{'database.password'} unless defined($dbpwd);
if(! defined($dbhost)) {
    if ($config{'database.url'} =~ m/mysql\\:\/\/(.*)\// ) {
        $dbhost = $1;
    }
}

if( ! -e $MIGRATIONDIR."/biomaj_11.done") {

printWarning  unless defined($db)  && defined($dbuser)  && defined($dbpwd) && defined($dbhost);

print "Install database\n";

if($db=~/mysql/) {
system("mysql -f --user=".$dbuser." --password=".$dbpwd." --host=".$dbhost." < ".$BIOMAJ_ROOT."/sql/mysql.sql");
	if ($? != 0) {
        warn "Failed to run the SQL script on database\n";
        warn "Check your configuration and reexecute the script manually\n";
        printUsage();
        exit;
    }
    
}
else {
# HSQL Not supported
}
open BIOMAJ11, ">".$MIGRATIONDIR."/biomaj_11.done" or die ;
print BIOMAJ11 "done";
close(BIOMAJ11);
}

if( ! -e $MIGRATIONDIR."/biomaj_11-12.done") {

print "Migrate from 1.1 to 1.2\n";


system("export BIOMAJ_ROOT=".$BIOMAJ_ROOT.";java -cp ".$BIOMAJ_ROOT."/lib/biomaj.jar org.inria.biomaj.exe.migration.CoreMigrationFrom1_1To1_2 ".$admin." ".$adminpwd." ".$adminemail);
open BIOMAJ11, ">".$MIGRATIONDIR."/biomaj_11-12.done" or die ;
print BIOMAJ11 "done";
close(BIOMAJ11);
warn "Do not forget to change default admin password\n";
}
