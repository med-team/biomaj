biomaj (1.2.3-12) unstable; urgency=medium

  * Team upload
  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.0 (no changes needed)

 -- Andreas Tille <tille@debian.org>  Wed, 27 Sep 2017 14:48:07 +0200

biomaj (1.2.3-11) unstable; urgency=medium

  * Team upload.
  * d/watch: version=4
  * cme fix dpkg-control
  * debhelper 10
  * s/mysql-server/default-mysql-server | virtual-mysql-server/
    Closes: #848467
  * s/mysql-client/default-mysql-client | virtual-mysql-client/

 -- Andreas Tille <tille@debian.org>  Sat, 17 Dec 2016 17:50:25 +0100

biomaj (1.2.3-10) unstable; urgency=medium

  * Patch biomajlogger to match libjgoodies-forms-java >= 1.6 

 -- Olivier Sallou <osallou@debian.org>  Mon, 21 Nov 2016 13:52:12 +0100

biomaj (1.2.3-9) unstable; urgency=medium

  * Add Brazilian Portuguese translation (Closes: #825046).
    Thanks to Leonardo Santiago Sidon da Rocha <leonardossr@gmail.com>. 

 -- Olivier Sallou <osallou@debian.org>  Fri, 17 Jun 2016 13:18:39 +0200

biomaj (1.2.3-8) unstable; urgency=medium

  * Allow mysql skip at install (Closes: #826903). 

 -- Olivier Sallou <osallou@debian.org>  Fri, 17 Jun 2016 13:10:13 +0200

biomaj (1.2.3-7) unstable; urgency=medium

  [ Emmanuel Bourg ]
  * Team upload.
  * Replaced the dependency on libcommons-net2-java with libcommons-net-java
    on the binary package

  [ Eugene Zhukov ]
  * d/control: replace dependency libsaxonb-java with libsaxonhe-java

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 06 Nov 2015 09:06:55 +0100

biomaj (1.2.3-6) unstable; urgency=medium

  * Replace libcommons-httpclient-java by libhttpclient-java
    (Closes: #800985). 

 -- Olivier Sallou <osallou@debian.org>  Tue, 06 Oct 2015 09:16:30 +0200

biomaj (1.2.3-5) unstable; urgency=medium

  * Replace libcommons-net2-java by libcommons-net-java (Closes: #800767). 

 -- Olivier Sallou <osallou@debian.org>  Mon, 05 Oct 2015 10:02:11 +0200

biomaj (1.2.3-4) unstable; urgency=medium

  * Team upload.
  * Replaced the dependency on libgnumail-java with libmail-java
  * Standards-Version updated to 3.9.6 (no changes)
  * Switch to debhelper level 9
  * debian/copyright:
    - Fixed the short name for the BSD license
    - Reordered the paragraphs to avoid a Lintian warning about unsued files

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 17 Oct 2014 11:52:38 +0200

biomaj (1.2.3-3) unstable; urgency=low

  * d/control: remove dependency on mysql-server but rather set it
    to mysql-client as server can be remote.

 -- Olivier Sallou <osallou@debian.org>  Sun, 10 Nov 2013 09:00:17 +0100

biomaj (1.2.3-2) unstable; urgency=low

  * d/control: Fix browse url for svn 
    d/config: Fix test for non-interactive mode

 -- Olivier Sallou <osallou@debian.org>  Wed, 06 Nov 2013 09:51:55 +0100

biomaj (1.2.3-1) unstable; urgency=low

  * New upstream release (fixes on local storage and stage option).

 -- Olivier Sallou <osallou@debian.org>  Wed, 30 Oct 2013 15:34:34 +0100

biomaj (1.2.2-2) unstable; urgency=low

  * d/postinst: Fix install in noninteractive mode (Closes: #721773).

 -- Olivier Sallou <osallou@debian.org>  Fri, 30 Aug 2013 12:11:50 +0200

biomaj (1.2.2-1) unstable; urgency=low

  * New upstream release with minor fixes
      d/patches/series: upstream bug 15914 fixed.

 -- Olivier Sallou <osallou@debian.org>  Tue, 18 Jun 2013 14:30:28 +0200

biomaj (1.2.1-3) unstable; urgency=low

  * Add patch to manage ant properties api change (Closes: #712328).

 -- Olivier Sallou <osallou@debian.org>  Tue, 18 Jun 2013 11:39:36 +0200

biomaj (1.2.1-2) unstable; urgency=low

  * Add patch to match libjgoodies-form-java  >1.3,
    FormFactory replaced by FormSpecs (Closes: #708454)
    d/control: Depends on libjgoodies-form-java > 1.3

 -- Olivier Sallou <osallou@debian.org>  Sun, 19 May 2013 13:21:55 +0200

biomaj (1.2.1-1) unstable; urgency=low

  * New usptream release
  * Add Japanase debconf translation (Closes: #690698)
    Thanks to victory <victory.deb@gmail.com>

 -- Olivier Sallou <osallou@debian.org>  Fri, 19 Oct 2012 17:04:19 +0200

biomaj (1.2.0-10) unstable; urgency=low

  * debian/rules, debian/postinst: fix permissions on
    properties file.

 -- Olivier Sallou <osallou@debian.org>  Thu, 04 Oct 2012 18:34:01 +0200

biomaj (1.2.0-9) unstable; urgency=low

  * debian/patches/set_env_file_for_debian:
    Prevent env.sh file update by program at install
    (Closes: #688433)

 -- Olivier Sallou <osallou@debian.org>  Wed, 03 Oct 2012 13:54:44 +0200

biomaj (1.2.0-8) unstable; urgency=low

  [ Olivier Sallou ]
  * debian/copyright: Fix copyright URL
  * debian/control: Switch to Standards 3.9.4
  * debian/rules, debian/postinst, debian/postrm: fix conffile update
    conflicting with Debian Policy (Closes: #688433).

 -- Olivier Sallou <osallou@debian.org>  Wed, 01 Aug 2012 15:59:55 +0200

biomaj (1.2.0-7) unstable; urgency=low

   [ Olivier Sallou ]
   Patch from James Page <james.page@ubuntu.com> (Closes: #683517)
   * Transition package to use default java implementation:
    - d/rules: Specify source/target = 1.5 to ensure bytecode is backwards
      compatible.
    - d/control: BD on default-jdk only. Switch runtime dependency to
      default-jre | java6-runtime. 

 -- Olivier Sallou <osallou@debian.org>  Wed, 01 Aug 2012 14:58:41 +0200

biomaj (1.2.0-6) unstable; urgency=low

  [ Olivier Sallou ]
  * Clean files for purge (Closes: #678490).

  [ Andreas Tille ]
  * debian/upstream: Enhance upstream information (authors format, missing
    fields)

  [Debconf translation updates]
  * French (Olivier Sallou, proofread by the French l10n team).

 -- Olivier Sallou <osallou@debian.org>  Tue, 08 May 2012 22:46:34 +0200

biomaj (1.2.0-5) unstable; urgency=low

  * Add missing file to package to get the console help (Closes: #658833)
    - biomaj.install

 -- Olivier Sallou <olivier.sallou@irisa.fr>  Wed, 01 Feb 2012 10:00:03 +0100

biomaj (1.2.0-4) unstable; urgency=low

  * [Olivier Sallou]
  * config: remove biomaj/jdk_home, not needed anymore

  * Debconf templates and debian/control reviewed by the debian-l10n-
    english team as part of the Smith review project. Closes: #653408
  * [Debconf translation updates]
  * Russian (Yuri Kozlov).  Closes: #656111
  * Italian (Francesca Ciceri).  Closes: #656254
  * French (Olivier Sallou).  Closes: #656066
  * Czech (Michal Simunek).  Closes: #656670
  * Dutch; (Jeroen Schot).  Closes: #657009
  * German (Chris Leick).  Closes: #657024
  * Danish (Joe Hansen).  Closes: #657341
  * Indonesian (Mahyuddin Susanto).  Closes: #657463
  * Spanish; (Camaleón).  Closes: #657491
  * Polish (Michał Kułach).  Closes: #657512
  * Swedish (Martin Bagge / brother).  Closes: #657548
  * Portuguese (Miguel Figueiredo).  Closes: #657664
   

 -- Olivier Sallou <olivier.sallou@irisa.fr>  Fri, 16 Dec 2011 15:53:10 +0100

biomaj (1.2.0-3) unstable; urgency=low

  * Add Portuguese translation for debconf (Closes: #651680)
  - Thanks to Pedro Ribeiro <p.m42.ribeiro@gmail.com> 

 -- Olivier Sallou <olivier.sallou@irisa.fr>  Fri, 16 Dec 2011 09:25:22 +0100

biomaj (1.2.0-2) unstable; urgency=low

  * Fix Mysql dependency name.

 -- Olivier Sallou <olivier.sallou@irisa.fr>  Fri, 11 Nov 2011 10:25:39 +0100

biomaj (1.2.0-1) unstable; urgency=low

  * New upstream release with new features and bug fixes.
  * Change in database model, migration script ran at install
  * New post processes added in default install (emboss)

 -- Olivier Sallou <olivier.sallou@irisa.fr>  Thu, 18 Aug 2011 09:55:23 +0200

biomaj (1.1.0-3) unstable; urgency=low

  * Change priority in debconf message for jdk home
  * Add spanish translation of debconf:
    Thanks to Camaleón <noelamac@gmail.com>
    Closes: #632952
  * Add sweden translation of debconf:
    Thanks to Martin Bagge <brother@bsnet.se>
    Closes: #628927
  * Add german translation of debconf:
    Thanks to Chris Leick <c.leick@vollbio.de>
    Closes: #629455
  * Change openjdk dependency to add default-jdk and sun jdk possible depends
    Closes: #633523

 -- Olivier Sallou <olivier.sallou@irisa.fr>  Thu, 11 Jul 2011 09:55:23 +0200

biomaj (1.1.0-2) unstable; urgency=low

  * Add russian translation of debconf:
    Thanks to Yuri Kozlov <yuray@komyakino.ru>
    Closes: #623807
  * debian/patches/fix623558_remove_pid:
    Remove unnecessary pid file creation
    Closes: #623558
  * debian/postrm,debian/preinst:
    Issue for smooth upgrade, require old package
    removal then new package install
    Closes: #624284 
    

 -- Olivier Sallou <olivier.sallou@irisa.fr>  Fri, 22 Apr 2011 16:27:11 +0200

biomaj (1.1.0-1) unstable; urgency=low

  * Initial release (Closes: #609038)
    First Debian package, fixed copyright file to list all licenses

 -- Olivier Sallou <olivier.sallou@irisa.fr>  Wed, 05 Jan 2011 10:39:00 +0100
