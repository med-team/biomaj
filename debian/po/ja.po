# Japanese translation.
# Copyright (C) 2012
# This file is distributed under the same license as the biomaj package.
# victory <victory.deb@gmail.com>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: biomaj\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-10-11 14:27+0000\n"
"PO-Revision-Date: 2012-10-11 23:27+0900\n"
"Last-Translator: victory <victory.deb@gmail.com>\n"
"Language-Team: Japanese <debian-japanese@lists.debian.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Configure MySQL connection now?"
msgstr "MySQL をここで設定しますか?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"Once the MySQL database for BioMAJ has been created and configured, it can "
"populate itself automatically instead of needing to be updated manually."
msgstr ""
"BioMAJ 用の MySQL データベースの作成と設定が終わると、手作業で更新させる必要は"
"なく、自動的に更新させることができます。"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"Please specify whether the database connection should be configured now."
msgstr "データベース接続をここで設定するかどうかを選択してください。"

#. Type: string
#. Description
#: ../templates:3001
msgid "MySQL server:"
msgstr "MySQL サーバ:"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Please enter the hostname or IP address of the MySQL server you want to use."
msgstr ""
"使用する MySQL サーバのホスト名か IP アドレスを入力してください。"

#. Type: string
#. Description
#: ../templates:4001
msgid "MySQL login for BioMAJ database:"
msgstr "BioMAJ データベース用の MySQL ID:"

#. Type: string
#. Description
#: ../templates:4001
msgid ""
"Please enter the login to use when connecting to the MySQL database server "
"to access the biomaj_log database."
msgstr ""
"biomaj_log データベースにアクセスする際 MySQL データベースサーバに接続するの"
"に使う ID を入力してください。"

#. Type: password
#. Description
#: ../templates:5001
msgid "MySQL password for BioMAJ database:"
msgstr "BioMAJ データベース用の MySQL パスワード:"

#. Type: password
#. Description
#: ../templates:5001
msgid ""
"Please enter the password to use when connecting to the MySQL database "
"server to access the biomaj_log database."
msgstr ""
"biomaj_log データベースにアクセスする際 MySQL データベースサーバに接続するの"
"に使うパスワードを入力してください。"

