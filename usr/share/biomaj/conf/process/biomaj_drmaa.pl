#!/usr/bin/perl

# History: 07/06/11 creation

=head1 NAME

biomaj_drmaa - DRMAA wrapper for job submission via BioMAJ

=head1 USAGE

This script should not be used independently from BioMAJ as it needs specific
environnment variables to be set by BioMAJ.

C<biomaj_drmaa exe_path exe_arg1 exe_arg2 ...>

=head1 DESCRIPTION

This script handles the submission of BioMAJ processes on a cluster via the DRMAA API.
It is called by BioMAJ with the appropriate paramaters during the post-processing phase.

It is assumed that your queuing system is correctly configured and that the possibly required environment variables
are set.

=head1 QUEUE SPECIFICATION

You can specify what queue the job should be submitted to with the variable $QUEUE: C<$QUEUE="-q queueName";>.
If none is specified, default one (if any) will be used.


=head1 AUTHOR

Romaric SABAS

=cut


use Schedule::DRMAAc qw(:all);
use Time::HiRes qw(time);
use File::Basename;


$QUEUE = "";


if (!exists($ENV{"datadir"}) || !exists($ENV{"dirversion"})) {
	print STDERR "datadir or dirversion environment variables are not set.\n";
	exit 1;
}

if ($#ARGV + 1 < 1) {
	print STDERR "Script path is needed\n";
	exit 1;
}

$scriptpath = $ARGV[0];
for ($i = 1; $i <= $#ARGV; $i++) {
	push @scriptargs, "$ARGV[$i]";
}

if (!-x $scriptpath) {
        print STDERR "File is not executable : " . $scriptpath . "\n";
        exit 1;
}

# script output/error paths
$time = time; # Get current time for unique name
$root = $ENV{"datadir"} . "/" . $ENV{"dirversion"} . "/future_release/" . basename($scriptpath);
$outpath = "$root.$time.out";
$errpath = "$root.$time.err";

# Get current environment
foreach (sort keys %ENV) {
        push @environment, "$_=$ENV{$_}";
}


# Init
($error, $diagnosis) = drmaa_init(undef);
if ($error) {
	die drmaa_strerror($error) . "\n" . $diagnosis;
}

# Create job template
($error, $job, $diagnosis) = drmaa_allocate_job_template();
if ($error) {
	die drmaa_strerror($error) . "\n" . $diagnosis;
}

# Set job output file
($error, $diagnosis) = drmaa_set_attribute($job, $DRMAA_OUTPUT_PATH, ":$outpath");
if ($error) {
	die drmaa_strerror($error) . "\n" . $diagnosis;
}
#Set job error file
($error, $diagnosis) = drmaa_set_attribute($job, $DRMAA_ERROR_PATH, ":$errpath");
if ($error) {
	die drmaa_strerror($error) . "\n" . $diagnosis;
}
# Set command to execute
($error, $diagnosis) = drmaa_set_attribute($job, $DRMAA_REMOTE_COMMAND, $scriptpath);
if ($error) {
	die drmaa_strerror($error) . "\n" . $diagnosis;
}
# Set command parameters
($error, $diagnosis) = drmaa_set_vector_attribute($job, $DRMAA_V_ARGV, [@scriptargs]);
if ($error) {
	die drmaa_strerror($error) . "\n" . $diagnosis;
}

# Set environment
($error, $diagnosis) = drmaa_set_vector_attribute($job, $DRMAA_V_ENV, [@environment]);
if ($error) {
        die drmaa_strerror($error) . "\n" . $diagnosis;
}

# QUEUE (optionnal)
($error, $diagnosis) = drmaa_set_attribute($job, $DRMAA_NATIVE_SPECIFICATION, $QUEUE);
if ($error) {
	die drmaa_strerror($error) . "\n" . $diagnosis;
}

# Start job
($error, $jobid, $diagnosis) = drmaa_run_job($job);
if ($error) {
	die drmaa_strerror($error) . "\n" . $diagnosis;
}
# Wait for job to end
($error, $job_id_out, $stat, $rusage, $diagnosis) = drmaa_wait($jobid, $DRMAA_TIMEOUT_WAIT_FOREVER);
if ($error) {
	die drmaa_strerror($error) . "\n" . $diagnosis;
}
# Get exit status
($error, $exit_status, $diagnosis) = drmaa_wexitstatus($stat);
if ($error) {
	die drmaa_strerror($error) . "\n" . $diagnosis;
}

# Delete template
($error, $diagnosis) = drmaa_delete_job_template($job);
if ($error) {
	die drmaa_strerror( $error ) . "\n" . $diagnosis;
}

# Communication with BioMAJ

# Print job stdout
open FILE, "<", $outpath or die $!;
while (<FILE>) {
	print STDOUT $_;
}
close FILE;
# Print job stderr
open FILE2, "<", $errpath or die $!;
while (<FILE2>) {
	print STDERR $_;
}
close FILE2;

# Delete tmp files
unlink $outpath;
unlink $errpath;

# Return job exit code
exit $exit_status;


