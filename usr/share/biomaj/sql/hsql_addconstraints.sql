-- Configuration
ALTER TABLE configuration ADD CONSTRAINT fk_configuration_remoteInfo1
    FOREIGN KEY (ref_idremoteInfo )
    REFERENCES remoteInfo (idremoteInfo )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;
ALTER TABLE configuration ADD CONSTRAINT fk_configuration_localInfo1
    FOREIGN KEY (ref_idlocalInfo )
    REFERENCES localInfo (idlocalInfo )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;
ALTER TABLE configuration ADD CONSTRAINT fk_configuration_bank1
    FOREIGN KEY (ref_idbank )
    REFERENCES bank (idbank )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

-- updateBank
ALTER TABLE updateBank ADD CONSTRAINT fk_updateBank_configuration1
    FOREIGN KEY (ref_idconfiguration )
    REFERENCES configuration (idconfiguration )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

-- session
ALTER TABLE session ADD CONSTRAINT fk_session_updateBank1
    FOREIGN KEY (ref_idupdateBank )
    REFERENCES updateBank (idupdateBank )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

-- metaprocess
ALTER TABLE metaprocess ADD CONSTRAINT fk_metaprocess_sessionTask1
    FOREIGN KEY (ref_idsessionTask )
    REFERENCES sessionTask (idsessionTask )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

-- process
ALTER TABLE process ADD CONSTRAINT fk_process_metaprocess1
    FOREIGN KEY (ref_idmetaprocess )
    REFERENCES metaprocess (idmetaprocess )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

-- file
ALTER TABLE file ADD CONSTRAINT fk_file_process1
    FOREIGN KEY (ref_idprocess )
    REFERENCES process (idprocess )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

-- productionDirectory
ALTER TABLE productionDirectory ADD CONSTRAINT fk_productionDirectory_bank1
    FOREIGN KEY (ref_idbank )
    REFERENCES bank (idbank )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

-- session_has_message
ALTER TABLE session_has_message ADD CONSTRAINT fk_session_has_message_session1
    FOREIGN KEY (ref_idsession )
    REFERENCES session (idsession )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;
ALTER TABLE session_has_message ADD CONSTRAINT fk_session_has_message_message1
    FOREIGN KEY (ref_idmessage )
    REFERENCES message (idmessage )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

-- sessionTask_has_message
ALTER TABLE sessionTask_has_message ADD CONSTRAINT fk_sessionTask_has_message_sessionTask1
    FOREIGN KEY (ref_idsessionTask )
    REFERENCES sessionTask (idsessionTask )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;
ALTER TABLE sessionTask_has_message ADD CONSTRAINT fk_sessionTask_has_message_message1
    FOREIGN KEY (ref_idmessage )
    REFERENCES message (idmessage )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

-- metaprocess_has_message
ALTER TABLE metaprocess_has_message ADD CONSTRAINT fk_metaprocess_has_message_metaprocess1
    FOREIGN KEY (ref_idmetaprocess )
    REFERENCES metaprocess (idmetaprocess )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;
ALTER TABLE metaprocess_has_message ADD CONSTRAINT fk_metaprocess_has_message_message1
    FOREIGN KEY (ref_idmessage )
    REFERENCES message (idmessage )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

-- process_has_message
ALTER TABLE process_has_message ADD CONSTRAINT fk_process_has_message_process1
    FOREIGN KEY (ref_idprocess )
    REFERENCES process (idprocess )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;
ALTER TABLE process_has_message ADD CONSTRAINT fk_process_has_message_message1
    FOREIGN KEY (ref_idmessage )
    REFERENCES message (idmessage )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

-- session_has_sessionTask
ALTER TABLE session_has_sessionTask ADD CONSTRAINT fk_session_has_sessionTask_session1
    FOREIGN KEY (ref_idsession )
    REFERENCES session (idsession )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;
ALTER TABLE session_has_sessionTask ADD CONSTRAINT fk_session_has_sessionTask_sessionTask1
    FOREIGN KEY (ref_idsessionTask )
    REFERENCES sessionTask (idsessionTask )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

-- sessionTask_has_file
ALTER TABLE sessionTask_has_file ADD CONSTRAINT fk_sessionTask_has_file_sessionTask1
    FOREIGN KEY (ref_idsessionTask )
    REFERENCES sessionTask (idsessionTask )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;
ALTER TABLE sessionTask_has_file ADD CONSTRAINT fk_sessionTask_has_file_file1
    FOREIGN KEY (ref_idfile )
    REFERENCES file (idfile )
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

