SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `biomaj_log` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `biomaj_log`;

-- -----------------------------------------------------
-- Table `biomaj_log`.`remoteInfo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`remoteInfo` (
  `idremoteInfo` INT NOT NULL AUTO_INCREMENT ,
  `protocol` VARCHAR(45) NULL ,
  `port` INT NULL ,
  `dbName` VARCHAR(45) NULL ,
  `dbFullname` TEXT NULL ,
  `dbType` VARCHAR(128) NULL ,
  `server` VARCHAR(256) NULL ,
  `remoteDir` VARCHAR(256) NULL ,
  PRIMARY KEY (`idremoteInfo`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`localInfo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`localInfo` (
  `idlocalInfo` INT NOT NULL AUTO_INCREMENT ,
  `offlineDirectory` VARCHAR(256) NULL ,
  `versionDirectory` VARCHAR(256) NULL ,
  `frequency` INT NULL ,
  `dolinkcopy` TINYINT(1) NULL ,
  `logfile` TINYINT(1) NULL ,
  `releaseFile` VARCHAR(512) NULL ,
  `releaseRegexp` VARCHAR(512) NULL ,
  `remoteFiles` TEXT NULL ,
  `remoteExcludedFiles` VARCHAR(512) NULL ,
  `localFiles` VARCHAR(512) NULL ,
  `nversions` INT NULL ,
  PRIMARY KEY (`idlocalInfo`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `biomaj_log`.`bank`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`bank` (
  `idbank` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  CONSTRAINT `uniqueBankName` UNIQUE (`name`),
  PRIMARY KEY (`idbank`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`configuration`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`configuration` (
  `idconfiguration` BIGINT NOT NULL ,
  `ref_idremoteInfo` INT NOT NULL ,
  `ref_idlocalInfo` INT NOT NULL ,
  `ref_idbank` INT NOT NULL ,
  `date` DATETIME NULL ,
  `file` VARCHAR(256) NULL ,
  PRIMARY KEY (`idconfiguration`) ,
  INDEX `fk_configuration_remoteInfos1` (`ref_idremoteInfo` ASC) ,
  INDEX `fk_configuration_localInfo1` (`ref_idlocalInfo` ASC) ,
  INDEX `fk_configuration_bank1` (`ref_idbank` ASC) ,
  CONSTRAINT `fk_configuration_remoteInfos1`
    FOREIGN KEY (`ref_idremoteInfo` )
    REFERENCES `biomaj_log`.`remoteInfo` (`idremoteInfo` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_configuration_localInfo1`
    FOREIGN KEY (`ref_idlocalInfo` )
    REFERENCES `biomaj_log`.`localInfo` (`idlocalInfo` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_configuration_bank1`
    FOREIGN KEY (`ref_idbank` )
    REFERENCES `biomaj_log`.`bank` (`idbank` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`updateBank`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`updateBank` (
  `idupdateBank` INT NOT NULL AUTO_INCREMENT ,
  `ref_idconfiguration` BIGINT NOT NULL ,
  `updateRelease` VARCHAR(45) NULL ,
  `productionDirectoryPath` VARCHAR(256) NULL ,
  `productionDirectoryDeployed` TINYINT(1) NULL ,
  `sizeDownload` VARCHAR(45) NULL ,
  `sizeRelease` VARCHAR(45) NULL ,
  `startTime` DATETIME NULL ,
  `endTime` DATETIME NULL ,
  `elapsedTime` VARCHAR(45) NULL ,
  `isUpdated` TINYINT(1) NULL ,
  `nbSessions` INT NULL ,
  `idLastSession` BIGINT NULL ,
  PRIMARY KEY (`idupdateBank`) ,
  INDEX `fk_updateBank_configuration1` (`ref_idconfiguration` ASC) ,
  INDEX `index_idLastSession` (`idLastSession` ASC) ,
  CONSTRAINT `fk_updateBank_configuration1`
    FOREIGN KEY (`ref_idconfiguration` )
    REFERENCES `biomaj_log`.`configuration` (`idconfiguration` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`session`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`session` (
  `idsession` BIGINT NOT NULL ,
  `ref_idupdateBank` INT NOT NULL ,
  `href` VARCHAR(256) NULL ,
  `parse` VARCHAR(45) NULL ,
  `status` TINYINT(1) NULL ,
  `startTime` DATETIME NULL ,
  `endTime` DATETIME NULL ,
  `elapsedTime` VARCHAR(45) NULL ,
  `logfile` VARCHAR(128) NULL ,
  PRIMARY KEY (`idsession`) ,
  INDEX `fk_session_updateBank1` (`ref_idupdateBank` ASC) ,
  CONSTRAINT `fk_session_updateBank1`
    FOREIGN KEY (`ref_idupdateBank` )
    REFERENCES `biomaj_log`.`updateBank` (`idupdateBank` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`sessionTask`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`sessionTask` (
  `idsessionTask` INT NOT NULL AUTO_INCREMENT ,
  `startTime` DATETIME NULL ,
  `endTime` DATETIME NULL ,
  `elapsedTime` VARCHAR(45) NULL ,
  `status` VARCHAR(45) NULL ,
  `value` VARCHAR(128) NULL ,
  `nbExtract` INT NULL ,
  `nbLocalOnlineFiles` INT NULL ,
  `nbLocalOfflineFiles` INT NULL ,
  `nbDownloadFiles` INT NULL ,
  `bandwidth` FLOAT NULL ,
  `nbFilesMoved` INT NULL ,
  `nbFilesCopied` INT NULL ,
  `taskType` VARCHAR(45) NULL ,
  `nbreMetaProcess` INT NULL ,
  PRIMARY KEY (`idsessionTask`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`metaprocess`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`metaprocess` (
  `idmetaprocess` VARCHAR(45) NOT NULL ,
  `name` VARCHAR(45) NULL ,
  `startTime` DATETIME NULL ,
  `endTime` DATETIME NULL ,
  `elapsedTime` VARCHAR(128) NULL ,
  `status` VARCHAR(45) NULL ,
  `logfile` VARCHAR(256) NULL ,
  `block` VARCHAR(45) NULL ,
  `ref_idsessionTask` INT NOT NULL ,
  PRIMARY KEY (`idmetaprocess`) ,
  INDEX `fk_metaprocess_sessionTask1` (`ref_idsessionTask` ASC) ,
  CONSTRAINT `fk_metaprocess_sessionTask1`
    FOREIGN KEY (`ref_idsessionTask` )
    REFERENCES `biomaj_log`.`sessionTask` (`idsessionTask` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`process`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`process` (
  `idprocess` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `keyname` VARCHAR(45) NULL ,
  `exe` VARCHAR(128) NULL ,
  `args` VARCHAR(512) NULL ,
  `description` VARCHAR(512) NULL ,
  `type` VARCHAR(45) NULL ,
  `startTime` DATETIME NULL ,
  `endTime` DATETIME NULL ,
  `elapsedTime` VARCHAR(45) NULL ,
  `biomaj_error` TINYINT(1) NULL ,
  `timestamp` BIGINT NULL ,
  `value` VARCHAR(45) NULL ,
  `ref_idmetaprocess` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idprocess`) ,
  INDEX `fk_process_metaprocess1` (`ref_idmetaprocess` ASC) ,
  CONSTRAINT `fk_process_metaprocess1`
    FOREIGN KEY (`ref_idmetaprocess` )
    REFERENCES `biomaj_log`.`metaprocess` (`idmetaprocess` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`file`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`file` (
  `idfile` INT NOT NULL AUTO_INCREMENT ,
  `location` VARCHAR(512) NULL ,
  `size` BIGINT NULL ,
  `time` BIGINT NULL ,
  `link` TINYINT(1) NULL ,
  `is_extract` TINYINT(1) UNSIGNED NULL ,
  `volatile` TINYINT(1) NULL ,
  `refHash` VARCHAR(256) NULL ,
  `fileType` VARCHAR(45) NULL ,
  `ref_idprocess` INT NULL ,
  PRIMARY KEY (`idfile`) ,
  INDEX `fk_file_process1` (`ref_idprocess` ASC) ,
  CONSTRAINT `fk_file_process1`
    FOREIGN KEY (`ref_idprocess` )
    REFERENCES `biomaj_log`.`process` (`idprocess` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`productionDirectory`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`productionDirectory` (
  `idproductionDirectory` INT NOT NULL AUTO_INCREMENT ,
  `remove` DATETIME NULL ,
  `creation` DATETIME NULL ,
  `size` VARCHAR(45) NULL ,
  `state` VARCHAR(45) NULL ,
  `session` BIGINT NULL ,
  `path` VARCHAR(256) NULL ,
  `ref_idbank` INT NOT NULL ,
  PRIMARY KEY (`idproductionDirectory`) ,
  INDEX `fk_productionDirectory_bank1` (`ref_idbank` ASC) ,
  CONSTRAINT `fk_productionDirectory_bank1`
    FOREIGN KEY (`ref_idbank` )
    REFERENCES `biomaj_log`.`bank` (`idbank` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`message`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`message` (
  `idmessage` INT NOT NULL AUTO_INCREMENT ,
  `message` TEXT NULL ,
  `type` VARCHAR(45) NULL ,
  PRIMARY KEY (`idmessage`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`session_has_message`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`session_has_message` (
  `idsession_has_message` INT NOT NULL AUTO_INCREMENT ,
  `ref_idsession` BIGINT NOT NULL ,
  `ref_idmessage` INT NOT NULL ,
  PRIMARY KEY (`idsession_has_message`) ,
  INDEX `fk_session_has_message_session1` (`ref_idsession` ASC) ,
  INDEX `fk_session_has_message_message1` (`ref_idmessage` ASC) ,
  CONSTRAINT `fk_session_has_message_session1`
    FOREIGN KEY (`ref_idsession` )
    REFERENCES `biomaj_log`.`session` (`idsession` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_session_has_message_message1`
    FOREIGN KEY (`ref_idmessage` )
    REFERENCES `biomaj_log`.`message` (`idmessage` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`sessionTask_has_message`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`sessionTask_has_message` (
  `ref_idsessionTask` INT NOT NULL ,
  `ref_idmessage` INT NOT NULL ,
  `idsessionTask_has_message` INT NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`idsessionTask_has_message`) ,
  INDEX `fk_sessionTask_has_message_sessionTask1` (`ref_idsessionTask` ASC) ,
  INDEX `fk_sessionTask_has_message_message1` (`ref_idmessage` ASC) ,
  CONSTRAINT `fk_sessionTask_has_message_sessionTask1`
    FOREIGN KEY (`ref_idsessionTask` )
    REFERENCES `biomaj_log`.`sessionTask` (`idsessionTask` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sessionTask_has_message_message1`
    FOREIGN KEY (`ref_idmessage` )
    REFERENCES `biomaj_log`.`message` (`idmessage` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`metaprocess_has_message`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`metaprocess_has_message` (
  `ref_idmetaprocess` VARCHAR(45) NOT NULL ,
  `ref_idmessage` INT NOT NULL ,
  `idmetaprocess_has_message` INT NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`idmetaprocess_has_message`) ,
  INDEX `fk_metaprocess_has_message_metaprocess1` (`ref_idmetaprocess` ASC) ,
  INDEX `fk_metaprocess_has_message_message1` (`ref_idmessage` ASC) ,
  CONSTRAINT `fk_metaprocess_has_message_metaprocess1`
    FOREIGN KEY (`ref_idmetaprocess` )
    REFERENCES `biomaj_log`.`metaprocess` (`idmetaprocess` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_metaprocess_has_message_message1`
    FOREIGN KEY (`ref_idmessage` )
    REFERENCES `biomaj_log`.`message` (`idmessage` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`process_has_message`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`process_has_message` (
  `ref_idprocess` INT NOT NULL ,
  `ref_idmessage` INT NOT NULL ,
  `idprocess_has_message` INT NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`idprocess_has_message`) ,
  INDEX `fk_process_has_message_process1` (`ref_idprocess` ASC) ,
  INDEX `fk_process_has_message_message1` (`ref_idmessage` ASC) ,
  CONSTRAINT `fk_process_has_message_process1`
    FOREIGN KEY (`ref_idprocess` )
    REFERENCES `biomaj_log`.`process` (`idprocess` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_process_has_message_message1`
    FOREIGN KEY (`ref_idmessage` )
    REFERENCES `biomaj_log`.`message` (`idmessage` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`session_has_sessionTask`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`session_has_sessionTask` (
  `ref_idsession` BIGINT NOT NULL ,
  `ref_idsessionTask` INT NOT NULL ,
  `idsession_has_sessionTask` INT NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`idsession_has_sessionTask`) ,
  INDEX `fk_session_has_sessionTask_session1` (`ref_idsession` ASC) ,
  INDEX `fk_session_has_sessionTask_sessionTask1` (`ref_idsessionTask` ASC) ,
  CONSTRAINT `fk_session_has_sessionTask_session1`
    FOREIGN KEY (`ref_idsession` )
    REFERENCES `biomaj_log`.`session` (`idsession` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_session_has_sessionTask_sessionTask1`
    FOREIGN KEY (`ref_idsessionTask` )
    REFERENCES `biomaj_log`.`sessionTask` (`idsessionTask` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`sessionTask_has_file`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`sessionTask_has_file` (
  `ref_idsessionTask` INT NOT NULL ,
  `ref_idfile` INT NOT NULL ,
  `idsessionTask_has_file` INT NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`idsessionTask_has_file`) ,
  INDEX `fk_sessionTask_has_file_sessionTask1` (`ref_idsessionTask` ASC) ,
  INDEX `fk_sessionTask_has_file_file1` (`ref_idfile` ASC) ,
  CONSTRAINT `fk_sessionTask_has_file_sessionTask1`
    FOREIGN KEY (`ref_idsessionTask` )
    REFERENCES `biomaj_log`.`sessionTask` (`idsessionTask` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sessionTask_has_file_file1`
    FOREIGN KEY (`ref_idfile` )
    REFERENCES `biomaj_log`.`file` (`idfile` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

