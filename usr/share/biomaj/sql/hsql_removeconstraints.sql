-- Configuration
ALTER TABLE configuration DROP CONSTRAINT fk_configuration_remoteInfo1;
ALTER TABLE configuration DROP CONSTRAINT fk_configuration_localInfo1;
ALTER TABLE configuration DROP CONSTRAINT fk_configuration_bank1;

-- updateBank
ALTER TABLE updateBank DROP CONSTRAINT fk_updateBank_configuration1;

-- session
ALTER TABLE session DROP CONSTRAINT fk_session_updateBank1;

-- metaprocess
ALTER TABLE metaprocess DROP CONSTRAINT fk_metaprocess_sessionTask1;

-- process
ALTER TABLE process DROP CONSTRAINT fk_process_metaprocess1;

-- file
ALTER TABLE file DROP CONSTRAINT fk_file_process1;

-- productionDirectory
ALTER TABLE productionDirectory DROP CONSTRAINT fk_productionDirectory_bank1;

-- session_has_message
ALTER TABLE session_has_message DROP CONSTRAINT fk_session_has_message_session1;
ALTER TABLE session_has_message DROP CONSTRAINT fk_session_has_message_message1;

-- sessionTask_has_message
ALTER TABLE sessionTask_has_message DROP CONSTRAINT fk_sessionTask_has_message_sessionTask1;
ALTER TABLE sessionTask_has_message DROP CONSTRAINT fk_sessionTask_has_message_message1;

-- metaprocess_has_message
ALTER TABLE metaprocess_has_message DROP CONSTRAINT fk_metaprocess_has_message_metaprocess1;
ALTER TABLE metaprocess_has_message DROP CONSTRAINT fk_metaprocess_has_message_message1;

-- process_has_message
ALTER TABLE process_has_message DROP CONSTRAINT fk_process_has_message_process1;
ALTER TABLE process_has_message DROP CONSTRAINT fk_process_has_message_message1;

-- session_has_sessionTask
ALTER TABLE session_has_sessionTask DROP CONSTRAINT fk_session_has_sessionTask_session1;
ALTER TABLE session_has_sessionTask DROP CONSTRAINT fk_session_has_sessionTask_sessionTask1;

-- sessionTask_has_file
ALTER TABLE sessionTask_has_file DROP CONSTRAINT fk_sessionTask_has_file_sessionTask1;
ALTER TABLE sessionTask_has_file DROP CONSTRAINT fk_sessionTask_has_file_file1;

