-- -----------------------------------------------------
-- Table `biomaj_log`.`bw_user`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`bw_user` (
  `iduser` INT NOT NULL AUTO_INCREMENT ,
  `login` VARCHAR(45) NULL ,
  `password` VARCHAR(45) NULL ,
  `auth_type` VARCHAR(45) NULL ,
  `auth_key` VARCHAR(128) NULL ,
  `mail_address` VARCHAR(128) NULL ,
  PRIMARY KEY (`iduser`),
  CONSTRAINT `uniqueLogin` UNIQUE (`login`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `biomaj_log`.`bw_group`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`bw_group` (
  `idgroup` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  PRIMARY KEY (`idgroup`),
  CONSTRAINT `uniqueGroup` UNIQUE (`name`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`bw_user_has_group`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`bw_user_has_group` (
  `iduser_has_group` INT NOT NULL AUTO_INCREMENT ,
  `ref_iduser` INT NOT NULL ,
  `ref_idgroup` INT NOT NULL ,
  INDEX `fk_user_has_group_user1` (`ref_iduser` ASC) ,
  INDEX `fk_user_has_group_group1` (`ref_idgroup` ASC) ,
  PRIMARY KEY (`iduser_has_group`) ,
  CONSTRAINT `fk_user_has_group_user1`
    FOREIGN KEY (`ref_iduser` )
    REFERENCES `biomaj_log`.`bw_user` (`iduser` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_group_group1`
    FOREIGN KEY (`ref_idgroup` )
    REFERENCES `biomaj_log`.`bw_group` (`idgroup` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `biomaj_log`.`schema_version`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `biomaj_log`.`schema_version` (
  `idschema_version` INT NOT NULL AUTO_INCREMENT ,
  `version` VARCHAR(45) NULL ,
  PRIMARY KEY (`idschema_version`) )
ENGINE = InnoDB;

