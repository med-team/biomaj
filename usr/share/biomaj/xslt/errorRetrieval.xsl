<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:sch="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes=" sch">
	<xsl:output method="xml"/>    
    <xsl:template match="/">
    <xsl:result-document href="error_temp.xml" method="xml">
    	<errors>
        <xsl:if test="doc-available('./errors/error_subtemp.xml')">
   	    	<xsl:variable name="temp" select="document('./errors/error_subtemp.xml')"/>
	        <xsl:copy-of select="$temp/errors/*" />
        </xsl:if>
        <xsl:for-each select="/bank/configuration">
        <xsl:if test="position()=last()">
            <xsl:variable name="dbName" select="remoteInfos/serverInfo/dbName" />
            <xsl:for-each select="//session">
            	<xsl:if test="position()=last()">
                        <xsl:variable name="id" select="@id"/>
                        <xsl:variable name="start" select="@start"/>
                        <status sessionId="{$id}" dbName="{$dbName}" time="{$start}" elapsedTime="{@elapsedTime}" log="{@logfile}" value="{@status}" />
                        <xsl:for-each select="./error">
                            <errorMessage sessionId="{$id}" dbName="{$dbName}" time="{$start}" message="{@message}" />
                        </xsl:for-each>
                        <xsl:for-each select="./warning">
                            <warningMessage sessionId="{$id}" dbName="{$dbName}" time="{$start}" message="{@message}" />
                        </xsl:for-each>
                 </xsl:if>
            </xsl:for-each>
        </xsl:if>
        </xsl:for-each>
        </errors>
    </xsl:result-document>
	</xsl:template>
</xsl:stylesheet>
