<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:sch="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes=" sch">
	<xsl:output method="xml"/>    
    <xsl:template match="/">
    	<xsl:copy-of select="*" />
	</xsl:template>
</xsl:stylesheet>