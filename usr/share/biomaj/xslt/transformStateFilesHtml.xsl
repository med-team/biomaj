<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:sch="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes=" sch">
	<xsl:output method="html" encoding="ISO-8859-1" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" indent="yes"/>

	<xsl:key name="Prod" match="//production" use="@session"/>
    <xsl:template name="header">
		<head>
			<title>BioMAJ - Web User Interface</title>
			<xsl:variable name="dbkeyword">
				<xsl:value-of select="/bank/configuration[position()=last()]/remoteInfos/serverInfo/dbName"/>
			</xsl:variable> 
			<meta name="keywords" content="bio bank biomaj {$dbkeyword} "/>
            <link rel="stylesheet" type="text/css" href="css/styles.css" />
            <script language="JavaScript" type="text/JavaScript" src="css/inc.js" ></script>
		</head>
	</xsl:template>
    
    <xsl:template match="/">
			<html>
		<xsl:call-template name="header"/>
        <body onload="MM_preloadImages('images/menu/biomaj_o.png','images/menu/globalStats_o.png','images/menu/localBank_o.png', 'images/menu/home_o.png');">
				<table cellspacing="0" cellpadding="0" class="outermost">
                  <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0" class="menu">
                        <tr>
                          <td align="center" valign="middle" class="tmenu">
                          <div id="message" style="font-family:Geneva, Arial, Helvetica, sans-serif; font-size:9pt;"></div></td>
                          
                        </tr>
                        <tr>
                          <td  class="mmenu">
                            <a href="index.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','images/menu/home_o.png',1)"><img src="images/menu/home.png" width="165" height="27" border="0" name="Image4" /></a><br />
                            <a href="bankListing.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/menu/localBank_o.png',1)"><img src="images/menu/localBank.png" width="165" height="27" border="0" name="Image1" /></a><br />
                            <a href="stats.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','images/menu/globalStats_o.png',1)"><img src="images/menu/globalStats.png" width="165" height="27" border="0" name="Image2" /></a><br />
                            <a href="http://biomaj.genouest.org" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image3','','images/menu/biomaj_o.png',1)"><img src="images/menu/biomaj.png" width="165" height="27" border="0" name="Image3"/></a><br />         
                             <img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image5"/><br />      
<img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image6"/>
               			  </td>
                        </tr>                
                        <tr>
                            <td class="bmenu1">
                            	<ul style="font-weight:bold">
                                
                                <xsl:variable name="dbname">
                                    <xsl:value-of select="/bank/configuration[position()=last()]/remoteInfos/serverInfo/dbName"/>
                                </xsl:variable>
            					
                                <li> <a href="{$dbname}Detailed.html" style="color:#FFFFFF">Detailed Report</a></li>
            					
                                <li> <a href="{$dbname}_stats.html" style="color:#FFFFFF">
                                    <xsl:value-of select="$dbname"/>Stats Page</a></li>
                                <li>[<a href="index.html" style="color:#FFFFFF">Main</a>]|[<a href="javascript:history.back()" style="color:#FFFFFF">Back</a>]</li>
    
                                <xsl:result-document href="{$dbname}Detailed.html" method="html">
                                    <xsl:call-template name="detailedInfo"/>
                                </xsl:result-document>
                                
                                </ul>
                             </td>
                        </tr>
                        <tr>
                          <td valign="top" class="bmenu2t">
                          
                          </td>
                        </tr>
                      </table>
                    </td>
                
                
                
                   <td align="right" valign="top">
                      <table cellspacing="0" cellpadding="0" class="content">
                        <tr>
                          <td colspan="3" class="tcontent">
						<span class="t_left">
                          	<img src="images/topRight.png" />
                          
                          </span>
                          <span class="t_right">
                            <img src="images/topLeft.png" />                      
                          </span>
                          
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="mcontent">
                        
                            <img src="images/banner.png" />
                        </td>
                        </tr>
                        <tr>
                          	<td valign="top" style="border-top: 10px solid #392C1F;" class="lcontent_i_about">
                            
                            <h1> Brief Information </h1>
                            
                            <table width="100%">
                            <xsl:for-each select="/bank/configuration">
                            <xsl:if test="position()=last()">
                            	<tr>
                                	<td style="color:#FFFFFF; background-color:#333333"> Bank Name</td>
                                    <td style="background-color:#CCCCCC"> <xsl:value-of select="./remoteInfos/serverInfo/dbName"/></td>                   
                                </tr>
                                <tr>
                                	<td style="color:#FFFFFF; background-color:#333333"> Description</td>
                                    <td style="background-color:#CCCCCC"> <xsl:value-of select="./remoteInfos/serverInfo/dbFullname"/></td>                   
                                </tr>
                                <tr>
                                	<td style="color:#FFFFFF; background-color:#333333"> Bank Type</td>
                                    <td style="background-color:#CCCCCC"> <xsl:value-of select="./remoteInfos/serverInfo/dbType"/></td>                   
                                </tr>
                                <tr>
                                	<td style="color:#FFFFFF; background-color:#333333"> URL</td>
                                    <td style="background-color:#CCCCCC"><a href="{./remoteInfos/serverInfo/protocol}://{./remoteInfos/serverInfo/server}"> <xsl:value-of select="./remoteInfos/serverInfo/protocol"/>://<xsl:value-of select="./remoteInfos/serverInfo/server"/></a></td>                   
                                </tr>
                            </xsl:if>
                            </xsl:for-each>
                            </table>
                            <br /><br />
                            <hr />
                            <h1> Production Directories </h1>
                            <table class="sortable" width="100%">
                            <tr style="color:#FFFFFF; background-color:#333333">
                            	<th>Creation Date</th>
                                <th>Path</th>
                                <th>Size</th>
                                <th>Status</th>
                            </tr>
                            <xsl:for-each select="/bank/production/directory">
                            	 <xsl:sort select="@session" order="descending"/> 
                            	
                                <tr class="row{position() mod 2}">
                                	<td><xsl:value-of select="@creation" /> </td>
                                    <td><xsl:value-of select="@path" /></td>
                                    <td><xsl:value-of select="@size" /></td>
                                    <td>
                                    	<xsl:value-of select="@state" />
                                        <xsl:variable select="@state" name="status" />
                                    	<xsl:if test="$status = 'deleted'">
                                        	<br /> on <xsl:value-of select="@remove" />
                                        </xsl:if>
                                    </td>
                                </tr>                                                
                                                                                   
                            </xsl:for-each>
                            </table>
                            
                             <xsl:variable name="dbname">
                                    <xsl:value-of select="/bank/configuration[position()=last()]/remoteInfos/serverInfo/dbName"/>
                                </xsl:variable>
                            <br /><br />
                            <b><i> For more details, please <a href="{$dbname}Detailed.html">click here</a></i></b>
                            
                            </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="footer">
                            
                           </td> 
                        </tr>
                      </table>
                    </td>            
                
                    <td width="100%" valign="top">
                    <table cellspacing="0" cellpadding="0" class="right">
                        <tr>
                          <td class="tright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="mright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td class="bright">
                          
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
		    </body>
			</html>
   </xsl:template>
    

	<xsl:template name="detailedInfo">
			<html>
		<xsl:call-template name="header"/><body onload="MM_preloadImages('images/menu/biomaj_o.png','images/menu/globalStats_o.png','images/menu/localBank_o.png', 'images/menu/home_o.png');">
				<table cellspacing="0" cellpadding="0" class="outermost">
                  <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0" class="menu">
                        <tr>
                          <td align="center" valign="middle" class="tmenu">
                          <div id="message" style="font-family:Geneva, Arial, Helvetica, sans-serif; font-size:9pt;"></div></td>
                          
                        </tr>
                        <tr>
                          <td  class="mmenu">
                            <a href="index.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','images/menu/home_o.png',1)"><img src="images/menu/home.png" width="165" height="27" border="0" name="Image4" /></a><br />
                            <a href="bankListing.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/menu/localBank_o.png',1)"><img src="images/menu/localBank.png" width="165" height="27" border="0" name="Image1" /></a><br />
                            <a href="stats.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','images/menu/globalStats_o.png',1)"><img src="images/menu/globalStats.png" width="165" height="27" border="0" name="Image2" /></a><br />
                            <a href="http://biomaj.genouest.org" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image3','','images/menu/biomaj_o.png',1)"><img src="images/menu/biomaj.png" width="165" height="27" border="0" name="Image3"/></a><br />         
                             <img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image5"/><br />      
<img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image6"/>
               			  </td>
                        </tr>                
                        <tr>
                            <td class="bmenu1">
                            <ul style="font-weight:bold">
                                <xsl:variable name="dbname">
                                    <xsl:value-of select="/bank/configuration[position()=last()]/remoteInfos/serverInfo/dbName"/>
                                </xsl:variable>
                                
                                <li><a href="{$dbname}.html" style="color:#FFFFFF">Brief Report</a></li>
            
                                <li><a href="{$dbname}_stats.html" style="color:#FFFFFF">
                                    <xsl:value-of select="$dbname"/>Stats Page</a></li>
                                
                                <li>[<a href="index.html" style="color:#FFFFFF">Main</a>]|[<a href="javascript:history.back()" style="color:#FFFFFF">Back</a>]</li>
                                
                                <xsl:call-template name="Make_bank_stat" />
                            </ul>
                                
                             </td>
                        </tr>
                        <tr>
                          <td valign="top" class="bmenu2t">
                          
                          </td>
                        </tr>
                      </table>
                    </td>
                
                
                
                   <td align="right" valign="top">
                      <table cellspacing="0" cellpadding="0" class="content">
                        <tr>
                          <td colspan="3" class="tcontent">
						<span class="t_left">
                          	<img src="images/topRight.png" />
                          
                          </span>
                          <span class="t_right">
                            <img src="images/topLeft.png" />                      
                          </span>
                          
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="mcontent">
                        
                            <img src="images/banner.png" />
                        </td>
                        </tr>
                        <tr>
                          	<td valign="top" style="border-top: 10px solid #392C1F;" class="lcontent_i_about">
                          
                            <xsl:for-each select="//configuration">
                                <xsl:sort select="@id" order="descending" data-type="number"/>
                                <div style="font-size:18px; font-weight:bold">
                                    <xsl:value-of select="descendant::dbFullname"/>
                                </div>
                                <br /><br />
        						<div style="font-size:16px; font-weight:bold">
                                	Configuration ( <xsl:value-of select="@date"/> )
                                </div>
                                <table class="sortable">
                                    <tr style="color:#FFFFFF; background-color:#333333">
                                        <th>
                                            <h3>Server infos</h3>
                                        </th>
                                        <th>
                                            <h3>local Infos</h3>
                                        </th>
                                        <th>
                                            <h3>Parameter info</h3>
                                        </th>
                                    </tr>
                                    <tr valign="top" class="row0">
                                        <td valign="top">
                                            <table class="soustab">
                                                <tr>
                                                    <th>
                                                        <div class="property">Bank Name</div>
                                                    </th>
                                                    <td valign="top">
                                                        <xsl:value-of select="descendant::dbName"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <div class="property">Protocol</div>
                                                    </th>
                                                    <td valign="top">
                                                        <xsl:value-of select="descendant::protocol"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <div class="property">Bank Type</div>
                                                    </th>
                                                    <td valign="top">
                                                        <xsl:value-of select="descendant::dbType"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <div class="property">Remote Database</div>
                                                    </th>
                                                    <td valign="top">
                                                        <a href="{descendant::protocol}://{descendant::server}/{descendant::remoteDir}">
                                                            <xsl:value-of select="descendant::protocol"/>://<xsl:value-of select="descendant::server"/>/<xsl:value-of select="descendant::remoteDir"/></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top">
                                            <table class="soustab">
                                                <tr>
                                                    <th>
                                                        <div class="property">Offline directory</div>
                                                    </th>
                                                    <td valign="top">
                                                        <p>
                                                            <xsl:value-of select="descendant::offlineDirectory"/>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <div class="property">Version Home directory</div>
                                                    </th>
                                                    <td valign="top">
                                                        <p>
                                                            <xsl:value-of select="descendant::versionDirectory"/>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <div class="property">Nb version management</div>
                                                    </th>
                                                    <td valign="top">
                                                        <xsl:value-of select="descendant::nversions/@nb"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
        
                                        <td valign="top">
                                            <table class="soustab">
                                                <tr>
                                                    <th>
                                                        <div class="property">Frequency update</div>
                                                    </th>
                                                    <td valign="top">
                                                        <xsl:value-of select="descendant::frequency"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <div class="property">RegExpr Remote Files</div>
                                                    </th>
                                                    <td valign="top">
                                                        <xsl:value-of select="descendant::remoteFiles/@regexp"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <div class="property">RegExpr Local Files</div>
                                                    </th>
                                                    <td valign="top">
                                                        <xsl:value-of select="descendant::localFiles/@regexp"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <div class="property">RegExpr Release</div>
                                                    </th>
                                                    <td valign="top">
                                                        <xsl:value-of select="descendant::releaseInfo/@regexp"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <div class="property">File Release</div>
                                                    </th>
                                                    <td valign="top">
                                                        <xsl:value-of select="descendant::releaseInfo/@file"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="row0">
                                        <th colspan="1">Properties file</th>
                                        <td colspan="2">
                                            <p>
                                                <xsl:value-of select="@file"/>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                                <p align="center">
                                
                                    <h2>Registred Update Cycle</h2>
                                    <table class="sortable" width="100%">
                                  
                                        <tr style="color:#FFFFFF; background-color:#333333">
                                            <th>Date</th>
                                            <th>Release</th>
                                            <th>Availability</th>
                                            <th>Status</th>
                                            <th>Duration</th>
                                        </tr>
                                        <xsl:for-each select="descendant::updateBank">
                                            <xsl:sort select="@idLastSession" order="descending" data-type="number"/>
                                            <xsl:variable name="id">
                                                <xsl:value-of select="@idLastSession"/>
                                            </xsl:variable>
                                            <xsl:variable name="name" select="parent::node()/remoteInfos/serverInfo/dbName" />
                                            <tr class="row{position() mod 2}">
                                                <td>
                                                    <a href="{$name}{$id}.html">
                                                        <xsl:value-of select="@start"/>
                                                    </a>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="workflowInfo/release/@value"/>
                                                </td>
                                                <td>
                                                    <xsl:call-template name="Dispo_status"/>
                                                </td>
                                                <td>
                                                    <xsl:call-template name="Update_status"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="@elapsedTime"/>
                                                </td>
                                            </tr>
											<xsl:result-document href="{$name}{$id}.html" method="html">
                                                <html>
                                               <xsl:call-template name="header"/><body onload="MM_preloadImages('images/menu/biomaj_o.png','images/menu/globalStats_o.png','images/menu/localBank_o.png', 'images/menu/home_o.png');">
                                                    <table cellspacing="0" cellpadding="0" class="outermost">
                                                      <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0" class="menu">
                                                                <tr>
                                                                  <td align="center" valign="middle" class="tmenu">
                                                                  <div id="message" style="font-family:Geneva, Arial, Helvetica, sans-serif; font-size:9pt;"></div></td>
                                                                  
                                                                </tr>
                                                                <tr>
                                                                  <td  class="mmenu">
                            <a href="index.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','images/menu/home_o.png',1)"><img src="images/menu/home.png" width="165" height="27" border="0" name="Image4" /></a><br />
                            <a href="bankListing.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/menu/localBank_o.png',1)"><img src="images/menu/localBank.png" width="165" height="27" border="0" name="Image1" /></a><br />
                            <a href="stats.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','images/menu/globalStats_o.png',1)"><img src="images/menu/globalStats.png" width="165" height="27" border="0" name="Image2" /></a><br />
                            <a href="http://biomaj.genouest.org" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image3','','images/menu/biomaj_o.png',1)"><img src="images/menu/biomaj.png" width="165" height="27" border="0" name="Image3"/></a><br />         
                             <img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image5"/><br />      
<img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image6"/>
               			  </td>
                                                                </tr>                
                                                                <tr>
                                                                    <td class="bmenu1">
                                                            			[<a href="index.html" style="color:#FFFFFF">Main</a>]|[<a href="javascript:history.back()" style="color:#FFFFFF">back</a>]
                                                                    </td>
                                                                </tr>
                                                                   <tr>
                                                                  <td valign="top" class="bmenu2t">
                                                                  
                                                                  </td>
                                                                </tr>
                                                              </table>
                                                            </td>               
                                                        
                                                            
                                                            <td align="right" valign="top">
                                                              <table cellspacing="0" cellpadding="0" class="content">
                                                                <tr>
                                                                  <td colspan="3" class="tcontent"><span class="t_left">
                                                                    <img src="images/topRight.png" />
                                                                  
                                                                  </span>
                                                                  <span class="t_right">
                                                                    <img src="images/topLeft.png" />                      
                                                                  </span>
                                                                  
                                                                  </td>
                                                                </tr>
                                                                <tr>
                                                                  <td colspan="3" class="mcontent">
                                                                
                                                                    <img src="images/banner.png" />
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                  <td valign="top" style="border-top: 10px solid #392C1F;" class="lcontent_i_about">

                                                                    <h1>
                                                                        <xsl:value-of select="$name "/>: Updates Cycles
                                                                    </h1>
                
                                                                    <h2>bank:
                                                                        <xsl:value-of select="$name"/>
                                                                    </h2>
                
                                                                    <xsl:variable name="back">
                                                                        <xsl:value-of select="@id"/>
                                                                    </xsl:variable>
                                                                    <h3>Release</h3>
                                                                    <table class="sortable" width="100%">
                                                                        <tr>
                                                                            <th colspan="1" style="color:#FFFFFF; background-color:#333333">Release</th>
                                                                            <td colspan="5" style="background-color:#CCCCCC">
                
                                                                                <xsl:value-of select="workflowInfo/release/@value"/>
                                                                            </td>
                                                                        </tr>
                
                                                                        <tr>
                                                                            <th style="color:#FFFFFF; background-color:#333333">BankSize</th>
                                                                            <td style="background-color:#CCCCCC">
                                                                                <xsl:value-of select="workflowInfo/sizeRelease/@size"/>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th style="color:#FFFFFF; background-color:#333333">Downloaded files size</th>
                                                                            <td style="background-color:#CCCCCC">
                                                                                <xsl:value-of select="workflowInfo/sizeDownload/@size"/>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th style="color:#FFFFFF; background-color:#333333">Elapsed time</th>
                                                                            <td colspan="5" style="background-color:#CCCCCC">
                                                                                <xsl:value-of select="@elapsedTime"/>
                                                                            </td>
                                                                        </tr>
                
                                                                        <tr> 
                                                                            <th style="color:#FFFFFF; background-color:#333333">Online Directory</th>
                                                                            <td colspan="5" style="background-color:#CCCCCC">
                                                                                <a href="{workflowInfo/productionDirectory/@path}">
                                                                                    <xsl:value-of select="workflowInfo/productionDirectory/@path"/>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <br></br>
                                                                    <h3>Sessions infos</h3>
                                                                    <table class="sortable" width="100%">
                                                                        <tr style="color:#FFFFFF; background-color:#333333">
                                                                            <th>Start</th>
                                                                            <th>End</th>
                                                                            <th>Elapsed Time</th>
                                                                            <th>Nb Files</th>
                                                                            <th>Status</th>
                                                                        </tr>
                                                                        <!--			<xsl:for-each select="./session/*/@status">-->
                
                                                                        <!--														<xsl:for-each select="child::*[name()!='bankstate']">-->
                                                                        <xsl:for-each select="session">
                                                                            <xsl:call-template name="print-session"/>
                                                                        </xsl:for-each>
                                                                    </table>
                                                                    <p>(Local => files from preview release)</p>
                
                                                                
                                                                    <div id="Process">
                                                                        <H3>METAPROCESS :</H3>
                                                                        <xsl:for-each select="session">
                                                                            <xsl:call-template name="PostProcess"/>
                                                                        </xsl:for-each>
                                                                    </div>
                                                                 </td>
                                                                 
                                                                </tr>
                                                                <tr>
                                                                  <td colspan="3" class="footer">
                                                                    
                                                                   </td> 
                                                                </tr>
                                                              </table>
                                                            </td>            
                                                        
                                                            <td width="100%" valign="top">
                                                            <table cellspacing="0" cellpadding="0" class="right">
                                                                <tr>
                                                                  <td class="tright">
                                                                  
                                                                  </td>
                                                                </tr>
                                                                <tr>
                                                                  <td valign="top" class="mright">
                                                                  
                                                                  </td>
                                                                </tr>
                                                                <tr>
                                                                  <td class="bright">
                                                                  
                                                                  </td>
                                                                </tr>
                                                              </table>
                                                            </td>
                                                          </tr>
                                                        </table>
                                                    </body>
                                                  </html>
                                            </xsl:result-document>
                                        </xsl:for-each>
                                    </table>
                                </p>
                                <img src="images/trash.gif" width="32" height="32"/>= Deleted Release
        
                                <img src="images/Closed_Folder_yellow.gif" width="32" height="32"/>= Release Available on local Repository
                                <br></br>
                                <img src="images/uptodate.gif"/>Completed Cycle
                                <img src="images/updating.gif"/>Runing or crashed Cycle
                                <img src="images/obsolete.gif"/>Obsolete Cycle
                                <img src="images/misc.png" width="16" height="16"/>Empty Cycle
                                <br></br>
                                <img src="images/colbleu.png"/>
                                <hr /><br /><br />
                            </xsl:for-each>

                      		</td>
                    	</tr>
                        <tr>
                          <td colspan="3" class="footer">
                            
                           </td> 
                        </tr>
                      </table>
                   </td>            
                
                    <td width="100%" valign="top">
                    	<table cellspacing="0" cellpadding="0" class="right">
                        <tr>
                          <td class="tright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="mright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td class="bright">
                          
                          </td>
                        </tr>
                    </table>
                   	</td>
                  </tr>
                </table>
		      </body>
			</html>
	</xsl:template>
	
	
	<xsl:template name="PostProcess">

		<xsl:for-each select="descendant-or-self::metaprocess">

			<br></br>
			<h2>Session:<xsl:value-of select="../../@start"/></h2>
			<table width="100%">
				<tr>
					<th>Meta Process : <xsl:value-of select="@name"/> (duration :
						<xsl:value-of select="@elapsedTime"/>)</th>
				</tr>
			</table>
			<xsl:for-each select="process">
				<table class="sortable" width="100%">
					<tr style="color:#FFFFFF; background-color:#333333">
						<th>PP Name</th>
						<th>description</th>
						<th>cmd</th>
						<th>Elabs. Time</th>
						<th>Status</th>
					</tr>
					<tr style="background-color:#CCCCCC">
						<td>
							<xsl:variable name="PPNAME" select="@name"/>
							<xsl:value-of select="@name"/>
						</td>
						<td>
							<xsl:value-of select="@desc"/>
						</td>
						<td>
							<xsl:value-of select="@exe|@args"/>
						</td>
						<td>El.Time.: <xsl:value-of select="@elapsedTime"/></td>
						<td>
							<xsl:if test="(compare(@biomaj_error,'false')=0)">
								<img src="images/uptodate.gif" alt="Process completed"/>
							</xsl:if>

							<xsl:if test="(compare(@biomaj_error,'false')!=0)">
								<img src="images/obsolete.gif" alt="Process crashed"/>
							</xsl:if>
						</td>
					</tr>
					<tr>
						<!--						<table>
							<tr>
								<th>Warn</th>
							</tr>
							<xsl:for-each select="warning">
								<tr>
									<xsl:value-of select="@message"/>
								</tr>
							</xsl:for-each>
						</table>-->
					</tr>
					</table>

					<xsl:if test="count(warning)&gt; 0">
						<table class="soustab">
							<tr>
								<th>Warning</th>
							</tr>

							<xsl:for-each select="warning">
								<tr>
									<td>
										<xsl:value-of select="@message"/>
									</td>
								</tr>
							</xsl:for-each>
						</table>
					</xsl:if>


					<xsl:if test="count(error)&gt; 0">
						<table class="soustab">
							<tr>
								<th>Errors</th>
							</tr>

							<xsl:for-each select="error">
								<tr>
									<td>
										<xsl:value-of select="@message"/>
									</td>
								</tr>
							</xsl:for-each>
						</table>
					</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>


	<xsl:template name="print-session">
		<tr style="background-color:#CCCCCC">
			<!-- Modif O.Filangi : le mot Session etait affiche constamment et decalait les autres champs
                         <td>
				<b>
					<xsl:value-of select="name()"/>
				</b>
			</td>-->
			<td>
				<xsl:value-of select="@start"/>
			</td>
			<td>
				<xsl:value-of select="@end"/>
			</td>
			<td>
				<xsl:value-of select="@elapsedTime"/>
			</td>
			<td>download=<xsl:value-of select="count(./download/files)"/>/
			
				Extract=<xsl:value-of select="count(./extract/files)"/>/
			
				Local=<xsl:value-of select="count(./addLocaFiles/files)"/></td>
			<td>
				<xsl:if test="(compare(@status,'true')!=0)">
					<img src="images/obsolete.gif" alt="Update Cycle crashed"/>
				</xsl:if>

				<xsl:if test="(compare(@status,'true')=0)">
					<img src="images/uptodate.gif" alt="Update Cycle Completed"/>
				</xsl:if>

				<!--			<xsl:call-template name="status"/>-->
			</td>
		</tr>
		<xsl:for-each select="child::warning">
			<xsl:call-template name="print_warn"/>
		</xsl:for-each>
		<xsl:for-each select="child::error">
			<xsl:call-template name="print_err"/>
		</xsl:for-each>
	</xsl:template>


	<xsl:template name="print_warn">
		<tr>
			<td>
				<div style="color:blue">
					<xsl:value-of select="name()"/>
				</div>
			</td>
			<td colspan="5">
				<div style="color:purple">
					<xsl:value-of select="@message"/>
				</div>
			</td>
		</tr>
	</xsl:template>

	<xsl:template name="print_err">
		<tr>
			<td>
				<div style="color:blue">
					<xsl:value-of select="name()"/>
				</div>
			</td>
			<td colspan="5">
				<div style="color:red">
					<xsl:value-of select="@message"/>
				</div>
			</td>
		</tr>
	</xsl:template>

	<xsl:template name="Update_status">

		<xsl:if test="compare(@update,'false')=0">
			<xsl:if test="compare(session[position()=last()]/@status,'true')=0">

				<img src="images/misc.png" width="32" height="32" alt="Empty update cycle"/>
			</xsl:if>

			<xsl:if test="compare(session[position()=last()]/@status,'false')=0">
				<xsl:if test="position() = last()">
					<img src="images/updating.gif" alt="Open Cycle"/>
				</xsl:if>
				<xsl:if test="position() != last()">
					<img src="images/obsolete.gif"/>
				</xsl:if>
			</xsl:if>
		</xsl:if>
		<xsl:if test="compare(@update,'true')=0">

			<img src="images/uptodate.gif" alt="Release Downloaded"/>
		</xsl:if>
		<!--		<xsl:if test="compare(@update,'false')=0">
			<img src="images/obsolete.gif"/>
		</xsl:if>-->

		<!--		<xsl:if test="(compare(.,'work') != 0) and (compare(.,'ok') != 0)">
			<p> test3</p>
			<img src="images/error.gif"/>
			<xsl:value-of select="." />
		</xsl:if>-->
	</xsl:template>
	<xsl:template name="Dispo_status">
		<xsl:variable name="vers_key" select="@idLastSession"/>
		<xsl:variable name="dispo" select="//directory[@session=$vers_key][position()=last()]/@state"/>



		<xsl:if test="compare($dispo,'deleted')=0">
			<img src="images/trash.gif" width="40" height="40" alt="Release {$dispo}"/>
		</xsl:if>

		<xsl:if test="compare($dispo,'available' )=0">
			<img src="images/Closed_Folder_yellow.gif" width="32" height="32" alt="Release {$dispo}"/>
		</xsl:if>

		<xsl:if test="compare($dispo,'')=0">
			<img src="images/error.gif" alt="Release {$dispo}"/></xsl:if>
		<!--		<xsl:if test="(compare(.,'work') != 0) and (compare(.,'ok') != 0)">
			<p> test3</p>
			<img src="images/error.gif"/>
			<xsl:value-of select="." />
		</xsl:if>-->
	</xsl:template>

	<xsl:template name="Make_bank_stat">
		<xsl:variable name="BANK" select="/bank/configuration[position()=last()]/remoteInfos/serverInfo/dbName"/>
		<!--		<p>NOM DE LA BANQUE RR = <xsl:value-of select="$RR"/></p>-->

		<xsl:result-document href="{$BANK}_stats.html" method="html">
			<xsl:call-template name="mainStats"/>
		</xsl:result-document>
	</xsl:template>

	<xsl:template name="stat">

		<xsl:variable name="BANK" select="/bank/configuration[position()=last()]/remoteInfos/serverInfo/dbName"/>
        <center><h2>STATISTICS FOR <xsl:value-of select="/bank/configuration[position()=last()]/remoteInfos/serverInfo/dbName" /></h2></center>
		<!--		<p>NOM DE LA BANQUE = <xsl:value-of select="$BANK"/></p>-->
        <div style="height:620px; overflow:hidden">
		<img src="stats/{$BANK}-size.png" id="bankSize" name="bankSize" width="100%"/><br /><br /><br /><br />
		<img src="stats/{$BANK}-time.png" id="bankTime" name="bankTime" width="100%"/><br /><br /><br /><br />
		<img src="stats/{$BANK}-bar-times.png" id="bankDuration" name="bankDuration" width="100%"/><br /><br /><br /><br />
		<img src="stats/{$BANK}-availabletime.png" name="bankSession" id="bankSession" width="100%"/><br /><br /><br /><br />
		<img src="./stats/{$BANK}-nbdownl.png" name="bankDownload" id="bankDownload" width="100%"/>
		<img src="./stats/{$BANK}-bandwidth.png" name="bankBandwidth" id="bankBandwidth" width="100%"/>
        </div>
	</xsl:template>

	<xsl:template name="mainStats">
    <html>
		<xsl:call-template name="header"/><body onload="MM_preloadImages('images/menu/biomaj_o.png','images/menu/globalStats_o.png','images/menu/localBank_o.png', 'images/menu/home_o.png');">
            <table cellspacing="0" cellpadding="0" class="outermost">
              <tr>
                <td valign="top">
                    <table cellspacing="0" cellpadding="0" class="menu">
                        <tr>
                          <td align="center" valign="middle" class="tmenu">
                          <div id="message" style="font-family:Geneva, Arial, Helvetica, sans-serif; font-size:9pt;"></div></td>
                          
                        </tr>
                        <tr>
                          <td  class="mmenu">
                            <a href="index.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','images/menu/home_o.png',1)"><img src="images/menu/home.png" width="165" height="27" border="0" name="Image4" /></a><br />
                            <a href="bankListing.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/menu/localBank_o.png',1)"><img src="images/menu/localBank.png" width="165" height="27" border="0" name="Image1" /></a><br />
                            <a href="stats.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','images/menu/globalStats_o.png',1)"><img src="images/menu/globalStats.png" width="165" height="27" border="0" name="Image2" /></a><br />
                            <a href="http://biomaj.genouest.org" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image3','','images/menu/biomaj_o.png',1)"><img src="images/menu/biomaj.png" width="165" height="27" border="0" name="Image3"/></a><br />         
                             <img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image5"/><br />      
<img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image6"/>
               			  </td>
                        </tr>                
                        <tr>
                            <td class="bmenu1">
                            <xsl:variable name="bankName" select="/bank/configuration[position()=last()]/remoteInfos/serverInfo/dbName" />
                            <ul style="font-weight:bold">
                            	<li><a href="{$bankName}.html" style="color:#FFFFFF">Brief Report</a></li>
                            	<li><a href="{$bankName}Detailed.html" style="color:#FFFFFF">Detailed Report</a></li>
                            </ul>
                            <b style="color:#FFFFFF">STATISTICS</b>
                            <ul style="color:#FFFFFF; font-weight:bold">
                            <li><a href="#bankSize" style="color:#FFFFFF">Size Evolution</a></li>
                            <li><a href="#bankTime" style="color:#FFFFFF"> Time Evolution</a></li>
                            <li><a href="#bankDuration" style="color:#FFFFFF">Duration</a></li>
                            <li><a href="#bankSession" style="color:#FFFFFF">Session Duration</a></li>
                            <li><a href="#bankDownload" style="color:#FFFFFF">Download</a></li>
                            <li><a href="#bankBandwidth" style="color:#FFFFFF">Bandwidth</a></li>                           
                           </ul>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="bmenu2t">
                               
                            </td>
                        </tr>
                     </table>
                   </td>               
                        
                            
                    <td align="right" valign="top">
                      <table cellspacing="0" cellpadding="0" class="content">
                        <tr>
                          <td colspan="3" class="tcontent"><span class="t_left">
                            <img src="images/topRight.png" />
                          
                          </span>
                          <span class="t_right">
                            <img src="images/topLeft.png" />                      
                          </span>
                          
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="mcontent">
                        
                            <img src="images/banner.png" />
                        </td>
                        </tr>
                        <tr>
                          <td valign="top" style="border-top: 10px solid #392C1F;" class="lcontent_i_about">
								<xsl:call-template name="stat"/>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="footer">
                            
                           </td> 
                        </tr>
                      </table>
                    </td>            
                
                    <td width="100%" valign="top">
                    <table cellspacing="0" cellpadding="0" class="right">
                        <tr>
                          <td class="tright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="mright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td class="bright">
                          
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
		      </body>
			</html>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Omim040407" userelativepaths="yes" externalpreview="yes" url="omim.xml" htmlbaseurl="" outputurl="toto.html" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="" ><advancedProp name="sInitialMode" value=""/><advancedProp name="bXsltOneIsOkay" value="false"/><advancedProp name="bSchemaAware" value="false"/><advancedProp name="bXml11" value="true"/><advancedProp name="iValidation" value="0"/><advancedProp name="bExtensions" value="true"/><advancedProp name="iWhitespace" value="0"/><advancedProp name="sInitialTemplate" value=""/><advancedProp name="bTinyTree" value="true"/><advancedProp name="bWarnings" value="false"/><advancedProp name="bUseDTD" value="false"/></scenario></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no" ><SourceSchema srcSchemaPath="..\statefile\statefiles230306\omim.xml" srcSchemaRoot="bank" AssociatedInstance="" loaderFunction="document" loaderFunctionUsesURI="no"/></MapperInfo><MapperBlockPosition><template match="bank"></template><template match="//dbName"><block path="p/xsl:value&#x2D;of" x="242" y="147"/><block path="xsl:value&#x2D;of" x="131" y="164"/></template><template match="dbName"></template></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->
