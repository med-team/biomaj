<?xml version="1.0"?>
<!-- 
 Copyright or Copr. INRIA/INRA
 mail David.Allouche@toulouse.inra.fr, Anthony.Assi@irisa.fr,Yoann.Beausse@toulouse.inra.fr,
 Olivier.Filangi@irisa.fr, Christophe.Caron@jouy.inra.fr,Hugues.Leroy@irisa.fr,Veronique.Martin@jouy.inra.fr

 Biomaj Software is a workflows motor engin dedicated to biological bank managenemt.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 
 
 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
--> 
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:sch="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes=" sch">
	<xsl:output method="html" encoding="ISO-8859-1" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" indent="yes"/>
	<!-- -->

	<xsl:key name="allbanks" match="//bank" use="@dbname"/>
    <xsl:key name="typeatts" match="//format" use="@name" />

    
	<xsl:template match="/biomaj">
		<xsl:call-template name="header"/>
        
		<xsl:call-template name="main">
        	<xsl:with-param name="sortedCol">dbname</xsl:with-param>
        	<xsl:with-param name="sortedOrder" select="'ascending'" />
        </xsl:call-template>
        
		<xsl:call-template name="Make_Global_stat"/>
        <xsl:call-template name="Create_bank_listing_html" />
        <xsl:call-template name="Create_other_dbtype_db_list"/>
        
        <xsl:call-template name="Create_bankListing_by_format"/>
        
       <!-- <xsl:call-template name="Create_other_sorted_index" />-->
        
        <xsl:result-document href="errorReport.html" method="html">
                <xsl:call-template name="header"/>
                <xsl:call-template name="errorMain" />
         </xsl:result-document>
         
         <xsl:result-document href="statusReport.html" method="html">
                <xsl:call-template name="header"/>
                <xsl:call-template name="statusMain" />
         </xsl:result-document>
         
	</xsl:template>
	<xsl:template name="header">
    <xsl:param name="rootDir"/>
		<head>
			<title>BioMAJ - Web User Interface</title>
            <meta name="keywords" content="biomaj bank index list"/>
            <link rel="stylesheet" type="text/css" href="{$rootDir}css/styles.css" />
            <link rel="stylesheet" type="text/css" href="{$rootDir}css/yui/build/fonts/fonts-min.css" />
            <link rel="stylesheet" type="text/css" href="{$rootDir}css/yui/build/button/assets/skins/sam/button.css" />
            <link rel="stylesheet" type="text/css" href="{$rootDir}css/yui/build/container/assets/skins/sam/container.css" />
            <script type="text/javascript" src="{$rootDir}css/yui/build/utilities/utilities.js"></script>
            <script type="text/javascript" src="{$rootDir}css/yui/build/button/button-beta.js"></script>
            <script type="text/javascript" src="{$rootDir}css/yui/build/container/container.js"></script>

            <script language="JavaScript" type="text/JavaScript" src="{$rootDir}css/inc.js" ></script>
            
            <style>
			#example {height:30em;}
			label { display:block;float:left;width:45%;clear:left; }
			.clear { clear:both; }
			#resp { margin:10px;padding:5px;border:1px solid #ccc;background:#fff;}
			#resp li { font-family:monospace }
			</style>

		</head>
	</xsl:template>
	<xsl:template name="main" match="node()">
    <xsl:param name="sortedCol"></xsl:param>
    <xsl:param name="sortedOrder" select="'ascending'" />

		<html>
		<body onload="MM_preloadImages('images/menu/biomaj_o.png','images/menu/globalStats_o.png','images/menu/localBank_o.png', 'images/menu/home_o.png');" class="yui-skin-sam">
				<table cellspacing="0" cellpadding="0" class="outermost">
                  <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0" class="menu">
                        <tr>
                          <td align="center" valign="middle" class="tmenu">
                          <div id="message" style="font-family:Geneva, Arial, Helvetica, sans-serif; font-size:9pt;"></div></td>
                          
                        </tr>
                        <tr>
                          <td  class="mmenu">
                            <a href="index.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','images/menu/home_o.png',1)"><img src="images/menu/home_o.png" width="165" height="27" border="0" name="Image4" /></a><br />
                            <a href="bankListing.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/menu/localBank_o.png',1)"><img src="images/menu/localBank.png" width="165" height="27" border="0" name="Image1" /></a><br />
                            <a href="stats.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','images/menu/globalStats_o.png',1)"><img src="images/menu/globalStats.png" width="165" height="27" border="0" name="Image2" /></a><br />
                            <a href="http://biomaj.genouest.org" onclick="window.open(this.href); return false;" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image3','','images/menu/biomaj_o.png',1)"><img src="images/menu/biomaj.png" width="165" height="27" border="0" name="Image3"/></a><br />         
                            <img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image5"/><br />      
<img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image6"/>
               			  </td>
                        </tr>                
                        <tr>
                            <td class="bmenu1">
                            
        					<a href="bankListing.html" style="color:#FFFFFF; font-weight:bold">
								All Banks
							</a>                           	
                            <ul><xsl:apply-templates select="./type" mode="filtre_index_general"/></ul>
                            
							<a href="#" onclick="YAHOO.example.container.dialog1.show();" style="color:#FFFFFF; font-weight:bold">&lt;&lt; Bank Tree &gt;&gt;</a> 
                            
                            <hr /><b style="color:#FFFFFF; font-weight:bold"> DATABASE FORMAT</b><BR />
                             <ul>
                             <xsl:for-each select="//format[generate-id(.)=
                               generate-id(key('typeatts',@name)[1])]">
                             	<li>
                                	<xsl:variable name="dbFormat" select="@name" />
                                	<a href="bankListing_format_{$dbFormat}.html" style="color:#FFFFFF; font-weight:bold"><xsl:value-of select="@name" /></a>
                                </li>
                              </xsl:for-each>
                             </ul>
                             <hr />
   							<a href="errorReport.html" style="color:#FFFFFF; font-weight:bold">ERROR REPORT</a> <hr />
   							<a href="statusReport.html" style="color:#FFFFFF; font-weight:bold">OPEN CYCLE</a> </td>
						</tr>
                       <tr>
                          <td valign="top" class="bmenu2t">
                          
                          </td>
                        </tr>
                      </table>
                    </td>
                
                
                
                    <td align="right" valign="top">
                      <table cellspacing="0" cellpadding="0" class="content">
                        <tr>
                          <td colspan="3" class="tcontent"><span class="t_left">
                          	<img src="images/topRight.png" />
                          
                          </span>
                          <span class="t_right">
                            <img src="images/topLeft.png" />                      
                          </span>
                          
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="mcontent">
                        
                            <img src="images/banner.png" />
                        </td>
                        </tr>
                        <tr>                          <td valign="top" style="border-top: 10px solid #392C1F;" class="lcontent_i_about">
                          			
                                    <div id="dialog1">
                                    <div class="hd">Bank Tree</div>
                                    <div class="bd">
                                    <ul><xsl:apply-templates mode="bankTree" select="/biomaj/type" /></ul>
                                    </div>
                                    </div>
                          
                          			<xsl:if test="$sortedCol='dbname'">
                                        <xsl:call-template name="quickView">
                                            <xsl:with-param name="sortedBy">dbname</xsl:with-param>
                                            <xsl:with-param name="order" select="$sortedOrder" />

                                        </xsl:call-template>
                                     </xsl:if>
                                     <xsl:if test="$sortedCol='size'">
                                        <xsl:call-template name="quickView">
                                            <xsl:with-param name="sortedBy">size</xsl:with-param>
                                            <xsl:with-param name="order" select="$sortedOrder" />
                                        </xsl:call-template>
                                     </xsl:if>
                                     <xsl:if test="$sortedCol='lastUpdate'">
                                        <xsl:call-template name="quickView">
                                            <xsl:with-param name="sortedBy">lastUpdate</xsl:with-param>
                                            <xsl:with-param name="order" select="$sortedOrder" />
                                        </xsl:call-template>
                                     </xsl:if>

                          
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="footer">
                            
                           </td> 
                        </tr>
                      </table>
                    </td>            
                
                    <td width="100%" valign="top">
                    <table cellspacing="0" cellpadding="0" class="right">
                        <tr>
                          <td class="tright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="mright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td class="bright">
                          
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
		      </body>
			</html>
	</xsl:template>
    
   	<xsl:template name="errorMain" match="node()">

		<html>
		  <body onload="MM_preloadImages('images/menu/biomaj_o.png','images/menu/globalStats_o.png','images/menu/localBank_o.png', 'images/menu/home_o.png');" class="yui-skin-sam">
				<table cellspacing="0" cellpadding="0" class="outermost">
                  <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0" class="menu">
                        <tr>
                          <td align="center" valign="middle" class="tmenu">
                          <div id="message" style="font-family:Geneva, Arial, Helvetica, sans-serif; font-size:9pt;"></div></td>
                          
                        </tr>
                        <tr>
                          <td  class="mmenu">
                            <a href="index.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','images/menu/home_o.png',1)"><img src="images/menu/home_o.png" width="165" height="27" border="0" name="Image4" /></a><br />
                            <a href="bankListing.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/menu/localBank_o.png',1)"><img src="images/menu/localBank.png" width="165" height="27" border="0" name="Image1" /></a><br />
                            <a href="stats.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','images/menu/globalStats_o.png',1)"><img src="images/menu/globalStats.png" width="165" height="27" border="0" name="Image2" /></a><br />
                            <a href="http://biomaj.genouest.org" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image3','','images/menu/biomaj_o.png',1)"><img src="images/menu/biomaj.png" width="165" height="27" border="0" name="Image3"/></a><br />         
                            <img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image5"/><br />      
<img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image6"/>
               			  </td>
                        </tr>                
                        <tr>
                            <td class="bmenu1">
                            
        					<a href="bankListing.html" style="color:#FFFFFF; font-weight:bold">
								All Banks
							</a>                           	
                            <ul><xsl:apply-templates select="./type" mode="filtre_index_general"/></ul>
                            
							<a href="#" onclick="YAHOO.example.container.dialog1.show();" style="color:#FFFFFF; font-weight:bold">&lt;&lt; Bank Tree &gt;&gt;</a> 
                            
                            <hr /><b style="color:#FFFFFF; font-weight:bold"> DATABASE FORMAT</b><BR />
                             <ul>
                             <xsl:for-each select="//format[generate-id(.)=
                               generate-id(key('typeatts',@name)[1])]">
                             	<li>
                                	<xsl:variable name="dbFormat" select="@name" />
                                	<a href="bankListing_format_{$dbFormat}.html" style="color:#FFFFFF; font-weight:bold"><xsl:value-of select="@name" /></a>
                                </li>
                              </xsl:for-each>
                             </ul>
                             <hr />
   							<a href="errorReport.html" style="color:#FFFFFF; font-weight:bold">ERROR REPORT</a> <hr />
   							<a href="statusReport.html" style="color:#FFFFFF; font-weight:bold">OPEN CYCLE</a> </td>
						</tr>
                       <tr>
                          <td valign="top" class="bmenu2t">
                          
                          </td>
                        </tr>
                      </table>
                    </td>
                
                
                
                    <td align="right" valign="top">
                      <table cellspacing="0" cellpadding="0" class="content">
                        <tr>
                          <td colspan="3" class="tcontent"><span class="t_left">
                          	<img src="images/topRight.png" />
                          
                          </span>
                          <span class="t_right">
                            <img src="images/topLeft.png" />                      
                          </span>
                          
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="mcontent">
                        
                            <img src="images/banner.png" />
                        </td>
                        </tr>
                        <tr>                          <td valign="top" style="border-top: 10px solid #392C1F;" class="lcontent_i_about">
                          			
                                    <div id="dialog1">
                                    <div class="hd">Bank Tree</div>
                                    <div class="bd">
                                    <ul><xsl:apply-templates mode="bankTree" select="/biomaj/type" /></ul>
                                    </div>
                                    </div>
                          
                          			<xsl:call-template name="errorReport" />

                          
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="footer">
                            
                           </td> 
                        </tr>
                      </table>
                    </td>            
                
                    <td width="100%" valign="top">
                    <table cellspacing="0" cellpadding="0" class="right">
                        <tr>
                          <td class="tright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="mright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td class="bright">
                          
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
		      </body>
			</html>
	</xsl:template>
    
    <xsl:template name="statusMain" match="node()">

		<html>
		<body onload="MM_preloadImages('images/menu/biomaj_o.png','images/menu/globalStats_o.png','images/menu/localBank_o.png', 'images/menu/home_o.png');" class="yui-skin-sam">
				<table cellspacing="0" cellpadding="0" class="outermost">
                  <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0" class="menu">
                        <tr>
                          <td align="center" valign="middle" class="tmenu">
                          <div id="message" style="font-family:Geneva, Arial, Helvetica, sans-serif; font-size:9pt;"></div></td>
                          
                        </tr>
                        <tr>
                          <td  class="mmenu">
                            <a href="index.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','images/menu/home_o.png',1)"><img src="images/menu/home_o.png" width="165" height="27" border="0" name="Image4" /></a><br />
                            <a href="bankListing.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/menu/localBank_o.png',1)"><img src="images/menu/localBank.png" width="165" height="27" border="0" name="Image1" /></a><br />
                            <a href="stats.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','images/menu/globalStats_o.png',1)"><img src="images/menu/globalStats.png" width="165" height="27" border="0" name="Image2" /></a><br />
                            <a href="http://biomaj.genouest.org" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image3','','images/menu/biomaj_o.png',1)"><img src="images/menu/biomaj.png" width="165" height="27" border="0" name="Image3"/></a><br />         
                            <img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image5"/><br />      
<img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image6"/>
               			  </td>
                        </tr>                
                        <tr>
                            <td class="bmenu1">
                            
        					<a href="bankListing.html" style="color:#FFFFFF; font-weight:bold">
								All Banks
							</a>                           	
                            <ul><xsl:apply-templates select="./type" mode="filtre_index_general"/></ul>
                            
							<a href="#" onclick="YAHOO.example.container.dialog1.show();" style="color:#FFFFFF; font-weight:bold">&lt;&lt; Bank Tree &gt;&gt;</a> 
                            
                            <hr /><b style="color:#FFFFFF; font-weight:bold"> DATABASE FORMAT</b><BR />
                             <ul>
                             <xsl:for-each select="//format[generate-id(.)=
                               generate-id(key('typeatts',@name)[1])]">
                             	<li>
                                	<xsl:variable name="dbFormat" select="@name" />
                                	<a href="bankListing_format_{$dbFormat}.html" style="color:#FFFFFF; font-weight:bold"><xsl:value-of select="@name" /></a>
                                </li>
                              </xsl:for-each>
                             </ul>
                             <hr />
   							<a href="errorReport.html" style="color:#FFFFFF; font-weight:bold">ERROR REPORT</a> <hr />
   							<a href="statusReport.html" style="color:#FFFFFF; font-weight:bold">OPEN CYCLE</a> </td>
						</tr>
                       <tr>
                          <td valign="top" class="bmenu2t">
                          
                          </td>
                        </tr>
                      </table>
                    </td>
                
                
                
                    <td align="right" valign="top">
                      <table cellspacing="0" cellpadding="0" class="content">
                        <tr>
                          <td colspan="3" class="tcontent"><span class="t_left">
                          	<img src="images/topRight.png" />
                          
                          </span>
                          <span class="t_right">
                            <img src="images/topLeft.png" />                      
                          </span>
                          
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="mcontent">
                        
                            <img src="images/banner.png" />
                        </td>
                        </tr>
                        <tr>                          <td valign="top" style="border-top: 10px solid #392C1F;" class="lcontent_i_about">
                          			
                                    <div id="dialog1">
                                    <div class="hd">Bank Tree</div>
                                    <div class="bd">
                                    <ul><xsl:apply-templates mode="bankTree" select="/biomaj/type" /></ul>
                                    </div>
                                    </div>
                          
                          			<xsl:call-template name="statusReport" />

                          
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="footer">
                            
                           </td> 
                        </tr>
                      </table>
                    </td>            
                
                    <td width="100%" valign="top">
                    <table cellspacing="0" cellpadding="0" class="right">
                        <tr>
                          <td class="tright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="mright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td class="bright">
                          
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
		      </body>
			</html>
	</xsl:template>
	
    <!-- ERROR MESSAGE -->
    
    <xsl:template name="errorReport">
    	<h2> Recent Error and Warning Messages</h2>
        <table class="sortable" width="100%">
        	
			<tr style="background-color:#333333; color:#FFFFFF">
            	<th>
            		Database Name
            	</th>
				<th>Time</th>
				<th>Description</th>
				<th>Details</th>
   			</tr>
            <tr>
            	<td colspan="4" align="center"><b>ERRORS</b></td>
            </tr>

            <xsl:variable name="errors" select="document('./errors/error_temp.xml')"/>
            <xsl:for-each select="$errors//errorMessage">
                <xsl:sort select="@sessionId" data-type="text" order="descending"></xsl:sort>
                <tr class="row{position() mod 2}">
                	<td>
                    	<xsl:value-of select="@dbName" />
                    </td>
                    <td>
                    	<xsl:value-of select="@time" />
                    </td>
                    <td>
                    	<xsl:value-of select="@message" />
                    </td>
                    <td>
                    	<a href="{@dbName}{@sessionId}.html">Click here!</a>
                    </td>
                </tr>
            </xsl:for-each>
        	 <tr>
            	<td colspan="4" align="center"><b>WARNINGS</b></td>
            </tr>

            <xsl:variable name="errors" select="document('./errors/error_temp.xml')"/>
            <xsl:for-each select="$errors//warningMessage">
                <xsl:sort select="@sessionId" data-type="text" order="descending"></xsl:sort>
                <tr class="row{position() mod 2}">
                	<td>
                    	<xsl:value-of select="@dbName" />
                    </td>
                    <td>
                    	<xsl:value-of select="@time" />
                    </td>
                    <td>
                    	<xsl:value-of select="@message" />
                    </td>
                    <td>
                    	<a href="{@dbName}{@sessionId}.html">Click here!</a>
                    </td>
                </tr>
            </xsl:for-each>
         </table>
    </xsl:template>
    
    <!-- Open Cycle -->
    <xsl:template name="statusReport">
    	<h2> Last Update Session Status</h2>
        <table class="sortable" width="100%">
        	
			<tr style="background-color:#333333; color:#FFFFFF">
            	<th>
            		Database Name
            	</th>
				<th>Time</th>
				<th>Log File</th>
				<th>Status</th>
                <th>Details</th>
   			</tr>

            <xsl:variable name="errors" select="document('./errors/error_temp.xml')"/>
            <xsl:for-each select="$errors//status">
                <xsl:sort select="@sessionId" data-type="text" order="descending"></xsl:sort>
                <xsl:if test="compare(@value,'false')=0">
                <tr >
                	<td>
                    	<xsl:value-of select="@dbName" />
                    </td>
                    <td>
                    	<xsl:value-of select="@time" />
                    </td>
                    <td>
                    	<xsl:value-of select="@log" />
                    </td>
                    <td>
                    	<xsl:if test="compare(@value,'false')=0">
                                <img src="images/updating.gif" alt="Open Cycle"/>
                        </xsl:if>
                        
                    </td>
                    <td>
                    	<a href="{@dbName}{@sessionId}.html">Click here!</a>
                    </td>
                </tr>
                </xsl:if>
            </xsl:for-each>
         </table>
    </xsl:template>

	<!-- QUICK VIEW -->
	<xsl:template name="quickView" match="/biomaj/banks">
    <xsl:param name="sortedBy">dbname </xsl:param>
    <xsl:param name="order" select="'ascending'" />
    
  
        <h2> Recently Updated Banks (last 30 days) - Generated the <xsl:value-of select="/biomaj/banks/@creation"/></h2>
        <table class="sortable">
			<tr style="background-color:#333333; color:#FFFFFF">
                                <th>DB name</th>
				<th>Description</th>
				<th>Size </th>
				<th>Av. Freq. Update</th>
				<th>Current Release</th>
                                <th>Last Update</th>
			</tr>
            
            <!-- Sorted by DBName -->
                <xsl:for-each select="/biomaj/banks/bank">
                    <xsl:sort select="@*[name()=$sortedBy]" data-type="text" order="{ $order }"></xsl:sort>
                    <xsl:variable  name="lastUpdate"
                          select="@lastUpdate"/>
                    <xsl:variable  name="year"
                          select="substring($lastUpdate,7,4)"/>
                    <xsl:variable  name="month"
                          select="substring($lastUpdate,4,2)"/>
                    <xsl:variable  name="day"
                          select="substring($lastUpdate,1,2)"/>
                    <xsl:variable name="updatedDate" select="concat($year,'-',$month,'-',$day) cast as sch:date" />
                     <xsl:if test="days-from-duration(current-date() - $updatedDate) &lt; 31"> 
                        <tr class="row1">
                            <td>
                        <b>		<xsl:value-of select="@dbname"/></b>
                            </td>
        
        
                            <td>
                                <xsl:value-of select="@fullname"/>
                            </td>
        
                            <td>
                                <table>
                                    <tr>
                                        <td class="subtd">
                                            <xsl:value-of select="productionDirectory/@size"/>
                                        </td>
                                        <td class="subtd">
                                            <xsl:value-of select="productionDirectory/@nbfilesInFlat"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <xsl:value-of select="nextReleaseInDay/@moy"/>
                            </td>
                            <td>
                                <a href="{@dbname}.html">
                                    <xsl:value-of select="release/@value"/>
                                </a>
                            </td>
                            <td>
                                <xsl:value-of select="@lastUpdate" /> 
                            </td>
                        </tr>
                   </xsl:if> 		
                </xsl:for-each>
            
        </table>      
    </xsl:template> 
    
       
	<!-- ALL BANK LISTINGS -->
	<xsl:template name="bankListing" match="/biomaj/banks">
    <xsl:param name="sortedBy">dbname </xsl:param>
    <xsl:param name="order" select="'ascending'" />
           
        <h2>Banks listing: ( creation: <xsl:value-of select="/biomaj/banks/@creation"/> )</h2>
		
        <table class='sortable'>
			<tr style="background-color:#333333; color:#FFFFFF">
            <th>DB name</th>
            <!--<u>
            <xsl:choose>
            	<xsl:when test="$sortedBy='dbname'">
                	<xsl:if test="$order='ascending'">
                    </xsl:if>
                    <xsl:if test="$order='descending'">
                    	<a href="bankListing.html" style="color:#FFFFFF">DB name <img src="images/des.png"  /></a>
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                	<a href="index.html" style="color:#FFFFFF">DB name</a>
                </xsl:otherwise>
            </xsl:choose>
            </u>
            </th>-->
				<th>Description</th>
				<th>Size </th>
				<th>Av. Freq. Update</th>
				<th>Current Release</th>
                <th>Last Update</th><!--<u>
                <xsl:choose>
                    <xsl:when test="$sortedBy='lastUpdate'">
                        <xsl:if test="$order='ascending'">
                            <a href="bankListing_sorted_by_lastUpdate_des.html" style="color:#FFFFFF">Last Update <img src="images/asc.png"  /></a>
                        </xsl:if>
                        <xsl:if test="$order='descending'">
                            <a href="bankListing_sorted_by_lastUpdate_asc.html" style="color:#FFFFFF">Last Update <img src="images/des.png"  /></a>
                        </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                        <a href="bankListing_sorted_by_lastUpdate_asc.html" style="color:#FFFFFF">Last Update</a>
                    </xsl:otherwise>
            	</xsl:choose>
               	</u></th>-->
			</tr>
			<xsl:for-each select="/biomaj/banks/bank">
                <xsl:sort select="@*[name()=$sortedBy]" data-type="text" order="{ $order }"></xsl:sort>
				<tr class="row{position() mod 2}">
					<td>
				<b>		<xsl:value-of select="@dbname"/></b>
					</td>


					<td>
						<xsl:value-of select="@fullname"/>
					</td>

					<td>
						<table>
							<tr>
								<td class="subtd">
									<xsl:value-of select="productionDirectory/@size"/>
								</td>
								<td class="subtd">
									<xsl:value-of select="productionDirectory/@nbfilesInFlat"/>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<xsl:value-of select="nextReleaseInDay/@moy"/>
					</td>
					<td>
						<a href="{@dbname}.html">
							<xsl:value-of select="release/@value"/>
						</a>
					</td>
                    <td>
                    	<xsl:value-of select="@lastUpdate" /> 
                    </td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
	
	<xsl:template match="type" mode="getTypePath"><xsl:if test="../name() = 'type' and ../@value!='all'"><xsl:apply-templates mode="getTypePath" select="parent::node()"/>/<xsl:value-of select="../@value"/></xsl:if></xsl:template>


	
  <!-- BANK TYPE MENU -->
  <xsl:template match="type" mode="filtre_index_general">
    <xsl:param name="rootDir"/>
		<xsl:variable name="type" select="./@value" />
    <xsl:if test="$type!='all'">
      <xsl:variable name="typePath">
	      <xsl:apply-templates mode="getTypePath" select="."/>
	    </xsl:variable>
	    <li><a href="{$rootDir}types{$typePath}/{@value}.html" style="color:#FFFFFF; font-weight:bold" onmouseover="showDiv('{@value}Sub');">
		    <xsl:value-of select="@value"/>
	    </a> </li>
    </xsl:if>
      
    <xsl:if test="$type='all'">
      <xsl:apply-templates mode="filtre_index_general" select="./type"/>
    </xsl:if>
  </xsl:template>
    
  <xsl:template match="type" mode="bankTree">
    <xsl:param name="rootDir"/>
  	<xsl:variable name="type" select="./@value" />
  	
    <xsl:if test="$type!='all'">
      <xsl:if test="@value!='all'">
        <xsl:variable name="typePath">
	        <xsl:apply-templates mode="getTypePath" select="."/>
	      </xsl:variable>
        <li><a href="{$rootDir}types{$typePath}/{@value}.html" style="font-weight:bold">
	        <xsl:value-of select="@value"></xsl:value-of>
        </a> </li>
      </xsl:if>
      <div id="{@value}Sub">
        <ul>
        	<xsl:apply-templates mode="bankTree" select="./type">
            <xsl:with-param name="rootDir"><xsl:value-of select="$rootDir"/></xsl:with-param>
          </xsl:apply-templates>
        </ul>
      </div>
    </xsl:if>
      
    <xsl:if test="$type='all'">
      <xsl:apply-templates mode="bankTree" select="./type">
        <xsl:with-param name="rootDir"><xsl:value-of select="$rootDir"/></xsl:with-param>
      </xsl:apply-templates>
    </xsl:if>
  </xsl:template>
    
    
    
	<xsl:template name="menu_stats" match="/biomaj/type">
		 <b style="color:#FFFFFF; font-weight:bold"> STATISTICS </b><BR />
        <ul style="color:#FFFFFF; font-weight:bold">
        	<li><a class="MenuLink" href="stats.html#banks" style="color:#FFFFFF">Banks sizes</a></li>
            <li><a class="MenuLink" href="stats.html#distribution" style="color:#FFFFFF">Bank Distribution</a></li>

            <xsl:for-each select="//type">
                <li><a class="MenuLink" href="stats.html#{@value}" style="color:#FFFFFF">
                    <xsl:value-of select="@value"/> Sizes</a></li>
            </xsl:for-each>
            <li><a class="MenuLink" href="stats.html#Free" style="color:#FFFFFF">Free Space</a></li>
        </ul>

		<a href="index.html" style="color:#FFFFFF">[MAIN]</a>|<a href="javascript:history.back()" style="color:#FFFFFF"> [BACK]</a>
	</xsl:template>

	<xsl:template name="mainStats">
    <html>
		<xsl:call-template name="header"/>

        <body class="yui-skin-sam" onload="MM_preloadImages('images/menu/biomaj_o.png','images/menu/globalStats_o.png','images/menu/localBank_o.png');">
            <table cellspacing="0" cellpadding="0" class="outermost">
              <tr>
                <td valign="top">
                    <table cellspacing="0" cellpadding="0" class="menu">
                        <tr>
                          <td align="center" valign="middle" class="tmenu">
                          <div id="message" style="font-family:Geneva, Arial, Helvetica, sans-serif; font-size:9pt;"></div></td>
                          
                        </tr>
                        <tr>
                          <td  class="mmenu">
                            <a href="index.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','images/menu/home_o.png',1)"><img src="images/menu/home.png" width="165" height="27" border="0" name="Image4" /></a><br />
                            <a href="bankListing.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/menu/localBank_o.png',1)"><img src="images/menu/localBank.png" width="165" height="27" border="0" name="Image1" /></a><br />
                            <a href="stats.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','images/menu/globalStats_o.png',1)"><img src="images/menu/globalStats_o.png" width="165" height="27" border="0" name="Image2" /></a><br />
                            <a href="http://biomaj.genouest.org" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image3','','images/menu/biomaj_o.png',1)"><img src="images/menu/biomaj.png" width="165" height="27" border="0" name="Image3"/></a><br />         
                            <img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image5"/><br />      
<img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image6"/>
               			  </td>
                        </tr>                
                        <tr>
                            <td class="bmenu1">
                                <xsl:call-template name="menu_stats"/>
                            </td>
                        </tr>
                           <tr>
                          <td valign="top" class="bmenu2t">
                          
                          </td>
                        </tr>
                      </table>
                    </td>               
                
                    
                    <td align="right" valign="top">
                      <table cellspacing="0" cellpadding="0" class="content">
                        <tr>
                          <td colspan="3" class="tcontent"><span class="t_left">
                          	<img src="images/topRight.png" />
                          
                          </span>
                          <span class="t_right">
                            <img src="images/topLeft.png" />                      
                          </span>
                          
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="mcontent">
                        
                            <img src="images/banner.png" />
                        </td>
                        </tr>
                        <tr>                          <td valign="top" style="border-top: 10px solid #392C1F;" class="lcontent_i_about">
                          			
                                    <div id="dialog1">
                                    <div class="hd">Bank Tree</div>
                                    <div class="bd">
                                    <ul><xsl:apply-templates mode="bankTree" select="/biomaj/type" /></ul>
                                    </div>
                                    </div>
<div style="height:650px; overflow:hidden; width:100%">
                                    	<xsl:call-template name="stats_contents"/>
                                    </div>

                          
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="footer">
                            
                           </td> 
                        </tr>
                      </table>
                    </td>            
                
                    <td width="100%" valign="top">
                    <table cellspacing="0" cellpadding="0" class="right">
                        <tr>
                          <td class="tright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="mright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td class="bright">
                          
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
		    </body>
			</html>
	</xsl:template>
	<xsl:template name="stats_contents">
		<div id="banks">
		<table>

				<th>ALL Banks Sizes</th>

				<br></br>
				<br></br>
				<tr>
					<td>
						<center>
							<img src="stats/pie_banks.png" width="100%"/>
						</center>
					</td>
				</tr>
				<tr id="distribution">
					<td>
                    	<br /><br />
						<center>
							<img src="stats/pie_banks_server.png" width="100%"/>
						</center>
					</td>
				</tr>
			</table>

		</div>

		<div id="Free">
			<table>

				<th>ALL Banks Sizes and Free Space</th>

				<br></br>
				<br></br>
				<tr>
					<td>
						<center>
							<img src="stats/pie_banks_with_freespace_and_others.png" width="100%"/>
						</center>
					</td>
				</tr>
			</table>
		</div>

		<xsl:for-each select="//type">
			<xsl:variable name="pie" select="@value"/>
			<div ID="{$pie}">
				<table>
					<th></th>

					<tr>
					</tr>
					<tr>
						<br></br>
					</tr>
					<tr>
						<td>
							<br></br>
							<br></br>
							<center>
								<img src="stats/pie_{$pie}.png" width="100%"/>
							</center>

							<br></br>
						</td>
					</tr>
				</table>
			</div>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="Make_Global_stat">

		<xsl:result-document href="stats.html" method="html">


			<xsl:call-template name="mainStats"/>
		</xsl:result-document>
	</xsl:template>
    
    <xsl:template name="bankListingBody">
    <xsl:param name="sortedCol"></xsl:param>
    <xsl:param name="sortedOrder" select="'ascending'" />

      <html>
		    <body class="yui-skin-sam" onload="MM_preloadImages('images/menu/biomaj_o.png','images/menu/globalStats_o.png','images/menu/localBank_o.png');">

                <table cellspacing="0" cellpadding="0" class="outermost">
                  <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0" class="menu">
                        <tr>
                          <td align="center" valign="middle" class="tmenu">
                          <div id="message" style="font-family:Geneva, Arial, Helvetica, sans-serif; font-size:9pt;"></div></td>
                          
                        </tr>
                        <tr>
                          <td  class="mmenu">
                            <a href="index.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','images/menu/home_o.png',1)"><img src="images/menu/home.png" width="165" height="27" border="0" name="Image4" /></a><br />
                            <a href="bankListing.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/menu/localBank_o.png',1)"><img src="images/menu/localBank_o.png" width="165" height="27" border="0" name="Image1" /></a><br />
                            <a href="stats.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','images/menu/globalStats_o.png',1)"><img src="images/menu/globalStats.png" width="165" height="27" border="0" name="Image2" /></a><br />
                            <a href="http://biomaj.genouest.org" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image3','','images/menu/biomaj_o.png',1)"><img src="images/menu/biomaj.png" width="165" height="27" border="0" name="Image3"/></a><br />         
                            <img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image5"/><br />      
<img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image6"/>
               			  </td>
                        </tr>                
                        <tr>
                            <td class="bmenu1">
                            
                           
                            <a href="#" onclick="YAHOO.example.container.dialog1.show();" style="color:#FFFFFF; font-weight:bold">&lt;&lt; Bank Tree &gt;&gt;</a> 
                             
                             <hr /><b style="color:#FFFFFF; font-weight:bold"> DATABASE FORMAT</b><BR />
                            	
                             <ul>
                             <xsl:for-each select="//format[generate-id(.)=
                               generate-id(key('typeatts',@name)[1])]">
                             	<li>
                                	<xsl:variable name="dbFormat" select="@name" />
                                	<a href="bankListing_format_{$dbFormat}.html" style="color:#FFFFFF; font-weight:bold"><xsl:value-of select="@name" /></a>
                                </li>
                              </xsl:for-each>
                             </ul>
                             <hr />
   							<a href="errorReport.html" style="color:#FFFFFF; font-weight:bold">ERROR REPORT</a> <hr />
   							<a href="statusReport.html" style="color:#FFFFFF; font-weight:bold">OPEN CYCLE</a> <hr />
                            <ul>
                            <li>
                                <a class="MenuLink" href="index.html" style="color:#FFFFFF; font-weight:bold">
                                    Back to Main
                                </a>
                            </li>
                            
                            <li> <a class="MenuLink" href="stats.html" style="color:#FFFFFF; font-weight:bold">
                               Stats Sizes</a></li>
                             </ul>

                            	
                             </td>
                        </tr>
                       <tr>
                          <td valign="top" class="bmenu2t">
                          
                          </td>
                        </tr>
                      </table>
                    </td>
                
                
                
                    <td align="right" valign="top">
                      <table cellspacing="0" cellpadding="0" class="content">
                        <tr>
                          <td colspan="3" class="tcontent"><span class="t_left">
                            <img src="images/topRight.png" />
                          
                          </span>
                          <span class="t_right">
                            <img src="images/topLeft.png" />                      
                          </span>
                          
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="mcontent">
                        
                            <img src="images/banner.png" />
                        </td>
                        </tr>
                        <tr>                          <td valign="top" style="border-top: 10px solid #392C1F;" class="lcontent_i_about">
                          			
                                    <div id="dialog1">
                                    <div class="hd">Bank Tree</div>
                                    <div class="bd">
                                    <ul><xsl:apply-templates mode="bankTree" select="/biomaj/type" /></ul>
                                    </div>
                                    </div>
<xsl:if test="$sortedCol='dbname'">
                                        <xsl:call-template name="bankListing">
                                            <xsl:with-param name="sortedBy">dbname</xsl:with-param>
                                            <xsl:with-param name="order" select="$sortedOrder" />

                                        </xsl:call-template>
                                     </xsl:if>
                                     <xsl:if test="$sortedCol='size'">
                                        <xsl:call-template name="bankListing">
                                            <xsl:with-param name="sortedBy">size</xsl:with-param>
                                            <xsl:with-param name="order" select="$sortedOrder" />
                                        </xsl:call-template>
                                     </xsl:if>
                                     <xsl:if test="$sortedCol='lastUpdate'">
                                        <xsl:call-template name="bankListing">
                                            <xsl:with-param name="sortedBy">lastUpdate</xsl:with-param>
                                            <xsl:with-param name="order" select="$sortedOrder" />
                                        </xsl:call-template>
                                     </xsl:if>

                          
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="footer">
                            
                           </td> 
                        </tr>
                      </table>
                    </td>            
                
                    <td width="100%" valign="top">
                    <table cellspacing="0" cellpadding="0" class="right">
                        <tr>
                          <td class="tright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="mright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td class="bright">
                          
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
            </body>
          </html>
    
    </xsl:template>
    
    
    <xsl:template name="Create_bank_listing_html">
    	 <xsl:result-document href="bankListing.html" method="html">
			<xsl:call-template name="header"/>
            <xsl:call-template name="bankListingBody">
            	<xsl:with-param name="sortedCol">dbname</xsl:with-param>
        		<xsl:with-param name="sortedOrder" select="'ascending'"></xsl:with-param>
            </xsl:call-template>
		</xsl:result-document>
<!--
            <xsl:call-template name="header"/>
            <xsl:call-template name="bankListingBody">
                <xsl:with-param name="sortedCol">lastUpdate</xsl:with-param>
        		<xsl:with-param name="sortedOrder" select="'ascending'"></xsl:with-param>
            </xsl:call-template>
        </xsl:result-document>
        <xsl:result-document href="bankListing_sorted_by_lastUpdate_des.html" method="html">
            <xsl:call-template name="header"/>
            <xsl:call-template name="bankListingBody">
                <xsl:with-param name="sortedCol">lastUpdate</xsl:with-param>
        		<xsl:with-param name="sortedOrder" select="'descending'"></xsl:with-param>
            </xsl:call-template>
        </xsl:result-document>
        <xsl:result-document href="bankListing_sorted_by_dbname_des.html" method="html">
            <xsl:call-template name="header"/>
            <xsl:call-template name="bankListingBody">
                <xsl:with-param name="sortedCol">dbname</xsl:with-param>
        		<xsl:with-param name="sortedOrder" select="'descending'"></xsl:with-param>
            </xsl:call-template>
        </xsl:result-document>
--> 
    </xsl:template>
<!--    
    <xsl:template name="Create_other_sorted_index">
        <xsl:result-document href="index_sorted_by_lastUpdate_asc.html" method="html">
            <xsl:call-template name="header"/>
            <xsl:call-template name="main">
                <xsl:with-param name="sortedCol">lastUpdate</xsl:with-param>
        		<xsl:with-param name="sortedOrder" select="'ascending'"></xsl:with-param>
            </xsl:call-template>
        </xsl:result-document>
        <xsl:result-document href="index_sorted_by_lastUpdate_des.html" method="html">
            <xsl:call-template name="header"/>
            <xsl:call-template name="main">
                <xsl:with-param name="sortedCol">lastUpdate</xsl:with-param>
        		<xsl:with-param name="sortedOrder" select="'descending'"></xsl:with-param>
            </xsl:call-template>
        </xsl:result-document>
        <xsl:result-document href="index_sorted_by_dbname_des.html" method="html">
            <xsl:call-template name="header"/>
            <xsl:call-template name="main">
                <xsl:with-param name="sortedCol">dbname</xsl:with-param>
        		<xsl:with-param name="sortedOrder" select="'descending'"></xsl:with-param>
            </xsl:call-template>
        </xsl:result-document>        
        
    </xsl:template>
-->    
    
    <xsl:template name="otherBankListing">
    <xsl:param name="rootDir"/>
    <xsl:param name="sortedBy">dbname </xsl:param>
    <xsl:param name="order" select="'ascending'" />
    <xsl:param name="type" />

    	<h1>bank Type:<xsl:value-of select="./@*"/></h1>
        
        <h2>Banks listing:</h2>

        <table class="sortable">
            <tr style="background-color:#333333; color:#FFFFFF">
            <th>DB name</th>
				<th>Description</th>
				<th>Size </th>
				<th>Av. Freq. Update</th>
				<th>Current Release</th>
                <th>Last Update</th>
            </tr>
            <xsl:for-each select="descendant::bank">
                <xsl:sort select="@*[name()=$sortedBy]" data-type="text" order="{ $order }"></xsl:sort>
                <xsl:variable name="NAME" select="@name"/>
                <xsl:variable name="CBANK" select="key('allbanks',$NAME)"/>

                <tr class="row{position() mod 2}">
                    <td>
                    <b>	<xsl:value-of select="$NAME"/> </b>
                    </td>

                    <td>
                        <xsl:value-of select="$CBANK/@fullname"/>
                    </td>

                    <td>
                    <table>
                        <tr>
            <td class="subtd">
                                <xsl:value-of select="$CBANK/productionDirectory/@size"/>
                            </td>
            <td class="subtd">
                                <xsl:value-of select="$CBANK/productionDirectory/@nbfilesInFlat"/>
                            </td>
                        </tr>
                    </table>
                    </td>
                    <td>
                        <xsl:value-of select="$CBANK/nextReleaseInDay/@moy"/>
                    </td>
                    <td>
                        <a href="{$rootDir}{$NAME}.html">
                            <xsl:value-of select="$CBANK/release/@value"/>
                        </a>
                    </td>
                    <td>
                        <xsl:value-of select="$CBANK/@lastUpdate" />
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
    
    <xsl:template name="otherBankListingBody">
    <xsl:param name="rootDir"/>
    <xsl:param name="sortedCol"></xsl:param>
    <xsl:param name="sortedOrder" select="'ascending'" />
    <xsl:param name="type" />


    <html>
      <body class="yui-skin-sam" onload="MM_preloadImages('{$rootDir}images/menu/biomaj_o.png','{$rootDir}images/menu/globalStats_o.png','{$rootDir}images/menu/localBank_o.png');">

        <table cellspacing="0" cellpadding="0" class="outermost">
          <tr>
            <td valign="top">
                <table cellspacing="0" cellpadding="0" class="menu">
                <tr>
                  <td align="center" valign="middle" class="tmenu">
                  <div id="message" style="font-family:Geneva, Arial, Helvetica, sans-serif; font-size:9pt;"></div></td>
                  
                </tr>
                <tr>
                  <td  class="mmenu">
                            <a href="{$rootDir}index.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','{$rootDir}images/menu/home_o.png',1)"><img src="{$rootDir}images/menu/home.png" width="165" height="27" border="0" name="Image4" /></a><br />
                            <a href="{$rootDir}bankListing.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','{$rootDir}images/menu/localBank_o.png',1)"><img src="{$rootDir}images/menu/localBank_o.png" width="165" height="27" border="0" name="Image1" /></a><br />
                            <a href="{$rootDir}stats.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','{$rootDir}images/menu/globalStats_o.png',1)"><img src="{$rootDir}images/menu/globalStats.png" width="165" height="27" border="0" name="Image2" /></a><br />
                            <a href="http://biomaj.genouest.org" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image3','','{$rootDir}images/menu/biomaj_o.png',1)"><img src="{$rootDir}images/menu/biomaj.png" width="165" height="27" border="0" name="Image3"/></a><br />         
                            <img src="{$rootDir}images/menu/bar.gif" width="165" height="27" border="0" name="Image5"/><br />      
<img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image6"/>
               			  </td>
                </tr>                
                <tr>
                    <td class="bmenu1" style="overflow:auto">
                    	
                            <b style="color:#FFFFFF; font-weight:bold"><xsl:value-of select="@value" /> </b><BR />
                            <ul>
                              <xsl:apply-templates select="./type" mode="filtre_index_general">
                                <xsl:with-param name="rootDir"><xsl:value-of select="$rootDir"/></xsl:with-param>
                              </xsl:apply-templates>
                            </ul>
                            
                            <a href="#" onclick="YAHOO.example.container.dialog1.show();" style="color:#FFFFFF; font-weight:bold">&lt;&lt; Bank Tree &gt;&gt;</a> 
<hr />
                    
                    <b style="color:#FFFFFF; font-weight:bold"> DATABASE FORMAT</b><BR />
                            	
                             <ul>
                             <xsl:for-each select="//format[generate-id(.)=
                               generate-id(key('typeatts',@name)[1])]">
                             	<li>
                                	<xsl:variable name="dbFormat" select="@name" />
                                	<a href="{$rootDir}bankListing_format_{$dbFormat}.html" style="color:#FFFFFF; font-weight:bold"><xsl:value-of select="@name" /></a>
                                </li>
                              </xsl:for-each>
                             </ul>
                        <hr />
   							<a href="{$rootDir}errorReport.html" style="color:#FFFFFF; font-weight:bold">ERROR REPORT</a> <hr />
   							<a href="{$rootDir}statusReport.html" style="color:#FFFFFF; font-weight:bold">OPEN CYCLE</a> <hr />
                        <ul>
                            <li>
                                <a class="MenuLink" href="{$rootDir}index.html" style="color:#FFFFFF; font-weight:bold">
                                    Back to Main
                                </a>
                            </li>
                            <li>
                            <a class="MenuLink" href="{$rootDir}stats.html#{@value}" style="color:#FFFFFF; font-weight:bold">
                                <xsl:value-of select="@value"/>Stats Sizes</a>
                             </li>
                        </ul>                                        
                    </td>
                </tr>
               <tr>
                  <td valign="top" class="bmenu2t">
                  
                  </td>
                </tr>
              </table>
            </td>
        
        
        
            <td align="right" valign="top">
              <table cellspacing="0" cellpadding="0" class="content">
                <tr>
                  <td colspan="3" class="tcontent"><span class="t_left">
                <img src="{$rootDir}images/topRight.png" />
              
              </span>
              <span class="t_right">
                <img src="{$rootDir}images/topLeft.png" />                      
              </span>
                  
                  </td>
                </tr>
                <tr>
                  <td colspan="3" class="mcontent">
                
                    <img src="{$rootDir}images/banner.png" />
                </td>
                </tr>
                <tr>                          <td valign="top" style="border-top: 10px solid #392C1F;" class="lcontent_i_about">
                          			
                                    <div id="dialog1">
                                    <div class="hd">Bank Tree</div>
                                    <div class="bd">
                                    <ul>
                                      <xsl:apply-templates mode="bankTree" select="/biomaj/type">
                                        <xsl:with-param name="rootDir"><xsl:value-of select="$rootDir"/></xsl:with-param>
                                      </xsl:apply-templates>
                                    </ul>
                                    </div>
                                    </div>
<xsl:if test="$sortedCol='name'">
                        <xsl:call-template name="otherBankListing">
                            <xsl:with-param name="rootDir"><xsl:value-of select="$rootDir"/></xsl:with-param>
                            <xsl:with-param name="sortedBy">name</xsl:with-param>
                            <xsl:with-param name="order" select="$sortedOrder" />
                            <xsl:with-param name="type" select="$type" />

                        </xsl:call-template>
                     </xsl:if>
                     <xsl:if test="$sortedCol='size'">
                        <xsl:call-template name="otherBankListing">
                            <xsl:with-param name="rootDir"><xsl:value-of select="$rootDir"/></xsl:with-param>
                            <xsl:with-param name="sortedBy">size</xsl:with-param>
                            <xsl:with-param name="order" select="$sortedOrder" />
                            <xsl:with-param name="type" select="$type" />
                        </xsl:call-template>
                     </xsl:if>
                     <xsl:if test="$sortedCol='lastUpdate'">
                        <xsl:call-template name="otherBankListing">
                            <xsl:with-param name="rootDir"><xsl:value-of select="$rootDir"/></xsl:with-param>
                            <xsl:with-param name="sortedBy">lastUpdate</xsl:with-param>
                            <xsl:with-param name="order" select="$sortedOrder" />
                            <xsl:with-param name="type" select="$type" />
                        </xsl:call-template>
                     </xsl:if>
                  </td>
                </tr>
                <tr>
                  <td colspan="3" class="footer">
                    
                   </td> 
                </tr>
              </table>
            </td>            
        
            <td width="100%" valign="top">
            <table cellspacing="0" cellpadding="0" class="right">
                <tr>
                  <td class="tright">
                  
                  </td>
                </tr>
                <tr>
                  <td valign="top" class="mright">
                  
                  </td>
                </tr>
                <tr>
                  <td class="bright">
                  
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </body>
    </html>
    </xsl:template>
    
    <xsl:template name="Create_other_dbtype_db_list">
      <xsl:for-each select="/biomaj/type[@value='all']">
        <xsl:call-template name="createTypeTree">
            <xsl:with-param name="path"></xsl:with-param>
            <xsl:with-param name="rootDir"></xsl:with-param>
        </xsl:call-template>
		  </xsl:for-each>
    </xsl:template>
    
    <xsl:template name="createTypeTree">
      <xsl:param name="path"/>
      <xsl:param name="rootDir"/>
      
      <xsl:for-each select="./type">
			  <xsl:variable name="type">
				  <xsl:value-of select="@value"/>
			  </xsl:variable>
			  <xsl:variable name="pathAppended"><!-- branch: construct name --><xsl:value-of select="$path"/><xsl:if test="count(type) &gt;= 1 and count(bank) = 0"><xsl:value-of select="$type"/>/</xsl:if></xsl:variable>
			  <xsl:variable name="rootDirAppended">../<xsl:value-of select="$rootDir"/></xsl:variable>
			  
        <xsl:if test="count(type) &gt;= 1 and count(bank) = 0"><!-- branch: add branch name to path and pass to children -->
          <xsl:call-template name="createTypeTree">
              <xsl:with-param name="path">
                <xsl:value-of select="$pathAppended"/>
              </xsl:with-param>
              <xsl:with-param name="rootDir">
                <xsl:value-of select="$rootDirAppended"/>
              </xsl:with-param>
          </xsl:call-template>
          
          <!-- branch: create document -->
          <xsl:result-document href="./types/{$path}{$type}.html" method="html">
            <xsl:call-template name="header">
              <xsl:with-param name="rootDir"><xsl:value-of select="$rootDirAppended"></xsl:value-of></xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="otherBankListingBody">
              <xsl:with-param name="rootDir"><xsl:value-of select="$rootDirAppended"></xsl:value-of></xsl:with-param>
              <xsl:with-param name="sortedCol">name</xsl:with-param>
              <xsl:with-param name="sortedOrder" select="'ascending'"></xsl:with-param>
              <xsl:with-param name="type" select="$type" />
            </xsl:call-template>
          </xsl:result-document>
        </xsl:if>
        
        <xsl:if test="count(type) = 0 and count(bank) &gt;= 1"> <!-- leaf: create document -->
          <xsl:result-document href="./types/{$pathAppended}{$type}.html" method="html">
            <xsl:call-template name="header">
              <xsl:with-param name="rootDir"><xsl:value-of select="$rootDirAppended"/></xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="otherBankListingBody">
              <xsl:with-param name="rootDir"><xsl:value-of select="$rootDirAppended"/></xsl:with-param>
              <xsl:with-param name="sortedCol">name</xsl:with-param>
              <xsl:with-param name="sortedOrder" select="'ascending'"></xsl:with-param>
              <xsl:with-param name="type" select="$type" />
            </xsl:call-template>
          </xsl:result-document>
        </xsl:if>
		  </xsl:for-each>
    </xsl:template>
    
    
  <!--
                      <xsl:result-document href="{$type}_sorted_by_lastUpdate_asc.html" method="html">
                          <xsl:call-template name="header"/>
                          <xsl:call-template name="otherBankListingBody">
                              <xsl:with-param name="sortedCol">lastUpdate</xsl:with-param>
                              <xsl:with-param name="sortedOrder" select="'ascending'"></xsl:with-param>
                              <xsl:with-param name="type" select="$type" />
                          </xsl:call-template>
                      </xsl:result-document>
                      <xsl:result-document href="{$type}_sorted_by_lastUpdate_des.html" method="html">
                          <xsl:call-template name="header"/>
                          <xsl:call-template name="otherBankListingBody">
                              <xsl:with-param name="sortedCol">lastUpdate</xsl:with-param>
                              <xsl:with-param name="sortedOrder" select="'descending'"></xsl:with-param>
                              <xsl:with-param name="type" select="$type" />
                          </xsl:call-template>
                      </xsl:result-document>
                      <xsl:result-document href="{$type}_sorted_by_dbname_des.html" method="html">
                          <xsl:call-template name="header"/>
                          <xsl:call-template name="otherBankListingBody">
                              <xsl:with-param name="sortedCol">name</xsl:with-param>
                              <xsl:with-param name="sortedOrder" select="'descending'"></xsl:with-param>
                              <xsl:with-param name="type" select="$type" />
                          </xsl:call-template>
                      </xsl:result-document>
  -->
    
    
    <xsl:template name="bankListingByFormatBody">
    <xsl:param name="sortedCol"></xsl:param>
    <xsl:param name="sortedOrder" select="'ascending'" />
    <xsl:param name="format" />


    <html>
		  <body class="yui-skin-sam" onload="MM_preloadImages('images/menu/biomaj_o.png','images/menu/globalStats_o.png','images/menu/localBank_o.png','images/menu/propFile_o.png','images/menu/procFile_o.png');">

                <table cellspacing="0" cellpadding="0" class="outermost">
                  <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0" class="menu">
                        <tr>
                          <td align="center" valign="middle" class="tmenu">
                          <div id="message" style="font-family:Geneva, Arial, Helvetica, sans-serif; font-size:9pt;"></div></td>
                          
                        </tr>
                        <tr>
                          <td  class="mmenu">
                            <a href="index.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','images/menu/home_o.png',1)"><img src="images/menu/home.png" width="165" height="27" border="0" name="Image4" /></a><br />
                            <a href="bankListing.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/menu/localBank_o.png',1)"><img src="images/menu/localBank_o.png" width="165" height="27" border="0" name="Image1" /></a><br />
                            <a href="stats.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','images/menu/globalStats_o.png',1)"><img src="images/menu/globalStats.png" width="165" height="27" border="0" name="Image2" /></a><br />
                            <a href="http://biomaj.genouest.org" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image3','','images/menu/biomaj_o.png',1)"><img src="images/menu/biomaj.png" width="165" height="27" border="0" name="Image3"/></a><br />         
                            <img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image5"/><br />      
<img src="images/menu/bar.gif" width="165" height="27" border="0" name="Image6"/>
               			  </td>
                        </tr>                
                        <tr>
                            <td class="bmenu1">
                                 
                            
                            <a href="#" onclick="YAHOO.example.container.dialog1.show();" style="color:#FFFFFF; font-weight:bold">&lt;&lt; Bank Tree &gt;&gt;</a> 
<hr />       
                                    
                                <b style="color:#FFFFFF; font-weight:bold"> DATABASE FORMAT</b><BR />
                            	
                             <ul>
                             <xsl:for-each select="//format[generate-id(.)=
                               generate-id(key('typeatts',@name)[1])]">
                             	<li>
                                	<xsl:variable name="dbFormat" select="@name" />
                                	<a href="bankListing_format_{$dbFormat}.html" style="color:#FFFFFF; font-weight:bold"><xsl:value-of select="@name" /></a>
                                </li>
                              </xsl:for-each>
                             </ul>
                             <hr />
   							<a href="errorReport.html" style="color:#FFFFFF; font-weight:bold">ERROR REPORT</a> <hr />
   							<a href="statusReport.html" style="color:#FFFFFF; font-weight:bold">OPEN CYCLE</a> <hr />        
                                <ul>
                                    <li>
                                        <a class="MenuLink" href="index.html" style="color:#FFFFFF; font-weight:bold">
                                            Back to Main
                                        </a>
                                    </li>
                                    <li>
                                    <a class="MenuLink" href="stats.html" style="color:#FFFFFF; font-weight:bold">
                                        Stats Sizes</a>
                                     </li>
                                </ul>                                        

                                
                             </td>
                        </tr>
                       <tr>
                          <td valign="top" class="bmenu2t">
                          
                          </td>
                        </tr>
                      </table>
                    </td>
                
                
                
                    <td align="right" valign="top">
                      <table cellspacing="0" cellpadding="0" class="content">
                        <tr>
                          <td colspan="3" class="tcontent"><span class="t_left">
                            <img src="images/topRight.png" />
                          
                          </span>
                          <span class="t_right">
                            <img src="images/topLeft.png" />                      
                          </span>
                          
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="mcontent">
                        
                            <img src="images/banner.png" />
                        </td>
                        </tr>
                        <tr>                          <td valign="top" style="border-top: 10px solid #392C1F;" class="lcontent_i_about">
                          			
                                    <div id="dialog1">
                                    <div class="hd">Bank Tree</div>
                                    <div class="bd">
                                    <ul><xsl:apply-templates mode="bankTree" select="/biomaj/type" /></ul>
                                    </div>
                                    </div>
<xsl:if test="$sortedCol='dbname'">
                                        <xsl:call-template name="bankListingByFormat">
                                            <xsl:with-param name="sortedBy">dbname</xsl:with-param>
                                            <xsl:with-param name="order" select="$sortedOrder" />
											<xsl:with-param name="format" select="$format" />
                                        </xsl:call-template>
                                     </xsl:if>
                                     <xsl:if test="$sortedCol='size'">
                                        <xsl:call-template name="bankListingByFormat">
                                            <xsl:with-param name="sortedBy">size</xsl:with-param>
                                            <xsl:with-param name="order" select="$sortedOrder" />
                                            <xsl:with-param name="format" select="$format" />
                                        </xsl:call-template>
                                     </xsl:if>
                                     <xsl:if test="$sortedCol='lastUpdate'">
                                        <xsl:call-template name="bankListingByFormat">
                                            <xsl:with-param name="sortedBy">lastUpdate</xsl:with-param>
                                            <xsl:with-param name="order" select="$sortedOrder" />
                                            <xsl:with-param name="format" select="$format" />
                                        </xsl:call-template>
                                     </xsl:if>

                          
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="footer">
                            
                           </td> 
                        </tr>
                      </table>
                    </td>            
                
                    <td width="100%" valign="top">
                    <table cellspacing="0" cellpadding="0" class="right">
                        <tr>
                          <td class="tright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="mright">
                          
                          </td>
                        </tr>
                        <tr>
                          <td class="bright">
                          
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
            </body>
          </html>
    
    </xsl:template>
    
   	<xsl:template name="bankListingByFormat" match="/biomaj/banks">
    <xsl:param name="sortedBy">dbname </xsl:param>
    <xsl:param name="order" select="'ascending'" />
    <xsl:param name="format" />
           
        <h2>Banks listing - Format: <xsl:value-of select="$format" /></h2>
		
        <table class="sortable">
			<tr style="background-color:#333333; color:#FFFFFF">
                                <th>DB name</th>
				<th>Description</th>
				<th>Size </th>
				<th>Av. Freq. Update</th>
				<th>Current Release</th>
                                <th>Last Update</th>
			</tr>
			<xsl:for-each select="/biomaj/banks/bank">
                <xsl:sort select="@*[name()=$sortedBy]" data-type="text" order="{ $order }"></xsl:sort>
                <xsl:for-each select="formats/format">
                	<xsl:variable name="formatXML" select="@name"/>
                	<xsl:if test="$format=$formatXML">
                        <tr class="row0">
                            <td>
                        <b>		<xsl:value-of select="../../@dbname"/></b>
                            </td>
        
        
                            <td>
                                <xsl:value-of select="../../@fullname"/>
                            </td>
        
                            <td>
                                <table>
                                    <tr>
                                        <td class="subtd">
                                            <xsl:value-of select="../../productionDirectory/@size"/>
                                        </td>
                                        <td class="subtd">
                                            <xsl:value-of select="../../productionDirectory/@nbfilesInFlat"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <xsl:value-of select="../../nextReleaseInDay/@moy"/>
                            </td>
                            <td>
                                <a href="{../../@dbname}.html">
                                    <xsl:value-of select="../../release/@value"/>
                                </a>
                            </td>
                            <td>
                                <xsl:value-of select="../../@lastUpdate" /> 
                            </td>
                        </tr>
                      </xsl:if>
                  </xsl:for-each>
			</xsl:for-each>
		</table>
	</xsl:template>
	
    <xsl:template name="Create_bankListing_by_format">
       <xsl:for-each select="//format[generate-id(.)=
                               generate-id(key('typeatts',@name)[1])]">
            <xsl:variable name="dbFormat" select="@name" />
            
            <xsl:result-document href="bankListing_format_{$dbFormat}.html" method="html">
                <xsl:call-template name="header"/>
                <xsl:call-template name="bankListingByFormatBody">
                    <xsl:with-param name="sortedCol">dbname</xsl:with-param>
                    <xsl:with-param name="sortedOrder" select="'ascending'"></xsl:with-param>
                    <xsl:with-param name="format"><xsl:value-of select="@name" /></xsl:with-param>
                </xsl:call-template>
            </xsl:result-document>
<!--
            <xsl:result-document href="bankListing_format_{$dbFormat}_sorted_by_dbname_des.html" method="html">
                <xsl:call-template name="header"/>
                <xsl:call-template name="bankListingByFormatBody">
                    <xsl:with-param name="sortedCol">dbname</xsl:with-param>
                    <xsl:with-param name="sortedOrder" select="'descending'"></xsl:with-param>
                    <xsl:with-param name="format"><xsl:value-of select="@name" /></xsl:with-param>
                </xsl:call-template>
            </xsl:result-document>
            <xsl:result-document href="bankListing_format_{$dbFormat}_sorted_by_lastUpdate_des.html" method="html">
                <xsl:call-template name="header"/>
                <xsl:call-template name="bankListingByFormatBody">
                    <xsl:with-param name="sortedCol">lastUpdate</xsl:with-param>
                    <xsl:with-param name="sortedOrder" select="'descending'"></xsl:with-param>
                    <xsl:with-param name="format"><xsl:value-of select="@name" /></xsl:with-param>
                </xsl:call-template>
            </xsl:result-document>
            <xsl:result-document href="bankListing_format_{$dbFormat}_sorted_by_lastUpdate_asc.html" method="html">
                <xsl:call-template name="header"/>
                <xsl:call-template name="bankListingByFormatBody">
                    <xsl:with-param name="sortedCol">lastUpdate</xsl:with-param>
                    <xsl:with-param name="sortedOrder" select="'ascending'"></xsl:with-param>
                    <xsl:with-param name="format"><xsl:value-of select="@name" /></xsl:with-param>
                </xsl:call-template>
            </xsl:result-document>        
-->
			
      </xsl:for-each>
        
    </xsl:template>

    
    
    
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="yes" url="test\Index\index.xml" htmlbaseurl="" outputurl="index.html" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no" ><SourceSchema srcSchemaPath="..\statefile\statefiles230306\index.xml" srcSchemaRoot="biomaj" AssociatedInstance="" loaderFunction="document" loaderFunctionUsesURI="no"/></MapperInfo><MapperBlockPosition><template match="/"></template><template match="//types"><block path="xsl:for&#x2D;each/br/a/xsl:value&#x2D;of" x="242" y="147"/></template><template match="//biomaj"></template><template match="//banks"><block path="table/xsl:for&#x2D;each/tr/td/xsl:value&#x2D;of" x="242" y="189"/><block path="table/xsl:for&#x2D;each/tr/td[1]/xsl:value&#x2D;of" x="282" y="189"/><block path="table/xsl:for&#x2D;each/tr/td[2]/a/xsl:value&#x2D;of" x="202" y="189"/></template><template match="node()"><block path="body/html/div[1]/xsl:call&#x2D;template" x="242" y="189"/><block path="body/html/xsl:call&#x2D;template" x="242" y="144"/></template><template name="header"></template></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->
