<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="2.0">
	<xsl:output method="text" encoding="UTF-8"/>
	
	<xsl:template match="/bank">

		<!-- TABLE BANK -->
		<xsl:text>INSERT INTO bank(name) VALUES('</xsl:text>
		<xsl:value-of select="configuration[last()]/remoteInfos/serverInfo/dbName" />
		<xsl:text>');&#10;</xsl:text>

		<xsl:for-each select="configuration">

			<!-- TABLE REMOTEINFO -->
			<xsl:choose>
				<xsl:when test="remoteInfos/serverInfo/protocol/@port">
					<xsl:text>INSERT INTO remoteInfo(protocol,port,dbName,dbFullname,dbType,server,remoteDir) VALUES('</xsl:text>
					<xsl:value-of select="remoteInfos/serverInfo/protocol" />
					<xsl:text>',</xsl:text>
					<xsl:value-of select="remoteInfos/serverInfo/protocol/@port"/>
					<xsl:text>,'</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>INSERT INTO remoteInfo(protocol,dbName,dbFullname,dbType,server,remoteDir) VALUES('</xsl:text>
					<xsl:value-of select="remoteInfos/serverInfo/protocol" />
					<xsl:text>','</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:value-of select="remoteInfos/serverInfo/dbName" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="remoteInfos/serverInfo/dbFullname" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="remoteInfos/serverInfo/dbType" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="remoteInfos/serverInfo/server" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="remoteInfos/remoteDir" />
			<xsl:text>');&#10;</xsl:text>

			<!-- TABLE LOCALINFO -->
			<xsl:text>INSERT INTO localInfo(offlineDirectory,versionDirectory,frequency,dolinkcopy,logfile,releaseFile,releaseRegexp,remoteFiles,remoteExcludedFiles,localFiles,nversions) VALUES('</xsl:text>
			<xsl:value-of select="localInfos/offlineDirectory" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="localInfos/versionDirectory" />
			<xsl:text>',</xsl:text>
			<xsl:value-of select="localInfos/frequency" />
			<xsl:text>,</xsl:text>
			<xsl:choose>
				<xsl:when test="localInfos/dolinkcopy/@value">
					<xsl:value-of select="localInfos/dolinkcopy/@value" />
					<xsl:text>,</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>false,</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="localInfos/logfile/@value">
					<xsl:value-of select="localInfos/logfile/@value" />
					<xsl:text>,'</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>false,'</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:value-of select="localInfos/releaseInfo/@file" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="localInfos/releaseInfo/@regexp" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="localInfos/remoteFiles/@regexp" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="localInfos/remoteExcludedFiles/@regexp" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="localInfos/localFiles/@regexp" />
			<xsl:text>',</xsl:text>
			<xsl:value-of select="localInfos/nversions/@nb" />
			<xsl:text>);&#10;</xsl:text>

			<!-- TABLE CONFIGURATION -->
			<xsl:text>$_LAST_CONFIG:=GENERATE&#10;</xsl:text>
			
			<xsl:text>INSERT INTO configuration(idconfiguration,date,file,ref_idremoteInfo,ref_idlocalInfo,ref_idbank) VALUES(</xsl:text>
			<xsl:text>$_LAST_CONFIG</xsl:text>
			<xsl:text>,'</xsl:text>
			<xsl:value-of select="@date" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@file" />
			<xsl:text>',(SELECT max(idremoteInfo) FROM remoteInfo),(SELECT max(idlocalInfo) FROM localInfo),(SELECT max(idbank) FROM bank));&#10;</xsl:text>

			<!-- TABLE UPDATEBANK -->
			<xsl:for-each select="updateBank">
			
				<xsl:choose>
					<xsl:when test="@end">
						<xsl:text>INSERT INTO updateBank(ref_idconfiguration,updateRelease,productionDirectoryPath,productionDirectoryDeployed,sizeDownload,sizeRelease,startTime,endTime,elapsedTime,isUpdated,nbSessions,idLastSession) VALUES(</xsl:text>
						<xsl:text>$_LAST_CONFIG,'</xsl:text>
						<xsl:value-of select="workflowInfo/release/@value" />
						<xsl:text>','</xsl:text>
						<xsl:value-of select="workflowInfo/productionDirectory/@path" />
						<xsl:text>',</xsl:text>
						<xsl:choose>
							<xsl:when test="workflowInfo/productionDirectory/@deployed">
								<xsl:value-of select="workflowInfo/productionDirectory/@deployed" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>null</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,'</xsl:text>
						<xsl:value-of select="workflowInfo/sizeDownload/@size" />
						<xsl:text>','</xsl:text>
						<xsl:value-of select="workflowInfo/sizeRelease/@size" />
						<xsl:text>','</xsl:text>
						<xsl:value-of select="@start" />
						<xsl:text>','</xsl:text>
						<xsl:value-of select="@end" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>INSERT INTO updateBank(ref_idconfiguration,updateRelease,productionDirectoryPath,productionDirectoryDeployed,sizeDownload,sizeRelease,startTime,elapsedTime,isUpdated,nbSessions,idLastSession) VALUES(</xsl:text>
						<xsl:text>$_LAST_CONFIG,'</xsl:text>
						<xsl:value-of select="workflowInfo/release/@value" />
						<xsl:text>','</xsl:text>
						<xsl:value-of select="workflowInfo/productionDirectory/@path" />
						<xsl:text>',</xsl:text>
						<xsl:choose>
							<xsl:when test="workflowInfo/productionDirectory/@deployed">
								<xsl:value-of select="workflowInfo/productionDirectory/@deployed" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>null</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,'</xsl:text>
						<xsl:value-of select="workflowInfo/sizeDownload/@size" />
						<xsl:text>','</xsl:text>
						<xsl:value-of select="workflowInfo/sizeRelease/@size" />
						<xsl:text>','</xsl:text>
						<xsl:value-of select="@start" />
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@elapsedTime" />
				<xsl:text>',</xsl:text>
				<xsl:value-of select="@update" />
				<xsl:text>,</xsl:text>
				<xsl:value-of select="@nbSessions" />
				<xsl:text>,</xsl:text>
				<xsl:value-of select="@idLastSession" />
				<xsl:text>);&#10;</xsl:text>
				<xsl:text>$_MAX_IDUPDATE:=SELECT max(idupdateBank) FROM updateBank&#10;</xsl:text>

				<!-- TABLE SESSION -->
				<xsl:for-each select="session">
					<xsl:text>$_LAST_SESSION:=GENERATE:=</xsl:text><xsl:value-of select="@id" /><xsl:text>&#10;</xsl:text>
					<xsl:text>INSERT INTO session(idsession,ref_idupdateBank,status,startTime,endTime,elapsedTime,logfile) VALUES(</xsl:text>
					<xsl:text>$_LAST_SESSION</xsl:text>
					<xsl:text>,$_MAX_IDUPDATE,</xsl:text>
					<xsl:value-of select="@status" />
					<xsl:text>,'</xsl:text>
					<xsl:value-of select="@start" />
					<xsl:text>',</xsl:text>
					<xsl:choose>
						<xsl:when test="@end">
							<xsl:text>'</xsl:text>
							<xsl:value-of select="@end" />
							<xsl:text>'</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>null</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>,'</xsl:text>
					<xsl:value-of select="@elapsedTime" />
					<xsl:text>','</xsl:text>
					<xsl:value-of select="@logfile" />
					<xsl:text>');&#10;</xsl:text>

					<!-- TABLE SESSIONTASK -->
					<xsl:apply-templates select="preprocess" />
					<xsl:apply-templates select="release" />
					<xsl:apply-templates select="check" />
					<xsl:apply-templates select="download" />
					<xsl:apply-templates select="extract" />
					<xsl:apply-templates select="addLocalFiles" />
					<xsl:apply-templates select="makeRelease" />
					<xsl:apply-templates select="postprocess" />
					<xsl:apply-templates select="deployment" />
					<xsl:apply-templates select="removeprocess" />

					<!-- TABLE MESSAGE + SESSION_HAS_MESSAGE -->
					<xsl:for-each select="warning">
						<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
						<xsl:value-of select="@message" />
						<xsl:text>','warning');&#10;</xsl:text>
						<xsl:text>INSERT INTO session_has_message(ref_idsession,ref_idmessage) VALUES(</xsl:text>
						<xsl:text>$_LAST_SESSION,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
					</xsl:for-each>
					<xsl:for-each select="error">
						<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
						<xsl:value-of select="@message" />
						<xsl:text>','error');&#10;</xsl:text>
						<xsl:text>INSERT INTO session_has_message(ref_idsession,ref_idmessage) VALUES(</xsl:text>
						<xsl:text>$_LAST_SESSION,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
					</xsl:for-each>

				</xsl:for-each>

			</xsl:for-each>

		</xsl:for-each>

		<!-- TABLE PRODUCTIONDIRECTORY -->
		<xsl:for-each select="production/directory">

			<xsl:text>INSERT INTO productionDirectory(remove,creation,size,state,session,path,ref_idbank) VALUES(</xsl:text>
			<xsl:choose>
				<xsl:when test="@remove">
					<xsl:text>'</xsl:text>
					<xsl:value-of select="@remove" />
					<xsl:text>'</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>null</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>,'</xsl:text>
			<xsl:value-of select="@creation" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@size" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@state" />
			<xsl:text>',#</xsl:text>
			<xsl:value-of select="@session" />
			<xsl:text>,'</xsl:text>
			<xsl:value-of select="@path" />
			<xsl:text>',(SELECT max(idbank) FROM bank));&#10;</xsl:text>

		</xsl:for-each>


	</xsl:template>


	<xsl:template match="preprocess">

		<xsl:text>INSERT INTO sessionTask(startTime,endTime,elapsedTime,status,nbreMetaProcess,taskType) VALUES('</xsl:text>
		<xsl:value-of select="@start" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@end" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@elapsedTime" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@status" />
		<xsl:text>',</xsl:text>
		<xsl:value-of select="@nbreMetaProcess" />
		<xsl:text>,'preprocess');&#10;</xsl:text>
		
		<xsl:text>$_MAX_IDSESSIONTASK:=SELECT max(idsessionTask) FROM sessionTask&#10;</xsl:text>

		<!-- TABLE METAPROCESS -->
		<xsl:for-each select="metaprocess">
			<xsl:text>$_LAST_META:=$_MAX_IDSESSIONTASK.RAND()&#10;</xsl:text>
			
			<xsl:text>INSERT INTO metaprocess(idmetaprocess,name,startTime,endTime,elapsedTime,status,logfile,block,ref_idsessionTask) VALUES(</xsl:text>
			<xsl:text>'$_LAST_META','</xsl:text>
			<xsl:value-of select="@name" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@start" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@end" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@elapsedTime" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@status" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@logfile" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@block" />
			<xsl:text>',$_MAX_IDSESSIONTASK);&#10;</xsl:text>
			
			
			<!-- TABLE PROCESS -->
			<xsl:for-each select="process">
				<xsl:text>INSERT INTO process(name,keyname,exe,args,description,type,startTime,endTime,elapsedTime,biomaj_error,timestamp,value,ref_idmetaprocess) VALUES('</xsl:text>
				<xsl:value-of select="@name" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@keyname" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@exe" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@args" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@desc" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@type" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@start" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@end" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@elapsedTime" />
				<xsl:text>',</xsl:text>
				<xsl:value-of select="@biomaj_error" />
				<xsl:text>,</xsl:text>
				<xsl:value-of select="@timestamp" />
				<xsl:text>,'</xsl:text>
				<xsl:value-of select="@value" />
				<xsl:text>','$_LAST_META');&#10;</xsl:text>
				
				<xsl:text>$_MAX_IDPROCESS:=SELECT max(idprocess) FROM process&#10;</xsl:text>

				<!-- TABLE FILE -->
				<xsl:for-each select="files/file">
					<xsl:text>INSERT INTO file(location,size,time,link,is_extract,volatile,fileType,ref_idprocess) VALUES('</xsl:text>
					<xsl:value-of select="@location" />
					<xsl:text>',</xsl:text>
					<xsl:value-of select="@size" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@time" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@link" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@extract" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@volatile" />
					<xsl:text>,'file',(SELECT max(idprocess) FROM process));&#10;</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="move/file">
					<xsl:text>INSERT INTO file(location,size,time,link,is_extract,volatile,fileType,ref_idprocess) VALUES('</xsl:text>
					<xsl:value-of select="@location" />
					<xsl:text>',</xsl:text>
					<xsl:value-of select="@size" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@time" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@link" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@extract" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@volatile" />
					<xsl:text>,'move',(SELECT max(idprocess) FROM process));&#10;</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="copy/file">
					<xsl:text>INSERT INTO file(location,size,time,link,is_extract,volatile,fileType,ref_idprocess) VALUES('</xsl:text>
					<xsl:value-of select="@location" />
					<xsl:text>',</xsl:text>
					<xsl:value-of select="@size" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@time" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@link" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@extract" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@volatile" />
					<xsl:text>,'copy',(SELECT max(idprocess) FROM process));&#10;</xsl:text>
				</xsl:for-each>


				<!-- TABLE MESSAGE + PROCESS_HAS_MESSAGE -->
				<xsl:for-each select="warning">
					<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
					<xsl:value-of select="@message" />
					<xsl:text>','warning');&#10;</xsl:text>
					<xsl:text>INSERT INTO process_has_message(ref_idprocess,ref_idmessage) VALUES(</xsl:text>
					<xsl:text>$_MAX_IDPROCESS,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="error">
					<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
					<xsl:value-of select="@message" />
					<xsl:text>','error');&#10;</xsl:text>
					<xsl:text>INSERT INTO process_has_message(ref_idprocess,ref_idmessage) VALUES(</xsl:text>
					<xsl:text>$_MAX_IDPROCESS,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
				</xsl:for-each>

			</xsl:for-each>

			<!-- TABLE MESSAGE + METAPROCESS_HAS_MESSAGE -->
			<xsl:for-each select="warning">
				<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
				<xsl:value-of select="@message" />
				<xsl:text>','warning');&#10;</xsl:text>
				<xsl:text>INSERT INTO metaprocess_has_message(ref_idmetaprocess,ref_idmessage) VALUES(</xsl:text>
				<xsl:text>$_LAST_META,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
			</xsl:for-each>
			<xsl:for-each select="error">
				<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
				<xsl:value-of select="@message" />
				<xsl:text>','error');&#10;</xsl:text>
				<xsl:text>INSERT INTO metaprocess_has_message(ref_idmetaprocess,ref_idmessage) VALUES(</xsl:text>
				<xsl:text>$_LAST_META,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
			</xsl:for-each>

		</xsl:for-each>

		<!-- TABLE SESSION_HAS_SESSIONTASK -->
		<xsl:text>INSERT INTO session_has_sessionTask(ref_idsession,ref_idsessionTask) VALUES(</xsl:text>
		<xsl:text>$_LAST_SESSION,$_MAX_IDSESSIONTASK);&#10;</xsl:text>

	</xsl:template>

	<xsl:template match="release">
		<xsl:text>INSERT INTO sessionTask(startTime,endTime,elapsedTime,status,value,taskType) VALUES('</xsl:text>
		<xsl:value-of select="@start" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@end" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@elapsedTime" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@status" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@value" />
		<xsl:text>','release');&#10;</xsl:text>
		
		<xsl:text>$_MAX_IDSESSIONTASK:=SELECT max(idsessionTask) FROM sessionTask&#10;</xsl:text>
		
		<xsl:apply-templates select="files/file" />
		<xsl:apply-templates select="move/file" />
		<xsl:apply-templates select="copy/file" />
		
		<xsl:apply-templates select="warning" />
		<xsl:apply-templates select="error" />

		<!-- TABLE SESSION_HAS_SESSIONTASK -->
		<xsl:text>INSERT INTO session_has_sessionTask(ref_idsession,ref_idsessionTask) VALUES(</xsl:text>
		<xsl:text>$_LAST_SESSION,$_MAX_IDSESSIONTASK);&#10;</xsl:text>
	</xsl:template>

	<xsl:template match="check">
		<xsl:text>INSERT INTO sessionTask(startTime,endTime,elapsedTime,status,nbextract,nbLocalOnlineFiles,nbLocalOfflineFiles,nbDownloadFiles,taskType) VALUES('</xsl:text>
		<xsl:value-of select="@start" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@end" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@elapsedTime" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@status" />
		<xsl:text>',</xsl:text>
		<xsl:value-of select="@nbextract" />
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@nbLocalOnlineFiles" />
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@nbLocalOfflineFiles"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@nbDownloadFiles"/>
		<xsl:text>,'check');&#10;</xsl:text>
		
		<xsl:text>$_MAX_IDSESSIONTASK:=SELECT max(idsessionTask) FROM sessionTask&#10;</xsl:text>
		
		<xsl:apply-templates select="files/file" />
		<xsl:apply-templates select="move/file" />
		<xsl:apply-templates select="copy/file" />
		
		<xsl:apply-templates select="warning" />
		<xsl:apply-templates select="error" />

		<!-- TABLE SESSION_HAS_SESSIONTASK -->
		<xsl:text>INSERT INTO session_has_sessionTask(ref_idsession,ref_idsessionTask) VALUES(</xsl:text>
		<xsl:text>$_LAST_SESSION,$_MAX_IDSESSIONTASK);&#10;</xsl:text>
	</xsl:template>
	
	<xsl:template match="download">
		<xsl:text>INSERT INTO sessionTask(startTime,endTime,elapsedTime,status,bandwidth,taskType) VALUES('</xsl:text>
		<xsl:value-of select="@start"/>
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@end"/>
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@elapsedTime"/>
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@status"/>
		<xsl:text>',</xsl:text>
		<xsl:value-of select="@bandWidth"/>
		<xsl:text>,'download');&#10;</xsl:text>
		
		<xsl:text>$_MAX_IDSESSIONTASK:=SELECT max(idsessionTask) FROM sessionTask&#10;</xsl:text>
		
		<xsl:apply-templates select="files/file" />
		<xsl:apply-templates select="move/file" />
		<xsl:apply-templates select="copy/file" />
		
		<xsl:apply-templates select="warning" />
		<xsl:apply-templates select="error" />
		
		<!-- TABLE SESSION_HAS_SESSIONTASK -->
		<xsl:text>INSERT INTO session_has_sessionTask(ref_idsession,ref_idsessionTask) VALUES(</xsl:text>
		<xsl:text>$_LAST_SESSION,$_MAX_IDSESSIONTASK);&#10;</xsl:text>
	</xsl:template>
	
	<xsl:template match="extract">
		<xsl:text>INSERT INTO sessionTask(startTime,endTime,elapsedTime,status,taskType) VALUES('</xsl:text>
		<xsl:value-of select="@start"/>
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@end"/>
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@elapsedTime"/>
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@status"/>
		<xsl:text>','extract');&#10;</xsl:text>
		
		<xsl:text>$_MAX_IDSESSIONTASK:=SELECT max(idsessionTask) FROM sessionTask&#10;</xsl:text>
		
		<xsl:apply-templates select="files/file" />
		<xsl:apply-templates select="move/file" />
		<xsl:apply-templates select="copy/file" />
		
		<xsl:apply-templates select="warning" />
		<xsl:apply-templates select="error" />
		
		<!-- TABLE SESSION_HAS_SESSIONTASK -->
		<xsl:text>INSERT INTO session_has_sessionTask(ref_idsession,ref_idsessionTask) VALUES(</xsl:text>
		<xsl:text>$_LAST_SESSION,$_MAX_IDSESSIONTASK);&#10;</xsl:text>
	</xsl:template>
	
	<xsl:template match="addLocalFiles">
		<xsl:text>INSERT INTO sessionTask(startTime,endTime,elapsedTime,status,taskType) VALUES('</xsl:text>
		<xsl:value-of select="@start" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@end" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@elapsedTime" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@status" />
		<xsl:text>','addLocalFiles');&#10;</xsl:text>
		
		<xsl:text>$_MAX_IDSESSIONTASK:=SELECT max(idsessionTask) FROM sessionTask&#10;</xsl:text>
		
		<xsl:apply-templates select="files/file" />
		<xsl:apply-templates select="move/file" />
		<xsl:apply-templates select="copy/file" />
		
		<xsl:apply-templates select="warning" />
		<xsl:apply-templates select="error" />

		<!-- TABLE SESSION_HAS_SESSIONTASK -->
		<xsl:text>INSERT INTO session_has_sessionTask(ref_idsession,ref_idsessionTask) VALUES(</xsl:text>
		<xsl:text>$_LAST_SESSION,$_MAX_IDSESSIONTASK);&#10;</xsl:text>
	</xsl:template>
	
	
	<xsl:template match="makeRelease">
		<xsl:text>INSERT INTO sessionTask(startTime,endTime,elapsedTime,status,nbFilesMoved,nbFilesCopied,taskType) VALUES('</xsl:text>
		<xsl:value-of select="@start" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@end" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@elapsedTime" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@status" />
		<xsl:text>',</xsl:text>
		<xsl:value-of select="@nbFilesMoved" />
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@nbFilesCopied" />
		<xsl:text>,'makeRelease');&#10;</xsl:text>
		
		<xsl:text>$_MAX_IDSESSIONTASK:=SELECT max(idsessionTask) FROM sessionTask&#10;</xsl:text>
		
		<xsl:apply-templates select="files/file" />
		<xsl:apply-templates select="move/file" />
		<xsl:apply-templates select="copy/file" />
		
		<xsl:apply-templates select="warning" />
		<xsl:apply-templates select="error" />

		<!-- TABLE SESSION_HAS_SESSIONTASK -->
		<xsl:text>INSERT INTO session_has_sessionTask(ref_idsession,ref_idsessionTask) VALUES(</xsl:text>
		<xsl:text>$_LAST_SESSION,$_MAX_IDSESSIONTASK);&#10;</xsl:text>
	</xsl:template>
	
	<xsl:template match="postprocess">

		<xsl:text>INSERT INTO sessionTask(startTime,endTime,elapsedTime,status,nbreMetaProcess,taskType) VALUES('</xsl:text>
		<xsl:value-of select="@start" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@end" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@elapsedTime" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@status" />
		<xsl:text>',</xsl:text>
		<xsl:value-of select="@nbreMetaProcess" />
		<xsl:text>,'postprocess');&#10;</xsl:text>
		
		<xsl:text>$_MAX_IDSESSIONTASK:=SELECT max(idsessionTask) FROM sessionTask&#10;</xsl:text>

		<!-- TABLE METAPROCESS -->
		<xsl:for-each select="metaprocess">
			<xsl:text>$_LAST_META:=$_MAX_IDSESSIONTASK.RAND()&#10;</xsl:text>
			
			<xsl:text>INSERT INTO metaprocess(idmetaprocess,name,startTime,endTime,elapsedTime,status,logfile,block,ref_idsessionTask) VALUES(</xsl:text>
			<xsl:text>'$_LAST_META','</xsl:text>
			<xsl:value-of select="@name" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@start" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@end" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@elapsedTime" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@status" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@logfile" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@block" />
			<xsl:text>',$_MAX_IDSESSIONTASK);&#10;</xsl:text>

			<!-- TABLE PROCESS -->
			<xsl:for-each select="process">
				<xsl:text>INSERT INTO process(name,keyname,exe,args,description,type,startTime,endTime,elapsedTime,biomaj_error,timestamp,value,ref_idmetaprocess) VALUES('</xsl:text>
				<xsl:value-of select="@name" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@keyname" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@exe" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@args" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@desc" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@type" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@start" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@end" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@elapsedTime" />
				<xsl:text>',</xsl:text>
				<xsl:value-of select="@biomaj_error" />
				<xsl:text>,</xsl:text>
				<xsl:value-of select="@timestamp" />
				<xsl:text>,'</xsl:text>
				<xsl:value-of select="@value" />
				<xsl:text>','$_LAST_META');&#10;</xsl:text>

				<xsl:text>$_MAX_IDPROCESS:=SELECT max(idprocess) FROM process&#10;</xsl:text>

				<!-- TABLE FILE -->
				<xsl:for-each select="files/file">
					<xsl:text>INSERT INTO file(location,size,time,link,is_extract,volatile,fileType,ref_idprocess) VALUES('</xsl:text>
					<xsl:value-of select="@location" />
					<xsl:text>',</xsl:text>
					<xsl:value-of select="@size" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@time" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@link" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@extract" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@volatile" />
					<xsl:text>,'file',$_MAX_IDPROCESS);&#10;</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="move/file">
					<xsl:text>INSERT INTO file(location,size,time,link,is_extract,volatile,fileType,ref_idprocess) VALUES('</xsl:text>
					<xsl:value-of select="@location" />
					<xsl:text>',</xsl:text>
					<xsl:value-of select="@size" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@time" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@link" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@extract" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@volatile" />
					<xsl:text>,'move',$_MAX_IDPROCESS);&#10;</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="copy/file">
					<xsl:text>INSERT INTO file(location,size,time,link,is_extract,volatile,fileType,ref_idprocess) VALUES('</xsl:text>
					<xsl:value-of select="@location" />
					<xsl:text>',</xsl:text>
					<xsl:value-of select="@size" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@time" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@link" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@extract" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@volatile" />
					<xsl:text>,'copy',$_MAX_IDPROCESS);&#10;</xsl:text>
				</xsl:for-each>


				<!-- TABLE MESSAGE + PROCESS_HAS_MESSAGE -->
				<xsl:for-each select="warning">
					<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
					<xsl:value-of select="@message" />
					<xsl:text>','warning');&#10;</xsl:text>
					<xsl:text>INSERT INTO process_has_message(ref_idprocess,ref_idmessage) VALUES(</xsl:text>
					<xsl:text>$_MAX_IDPROCESS,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="error">
					<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
					<xsl:value-of select="@message" />
					<xsl:text>','error');&#10;</xsl:text>
					<xsl:text>INSERT INTO process_has_message(ref_idprocess,ref_idmessage) VALUES(</xsl:text>
					<xsl:text>$_MAX_IDPROCESS,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
				</xsl:for-each>

			</xsl:for-each>

			<!-- TABLE MESSAGE + METAPROCESS_HAS_MESSAGE -->
			<xsl:for-each select="warning">
				<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
				<xsl:value-of select="@message" />
				<xsl:text>','warning');&#10;</xsl:text>
				<xsl:text>INSERT INTO metaprocess_has_message(ref_idmetaprocess,ref_idmessage) VALUES(</xsl:text>
				<xsl:text>$_LAST_META,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
			</xsl:for-each>
			<xsl:for-each select="error">
				<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
				<xsl:value-of select="@message" />
				<xsl:text>','error');&#10;</xsl:text>
				<xsl:text>INSERT INTO metaprocess_has_message(ref_idmetaprocess,ref_idmessage) VALUES(</xsl:text>
				<xsl:text>$_LAST_META,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
			</xsl:for-each>

		</xsl:for-each>

		<!-- TABLE SESSION_HAS_SESSIONTASK -->
		<xsl:text>INSERT INTO session_has_sessionTask(ref_idsession,ref_idsessionTask) VALUES(</xsl:text>
		<xsl:text>$_LAST_SESSION,$_MAX_IDSESSIONTASK);&#10;</xsl:text>
	</xsl:template>
	
	<xsl:template match="deployment">
		<xsl:text>INSERT INTO sessionTask(startTime,endTime,elapsedTime,status,taskType) VALUES('</xsl:text>
		<xsl:value-of select="@start" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@end" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@elapsedTime" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@status" />
		<xsl:text>','deployment');&#10;</xsl:text>
		
		<xsl:text>$_MAX_IDSESSIONTASK:=SELECT max(idsessionTask) FROM sessionTask&#10;</xsl:text>
		
		<xsl:apply-templates select="files/file" />
		<xsl:apply-templates select="move/file" />
		<xsl:apply-templates select="copy/file" />
		
		<xsl:apply-templates select="warning" />
		<xsl:apply-templates select="error" />

		<!-- TABLE SESSION_HAS_SESSIONTASK -->
		<xsl:text>INSERT INTO session_has_sessionTask(ref_idsession,ref_idsessionTask) VALUES(</xsl:text>
		<xsl:text>$_LAST_SESSION,$_MAX_IDSESSIONTASK);&#10;</xsl:text>
	</xsl:template>
	
	<xsl:template match="removeprocess">

		<xsl:text>INSERT INTO sessionTask(startTime,endTime,elapsedTime,status,nbreMetaProcess,taskType) VALUES('</xsl:text>
		<xsl:value-of select="@start" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@end" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@elapsedTime" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="@status" />
		<xsl:text>',</xsl:text>
		<xsl:value-of select="@nbreMetaProcess" />
		<xsl:text>,'removeprocess');&#10;</xsl:text>
		
		<xsl:text>$_MAX_IDSESSIONTASK:=SELECT max(idsessionTask) FROM sessionTask&#10;</xsl:text>

		<!-- TABLE METAPROCESS -->
		<xsl:for-each select="metaprocess">
			<xsl:text>$_LAST_META:=$_MAX_IDSESSIONTASK.RAND()&#10;</xsl:text>
			
			<xsl:text>INSERT INTO metaprocess(idmetaprocess,name,startTime,endTime,elapsedTime,status,logfile,block,ref_idsessionTask) VALUES(</xsl:text>
			<xsl:text>'$_LAST_META','</xsl:text>
			<xsl:value-of select="@name" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@start" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@end" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@elapsedTime" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@status" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@logfile" />
			<xsl:text>','</xsl:text>
			<xsl:value-of select="@block" />
			<xsl:text>',$_MAX_IDSESSIONTASK);&#10;</xsl:text>

			<!-- TABLE PROCESS -->
			<xsl:for-each select="process">
				<xsl:text>INSERT INTO process(name,keyname,exe,args,description,type,startTime,endTime,elapsedTime,biomaj_error,timestamp,value,ref_idmetaprocess) VALUES('</xsl:text>
				<xsl:value-of select="@name" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@keyname" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@exe" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@args" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@desc" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@type" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@start" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@end" />
				<xsl:text>','</xsl:text>
				<xsl:value-of select="@elapsedTime" />
				<xsl:text>',</xsl:text>
				<xsl:value-of select="@biomaj_error" />
				<xsl:text>,</xsl:text>
				<xsl:value-of select="@timestamp" />
				<xsl:text>,'</xsl:text>
				<xsl:value-of select="@value" />
				<xsl:text>','$_LAST_META');&#10;</xsl:text>

				<xsl:text>$_MAX_IDPROCESS:=SELECT max(idprocess) FROM process&#10;</xsl:text>
				
				<!-- TABLE FILE -->
				<xsl:for-each select="files/file">
					<xsl:text>INSERT INTO file(location,size,time,link,is_extract,volatile,fileType,ref_idprocess) VALUES('</xsl:text>
					<xsl:value-of select="@location" />
					<xsl:text>',</xsl:text>
					<xsl:value-of select="@size" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@time" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@link" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@extract" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@volatile" />
					<xsl:text>,'file',$_MAX_IDPROCESS);&#10;</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="move/file">
					<xsl:text>INSERT INTO file(location,size,time,link,is_extract,volatile,fileType,ref_idprocess) VALUES('</xsl:text>
					<xsl:value-of select="@location" />
					<xsl:text>',</xsl:text>
					<xsl:value-of select="@size" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@time" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@link" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@extract" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@volatile" />
					<xsl:text>,'move',$_MAX_IDPROCESS);&#10;</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="copy/file">
					<xsl:text>INSERT INTO file(location,size,time,link,is_extract,volatile,fileType,ref_idprocess) VALUES('</xsl:text>
					<xsl:value-of select="@location" />
					<xsl:text>',</xsl:text>
					<xsl:value-of select="@size" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@time" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@link" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@extract" />
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@volatile" />
					<xsl:text>,'copy',$_MAX_IDPROCESS);&#10;</xsl:text>
				</xsl:for-each>


				<!-- TABLE MESSAGE + PROCESS_HAS_MESSAGE -->
				<xsl:for-each select="warning">
					<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
					<xsl:value-of select="@message" />
					<xsl:text>','warning');&#10;</xsl:text>
					<xsl:text>INSERT INTO process_has_message(ref_idprocess,ref_idmessage) VALUES(</xsl:text>
					<xsl:text>$_MAX_IDPROCESS,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="error">
					<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
					<xsl:value-of select="@message" />
					<xsl:text>','error');&#10;</xsl:text>
					<xsl:text>INSERT INTO process_has_message(ref_idprocess,ref_idmessage) VALUES(</xsl:text>
					<xsl:text>$_MAX_IDPROCESS,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
				</xsl:for-each>

			</xsl:for-each>

			<!-- TABLE MESSAGE + METAPROCESS_HAS_MESSAGE -->
			<xsl:for-each select="warning">
				<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
				<xsl:value-of select="@message" />
				<xsl:text>','warning');&#10;</xsl:text>
				<xsl:text>INSERT INTO metaprocess_has_message(ref_idmetaprocess,ref_idmessage) VALUES(</xsl:text>
				<xsl:text>$_LAST_META,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
			</xsl:for-each>
			<xsl:for-each select="error">
				<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
				<xsl:value-of select="@message" />
				<xsl:text>','error');&#10;</xsl:text>
				<xsl:text>INSERT INTO metaprocess_has_message(ref_idmetaprocess,ref_idmessage) VALUES(</xsl:text>
				<xsl:text>$_LAST_META,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
			</xsl:for-each>

		</xsl:for-each>

		<!-- TABLE SESSION_HAS_SESSIONTASK -->
		<xsl:text>INSERT INTO session_has_sessionTask(ref_idsession,ref_idsessionTask) VALUES(</xsl:text>
		<xsl:text>$_LAST_SESSION,$_MAX_IDSESSIONTASK);&#10;</xsl:text>
	</xsl:template>
	
	<xsl:template match="files/file">
		<xsl:text>INSERT INTO file(location,size,time,link,is_extract,volatile,refHash,fileType) VALUES('</xsl:text>
		<xsl:value-of select="@location"/>
		<xsl:text>',</xsl:text>
		<xsl:value-of select="@size"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@time"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@link"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@extract"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@volatile" />
		<xsl:text>,'</xsl:text>
		<xsl:value-of select="@refHash"/>
		<xsl:text>','file');&#10;</xsl:text>

		<xsl:text>INSERT INTO sessionTask_has_file(ref_idsessionTask,ref_idfile) VALUES(</xsl:text>
		<xsl:text>$_MAX_IDSESSIONTASK,(SELECT max(idfile) FROM file));&#10;</xsl:text>
	</xsl:template>
	
	<xsl:template match="move/file">
		<xsl:text>INSERT INTO file(location,size,time,link,is_extract,volatile,refHash,fileType) VALUES('</xsl:text>
		<xsl:value-of select="@location"/>
		<xsl:text>',</xsl:text>
		<xsl:value-of select="@size"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@time"/>
		<xsl:text>,'</xsl:text>
		<xsl:value-of select="@link"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@extract"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@volatile" />
		<xsl:text>,</xsl:text>
		<xsl:choose>
			<xsl:when test="@refHash">
				<xsl:text>'</xsl:text>
				<xsl:value-of select="@refHash"/>
				<xsl:text>'</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>null</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>,'move');&#10;</xsl:text>

		<xsl:text>INSERT INTO sessionTask_has_file(ref_idsessionTask,ref_idfile) VALUES(</xsl:text>
		<xsl:text>$_MAX_IDSESSIONTASK,(SELECT max(idfile) FROM file));&#10;</xsl:text>
	</xsl:template>
	
	<xsl:template match="copy/file">
		<xsl:text>INSERT INTO file(location,size,time,link,is_extract,volatile,refHash,fileType) VALUES('</xsl:text>
		<xsl:value-of select="@location"/>
		<xsl:text>',</xsl:text>
		<xsl:value-of select="@size"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@time"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@link"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@extract"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@volatile" />
		<xsl:text>,'</xsl:text>
		<xsl:value-of select="@refHash"/>
		<xsl:text>','copy');&#10;</xsl:text>
 
		<xsl:text>INSERT INTO sessionTask_has_file(ref_idsessionTask,ref_idfile) VALUES(</xsl:text>
		<xsl:text>$_MAX_IDSESSIONTASK,(SELECT max(idfile) FROM file));&#10;</xsl:text>
	</xsl:template>
	
	<xsl:template match="warning">
		<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
		<xsl:value-of select="@message" />
		<xsl:text>','warning');&#10;</xsl:text>
		<xsl:text>INSERT INTO sessionTask_has_message(ref_idsessionTask,ref_idmessage) VALUES(</xsl:text>
		<xsl:text>$_MAX_IDSESSIONTASK,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
	</xsl:template>
	
	<xsl:template match="error">
		<xsl:text>INSERT INTO message(message,type) VALUES('</xsl:text>
		<xsl:value-of select="@message" />
		<xsl:text>','error');&#10;</xsl:text>
		<xsl:text>INSERT INTO sessionTask_has_message(ref_idsessionTask,ref_idmessage) VALUES(</xsl:text>
		<xsl:text>$_MAX_IDSESSIONTASK,(SELECT max(idmessage) FROM message));&#10;</xsl:text>
	</xsl:template>
	
</xsl:stylesheet>
