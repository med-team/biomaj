
package org.gmod.biomaj.ant.task;

import java.io.File;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.inria.biomaj.utils.BiomajBuildException;

/**
 * 
 * A set of common functions used for input validation in Ant
 * tasks.
 * 
 * @author Josh Goodman
 * @version CVS $Revision: 1.1 $
 * 
 */
public class InputValidation {

	/**
     * Checks if String properties have been set.
	 * @param p TODO
	 * @param property Name of the property.
	 * @param desc A short description of the property.
     * 
     * @throws org.apache.tools.ant.BuildException If a required property has not been set.
     */
    public static void checkString(Project p, String property, String desc) throws BuildException {
    	if ((property==null)||(property.length() <= 0)||(property.startsWith("${")) ) {
            throw new BiomajBuildException(p,"inputvalidation.checkstring", desc,new Exception());
        }
    }

    /**
     * Passes filenames to the function {@linkplain org.gmod.biomaj.ant.task.InputValidation#checkString(Project, String, String) checkString}
     * from File objects.
     * @param p TODO
     * @param file File object to check.
     * @param desc A brief description of the file object.
     * 
     * @throws org.apache.tools.ant.BuildException If a required property has not been set.
     */
    public static void checkString(Project p, File file, String desc) throws BuildException {
        checkString(p,file.getPath(), desc);
    }
	
}
