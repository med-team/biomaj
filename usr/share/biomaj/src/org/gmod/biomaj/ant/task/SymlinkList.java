/* File: SymlinkList.java
 * Created by jogoodma
 * Created on Aug 5, 2004 2:03:10 PM
 * Project: citrina
 * 
 * CVS Info:
 * $Id: SymlinkList.java,v 1.2 2004/08/06 19:39:07 jogoodma Exp $
 * $Author: jogoodma $
 * $Date: 2004/08/06 19:39:07 $
 *
 * Copyright (c) 2004, Indiana University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of Indiana University, Bloomington nor the names
 *    of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package org.gmod.biomaj.ant.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.inria.biomaj.utils.BiomajBuildException;

/**
 * This Ant task reads a list of file names from a file and loads
 * them into an Ant property.  Each filename will be delimited by
 * the given delimiter character or the default one of ','.  Each
 * filename needs to be on its own line and lines with # are skipped.
 * 
 * @author Josh Goodman
 * @version CVS $Revision: 1.2 $
 * 
 */
public class SymlinkList extends Task {

	/**
	 * @uml.property  name="propertyName"
	 */
	private String propertyName;
	/**
	 * @uml.property  name="sourceFile"
	 */
	private File sourceFile;
	/**
	 * @uml.property  name="delimiter"
	 */
	private String delimiter = ",";
	
	/**
	 *
	 * @see org.apache.tools.ant.Task#execute()
	 */
	@Override
	public void execute() throws BuildException {
		//Verify the required input.
		checkInput();
		Project p = getProject();
		
		try {
			//Read in the entire file line by line.
			BufferedReader br = new BufferedReader(new FileReader(sourceFile));
			StringBuffer sb = new StringBuffer();
			String line = null;
			
			while ((line = br.readLine()) != null) {
				//Skip lines starting with a # character.
				if (!line.startsWith("#")) {
					sb.append(line);
					sb.append(delimiter);
				}
			}
			
			//Set the property if it isn't empty.
			if (sb.length() > 0) {
				//Remove the last delimiter.
				sb.deleteCharAt(sb.lastIndexOf(delimiter));
				//Set the property with the filenames.
				p.setProperty(propertyName,sb.toString());
			}
			
			
			
		} catch(FileNotFoundException fnfe) {
			throw new BiomajBuildException(getProject(),fnfe);
		} catch(IOException ioe) {
			throw new BiomajBuildException(getProject(),ioe);
		}
	}
	
	/**
     * Checks the attributes passed to ftplisting task.
     *
     * @throws org.apache.tools.ant.BuildException If required attributes are not set.
     */
    private void checkInput() {
        InputValidation.checkString(getProject(),propertyName, "the name of the Ant property to set");
        InputValidation.checkString(getProject(),sourceFile, "the input file to get link names from");
    }
    
	/**
	 * Sets the delimiter to use in the property.  Each file name in the source file will be concatenated together separated by the delimiter.  The default delimiter is ','.
	 * @param delimiter  The delimiter to set.
	 * @uml.property  name="delimiter"
	 */
	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}
	
	/**
	 * The property name to set using the file names from  the source file.
	 * @param propertyName  The propertyName to set.
	 * @uml.property  name="propertyName"
	 */
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	
	/**
	 * Sets the source file to get file names from. Lines starting with a '#' character will be ignored.
	 * @param sourceFile  The sourceFile to set.
	 * @uml.property  name="sourceFile"
	 */
	public void setSourceFile(File sourceFile) {
		this.sourceFile = sourceFile;
	}
}
