package org.gmod.biomaj.ant.task.net;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.task.BmajExtract;
import org.inria.biomaj.ant.task.BmajTask;
import org.inria.biomaj.internal.ant.task.net.DirectHttpImpl;
import org.inria.biomaj.internal.ant.task.net.HttpImpl;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.session.bank.Bank;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.FileDesc;
import org.inria.biomaj.session.bank.ProductionDirectory;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * 
 * FileCheck class modified for directhttp protocol.
 * 
 * @author rsabas
 *
 */
public class DirectFileCheck extends BmajTask {

	private boolean fromScratch = false;
	private String url;
	private File onlineDir;
	private File offlineDir;
	private String method;
	
	private File output;
	private File keepOfflineFileList;
	private File copyFileList;
	private File extractFileList;
	private File updateFile;
	
	private BufferedWriter ftpFilesNeeded;
	private BufferedWriter keepOfflineFile;
	private BufferedWriter copyFile;
	private BufferedWriter extractFile;
	private BufferedWriter up2dateFileWriter;
	
	private long sizeToDownload = 0;
	private Date lastModified = new Date();
	private int countDownload = 0;
	private int countExtract = 0;
	private int countKeepOffline = 0;
	private int countCopyLocalOnline = 0;
	private int countCopyLocalOffline = 0;
	private String parameters;
	
	private Bank lastUpdateSession = null;
	
	
	public void execute() {
	
		setIO();				//Create the file IO objects.
		log("Finished initializing filecheck task.",Project.MSG_VERBOSE);

		parameters = "";
		String urlParams = getProject().getProperty("url.params");
		if (urlParams != null && !urlParams.trim().isEmpty()) {
			String[] params = urlParams.split(",");
			for (String param : params) {
				String paramValue = getProject().getProperty(param + ".value");
				if (paramValue != null && !paramValue.trim().isEmpty()) {
					try {
						
						if (paramValue.contains("_:_")) {
							String[] values = paramValue.split("_:_");
							for (String val : values) {
								if (!val.trim().isEmpty()) {
									parameters += param + "=" + URLEncoder.encode(val, "UTF-8") + "&"; 
								} else {
									parameters += param + "&";
								}
							}
						} else {
							parameters += param + "=" + URLEncoder.encode(paramValue, "UTF-8") + "&";
						}
						
					} catch (UnsupportedEncodingException e) {
						log(e, Project.MSG_ERR);
					}
				}
				else {
					parameters += param + "&";
				}
			}
			if (parameters.charAt(parameters.length() - 1) == '&') {
				parameters = parameters.substring(0, parameters.length() - 1);
			}
			getProject().setProperty(BiomajConst.urlConcatParams, parameters);
		}
		
		boolean logFilesIsOk = Boolean.valueOf(getProject().getProperty(BiomajConst.logFilesProperty));

		
		RemoteFile rf;
		try {
			rf = getRemoteFile();
		} catch (IOException e1) {
			throw new BiomajBuildException(getProject(), e1);
		}
		List<RemoteFile> listRF = new ArrayList<RemoteFile>();
		listRF.add(rf);
		
//		long remoteDate = rFile.getDate().getTime();
//		long remoteSize  = rFile.getSize();
//		String fileName = rFile.getAbsolutePath();

		String name = getProject().getProperty(BiomajConst.dbNameProperty);
		//on cherche le dernier repertoire de prod creee
//		Vector<ProductionDirectory> lpd = BiomajQueryXmlStateFile.getAvailableDirectoriesProduction(name);
		
		List<ProductionDirectory> lpd = BiomajSQLQuerier.getAvailableProductionDirectories(name);
		
		if (lpd.size()<=0) {
			log("No production directories have been created !",Project.MSG_VERBOSE);
		} else {
			ProductionDirectory pd = lpd.get(lpd.size()-1);
			lastUpdateSession = new Bank();
			try {
				if (!BiomajSQLQuerier.getUpdateBank(name, pd.getSession(), lastUpdateSession, true))
					lastUpdateSession = null;
			} catch (BiomajException e) {
				e.printStackTrace();
			}
			
		}
		
		for (RemoteFile rFile : listRF) {
			
			String oldBase = rFile.getBase(); 
			
			//log(Integer.toString(compteur),Project.MSG_INFO);
			long remoteDate = rFile.getDate().getTime();
			long remoteSize  = rFile.getSize();
			String fileName = rFile.getAbsolutePath();

			if (!fromScratch) {

				if (logFilesIsOk && lastUpdateSession != null) {
					/*
					 * Cas ou on a une archive de type tar
					 * 1 archive correspond a plusieurs fichiers en prod
					 */
					Vector<FileDesc> lFd = lastUpdateSession.getGeneratedFiles(getProject(), rFile);
//					Vector<FileDesc> lFd = null;
					if (lFd != null && lFd.size() > 0) {
						log("Archives "+rFile.getAbsolutePath()+" with "+Integer.toString(lFd.size())+" files",Project.MSG_VERBOSE);		
						for (int aF=0;aF<lFd.size();aF++)
							log("   -> "+lFd.get(aF).getName()+" timestamp="+Long.toString(lFd.get(aF).getTime())+" size="+lFd.get(aF).getSize(),Project.MSG_VERBOSE);	
						
						int count = 0;
						Vector<String> toCopy = new Vector<String>();
						for (FileDesc fd : lFd) {
							log(rFile.getName()+" contains "+fd.getLocation(),Project.MSG_DEBUG);	
//							File inOnline = new File(onlineDir.getAbsolutePath()+"/"+fd.getLocation().replace(offlineDir.getAbsolutePath(), ""));
							File inOnline = new File(fd.getLocation());
							if (inOnline.exists()) {
								log(fd.getLocation()+" find in production!",Project.MSG_VERBOSE);
								try {
									toCopy.add(inOnline.getCanonicalPath());
								} catch (IOException e) {
									log(e, Project.MSG_ERR);
								}
								count++;
							} else {
								File inOffline = new File(offlineDir.getAbsolutePath() + " / " + fd.getLocation().replace(onlineDir.getAbsolutePath(), ""));
								if (inOffline.exists()) {
									log(fd.getLocation()+" find in offline!",Project.MSG_VERBOSE);
									addFileToKeepOfflineList(inOffline.getAbsolutePath());
									count++;
								} else {
									log("file ["+fd.getLocation()+"] has been deleted, BioMAJ download "+rFile.getAbsolutePath(),Project.MSG_WARN);
									count = 0;
									countDownload = 1;
									if (rFile.getName().endsWith(".zip") || rFile.getName().endsWith(".tar") || rFile.getName().endsWith(".bz2")
											|| rFile.getName().endsWith(".gz") || rFile.getName().endsWith(".Z") || rFile.getName().endsWith(".tgz")) {
										addFileToExtractList(rFile.getName());
									}
									break;
								}
							}
						}
						/*
						 * Si tous les fichiers existent, on ajoute en copy/link et on passe au remote file suivant!
						 */
						if (count == lFd.size()) {
							for (String path : toCopy) {
								addFileToCopyList(path);
							}
							continue;
						}
					} else {
						log("None file ["+rFile.getAbsolutePath()+"] have been stored in the last session ",Project.MSG_VERBOSE);
					}
				}
				/*
				 * Cas ou on a 1 archive correspondant a 1 fichier en prod
				 */
				String fileNameUncompressed = getLocalNameFromStateFile(rFile,logFilesIsOk);
				File fLocalUncompressedInOffline = new File(offlineDir,fileNameUncompressed);
				/*
				 * Fichier decompresse qui existe dans offline
				 */
				if (fLocalUncompressedInOffline.exists()) {
					FileDesc fd = null;//getFileDescFromFileCompressed(fileName);
					if (fd == null)
					{
						//log(fLocalUncompressedInOffline.getName()+" not handle by the statefile",Project.MSG_INFO);
						if (remoteDate<=fLocalUncompressedInOffline.lastModified()) {
							log(fLocalUncompressedInOffline.getAbsolutePath() +" find in offline directory",Project.MSG_VERBOSE);
							//!!! ON doit laisser une infos comme quoi il y a une nouvelle version a produire
							addLocalOfflineFile(fLocalUncompressedInOffline.getAbsolutePath());
							addFileToKeepOfflineList(fLocalUncompressedInOffline.getAbsolutePath());
							countCopyLocalOffline++;
							continue;
						}
					}
				} 
				
				// le fichier existe en offline mais ne correspond pas au fichier distant : on doit l effacer
				// pour que les links du online vers le offline possible fonctionne par la suite
				fLocalUncompressedInOffline.delete();
	
				File fLocalUncompressedInOnline = new File(onlineDir,fileNameUncompressed);
				/*
				 * Fichier decompresse qui se trouve dans online
				 */
				if (fLocalUncompressedInOnline.exists()) {
					FileDesc fd = null;//getFileDescFromFileCompressed(fileName);
					if (fd == null)
					{
						//log(fLocalUncompressedInOnline.getName()+" not handle by the statefile",Project.MSG_INFO);
						long t = fLocalUncompressedInOnline.lastModified();
						if (remoteDate<=t) {
							log(fLocalUncompressedInOnline.getAbsolutePath() +" find in online directory",Project.MSG_VERBOSE);
							try {
								addFileToCopyList(fLocalUncompressedInOnline.getCanonicalPath());
							} catch (IOException e) {
								log(e, Project.MSG_ERR);
							}
							//si le meme fichier existe dans le offline, il faut l effacer
							if (fLocalUncompressedInOffline.exists())
							{
								log("delete:"+fLocalUncompressedInOffline.getAbsolutePath());
								fLocalUncompressedInOffline.delete();
							}
							continue;
						} else {
							log(rFile.getAbsolutePath()+": Local date ["+BiomajUtils.dateToString(new Date(fLocalUncompressedInOnline.lastModified()), Locale.US)+
									"] Remote date ["+BiomajUtils.dateToString(new Date(remoteDate), Locale.US)+"]",Project.MSG_VERBOSE);
						}
					}
				}
	
				File fLocalcompressedInOffline = new File(offlineDir,fileName);
				/*
				 * Fichier compresse qui existe dans offline
				 */
				if (fLocalcompressedInOffline.exists()) {
					FileDesc fd = null;//  getFileDescFromFileCompressed(fileName);
					if (fd == null)
					{
						//log(fileName+" not handle by the statefile",Project.MSG_INFO);
						if (remoteSize==fLocalcompressedInOffline.length()&&(remoteDate<=fLocalcompressedInOffline.lastModified())) {
							log(fLocalcompressedInOffline.getAbsolutePath() +" find in offline directory",Project.MSG_VERBOSE);
							addFileToExtractList(fileName);
							addLocalOfflineFile(fLocalcompressedInOffline.getAbsolutePath());
							addFileToKeepOfflineList(fLocalcompressedInOffline.getAbsolutePath());
							countCopyLocalOffline++;
							continue;
						}
					}
				}
	
	//			YOANN		Prise en compte des fichiers compresses dans le online		
				/*
				 * Fichier compresse qui se trouve dans online
				 */
	//			YOANN Prise en compte des fichiers compresses dans le online			
				File fLocalcompressedInOnline = new File(onlineDir,fileName);
	//			FIN YOANN	
				if (fLocalcompressedInOnline.exists()) {
					FileDesc fd = null;//getFileDescFromFileCompressed(fileName);
					if (fd == null)
					{
						//log(fLocalcompressedInOnline.getName()+" not handle by the statefile",Project.MSG_INFO);
						long t = fLocalcompressedInOnline.lastModified();
						if (remoteDate<=t) {
							log(fLocalcompressedInOnline.getAbsolutePath() +" find in online directory",Project.MSG_VERBOSE);
							try {
								addFileToCopyList(fLocalcompressedInOnline.getCanonicalPath());
							} catch (IOException e) {
								log(e, Project.MSG_ERR);
							}
							//si le meme fichier existe dans le offline, il faut l effacer
							if (fLocalcompressedInOffline.exists())
							{
								log("delete:"+fLocalcompressedInOffline.getAbsolutePath());
								fLocalcompressedInOffline.delete();
							}
							continue;
						}
					}
				}
			}

//			FIN YOANN

			// If bank was computed, restore old base to have a valid path
			rFile.setBase(oldBase);

		}
		
		finished();
	}

	private RemoteFile getRemoteFile() throws IOException {
		String release = null;
		String name = getProject().getProperty(BiomajConst.dbNameProperty);
		Map<String, String> update = BiomajSQLQuerier.getLatestUpdate(name, true);
		if (update != null) {
			release = update.get(BiomajSQLQuerier.UPDATE_RELEASE);
		}
		url = url.replaceAll("http://", "");
		String _url = url;
		if (method.equalsIgnoreCase(DirectHttpImpl.GET) && !parameters.trim().isEmpty())
			_url = url + "?" + parameters;

		URLConnection cx = HttpImpl.connectToURLThroughProxyIfNeeded("http://" + _url);
		if (method.equalsIgnoreCase(DirectHttpImpl.POST)) {
			cx.setDoOutput(true);
			OutputStreamWriter osw;
			try {
				osw = new OutputStreamWriter(cx.getOutputStream());
				osw.write(this.parameters);
				osw.flush();
				osw.close();
			} catch (IOException e) {
				e.printStackTrace();
				throw e;
			}
		}
		
		long dlSize = 0;
		try {
			cx.connect();
			cx.getInputStream(); // Causes an exception if bad response
			lastModified = new Date(cx.getHeaderFieldDate("Last-Modified", new Date().getTime()));
			if (cx.getHeaderField("Content-Length") != null)
				dlSize = Long.valueOf(cx.getHeaderField("Content-Length"));
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(lastModified);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		String targetName = null;
		if ((targetName = getProject().getProperty("target.name")) == null) {
			targetName = "data";
			getProject().setProperty("target.name", targetName);
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(getProject().getProperty("release.dateformat"));
			Date refDate = new Date(0);
			if (release != null)
				refDate = sdf.parse(release);
			if (refDate.compareTo(cal.getTime()) < 0 || fromScratch) {
				getProject().setProperty(BiomajConst.remoteReleaseDynamicProperty, sdf.format(lastModified));
				if (targetName.endsWith(".zip") || targetName.endsWith(".tar") || targetName.endsWith(".bz2")
						|| targetName.endsWith(".gz") || targetName.endsWith(".Z") || targetName.endsWith(".tgz")) {
					addFileToExtractList(targetName);
				}
				countDownload = 1;
				sizeToDownload = dlSize;
				
			} else {
				getProject().setProperty(BiomajConst.remoteReleaseDynamicProperty, release);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		RemoteFile rFile = null;
		try {
			rFile = new RemoteFile("name=" + targetName + ",base=,link=false,date="
					+ BiomajUtils.dateToString(lastModified, Locale.US) + ",size=" + sizeToDownload + ",isDir=false");
		} catch (BiomajException e1) {
			e1.printStackTrace();
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		return rFile;
	}

	/*
	@Override
	public void execute() throws BuildException {
		setIO();
		
		String release = null;
		String name = getProject().getProperty(BiomajConst.dbNameProperty);
		Map<String, String> update = BiomajSQLQuerier.getLatestUpdate(name, true);
		if (update != null) {
			release = update.get(BiomajSQLQuerier.UPDATE_RELEASE);
		}
		url = url.replaceAll("http://", "");
		URLConnection cx = HttpImpl.connectToURLThroughProxyIfNeeded("http://" + url);
		long dlSize = 0;
		try {
			cx.connect();
			lastModified = new Date(cx.getHeaderFieldDate("Last-Modified", new Date().getTime()));
			dlSize = Long.valueOf(cx.getHeaderField("Content-Length"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(lastModified);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(getProject().getProperty("release.dateformat"));
			Date refDate = new Date(0);
			if (release != null)
				refDate = sdf.parse(release);
			if (refDate.compareTo(cal.getTime()) < 0 || fromScratch) {
				getProject().setProperty(BiomajConst.remoteReleaseDynamicProperty, sdf.format(lastModified));
				String targetName = getProject().getProperty("target.name");
				if (targetName.endsWith(".zip") || targetName.endsWith(".tar") || targetName.endsWith(".bz2")
						|| targetName.endsWith(".gz") || targetName.endsWith(".Z") || targetName.endsWith(".tgz")) {
					addFileToExtractList(targetName);
				}
				countDownload = 1;
				sizeToDownload = dlSize;
				
			} else {
				getProject().setProperty(BiomajConst.remoteReleaseDynamicProperty, release);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		finished();
		
	}*/

	private void addFileToKeepOfflineList(String fileName)    {
		try {
			log(fileName + " added to the keep offline list.",Project.MSG_VERBOSE);
			keepOfflineFile.write(fileName);
			keepOfflineFile.newLine();
			countKeepOffline++; 
		} catch (IOException e) {
			throw new BiomajBuildException(getProject(),"fileCheck.error.keep.offline.file",fileName,e);
		}
	}
	
	private String getLocalNameFromStateFile(RemoteFile rf,boolean logFilesIsOk) {
		//FileDesc res = null ;
		String defaultValue = rf.getAbsolutePath();

		//Si on ne trouve pas dans le fichier xml le fichier par defaut, le fichier en local porte 
		//le meme nom si no.extract=true et sans l extension de compression si no.extract=false

		if (!getProject().getProperties().containsKey(BiomajConst.noExtractProperty))
			defaultValue = BiomajUtils.getLocalFileName(rf.getAbsolutePath());
		else if ((!Boolean.valueOf(getProject().getProperty(BiomajConst.noExtractProperty)))) {
			defaultValue = BiomajUtils.getLocalFileName(rf.getAbsolutePath());
		}
		if (logFilesIsOk) {
			if (lastUpdateSession == null)
				return defaultValue;

			//res = lastUpdateSession.findFile(rf.getAbsolutePath(), rf.getDate().getTime(), rf.getSize(), Session.DOWNLOAD);
			String refHash = BiomajUtils.getHashFromRemoteFile(rf);
			/*
			if (res == null)
			{
				log(rf.getAbsolutePath()+" never logged in statefiles",Project.MSG_VERBOSE);
				return defaultValue;
			}
			 */
			Vector<FileDesc> lFd = lastUpdateSession.findDependancesFilesFromExtraction(refHash);
			if (lFd.size()==1) {
				log("Find !!!:"+lFd.get(0).getName());
				return lFd.get(0).getName();
			} else {
				BiomajLogger.getInstance().log("FileCheck::getLocalNameFromStateFile find more than one local file :");
				for (FileDesc f : lFd)
					BiomajLogger.getInstance().log(f.toString());

			}
		}

		return defaultValue;
	}
	
	private void addFileToCopyList(String path) {
		try {
			//Secure system : we test if the file exist!
			File f = new File(path);
			if (!f.exists())
				throw new BiomajBuildException(getProject(),"fileCheck.error.add.file",path,new Exception());
			//log("AVANT PATH COPYLIST:"+path,Project.MSG_INFO);
			String newPath = path.replace(onlineDir.getCanonicalPath()+"/","");
			//log("PATH COPYLIST:"+path,Project.MSG_INFO);

			//Correction Bug 28 novembre , il faut creer les sous repertoires si il n existe pas dans le offline!
			BiomajUtils.createSubDirectories(BiomajUtils.getRelativeDirectory(offlineDir.getAbsolutePath()+"/"+newPath));

			copyFile.write(newPath);
			copyFile.newLine();
			countCopyLocalOnline++;
		} catch (IOException e) {
			throw new BiomajBuildException(getProject(),e);
		}
	}
	
	private void addFileToExtractList(String fileName) {
		try {
			String extractName = BmajExtract.removeExtension(fileName);
			if (extractName.compareTo(fileName)==0) {
				log("no compressed format:"+fileName,Project.MSG_VERBOSE);
				return;
			}
			log(fileName + " added to the extract list.",Project.MSG_VERBOSE);
			extractFile.write(fileName);
			extractFile.newLine();
			countExtract++; 
		} catch (Exception e) {
			throw new BiomajBuildException(getProject(),"fileCheck.error.extract.file",fileName,e);
		}
	}
	
	private void setIO() {
		try {
			ftpFilesNeeded = new BufferedWriter(new FileWriter(output));
			keepOfflineFile = new BufferedWriter(new FileWriter(keepOfflineFileList));
			copyFile = new BufferedWriter(new FileWriter(copyFileList));
			extractFile = new BufferedWriter(new FileWriter(extractFileList));
			up2dateFileWriter = new BufferedWriter(new FileWriter(updateFile));
		} catch (Exception e) {
			throw new BiomajBuildException(getProject(),e);
		}
	}
	
	private void finished() {
		try {
//			The copy file is used as an includesfile in ant.  So when it is empty all files get included.
			//That is not what we want to happen so we use this line to prevent it.
			copyFile.write("#PLACE_HOLDER_TO_PREVENT_EMPTY_FILE");
			copyFile.newLine();

			extractFile.write("#PLACE_HOLDER_TO_PREVENT_EMPTY_FILE");
			extractFile.newLine();

			keepOfflineFile.write("#PLACE_HOLDER_TO_PREVENT_EMPTY_FILE");
			extractFile.newLine();

			//Close the file buffers and flush them out.
			copyFile.close();
			ftpFilesNeeded.close();
			extractFile.close();
			keepOfflineFile.close();

			//Set the count property to the final download count.
			up2dateFileWriter.write("num.new.files.download_and_extract=" + Integer.toString(countDownload)+"\n");
			up2dateFileWriter.write("remote.files.size=" + Long.toString(sizeToDownload)+"\n");
			up2dateFileWriter.write("num.new.files.extract=" + Integer.toString(countExtract)+"\n");
			up2dateFileWriter.write("num.new.files.offline=" + Integer.toString(countKeepOffline)+"\n");

			up2dateFileWriter.newLine();

			if (countDownload > 0)
				up2dateFileWriter.write(BiomajConst.filesDownloadNeedDynamicProperty+"=true\n");
			else
				up2dateFileWriter.write(BiomajConst.filesDownloadNeedDynamicProperty+"=false\n");

			getProject().setProperty(BiomajConst.countDownloadProperty, Integer.toString(countDownload));

			if (countExtract>0) 
				up2dateFileWriter.write(BiomajConst.filesExtractNeedDynamicProperty+"=true\n");
			else
				up2dateFileWriter.write(BiomajConst.filesExtractNeedDynamicProperty+"=false\n");

			getProject().setProperty(BiomajConst.countExtractProperty, Integer.toString(countExtract));
			
			up2dateFileWriter.write(BiomajConst.filesCopyNeedDynamicProperty+"="+Boolean.toString(false)+"\n");
			getProject().setProperty(BiomajConst.filesCopyNeedDynamicProperty, Boolean.toString(false));
			getProject().setProperty(BiomajConst.countLocalOnlineFileProperty, Integer.toString(countCopyLocalOnline));
			getProject().setProperty(BiomajConst.countLocalOfflineFileProperty, Integer.toString(countCopyLocalOffline));

			//on en profite pour mettre la valeur de la release distant!
			up2dateFileWriter.write(BiomajConst.remoteReleaseDynamicProperty+"="+getProject().getProperty(BiomajConst.releaseResultProperty)+"\n");

			up2dateFileWriter.close();

		} catch (Exception e) {
			throw new BiomajBuildException(getProject(),e);
		} 
	}

	public boolean isFromScratch() {
		return fromScratch;
	}

	public void setFromScratch(boolean fromScratch) {
		this.fromScratch = fromScratch;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public File getOnlineDir() {
		return onlineDir;
	}

	public void setOnlineDir(File onlineDir) {
		this.onlineDir = onlineDir;
	}

	public File getOfflineDir() {
		return offlineDir;
	}

	public void setOfflineDir(File offlineDir) {
		this.offlineDir = offlineDir;
	}

	public File getOutput() {
		return output;
	}

	public void setOutput(File output) {
		this.output = output;
	}

	public File getKeepOfflineFileList() {
		return keepOfflineFileList;
	}

	public void setKeepOfflineFileList(File keepOfflineFileList) {
		this.keepOfflineFileList = keepOfflineFileList;
	}

	public File getCopyFileList() {
		return copyFileList;
	}

	public void setCopyFileList(File copyFileList) {
		this.copyFileList = copyFileList;
	}

	public File getExtractFileList() {
		return extractFileList;
	}

	public void setExtractFileList(File extractFileList) {
		this.extractFileList = extractFileList;
	}

	public File getUpdateFile() {
		return updateFile;
	}

	public void setUpdateFile(File updateFile) {
		this.updateFile = updateFile;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
}
