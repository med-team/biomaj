/* File: DirectoryHasFilesCondition.java
 * Created by jogoodma
 * Created on Feb 12, 2004 10:22:38 AM
 * Project: citrina
 * 
 * CVS Info:
 * $Id: DirectoryHasFilesCondition.java,v 1.2 2004/06/07 20:01:34 jogoodma Exp $
 * $Author: jogoodma $
 * $Date: 2004/06/07 20:01:34 $
 *
 * Copyright (c) 2004, Indiana University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of Indiana University, Bloomington nor the names
 *    of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
package org.gmod.biomaj.ant.conditions;

import java.util.Vector;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.MatchingTask;
import org.apache.tools.ant.taskdefs.condition.Condition;
import org.apache.tools.ant.types.FileSet;
import org.inria.biomaj.utils.BiomajBuildException;


/**
 * Custom ant condition to check whether a directory contains files or not.
 * Meant to be used within Ant's <condition> task.
 * 
 * @author Josh Goodman
 * @version CVS $Revision: 1.2 $
 * 
 */
public class DirectoryHasFilesCondition extends MatchingTask implements Condition {
	/**
	 * @uml.property  name="filesets"
	 * @uml.associationEnd  multiplicity="(0 -1)" elementType="org.apache.tools.ant.types.FileSet"
	 */
	private Vector<FileSet> filesets = new Vector<FileSet>();
	
	/**
	 * Sets the FileSet to be checked for the presence or lack of files.
	 * 
	 * @param fileset File set to check for files.
	 */
	public void addFileset(FileSet fileset) {
		filesets.add(fileset);
	}
	
	
	/**
	 *  Method to evaluate whether the directory has files or not.
	 * 
	 * @see org.apache.tools.ant.taskdefs.condition.Condition#eval()
	 * @return A boolean that is true if the directory has files or false if it doesn't.
	 */
	public boolean eval() {
		boolean hasFiles = false;
		
		if (filesets.size() <= 0) {
			throw new BiomajBuildException(this.getProject(),"directoryHasFilesCondition.error.fileset",new Exception());
		}
		
		for (int i=0; i < filesets.size(); i++) {
			FileSet fs = filesets.elementAt(i);
			DirectoryScanner ds = fs.getDirectoryScanner(getProject());
			String[] files = ds.getIncludedFiles();
			log("Found " + files.length + " files",Project.MSG_DEBUG);
			if (files.length >0) {
				hasFiles = true;
			}
		}
		
		return hasFiles;
	}
}
