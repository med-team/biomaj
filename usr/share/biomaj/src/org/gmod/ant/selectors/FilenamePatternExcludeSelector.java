package org.gmod.ant.selectors;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.tools.ant.types.selectors.BaseSelector;

public class FilenamePatternExcludeSelector extends BaseSelector {
	
	/**
	 * @uml.property  name="patterns" multiplicity="(0 -1)" dimension="1"
	 */
	private String[] patterns;
	
	/**
	 * Set the pattern to search a fileset for.
	 * 
	 * @param s A string consisting of the java pattern to use for searching.
	 */
	public void setPattern(String s) {
		patterns = s.split("\\s+");
	}
	
	@Override
	public void verifySettings()   {
		if (patterns == null) {
			setError("The pattern parameter should be set.");
		}
	}
	
	@Override
	public boolean isSelected(File arg0, String arg1, File arg2) {
		validate();
		
		for (int i=0; i<patterns.length; i++) {
			if (patterns[i].trim().compareTo("")==0)
				continue;
			Pattern pattern = Pattern.compile(patterns[i]);
			Matcher matcher = pattern.matcher(arg1);
			if (matcher.find())
				return false;
		}
		return true;
	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub
		super.validate();
	}
	
}
