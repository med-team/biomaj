/* File: FilenamePatternSelector.java
 * Created by jogoodma
 * Created on Feb 24, 2004 1:24:46 PM
 * Project: citrina
 * 
 * CVS Info:
 * $Id: FilenamePatternSelector.java,v 1.5 2004/08/24 16:30:49 jogoodma Exp $
 * $Author: jogoodma $
 * $Date: 2004/08/24 16:30:49 $
 *
 * Copyright (c) 2004, Indiana University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of Indiana University, Bloomington nor the names
 *    of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

package org.gmod.ant.selectors;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.types.selectors.BaseSelector;


/**
 * @author jogoodma
 * @version CVS $Revision: 1.5 $
 */
public class FilenamePatternSelector extends BaseSelector {
	/**
	 * @uml.property  name="patterns" multiplicity="(0 -1)" dimension="1"
	 */
	private String[] patterns;
	
	/**
	 * Set the pattern to search a fileset for.
	 * 
	 * @param s A string consisting of the java pattern to use for searching.
	 */
	public void setPattern(String s) {
		patterns = s.split("\\s+");
	}
	
	@Override
	public void verifySettings()   {
		if (patterns == null) {
			setError("The pattern parameter should be set.");
		}
	}
	
	/**
	 * Compare the filename sent via the fileset to the pattern.
	 * 
	 * @see org.apache.tools.ant.types.selectors.FileSelector#isSelected(java.io.File, java.lang.String, java.io.File)
	 */
	@Override
	public boolean isSelected(File base, String filename, File file) throws BuildException {
		validate();
		
		for (int i=0; i<patterns.length; i++) {
			Pattern pattern = Pattern.compile(patterns[i]);
			Matcher matcher = pattern.matcher(filename);
			if (matcher.find())
				return true;
		}
		return false;
	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub
		super.validate();
	}

	
}
