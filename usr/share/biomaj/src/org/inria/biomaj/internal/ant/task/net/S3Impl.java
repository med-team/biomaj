package org.inria.biomaj.internal.ant.task.net;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Vector;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;
import org.jets3t.service.Jets3tProperties;
import org.jets3t.service.S3Service;
import org.jets3t.service.S3ServiceException;
import org.jets3t.service.ServiceException;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Bucket;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.multi.DownloadPackage;
import org.jets3t.service.multi.SimpleThreadedStorageService;
import org.jets3t.service.security.AWSCredentials;


/**
 * Class for S3 based servers querying. 
 * 
 * @author rsabas
 *
 */
public class S3Impl extends RemoteCommandImpl {

	private S3Service s3;
	
	public S3Impl(Task task) {
		super(task);
	}
    
    @Override
    public void init(String server, Integer port, String username,
    		String password) throws BiomajBuildException {
    	super.init(server, port, username, password);
    	
    	Jets3tProperties config = null;
		try {
			InputStream in  = new FileInputStream(BiomajUtils.getBiomajRootDirectory() + "/jets3t.properties");
			config = Jets3tProperties.getInstance(in, "jets3t");
			
			String base = server.substring(0, server.indexOf('/'));
			String virtualPath = server.substring(server.indexOf('/'));
			config.setProperty("s3service.s3-endpoint", base);
			config.setProperty("s3service.s3-endpoint-http-port", port.toString());
			config.setProperty("s3service.s3-endpoint-virtual-path", virtualPath);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (config == null)
			return;
		
		AWSCredentials credentials = new AWSCredentials(username, password);
		try {
	    	s3 = new RestS3Service(credentials, "BioMAJ S3 Impl", null, config);
		} catch (S3ServiceException e) {
			s3 = null;
			e.printStackTrace();
		}
    	
    }

	@Override
	public void disconnect() {
	}

	@Override
	protected boolean getFileWithImpl(String remoteDirectory, String nameFile,
			String targetDirectory, String targetName) throws BiomajException {
		
		log("S3 : retrieving object '" + nameFile + "' in bucket '" + remoteDirectory + "'", Project.MSG_DEBUG);
		try {
			S3Object dl = s3.getObject(remoteDirectory, nameFile);
			BufferedReader br = new BufferedReader(new InputStreamReader(dl.getDataInputStream()));
			OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(targetDirectory + "/" + targetName));
			String line = "";
			while ((line = br.readLine()) != null) {
				out.write(line);
			}
			out.close();
			dl.closeDataInputStream();
			br.close();
			
			// Add integrity check ?
			
			return true;
		} catch (S3ServiceException e) {
			log(e.getMessage(), Project.MSG_ERR);
			e.printStackTrace();
		} catch (ServiceException e) {
			log(e.getMessage(), Project.MSG_ERR);
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			log(e.getMessage(), Project.MSG_ERR);
			e.printStackTrace();
		} catch (IOException e) {
			log(e.getMessage(), Project.MSG_ERR);
			e.printStackTrace();
		}
		
		return false;
	}
	
	/**
	 * This method uses jets3t multi threaded download manager to download several files
	 * at the same time.
	 * 
	 * @param bucket
	 * @param names
	 * @param destDir
	 * @return download success
	 */
	public boolean downloadFiles(String bucket, List<String> names, String destDir) {
		DownloadPackage[] pkg = new DownloadPackage[names.size()];
		for (int i = 0; i < names.size(); i++) {
			log("Adding to batch download : " + names.get(i), Project.MSG_DEBUG);
			pkg[i] = new DownloadPackage(new S3Object(names.get(i)), new File(destDir + "/" + bucket + "/" + names.get(i)));
		}
    	
    	SimpleThreadedStorageService multi = new SimpleThreadedStorageService(s3);
    	try {
			multi.downloadObjects(bucket, pkg);
			return true;
		} catch (ServiceException e) {
			log(e.getMessage(), Project.MSG_ERR);
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public List<RemoteFile> listAll(String base, String directoryToApplyLs)
			throws BiomajBuildException {
		// Only buckets at root
		if (base.trim().isEmpty() || (base.equals("/") && directoryToApplyLs.trim().isEmpty())) // List buckets
			return listDir(base, directoryToApplyLs);
		else { // Looking into a bucket, only objects are available
			if (!base.equals("/"))
				return listFiles(base, directoryToApplyLs);
			else
				return listFiles("", directoryToApplyLs);
		}
	}

	@Override
	public Vector<RemoteFile> listDir(String base, String directoryToApplyLs)
			throws BiomajBuildException { // Actually listing buckets
		
		
		Vector<RemoteFile> filz = new Vector<RemoteFile>();
		// No used for method paramaters as there is only one level of buckets
		try {
			for (S3Bucket bucket : s3.listAllBuckets()) {
				RemoteFile rf = new RemoteFile();
				rf.setDir(true);
				rf.setName(bucket.getName());
				filz.add(rf);
			}
			
			return filz;
		} catch (S3ServiceException e) {
			log(e.getMessage(), Project.MSG_ERR);
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public Vector<RemoteFile> listFiles(String base, String directoryToApplyLs)
			throws BiomajBuildException {
		
		Vector<RemoteFile> filz = new Vector<RemoteFile>();
		// Bucket name is either base or directoryToApplyLs
		String bucketName = base.trim().isEmpty() ? directoryToApplyLs : base;
		try {
			for (S3Object obj : s3.listObjects(bucketName)) {
				RemoteFile rf = new RemoteFile();
				rf.setDir(false);
				rf.setName(obj.getName());
				rf.setBase(bucketName);
				rf.setDate(obj.getLastModifiedDate());
				rf.setSize(obj.getContentLength());
				filz.add(rf);
			}
			
			return filz;
		} catch (S3ServiceException e) {
			log(e.getMessage(), Project.MSG_ERR);
			e.printStackTrace();
		}
		
		return null;
	}
	
}
