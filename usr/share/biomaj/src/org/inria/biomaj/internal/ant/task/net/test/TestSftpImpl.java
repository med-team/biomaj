package org.inria.biomaj.internal.ant.task.net.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;
import java.util.Vector;

import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.internal.ant.task.net.SftpImpl;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 * JUnit test class for sftp implementation.
 * It consists in :
 * <ul>
 *	<li>testing the remote listing methods : ensures that the listed
 *  files and directories are the expected ones</li>
 *  <li>testing the download method : ensures that all the files are
 *  downloaded and that they are the same (md5 checksum)</li>
 * </ul>
 * 
 * Sftp server has to be running on localhost as we create need to create the
 * tests files.
 * 
 * @author rsabas
 * 
 */
public class TestSftpImpl {
	
	private final int FILE_COUNT = 5;
	private final int DIR_COUNT = 2;

	// Server address
	private String server;
	private int port;
	private String login;
	private String password;
	// Remote directory to be accessed
	private String remoteDirectory;
	// Local directory where the files should be download to
	private String localDirectory;

	// Files to retrieve
	private HashMap<String, File> remoteFiles;
	// Remote directories to be checked after listing
	private Vector<File> remoteDirectories;
	
	private SftpImpl client;
	
	public TestSftpImpl() {
		remoteFiles = new HashMap<String, File>(FILE_COUNT);
		remoteDirectories = new Vector<File>(DIR_COUNT);
		client = new SftpImpl(null);
		localDirectory = "/home/romaric/sftpdl";
		port = 22;
	}

	/**
	 * Sets up the test environment.
	 */
	@Before
	public void setUp() throws Exception {

		server = "localhost";
		login = "romaric";
		password = "toto";

		
		remoteDirectory = BiomajUtils.getBiomajRootDirectory() + "/testsftp";
		File testDir = new File(remoteDirectory);
		if (testDir.exists())
			removeDir(testDir);
		
		testDir.mkdir();
		
		File f = null;
		
		// Dummy files
		for (int i = 0; i < FILE_COUNT; i++) {
			f = new File(testDir, "file" + i );
			f.createNewFile();
			createRemoteFile(f);
		}
		
		// Dummy directories
		for (int i = 0; i < DIR_COUNT; i++) {
			f = new File(remoteDirectory + "/dir" + i);
			f.mkdir();
			remoteDirectories.add(f);
		}
		
		/*
		 * Starts up the client
		 */
		
		// As we can't call the client.init() method because it needs objects
		// that are not initialized unless we run the whole biomaj application,
		// we will manually initialize the fields that we need the work the
		// tests out.
		client.setServer(server);
		client.setPort(port);
		client.setUsername(login);
		client.setPassword(password);
		client.setTimeout(SftpImpl.DEFAULT_TIMEOUT);
		client.setMaxTries(SftpImpl.DEFAULT_MAX_TRIES);
		
		client.connect();
	}
	
	
	/**
	 * This method : 
	 *  - Writes a random sequence of bytes in the given file
	 *  - Calcultates the md5 hash of the file
	 *  - Adds the file to the map with the hash as the key
	 * 
	 * @param file
	 */
	private void createRemoteFile(File file) {
		FileOutputStream out = null;
		try {
			// Writing in the file
			out = new FileOutputStream(file);
			byte[] buffer = new byte[100];
			Random rnd = new Random();
			rnd.nextBytes(buffer);
			out.write(buffer);
			
			// Getting the md5 hash 
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(buffer);
			String hash = new String(digest.digest());
			
			// Registering the file in the map
			remoteFiles.put(hash, file);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		finally {
			if (out != null)
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	
	/**
	 * Recursively removes the given directory and its children.
	 * @param file dir to remove
	 */
	private static void removeDir(File file) {
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			for (File f : files)
				removeDir(f);
		}
		file.delete();
	}

	/**
	 * Tears down test environment.
	 */
	@After
	public void tearDown() throws Exception {
		removeDir(new File(remoteDirectory));
		
		client.disconnect();
	}

	/**
	 * Tests the file listing method.
	 */
	@Test
	public void testListFiles() {
		Vector<RemoteFile> check = client.listFiles(remoteDirectory, "");
		// All files are listed
		assertEquals(remoteFiles.size(), check.size());
		// The files are the same
		Collection<File> files = remoteFiles.values();
		for (RemoteFile rf : check)
			assertTrue(contains(files, rf.getName()));
	}
	
	/**
	 * Tests whether a vector contains a file with the given filename.
	 * @param files file vector
	 * @param fileName file name to be found
	 * @return found ? 
	 */
	private static boolean contains(Collection<File> files, String fileName) {
		for (File f : files)
			if (f.getName().equals(fileName))
				return true;
		return false;
	}

	/**
	 * Tests the directory listing method.
	 */
	@Test
	public void testListDir() {
		Vector<RemoteFile> check = client.listDir(remoteDirectory, "");

		// All the directories are listed
		assertEquals(remoteDirectories.size(), check.size());
		// The files are the same
		for (RemoteFile rf : check)
			assertTrue(contains(remoteDirectories, rf.getName()));
	}

	/**
	 * Test the file retrieving method.
	 * Downloads each file and checks their md5 hash.
	 * 
	 * @throws BiomajException
	 */
	@Test
	public void testGetFileWithImpl() throws BiomajException {
		Set<String> keys = remoteFiles.keySet(); 
		for (String key : keys) {
			String fileName = remoteFiles.get(key).getName();
			/*
			 * The method we need to test is protected, so we use some
			 * reflection to access it. The method is identified by its name
			 * and its parameters type.
			 */
			Class<?>[] paramsType = new Class[]{String.class, String.class, String.class, String.class};
			try {
				// Method lookup
				Method toTest = client.getClass().getDeclaredMethod("getFileWithImpl", paramsType);
				// Access restriction suppression
				toTest.setAccessible(true);
				// Invocation
				Object[] paramsValue = new Object[]{remoteDirectory, fileName, localDirectory, fileName};
				toTest.invoke(client, paramsValue);
			} catch (SecurityException e) {
				System.err.println(e.getMessage());
			} catch (NoSuchMethodException e) {
				System.err.println(e.getMessage());
			} catch (IllegalArgumentException e) {
				System.err.println(e.getMessage());
			} catch (IllegalAccessException e) {
				System.err.println(e.getMessage());
			} catch (InvocationTargetException e) {
				System.err.println(e.getMessage());
			}
			
			File test = new File(localDirectory + "/" + fileName);
			assertTrue(test.exists());
			assertTrue(sameHash(key, test));
		}
	}
	
	/**
	 * Tests whether the hash of the file is the same as
	 * the given one.
	 * 
	 * @param hash reference hash
	 * @param file file whose hash has to be checked
	 * @return same hash ?
	 */
	private boolean sameHash(String hash, File file) {
		byte[] buffer = new byte[(int)file.length()];
		FileInputStream in = null;
		try {
			in = new FileInputStream(file);
			in.read(buffer);
			
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(buffer);
			String foo = new String(digest.digest());
			return foo.equals(hash);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null)
					in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return false;
	}

}
