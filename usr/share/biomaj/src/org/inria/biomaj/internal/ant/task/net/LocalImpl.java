/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.internal.ant.task.net;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

public class LocalImpl extends RemoteCommandImpl {

//	  private ArrayList<File> listFiles = new ArrayList<File>() ;
	
	public LocalImpl(Task task) {
		super(task);
	}

	  
	@Override
	protected boolean getFileWithImpl(String remoteDirectory, String nameFile, String targetDirectory, String targetName) throws BiomajException {
		try {
			BiomajUtils.copy(new File(remoteDirectory,nameFile),new File(targetDirectory,targetName));
			return true;
		} catch (IOException ioe) {
			log(ioe.getMessage(),Project.MSG_WARN);
			return false;
		}
	}


	@Override
	public void init(String server, Integer port, String username, String password) throws BiomajBuildException {
		// TODO Auto-generated method stub
		super.init(server,port,username,password);
	}
  
	

	@Override
	public void disconnect() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Vector<RemoteFile> listDir(String base, String directoryToApplyLs) throws BiomajBuildException {
		File dir = new File (base,directoryToApplyLs);
		Vector<RemoteFile> res = new Vector<RemoteFile>();
		if (!dir.exists()) {
			log(dir.getAbsolutePath()+" does not exist!",Project.MSG_INFO);
			return res;
		}
		
		File[] list = dir.listFiles();
		
		for (int i=0;i<list.length;i++) {
			if (list[i].isDirectory()) {
				res.add(new RemoteFile(directoryToApplyLs,list[i]));
			}
		}
		return res;
	}


	@Override
	public Vector<RemoteFile> listFiles(String base, String directoryToApplyLs) throws BiomajBuildException {
		File dir = new File (base,directoryToApplyLs);
		Vector<RemoteFile> res = new Vector<RemoteFile>();
		if (!dir.exists()) {
			log(dir.getAbsolutePath()+"does not exist!",Project.MSG_INFO);
			return res;
		}
		
		File[] list = dir.listFiles();
		
		for (int i=0;i<list.length;i++) {
			if (list[i].isFile()) {
				{
					res.add(new RemoteFile(directoryToApplyLs,list[i]));
					
				}
			}
		}
		return res;
	}
	
	@Override
	public List<RemoteFile> listAll(String base, String directoryToApplyLs)
			throws BiomajBuildException {
		
		File dir = new File(base,directoryToApplyLs);
		List<RemoteFile> res = new ArrayList<RemoteFile>();
		if (!dir.exists()) {
			log(dir.getAbsolutePath()+"does not exist!",Project.MSG_INFO);
			return res;
		}
		
		for (File f : dir.listFiles()) {
			res.add(new RemoteFile(directoryToApplyLs, f));	
		}
		
		return res;
	}
}
