/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.internal.ant.task.net;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * @author  ofilangi
 * @version  Biomaj 0.9
 * @since  Biomaj 0.8 /Citrina 0.5
 */
public class RemoteFile {

	public static final Pattern patRemoteFile = Pattern.compile("name=([\\S]+),base=([\\S]*),link=([\\S]*),date=("+BiomajUtils.REGEXP_DATE_FORMAT_IN_USE+"),size=([\\d]+),isDir=([\\w]+)");
	/**
	 * @uml.property  name="name"
	 * @uml.associationEnd  
	 */
	private String name ="";

	/**
	 * la base est la somme des repertoires qui  matche avec l'expression reguliere remote.files
	 * @uml.property  name="base"
	 */
	private String base ="";
	/**
	 * @uml.property  name="linkName"
	 */
	private String linkName ="";

	/**
	 * @uml.property  name="date"
	 */
	private Date   date = new Date(0);
	/**
	 * @uml.property  name="size"
	 */
	private Long   size = new Long(-1);

	/**
	 * @uml.property  name="isDir"
	 */
	private boolean isDir = false;

	public RemoteFile(String base,FTPClient client,FTPFile file) throws BiomajException {
		setAttributes(client,file);
		setBase(base);
	}

	/**
	 * @param base
	 * @param file
	 */
	public RemoteFile(String base,File file) {
		this.base = base;
		name = file.getName();
		date = new Date(file.lastModified());
		size = file.length();
		isDir = file.isDirectory();		
	}

	public RemoteFile(String lineToParse) throws BiomajException,ParseException {
		Matcher match = patRemoteFile.matcher(lineToParse);
		if (!match.find()) {
			BiomajLogger.getInstance().log("Can't parse Remote File with this line:"+lineToParse);
			throw new BiomajException("remote.file.error","Can't parse Remote File with this line:"+lineToParse);
		}
		
		name = match.group(1);
		base = match.group(2);
		linkName = match.group(3);
		date = BiomajUtils.stringToDate(match.group(4));
		size = Long.valueOf(match.group(5));
		isDir = Boolean.valueOf(match.group(6));
	}


	public RemoteFile() {

	}

	@Override
	public String toString() {
		return "name="+name+",base="+base+",link="+linkName+",date="+BiomajUtils.dateToString(date, Locale.US)+",size="+Long.toString(size)+",isDir="+Boolean.toString(isDir)+";";
	}


	public void setAttributes(FTPClient client,FTPFile file) throws BiomajException  {


		if (file.isSymbolicLink()){
			linkName = file.getLink();
			try {
				if(!FTPReply.isPositiveCompletion(client.getReplyCode())) {
					throw new BiomajException("ftp.error",client.getReplyString());
				}
				FTPFile[] l = client.listFiles(BiomajUtils.getRelativeDirectory(file.getLink()));
				FTPFile f = null;
				for (FTPFile i :l)
					if (i.getName().compareTo(BiomajUtils.getNameFile(file.getLink()))==0)
						{
						f = i;
						break;
						}
				
				if (f == null) {
					//On a pas reussi a trouver le link.....
					date = new Date(file.getTimestamp().getTime().getTime());
					size = file.getSize();
				} else {
					size = f.getSize();
					date = new Date(f.getTimestamp().getTime().getTime());
				}
			} catch (IOException ioe) {
				BiomajLogger.getInstance().log(ioe);
				throw new BiomajException("ftp.error",ioe.getMessage());
			}
		} else {
			date = new Date(file.getTimestamp().getTime().getTime());
			size = file.getSize();
		}
		isDir = file.isDirectory();
		name = file.getName();
	}


	/**
	 * @return  the date
	 * @uml.property  name="date"
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date  the date to set
	 * @uml.property  name="date"
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return  the name
	 * @uml.property  name="name"
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name  the name to set
	 * @uml.property  name="name"
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return  the size
	 * @uml.property  name="size"
	 */
	public Long getSize() {
		return size;
	}

	/**
	 * @param size  the size to set
	 * @uml.property  name="size"
	 */
	public void setSize(Long size) {
		this.size = size;
	}

	/**
	 * @return  the linkName
	 * @uml.property  name="linkName"
	 */
	public String getLinkName() {
		if (getBase().compareTo("")!=0)
			return getBase()+"/"+linkName;

		return linkName;
	}

	/**
	 * @param linkName  the linkName to set
	 * @uml.property  name="linkName"
	 */
	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	public boolean isLink() {
		return (this.linkName.compareTo("")!=0);
	}

	/**
	 * @return
	 * @uml.property  name="isDir"
	 */
	public boolean isDir() {
		return isDir;
	}

	/**
	 * @param isDir  the isDir to set
	 * @uml.property  name="isDir"
	 */
	public void setDir(boolean isDir) {
		this.isDir = isDir;
	}

	/**
	 * @return  the base
	 * @uml.property  name="base"
	 */
	public String getBase() {
		return base;
	}

	public String getAbsolutePath() {
		if (getBase().compareTo("")!=0)
			return getBase()+"/"+getName();
		return getName();
	}

	/**
	 * @param base  the base to set
	 * @uml.property  name="base"
	 */
	public void setBase(String base) {
		this.base = base;
	}


}
