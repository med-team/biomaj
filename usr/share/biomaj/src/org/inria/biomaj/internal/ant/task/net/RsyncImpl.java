/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.internal.ant.task.net;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.List;
import java.util.Vector;
import java.util.regex.Pattern;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.Execute;
import org.apache.tools.ant.taskdefs.LogStreamHandler;
import org.apache.tools.ant.types.Commandline;
import org.inria.biomaj.ant.task.net.RSyncListingParser;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajException;

public class RsyncImpl extends RemoteCommandImpl {

	/**
	 * @uml.property  name="server"
	 */
	private String server;
	/**
	 * @uml.property  name="username"
	 */
	private String username;
	/**
	 * @uml.property  name="password"
	 */
	private String password;

	public RsyncImpl(Task task) {
		super(task);
	}


	@Override
	public void init  (
			String server,
			Integer port, 
			String username, 
			String password) throws BiomajBuildException {
		super.init(server,port,username,password);
		setServer(server);
		setUsername(username);
		setPassword(password);
	}



	/*

	public void getListingFiles (String directory, String listingFile,String regexp, String regexpExcluded)  throws BiomajException {
		InputValidation.checkString(getTask().getProject(),directory, "remote directory");
		InputValidation.checkString(getTask().getProject(),listingFile, "the listing file");
		InputValidation.checkString(getTask().getProject(),regexp, "the remote file regular expression");

		directory =  checkRemoteDirectory(directory) ;

		try {
		BufferedWriter bw = new BufferedWriter(new FileWriter(listingFile));  

		Vector<String> listing = getOuptutListingRsync(directory);
		log("nb file in remote directory:"+listing.size(),Project.MSG_DEBUG);
		for (int i=0;i<listing.size();i++) {

			RSyncListingParser lp = new RSyncListingParser(getTask().getProject());
			lp.setLine(listing.get(i));
			lp.parse();
			String nameFile = lp.getFileName();
			String[] regexps = regexp.split("\\s");

			for (int j=0;j<regexps.length;j++) {
				Pattern p = Pattern.compile(regexps[j]);
				log("regexp:"+regexps[j],Project.MSG_DEBUG);
				log("name:"+nameFile,Project.MSG_DEBUG);
				if (p.matcher(nameFile).find())
					{
					if (checkWithExcludedFiles(regexpExcluded,listing.get(i))) {
						log("ok match!"+regexps[j],Project.MSG_DEBUG);
						bw.write(listing.get(i)+"\n");
					} else {
						log(listing.get(i) + " match with remote.files but is excluded by:"+regexpExcluded, Project.MSG_VERBOSE);
					}
					} else {
						log("no match!",Project.MSG_DEBUG);
					}
			}

		}
		 bw.flush();
         bw.close();

		 } catch(IOException ioe) {
	            throw new BiomajException("remote.error.connect",ioe.getMessage());
	     } catch (Exception e) {
	    	 throw new BiomajException("unknown.error",e.getMessage());
	     }

	}
	 */

	@Override
	public void disconnect() {
		// TODO Auto-generated method stub

	}


	public Vector<RemoteFile> getFilesRegExp(String remoteDirectory,String regexp) throws BiomajException {
		Vector<RemoteFile> result = new Vector<RemoteFile>();
		remoteDirectory =  checkRemoteDirectory(remoteDirectory) ;

		Vector<String> listing = getOuptutListingRsync(remoteDirectory);
		for (int i=0;i<listing.size();i++) {
			try {
				RSyncListingParser lp = new RSyncListingParser(getTask().getProject());
				lp.setLine(listing.get(i));
				lp.parse();
				RemoteFile file = lp.getRemoteFile();
				Pattern p = Pattern.compile(regexp);
				log("regexp:"+regexp,Project.MSG_DEBUG);
				if (p.matcher(file.getName()).find())
				{
					log("ok match!"+regexp,Project.MSG_DEBUG);
					result.add(file);
				}
			} catch (ParseException e) {
				throw new BiomajBuildException(getTask().getProject(),"io.error",e.getMessage(),e);
			}
		}
		return result;
	}


	@Override
	protected boolean getFileWithImpl(String remoteDirectory,String nameFile,String targetDirectory,String targetName) throws BiomajException {
		remoteDirectory =  checkRemoteDirectory(remoteDirectory) ;
		String cmd = "rsync://"+getServer()+remoteDirectory+nameFile;

		try {
			Commandline mycmd = new Commandline();
			String rsync = BiomajInformation.getInstance().getProperty(BiomajInformation.RSYNC);
	       
			mycmd.setExecutable(rsync);
			mycmd.createArgument().setValue(cmd);
			mycmd.createArgument().setValue(targetDirectory.trim()+"/"+targetName);
			//Process proc = Runtime.getRuntime().exec(cmd);
			//proc.waitFor();
			Execute exe = new Execute(new LogStreamHandler(getTask(), Project.MSG_INFO,Project.MSG_ERR),null);
			exe.setCommandline(mycmd.getCommandline());
			exe.execute();	

			int code_retour = exe.getExitValue();

			if (code_retour!=0) {
				log("file:"+cmd,Project.MSG_WARN);
				log("code retour:"+code_retour,Project.MSG_WARN);	
				return false;
			}

		} catch (Throwable t) {
			log("Error with rsync!",Project.MSG_DEBUG);
			log(t.getStackTrace().toString(),Project.MSG_ERR);
			throw new BuildException(t);
		}

		return true;
	}

	/**
	 * Get a vector of each output line with the command rsync
	 * @param directory
	 * @param regexp
	 * @return
	 */
	protected Vector<String> getOuptutListingRsync(String directory) throws BiomajException {
		log("getOuptutListingRsync",Project.MSG_DEBUG);
		Vector<String> result = new Vector<String>();

		String rsync = BiomajInformation.getInstance().getProperty(BiomajInformation.RSYNC);
		
		String cmd = rsync+" -t rsync://"+getServer()+directory ;

		try {

			Process proc = Runtime.getRuntime().exec(cmd);
			BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String line ;
			while((line = br.readLine())!= null)
				result.add(line);

		} catch (Throwable t) {
			log("Error with rsync!",Project.MSG_ERR);
			BiomajLogger.getInstance().log(t.getMessage());
			throw new BuildException(t);
		}
		return result;
	}

	/**
	 * @return  the password
	 * @uml.property  name="password"
	 */
	public String getPassword() {
		return password;
	}


	/**
	 * @param password  the password to set
	 * @uml.property  name="password"
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	/**
	 * @return  the server
	 * @uml.property  name="server"
	 */
	public String getServer() {
		return server;
	}


	/**
	 * @param server  the server to set
	 * @uml.property  name="server"
	 */
	public void setServer(String server) {
		this.server = server;
	}


	/**
	 * @return  the username
	 * @uml.property  name="username"
	 */
	public String getUsername() {
		return username;
	}


	/**
	 * @param username  the username to set
	 * @uml.property  name="username"
	 */
	public void setUsername(String username) {
		this.username = username;
	}


	private Vector<RemoteFile> list(String base, String directoryToApplyLs,boolean dir, boolean matchAll) throws BiomajBuildException {
		Vector<RemoteFile> result = new Vector<RemoteFile>();
		String where  =  checkRemoteDirectory(base+"/"+directoryToApplyLs) ;
		try {
		Vector<String> listing = getOuptutListingRsync(where);
		RSyncListingParser lp = new RSyncListingParser(getTask().getProject());

		for (int i=0;i<listing.size();i++) {
			try {
				String line = listing.get(i);

				String regex = "^(d|-)r(w|-).*";
				if ( !line.matches(regex) )
					continue;

				lp.setLine(line);
				lp.parse();
				RemoteFile file = lp.getRemoteFile();
				file.setBase(directoryToApplyLs);
				if (matchAll || file.isDir()==dir) {
					result.add(file);
				}
			} catch (ParseException e) {
				throw new BiomajBuildException(getTask().getProject(),"io.error",e.getMessage(),e);
			}
		}
		} catch (BiomajException be) {
			throw new BiomajBuildException(getTask().getProject(),be);
		}
		return result;
	}

	@Override
	public Vector<RemoteFile> listDir(String base, String directoryToApplyLs) throws BiomajBuildException {
		return list(base,directoryToApplyLs,true, false);
	}


	@Override
	public Vector<RemoteFile> listFiles(String base, String directoryToApplyLs) throws BiomajBuildException {
		return list(base,directoryToApplyLs,false, false);
	}
	
	@Override
	public List<RemoteFile> listAll(String base, String directoryToApplyLs)
			throws BiomajBuildException {
		return list(base, directoryToApplyLs, false, true);
	}

}

