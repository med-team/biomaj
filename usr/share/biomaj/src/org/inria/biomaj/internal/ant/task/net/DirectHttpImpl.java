package org.inria.biomaj.internal.ant.task.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URLConnection;
import java.util.List;
import java.util.Vector;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajException;

public class DirectHttpImpl extends RemoteCommandImpl {

	private String url;
	private String method;
	private String parameters;
	
	public static String GET = "GET";
	public static String POST = "POST";
	
	public DirectHttpImpl(Task task, String url, String method, String parameters) {
		super(task);
		this.url = url.replaceFirst("http://", "");
		this.method = method;
		this.parameters = parameters;
	}
	
	@Override
	public void init(String server, Integer port, final String username,
			final String password) throws BiomajBuildException {
		
		super.init(server,port,username,password);
		
		Authenticator.setDefault(new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password.toCharArray());
			}
		});
	}

	@Override
	public void disconnect() {
		
	}

	@Override
	protected boolean getFileWithImpl(String remoteDirectory, String nameFile,
			String targetDirectory, String targetName) throws BiomajException {
		
		try {
			
			String _url = url;
			if (method.equalsIgnoreCase(GET) && !parameters.trim().isEmpty())
				_url = url + "?" + parameters;
			
			URLConnection cx = HttpImpl.connectToURLThroughProxyIfNeeded("http://" + _url);
			if (method.equalsIgnoreCase(POST)) {
				cx.setDoOutput(true);
				OutputStreamWriter osw = new OutputStreamWriter(cx.getOutputStream());
				osw.write(this.parameters);
				osw.flush();
				osw.close();
			}
			cx.connect();
			DataInputStream  dis = new DataInputStream(cx.getInputStream());

			FileOutputStream fileStream = new FileOutputStream(targetDirectory+"/"+targetName);
			DataOutputStream dos = new DataOutputStream(fileStream);

			// recovery of the html document into the variable inputLine
			log("File is writing:"+targetDirectory+"/"+targetName,Project.MSG_DEBUG);
			byte[] b = new byte[1024];
			int nbBytes = 0;
			while ( ( nbBytes = dis.read(b)) != -1)
				dos.write(b,0,nbBytes);

			log("File written!",Project.MSG_DEBUG);
			dis.close () ;
			dos.close() ;

		}
		catch (FileNotFoundException fe) {
			return false;
		}
		catch (Exception ex) {
			throw new BiomajBuildException(getTask().getProject(),ex);
		}
		return true;
	}
	
	public String getParameters() {
		return this.parameters;
	}


	/* 
	 * Not used
	 * (non-Javadoc)
	 * @see org.inria.biomaj.internal.ant.task.net.RemoteCommandImpl#listDir(java.lang.String, java.lang.String)
	 */
	@Override
	public Vector<RemoteFile> listDir(String base, String directoryToApplyLs)
			throws BiomajBuildException {
		return null;
	}

	/*
	 * Not used
	 * (non-Javadoc)
	 * @see org.inria.biomaj.internal.ant.task.net.RemoteCommandImpl#listFiles(java.lang.String, java.lang.String)
	 */
	@Override
	public Vector<RemoteFile> listFiles(String base, String directoryToApplyLs)
			throws BiomajBuildException {
		return null;
	}
	
	/*
	 * Not used
	 * (non-Javadoc)
	 * @see org.inria.biomaj.internal.ant.task.net.RemoteCommandImpl#listAll(java.lang.String, java.lang.String)
	 */
	@Override
	public List<RemoteFile> listAll(String base, String directoryToApplyLs)
			throws BiomajBuildException {
		return null;
	}

}
