package org.inria.biomaj.internal.ant.task.net;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.ChannelSftp.LsEntry;

/**
 * Implementation of the sftp protocol based on the
 * JSch library (JCraft implementation of ssh2).
 * 
 * @author rsabas
 * 
 */
public class SftpImpl extends RemoteCommandImpl {

	public static final int DEFAULT_TIMEOUT = 100000;
	public static final int DEFAULT_MAX_TRIES = 5;

	/**
	 * Server address
	 */
	private String server;

	/**
	 * Listening port
	 */
	private int port;

	/**
	 * User login
	 */
	private String username;

	/**
	 * User password
	 */
	private String password;

	/**
	 * Sftp connexion
	 */
	private ChannelSftp sftpChannel;

	/**
	 * User defined timeout (DEFAULT_TIMEOUT if none)
	 */
	private int timeout;
	
	/**
	 * User defined max connection tries number
	 */
	private int maxTries;
	
	public SftpImpl(Task task) {
		super(task);
	}

	/**
	 * Initializes the remote host and connection parameters.
	 */
	@Override
	public void init(String server, Integer port, String username,
			String password) throws BiomajBuildException {
		this.server = server;
		this.port = port;
		this.username = username;
		this.password = password;

		// Initialisation timeout
		if (getTask().getProject().getProperties().containsKey(BiomajConst.ftpTimeOut)) {
			String ftpTimeout = getTask().getProject().getProperty(BiomajConst.ftpTimeOut);
			try {
				timeout = Integer.valueOf(ftpTimeout);

				if ((timeout != -1) && (timeout < 0)) {
					log("Bad value for [" + BiomajConst.ftpTimeOut + ":" + ftpTimeout + "] has to be positive or -1.", Project.MSG_WARN);
					timeout = DEFAULT_TIMEOUT;
				}

			} catch (NumberFormatException nf) {
				log("Bad value for [" + BiomajConst.ftpTimeOut + ":" + ftpTimeout + "]", Project.MSG_WARN);
				timeout = DEFAULT_TIMEOUT;
			}
		}

		// Initialisation tentatives connexion
		if (getTask().getProject().getProperties().containsKey(BiomajConst.ftpTriesConnexion)) {
			String tries = getTask().getProject().getProperty(BiomajConst.ftpTriesConnexion);
			try {
				maxTries = Integer.valueOf(tries);

				if (maxTries < 0) {
					log("Bad value for [" + BiomajConst.ftpTriesConnexion + ":" + tries + "] has to be positive.", Project.MSG_WARN);
					maxTries = DEFAULT_MAX_TRIES;
				}

			} catch (NumberFormatException nf) {
				log("Bad value for [" + BiomajConst.ftpTriesConnexion + ":" + tries + "]", Project.MSG_WARN);
				maxTries = DEFAULT_MAX_TRIES;
			}
		}

		connect();

	}

	/**
	 * Connects to the server and opens an sftp channel.
	 * 
	 */
	public void connect() {
		JSch secureChannel = new JSch();
		boolean fine = false;
		Exception ex = null;
		for (int tries = 0; tries < maxTries && !fine; tries++) {
			try {
				Session sftpSession = secureChannel.getSession(username, server, port);
//				sftpSession.setTimeout(timeout);
				sftpSession.setPassword(password);
				sftpSession.setConfig("StrictHostKeyChecking", "no");
				sftpSession.connect();
				sftpChannel = (ChannelSftp) sftpSession.openChannel("sftp");
				sftpChannel.connect();
				fine = true;
			} catch (JSchException e) {
				// Pas d'exception specialisees dans Jsch donc pas moyen d'identifier
				// le type de probleme :(
				log(e.getMessage(), Project.MSG_WARN);
				ex = e;
			}
		}
		if (!fine)
			throw new BiomajBuildException(getTask().getProject(), "remote.error.autoreconnect", Integer.toString(maxTries), ex);
	}

	@Override
	public void disconnect() {
		if (sftpChannel != null) {
			sftpChannel.disconnect();
			try {
				sftpChannel.getSession().disconnect();
			} catch (JSchException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * Retrieves the remote file denoted by its directory and name.
	 * 
	 * @param remoteDirectory
	 *            remote directory
	 * @param nameFile
	 *            remote file name
	 * @param targetDirectory
	 *            local directory
	 * @param targetName
	 *            local name
	 */
	@Override
	protected boolean getFileWithImpl(String remoteDirectory, String nameFile,
			String targetDirectory, String targetName)
			throws BiomajBuildException {

		try {
			sftpChannel.cd(remoteDirectory);
			FileOutputStream out = new FileOutputStream(targetDirectory + "/" + targetName);
			sftpChannel.get(nameFile, out);
			out.close();

			return true;
		} catch (SftpException e) {
			throw new BiomajBuildException(getTask().getProject(),
					"sftp.error.remoteCommand", e);
		} catch (FileNotFoundException e) {
			throw new BiomajBuildException(getTask().getProject(),
					"sftp.error.io.fileNotFound", e);
		} catch (IOException e) {
			throw new BiomajBuildException(getTask().getProject(), e);
		}
	}
	
	@Override
	public List<RemoteFile> listAll(String base, String directoryToApplyLs)
			throws BiomajBuildException {
		
		return list(base, directoryToApplyLs, false, true);
	}
	
	/**
	 * Lists the files or directories in the given directory (base + directoryToApplyLs)
	 * 
	 * @param base base directory
	 * @param directoryToApplyLs directory to be listed in the base directory
	 * @param isDir whether we list files or directory
	 * @param matchAll if true accept file AND directory
	 * 
	 * @return Listed items
	 * @throws BiomajBuildException
	 */
	private Vector<RemoteFile> list(String base, String directoryToApplyLs, boolean isDir, boolean matchAll) throws BiomajBuildException {
		
		String workingDirectory = base + "/" + directoryToApplyLs;
		Vector<RemoteFile> files = new Vector<RemoteFile>();
		final int maxTry = 2;
		int errorCount = 0;
		while (errorCount < maxTry) {
			try {
				// Directory listing
				Vector<?> entries = sftpChannel.ls(workingDirectory);
				for (int i = 0; i < entries.size(); i++) {
					LsEntry entry = (LsEntry) entries.get(i);
					if ((matchAll || entry.getAttrs().isDir() == isDir) && 
							!entry.getFilename().equals(".") && !entry.getFilename().equals("..")) {
						// Setting the remote file properties
						RemoteFile file = new RemoteFile();
						file.setName(entry.getFilename());
						file.setBase(directoryToApplyLs);
						file.setDate(new Date((long)entry.getAttrs().getMTime() * 1000));
						
						boolean _isDir = entry.getAttrs().isDir(); // If match all
						file.setDir(_isDir);
						file.setSize(_isDir ? 0 : entry.getAttrs().getSize());
						
						files.add(file);
					}
				}
				break; // Exit the loop if no error
			} catch (SftpException e) {
				log(e.getMessage(), Project.MSG_WARN);
				if (++errorCount < maxTry) {
					log("Trying to reconnect", Project.MSG_DEBUG);
					disconnect();
					connect();
				} else {
					throw new BiomajBuildException(getTask().getProject(), e);
				}
			}
		}
		return files;
	}

	/**
	 * Lists the directories in the given directory.
	 */
	@Override
	public Vector<RemoteFile> listDir(String base, String directoryToApplyLs)
			throws BiomajBuildException {
		return list(base, directoryToApplyLs, true, false);
	}

	/**
	 * Lists the files in the given directory.
	 */
	@Override
	public Vector<RemoteFile> listFiles(String base, String directoryToApplyLs)
			throws BiomajBuildException {
		return list(base, directoryToApplyLs, false, false);
	}
	
	/*
	 * GETTERS // SETTERS
	 */

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public int getMaxTries() {
		return maxTries;
	}

	public void setMaxTries(int maxTries) {
		this.maxTries = maxTries;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

}
