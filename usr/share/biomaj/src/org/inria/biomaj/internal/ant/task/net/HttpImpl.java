/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.internal.ant.task.net;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.util.List;
import java.util.Vector;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.gmod.biomaj.ant.task.InputValidation;
import org.inria.biomaj.ant.task.net.HttpListingParser;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;


public class HttpImpl extends RemoteCommandImpl {
	
	/**
	 * @uml.property  name="httpServer"
	 */
	private String httpServer;
	
	
	public HttpImpl(Task task) {
		super(task);
	}

	@Override
	public void init  (
			String server,
			Integer port, 
			final String username, 
			final String password) throws BiomajBuildException {
		super.init(server,port,username,password);
		
		Authenticator.setDefault(new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password.toCharArray());
			}
		});
		
		httpServer = server;
		//Correction OFI 08/06/2007 - on enleve http:// 
		httpServer =  httpServer.replace("http://","");
		
		//les proprietes de filtrage pour le protocol http doivent etre positionne
		InputValidation.checkString(getTask().getProject(), BiomajConst.httpParseDirLineProperty, "Describe how to parse a directory line in the html format on server.");
		InputValidation.checkString(getTask().getProject(), BiomajConst.httpParseFileLineProperty, "Describe how to parse a file line in the html format on server.");
		InputValidation.checkString(getTask().getProject(), BiomajConst.httpGroupDirName, "Group where name of directory is in "+BiomajConst.httpParseDirLineProperty);
		InputValidation.checkString(getTask().getProject(), BiomajConst.httpGroupDirDate, "Group where date of directory is in "+BiomajConst.httpParseDirLineProperty);
		InputValidation.checkString(getTask().getProject(), BiomajConst.httpGroupFileName, "Group where name of file is in "+BiomajConst.httpParseDirLineProperty);
		InputValidation.checkString(getTask().getProject(), BiomajConst.httpGroupFileDate, "Group where date of file is in "+BiomajConst.httpParseDirLineProperty);
		InputValidation.checkString(getTask().getProject(), BiomajConst.httpGroupFileSize, "Group where size of file is in "+BiomajConst.httpParseDirLineProperty);
		
	}
	
	
	
	@Override
	public void disconnect() {
	}

	/**
	 * Get a remote file
	 * @param remoteDirectory   : directory to find file
	 * @param nameFile          : file to download
	 * @param targetDirectory   : local directory where the file will be stored
	 * @param targetName        : file name in the local directory 
	 * @return
	 */
	@Override
	protected boolean getFileWithImpl(String remoteDirectory,String nameFile,String targetDirectory,String targetName) throws BiomajException{

		try {
			
			URLConnection cx = connectToURLThroughProxyIfNeeded("http://" + httpServer + remoteDirectory + "/" + nameFile);
			cx.connect();

			DataInputStream  dis = new DataInputStream(cx.getInputStream());

			FileOutputStream fileStream = new FileOutputStream(targetDirectory+"/"+targetName);
			DataOutputStream dos = new DataOutputStream(fileStream);

			// recovery of the html document into the variable inputLine
			log("File is writing:"+targetDirectory+"/"+targetName,Project.MSG_DEBUG);
			byte[] b = new byte[1024];
			int nbBytes = 0;
			while ( ( nbBytes = dis.read(b)) != -1)
				dos.write(b,0,nbBytes);

			log("File is write!",Project.MSG_DEBUG);
			dis.close () ;
			dos.close() ;

		}
		catch (FileNotFoundException fe) {
			return false;
		}
		catch (Exception ex) {
			throw new BiomajBuildException(getTask().getProject(),ex);
		}
		return true;
	}

	public Vector<RemoteFile> list(String base, String directoryToApplyLs,boolean directory, boolean matchAll) throws BiomajBuildException {
		Vector<RemoteFile> res = new Vector<RemoteFile>();
		BufferedReader in = null;
		try {
			Project p = getTask().getProject();
			URLConnection cx = connectToURLThroughProxyIfNeeded("http://" + httpServer + base + "/" + directoryToApplyLs);
			cx.connect();
			
			in = new BufferedReader(new InputStreamReader(cx.getInputStream()));
			
			String tmp = "" ;
			
			HttpListingParser hlp = new HttpListingParser(p.getProperty(BiomajConst.httpParseDirLineProperty),
					Integer.valueOf(p.getProperty(BiomajConst.httpGroupDirName)),Integer.valueOf(p.getProperty(BiomajConst.httpGroupDirDate)),"",
					p.getProperty(BiomajConst.httpParseFileLineProperty),Integer.valueOf(p.getProperty(BiomajConst.httpGroupFileName)),
					Integer.valueOf(p.getProperty(BiomajConst.httpGroupFileDate)),Integer.valueOf(p.getProperty(BiomajConst.httpGroupFileSize)),"");
			// recovery of the html document into the variable inputLine
			while ( (tmp = in.readLine ()) != null) {
				hlp.setLine(tmp);
				
				if (!hlp.parse())
					continue;
				
				if (matchAll || hlp.getRemoteFile().isDir()==directory) {
					RemoteFile rf = hlp.getRemoteFile();
					rf.setBase(directoryToApplyLs);
					res.add(rf);
				}
			}
			
		} catch (IOException e) {
			BiomajLogger.getInstance().log(e);
			throw new BiomajBuildException(getTask().getProject(),"io.error",e.getMessage(),e);
		} catch (ParseException e) {
			BiomajLogger.getInstance().log(e);
			throw new BiomajBuildException(getTask().getProject(),"io.error",e.getMessage(),e);
		} finally {
			try {
				if ((in != null))
					in.close();
			} catch (IOException ioe) {
				
			}
		}

		return res;
	}
	
	@Override
	public List<RemoteFile> listAll(String base, String directoryToApplyLs)
			throws BiomajBuildException {
		return list(base, directoryToApplyLs, false, true);
	}
	
	
	public static URLConnection connectToURLThroughProxyIfNeeded(String address) {
		
		URL url;
		try {
			url = new URL(address);
		} catch (MalformedURLException e1) {
			BiomajLogger.getInstance().log(e1);
			return null;
		}
		
		Proxy proxy = Proxy.NO_PROXY;
		
		if (System.getProperty("proxySet") != null && System.getProperty("proxySet").equals("true")) {
			String proxyHost = System.getProperty("socksProxyHost");
			int proxyPort = 1080;
			if (System.getProperty("socksProxyPort") != null)
				proxyPort = Integer.valueOf(System.getProperty("socksProxyPort"));
			
			if (System.getProperty("socksProxyUser") != null) {
				final String proxyUser = System.getProperty("socksProxyUser");
				final String proxyPassword = System.getProperty("socksProxyPassword");
				Authenticator.setDefault(new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(proxyUser, proxyPassword.toCharArray());
					}
				});
			}
			proxy = new Proxy(Proxy.Type.SOCKS, new InetSocketAddress(proxyHost, proxyPort));
		}
		
		try {
			HttpURLConnection cx = (HttpURLConnection) url.openConnection(proxy);
			return cx;
		} catch (IOException e) {
			BiomajLogger.getInstance().log(e);
		}
		
		return null;
	}



	@Override
	public Vector<RemoteFile> listDir(String base, String directoryToApplyLs) throws BiomajBuildException {
		return list(base,directoryToApplyLs,true, false);
	}

	@Override
	public Vector<RemoteFile> listFiles(String base, String directoryToApplyLs) throws BiomajBuildException {
		return list(base,directoryToApplyLs,false, false);
	}
	
	
	
}
