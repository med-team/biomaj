/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.internal.ant.task.net;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;
import java.util.regex.Pattern;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.gmod.biomaj.ant.task.InputValidation;
import org.inria.biomaj.ant.logger.BiomajConsoleLogger;
import org.inria.biomaj.ant.task.BmajExtract;
import org.inria.biomaj.ant.task.net.RemoteCommand;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;


abstract public class RemoteCommandImpl {

	/**
	 * @uml.property  name="task"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private Task task;

	/**
	 * @uml.property  name="bw"
	 */
	private BufferedWriter bw;

	/**
	 * @uml.property  name="server"
	 */
	private String server = "";
	
	/**
	 * UseFull to get a listing of Remote file which are ever request!
	 * @author  ofilangi
	 */
	private class ListingBuffer {
		private Collection<RemoteFile> listBuf = new Vector<RemoteFile>(); 
		/**
		 * @uml.property  name="remoteDirectory"
		 */
		private String remoteDirectory = "";
		/**
		 * @uml.property  name="remoteFiles"
		 */
		private String remoteFiles     = "";
		/**
		 * @uml.property  name="excludedFiles"
		 */
		private String excludedFiles   = "";
		/**
		 * @return  the excludedFiles
		 * @uml.property  name="excludedFiles"
		 */
		public String getExcludedFiles() {
			return excludedFiles;
		}
		/**
		 * @param excludedFiles  the excludedFiles to set
		 * @uml.property  name="excludedFiles"
		 */
		public void setExcludedFiles(String excludedFiles) {
			this.excludedFiles = excludedFiles;
		}
		/**
		 * @return  the listBuf
		 * @uml.property  name="listBuf"
		 */
		public Collection<RemoteFile> getListBuf() {
			return listBuf;
		}
		/**
		 * @param listBuf  the listBuf to set
		 * @uml.property  name="listBuf"
		 */
		public void setListBuf(Collection<RemoteFile> listBuf) {
			this.listBuf = listBuf;
		}
		/**
		 * @return  the remoteDirectory
		 * @uml.property  name="remoteDirectory"
		 */
		public String getRemoteDirectory() {
			return remoteDirectory;
		}
		/**
		 * @param remoteDirectory  the remoteDirectory to set
		 * @uml.property  name="remoteDirectory"
		 */
		public void setRemoteDirectory(String remoteDirectory) {
			this.remoteDirectory = remoteDirectory;
		}
		/**
		 * @return  the remoteFiles
		 * @uml.property  name="remoteFiles"
		 */
		public String getRemoteFiles() {
			return remoteFiles;
		}
		/**
		 * @param remoteFiles  the remoteFiles to set
		 * @uml.property  name="remoteFiles"
		 */
		public void setRemoteFiles(String remoteFiles) {
			this.remoteFiles = remoteFiles;
		}

	}

	/**
	 * Commun a toutes les instanciations de protocol pour la session biomaj courante
	 */
	private static HashMap<String, ListingBuffer> lBuffer = null;
	
	
	/**
	 * Constructor
	 * @param project
	 * @param task
	 */
	public RemoteCommandImpl(Task task) {
		setTask(task);

		
		if (lBuffer == null)
			lBuffer = new HashMap<String, ListingBuffer>();
	}

	/*
	 * Methode to redefine in specific protocol implementation
	 */
	public void init(String server, Integer port, String username, String password) throws BiomajBuildException {
		this.server = server;
	}

	abstract public void disconnect(); 

	abstract public Vector<RemoteFile> listFiles(String base,String directoryToApplyLs) throws BiomajBuildException ;
	abstract public Vector<RemoteFile> listDir(String base,String directoryToApplyLs) throws BiomajBuildException ;
	
	/**
	 * List all elements in given directory (files and directories).
	 * 
	 * @param base
	 * @param directoryToApplyLs
	 * @return
	 * @throws BiomajBuildException
	 */
	abstract public List<RemoteFile> listAll(String base, String directoryToApplyLs) throws BiomajBuildException;

	/*
	 * Generic Algo to get a remote listing file independently from the protocol
	 */

	/**
	 * 
	 * @param remoteDirectory
	 * @param listingFile
	 * @param remoteFiles
	 * @param regexpExcluded
	 * @throws BiomajException
	 * @throws BiomajBuildException
	 */

	public void getListingFilesWithBufferFile(String remoteDirectory, String listingFile,String remoteFiles, String regexpExcluded) throws BiomajBuildException {
		//InputValidation.checkString(getTask().getProject(),remoteDirectory, "remote directory");
		InputValidation.checkString(getTask().getProject(),listingFile, "the listing file");
		InputValidation.checkString(getTask().getProject(),remoteFiles, "the remote file regular expression");

		try {
			
			Collection<RemoteFile> resultat = new Vector<RemoteFile>() ;
			if (!bufferExist(remoteDirectory, remoteFiles, regexpExcluded)) {
				TreeMap<String,RemoteFile> res = new TreeMap<String,RemoteFile>();
				
				if (remoteFiles.equals(BiomajConst.regexpAll)) {
					listArborescence(remoteDirectory, "", remoteFiles, res, true);
				} else {
					String[] listExpression = remoteFiles.split("\\s");
					for (int i = 0; i < listExpression.length; i++) {
						log("Checking expression ["+listExpression[i]+"]...",Project.MSG_INFO);
						try {
							getFilesMatchWithRegularExpression(remoteDirectory, "", listExpression[i], regexpExcluded,res,true);
						} catch (Exception ex) {
							throw new BiomajBuildException(getTask().getProject(), ex);
						}
					}
				}
				resultat = res.values();
			} else {
				log("UN BUFFER EXIST!!",Project.MSG_DEBUG);
				resultat = getResultBuffer();
			}
			
			bw = new BufferedWriter(new FileWriter(listingFile));  
			writeListing(resultat);
			bw.close();
		} catch (IOException e) {
			throwExceptionBiomaj("io.error",e.getMessage());
		}
	}


	public Collection<RemoteFile> getListingFiles(String remoteDirectory,String remoteFiles, String regexpExcluded) throws BiomajBuildException {
		//	InputValidation.checkString(getTask().getProject(),remoteDirectory, "remote directory");
		//InputValidation.checkString(getTask().getProject(),listingFile, "the listing file");
		InputValidation.checkString(getTask().getProject(),remoteFiles, "the remote file regular expression");
		TreeMap<String,RemoteFile> res = new TreeMap<String,RemoteFile>();

		if (remoteFiles.equals(BiomajConst.regexpAll)) { // List all recursively
			listArborescence(remoteDirectory, "", regexpExcluded, res, true);
		} else {
			String[] listExpression = remoteFiles.trim().split("\\s+");
			
			for (int i = 0; i < listExpression.length; i++) {
				log("Checking expression ["+listExpression[i]+"]...",Project.MSG_INFO);
				
				try {
					getFilesMatchWithRegularExpression(remoteDirectory, "", listExpression[i], regexpExcluded,res,true);
				} catch (Exception ex) {
					ex.printStackTrace();
					throw new BiomajBuildException(getTask().getProject(), ex);
				}
			}
		}

		setResultBuffer(remoteDirectory, remoteFiles, regexpExcluded, res.values());

		return res.values();
	}
	
	protected void listArborescence(final String remoteDirectory, String relativePath, final String excludedFiles, Map<String, RemoteFile> res, boolean log) {
		if (log)
			log("Listing arboresence under '" + remoteDirectory + "/" + relativePath + "' omitting '" + excludedFiles + "'", Project.MSG_VERBOSE);
		
		List<RemoteFile> filz = listAll(remoteDirectory, relativePath);
		int total = filz.size();
		int  i = 1;
		for (RemoteFile file : filz) {
			
			if (log) {
				log(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE + "[" + (i++) + "/" + total + "]", Project.MSG_INFO);
			}
			
			if (match(file.getAbsolutePath(), ".*", excludedFiles)) {
				if (file.isDir()) {
					if (log)
						log("Listing sub dir : " + remoteDirectory, Project.MSG_VERBOSE);
					
					final String newPath = relativePath.trim().isEmpty() ? file.getName() : relativePath + "/" + file.getName();
					
					listArborescence(remoteDirectory, newPath, excludedFiles, res, log);
				} else {
					log("Adding to list :" + file.toString(), Project.MSG_VERBOSE);
					res.put(file.getAbsolutePath(), file);
				}
			}
		}
	}

	protected void getFilesMatchWithRegularExpression(final String remoteDirectory, final String path, final String regExpr, final String regExprExclu, final Map<String,RemoteFile> resultat, final boolean logPercent) throws BiomajBuildException {
		
		if (logPercent)
			log("Searching files in root directory....",Project.MSG_VERBOSE);

		if ( resultat == null ) {
			log("argument 'resulat' from getFilesMatchWithRegularExpression can't be set null",Project.MSG_ERR);
			throw new BiomajBuildException(getTask().getProject(),"",new Exception());
		}
		
		//Il n'y a pas de repertoire dans l'expression reguliere
		if (!regExpr.contains("/")) {
			if (!path.endsWith("/..") && !path.endsWith("/.") && !path.equals(".") && !path.equals("..")) {
				if (logPercent)
					log("Biomaj is creating a file list...",Project.MSG_VERBOSE);
				
				
				Vector<RemoteFile> lFiles = listFiles(remoteDirectory,path);
				int compteur = 0;
				for (RemoteFile f : lFiles) {
					if (logPercent) {
						float a = ((float)++compteur/ (float)lFiles.size())*100;
						log(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE+"["+Integer.toString((int)a)+"%]",Project.MSG_INFO);
					}
					//Correction bug O.F
					//if (match(f.getAbsolutePath(),regExpr,regExprExclu))
					if (match(f.getName(),regExpr,regExprExclu)){
						
						log(f.getName()+" is ok!  toString:"+f.toString(),Project.MSG_VERBOSE);
	
						//if (resultat!=null)
						if (!resultat.containsKey(f.getAbsolutePath())){
							resultat.put(f.getAbsolutePath(),f);
						}
					}
				}
			}
			return;
		}

		
		if (regExpr.equals(BiomajConst.regexpAll)) {
			listArborescence(remoteDirectory, path, regExprExclu, resultat, false);
			return;
		}
		
		//On decoupe en sous repertoire et fichier
		String[] decomposeDirectory = regExpr.split("/");
		// File regexp
		final String newRegExpr = regExpr.substring(decomposeDirectory[0].length()+1);

		if (logPercent)
			log("Biomaj is creating a list of sub-directory...",Project.MSG_VERBOSE);

		if (!path.equals(".") && !path.equals("..")) {
			
			
			Vector<RemoteFile> lDirs = listDir(remoteDirectory,path);
			
			
			if (logPercent)
				log("Searching files in sub-directory...",Project.MSG_VERBOSE);
	
			int compteur = 0;
			int total = lDirs.size() * 100;
			for (RemoteFile d : lDirs) {
				
				compteur++;
				if (match(d.getName(),decomposeDirectory[0],regExprExclu)) {
					if (logPercent) {
						float a = (float) (compteur/ total);
						log(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE+"["+Integer.toString((int)a)+"%]",Project.MSG_INFO);
					}
					log("Expr:"+regExpr,Project.MSG_VERBOSE);
					log(d.getAbsolutePath()+" match with "+decomposeDirectory[0],Project.MSG_VERBOSE);
					log("newExpr:"+newRegExpr,Project.MSG_VERBOSE);
					
					final String newPath = path.trim().isEmpty() ? d.getName() : path + "/" + d.getName();
					getFilesMatchWithRegularExpression(remoteDirectory, newPath, newRegExpr, regExprExclu, resultat, false);
//					if (registeredException.get(bank) == null) {
//						startThreadedListing(false, logPercent, newRegExpr, resultat, regExprExclu, newPath, remoteDirectory);
//					} else {
//						throw new BiomajBuildException(task.getProject(), registeredException.get(bank));
//					}
				}
			}
		}
	}
	
	private boolean match(String filePath, String expr, String exprExc) {
		final Pattern p = Pattern.compile(expr);
		boolean exclu = false;
		//log("filePath:"+filePath+"  expr:"+expr+" exprExclu:"+exprExc,Project.MSG_INFO);
		if (exprExc.compareTo("")!=0) {
			String[] exclusions = exprExc.split("\\s");
			for (int i=0;i<exclusions.length;i++) {
				Pattern pExclu = Pattern.compile(exclusions[i].trim());
				exclu = exclu || pExclu.matcher(filePath).find();
			}
		}

		if (p.matcher(filePath).matches() && !exclu) {
			log(filePath+" match with:"+expr,Project.MSG_VERBOSE);
			return true;
		}

		log(filePath+" not match! with:"+expr +" exclude:"+Boolean.toString(exclu),Project.MSG_VERBOSE);
		return false;
	}

	/*
	 * End Algo
	 */

	abstract protected boolean getFileWithImpl(String remoteDirectory,String nameFile,String targetDirectory,String targetName) 
	throws BiomajException;


	/**
	 * Get a remote file 
	 * @param remoteDirectory   : directory to find file
	 * @param nameFile          : file to download
	 * @param targetDirectory   : local directory where the file will be stored
	 * @param targetName        : file name in the local directory 
	 * @return
	 */
	public boolean getFile(String remoteDirectory,String nameFile,String targetDirectory,String targetName) throws BiomajException{
		
		if (BiomajUtils.getRelativeDirectory(targetName).compareTo("")!=0) {
			BiomajUtils.createSubDirectories(targetDirectory+"/"+BiomajUtils.getRelativeDirectory(targetName));
		}

		boolean ret = getFileWithImpl(remoteDirectory,nameFile,targetDirectory,targetName);
		
		if (!ret)
			return ret;

		File f = new File(targetDirectory.trim()+"/"+targetName);

		//test checksum if file is zipped!
		//------------------------------

		if (BmajExtract.containsCompressedFormat(nameFile)) {
			if (BmajExtract.check(f,getTask())) {
				getTask().log("compressed file is check!",Project.MSG_VERBOSE);
			} else {
				getTask().log("compressed file is corrupted!",Project.MSG_VERBOSE);
				return false;
			}
		}

		return ret;

	}

	public static boolean getFile(Task task,String username,String password,String nameFileWithEntireUrl,String targetDirectory,String targetName) throws BiomajException {

		String protocol = getProtocolAttributRemoteConfig(nameFileWithEntireUrl);
		if (protocol == null)
			return false;

		String serveur =  getServeurAttributRemoteConfig(nameFileWithEntireUrl);

		if (serveur == null)
			return false;

		String filepath = getFilePathAttributRemoteConfig(nameFileWithEntireUrl);
		if (filepath == null)
			return false;
		if (BiomajUtils.getRelativeDirectory(filepath).compareTo("")!=0) {
			BiomajUtils.createSubDirectories(targetDirectory+"/"+BiomajUtils.getRelativeDirectory(filepath));
		}

		Integer port = getPortAttributRemoteConfig(nameFileWithEntireUrl);

		RemoteCommandImpl rci = null ;
		if (protocol.substring(0).compareTo(RemoteCommand.FTP_PROTOCOL)==0) {
			rci = new FtpImpl(task);
		} else if (protocol.substring(0).compareTo(RemoteCommand.HTTP_PROTOCOL)==0){
			rci = new HttpImpl(task);
		} else if (protocol.substring(0).compareTo(RemoteCommand.RSYNC_PROTOCOL)==0){
			rci = new RsyncImpl(task);
		} else if (protocol.substring(0).compareTo(RemoteCommand.LOCAL_PROTOCOL)==0){
			rci = new LocalImpl(task);
		} 

		//On ne specifie pas le password et le username
		rci.init(serveur,port,username,password);
		boolean ret = rci.getFileWithImpl("", filepath, targetDirectory, targetName);
		rci.disconnect();
		return ret;
	}

	/**
	 * Get a remote file and uncompressed this file
	 * @param remoteDirectory   : directory to find file
	 * @param nameFile          : file to download
	 * @param targetDirectory   : local directory where the file will be stored
	 * @return the name file
	 */
	public String getFileUncompressed(String remoteDirectory,String nameFile,String targetDirectory) throws BiomajException{
		if (getFile(remoteDirectory,nameFile,targetDirectory,nameFile)) {
			String newNameFile = BmajExtract.uncompressedFile(targetDirectory,nameFile,task);
			if (newNameFile.compareTo(nameFile)!=0){
				return newNameFile;
			} 
		}
		return nameFile;
	}

	public static String getFileUncompressed(Task t, String username,String password, String url,String targetDirectory) throws BiomajException{
		String[] res = url.split("/");
		if (getFile(t,username,password,url,targetDirectory,res[res.length-1])) {
			String newNameFile = BmajExtract.uncompressedFile(targetDirectory,res[res.length-1],t);
			if (newNameFile.compareTo(res[res.length-1])!=0){
				return newNameFile;
			} 
		}
		return res[res.length-1];
	}


	/**
	 * if there are not / at end of a directory, we add one!
	 * @param remoteDirectory
	 */
	protected String checkRemoteDirectory(String remoteDirectory) {
		//remoteDirectory = remoteDirectory.trim();
		if ((remoteDirectory.length()>0)&&(remoteDirectory.charAt(remoteDirectory.length()-1)!='/')) {
			//log("Remote directory has to finish by '/'",Project.MSG_VERBOSE);
			return remoteDirectory+"/";
		}
		return remoteDirectory;
	}

	/**
	 * @return  the task
	 * @uml.property  name="task"
	 */
	public Task getTask() {
		return task;
	}

	/**
	 * @param task  the task to set
	 * @uml.property  name="task"
	 */
	public void setTask(Task task) {
		this.task = task;
	}

	protected boolean checkWithExcludedFiles(String regExcluded, String filepath) {

		if ((regExcluded==null)||(regExcluded.trim().compareTo("")==0))
			return true;

		String[] lExcReg = regExcluded.split("\\s");

		for (int i=0; i<lExcReg.length;i++) {
			Pattern p = Pattern.compile(lExcReg[i]);
			if (p.matcher(filepath).find())
			{

				return false; //le fichier appartient a l expression reguliere
			}
		}

		return true;
	}

	public void log(String message,int level) {
		if (task!=null) {
			task.log(message,level);
		} else {
			System.out.println(level+":"+message);
		}

	}

	private void throwExceptionBiomaj(String key,String arg) throws BiomajBuildException {

		if (arg == null)
			arg = "";

		if (task == null) {
			throw new BiomajBuildException(null,key,arg,new Exception());
		}
		else
			throw new BiomajBuildException(task.getProject(),key,arg,new Exception());

	}

	private boolean bufferExist(String remoteDirectory, String remoteFiles, String regexpExcluded) {
		if (lBuffer.containsKey(server)) {
			ListingBuffer lb = lBuffer.get(server);
			log("remoteDirectory - old:["+lb.remoteDirectory+"] new:["+remoteDirectory+"]",Project.MSG_DEBUG);
			log("remoteFiles - old:["+lb.remoteFiles+"] new:["+remoteFiles+"]",Project.MSG_DEBUG);
			log("excludedFiles - old:["+lb.excludedFiles+"] new:["+regexpExcluded+"]",Project.MSG_DEBUG);

			return ((lb.remoteDirectory.compareTo(remoteDirectory)==0)&&
					(lb.remoteFiles.compareTo(remoteFiles)==0)&&
					(lb.excludedFiles.compareTo(regexpExcluded)==0));
		}
		log("server:["+server+"] is not present in buffer!",Project.MSG_DEBUG);
		return false;
	}

	private Collection<RemoteFile> getResultBuffer() throws  BiomajBuildException {
		if (lBuffer.containsKey(server)) {
			return lBuffer.get(server).getListBuf();
		}
		throw new BiomajBuildException(getTask().getProject(),"unknown.error","Erreur interne, mauvaise implementation du buffer : RemoteCommandImpl",new Exception());
	}

	private void setResultBuffer(String remoteDirectory, String remoteFiles, String regexpExcluded,Collection<RemoteFile> lFiles) {
		lBuffer.remove(server);
		ListingBuffer lb = new ListingBuffer();
		lb.setExcludedFiles(regexpExcluded);
		lb.setListBuf(lFiles);
		lb.setRemoteDirectory(remoteDirectory);
		lb.setRemoteFiles(remoteFiles);
		lBuffer.put(server, lb);
	}

	private void writeListing (Collection<RemoteFile> lFiles) throws  BiomajBuildException  {
		try {
			for (RemoteFile f : lFiles) {
				bw.write(f.toString());
				bw.newLine();
			}
		} catch (IOException ioe) {
			throwExceptionBiomaj("",bw.toString());
		}
	}


	public static String getProtocolAttributRemoteConfig(String url) {
		if (!url.contains("://"))
			return null;

		String[] res = url.split("://");
		String protocol = res[0] ;

		if ((protocol.compareTo(RemoteCommand.FTP_PROTOCOL)!=0)&&(protocol.compareTo(RemoteCommand.LOCAL_PROTOCOL)!=0)
				&&(protocol.compareTo(RemoteCommand.HTTP_PROTOCOL)!=0)&&(protocol.compareTo(RemoteCommand.RSYNC_PROTOCOL)!=0)) {
			return null;
		}
		return protocol;
	}

	public static String getServeurAttributRemoteConfig(String url) {
		if (!url.contains("://"))
			return null;

		String[] res = url.split("://");

		String serveurAndRest = res[1]; 

		while (serveurAndRest.contains("//"))
			serveurAndRest = serveurAndRest.replace("//", "/");

		if (serveurAndRest.startsWith("/"))
			serveurAndRest.replaceFirst("/", "");

		String serveurS =serveurAndRest;

		if (serveurAndRest.contains("/")) {
			serveurS = serveurAndRest.split("/")[0];
		}

		if (serveurS.matches("[\\w]+:[d]+")) 
			return serveurS.split(":")[0];

		return serveurS ;
	}

	public static int getPortAttributRemoteConfig(String url) {
		if (!url.contains("://"))
			return 21;

		String[] res = url.split("://");

		String serveurAndRest = res[1]; 

		while (serveurAndRest.contains("//"))
			serveurAndRest = serveurAndRest.replace("//", "/");

		if (serveurAndRest.startsWith("/"))
			serveurAndRest.replaceFirst("/", "");

		String serveurS =serveurAndRest;

		if (serveurAndRest.contains("/")) {
			serveurS = serveurAndRest.split("/")[0];
		}

		if (serveurS.matches("[\\w]+:[d]+")) 
			return Integer.valueOf(serveurS.split(":")[1]);

		return 21;
	}

	public static String getFilePathAttributRemoteConfig(String url) {
		if (!url.contains("://"))
			return null;

		String[] res = url.split("://");

		String serveurAndRest = res[1]; 

		while (serveurAndRest.contains("//"))
			serveurAndRest = serveurAndRest.replace("//", "/");

		if (serveurAndRest.startsWith("/"))
			serveurAndRest.replaceFirst("/", "");

		String serveurS =serveurAndRest;

		if (serveurAndRest.contains("/")) {
			serveurS = serveurAndRest.split("/")[0];
		}
		return serveurAndRest.replace(serveurS, "");
	}
}
