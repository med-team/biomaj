/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.internal.ant.task.net;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPConnectionClosedException;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileEntryParser;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.parser.FTPFileEntryParserFactory;
import org.apache.commons.net.ftp.parser.ParserInitializationException;
import org.apache.commons.net.ftp.parser.UnixFTPEntryParser;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;


public class FtpImpl extends RemoteCommandImpl {  

	private static final int CONNEX_TIME_OUT_STATIC =   100000;

	
	private boolean activeMode   = false ;
	/**
	 * @uml.property  name="cONNEX_TIME_OUT"
	 */
	private int CONNEX_TIME_OUT  =   CONNEX_TIME_OUT_STATIC;

	private static final int NB_TRY_STATIC = 5; 

	/**
	 * @uml.property  name="nB_TRY"
	 */
	private int NB_TRY = NB_TRY_STATIC; 

	/**
	 * @uml.property  name="server"
	 */
	private String server ;

	/**
	 * @uml.property  name="port"
	 */
	private int port ;

	/**
	 * @uml.property  name="username"
	 */
	private String username ;

	/**
	 * @uml.property  name="password"
	 */
	private String password ;

	/**
	 * @uml.property  name="client"
	 * @uml.associationEnd  
	 */
	private FTPClient client;
	
	private int currentReconnectionTries = 0;
	private static final int MAX_RECONNECTION_TRIES = 3;

	public FtpImpl(Task task) {
		super(task);
	}

	@Override
	public void init(String server, Integer port, String username, String password) throws BiomajBuildException {
		super.init(server,port,username,password);
		this.server = server;
		this.port = port ;
		this.username = username ;
		this.password = password;

		if (getTask().getProject().getProperties().containsKey(BiomajConst.ftpActiveMode)) {
			activeMode = Boolean.valueOf(getTask().getProject().getProperty(BiomajConst.ftpActiveMode)) ;
		}
		
		//Initialisation du parametrage du ftp
		if (getTask().getProject().getProperties().containsKey(BiomajConst.ftpTimeOut))
		{
			try {
				CONNEX_TIME_OUT = Integer.valueOf(getTask().getProject().getProperty(BiomajConst.ftpTimeOut));

				if ((CONNEX_TIME_OUT!=-1)&&(CONNEX_TIME_OUT<0)) {
					log("Bad value for ["+BiomajConst.ftpTimeOut+":"+getTask().getProject().getProperties().get(BiomajConst.ftpTimeOut)+"] has to be positive or -1.",Project.MSG_WARN);
					CONNEX_TIME_OUT = CONNEX_TIME_OUT_STATIC;
				}


			} catch (NumberFormatException nf) {
				log("Bad value for ["+BiomajConst.ftpTimeOut+":"+getTask().getProject().getProperties().get(BiomajConst.ftpTimeOut)+"]",Project.MSG_WARN);
				CONNEX_TIME_OUT = CONNEX_TIME_OUT_STATIC;
			}
		}

		if (getTask().getProject().getProperties().containsKey(BiomajConst.ftpTriesConnexion))
		{
			try {
				NB_TRY = Integer.valueOf(getTask().getProject().getProperty(BiomajConst.ftpTriesConnexion));

				if (NB_TRY<0)
				{
					log("Bad value for ["+BiomajConst.ftpTriesConnexion+":"+getTask().getProject().getProperties().get(BiomajConst.ftpTriesConnexion)+"] has to be positive.",Project.MSG_WARN);
					NB_TRY = NB_TRY_STATIC; 
				}

			} catch (NumberFormatException nf) {
				log("Bad value for ["+BiomajConst.ftpTriesConnexion+":"+getTask().getProject().getProperties().get(BiomajConst.ftpTriesConnexion)+"]",Project.MSG_WARN);
				NB_TRY = NB_TRY_STATIC; 
			}
		}

		connect();
	}
	
	public void connect() throws BiomajBuildException {
		class MyFTPFileEntryParserFactoryWhenSystemNameIsNull implements FTPFileEntryParserFactory {
			public FTPFileEntryParser createFileEntryParser(String key)
            throws ParserInitializationException {
				return new UnixFTPEntryParser();
			
			}
			public FTPFileEntryParser createFileEntryParser(FTPClientConfig config)
            throws ParserInitializationException {
				return new UnixFTPEntryParser();
			}
			
		} ;
		try {	
			client = new FTPClient();
//			if (CONNEX_TIME_OUT>0)
//				client.setDefaultTimeout(CONNEX_TIME_OUT);
			int tries= 0;
			boolean isOk = false ;
			while (tries++ < NB_TRY && !isOk) {
//				try {
					client.connect(server, port);
					//checkFtpCode(client);

					if (!client.login(username, password)) {
						throw new BiomajBuildException(getTask().getProject(),"ftp.loggin.error",client.getReplyString(),new Exception());
					}	
					
					if (client.getSystemName()==null) {
//						use parserKey,pathName method signature
						client.setParserFactory(new MyFTPFileEntryParserFactoryWhenSystemNameIsNull());
					}
					
					if (!activeMode) {
						client.enterLocalPassiveMode();
						log("Changed to passive mode.",Project.MSG_VERBOSE);
					}
					
					isOk = true;
					
//				} catch (SocketTimeoutException ste) {
//					log("** connect() ERROR:SOCKETTIMEOUTEXCEPTION **", Project.MSG_VERBOSE);
//					log("try to reconect on the server:"+server,Project.MSG_VERBOSE);
//					e = ste ;
//				}
			}
			if (!isOk) {
				throw new BiomajBuildException(getTask().getProject(),"remote.error.autoreconnect",Integer.toString(NB_TRY),null);
			}
			/*
			if (!client.login(username, password)) {
				throw new BiomajBuildException(getTask().getProject(),"ftp.loggin.error",client.getReplyString(),new Exception());
			}*/
		} catch (SocketException exSoc) {
			throw new BiomajBuildException(getTask().getProject(),"ftp.socket.error",exSoc.toString(),exSoc);
		} catch (IOException exIo) {
			throw new BiomajBuildException(getTask().getProject(),"io.error",exIo.toString(),exIo);
		}
		
	}


	@Override
	public void disconnect() {
		if (client == null)
			return;
		if(client.isConnected()) {
			try {
				client.disconnect();
			} catch(IOException ioe) {
				getTask().getProject().log("Can't disconnect from server : "+ioe.getMessage(),Project.MSG_VERBOSE);
			}
		}	
	}

	/**
	 * Get a remote file
	 * @param remoteDirectory   : directory to find file
	 * @param nameFile          : file to download
	 * @param targetDirectory   : local directory where the file will be stored
	 * @param targetName        : file name in the local directory 
	 * @return
	 */
	@Override
	protected boolean getFileWithImpl(String remoteDirectory,String nameFile,String targetDirectory,String targetName) throws BiomajBuildException {
		
		boolean isOk=false;
		remoteDirectory =  checkRemoteDirectory(remoteDirectory) ;
		try {
			// If connection was somehow lost, try to reconnect
			int reply = client.getReplyCode();
			while (!FTPReply.isPositiveCompletion(reply) && currentReconnectionTries++ < MAX_RECONNECTION_TRIES) {
				log("FTP wrong reply code. Try to reconnect", Project.MSG_WARN);
				disconnect();
				Random rand = new Random();
				int wait = 100 + rand.nextInt(400); // Wait between 100 and 500
				TimeUnit.MILLISECONDS.sleep(wait);
				connect();
				reply = client.getReplyCode();
			}
			// If could not reconnect, stop
			if (currentReconnectionTries >= MAX_RECONNECTION_TRIES)
				throw new BiomajBuildException(getTask().getProject(), "remote.error.connection.closed", new Exception("Maximum number of attempts reached for file " + nameFile));
			
			FileOutputStream fileStream = new FileOutputStream(targetDirectory+"/"+targetName);
			client.changeWorkingDirectory(remoteDirectory);
//			Bug O.F 21/09/2006 deafult mode is ASCII
			client.setFileType(FTP.BINARY_FILE_TYPE);

			isOk=client.retrieveFile(nameFile,fileStream);
			
			fileStream.close();
		} catch (FTPConnectionClosedException ex) {
			throw new BiomajBuildException(getTask().getProject(),"remote.error.connection.closed",ex.getMessage(),ex);

		} catch (Throwable ex) {
			throw new BiomajBuildException(getTask().getProject(),ex);

		} 
		return isOk;
	}



/*
	private void checkFtpCode(FTPClient ftp) throws BiomajBuildException {
		if(!FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
			throw new BiomajBuildException(getTask().getProject(),"ftp.error",ftp.getReplyString(),new Exception());
		}
	}
*/

	private Vector<RemoteFile> list(String base, String directoryToApplyLs,boolean matchFile, boolean matchAll) throws BiomajBuildException {
		base =  checkRemoteDirectory(base) ;
		log("methode list : base ["+base+"] directoryToApplyLs ["+directoryToApplyLs+"] matchFile ["+Boolean.valueOf(matchFile)+"]",Project.MSG_VERBOSE);
		Vector<RemoteFile> remoteFiles = new Vector<RemoteFile>();

		int tentative =  0 ;

		while ( tentative++ < NB_TRY ) {
			try {
				//checkFtpCode(client);
				log("Listing FTP files", Project.MSG_DEBUG);
				String ref = base+"/"+directoryToApplyLs;
//				log("change directory:["+ref+"]", Project.MSG_DEBUG);
//				client.changeWorkingDirectory(ref);
				log("Listing directory:["+ref+"]", Project.MSG_DEBUG);
				FTPFile[] list = client.listFiles(ref);
//				FTPFile[] list = client.listFiles();
				log("Files found in " + ref + ": " + list.length, Project.MSG_DEBUG);
				
				//Si c est un lien, on doit determiner si c est un fichier ou un repertoire

				for (int i=0;i<list.length;i++) {
					if (list[i]==null)
						continue;
					if (matchAll || isFile(ref,list[i])==matchFile) {
						log("add remote file:["+list[i].getName()+"]", Project.MSG_DEBUG);
						remoteFiles.add(new RemoteFile(directoryToApplyLs,client,list[i]));
					}
				}
				

				return remoteFiles;

			} catch(IOException ioe) {
				log("** list() ERROR:IOEXCEPTION **", Project.MSG_VERBOSE);
				log("MESSAGE:"+ioe.getMessage(), Project.MSG_DEBUG);

				if(client.isConnected()) {
					try {
						client.disconnect();
					} catch(IOException iof) {
						log(iof.getMessage(),Project.MSG_VERBOSE);
						//throw new BiomajBuildException(getTask().getProject(),"remote.error.connect",client.getPassiveHost().toString() + ioe.getMessage(),iof);
					}
				}

				log(ioe.getMessage(),Project.MSG_VERBOSE);
				connect();
				//throw new BiomajBuildException(getTask().getProject(),"remote.error.connect",client.getPassiveHost().toString() + ioe.getMessage(),new Exception());
			}
			catch (BiomajBuildException n) {
				throw n;
			} 
			catch (BiomajException n) {
				throw new BiomajBuildException(getTask().getProject(),n);
			}  
			catch (Exception e) {
				log("Exception throwable :"+e.getMessage(), Project.MSG_ERR);
				for (StackTraceElement s : e.getStackTrace()) {
					log(s.toString(),Project.MSG_VERBOSE);
				}
				throw new BiomajBuildException(getTask().getProject(),e);
			}
			catch (Throwable t) {
				log("Exception throwable :"+t.getMessage(), Project.MSG_ERR);
				for (StackTraceElement s : t.getStackTrace()) {
					log(s.toString(),Project.MSG_VERBOSE);
				}
				throw new BiomajBuildException(getTask().getProject(),t);
			}
		}
		throw new BiomajBuildException(getTask().getProject(),"remote.error.autoreconnect",Integer.toString(NB_TRY),null);
	}

	@Override
	public Vector<RemoteFile> listDir(String base, String directoryToApplyLs) throws BiomajBuildException {
		return list(base,directoryToApplyLs,false,false);
	}

	@Override
	public Vector<RemoteFile> listFiles(String base, String directoryToApplyLs) throws BiomajBuildException {
		return list(base,directoryToApplyLs,true,false);
	}
	
	@Override
	public List<RemoteFile> listAll(String base, String directoryToApplyLs)
			throws BiomajBuildException {
		return list(base,directoryToApplyLs,true,true);
	}


	/**
	 * Si f est un lien, donne son lien d'origine (fichier no link) sinon rend f
	 * @param f
	 * @return
	 */

	public boolean isFile(String base,FTPFile f) throws IOException,BiomajException {
		if (!f.isSymbolicLink())
			return f.isFile();


		log(f.getName()+" is a link:"+f.getRawListing(),Project.MSG_DEBUG);

		String ln = f.getLink();
		boolean res = false;
		/*
		 * Il arrive qu il y ai des probleme de connexion time-out, on essaye un certain nombre de tentative 
		 * (connexion/deconnexion) avant de renvoyer une erreur
		 */
		int tentative =  0 ;

		while ( tentative++ < NB_TRY ) {
			try {

				//test pour savoir si le lien est un repertoire ou non!
				if (res = client.changeWorkingDirectory(base + "/" + ln)) {
					log("DIRECTORY",Project.MSG_VERBOSE);
				} else { 
					log("FILE",Project.MSG_VERBOSE);
				}

				log("Change directory:"+base,Project.MSG_DEBUG);
				//on revient au working directory initial
				client.changeWorkingDirectory(base);
				//checkFtpCode(client);
				//throw new IOException("TEST");
				return !res;

			} catch (IOException ioe) {
				log("isFile() : ** ERROR:IOEXCEPTION **", Project.MSG_VERBOSE);
				if(client.isConnected()) {
					try {
						client.disconnect();
					} catch(IOException iof) {
						log(iof.getMessage(),Project.MSG_VERBOSE);
						//throw new BiomajException("remote.error.connect",ioe.getMessage());
					}
				}
				log(ioe.getMessage(),Project.MSG_VERBOSE);
				//throw new BiomajException("remote.error.connect",client.getPassiveHost().toString() + ioe.getMessage());
				//throw ioe;
				connect();
			}
		}
		throw new BiomajException("remote.error.autoreconnect",Integer.toString(NB_TRY));
	}

}
