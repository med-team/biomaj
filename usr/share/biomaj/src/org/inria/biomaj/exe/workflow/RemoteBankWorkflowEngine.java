/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.workflow;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.Semaphore;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.BuildListener;
import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.logger.BiomajConsoleLogger;
import org.inria.biomaj.ant.logger.BiomajMirrorListenerHandler;
import org.inria.biomaj.ant.logger.SimpleLoggerHistoric;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.session.bank.Bank;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.Configuration;
import org.inria.biomaj.session.bank.FileDesc;
import org.inria.biomaj.session.bank.ProductionDirectory;
import org.inria.biomaj.session.bank.Session;
import org.inria.biomaj.session.process.BiomajProcess;
import org.inria.biomaj.session.process.DecisionWorkflowPostProcess;
import org.inria.biomaj.session.process.MetaProcess;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.singleton.BiomajSession;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;


public class RemoteBankWorkflowEngine extends WorkflowEngine {
	/** Nbre de gestion maximum de bank */
	private static int max_connexion_remote = -1;

	private static Semaphore nbConnexion ;

	/**
	 * @uml.property  name="blh"
	 * @uml.associationEnd  
	 */
	private BiomajMirrorListenerHandler blh = null; 

	//sous process qui contient une liste de process a executer sequentiellement
	/**
	 * @uml.property  name="idListProcessSeq"
	 */
	private String idListProcessSeq ;


	/**
	 * @uml.property  name="bProject"
	 * @uml.associationEnd  
	 */
	private BiomajAntProject bProject = null ;
	/**
	 * @uml.property  name="console"
	 * @uml.associationEnd  
	 */
	private BuildListener console = null;

	/**
	 * @uml.property  name="level"
	 */
	private int level = Project.MSG_INFO;

	/**
	 * @uml.property  name="withConsole"
	 */
	private boolean withConsole = true;

	/**
	 * metaprocess name for subtask
	 * @uml.property  name="metaProcessName"
	 */
	private String metaProcessName = null ;

	/**
	 * Type process (post or pre) for subtask
	 * @uml.property  name="typeProcessName"
	 */

	private String typeProcessName = null ;

	/**
	 * @uml.property  name="modeForProcesses"
	 */
	private int modeForProcesses = DecisionWorkflowPostProcess.MODE_VERIF;
	/**
	 * @uml.property  name="bank"
	 * @uml.associationEnd  
	 */
	private Bank bank;
	
//	private boolean tmpMove = false;

	public RemoteBankWorkflowEngine(BiomajBank bank) throws BiomajException {
		super(bank);
		if (max_connexion_remote == -1) {
			loadMaxConnexion();
			nbConnexion = new Semaphore(max_connexion_remote);
		}
	}

	protected void initWorkflow() throws BiomajException {
		super.initWorkflow();
		
		
		bProject = new BiomajAntProject();	
		Properties props = getBiomajBank().getPropertiesFromBankFile();
		props.put("fromScratch", String.valueOf(isFromScratch()));
		
		
		if (!props.containsKey("ref.release"))
			props.put("ref.release", "");
		if (props.containsKey("db.source")) { // Computed bank
			props.put("computed", "true");
		} else {
			props.put("computed", "false");
		}
		
		
		bProject.init(BiomajConst.mirrorXmlFile, props);
		
		
		SimpleLoggerHistoric slh = BiomajLogger.getInstance().initLogger(getBiomajBank().getDbName(), "mirror.log");
		if (slh != null)
			bProject.addBuildListener(slh);

		blh = new BiomajMirrorListenerHandler();
		bProject.addBuildListener(blh);
		//}
		
		if (withConsole)
			initConsoleLogger();
		
		/*
		 * Verifie que ces proprietes n ont pas ete modifier par rapport aux derniers repertoire de prod !
		 * Si modificiation, il faut proposer a l utilisateur d'executer l'option --move pour exporter l ensemble des repertoires de version !
		 */
		checkVersionDirectory();
		

		/* Verifie l'existence des repertoire de production */
		checkProductionDirectory();
		
		/*
		 * If the workflow is launched with a stop point,set a propety to informe listener to load the last session
		 */
		setProperty(BiomajConst.newUpdateProperty, Boolean.toString(isForceForANewUpdate() || isFromScratch()));
		
		
		if (isFromScratch()) {
			modeForProcesses = DecisionWorkflowPostProcess.MODE_NEW_RELEASE;
		} else if (isForceForANewUpdate()) {
			modeForProcesses = DecisionWorkflowPostProcess.MODE_NEW_RELEASE;
			File offline = new File(getProperty(BiomajConst.dataDirProperty)+ "/" + getProperty(BiomajConst.offlineDirProperty));
			File flat = new File(getProperty(BiomajConst.dataDirProperty)+ "/" + getProperty(BiomajConst.versionDirProperty)+"/"+BiomajConst.futureReleaseLink+"/flat");
			try {
				BiomajUtils.moveAllFilesToDirectory(flat,offline);
				flat.delete();
			} catch (IOException ioe) {
				throw new BiomajException(ioe);
			}
		}
		bProject.start();
		printInformation();
		
		//Le projet est initialiser, on peut recuperer les donnees (Chargement des anciennes sessions) 
		//	if (blh!=null) {
		bank = BiomajSession.getInstance().getBank(this.getBiomajBank().getDbName());
		if ((getCommand()!=WorkflowEngine.TARGET_ALL)&&(getCommand()!=WorkflowEngine.TARGET_REBUILD)) {
			printInformationLastUpdate();
		}
		//}

	}

	public Bank getBank() {
		return bank;
	}

	public boolean getModeConsole() {
		return withConsole;
	}

	@Override
	public void setModeConsole(boolean withConsole, int mode) {
		this.withConsole = withConsole;
		this.level = mode ;
	}

	protected void endWorkflow() throws BiomajException {
		super.endWorkflow();
		sendMessage(getMessageHeader(true),getMessage());
		bProject.getProject().fireBuildFinished(null);	
		treatEnd();
	}


	@Override
	protected void endWithErrorWorkflow(Exception e) throws BiomajException {
		super.endWithErrorWorkflow(e);
		sendMessage(getMessageHeader(false),getMessage());
		if (bank != null) {
			bank.setErrorOnWorkflow(true);

		}
		bProject.getProject().fireBuildFinished(e);
		treatEnd();
	}

	protected void treatEnd() throws BiomajException{
		if (isWorkWithCurrentDirectory()) {
			File futur_rel = new File(getProperty(BiomajConst.dataDirProperty)+ "/" + getProperty(BiomajConst.versionDirProperty)+"/"+BiomajConst.futureReleaseLink);
			futur_rel.delete();
		}
		
		// Si on a des fichier a redeplacer dans leur repertoire d'origine dans le cas d'une execution fromscratch
		/*
		if (tmpMove) {
			Vector<ProductionDirectory> dirs = bank.getBankStateListProductionDirectories();
			String releasePath = dirs.get(dirs.size() - 2).getPath();
			
			File flatDir = new File(releasePath +"/flat");
			File tmpDir = new File(releasePath + "/" + BiomajConst.tmpMoveDir);
			try {
				BiomajUtils.moveAllFilesToDirectory(tmpDir, flatDir);
			} catch (IOException ex) {
				throw new BiomajException(ex);
			}
			tmpDir.delete();
		}*/
	}

	protected void launchWithTarget(String target) throws BiomajException {

		setProperty("task", target);

		//log("External task:"+getProperty("task"),Project.MSG_WARN);

		try {
			bProject.getProject().executeTarget("workflow_control");
		} catch (Exception e) {
			bProject.getProject().log(e.getStackTrace().toString(),Project.MSG_DEBUG);
			throw new BiomajException(e);
		}

		String stop = getProperty("stop.workflow");
		if ((stop != null)&&(stop.compareTo("true")==0)) {
			killApplication() ;
		}
	}



	@Override
	public Vector<WorkflowEngine> getChildren() {
		return new Vector<WorkflowEngine>();
	}

	/*
	 * O.FIlangi deprecated methode from class Thread
	public void destroy() {
		project.removeBuildListener(console);
	}
	 */
	@Override
	protected void runAll() throws BiomajException {
//		no test for an existing session, this start a new session!
		checkFrequency();
		runPreProcess();
		runMirror();
		runMakeProduction();
		runPostProcess();
		
//		waitIfNeeded();
		
		runDeployment();
		runDeleteOldVersion();
		writeEnd();
	}

	@Override
	protected void runUntilDeployment() throws BiomajException {
		checkFrequency();
		runPreProcess();
		runMirror();
		runMakeProduction();
		runPostProcess();
		runDeployment();
		removeAllBlockProcess();
		runDeleteOldVersion();
		writeEnd();
	}

	/**
	 * 
	 * run pre-process workflow
	 */
	protected void runUntilPreProcess() throws BiomajException{
		checkFrequency();
		runPreProcess();
		runDeleteOldVersion();
		writeEnd();
	}

	/**
	 * run mirror workflow
	 */
	protected void runUntilMirror() throws BiomajException {
		checkFrequency();
		runPreProcess();
		runMirror() ;
		runDeleteOldVersion();
		writeEnd();
	}


	/**
	 */
	protected void runUntilMakeProduction() throws BiomajException {
		checkFrequency();
		runPreProcess();
		runMirror() ;
		runMakeProduction() ;
		runDeleteOldVersion();
		writeEnd();
	}


	/**
	 * run Post-Process Workflow
	 */
	protected void runUntilPostProcess() throws BiomajException {
		checkFrequency();
		runPreProcess();
		runMirror() ;
		runMakeProduction() ;
		runPostProcess();
		runDeleteOldVersion();
		writeEnd();
	}

	@Override
	protected void runRebuild() throws BiomajException {

		writeBeginStag("REBUILD");
		//test sur le rebuild !		
		if (!linkFuturReleaseExist()) {
			List<ProductionDirectory> lpd = BiomajSQLQuerier.getAvailableProductionDirectories(getBiomajBank().getDbName());

			if (lpd.size() > 0) {
				//throw new BiomajException("error.rebuild.nofind.version");

				//dans un premier temps on reconstruit seulement la derniere version
				ProductionDirectory pd = lpd.get(lpd.size()-1);

				Bank b =  new Bank();
				if (!BiomajSQLQuerier.getUpdateBank(getBiomajBank().getDbName(), pd.getSession(), b, true))
					throw new BiomajException("error.rebuild.nofind.updatebank.with.prod.version");
				
				Map<String, String> info = BiomajSQLQuerier.getBankInfo(getBiomajBank().getDbName());
				if (info == null)
					throw new BiomajException("error.rebuild.nofind.configuration.with.prod.version");
				
				Configuration config = new Configuration(info);
				b.setBankStateListProductionDirectories(BiomajSQLQuerier.getAllProductionDirectories(getBiomajBank().getDbName()));

				b.setConfig(config);
				bank.setBankToRebuild(b);
				deleteDependances();
				launchWithTarget("action_rebuild");
				runPostProcess();
				runDeployment();
				runDeleteOldVersion();
				writeEnd();
				return ;
			} 
		}

		throw new BiomajException("error.rebuild.execute");
	}

	private void deleteDependances() throws BiomajException {
		String[] process  = getListProcessToRebuild();
		String[] metas   = getListMetaProcessToRebuild();
		String[] blocks = getListBlockProcessToRebuild();

		if (process == null) { 
			process = new String[0] ;
		}
		if (metas == null) { 
			metas = new String[0] ;
		}
		
		if (blocks == null) { 
			blocks = new String[0] ;
		}
		
		if (( blocks.length != 0)||(metas.length != 0)||( process.length != 0) ) {
			Vector<MetaProcess> mps = bank.getAllMetaprocess();
			//log("List of all MetaOP:"+mps.size(),Project.MSG_WARN);
			for (MetaProcess mp : mps) {
				boolean metaProcessIsDeleted = false ;
				if (blocks.length != 0) {
					for (String bl : blocks) {
						if (!metaProcessIsDeleted && mp.getBlock().compareTo(bl.trim())==0) {
							log("deleting dependances from BLOCK:"+bl,Project.MSG_WARN);
							//effacement des deps
							for (BiomajProcess bp : mp.getListProcess()) {
								for (FileDesc fd : bp.getDependancesOutput()) {
									deleteFile(fd);
								}
							}
							metaProcessIsDeleted = true;
						} 
					}
				}
				if (!metaProcessIsDeleted&&(metas.length != 0)) {
					for (String meta : metas) {
						if (meta.trim().compareTo(mp.getName())==0) {
							log("Deleting dependances from METAPROC:"+meta,Project.MSG_WARN);
							//effacement des deps
							for (BiomajProcess bp : mp.getListProcess()) {
								for (FileDesc fd : bp.getDependancesOutput()) {
									deleteFile(fd);
								}
							}
						}
					}
				}
			}

			if (process.length != 0) {
				Vector<BiomajProcess> bps = new Vector<BiomajProcess>();
				for (MetaProcess mp : mps)
					bps.addAll(mp.getListProcess());

				for (BiomajProcess bp : bps) {
					for (String proc : process)
						if (bp.getKeyName().compareTo(proc) == 0) {
							log("Deleting dependances from PROCESS:"+proc,Project.MSG_WARN);
							for (FileDesc fd : bp.getDependancesOutput()) {
								deleteFile(fd);
							}

						}
				}
			}

		} else {
			log("Aucun process n as besoin d effacement",Project.MSG_WARN);
		}
	}

	private void deleteFile(FileDesc fd) {
		//System.out.println(Boolean.toString(fd.isLink()));
		log("delete:"+fd.getLocation(),Project.MSG_INFO);		
		new File(fd.getLocation()).delete();
	}
	
	private boolean checkFrequency() throws BiomajException {

		if (bank.workflowTaskEverPast(Session.PREPROCESS)) {
			log("Check frequency is ok....",Project.MSG_INFO);
			return true;
		}
		writeBeginStag("FREQUENCY UPDATE");
		launchWithTarget("action_frequency");

		boolean val = Boolean.valueOf(getProperty("frequency.ok"));

		if (val)
			launchWithTarget("action_clean_log");
		else
			killApplication();
		
		return val;
	}


	protected void runPreProcess() throws BiomajException {
		if (bank.workflowTaskEverPast(Session.PREPROCESS)) {
			log("Preprocess is ok....",Project.MSG_INFO);
			return ;
		}

		writeBeginStag("PRE-PROCESS");

		setProperty("type_process", BiomajConst.preprocessTarget);
		runProcessesWithProperty(BiomajConst.dbPreProcessProperty,"");
		if (blh!=null)
		{
			blh.finishedPreProcess(getProject());
		}
	}

	protected void runMirror() throws BiomajException {
		if (bank.workflowTaskEverPast(Session.COPY)) {
			log("Mirror is ok....",Project.MSG_INFO);
			log("load last release...:"+bank.getWorkflowInfoRelease(),Project.MSG_INFO);
			setProperty(BiomajConst.remoteReleaseDynamicProperty, bank.getWorkflowInfoRelease());
			return ;
		} 
		writeBeginStag("SYNCHRONIZATION");
		launchMirror();
	}

	private synchronized void launchMirror() throws BiomajException {
		{
			try {
				log("Wait to download... available connections remaining : "+nbConnexion.availablePermits(),Project.MSG_VERBOSE);
				nbConnexion.acquire();
			} catch (InterruptedException e) {
				BiomajLogger.getInstance().log(e);
			}
			try {
				if (bank.getConfig().getProtocol().equals("directhttp"))
					launchWithTarget("action_direct_mirror");
				else
					launchWithTarget("action_mirror");
			} catch (Exception e) {
				throw new BiomajException(e);
			} finally {
				nbConnexion.release();
			}
		}
	}

	/**
	 */
	protected void runMakeProduction() throws BiomajException {

		if (bank.workflowTaskEverPast(Session.MOVE)) {
			log("Make directory production is ok....",Project.MSG_INFO);

			if (!linkFuturReleaseExist())
			{
				log("no link futur exist !:"+bank.getWorkflowInfoProductionDir(),Project.MSG_VERBOSE);
				return;
			}

			if (bank.getWorkflowInfoProductionDir()==null)	
				throw new BiomajException("workflowengine.makeproduction.error");

			log("load last online directory...:"+bank.getWorkflowInfoProductionDir(),Project.MSG_INFO);
			setProperty(BiomajConst.onlineDirDynamicProperty, bank.getWorkflowInfoProductionDir());
			return ;
		}
		launchWithTarget("action_move");			 
		//Si une nouvelle version est charge on ne prend pas en compte les ancien process...
		if (Boolean.valueOf(getProperty(BiomajConst.offlineHasFilesDynamicProperty)))
			modeForProcesses = DecisionWorkflowPostProcess.MODE_NEW_RELEASE;
		else
			modeForProcesses = DecisionWorkflowPostProcess.MODE_VERIF;
	}

	protected void runPostProcess() throws BiomajException {

		if (!linkFuturReleaseExist())
		{
			removeAllBlockProcess();
			return;
		}

		if (!isWorkWithCurrentDirectory()&&bank.workflowTaskEverPast(Session.POSTPROCESS)) {
			log("post process is ok....",Project.MSG_INFO);
			removeAllBlockProcess();
			return ;
		}

		writeBeginStag("POST-PROCESS");

		setProperty("type_process", BiomajConst.postprocessTarget);

		/** Gestion des blocks depuis la 0.9.2.x : on garde la compatibilite avec les anciennes version si la 
		 *  propriete BLOCK n est pas definie !*/

		boolean processLaunched = false;
		if ((!getProject().getProperties().containsKey(BiomajConst.blockPostprocessProperty))||
				(getProject().getProperty(BiomajConst.blockPostprocessProperty).trim().compareTo("")==0)) {
			processLaunched = runProcessesWithProperty(BiomajConst.dbPostProcessProperty,""); 
		} else {
			processLaunched = executeBlockSequentially(BiomajConst.dbPostProcessProperty,getProject().getProperty(BiomajConst.blockPostprocessProperty));
		}

		if (blh != null)
			blh.finishedPostProcess(getProject(), processLaunched);
	}


	public void runPostProcess(String listBlock) throws BiomajException {

		if (!linkFuturReleaseExist())
			return;

		writeBeginStag("POST-PROCESS");

		setProperty("type_process", BiomajConst.postprocessTarget);

		boolean res = executeBlockSequentially(BiomajConst.dbPostProcessProperty,listBlock);

		if (blh != null)
			blh.finishedPostProcess(getProject(), res);


	}

	public void runOnlyRemoveProcess() throws BiomajException {
		Vector<String> versions = getVersionToRemove();
		for (String version : versions) {
			runRemoveProcess(version);
		}
		writeEnd();
	}
	
	public void runRemoveProcess(String version) throws BiomajException {
		try {
			// Don't check if it has already been done. We may want to launch several times the remove processes.
			// (if deleting several old versions for example)
			// Note that in this case, the log and statefile corresponding to the remove process (statefiles/bankname/METAPROCESS.id.xml and log/bankname/time/METAPROCESS.log)
			// will be the ones from the last execution. Previous ones are overwritten everytime a removeprocess is launched.
	
			writeBeginStag("REMOVE-PROCESS");
			bank.setRemove(true);
	
			bank.setWorkflowInfoRemovedRelease(version);
			setProperty("type_process", BiomajConst.removeprocessTarget);
	
			runProcessesWithProperty(BiomajConst.dbRemoveProcessProperty,"");
	
			if (blh!=null)
			{
				blh.finishedRemoveProcess(getProject());
			}
		}
		catch (Exception e) {
			// Get the log file path
			String log ="";
			if (bank != null) {
				log = bank.getLogFile() ;
			} else {
				log = BiomajLogger.getInstance().getFileNameGeneralLog();
			}
			
			log("\n--There was a problem during remove process execution. See the log ["+log+"] for more details.", Project.MSG_WARN);
			bank.setRemoveOk(false);
		}
		// Next line because we called createWorkflow before getting there. We have to be sure to remove EVERY reference to the same WE (1 call to createWorkflow = 1 reference).
		WorkflowEngine.removeWE(getBiomajBank().getDbName());
	}
	

	protected void runDeployment() throws BiomajException {
		
		if (!linkFuturReleaseExist())
			return;

		if (bank.workflowTaskEverPast(Session.DEPLOYMENT)) {
			log("Deployment is ok....",Project.MSG_INFO);
			return ;
		}

		writeBeginStag("DEPLOYMENT");
		File f = new File(getProject().getProperty(BiomajConst.dataDirProperty)+"/"+getProject().getProperty(BiomajConst.versionDirProperty)+"/"+BiomajConst.futureReleaseLink);

		if (f.exists()) {
			try {
				setProperty(BiomajConst.onlineDirDynamicProperty, f.getCanonicalPath());
				launchWithTarget("action_deployment");
				writeEndDeploy();
			} catch (IOException ioe) {
				throw new BiomajException("error.futur.release.not.exist",ioe);
			}
		}
		
		if (getCommand()==WorkflowEngine.TARGET_DEPLOY) {
			// When runDeployment is called directly, old versions are not deleted affter deploying the new one. So force it here.
			runDeleteOldVersion();
		}
	}

	/**
	 * run a Block definition
	 * 
	 * @since 0.9.2  evolution with block term
	 * @param property
	 * @throws BuildException
	 * @throws BiomajException
	 * 
	 * @return true if at least one process has been launched, false otherwise 
	 */
	private boolean runProcessesWithProperty(String property,String block) throws BuildException, BiomajException {
		
		boolean postProcess = false;
		boolean launched = false;

		log(BiomajUtils.dateToString(new Date(), Locale.US)+" ["+getBiomajBank().getDbName()+"]["+block+"]",Project.MSG_DEBUG);

		if (BiomajConst.dbPostProcessProperty.compareTo(property)==0)
			postProcess = true;
		else
			postProcess = false;

		boolean conditionToProcess = false;
		String blockProperty = property;

		if ((block != null) && (block.trim().compareTo("")!=0))
			blockProperty = block + "." + property;
		else
			block = "";

		
		conditionToProcess = getBiomajBank().getPropertiesFromBankFile().containsKey(blockProperty) && 
				getBiomajBank().getPropertiesFromBankFile().getProperty(blockProperty).trim().compareTo("")!=0;

		if (!conditionToProcess) {
//			if (BiomajConst.dbRemoveProcessProperty.compareTo(property)==0)
//				bank.setRemove(false);
			
			log("No parallel process defined! ("+property+" not set)", Project.MSG_INFO);
			if (blh!=null) {
				if (BiomajConst.dbPostProcessProperty.compareTo(property)==0)
					blh.startPostProcess(getProject(),"",block);
				else if (BiomajConst.dbPreProcessProperty.compareTo(property)==0)
					blh.startPreProcess(getProject(),"");
				else {
					blh.startRemoveProcess(getProject(),"", block); // Block is not (yet?) used (not read from properties file)
				}
			}
			return false;
		}

//		On recupere la liste de processus parallele!
		String list_proc_parallel = getBiomajBank().getPropertiesFromBankFile().getProperty(blockProperty);

		String[] list = list_proc_parallel.split(",");

		if (list.length==0) {
//			if (BiomajConst.dbRemoveProcessProperty.compareTo(property)==0)
//				bank.setRemove(false);
			
			log("No parallel process defined! ("+blockProperty+" not set)", Project.MSG_INFO);
			return false;
		}

		DecisionWorkflowPostProcess dwpp = new DecisionWorkflowPostProcess(block,getBiomajBank().getDbName(),blockProperty,bProject.getProject(),modeForProcesses,postProcess);

		ProcessSequentielHandler[] lThread = new ProcessSequentielHandler[list.length];

		log(list.length+" meta process found",Project.MSG_INFO);
		log("Process to launch:",Project.MSG_INFO);
		for (int i=0;i<list.length;i++) {
			String listProc = dwpp.getListSubProcess(block,list[i]);
			/*			
			 * Correction Bug 26/03 Si pas de process a lance alors pas de xml creer!
			 * Pas d execution de processHandler
			 */
			if (listProc.trim().compareTo("")==0) {
				continue;
			}

			//OFI 08/2007 ver 0.9.2.x
			//On est dans le cas ou un fichier dependant a ete efface du repertoire du prod
			//ce qui lance l execution du process attache
			/*
			try {
				if (!linkFuturReleaseExist()&&(linkCurrentReleaseExist())) {
					if (lpd == null)
						lpd = BiomajQueryXmlStateFile.getAllDirectoriesProduction(getBiomajBank().getDbName()); 

					if ((lpd == null)||(lpd.size()<=0))
					{
						log("Can't get last production directory",Project.MSG_ERR);
						throw new BiomajBuildException(getProject(),new Exception("Can't get last production directory"));
					}					
					String path = lpd.get(lpd.size()-1).getPath(); 
					createLinkFuturRelease(path);
					localWorkWithCurrentDirectory = true;
				}
			} catch (ParseException pe) {
				throw new BiomajBuildException(getProject(),pe);
			}

			 */
			ProcessSequentielHandler rbew = new ProcessSequentielHandler(getBiomajBank(),bank,getProperty("type_process"));
			rbew.setBlock(block);
			rbew.setMetaProcess(dwpp.getMetaProcess(block,list[i]));
			rbew.setListProcessToLaunch(listProc);
			log(list[i]+"="+rbew.getListProcessToLaunched(),Project.MSG_INFO);
			rbew.setModeConsole(getModeConsole());
			/*
			 * ON NE PEUX PAS PASSER ONLINE DIR CAR 
			 * ON PEUT TRAITER LA NOUVELLE RELEASE MAIS AUSSI LA CURRENT.......
			 * PAS DE SENS....
			if (getProject().getProperty(CitrinaConst.onlineDirDynamicProperty)==null) {
				log("Devel error: Can't load onlineDirDynamicProperty in RemoteBankWorkflowEngine!",Project.MSG_ERR); 
				killApplication() ;
			}

			rbew.getProject().setProperty(CitrinaConst.onlineDirDynamicProperty, getProject().getProperty(CitrinaConst.onlineDirDynamicProperty));*/

			lThread[i] = rbew;

			if (blh!=null) {
				if (BiomajConst.dbPostProcessProperty.compareTo(property)==0)
					blh.startPostProcess(getProject(),list[i],block);
				else if (BiomajConst.dbPreProcessProperty.compareTo(property)==0)
					blh.startPreProcess(getProject(),list[i]);
				else {
					blh.startRemoveProcess(getProject(),list[i], block);
				}
			}
			launched = true;
			rbew.start();
		}
		log(list.length+" console(s) are open.",Project.MSG_INFO);
		for (int i=0;i<lThread.length;i++) {
			if (lThread[i]!=null)
				lThread[i].synchr();
		}
		
		boolean retour = true;

		for (int i=0;i<lThread.length;i++) {
			if (lThread[i]==null)
				continue;
			while (lThread[i].isErrorOnWorkflow()==null) {}//rien, on attend que cette variable soit initialisee!
			retour=(!lThread[i].isErrorOnWorkflow())&&retour;
		}			

		/* Toutes les branches obnt  executer, on peut arreter l application si il y a eu une erreur  */
		/*----------------------------------------------------------------------------------------------*/

		if (!retour)
			killApplication();
		/*
		if (localWorkWithCurrentDirectory)
			removelinkFuturRelease();
		 */	
		
		return launched;
	}

	protected void runSeqListProcess() throws BuildException, BiomajException{

		if (!getBiomajBank().getPropertiesFromBankFile().containsKey(idListProcessSeq)) 
		{
			log("Meta process not defined : "+idListProcessSeq,Project.MSG_ERR);
			throw new BiomajException("postprocess.define.process.fail",idListProcessSeq);
		}

		setProperty("list_process_seq", getBiomajBank().getPropertiesFromBankFile().getProperty(idListProcessSeq));
		launchWithTarget("action_process");
	}

	protected void runDeleteOldVersion() throws BiomajException {
		launchWithTarget("action_delete_version");
	}


	@Override
	protected void runSubTask() throws BiomajException {
		runSeqListProcess();
	}

	protected boolean executeBlockSequentially(String property,String allBlock) throws BiomajException {

		if ((allBlock == null)||(allBlock.trim().compareTo("")==0)) {
			getProject().log("Dev error:can't execute Block process with empty list![value="+allBlock+"]", Project.MSG_ERR);
			return false;
		}

		String[] listBlock = allBlock.split(",");
		boolean res = false;

		for (String b : listBlock) {
			getProject().log("                                ----> BLOCK:"+b);
			res = runProcessesWithProperty(property,b);
			free(b);
		}
		
		return res;
	}

	@Override
	public void log(String message, int priority) {
		if ((bProject != null)&&(bProject.getProject()!=null)&&(message!=null)) {
			//System.out.println("mess:"+message+" prio:"+priority);
			bProject.getProject().log(message, priority);
		}
		else
			System.out.println(message);
	}

	public String getProperty(String key) {
		return bProject.getProperty(key);
	}

	public void setProperty(String key,String value) {
		bProject.setProperty(key, value);
	}

	/**
	 * @return  the project
	 * @uml.property  name="project"
	 */
	public Project getProject() {
		return bProject.getProject();
	}

	/**
	 * @return  the metaProcessName
	 * @uml.property  name="metaProcessName"
	 */
	public String getMetaProcessName() {
		return metaProcessName;
	}

	/**
	 * @param metaProcessName  the metaProcessName to set
	 * @uml.property  name="metaProcessName"
	 */
	public void setMetaProcessName(String metaProcessName) {
		this.metaProcessName = metaProcessName;
	}

	/**
	 * @return  the typeProcessName
	 * @uml.property  name="typeProcessName"
	 */
	public String getTypeProcessName() {
		return typeProcessName;
	}

	/**
	 * @param typeProcessName  the typeProcessName to set
	 * @uml.property  name="typeProcessName"
	 */
	public void setTypeProcessName(String typeProcessName) {
		this.typeProcessName = typeProcessName;
	}	

	private void killApplication() throws BiomajException {
		setErrorOnWorkflow(true);
		String log ="";
		if (bank != null) {
			log = bank.getLogFile() ;
		} else {
			log = BiomajLogger.getInstance().getFileNameGeneralLog();
		}

		throw new BiomajException("kill.application",log);
	}

	/*
	 * Recupere une liste de process qui a ete executee avec succes aux sessions anterieures...
	 * pour une release!


	private String[] getProcessesOkForRelease(String metaProcess) {

	}
	 */
	private void loadMaxConnexion() throws BiomajException {
		if (!getBiomajBank().getPropertiesFromBankFile().containsKey(BiomajConst.bankNumThreadProperty))
			throw new BiomajException("citrinautils.error.property",BiomajConst.bankNumThreadProperty);
		try {
			max_connexion_remote = Integer.valueOf((String)getBiomajBank().getPropertiesFromBankFile().get(BiomajConst.bankNumThreadProperty));
		} catch (Exception e) {
			throw new BiomajException("citrinautils.error.property.value",BiomajConst.bankNumThreadProperty);
		}
	}

	public String getBaseProductionDirectory() {
		String datadir=getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.dataDirProperty);
		String dirVersion = getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.versionDirProperty);
		return datadir+"/"+dirVersion;
	}

	public void createLinkFuturRelease(String path) throws BiomajException {
		try {
			BiomajUtils.createLinkOnFileSystem(new File(path), BiomajConst.futureReleaseLink);
		} catch (Exception e) {
			throw new BiomajException(e);
		}
	}

	public boolean linkFuturReleaseExist() {
		/* Condition pour lancer les post processus : le repertoire future_release exist!*/
		return new File(getBaseProductionDirectory()+"/"+BiomajConst.futureReleaseLink).exists();
	}

	public void removelinkFuturRelease() {
		/* Condition pour lancer les post processus : le repertoire future_release exist!*/
		new File(getBaseProductionDirectory()+"/"+BiomajConst.futureReleaseLink).delete();
	}

	public boolean linkCurrentReleaseExist() {
		return new File(getBaseProductionDirectory()+"/"+BiomajConst.currentLink).exists();
	}

	private void printInformation() {
		log("#==========================================#",Project.MSG_INFO);	
		log("FULL NAME  :"+getProperty(BiomajConst.dbFullNameProperty),Project.MSG_INFO);
		log("NAME       :"+getProperty(BiomajConst.dbNameProperty),Project.MSG_INFO);	
		String url = getProperty(BiomajConst.protocolProperty) ;
		if (getProperty(BiomajConst.serverProperty) != null)
			url += "://"+getProperty(BiomajConst.serverProperty)+":" ;
		
		if (getProperty(BiomajConst.remoteDirProperty) != null)
			url += getProperty(BiomajConst.remoteDirProperty);
		//else if (getProperty(BiomajConst.flatRepositoryProperty) != null)
			//url += getProperty(BiomajConst.flatRepositoryProperty);
		
		log("URL        :"+url+".",Project.MSG_INFO);	
		log("#==========================================#",Project.MSG_INFO);	
		log("",Project.MSG_INFO);
		log("",Project.MSG_INFO);
		log("",Project.MSG_INFO);
		log("",Project.MSG_INFO);
	}

	private void printInformationLastUpdate() {
		log("Load current cycle update....",Project.MSG_INFO);
		if (bank.getWorkflowInfoRelease() != null)
			log("release:"+bank.getWorkflowInfoRelease(),Project.MSG_INFO);
	}

	protected void writeBeginStag(String nameStag) {
		log("",Project.MSG_INFO);
		log("                  #=======================#",Project.MSG_INFO);
		log("                  #    ["+nameStag.toUpperCase()+"]",Project.MSG_INFO);
		log("                  #=======================#",Project.MSG_INFO);
		log("",Project.MSG_INFO);
	}

	protected void writeEndDeploy() {
		log("",Project.MSG_INFO);
		log(getBiomajBank().getDbName()+" has been updated.",Project.MSG_INFO);
		log(bank.getWorkflowInfoRelease()+" is now online. in directory ["+bank.getWorkflowInfoProductionDir()+"]",Project.MSG_INFO);
	}

	protected void writeEnd() {
		log("                  #=======================#",Project.MSG_INFO);
		log("                  BIOMAJ SESSION FINISHED",Project.MSG_INFO);
		log("                  #=======================#",Project.MSG_INFO);
	}

	protected void writeEndStag(String nameStag) {
		log("                  #====================#",Project.MSG_INFO);
	}

	/**
	 * Send a message to tha bank admin when an error appears.
	 *
	 */
	private void sendMessage(String header,String msgText) {
		BiomajLogger.getInstance().log("start :sendMessage["+header+"]");

		if (!getBiomajBank().getPropertiesFromBankFile().containsKey(BiomajConst.mailSmtpHostProperty)&&
				(!getBiomajBank().getPropertiesFromBankFile().containsKey(BiomajConst.mailAdminProperty))&&
				(!getBiomajBank().getPropertiesFromBankFile().containsKey(BiomajConst.mailFromProperty))) {
			//log("An errors appears but user are not defined mail properties.",Project.MSG_WARN);
			return;
		}

		if (!getBiomajBank().getPropertiesFromBankFile().containsKey(BiomajConst.mailSmtpHostProperty)) {
			log(BiomajConst.mailSmtpHostProperty+" are not defined in global properties. ["+
					BiomajConst.mailAdminProperty+"="+getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.mailAdminProperty)+"]",Project.MSG_ERR);

			return;
		}

		if (!getBiomajBank().getPropertiesFromBankFile().containsKey(BiomajConst.mailAdminProperty)) {
			log(BiomajConst.mailAdminProperty+" are not defined in global properties. ["+
					BiomajConst.mailAdminProperty+"="+getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.mailAdminProperty)+"]",Project.MSG_ERR);

			return;
		}

		if (!getBiomajBank().getPropertiesFromBankFile().containsKey(BiomajConst.mailFromProperty)) {
			log(BiomajConst.mailFromProperty+" are not defined in global properties. ["+
					BiomajConst.mailFromProperty+"="+getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.mailFromProperty)+"]",Project.MSG_ERR);

			return;
		}



		boolean debug = false;
		// create some properties and get the default Session
		Properties props = new Properties();
		String host = getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.mailSmtpHostProperty);
		String from = getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.mailFromProperty);
		//log("HOST:"+host);

		props.put("mail.smtp.host", host);
		if (debug) props.put("mail.debug", Boolean.toString(debug));


		javax.mail.Session session = javax.mail.Session.getInstance(props, null);
		session.setDebug(debug);

		try {
			// create a message
			Message msg = new MimeMessage(session);
			String tos = getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.mailAdminProperty);
			//msg.setFrom(new InternetAddress("Biomaj-"+Biomaj.VERSION+"@noreply.fr"));
			msg.setFrom(new InternetAddress(from));
			String[] lTos = tos.split(",");
			InternetAddress[] address = new InternetAddress[lTos.length];

			int i=0;
			for (String to : lTos)
				address[i++] = new InternetAddress(to);

			//log("TO:"+to);
			msg.setRecipients(Message.RecipientType.TO, address);
			msg.setSubject(header);
			msg.setSentDate(new Date());
			// If the desired charset is known, you can use
			// setText(text, charset)
			msg.setText(msgText);

			Transport.send(msg);
		} catch (MessagingException mex) {
			log("\n--Can't Send a mail:"+mex.getLocalizedMessage(),Project.MSG_WARN);
			BiomajLogger.getInstance().log(mex);
			log("\n--Exception handling in RemoteBankWorkflowEngine.java",Project.MSG_DEBUG);

			log("",Project.MSG_DEBUG);
			for (StackTraceElement st : mex.getStackTrace())
				log(st.toString(),Project.MSG_DEBUG);

			Exception ex = mex;
			do {
				if (ex instanceof SendFailedException) {
					SendFailedException sfex = (SendFailedException)ex;
					Address[] invalid = sfex.getInvalidAddresses();
					if (invalid != null) {
						log("    ** Invalid Addresses",Project.MSG_DEBUG);
						if (invalid != null) {
							for (int i = 0; i < invalid.length; i++) 
								log("         " + invalid[i],Project.MSG_DEBUG);
						}
					}
					Address[] validUnsent = sfex.getValidUnsentAddresses();
					if (validUnsent != null) {
						log("    ** ValidUnsent Addresses",Project.MSG_DEBUG);
						if (validUnsent != null) {
							for (int i = 0; i < validUnsent.length; i++) 
								log("         "+validUnsent[i],Project.MSG_DEBUG);
						}
					}
					Address[] validSent = sfex.getValidSentAddresses();
					if (validSent != null) {
						log("    ** ValidSent Addresses",Project.MSG_DEBUG);
						if (validSent != null) {
							for (int i = 0; i < validSent.length; i++) 
								log("         "+validSent[i],Project.MSG_DEBUG);
						}
					}
				}

				if (ex instanceof MessagingException)
					ex = ((MessagingException)ex).getNextException();
				else
					ex = null;
			} while (ex != null);
		}
		BiomajLogger.getInstance().log("END :sendMessage["+header+"]");
	}


	private String getMessageHeader(boolean isOk) {
		if (bank != null && bank.getConfig() != null) {
			String h = "Biomaj message: BANK ["+bank.getConfig().getName()+"] - STATUS ["+Boolean.toString(isOk).toUpperCase()+"]" + " - UPDATE ["+Boolean.toString(bank.isUpdate()).toUpperCase()+"]";
			if (bank.isUpdate()) 
				h += " - RELEASE:"+bank.getWorkflowInfoRelease();
			if (bank.isRemove())
				h += " - REMOVE ["+Boolean.toString(bank.isRemoveOk()).toUpperCase()+"]";
			return h;
		}
		else
			return "Biomaj message: BANK ["+this.getBiomajBank().getDbName()+"] - STATUS ["+Boolean.toString(isOk).toUpperCase()+"]";
	}

	private String getMessage() throws BiomajException {
		String res ="";
		if (bank==null)
			return "Biomaj can't begin a session!";

		res+="Start        :"+BiomajUtils.dateToString(bank.getStart(), Locale.US)+"\n";
		res+="End          :"+BiomajUtils.dateToString(new Date(), Locale.US)+"\n";
		if (bank.getStart()!=null)
			res+="Elapsed time :"+BiomajUtils.timeToString(new Date().getTime() - bank.getStart().getTime())+"\n";

		if (bank.isUpdate()) {
			res+="\n****************** INFO RELEASE ***************\n";

			res+="Number of session                 :"+Integer.toString(bank.getListOldSession().size()+1)+"\n\n";

			res+="Production directory :"+(bank.getWorkflowInfoProductionDir())+"\n";
			res+="Release              :"+(bank.getWorkflowInfoRelease())+"\n";
			res+="Download             :"+(BiomajUtils.sizeToString(bank.getWorkflowInfoSizeDownload()))+"\n";
			res+="Bandwidth (Mo/s)     :"+Float.toString(bank.getBandWidth())+"\n";
			res+="Num files downloaded :"+bank.getNbFilesDownloaded()+"\n";
			res+="Release              :"+(BiomajUtils.sizeToString(bank.getWorkflowInfoSizeRelease()))+"\n";

			Collection<MetaProcess> lMpPre = bank.getAvailableMetaProcess(Session.PREPROCESS);
			res+="------------\nPre processes:\n";
			if (lMpPre.size() > 0) {
				for (MetaProcess mpPre : lMpPre) {
					res+="Metaproc:["+ mpPre.getName() +"] log:["+mpPre.getLogFile()+"]"+"\n";
					for (BiomajProcess bp : mpPre.getListProcess()) {
						res+=" --> "+bp.getNameProcess()+"("+bp.getDescription()+")\n";
					}
				}
			}
			else {
				res+="None\n";
			}
			res+="\n";
			
			Collection<MetaProcess> lMp = bank.getAvailableMetaProcess(Session.POSTPROCESS);
			res+="------------\nPost processes:\n";
			if (lMp.size() > 0) {
				for (MetaProcess mp : lMp) {
					res+="Metaproc:["+ mp.getName() +"] log:["+mp.getLogFile()+"]"+"\n";
					for (BiomajProcess bp : mp.getListProcess()) {
						res+=" --> "+bp.getNameProcess()+"("+bp.getDescription()+")\n";
					}
				}
			}
			else {
				res+="None\n";
			}
			res+="\n";

			Collection<MetaProcess> lMpR = bank.getAvailableMetaProcess(Session.REMOVEPROCESS);
			res+="------------\nRemove processes:\n";
			if (lMpR.size() > 0) {
				for (MetaProcess mpR : lMpR) {
					res+="Metaproc:["+ mpR.getName() +"] log:["+mpR.getLogFile()+"]"+"\n";
					for (BiomajProcess bp : mpR.getListProcess()) {
						res+=" --> "+bp.getNameProcess()+"("+bp.getDescription()+")\n";
					}
				}
			}
			else {
				res+="None\n";
			}
			res+="\n";
			res+="\n***********************************************\n";
		} else if (bank.isRemove()) {
			
			res+="\n****************** INFO REMOVE ***************\n";

			Collection<MetaProcess> lMpR = bank.getAvailableMetaProcess(Session.REMOVEPROCESS);
			res+="------------\nRemove processes:\n";
			if (lMpR.size() > 0) {
				for (MetaProcess mpR : lMpR) {
					res+="Metaproc:["+ mpR.getName() +"] log:["+mpR.getLogFile()+"]"+"\n";
					for (BiomajProcess bp : mpR.getListProcess()) {
						res+=" --> "+bp.getNameProcess()+"("+bp.getDescription()+")\n";
					}
				}
			}
			else {
				res+="None\n";
			}
			res+="\n";
			res+="\n***********************************************\n";
		}

		try {
			/*
			if (bank!=null)
				for (Session s : bank.getListOldSession()) {
					for (String d : s.getMessageErrorOnSession())
						res+=d+"\n";
				}
			 */
			
			if ((bank!=null)&&(bank.getCurrentSession()!=null))
				for (String s : bank.getCurrentSession().getMessageErrorOnSession())
					res+=s+"\n";
		} catch (ParseException pe) {
			getProject().log(pe.getMessage(),Project.MSG_ERR);
		}
		return res;
	}

	public void updateProperties() {
		Properties props = getBiomajBank().getPropertiesFromBankFile();
		Enumeration<?> keys = props.keys();

		while (keys.hasMoreElements()) {
			String key = (String)keys.nextElement();
			if ((getProperty(key)==null)||(getProperty(key).compareTo(props.getProperty(key))!=0)) {
				log("update property ["+key+":"+props.getProperty(key)+"]",Project.MSG_DEBUG);
				setProperty(key,props.getProperty(key));
			}
		}
	}

	/**
	 * Initialize console logger with good title
	 *
	 */
	private void initConsoleLogger() {

		int value = BiomajConsoleLogger.SYNC;
		if ((getCommand()==WorkflowEngine.TARGET_ALL)||(getCommand()==TARGET_UNTIL_DEPLOY))
			value = BiomajConsoleLogger.SYNC_DEP;
		else if ((getCommand()==WorkflowEngine.TARGET_DEPLOY))
			value = BiomajConsoleLogger.DEP;

		console = new BiomajConsoleLogger(getBiomajBank().getDbName(),this,value);


		((BiomajConsoleLogger)console).setMessageOutputLevel(level);
		bProject.addBuildListener(console);
	}

	private void checkProductionDirectory() throws BiomajBuildException {
		log("[check production directories]",Project.MSG_VERBOSE);
		
		List<ProductionDirectory> lpd = BiomajSQLQuerier.getAvailableProductionDirectories(getBiomajBank().getDbName());
		
		for (ProductionDirectory pd : lpd) {
			File d = new File(pd.getPath());
			if (!d.exists()) {
				log("Directory '" + pd.getPath() + "' could not be found." +
						"Related session deleted. Directory status set to 'deleted'.", Project.MSG_WARN);
				BiomajSQLQuerier.deleteSession(pd.getSession(), null);
				if (BiomajSQLQuerier.setDirectoryStateToDeleted(pd) <= 0) {
					throw new BiomajBuildException(getProject(),"production.directory.does.not.exist",
							pd.getPath(),BiomajUtils.getBiomajRootDirectory()+"/statefiles/"+getBiomajBank().getDbName()+".xml",null);
				}
			} else {
				log(pd.getPath(),Project.MSG_VERBOSE);
			}
		}

	}
	
	private void checkVersionDirectory() throws BiomajBuildException {

		Map<String, String> info = BiomajSQLQuerier.getBankInfo(getBiomajBank().getDbName());
		if (info == null)
			return;
		
		
		Configuration config = new Configuration(info);
		
		String lastVersionDirectory = config.getVersionDirectory();

		String availableDir = getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.dataDirProperty) +"/"+
		getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.versionDirProperty);


		File prodDir = new File(availableDir);

		if (prodDir.compareTo(new File(lastVersionDirectory))!=0) {
			throw new BiomajBuildException(getProject(),"remotebankworkflow.error.change.versiondir", prodDir.getAbsolutePath(),null);
		}
	}
	
	public BiomajMirrorListenerHandler getListener() {
		return blh;
	}

}
