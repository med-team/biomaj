package org.inria.biomaj.exe.options;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Vector;


import org.inria.biomaj.exe.bank.BankFactory;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.Configuration;
import org.inria.biomaj.session.bank.ProductionDirectory;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * This class handle the move-production-directories option. 
 * 
 * @author ofilangi
 * @version Biomaj 0.9
 * @since Biomaj 0.8 /Citrina 0.5
 */
public class BiomajMoveProductionDirectories {

	private BiomajBank biomajBank = null ;
	private Vector<ProductionDirectory> lpd = null ;
	private File newProdDir = null ;

	
	public void moveProductionDirectories(String dbName) {
		try {
			biomajBank = new BankFactory().createBank(dbName,true);
			lpd = new Vector<ProductionDirectory>();
			List<Map<String, String>> dirs = BiomajSQLQuerier.getProductionDirectories(dbName);
			for (Map<String, String> dir : dirs)
				lpd.add(new ProductionDirectory(dir));

			newProdDir = new File(getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.dataDirProperty) +"/"+
					getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.versionDirProperty));

			for (ProductionDirectory pd : lpd)
				if (pd.getState()==ProductionDirectory.AVAILABLE)
					System.out.println("["+pd.getPath()+"] ");

			Configuration config = new Configuration(BiomajSQLQuerier.getBankInfo(dbName));
			String lastVersionDirectory = config.getVersionDirectory();

			if (!checkIfNeed(lastVersionDirectory)) {
				System.out.println(dbName+" no need to change production version.");
				return;
			}

			//Verifie qu il n y a pas de version en construction
			if (new File(lastVersionDirectory+"/"+BiomajConst.futureReleaseLink).exists()) {
				System.err.println("A future version was detected. Please stabilize your version to execute the export of the repository.");
				return;
			}

			if (!newProdDir.exists())
				BiomajUtils.createSubDirectories(newProdDir.getAbsolutePath());

			if (!checkSizeAvailable()) {
				return;
			}

			System.out.println("Are you sure to perform this operation?[y/n]");

			int c = System.in.read();
			if (c != 'y')
				return ;

			//			System.out.print("\rCompteur: " + i++);
			//System.exit(0);

			for (ProductionDirectory pd : lpd) {
				if (pd.getState() == ProductionDirectory.REMOVE)
					continue;
				String version = pd.getPath().replaceFirst(lastVersionDirectory, "");
				File newVersion = new File(newProdDir.getAbsolutePath()+"/"+version);
				System.out.println("move ["+pd.getPath()+"]-->["+newVersion.getAbsolutePath()+"]");
				BiomajUtils.deleteAll(newVersion);
				boolean b = move(pd.getPath(),newProdDir.getAbsolutePath()+"/"+version);
				System.out.println(pd.getPath());
				if (b) {
					String oldPath = pd.getPath();
					
					pd.setPath(newVersion.getAbsolutePath());
					BiomajSQLQuerier.updateProductionDirectory(dbName, oldPath, pd);
					BiomajSQLQuerier.updateVersionDirectory(dbName, config.getVersionDirectory(), newProdDir.getAbsolutePath());
					BiomajSQLQuerier.moveFiles(oldPath, pd.getPath());
				}
			}

			lpd = new Vector<ProductionDirectory>();
			lpd.addAll(BiomajSQLQuerier.getAvailableProductionDirectories(dbName));

			//Creer le lien current
			if (lpd.size()>0) {

				File oldCurrent = new File(lastVersionDirectory+"/"+BiomajConst.currentLink);
				oldCurrent.delete();

				BiomajUtils.createLinkOnFileSystem(new File(lpd.get(lpd.size()-1).getPath()), BiomajConst.currentLink);
			}

		} catch (Exception e) {
			System.err.println(e.getLocalizedMessage());
			return;
		}
	}
	
	
	/**
	 * 
	 * @return true if the version directory need to be changed.
	 * @throws BiomajException
	 */
	private boolean checkIfNeed(String lastDir) throws BiomajException {
		return new File(lastDir).getAbsolutePath().compareTo(newProdDir.getAbsolutePath())!=0;
	}


	private boolean checkSizeAvailable() throws BiomajException {

		if (newProdDir.getTotalSpace() == 0) {
			System.out.println("Unable to determine the space available memory for ["+newProdDir.getAbsolutePath()+"]");
			System.out.println("Would you continue the operation?[y/n]");
			try {
				int c = System.in.read();
				if (c == 'y')
					return true;

				return false;

			} catch (IOException e) {
				System.err.println(e.getLocalizedMessage());
				return false;
			}
		}

		int totalSize = 0;
		for (ProductionDirectory pd : lpd) 
			totalSize += pd.getSize();

		if (totalSize >= newProdDir.getFreeSpace())
		{
			System.err.println("["+newProdDir.getAbsolutePath()+"] production directory does not have enough free space.");
			return false;
		}

		return true;
	}

	private boolean move(String in,String out) throws IOException {
		File inF = new File(in);
		if (!inF.exists()) {
			System.err.println("["+in+"] does not exist.");
			return false;
		}

		File outF = new File(out);

		if (!outF.exists())
			BiomajUtils.createSubDirectories(outF.getAbsolutePath());


		//boolean moveIsOk = inF.renameTo(outF);
		boolean moveIsOk = false ;
		if (!moveIsOk) {

			return BiomajUtils.moveAllFilesToDirectory(inF, outF);
		}

		return true;
	}

	public BiomajBank getBiomajBank() {
		return biomajBank;
	}

}
