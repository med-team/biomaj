/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.workflow;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Vector;
import java.util.concurrent.Semaphore;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.singleton.BiomajSession;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;


/**
 * @author  ofilangi
 * @version  Biomaj 0.9
 * @since  Biomaj 0.8 /Citrina 0.5
 */
public abstract class WorkflowEngine  extends BiomajThread {

	public static final int TARGET_ALL                            = 0;
	public static final int TARGET_UNTIL_PREPROCESS               = 1;
	public static final int TARGET_UNTIL_MIRROR                   = 2;
	public static final int TARGET_UNTIL_POSTPROCESS              = 3;
	public static final int TARGET_UNTIL_MAKEPROD                 = 4;
	public static final int TARGET_UNTIL_DEPLOY                   = 5;
	public static final int TARGET_SUB_TASK                       = 6;
	public static final int TARGET_PREPROCESS                     = 7;
	public static final int TARGET_MIRROR                         = 8;
	public static final int TARGET_POSTPROCESS                    = 9;
	public static final int TARGET_DEPLOY                         = 10;
	public static final int TARGET_REBUILD                        = 11;
	public static final int TARGET_REMOVEPROCESS              	  = 12;


	/** Keep all instance of workflow engine to handle dependances and no redondance */
	private static TreeMap<String, WorkflowEngine> mapBankNameWE = new TreeMap<String, WorkflowEngine>();
	/** Instance of each workflow */
	private static TreeMap<String, Integer> mapBankRef = new TreeMap<String, Integer>();

	/**
	 * @uml.property  name="command"
	 */
	private int command                         =-1;

	/** Extension lock file*/
	public static final String extensionLock   = ".lock";

	/**
	 * @uml.property  name="checkLock"
	 */
	private Boolean checkLock = true;

	private BiomajBank biomajBank;
	/**
	 * Default is true, set false, if you don't want that biomaj check if there are lock
	 * @param  check
	 * @uml.property  name="workWithCurrentDirectory"
	 */

	private boolean workWithCurrentDirectory = false;

	/**
	 * @uml.property  name="forceForANewUpdate"
	 */
	private boolean forceForANewUpdate       = false;
	
	private boolean fromScratch = false;

	/**
	 * Initialized ti True When a Block is Executed
	 * @uml.property  name="hm_blockIsOk"
	 * @uml.associationEnd  qualifier="blockName:java.lang.String java.util.concurrent.Semaphore"
	 */
	private HashMap<String, Semaphore> hm_blockIsOk ;

	private String[] listProcessToRebuild = null ;
	private String[] listMetaProcessToRebuild = null ;
	private String[] listBlockProcessToRebuild = null ;


	private Vector<String> versionsToRemove;

	public Vector<String> getVersionToRemove() {
		return versionsToRemove;
	}


	public void setVersionsToRemove(Vector<String> versionsToRemove) {
		this.versionsToRemove = versionsToRemove;
	}


	public void lockHasBeCheck(boolean check) {
		checkLock = check;
	}


	public WorkflowEngine(BiomajBank bank) {
		super(bank.getDbName());
		biomajBank = bank;

		hm_blockIsOk = new HashMap<String, Semaphore>();
		if ((bank.getPropertiesFromBankFile().containsKey(BiomajConst.blockPostprocessProperty))&&
				(bank.getPropertiesFromBankFile().getProperty(BiomajConst.blockPostprocessProperty).trim().compareTo("")!=0))
		{
			String[] lBlocks = bank.getPropertiesFromBankFile().getProperty(BiomajConst.blockPostprocessProperty).split(",");
			for (String b : lBlocks) {
				hm_blockIsOk.put(b, new Semaphore(0));
			}

		}

		if (getMapBankNameWE().containsKey(bank.getDbName()))
		{
			BiomajLogger.getInstance().log("Workflow engine ever instanciate for ["+bank.getDbName()+"]");
			return;
		}
		getMapBankNameWE().put(bank.getDbName(), this);
		getMapBankRef().put(bank.getDbName(), new Integer(1));

	}

	/**
	 * Getter of the property <tt>biomajBank</tt>
	 * @return  Returns the biomajBank.
	 * @uml.property  name="biomajBank"
	 */
	public BiomajBank getBiomajBank() {
		return biomajBank;
	}

	/**
	 * Setter of the property <tt>biomajBank</tt>
	 * @param biomajBank  The biomajBank to set.
	 * @uml.property  name="biomajBank"
	 */
	public void setBiomajBank(BiomajBank biomajBank) {
		this.biomajBank = biomajBank;
	}

	public abstract void setModeConsole(boolean withConsole,int mode);

	protected void initWorkflow() throws BiomajException {
		setLock();
		//synchr();
	}

	protected void endWorkflow() throws BiomajException {
		unlock();
		release();
		setErrorOnWorkflow(false);
		BiomajSession.getInstance().removeBank(getBiomajBank().getDbName());
		BiomajLogger.getInstance().close(getBiomajBank().getDbName());
	}

	protected void endWithErrorWorkflow(Exception e) throws BiomajException {
		unlock();
		release();
		setErrorOnWorkflow(true);
		BiomajSession.getInstance().removeBank(getBiomajBank().getDbName());
		BiomajLogger.getInstance().close(getBiomajBank().getDbName());
	}

	public abstract void log(String message, int priority);

	protected abstract void runAll() throws BiomajException ;

	/**
	 * run mirror workflow
	 * @throws BiomajException
	 */
	protected abstract void runUntilMirror() throws BiomajException ;

	/**
	 * @throws BiomajException
	 */
	protected abstract void runUntilMakeProduction() throws BiomajException ;

	/**
	 * run Post-Process Workflow
	 * @throws BiomajException
	 */
	protected abstract void runUntilPostProcess() throws BiomajException ;

	/**
	 * run pre-process workflow
	 * @throws BiomajException
	 */
	protected abstract void runUntilPreProcess() throws BiomajException ;

	/**
	 * run Deployement workflow
	 * @throws BiomajException
	 */
	protected abstract void runUntilDeployment() throws BiomajException ;


	protected abstract void runRebuild() throws BiomajException ; 

	protected abstract void runDeployment() throws BiomajException ;

	protected abstract void runMirror() throws BiomajException ;

	protected abstract void runMakeProduction() throws BiomajException ;

	protected abstract void runPostProcess() throws BiomajException ;

	protected abstract void runPreProcess() throws BiomajException ;
	
	public abstract void runRemoveProcess(String version) throws BiomajException ;
	
	public abstract void runOnlyRemoveProcess() throws BiomajException ;

	protected void runSubTask() throws BiomajException {
		throw new BiomajException("workflowengine.subtask.defined");
	}

	public String getCommandStr() {
		switch (command) {
		case TARGET_ALL: return "ALL";
		case TARGET_UNTIL_MIRROR: return "PRE-PROCESS SYNC";
		case TARGET_UNTIL_DEPLOY:  return "PRE-PROCESS SYNC POST-PROCESSING MOVE";
		case TARGET_UNTIL_POSTPROCESS : return "PRE-PROCESS SYNC POST-PROCESSING";
		case TARGET_UNTIL_PREPROCESS: return "PRE-PROCESSING";
		//case TARGET_PRE_MIRROR_POST: return "PRE-PROCESSING SYNC POST-PROCESSING";
		case TARGET_PREPROCESS: return "PRE-PROCESSING";
		case TARGET_MIRROR: return "SYNC";
		case TARGET_DEPLOY:  return "MOVE";
		case TARGET_POSTPROCESS : return "POST-PROCESSING";
		case TARGET_SUB_TASK: return "SUB-PROCESS";
		case TARGET_REBUILD: return "REBUILD";
		case TARGET_REMOVEPROCESS: return "REMOVE-PROCESS";
		}
		return "COMMAND NO DEFINED:"+getCommand();
	}

	/**
	 * @return  the command
	 * @uml.property  name="command"
	 */
	public int getCommand() {
		return command;
	}



	/**
	 * @param command  the command to set
	 * @uml.property  name="command"
	 */
	public void setCommand(int command) {
		this.command = command;
	}


	@Override
	public void run() {
		try {

			if (bankIsLock(getBiomajBank().getDbName()))
			{
				//initWorkflow();
				//log("A biomaj process running for bank:"+getBiomajBank().getDbName(),Project.MSG_ERR);
				//log("if you think this is a error please delete manually the file:"+getNameLockFile(getBiomajBank().getDbName()),Project.MSG_INFO);


				System.err.println();
				System.err.println();
				System.err.println("A biomaj process running for bank:"+getBiomajBank().getDbName());
				System.err.println("if you think this is a error please delete manually the file:"+getNameLockFile(getBiomajBank().getDbName()));
				System.err.println();
				System.err.println();

				BiomajSession.getInstance().removeBank(getBiomajBank().getDbName());
				BiomajLogger.getInstance().close(getBiomajBank().getDbName());
				//setLock();
				return;
			}
			//super.run();
			//BiomajQueryXmlStateFile bq = new BiomajQueryXmlStateFile();
			//Session session = new Session();
			//bq.getLastSession(getBiomajBank().getDbName(), session);
			
			initWorkflow();
			switch (getCommand()) {
			case TARGET_ALL : runAll();
			break;
			case TARGET_UNTIL_MIRROR : runUntilMirror();
			break;
			case TARGET_UNTIL_PREPROCESS : runUntilPreProcess();
			break;
			case TARGET_UNTIL_POSTPROCESS : runUntilPostProcess();
			break;
			case TARGET_UNTIL_MAKEPROD : runUntilMakeProduction();
			break;
			case TARGET_UNTIL_DEPLOY : runUntilDeployment();
			break;
			case TARGET_MIRROR : runMirror();
			break;
			case TARGET_PREPROCESS : runPreProcess();
			break;
			case TARGET_POSTPROCESS : runPostProcess();
			break;
			case TARGET_DEPLOY : runDeployment();
			break;
			case TARGET_REBUILD : runRebuild();
			break;
			case TARGET_REMOVEPROCESS : runOnlyRemoveProcess();
			break;
			case TARGET_SUB_TASK : runSubTask();
			break;
			default:
				System.err.println("BIOMAJ ERROR ! No command defined");
			}
			endWorkflow();
		}  
		catch (BuildException e) {
			try { endWithErrorWorkflow(null) ;} catch (BiomajException be) { BiomajLogger.getInstance().log(be);} 
			BiomajLogger.getInstance().log(e);
			BiomajLogger.getInstance().close(getBiomajBank().getDbName());
		} catch (BiomajException e) {
			log(e.getLocalizedMessage(), Project.MSG_ERR);
			try { endWithErrorWorkflow(null) ;} catch (BiomajException be) { BiomajLogger.getInstance().log(be);} 
		} catch (Exception e) {
			BiomajLogger.getInstance().log(e);
			log(e.getLocalizedMessage(), Project.MSG_ERR);
			BiomajLogger.getInstance().close(getBiomajBank().getDbName());
			try { endWithErrorWorkflow(null) ;} catch (BiomajException be) { BiomajLogger.getInstance().log(be);} 
		} 
	}

	/**
	 * True if a citrina execution is in current process with the bank specified
	 * False otherwise
	 * @param bankName
	 * @return
	 */
	private Boolean bankIsLock(String bankName) throws BiomajException {
		if (!checkLock)
			return false;
		//another citrina process treat the bank!
		File f = new File(getNameLockFile(bankName));
		if (f.exists())
			return true;

		return false;
	}


	/**
	 * Set a lock for bankName
	 * @param bankName
	 */
	private void setLock() throws BiomajException {
		if (!checkLock)
			return;
		File f = new File(getNameLockFile(getBiomajBank().getDbName()));

		try {
			FileOutputStream fi = new FileOutputStream(f,true);
			fi.write("LOCK".getBytes());
			fi.close();

		} catch (FileNotFoundException e) {

		} catch (IOException e) {
			BiomajLogger.getInstance().log(e);
		}
	}

	/**
	 * Remove a lock for bankName
	 * @param bankName
	 */
	protected void unlock() throws BiomajException {
		if (!checkLock)
			return;
		String bankName = getBiomajBank().getDbName();
		File f = new File(getNameLockFile(bankName));
		f.delete();
	}

	/**
	 * Get the name of lock file,
	 * WARNING : not create the temp file in /tmp because, citrina can run on several computer!
	 * @param bankName
	 */
	public static String getNameLockFile(String bankName) throws BiomajException {
		String tmpDir = BiomajInformation.getInstance().getProperty(BiomajInformation.TMPDIR) + "/";
		File tmp = new File(tmpDir);
		if (!tmp.exists())
			tmp.mkdir();
		return tmpDir + bankName + extensionLock;
	}


	/**
	 * @return  the workWithCurrentDirectory
	 * @uml.property  name="workWithCurrentDirectory"
	 */
	public boolean isWorkWithCurrentDirectory() {
		return workWithCurrentDirectory;
	}


	/**
	 * @param workWithCurrentDirectory  the workWithCurrentDirectory to set
	 * @uml.property  name="workWithCurrentDirectory"
	 */
	public void setWorkWithCurrentDirectory(boolean workWithCurrentDirectory) {
		this.workWithCurrentDirectory = workWithCurrentDirectory;
	}


	/**
	 * @return  the forceForANewUpdate
	 * @uml.property  name="forceForANewUpdate"
	 */
	public boolean isForceForANewUpdate() {
		return forceForANewUpdate;
	}


	/**
	 * @param forceForANewUpdate  the forceForANewUpdate to set
	 * @uml.property  name="forceForANewUpdate"
	 */
	public void setForceForANewUpdate(boolean forceForANewUpdate) {
		this.forceForANewUpdate = forceForANewUpdate;
	}

	public boolean isFromScratch() {
		return fromScratch;
	}

	public void setFromScratch(boolean fromScratch) {
		this.fromScratch = fromScratch;
	}


	public static TreeMap<String, WorkflowEngine> getMapBankNameWE() {
		return mapBankNameWE;
	}

	public static boolean workflowIsInstanciate(String bankName) {
		return getMapBankNameWE().containsKey(bankName);
	}

	public static WorkflowEngine getWorkflow(String bankName) {
		return getMapBankNameWE().get(bankName);
	}


	public static void addReference(String bankName) {
		if (getMapBankRef().containsKey(bankName))
			getMapBankRef().put(bankName, getMapBankRef().get(bankName)+1);
	}

	public static Integer getReference(String bankName) {
		if (getMapBankRef().containsKey(bankName))
			return getMapBankRef().get(bankName);

		return new Integer(0);
	}

	public static void removeWE(String bankName) {
		if (!getMapBankRef().containsKey(bankName))
			return;
		Integer value = getMapBankRef().get(bankName);

		if (value <= 1) { // Removing last reference
			getMapBankRef().remove(bankName);
			getMapBankNameWE().remove(bankName);
		} else { // Not the last reference
			getMapBankRef().put(bankName, value - 1);
		}
	}

	public static TreeMap<String, Integer> getMapBankRef() {
		return mapBankRef;
	}


	public Boolean blockExist(String blockName) {
		return hm_blockIsOk.containsKey(blockName);
	}


	public Boolean blockIsExecute(String blockName) {
		if (hm_blockIsOk.containsKey(blockName))
			return (hm_blockIsOk.get(blockName).availablePermits()<=0);
		return false;
	}

	protected void setBlockExecute(String blockName) throws BiomajException {
		/*		System.out.println(getBiomajBank().getDbName()+"<-- lock by process:"+blockName);
		System.out.println(hm_blockIsOk.size());
		Semaphore s = hm_blockIsOk.get(blockName);

		if ( s==null )
			throw new BiomajException("workflow.error.synchro.null.block",blockName,getBiomajBank().getDbName());

		if (s.availablePermits()<1) {
			throw new BiomajException("workflow.error.synchro.block",blockName,getBiomajBank().getDbName());
		}
		try {
			s.acquire();
		} catch (InterruptedException ie) {
			throw new BiomajException("workflow.error.interrupted.synchro.block");
		}
		System.out.println("--> lock by process:"+blockName);*/
	}

	protected void removeBlockExecute(String blockName) throws BiomajException {
		log(getBiomajBank().getDbName()+"<-- remove block from handler manager:"+blockName,Project.MSG_DEBUG);
		int nbRef = getAllReferences().size();
		log(getBiomajBank().getDbName()+" : Nb ref father:"+nbRef,Project.MSG_DEBUG);
		hm_blockIsOk.get(blockName).release(nbRef);
	}

	protected void removeAllBlockProcess() throws BiomajException {
		log(getBiomajBank().getDbName()+"<-- clear all block from handler manager:",Project.MSG_DEBUG);
		Collection<Semaphore> cs = hm_blockIsOk.values();
		int nbRef = getAllReferences().size();
		log(getBiomajBank().getDbName()+" : Nb ref father:"+nbRef,Project.MSG_DEBUG);
		//On relache le lock au nombre de Pere

		for (Semaphore s : cs) {
			s.release(nbRef);
		}
	}

	public void synchr(String blockName) throws BiomajException {
		log(getBiomajBank().getDbName()+"<-- synchr:"+blockName,Project.MSG_DEBUG);

		//Si le process n est pas lance, c est qu il est ok!
		if (!hm_blockIsOk.containsKey(blockName)) {
			log(getBiomajBank()+getName()+":Process "+blockName+" is ever executed!",Project.MSG_DEBUG);
			return;
		}
		try {
			hm_blockIsOk.get(blockName).acquire();
		} catch (InterruptedException ie) {
			throw new BiomajException("workflow.error.interrupted.synchro.block");
		}
		log("--> synchr:"+blockName,Project.MSG_DEBUG);
	}


	public void free(String blockName) throws BiomajException {
		log(getBiomajBank().getDbName()+"<-- free:"+blockName,Project.MSG_DEBUG);
		int nbRef = getAllReferences().size();
		log(getBiomajBank().getDbName()+"Nb ref father:"+nbRef,Project.MSG_DEBUG);
		hm_blockIsOk.get(blockName).release(nbRef);
		log("--> free:"+blockName,Project.MSG_DEBUG);
	}


	public abstract Vector<WorkflowEngine> getChildren() ;

	/**
	 * Retourne un vecteur de workflowengine qui depende du workflow engine courant:
	 * Si this == RemoteWorkflow Engine le vecteur est vide
	 * 
	 * @return
	 */
	public Vector<WorkflowEngine> getAllReferences() {

		Vector<WorkflowEngine> result = new Vector<WorkflowEngine>();

		for (WorkflowEngine we : mapBankNameWE.values()) {
			if (we.getId()==this.getId())
				continue;
			Vector<WorkflowEngine> vw = we.getChildren();
			for (WorkflowEngine we2 : vw) {
				if (we2.getId()==this.getId())
					result.add(we2);
			}
		}

		return result;

	}


	public String[] getListBlockProcessToRebuild() {
		return listBlockProcessToRebuild;
	}


	public void setListBlockProcessToRebuild(String[] listBlockProcessToRebuild) {
		this.listBlockProcessToRebuild = listBlockProcessToRebuild;
	}


	public String[] getListMetaProcessToRebuild() {
		return listMetaProcessToRebuild;
	}


	public void setListMetaProcessToRebuild(String[] listMetaProcessToRebuild) {
		this.listMetaProcessToRebuild = listMetaProcessToRebuild;
	}


	public String[] getListProcessToRebuild() {
		return listProcessToRebuild;
	}


	public void setListProcessToRebuild(String[] listProcessToRebuild) {
		this.listProcessToRebuild = listProcessToRebuild;
	}
	
	/**
	 * /!\ For testing purpose only /!\
	 */
	public static void initWorkflowEngine() {
		/*
		 * We need to reinit the map when we run successive
		 * instances of biomaj in the same jvm.
		 * For example if we run an update process and a remove
		 * process for the same bank, the same WorklflowEngine object will
		 * be returned, which is not good as mutliple call at the
		 * Thread start method raises an exception. 
		 */
		mapBankNameWE = new TreeMap<String, WorkflowEngine>();
	}

}
