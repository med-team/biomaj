package org.inria.biomaj.exe.options.test;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.tools.ant.Project;
import org.inria.biomaj.exe.bank.BankFactory;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.exe.options.BiomajRemoveBank;
import org.inria.biomaj.exe.workflow.WorkflowEngine;
import org.inria.biomaj.exe.workflow.WorkflowEngineFactory;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.Configuration;
import org.inria.biomaj.sql.SQLConnection;
import org.inria.biomaj.sql.SQLConnectionFactory;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Runs an update process then a remove process
 * and checks that all the related stuff in the bdd
 * and the file system has been removed.
 * Therefore, update option should tested beforehand.
 * 
 * @author rsabas
 *
 */
public class TestRemoveAllOption {

	private static Configuration config = new Configuration();
	private static String bankName = "testbank";

	@BeforeClass
	public static void setup() {
		TestUpdateOption.createTestFiles();
		
		SQLConnectionFactory.setTestMode(true);
		System.out.println("Running update of testbank...");
		BankFactory bf = new BankFactory();
		
		try {
			WorkflowEngine.initWorkflowEngine();
			BiomajUtils.fillConfig(bankName, config);
			BiomajBank bb = bf.createBank(bankName,true);
			
			WorkflowEngineFactory wef = new WorkflowEngineFactory();
			WorkflowEngine we ;
			we = wef.createWorkflow(bb);
			we.setModeConsole(false, Project.MSG_INFO);
			we.setWorkWithCurrentDirectory(false);
			we.setForceForANewUpdate(false);
			we.setFromScratch(false);
			we.setCommand(WorkflowEngine.TARGET_ALL);
			we.start();
			
			we.join(); // wait for thread to end

		} catch (BiomajException e) {
			System.err.println("Biomaj has detected an error! for bank ["+ bankName+"]");
			System.err.println(e.getLocalizedMessage());
			
		} catch (BiomajBuildException bbe) {
			System.err.println("Biomaj has detected an error during the execution of workflow for the bank :"
					+ bankName);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
		}
	}
	
	/**
	 * 1 - Run remove process
	 * 2 - Check that production directories in file system are deleted
	 * 3 - Check that the only update record remaining refers to a remove task
	 * 4 - Check that the productiondirectory record status is set to 'remove'
	 */
	@Test
	public void runTest() {
		// Make sure that we won't handle an existing workflow
		// to avoid IllegalThreadStateException
		WorkflowEngine.initWorkflowEngine();
		BiomajRemoveBank brb = new BiomajRemoveBank();
		brb.setBankName(bankName);
		try {
			brb.executeWithoutUserInteraction(null, true, false).join();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		try {
			BiomajBank b = new BankFactory().createBank(bankName, false);
			String dataDir = (String) b.getPropertiesFromBankFile().get(BiomajConst.dataDirProperty);
			String versionDir = (String) b.getPropertiesFromBankFile().get(BiomajConst.versionDirProperty);

			// Production dirs are deleted
			assertTrue(new File(dataDir + "/" + versionDir).exists() == false);
			// Can't test whether log files are deleted as the remove process itself
			// generates logs...);
			
			try {
				// Check that no more sessiontask
				String query ="SELECT idsessiontask FROM sessionTask WHERE " +
						"taskType='removeprocess' AND idsessionTask IN (" +
						"SELECT ref_idsessionTask FROM session_has_sessionTask WHERE ref_idsession IN " +
						"(SELECT idLastSession FROM updateBank WHERE ref_idconfiguration=(" +
						"SELECT idconfiguration FROM configuration WHERE ref_idbank=(" +
						"SELECT idbank FROM bank WHERE name='" + bankName + "'))))";
				
				SQLConnection conn = SQLConnectionFactory.getConnection();
				Statement stat = conn.getStatement();
				ResultSet rs = conn.executeQuery(query, stat);
				assertTrue(!rs.next());
				SQLConnectionFactory.closeConnection(stat);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			assertTrue(BiomajSQLQuerier.getProductionDirectories(bankName).size() == 0);
			assertTrue(BiomajSQLQuerier.getAvailableProductionDirectories(bankName).size() == 0);

		} catch (BiomajException e) {
			e.printStackTrace();
		}
	}
	
	@AfterClass
	public static void cleanup() {
		BiomajUtils.deleteAll(new File(BiomajUtils.getBiomajRootDirectory() + "/testbanks"));
	}
}
