/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.bank;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;


/**
 * @uml.stereotype  uml_id="Standard::Metaclass" 
 */
public class BankSet extends org.inria.biomaj.exe.bank.BiomajBank {
	
	
	public BankSet(Properties props) {
		super(props);
	}

	public BankSet() {
		
	}
	
	
	@Override
	public boolean check() {
		// TODO Auto-generated method stub
		return false;
	}

	private List<BiomajBank> bankList = new ArrayList<BiomajBank>();

	/**
	 * Getter of the property <tt>listBanks</tt>
	 * @return  Returns the biomajBank.
	 * @uml.property  name="listBanks"
	 */
	public List<BiomajBank> getBankList() {
		return bankList;
	}

	/**
	 * Returns the element at the specified position in this list.
	 * @param index  index of element to return.
	 * @return  the element at the specified position in this list.
	 * @see java.util.List#get(int)
	 * @uml.property  name="listBanks"
	 */
	public BiomajBank getBankAt(int i) {
		return bankList.get(i);
	}

	/**
	 * Returns an iterator over the elements in this list in proper sequence.
	 * @return  an iterator over the elements in this list in proper sequence.
	 * @see java.util.List#iterator()
	 * @uml.property  name="listBanks"
	 */
	public Iterator<BiomajBank> listBanksIterator() {
		return bankList.iterator();
	}

	/**
	 * Returns <tt>true</tt> if this list contains no elements.
	 * @return  <tt>true</tt> if this list contains no elements.
	 * @see java.util.List#isEmpty()
	 * @uml.property  name="listBanks"
	 */
	public boolean bankListIsEmpty() {
		return bankList.isEmpty();
	}

	/**
	 * Returns <tt>true</tt> if this list contains the specified element.
	 * @param element  element whose presence in this list is to be tested.
	 * @return  <tt>true</tt> if this list contains the specified element.
	 * @see java.util.List#contains(Object)
	 * @uml.property  name="listBanks"
	 */
	public boolean containsListBanks(BiomajBank biomajBank) {
		return this.bankList.contains(biomajBank);
	}

	/**
	 * Returns <tt>true</tt> if this list contains all of the elements of the specified collection.
	 * @param elements  collection to be checked for containment in this list.
	 * @return  <tt>true</tt> if this list contains all of the elements of the specified collection.
	 * @see java.util.List#containsAll(Collection)
	 * @uml.property  name="listBanks"
	 */
	public boolean containsAllListBanks(Collection<? extends BiomajBank> listBanks) {
		return bankList.containsAll(listBanks);
	}

	/**
	 * Returns the number of elements in this list.
	 * @return  the number of elements in this list.
	 * @see java.util.List#size()
	 * @uml.property  name="listBanks"
	 */
	public int listBanksSize() {
		return bankList.size();
	}

	/**
	 * Returns an array containing all of the elements in this list in proper sequence.
	 * @return  an array containing all of the elements in this list in proper sequence.
	 * @see java.util.List#toArray()
	 * @uml.property  name="listBanks"
	 */
	public BiomajBank[] listBanksToArray() {
		return bankList.toArray(new BiomajBank[bankList.size()]);
	}

	/**
	 * Returns an array containing all of the elements in this list in proper sequence; the runtime type of the returned array is that of the specified array.
	 * @param a  the array into which the elements of this list are to be stored.
	 * @return  an array containing all of the elements in this list in proper sequence.
	 * @see java.util.List#toArray(Object[])
	 * @uml.property  name="listBanks"
	 */
	public <T extends BiomajBank> T[] listBanksToArray(T[] listBanks) {
		return bankList.toArray(listBanks);
	}

	/**
	 * Inserts the specified element at the specified position in this list (optional operation)
	 * @param index  index at which the specified element is to be inserted.
	 * @param element  element to be inserted.
	 * @see java.util.List#add(int,Object)
	 * @uml.property  name="listBanks"
	 */
	public void addListBanks(int index, BiomajBank biomajBank) {
		this.bankList.add(index, biomajBank);
	}

	/**
	 * Appends the specified element to the end of this list (optional operation).
	 * @param element  element to be appended to this list.
	 * @return  <tt>true</tt> (as per the general contract of the <tt>Collection.add</tt> method).
	 * @see java.util.List#add(Object)
	 * @uml.property  name="listBanks"
	 */
	public boolean addListBanks(BiomajBank biomajBank) {
		return this.bankList.add(biomajBank);
	}

	/**
	 * Removes the element at the specified position in this list (optional operation).
	 * @param index  the index of the element to removed.
	 * @return  the element previously at the specified position.
	 * @see java.util.List#remove(int)
	 * @uml.property  name="listBanks"
	 */
	public Object removeListBanks(int index) {
		return bankList.remove(index);
	}

	/**
	 * Removes the first occurrence in this list of the specified element  (optional operation).
	 * @param element  element to be removed from this list, if present.
	 * @return  <tt>true</tt> if this list contained the specified element.
	 * @see java.util.List#remove(Object)
	 * @uml.property  name="listBanks"
	 */
	public boolean removeListBanks(BiomajBank biomajBank) {
		return this.bankList.remove(biomajBank);
	}

	/**
	 * Removes all of the elements from this list (optional operation).
	 * @see java.util.List#clear()
	 * @uml.property  name="listBanks"
	 */
	public void clearListBanks() {
		bankList.clear();
	}

	/**
	 * Setter of the property <tt>listBanks</tt>
	 * @param listBanks  the biomajBank to set.
	 * @uml.property  name="listBanks"
	 */
	public void setListBanks(List<BiomajBank> listBanks) {
		bankList = listBanks;
	}

}
