/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.options;

import java.io.File;

import org.inria.biomaj.ant.logger.DBWriter;
import org.inria.biomaj.exe.bank.BankFactory;
import org.inria.biomaj.session.bank.Bank;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.Configuration;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * 
 * @author ofilangi
 * @version Biomaj 0.9
 * @since Biomaj 0.8 /Citrina 0.5
 */
public class BiomajImport {

	
	public static void importIntoDB(String bankName) {

		if (bankName == null)
		{
			BiomajLogger.getInstance().log("[Internal Error] : [bankName=null]");
			return;
		}
		
		
		if (BiomajSQLQuerier.getBanks().contains(bankName)) {
			System.out.println("Bank '" + bankName + "' already exists in the database.");
		} else {
			try {
				BankFactory bf = new BankFactory();
				// Permet de tester si la bank est valide
				bf.createBank(bankName,true);
				
				if (BiomajSQLQuerier.getBanks().contains(bankName)) {
					System.out.println("Bank '" + bankName + "' already exists in the database !");
					return;
				}
	
				Configuration config = new Configuration();
				BiomajUtils.fillConfig(bankName, config);
	
				File current = new File(config.getVersionDirectory() + "/current");
	
				if (!current.exists()) {
					System.out.println("No production directory ["
							+ current.getAbsolutePath() + "] exist.");
					return;
				}
	
				Bank xbd = new Bank();
				xbd.setConfig(config);
	
				xbd.fillWithDirectoryVersion(current.getAbsolutePath());
				DBWriter.createContentConfig(config, DBWriter.getBankId(config.getName()));
				DBWriter.createUpdateBank(xbd, config.getId(),-1);
	//			DBWriter.updateStateSession(xbd, true, true);
				DBWriter.updateStateSessionWithProductionDir(xbd);
				System.out.println("[" + bankName + "]");
	
			} catch (BiomajException be) {
				BiomajLogger.getInstance().log(be);
				System.err.println("\n" + be.getMessage());
				return;
			}
		}
	}
	
	

}
