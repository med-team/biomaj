/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.bank;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.inria.biomaj.ant.task.net.RemoteCommand;
import org.inria.biomaj.session.dependency.GraphElement;
import org.inria.biomaj.session.dependency.GraphEvaluator;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;

/**
 * 18/06/13 [#15914] ant 1.9 return objects in getProperties instead of String
 * @author osallou
 *
 */

public class BankFactory {


	public BiomajBank createBank(String bankName,boolean verbose) throws BiomajException {
		return createBank(bankName,false,verbose);
	}

	
	public BiomajBank createBank(String bankName,boolean testMode,boolean verbose) throws BiomajException {
		
		if ((bankName==null)||(bankName.trim().compareTo("")==0))
			throw new BiomajException("bankfactory.error.banks.blank",bankName);

		Properties bankProperties = new Properties();

		File global_properties = new File(BiomajInformation.getInstance().getProperty(BiomajInformation.WORKFLOWSDIR)+"/"+BiomajConst.globalProperties);
		addProperties(bankProperties,global_properties);

		/*
		File parent = new File(BiomajInformation.getInstance().getProperty(BiomajInformation.WORKFLOWSDIR));
		for (File f : parent.listFiles()) {
			if (f.getName().equalsIgnoreCase(bankName + ".properties")) {
				file = f;
				break;
			}
		}*/
		
		File file = getBankPath(bankName);
		
//		File file = new File(BiomajInformation.getInstance().getProperty(BiomajInformation.WORKFLOWSDIR)+"/"+bankName+".properties");
		
		
		if (file == null) {
			throw new BiomajException("bankfactory.error.banks.exist",bankName);
		}

		addProperties(bankProperties, file);

		// 0.9.3.0 notion d include dans le fichier de propriete :
		while (bankProperties.containsKey(BiomajConst.includePropertiesProperty)) {
			String[] filesToInclude = bankProperties.getProperty(BiomajConst.includePropertiesProperty).split(",");
			bankProperties.remove(BiomajConst.includePropertiesProperty);
			String base = BiomajInformation.getInstance().getProperty(BiomajInformation.WORKFLOWSDIR);
//			String base = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf('/'));
			
			for ( String f : filesToInclude ) {
				addProperties(bankProperties, new File(base+"/"+f));
			}
		}


		//postulat : A bank can't be a virtual and depends bank !
		boolean isVirtual = false;
		boolean isDepends = false;

		if (bankProperties.containsKey(BiomajConst.virtualListProperty))
		{
			isVirtual = true;
			if (bankProperties.getProperty(BiomajConst.virtualListProperty).trim().compareTo("")==0)
			{
				isVirtual = false;
				bankProperties.remove(BiomajConst.virtualListProperty);
			}
		}

		if (bankProperties.containsKey(BiomajConst.dependsListProperty))
		{
			isDepends = true;
			if (bankProperties.getProperty(BiomajConst.dependsListProperty).trim().compareTo("")==0)
			{
				isDepends = false;
				bankProperties.remove(BiomajConst.dependsListProperty);
			}
		}

		if (bankProperties.containsKey(BiomajConst.mailSmtpHostProperty)||
				bankProperties.containsKey(BiomajConst.mailAdminProperty)||
				bankProperties.containsKey(BiomajConst.mailFromProperty)) {


			if (!bankProperties.containsKey(BiomajConst.mailSmtpHostProperty)) {
				System.err.println(BiomajConst.mailSmtpHostProperty+" are not defined in global properties. ["+
						BiomajConst.mailAdminProperty+"="+bankProperties.getProperty(BiomajConst.mailAdminProperty)+"]");
				throw new BiomajException("biomaj.property.define.error",bankName,BiomajConst.mailSmtpHostProperty);

			}

			if (!bankProperties.containsKey(BiomajConst.mailAdminProperty)) {
				System.err.println(BiomajConst.mailAdminProperty+" are not defined in global properties. ["+
						BiomajConst.mailAdminProperty+"="+bankProperties.getProperty(BiomajConst.mailAdminProperty)+"]");
				throw new BiomajException("biomaj.property.define.error",bankName,BiomajConst.mailAdminProperty);
			}

			if (!bankProperties.containsKey(BiomajConst.mailFromProperty)) {
				System.err.println(BiomajConst.mailFromProperty+" are not defined in global properties. ["+
						BiomajConst.mailFromProperty+"="+bankProperties.getProperty(BiomajConst.mailFromProperty)+"]");
				throw new BiomajException("biomaj.property.define.error",bankName,BiomajConst.mailAdminProperty);
			}
		}

		if (isDepends) {
			constructAndCheckPropertiesForDependantBank(bankProperties,file.getAbsolutePath(),verbose);
		}

		if (!isVirtual)
		{
			boolean isOk = checkProperties(bankProperties,file.getAbsolutePath(),verbose);
			if (!isOk)
				throw new BiomajException("bankfactory.error.property",bankName);
		}

		if (isVirtual&&isDepends)
			throw new BiomajException("bankfactory.error.incompatible.virtual.depends",bankName);//"bankfactory.error.incompatible.virtual.depends",bankName);

		if (isVirtual) {
			return createVirtualBank(bankProperties,testMode);
		} else if (isDepends) {
			return createDependsBank(bankProperties,testMode);
		}

		//bankProperties.list(System.out);

		return new RemoteBank(bankProperties);

	}

	
	/**
	 * Searches for given bank in the workflow.dir directory and subdirectories (users...)
	 * and return corresponding file.
	 * 
	 * @return
	 */
	public static File getBankPath(String name) {
		String newName = name.endsWith(".properties") ? name : name + ".properties";
		List<File> dirsToBrowse = new ArrayList<File>();
		try {
			File dir = new File(BiomajInformation.getInstance().getProperty(BiomajInformation.WORKFLOWSDIR));
			if (dir.isDirectory()) {
				for (File f : dir.listFiles()) {
					if (!f.isDirectory()) {
						if (f.getName().equalsIgnoreCase(newName))
							return f;
					} else {
						dirsToBrowse.add(f);
					}
				}
			} else {
				return null;
			}
		} catch (BiomajException e) {
			e.printStackTrace();
			return null;
		}
		
		// If was not found under root, look in subdirs
		for (File dir : dirsToBrowse) {
			for (File f : dir.listFiles()) {
				if (!f.isDirectory() && f.getName().equalsIgnoreCase(newName))
					return f;
			}
		}
		
		return null;
	}

	public BiomajBank createBankSet(String[] banks,boolean testMode) throws BiomajException {
		BankSet b = new BankSet();

		for (int i=0; i<banks.length;i++) {
			b.addListBanks(createBank(banks[i],testMode));
		}

		return b;
	}


	protected BiomajBank createVirtualBank(Properties props,boolean testMode) throws BiomajException {

		VirtualBank b = new VirtualBank(props);

		String listBank = props.getProperty(BiomajConst.virtualListProperty);

		if (listBank == null)
			throw new NullPointerException("virtual bank property bad defined !");

		String[] banks = listBank.split(",");

		for (int i=0; i<banks.length;i++) {
			b.addListBanks(createBank(banks[i],testMode));
		}

		return b;
	}

	protected BiomajBank createDependsBank(Properties props, boolean testMode) throws BiomajException {

		GraphElement root = new GraphElement(null, props.getProperty(BiomajConst.dbNameProperty));
		GraphEvaluator eval = new GraphEvaluator(root);
		buildGraph(eval, root, props);
		
		if (eval.isValid()) {
			ComputedBank b = new ComputedBank(props);
	
			String listBank = props.getProperty(BiomajConst.dependsListProperty);
	
			if (listBank == null)
				throw new NullPointerException("virtual bank property not defined !");
	
			String[] banks = listBank.split(",");
	
			for (int i=0; i<banks.length;i++) {
				b.addListBanks(createBank(banks[i],testMode));
			}
	
			return b;
		 } else {
			 System.err.println("*** Redundant dependency for bank '" + props.getProperty(BiomajConst.dbNameProperty) + "'.");
			 return null;
		 }
	}
	
	private void buildGraph(GraphEvaluator eval, GraphElement current, Properties bank) {
		if (bank.containsKey(BiomajConst.dependsListProperty)) {
			String dependencies = bank.getProperty(BiomajConst.dependsListProperty).trim();
			if (!dependencies.isEmpty()) {
				for (String bankName : dependencies.split(",")) {
					GraphElement g = new GraphElement(current, bankName);
					if (eval.isValid()) {
						Properties childProps = new Properties();
						try {
							
//							File childBank = new File(BiomajInformation.getInstance().getProperty(BiomajInformation.WORKFLOWSDIR) + "/" + bankName + ".properties");
							File childBank = getBankPath(bankName);
							if (childBank!=null && childBank.exists()) {
								FileReader reader = new FileReader(childBank);
								childProps.load(reader);
								reader.close();
								buildGraph(eval, g, childProps);
							} else {
								System.err.println("Error in bank dependencies. '" + bankName + "' does not exist.");
								return;
							}
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	protected void addProperties(Properties in_out_props,File toAdd) throws BiomajException {

		if (toAdd == null)
			throw new BiomajException("error.dev","var fileToAdd is null !");

		if (!toAdd.exists()) {
			throw new BiomajException("io.error","file ["+toAdd.getAbsolutePath()+"] does not exist!");
		}

		try {
			Properties bankProperties = new Properties();
			FileInputStream fis = new FileInputStream(toAdd);
			bankProperties.load(fis);
			fis.close();
			Enumeration<?> keys = bankProperties.keys();
			while (keys.hasMoreElements()) {
				String k = (String)keys.nextElement();
				if (in_out_props.contains(k))
					in_out_props.remove(k);

				in_out_props.put(k, getRecursiveProperty( in_out_props,bankProperties,k,(String)bankProperties.get(k)));
			}

		} catch (FileNotFoundException e) {
			System.err.println("ERROR BIOMAJ file not found:"+e.getMessage());
		}	catch (IOException ioe) {
			System.err.println("ERROR BIOMAJ: ioexception"+ioe.getMessage());
		}
	}

	private String getRecursiveProperty(Properties prop1,Properties prop2,String keyOrigine,String value) throws BiomajException {

		if (value == null || value.compareTo("")==0)
			return "";

		//dans le cas des arguments d'un process on n'effactue pas de verif
		if (keyOrigine.endsWith(".args"))
			return value;

		if (value.startsWith("${"))
		{
			String key = value.replace("${", "");
			key = key.replace("}", "");

			if (key.trim().compareTo(keyOrigine)==0)
				throw new BiomajException("bankfactory.recursive.key",keyOrigine);

			String val ="" ;
			if (prop1.containsKey(key)) {
				val = (String) prop1.get(key);
			} else if (prop2.containsKey(key)) {
				val = (String) prop2.get(key);
			} else {
				String name = "";
				if (prop1.containsKey(BiomajConst.dbNameProperty))
					name = prop1.getProperty(BiomajConst.dbNameProperty);
				else if (prop2.containsKey(BiomajConst.dbNameProperty))
					name = prop2.getProperty(BiomajConst.dbNameProperty);
				throw new BiomajException("biomaj.property.define.error",name,key);
			}			

			return getRecursiveProperty(prop1,prop2,key,val);
		}

		return value;
	}

	public static String getRecursiveProperty(Hashtable<String, Object> prop,String keyOrigine) throws BiomajException {

		if ((prop == null)||(keyOrigine == null))
			throw new BiomajException("error.dev","Properties data are not set.");

		if (!prop.containsKey(keyOrigine)) {
			String name = "";
			if (prop.containsKey(BiomajConst.dbNameProperty))
				name = (String) prop.get(BiomajConst.dbNameProperty);
			throw new BiomajException("biomaj.property.define.error",name,keyOrigine);
		}

		String value = (String) prop.get(keyOrigine);


		if (value.startsWith("${"))
		{
			String key = value.replace("${", "");
			key = key.replace("}", "");

			if (key.trim().compareTo(keyOrigine)==0)
				throw new BiomajException("bankfactory.recursive.key",keyOrigine);

			if (prop.containsKey(key)) {
				return getRecursiveProperty(prop,key);
			} 	
		}

		return value;
	}

	protected void checkRegExp(Properties bankProperties,String property,String fileName) throws BiomajException {

		if (bankProperties.containsKey(property))
		{
			String value = bankProperties.getProperty(property);
			String[] values = value.split("\\s");
			String val="";
			try {
				for (int i=0;i<values.length;i++) {
					val = values[i];
					Pattern.compile(val);
				}
			} catch (PatternSyntaxException pse) {
				System.err.println(pse.getMessage());
				throw new BiomajException("bankfactory.error.property.regexpr",val,property,fileName);
			}
		}

	}

	protected void checkProperty(Properties bankProperties,String property,String fileName) throws BiomajException {

		if (!bankProperties.containsKey(property) || (bankProperties.get(property).toString().trim().compareTo("")==0))
			throw new BiomajException("bankfactory.error.property.exist",property,fileName);

	}

	protected void checkProcesses(Properties bankProperties,String block,String property) throws BiomajException {

		if ((!bankProperties.containsKey(property)&&(block==null)))
			return;

		String dbName =  bankProperties.getProperty(BiomajConst.dbNameProperty);

		if ((!bankProperties.containsKey(property)&&(block!=null)))
			throw new BiomajException("bankfactory.error.block.dbproperty",dbName,block);

		String lMeta = ((String)bankProperties.get(property));

		if (lMeta.trim().compareTo("")==0)
			return;

		String[] listMeta = lMeta.split(",");
		for (String metaProcess : listMeta) {

			if (!bankProperties.containsKey(metaProcess))
				throw new BiomajException("bankfactory.error.metaprocess.exist",dbName,metaProcess);

			String list = ((String)bankProperties.get(metaProcess));

			if (list.trim().compareTo("")==0)
				throw new BiomajException("bankfactory.error.metaprocess.empty",dbName,metaProcess);

			String[] listProcess = list.split(",");


			for (String process: listProcess) {

				if (!bankProperties.containsKey(process+".name"))
					throw new BiomajException("bankfactory.error.process.exist",dbName,process+".name");
				if (!bankProperties.containsKey(process+".exe"))
					throw new BiomajException("bankfactory.error.process.exist",dbName,process+".exe");
				if (!bankProperties.containsKey(process+".args"))
					throw new BiomajException("bankfactory.error.process.exist",dbName,process+".args");
				if (!bankProperties.containsKey(process+".desc"))
					throw new BiomajException("bankfactory.error.process.exist",dbName,process+".desc");
				if (!bankProperties.containsKey(process+".type"))
					throw new BiomajException("bankfactory.error.process.exist",dbName,process+".type");
			}
		}
	}


	protected void checkUnicityProcessName(Properties bankProperties) throws BiomajException {
		ArrayList<String> blocks = new ArrayList<String>();
		ArrayList<String> metas =  new ArrayList<String>();
		ArrayList<String> processes =  new ArrayList<String>();

		if (bankProperties.containsKey(BiomajConst.blockPostprocessProperty)) {
			Collections.addAll(blocks, ((String)bankProperties.get(BiomajConst.blockPostprocessProperty)).split(","));
		} else {
			if (bankProperties.containsKey(BiomajConst.dbPostProcessProperty)) {
				Collections.addAll(metas,((String)bankProperties.get(BiomajConst.dbPostProcessProperty)).split(","));
			}
		}

		for (String block : blocks) {
			if (bankProperties.containsKey(block+"."+BiomajConst.dbPostProcessProperty))
				Collections.addAll(metas, ((String)bankProperties.get(block+"."+BiomajConst.dbPostProcessProperty)).split(","));
		}

		for (String meta : metas) {
			if (bankProperties.containsKey(meta))
				Collections.addAll(processes, ((String)bankProperties.get(meta)).split(","));
		}

		if (blocks.size()>0) {
			for (String block : blocks) {
				for (String meta : metas) {
					for (String proc : processes) {
						if (proc.compareTo(meta)==0) {
							throw new BiomajException("bankfactory.error.meta-process.name",proc);
						}
						if (proc.compareTo(block)==0) {
							throw new BiomajException("bankfactory.error.block-process.name",proc);
						}
						if (block.compareTo(meta)==0) {
							throw new BiomajException("bankfactory.error.block-meta.name",meta);
						}
					}
				}
			}
		} else {
			for (String meta : metas) {
				for (String proc : processes) {
					if (proc.compareTo(meta)==0) {
						throw new BiomajException("bankfactory.error.meta-process.name",proc);
					}
				}
			}
		}




	}

	protected boolean constructAndCheckPropertiesForDependantBank(Properties bankProperties,String fileName,boolean verbose) throws BiomajException {

		// la banque est forcement en mode local
		bankProperties.setProperty(BiomajConst.serverProperty, "localhost");
		bankProperties.setProperty(BiomajConst.protocolProperty, RemoteCommand.LOCAL_PROTOCOL);

//		String[] list = bankProperties.getProperty(BiomajConst.dependsListProperty).split(",");

		if (!bankProperties.containsKey(BiomajConst.flatRepositoryProperty)) {
			if (verbose)
				System.out.println("Default value for property "+BiomajConst.flatRepositoryProperty+" [/flat]");
		}

		if (!bankProperties.containsKey(BiomajConst.blockPostprocessProperty)) {
			if (verbose) {
				System.out.println();
				System.out.println("No property "+BiomajConst.blockPostprocessProperty+" defined");
			}
		}

//		if (list.length>1) {
//			if (verbose)
//				System.err.println("ERREUR DEPENDENCE");
//			return false;
//		}
		return true;
	}

	protected boolean checkProperties(Properties bankProperties,String fileName,boolean verbose) throws BiomajException {

		checkProperty(bankProperties, BiomajConst.dbFullNameProperty,fileName);
		checkProperty(bankProperties, BiomajConst.dbNameProperty,fileName);
		String dbName = bankProperties.getProperty(BiomajConst.dbNameProperty);

		//Contrainte sur dbname

		/*if (dbName.contains(BmajVersionManagement.SEP_NUMBER_OF_RELEASE_VERSION))
		{
			System.err.println("dbname ["+dbName+"] can't contains ["+BmajVersionManagement.SEP_NUMBER_OF_RELEASE_VERSION+"] characters");
			throw new BiomajException("kill.application",BiomajConst.dbNameProperty,fileName);
		}*/

		if (!bankProperties.containsKey(BiomajConst.dependsListProperty))
			checkProperty(bankProperties, BiomajConst.remoteDirProperty,fileName);
		
		checkProperty(bankProperties, BiomajConst.dataDirProperty,fileName);
		//checkProperty(bankProperties, BiomajConst.offlineDirProperty,fileName);
		if (!bankProperties.containsKey(BiomajConst.offlineDirProperty) ||
				bankProperties.getProperty(BiomajConst.offlineDirProperty).trim().isEmpty()) {
			bankProperties.setProperty(BiomajConst.offlineDirProperty, dbName+"_tmp");
			if (verbose)
				System.out.println("offline directory not defined. default value["+bankProperties.getProperty(BiomajConst.offlineDirProperty)+"]");
		}

		checkProperty(bankProperties, BiomajConst.localFilesProperty,fileName);
		checkProperty(bankProperties, BiomajConst.frequencyProperty,fileName);

		if (!bankProperties.containsKey(BiomajConst.localFilesExcludedProperty)) {
			bankProperties.setProperty(BiomajConst.localFilesExcludedProperty, "");
		}

		
		if (!bankProperties.containsKey(BiomajConst.dependsListProperty)
				&& !bankProperties.get(BiomajConst.protocolProperty).equals("directhttp")) {
			checkProperty(bankProperties, BiomajConst.protocolProperty,fileName);
			checkProperty(bankProperties, BiomajConst.remoteFilesProperty,fileName);
		}

		checkProperty(bankProperties, BiomajConst.keepOldVersionProperty,fileName);
		try {
			Integer k = Integer.valueOf((String)bankProperties.get(BiomajConst.keepOldVersionProperty));

			if (k<0) {
				if (verbose)
					System.err.println("property keep.old.version must be positive ["+(String)bankProperties.get(BiomajConst.keepOldVersionProperty)+"].");
				return false;
			}
		} catch (NumberFormatException ne) {
			System.err.println("property keep.old.version must be a positive number ["+(String)bankProperties.get(BiomajConst.keepOldVersionProperty)+"].");
		}
		//checkProperty(bankProperties, BiomajConst.versionDirProperty,fileName);
		if (!bankProperties.containsKey(BiomajConst.versionDirProperty) || 
				bankProperties.getProperty(BiomajConst.versionDirProperty).trim().isEmpty()) {
			bankProperties.setProperty(BiomajConst.versionDirProperty, dbName);
			if (verbose)
				System.out.println("version directory not defined. default value["+bankProperties.getProperty(BiomajConst.versionDirProperty)+"]");
		}

		checkProperty(bankProperties, BiomajConst.productionDirChmodProperty,fileName);

		if (bankProperties.contains(BiomajConst.remoteDirProperty) &&
				!bankProperties.getProperty(BiomajConst.remoteFilesProperty).endsWith(BiomajConst.regexpAll))
			checkRegExp(bankProperties, BiomajConst.remoteFilesProperty,fileName);
		
		checkRegExp(bankProperties, BiomajConst.localFilesProperty,fileName);
		checkRegExp(bankProperties, BiomajConst.localFilesExcludedProperty,fileName);

		if (bankProperties.containsKey(BiomajConst.releaseRegExpProperty))
			checkRegExp(bankProperties, BiomajConst.releaseRegExpProperty,fileName);
		/*
		if (verbose) {
		//System.out.println("Check correspondances with\n"+BiomajConst.remoteFilesProperty+"["+bankProperties.getProperty(BiomajConst.remoteFilesProperty)+"]\n"
			//	+BiomajConst.localFilesProperty+"["+bankProperties.getProperty(BiomajConst.localFilesProperty)+"]");
		//System.out.println("each expression of local.files has to finished with '$' to no match with remote.files!");
		}
		 */
		checkProcesses(bankProperties,null,BiomajConst.dbPreProcessProperty);

		if ((!bankProperties.containsKey(BiomajConst.blockPostprocessProperty))||(bankProperties.getProperty(BiomajConst.blockPostprocessProperty).trim().compareTo("")==0))
			checkProcesses(bankProperties,null,BiomajConst.dbPostProcessProperty);
		else {
			String[] lBlocks = bankProperties.getProperty(BiomajConst.blockPostprocessProperty).split(",");
			for (String block : lBlocks) 
				checkProcesses(bankProperties,block,block+"."+BiomajConst.dbPostProcessProperty);
		}

		checkUnicityProcessName(bankProperties);

		//Ajout 17/04/2007 O.Filangi - no.extract initialiser a faux si non initi dans le properties
		if (!bankProperties.containsKey(BiomajConst.noExtractProperty))
			bankProperties.setProperty(BiomajConst.noExtractProperty, "false");
//		Ajout 25/07/2007 O.Filangi server=localhost si protocol=copy
		if (bankProperties.containsKey(BiomajConst.protocolProperty)&&(bankProperties.getProperty(BiomajConst.protocolProperty).compareTo(RemoteCommand.LOCAL_PROTOCOL)==0)) {
			bankProperties.remove(BiomajConst.serverProperty);
			bankProperties.setProperty(BiomajConst.serverProperty, "localhost");
		}

		//Ajout d'un test sur la construction du nom du serveur : si celui-ci contient ://, property not valid!
		if ((!bankProperties.containsKey(BiomajConst.dependsListProperty))&&(!bankProperties.containsKey(BiomajConst.serverProperty))) {
			if (verbose)
				System.err.println("server property is not set.");
			return false;
		}

		String value = bankProperties.getProperty(BiomajConst.serverProperty);
		if (value.contains("://")) {
			if (verbose)
				System.err.println("value for server property is not correct:["+value+"] (protocol must not be specified like ftp:// or http://)");
			return false;
		}


		if (!bankProperties.containsKey(BiomajConst.logFilesProperty))
		{
			System.err.println("You have to define property ["+BiomajConst.logFilesProperty+"=true|false].");
			return false;
		}


		return true;
	}

}

