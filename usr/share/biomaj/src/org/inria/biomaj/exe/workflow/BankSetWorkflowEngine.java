/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.workflow;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import org.apache.tools.ant.Project;
import org.inria.biomaj.exe.bank.BankSet;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;


public abstract class BankSetWorkflowEngine extends WorkflowEngine {

	//	Les sous workflows executes!
	/**
	 * @uml.property  name="lThread"
	 * @uml.associationEnd  multiplicity="(0 -1)"
	 */
	private WorkflowEngine[] threads = new WorkflowEngine[0];
	/**
	 * @uml.property  name="withConsole"
	 */
	private boolean withConsole = true;
	/**
	 * @uml.property  name="mode"
	 */
	private int  mode;
	
	private static List<String> executedBanks = new ArrayList<String>();
	
	public BankSetWorkflowEngine(BiomajBank bank) {
		super(bank);
	}

	@Override
	public void setModeConsole(boolean withConsole, int mode) {
		this.withConsole = withConsole;
		this.mode = mode;
	}
	
	protected void initChildren() throws BiomajException{
		WorkflowEngineFactory wef = new WorkflowEngineFactory();

		BankSet bs = (BankSet)getBiomajBank();
		threads = new WorkflowEngine[bs.getBankList().size()];
		int i = 0;
		for (BiomajBank b : bs.getBankList()) {
			String lock = BiomajInformation.getInstance().getProperty(BiomajInformation.TMPDIR) + "/" + b.getDbName() + ".lock";
			if (new File(lock).exists())
				throw new BiomajException(new Exception("A child bank is already updating : " + b.getDbName()));
			
			WorkflowEngine we ;
			we = wef.createWorkflow(b);
			we.setModeConsole(withConsole, mode);
			
			threads[i++] = we;
		}
	}
	
	synchronized protected void startChildren(int target) throws BiomajException {
		initChildren();
		for (int i=0;i<threads.length;i++) {
			if ((!threads[i].isAlive())&&(threads[i].getState()==State.NEW)) {
				threads[i].setCommand(target);
				threads[i].start();
			}
		}
	}
	
	/**
	 * Starts the registered children.
	 * 
	 * @return true if any of the children was updated
	 * @throws BiomajException
	 */
	synchronized protected boolean startChildren() throws BiomajException {
		initChildren();
		for (int i = 0; i < threads.length; i++) {
			if (!executedBanks.contains(threads[i].getBiomajBank().getDbName()) && 
					!threads[i].isAlive() && threads[i].getState().equals(State.NEW)) {
				executedBanks.add(threads[i].getBiomajBank().getDbName());
				threads[i].setCommand(WorkflowEngine.TARGET_ALL);
				threads[i].start();
			}
		}
		
		boolean error = false;
		// Wait for threads to die
		for (int i = 0; i < threads.length; i++) {
			try {
				threads[i].join();
				if (!error) {
					error = threads[i].isErrorOnWorkflow();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if (error) {
			throw new BiomajException(new Exception("Child bank update failed"));
		}
		

		/*
		// Wait for threads to pause after post processing
		for (int i = 0; i < threads.length; i++) {
			while (threads[i].isAlive() && !threads[i].isWaiting()) {
				try {
					TimeUnit.MILLISECONDS.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}*/
		
		// Get update status of children
		boolean updated = false;
		for (int i = 0; i < threads.length; i++) {
			if (threads[i] instanceof RemoteBankWorkflowEngine) {
				if (((RemoteBankWorkflowEngine) threads[i]).getListener().getLogger().getBank().isUpdate()) {
					updated = true;
					break;
				}
			}
		}
		
		return updated;
	}
	
	protected synchronized void resumeChildren() throws BiomajException {
//		initChildren();
		for (int i = 0; i < threads.length; i++) {
			if (threads[i].isAlive() && threads[i].getState().equals(State.WAITING)) {
				log("Resuming thread for " + threads[i].getBiomajBank().getDbName(), Project.MSG_DEBUG);
				synchronized (threads[i]) {
					threads[i].notify();
				}
				
			} else {
				log("Could not notify...", Project.MSG_ERR);
				
				log("Name : " + threads[i].getBiomajBank().getDbName(), Project.MSG_ERR);
				log("Alive : " + threads[i].isAlive(), Project.MSG_ERR);
				log("State : " + threads[i].getState().toString(), Project.MSG_ERR);
			}
		}
		
		// Wait for threads death
		for (int i = 0; i < threads.length; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	protected void synchronizeChildren() throws BiomajException {
		for (int i=0;i<threads.length;i++) {
			threads[i].synchr();
		}
		
		for (int i=0;i<threads.length;i++) {
			while (threads[i].isErrorOnWorkflow()==null) {} // Wait for this to return something (false or true)
			if (threads[i].isErrorOnWorkflow())
				throw new BiomajException("kill.application.withoutlog");
		}
		
		for (int i=0;i<threads.length;i++) {
			WorkflowEngine.removeWE(threads[i].getBiomajBank().getDbName());
		}
	}
	
	/**
	 * Synchronisation sur un block de bank....
	 * @param bank
	 * @param block
	 * @throws BiomajException
	 */
	protected void synchronizeFils(String bank,String block) throws BiomajException {
		
		WorkflowEngine we = null ;
		for (WorkflowEngine w : threads) {
			if (w.getBiomajBank().getDbName().compareTo(bank)==0) {
				we = w;
				break;
			}
		}
		
		if (we == null)
			throw new BiomajException("bankset.workflow.error.name.bank.synchro",bank,block);
		
		if (!we.blockExist(block))
			throw new BiomajException("bankset.workflow.error.name.block.synchro",bank,block);
		
		we.synchr(block);
		
		for (int i=0;i<threads.length;i++) {
			if ((threads[i].isErrorOnWorkflow()!=null)&&(threads[i].isErrorOnWorkflow()))
				throw new BiomajException("kill.application");
		}
	}

	@Override
	protected void endWithErrorWorkflow(Exception e) throws BiomajException {
		super.endWithErrorWorkflow(e);
		/*for (int i=0;i<lThread.length;i++) {
			lThread[i].endWithErrorWorkflow(e);
		}*/
	}



	@Override
	public void log(String message, int priority) {
		if (threads == null) {
			System.out.println(message);
			return;
		}
		
		for (int i=0;i<threads.length;i++) {
			if (threads[i] != null)
				threads[i].log(message,priority);
		}
		
	}
	
	
	protected String getBlockBeforeAll() {
		
		String lBlocks = getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.blockPostprocessProperty);
		
		if ((lBlocks == null) || (lBlocks.trim().compareTo("")==0) ) {
			return "";
		}
		
		String[] llB = lBlocks.split(",");
		String result = "";
		boolean stop = true ;
		for (String b : llB) {
			stop = true ;
			if (getBiomajBank().getPropertiesFromBankFile().containsKey(b+".dep")) {
				String dep =  getBiomajBank().getPropertiesFromBankFile().getProperty(b+".dep");
				if (dep.trim().compareTo("before.all")==0)
					{
					result=result+b+",";
					stop = false ;
					}
			} 
			if (stop) {
				if (result.compareTo("")!=0)
					return result.substring(0, result.length()-1);
				
				return result;
			}
				
		}
		return result;
	}
	
	protected String getBlockAfterAll() {
		
		String lBlocks = getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.blockPostprocessProperty);
		
		if ((lBlocks == null) || (lBlocks.trim().compareTo("")==0) ) {
			return "";
		}
		
		String[] llB = lBlocks.split(",");
		String result = "";
		boolean afterAll = false;
		for (String b : llB) {
			if (afterAll||((!getBiomajBank().getPropertiesFromBankFile().containsKey(b+".dep"))
							||(getBiomajBank().getPropertiesFromBankFile().getProperty(b+".dep").trim().compareTo("after.all")==0))) {
				afterAll = true;
				result=result+b+",";
			}
		}
		
		if (result.compareTo("")!=0)
			return result.substring(0, result.length()-1);
		
		return result;
	}

	
	protected String getBankAndBlockChild(int index) {
		
		String lBlocks = getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.blockPostprocessProperty);
		
		if ((lBlocks == null) || (lBlocks.trim().compareTo("")==0) ) {
			return null;
		}
		
		String[] llB = lBlocks.split(",");
		int compteur = 0;
		for (String b : llB) {
			if (getBiomajBank().getPropertiesFromBankFile().containsKey(b+".dep")) {
				String dep =  getBiomajBank().getPropertiesFromBankFile().getProperty(b+".dep");
				if (dep.trim().compareTo("before.all")==0)
					continue;
				if (dep.trim().compareTo("after.all")==0)
					return null;
				if (compteur == index)
					return dep.trim();
				compteur++;
			} else
				return null; // pas de dependance donc after.all
		}
		return null;
	}
	
	protected String getBlockDependsChild(int index) {
		
		String lBlocks = getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.blockPostprocessProperty);
		
		if ((lBlocks == null) || (lBlocks.trim().compareTo("")==0) ) {
			return "";
		}
		
		String[] llB = lBlocks.split(",");
		int compteur = 0;
		for (String b : llB) {
			if (getBiomajBank().getPropertiesFromBankFile().containsKey(b+".dep")) {
				String dep =  getBiomajBank().getPropertiesFromBankFile().getProperty(b+".dep");
				if (dep.trim().compareTo("before.all")==0)
					continue;
				if (dep.trim().compareTo("after.all")==0)
					return null;
				if (compteur == index)
					return b;
				compteur++;
			} else
				return null; // pas de dependnace donc after.all
		}
		return null;
	}

	@Override
	public Vector<WorkflowEngine> getChildren() {
		Vector<WorkflowEngine> we = new Vector<WorkflowEngine>();
		Collections.addAll(we, threads);
		return we;
	}
	
}
