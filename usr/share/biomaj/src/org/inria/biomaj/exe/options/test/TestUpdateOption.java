package org.inria.biomaj.exe.options.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Map;

import org.apache.tools.ant.Project;
import org.inria.biomaj.exe.bank.BankFactory;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.exe.workflow.WorkflowEngine;
import org.inria.biomaj.exe.workflow.WorkflowEngineFactory;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.Configuration;
import org.inria.biomaj.sql.SQLConnectionFactory;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * 
 * Runs a biomaj update process for a specific test bank
 * and checks that :
 * 	- required records have been inserted in the db
 * 	- required files have been downloaded
 * 
 * @author rsabas
 *
 */
public class TestUpdateOption {
	
	private static Configuration config = new Configuration();
	private static final String FILE_SEP = System.getProperty("file.separator");
	private static final String DIR = BiomajUtils.getBiomajRootDirectory() + "/testbanks";
	private static String bankName = "testbank";
	private static BiomajBank bb;
	
	public static void createTestFiles() {
		File dir =  new File(DIR);
		BiomajUtils.deleteAll(dir);
		dir.mkdir();
		
		try {
			PrintWriter pw = new PrintWriter(new File(DIR + "/test1.txt"));
			pw.println("blabliblo");
			pw.close();
			
			pw = new PrintWriter(new File(DIR + "/test2.txt"));
			pw.print("plapliplo");
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@BeforeClass
	public static void setup() {
		SQLConnectionFactory.setTestMode(true);
		createTestFiles();
		System.out.println("Running update of testbank...");
		BankFactory bf = new BankFactory();
		
		try {
			WorkflowEngine.initWorkflowEngine();
			BiomajUtils.fillConfig(bankName, config);
			bb = bf.createBank(bankName,true);
			WorkflowEngineFactory wef = new WorkflowEngineFactory();
			WorkflowEngine we ;
			we = wef.createWorkflow(bb);
			we.setModeConsole(false, Project.MSG_INFO);
			we.setWorkWithCurrentDirectory(false);
			we.setForceForANewUpdate(false);
			we.setFromScratch(false);
			we.setCommand(WorkflowEngine.TARGET_ALL);
			we.start();
			
			we.join(); // wait for thread to end

		} catch (BiomajException e) {
			System.err.println("Biomaj has detected an error! for bank ["+ bankName+"]");
			System.err.println(e.getLocalizedMessage());
			
		} catch (BiomajBuildException bbe) {
			System.err.println("Biomaj has detected an error during the execution of workflow for the bank :"
					+ bankName);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
		}
	}
	
	/**
	 * 1 - Check that general info have been created in the db
	 * 2 - Check that an update record have been inserted
	 * 3 - Check that session records have been inserted
	 * 4 - Check production directory (see method for test detail)
	 */
	@Test
	public void runTest() {
		assertTrue(BiomajSQLQuerier.getBankInfo(bankName) != null);
		Map<String, String> info = BiomajSQLQuerier.getLatestUpdate(bankName, false);
		assertTrue(info != null);
		assertTrue(BiomajSQLQuerier.getUpdateSessions(Integer.valueOf(info.get(BiomajSQLQuerier.UPDATE_ID))).size() > 0);
		checkProductionDirectory();
	}
	
	@AfterClass
	public static void cleanup() {
		
		String dataDir = (String) bb.getPropertiesFromBankFile().get(BiomajConst.dataDirProperty);
		String versionDir = (String) bb.getPropertiesFromBankFile().get(BiomajConst.versionDirProperty);

		File dirToDel = new File(dataDir + "/" + versionDir);
		BiomajUtils.deleteAll(dirToDel);
		BiomajUtils.deleteAll(new File(DIR));
		
	}

	/**
	 * 1 - Check that current link has been created
	 * 2 - Check that flat directory has been created
	 * 3 - Check that the 2 files have been downloaded
	 */
	private void checkProductionDirectory() {
		String datadir = config.getVersionDirectory();
		File current = new File(datadir + FILE_SEP + "current");
		assertTrue(current.exists());
		File flat = new File(current.getPath() + FILE_SEP + "flat");
		assertTrue(flat.exists());
		File[] files = flat.listFiles();
		assertTrue(files.length == 2);
		assertTrue(files[0].getName().equals("test1.txt") && files[1].getName().equals("test2.txt"));
	}
	
}
