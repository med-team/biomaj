package org.inria.biomaj.exe.migration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.sql.SQLConnection;
import org.inria.biomaj.sql.SQLConnectionFactory;
import org.inria.biomaj.utils.BiomajUtils;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.DOMOutputter;
import org.xml.sax.SAXException;

/**
 * Executable class that parses a given list of xml statefiles
 * into SQL queries that are inserted into the database.
 * 
 * @author rsabas
 *
 */
public class Parser {
	
	protected static Logger logger = Logger.getLogger(Parser.class);

	@SuppressWarnings("static-access")
	public static void main(String[] args) {
//		DefaultOptionBuilder oBuilder = new DefaultOptionBuilder();
//		ArgumentBuilder aBuilder = new ArgumentBuilder();
//		GroupBuilder gBuilder = new GroupBuilder();
		
//		Option dirOption = OptionBuilder.withArgName("dir").withDescription("Directory to search the statefiles within").hasArgs(1).withLongOpt("dir").create();
//		Option fileOption = OptionBuilder.withArgName("files").withDescription("List of statefiles to import").hasArgs().withLongOpt("files").create();
//		Option batchOption = oBuilder.reset().withLongName("batch").withDescription("Execute the queries in batch mode (faster but no error reporting if the batch fails)").withRequired(false).create();
		
//		Group group = gBuilder.reset().withOption(dirOption).withOption(fileOption).withOption(batchOption).create();
		OptionGroup group = new OptionGroup();
		group.addOption(OptionBuilder.withArgName("dir").withDescription("Directory to search the statefiles within").hasArgs(1).withLongOpt("dir").create('d'));
		group.addOption(OptionBuilder.withArgName("file1 file2 ...").withDescription("List of statefiles to import").hasArgs().withLongOpt("files").create('f'));
		
//		Group group = gBuilder.reset().withOption(dirOption).withOption(fileOption).create();
		
		Options options = new Options();
		options.addOptionGroup(group);
		
		CommandLineParser parser = new BasicParser();
		HelpFormatter help = new HelpFormatter();
		
		CommandLine line = null;
		try {
			line = parser.parse(options, args);
		} catch (ParseException e3) {
			System.err.println(e3.getMessage());
			help.printHelp("import_statefiles.sh", options);
		}
		
		if (line == null)
			return;
		
		List<String> banks = BiomajSQLQuerier.getBanks();
		
		List<String> xmlFiles = new ArrayList<String>();
		List<String> sqlFiles = new ArrayList<String>();
		if (line.hasOption("dir")) { // Get all available statefiles in directory
			
			String path = line.getOptionValue("dir");
			File dir;
			if (path.startsWith("/")) // absolute path
				dir = new File(path);
			else // relative path
				dir = new File(System.getProperty("user.dir") + System.getProperty("file.separator") + path);
			
			if (!dir.exists()) {
				System.out.println("Directory does not exist : " + dir.getAbsolutePath());
				logger.error("Directory does not exist : " + dir.getAbsolutePath());
				return;
			}
				
			for (File f : dir.listFiles()) {
				// Check that it is an XML file ant that it has a corresponding
				// directory.
				if (f.isFile() && f.getName().endsWith(".xml") && !f.getName().equalsIgnoreCase("index.xml")
						&& new File(f.getPath().substring(0, f.getPath().lastIndexOf('.'))).isDirectory()) {
					if (checkExistence(f.getAbsolutePath(), banks)) {
						System.out.println(f.getName() + " already in the database. Skipped.");
						logger.info(f.getName() + " already in the database. Skipped.");
						continue;
					}
					xmlFiles.add(f.getPath());
				}
			}
		} else if (line.hasOption("files")) { // Get statefiles list
	
			String[] filz = line.getOptionValues("files");
			for (String fileName : filz) {
				if (!new File(fileName).exists()) {
					System.out.println("File does not exists : " + fileName + ". Skipped.");
					logger.warn("File does not exists : " + fileName + ". Skipped.");
					continue;
				}
				if (checkExistence(fileName, banks)) {
					System.out.println(fileName + " already in the database.");
					logger.info(fileName + " already in the database.");
					continue;
				}
				xmlFiles.add(fileName);
			}
		} else { // Print help if no option was chosen
			help.printHelp("import_statefiles.sh", options);
			return;
		}
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setXIncludeAware(true);
		factory.setNamespaceAware(true);

		try {
			for (String fileName : xmlFiles) {
				System.out.println("Building queries for " + fileName + " ...");
				logger.debug("Building queries for " + fileName + " ...");
				DocumentBuilder builder = factory.newDocumentBuilder();
	
				Source source = new DOMSource(builder.parse(new File(fileName)));
				Result output = new StreamResult(new File("xmlOutput.xml"));
				Transformer trans = TransformerFactory.newInstance()
						.newTransformer();
				trans.transform(source, output);
	
				Source xslSource = new StreamSource(new File(BiomajUtils.getBiomajRootDirectory() + "/xslt/xmlToSql.xsl"));
				source = new DOMSource(fixXML("xmlOutput.xml"));
	
				String tmpName;
				if (fileName.contains("/"))
					tmpName = fileName.substring(fileName.lastIndexOf('/'), fileName.lastIndexOf('.'));
				else
					tmpName = fileName.substring(0, fileName.lastIndexOf('.'));
				
				String outName = BiomajUtils.getBiomajRootDirectory() + "/sql/migration" + tmpName + "_queries.sql";
				sqlFiles.add(outName);
				
				output = new StreamResult(new File(outName));
				trans = TransformerFactory.newInstance().newTransformer(xslSource);
				trans.transform(source, output);
			}
		} catch (ParserConfigurationException ex) {
			ex.printStackTrace();
		} catch (SAXException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (TransformerConfigurationException ex) {
			ex.printStackTrace();
		} catch (TransformerFactoryConfigurationError ex) {
			ex.printStackTrace();
		} catch (TransformerException ex) {
			ex.printStackTrace();
		}

		long start = new Date().getTime();
		
		SQLConnection conn = SQLConnectionFactory.getConnection();
		
		if (SQLConnectionFactory.getDBType().equals(SQLConnectionFactory.HSQLDB_CONNECTION)) {
			System.out.println("Removing foreign keys...");
			removeConstraints();
			insertIntoBdd(sqlFiles);
			System.out.println("Restauring foreign keys...");
			addConstraints();
		} else if (SQLConnectionFactory.getDBType().equals(SQLConnectionFactory.MYSQL_CONNECTION)) {
			Statement stat = conn.getStatement();
			conn.executeUpdate("SET foreign_key_checks = 0;", stat);
			SQLConnectionFactory.closeConnection(stat);
			
			insertIntoBdd(sqlFiles);
			
			stat = conn.getStatement();
			conn.executeUpdate("SET foreign_key_checks = 1;", stat);
			SQLConnectionFactory.closeConnection(stat);
		}
		
		System.out.println("Elapsed time : " + (new Date().getTime() - start) + " ms");
		


		
//		BiomajSQLQuerier.getConnection().executeQuery("SET REFERENTIAL_INTEGRITY TRUE", false);
		
//		HSQLDBConnection.getInstance().executeQuery("SET PROPERTY \"hsqldb.cache_scale\" 14", false);
//		HSQLDBConnection.getInstance().executeQuery("CHECKPOINT", false);
		
		System.out.println("Logs available in xmlToSql.log");
	}

	/**
	 * Formats the file to be compatible with the database structure
	 * and column types.
	 * 
	 * @param path
	 * @return
	 */
	public static org.w3c.dom.Document fixXML(String path) {

		SAXBuilder builder = new SAXBuilder();
		try {
			Document doc = builder.build(new File(path));
			Element elt = doc.getRootElement();
			processNodes(elt);

			DOMOutputter out = new DOMOutputter();
			return out.output(doc);

		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;

	}

	private static void processNodes(Element elt) {

		String content;

		List<?> attrs = elt.getAttributes();
		for (Object obj : attrs) {
			Attribute attr = (Attribute) obj;
			/*
			 * Have to escape the simple quotes for the bdd.
			 */
			if ((content = attr.getValue()).contains("'"))
				elt.setAttribute(attr.getName(), content.replaceAll("'", "''"));
			/*
			 * elapsedTime tag sometimes got mispelled with ellapsedTime or
			 * elapsedtime... Have to handle that to...
			 */
			if (attr.getName().equalsIgnoreCase("elapsedtime")
					|| attr.getName().equalsIgnoreCase("ellapsedTime")) {
				attr.setName("elapsedTime");
			}
			/*
			 * Date format must be yyy-MM-dd hh:mm:ss
			 */
			else if (attr.getName().equalsIgnoreCase("date"))
				attr.setValue(frToEn(attr.getValue()));
			else if (attr.getName().equalsIgnoreCase("start"))
				attr.setValue(frToEn(attr.getValue()));
			else if (attr.getName().equalsIgnoreCase("end"))
				attr.setValue(frToEn(attr.getValue()));
			else if (attr.getName().equalsIgnoreCase("creation"))
				attr.setValue(frToEn(attr.getValue()));
			else if (attr.getName().equalsIgnoreCase("remove"))
				attr.setValue(frToEn(attr.getValue()));
		}

		if ((content = elt.getText()).contains("'"))
			elt.setText(content.replaceAll("'", "''"));

		List<?> children = elt.getChildren();
		if (children.size() > 0) {
			for (Object child : children) {
				processNodes((Element) child);
			}
		}
	}

	private static String frToEn(String date) {
		SimpleDateFormat inFormat;
		if (date.length() == 16)
			inFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		else
			inFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		try {
			SimpleDateFormat outFormat = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			return outFormat.format(inFormat.parse(date));
		} catch (java.text.ParseException e) {
			e.printStackTrace();

			return null;
		}
	}
	
	
	private static void removeConstraints() {
		String path = BiomajUtils.getBiomajRootDirectory() + "/sql/hsql_removeconstraints.sql";
		
		SQLConnection conn = SQLConnectionFactory.getConnection();
		Statement stat = conn.getStatement();
		conn.executeUpdate(getQueryFromFile(path), stat);
		SQLConnectionFactory.closeConnection(stat);
	}
	
	private static void addConstraints() {
		String path = BiomajUtils.getBiomajRootDirectory() + "/sql/hsql_addconstraints.sql";
		
		SQLConnection conn = SQLConnectionFactory.getConnection();
		Statement stat = conn.getStatement();
		conn.executeUpdate(getQueryFromFile(path), stat);
		SQLConnectionFactory.closeConnection(stat);
	}
	
	private static String getQueryFromFile(String fileName) {
		StringBuffer query = new StringBuffer(10000);
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line = "";
			while ((line = br.readLine()) != null)
				if (!line.trim().isEmpty() && !line.startsWith("--"))
					query.append(line);
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		return query.toString();
	}
	
	private static void insertIntoBdd(List<String> files) {
		
		/*
		 * We store in this map all the 'variables' defined in the queries file
		 * with the following syntax : $<var_name>:=<sql statement>.
		 */
		Map<String, String> vars = new HashMap<String, String>();
		
		SQLConnection sc = SQLConnectionFactory.getConnection();
		Statement statement = sc.getStatement();
		
		long idSession = 1;
		long idConfiguration = 1;
		
		try {
			statement.getConnection().setAutoCommit(false);
		} catch (SQLException ex) {
			logger.error("", ex);
			return;
		}
		try {
			// Session and configuration intialization
			ResultSet rs = statement.executeQuery("SELECT max(idsession) FROM session;");
			if (rs.next() && rs.getLong(1) >= idSession)
				idSession = rs.getLong(1) + 1;
			rs = statement.executeQuery("SELECT max(idconfiguration) FROM configuration;");
			if (rs.next() && rs.getLong(1) >= idConfiguration)
				idConfiguration= rs.getLong(1) + 1;
		} catch (SQLException e1) {
			logger.error("", e1);
			e1.printStackTrace();
			return;
		}
		
		for (String file : files) {
			boolean broken = false;
			System.out.println("Populating db with " + file + "...");
			logger.debug("Populating db with " + file + "...");
			String line = "";
			String tmp = "";
			Map<Long, Long> refOldSession = new HashMap<Long, Long>();
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				while ((line = br.readLine()) != null) {
					try {
						if (line.startsWith("INSERT INTO") && line.endsWith(");")) {
							
							if (line.contains("#")) { // Replace old sessionid with new in production directory
								Matcher match = Pattern.compile("#\\p{Digit}+").matcher(line);
								if (match.find())
									line = line.replace(match.group(), String.valueOf(refOldSession.get(Long.valueOf(match.group().substring(1)))));
							} else if (line.contains("$_")) {
								Matcher match = Pattern.compile("\\$\\w+").matcher(line);
								int startIndex = 0;
								while (match.find(startIndex)) {
									line = line.replace(match.group(), vars.get(match.group()).toString());
									startIndex = match.end();
								}
								logger.debug("Interpreted line : " + line);
							}
							logger.debug("\t" + line);
							statement.executeUpdate(line);
						} else if (line.startsWith("$")) { // assign the result of a statement
							String[] split = line.split(":=");
							if (split[1].toUpperCase().startsWith("SELECT")) {
								ResultSet res = statement.executeQuery(split[1]);
								res.next();
								vars.put(split[0], String.valueOf(res.getInt(1)));
							} else if (split[1].startsWith("$")) { // METAID
								Matcher match = Pattern.compile("\\$\\w+").matcher(split[1]);
								if (match.find()) {
									Random rand = new Random();
									vars.put(split[0], vars.get(match.group()) + rand.nextLong());
								}
							} else if (split.length == 3) { // ID session
								vars.put(split[0], String.valueOf(idSession));
								refOldSession.put(Long.valueOf(split[2]), idSession);
								idSession++;
							} else if (split[1].equals("GENERATE")) { // ID config
								vars.put(split[0], String.valueOf(idConfiguration++));
							} else { // Assign a value
								Matcher match = Pattern.compile("\\w+").matcher(split[1]);
								if (match.find())
									vars.put(split[0], match.group());
							}
						} else { // Insert statement on several lines due the presence of
								// a \n character in one of the fields.
							
							while (!line.endsWith(");")) {
								tmp += line;
								line = br.readLine();
							}
							tmp += line;
							if (tmp.contains("$_")) {
								Matcher match = Pattern.compile("\\$\\w+").matcher(tmp);
								int startIndex = 0;
								while (match.find(startIndex)) {
									line = line.replace(match.group(), vars.get(match.group()).toString());
									startIndex = match.end();
								}
								logger.debug("Interpreted line : " + line);
							}
							logger.debug("\t" + tmp);
							line = tmp;
							statement.executeUpdate(line);
							tmp = "";
						}
					} catch (SQLException ex) {
						logger.error("", ex);
						System.err.println(ex.getMessage());
					
						/*
						 * ROLLBACK
						 */
						try {
							statement.getConnection().rollback();
							System.out.println("Error on bank, insertion of bank aborted...");
							logger.debug("Rollbacked ! Bank won't be loaded into bdd...");
							broken = true;
							break;
						} catch (SQLException e1) {
							logger.error("", e1);
							e1.printStackTrace();
							return;
						}
					}
				}
				br.close();
				
				// Modifying idLastSession in updateBank
				logger.info("Updating idLastSession to new values...");
				for (long l : refOldSession.keySet()) {
					try {
						String uQ = "UPDATE updateBank SET idLastSession=" + refOldSession.get(l) + " WHERE idLastSession = " + l + ";";
						logger.debug(uQ);
						statement.executeUpdate(uQ);
					} catch (SQLException e) {
						logger.error(e);
						e.printStackTrace();
					}
				}
				
				
			} catch (FileNotFoundException e) {
				logger.error("", e);
				e.printStackTrace();
				return;
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
			
			/*
			 * COMMIT
			 */
			if (!broken) {
				try {
					statement.getConnection().commit();
					logger.debug("Commited !");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		SQLConnectionFactory.closeConnection(statement);
	}
	
	private static boolean checkExistence(String file, List<String> banks) {
		for (String s : banks) {
			
			// First try to compare with the filename
			if (file.substring(file.lastIndexOf('/') + 1, file.lastIndexOf('.')).equalsIgnoreCase(s))
				return true;
			
			
			// If file name and dbname are different, we have to read in the file
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line = "";
				while ((line = br.readLine()) != null) {
					if (line.contains("<dbName>") && 
							line.substring(line.indexOf("<dbName>") + 8, line.indexOf("</dbName>")).equals(s)) {
						br.close();
						return true;
					}
				}
				br.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		return false;
	}
	
}