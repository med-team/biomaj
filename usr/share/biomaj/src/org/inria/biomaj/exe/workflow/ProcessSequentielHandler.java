/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.workflow;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Properties;
import java.util.Vector;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.logger.BiomajConsoleLogger;
import org.inria.biomaj.ant.logger.BiomajProcessListenerHandler;
import org.inria.biomaj.ant.logger.SimpleLoggerHistoric;
import org.inria.biomaj.ant.task.BmajExecute;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.session.bank.Bank;
import org.inria.biomaj.session.bank.FileDesc;
import org.inria.biomaj.session.process.BiomajProcess;
import org.inria.biomaj.session.process.MetaProcess;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.singleton.BiomajSession;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;

public class ProcessSequentielHandler extends BiomajThread {


	/**
	 * @uml.property  name="bProject"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private BiomajAntProject bProject = null;
	/**
	 * Data process
	 * @uml.property  name="biomajBank"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */

	private BiomajBank biomajBank;

	/**
	 * @uml.property  name="xmlBank"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private Bank xmlBank;

	/**
	 * @uml.property  name="typeProcessName"
	 */
	private String typeProcessName;

	/**
	 * @uml.property  name="metaProc"
	 * @uml.associationEnd  
	 */
	private MetaProcess metaProc = null;

	/**
	 * @uml.property  name="block"
	 */
	private String block="";

	/**
	 * @uml.property  name="listProcessToLaunched"
	 */
	private String processListToLaunch = "";

	/**
	 * @uml.property  name="listenerHandler"
	 * @uml.associationEnd  
	 */
	BiomajProcessListenerHandler listenerHandler;

	/**
	 * Others
	 * @uml.property  name="withConsole"
	 */

	private boolean withConsole = true;

	/** End Data */

	/** Constructor */
	public ProcessSequentielHandler(BiomajBank biomajBank, Bank xmlBank,
			String typeProcessName) throws BiomajException {
		super(biomajBank.getDbName());
		this.biomajBank = biomajBank;
		this.xmlBank = xmlBank;
		this.typeProcessName = typeProcessName;
		bProject = new BiomajAntProject();
	}

	@Override
	public void interrupt() {
		super.interrupt();
	}

	@Override
	public boolean isInterrupted() {
		return super.isInterrupted();
	}

	public void initProcess() {

		Properties props = biomajBank.getPropertiesFromBankFile();
		try {
			bProject.init(BiomajConst.processXmlFile, props);
		} catch (BiomajException e) {
			setErrorOnWorkflow(true);
			return;
		}

		if (xmlBank.getWorkflowInfoRelease() != null)
			getProject().setProperty(BiomajConst.remoteReleaseDynamicProperty,
					xmlBank.getWorkflowInfoRelease());
		if (xmlBank.getWorkflowInfoRemovedRelease() != null)
			getProject().setProperty(BiomajConst.removedReleaseProperty,
					xmlBank.getWorkflowInfoRemovedRelease());
		// Dynamics properties

		/*
		 * if (blh==null) blh = new BiomajListenerHandler();
		 */

		try {
			listenerHandler = new BiomajProcessListenerHandler(xmlBank);
			if (metaProc != null) {
				metaProc.setBlock(block);
				listenerHandler.setMetaProcess(metaProc);
			}

			if (BiomajLogger.getInstance().getLogger(biomajBank.getDbName()) != null) { 
				String directory = BiomajLogger.getInstance().getLogger(biomajBank.getDbName()).getNameDirectory();
				SimpleLoggerHistoric slh ;

				if (metaProc == null) { // This is not supposed to happen with current code (tested in run()).
					String nameFile = block+".log";
					if (block.isEmpty())
						nameFile = "defaultBlock"+nameFile;
					slh = BiomajLogger.getInstance().initLogger(biomajBank.getDbName()+"Block:"+block,directory, nameFile );
				}
				else
					{
					String nameFile = metaProc.getName()+".log";
					if (!block.isEmpty())
						nameFile = block+"."+nameFile;
					slh = BiomajLogger.getInstance().initLogger(biomajBank.getDbName()+"Block:"+block+"."+metaProc.getName(),directory, nameFile );
					getProject().log("name meta         :"+metaProc.getName(),Project.MSG_VERBOSE);
					}
			
				getProject().log("Log name directory:"+directory,Project.MSG_VERBOSE);
				if (slh != null)
					{
					slh.setNameDirectory(directory);
					getProject().addBuildListener(slh);
					}
			}
			getProject().addBuildListener(listenerHandler);
		} catch (BiomajException e) {
			setErrorOnWorkflow(true);
			return;
		}

		getProject().setProperty("type_process", typeProcessName);

		int valueConsole = BiomajConsoleLogger.POST;
		if (typeProcessName.compareTo(BiomajConst.preprocessTarget)==0)
			valueConsole = BiomajConsoleLogger.PRE;
		else if (typeProcessName.compareTo(BiomajConst.removeprocessTarget)==0)
			valueConsole = BiomajConsoleLogger.REMOVE;
		// Console logger
		// DefaultLogger consoleLogger = new DefaultLogger();

		if (withConsole) {
			// level = Project.MSG_DEBUG;
			BiomajConsoleLogger console = null;
			if (metaProc != null)
				console = new BiomajConsoleLogger(biomajBank.getDbName() + "-" + block +":"
						+ metaProc.getName(), this,valueConsole);
			else
				console = new BiomajConsoleLogger(biomajBank.getDbName()+ "-" + block +":", this,valueConsole);

			(console)
			.setMessageOutputLevel(Project.MSG_INFO);
			getProject().addBuildListener(console);
		}
		bProject.start();
		setErrorOnWorkflow(false);
	}

	@Override
	public void run() {
		if (metaProc == null) {
			getProject().log("Unexpected undefined metaprocess for block:["
					+ block + "]", Project.MSG_INFO);
			setErrorOnWorkflow(true);
			release();
			return;
		}
		
		if ((processListToLaunch == null)
				|| (processListToLaunch.trim().compareTo("") == 0)) {
			getProject().log("No process needs to be launched for MetaProcess:["
					+ metaProc.getName() + "]", Project.MSG_INFO);
			setErrorOnWorkflow(false);
			release();
			return;
		}
		initProcess();

		getProject().log("#===================================#", Project.MSG_INFO);
		getProject().log("BLOCK             :" + block, Project.MSG_INFO);
		getProject().log("[MetaProcess]", Project.MSG_INFO);
		getProject().log("NAME              :" + metaProc.getName(), Project.MSG_INFO);
		getProject().log("PROCESS LIST      :" + getProject().getProperty(metaProc.getName()), Project.MSG_INFO);
		getProject().log("TO EXECUTE        :" + getListProcessToLaunched(), Project.MSG_INFO);
		getProject().log("#===================================#", Project.MSG_INFO);
		getProject().log("", Project.MSG_INFO);
		getProject().log("", Project.MSG_INFO);

		if (isErrorOnWorkflow()) {
			getProject().log("Can't instanciate BiomajListenerHandler for:"
					+ metaProc.getName(), Project.MSG_ERR);
			setErrorOnWorkflow(true);
			release();
			return;
		}

		if (!biomajBank.getPropertiesFromBankFile().containsKey(
				metaProc.getName())) {
			getProject().log("Meta process not defined : " + metaProc.getName(),
					Project.MSG_ERR);
			setErrorOnWorkflow(true);
			release();
			return;
			// throw new
			// BiomajException("postprocess.define.process.fail",idListProcessSeq);
		}
		// getProject().setProperty("list_process_seq",
		// biomajBank.getPropertiesFromBankFile().getProperty(metaProc.getName()));
		getProject().setProperty("list_process_seq", processListToLaunch);
		getProject().setProperty("metaprocess", metaProc.getName());
		boolean error = false;
		try {
			setEnvironmentFilesList();
			launchWithTarget();
		} 
		catch (BuildException be) {
			error = true;
			getProject().fireBuildFinished(be);
			setErrorOnWorkflow(true);
			try {
				listenerHandler.setErrorOnCurrentProcess();
			} catch (BiomajException bbe) {
				getProject().log("Can't set error on current process.",
						Project.MSG_ERR);
			}
			// Pour l instant quand un process retourne une erreur on arrete le
			// metaprocess
			if (true) {
				// Erreur globale non gerer
				release();
				return;
			}/* else {
				getProject().log("Keep others processes....", Project.MSG_ERR);
			}*/
		}
		if (!error) {
			getProject().log("No error detected on subprocesses....ok",
					Project.MSG_INFO);
			setErrorOnWorkflow(false);
		}
		deleteVolatilesFiles();
		getProject().fireBuildFinished(null);
		release();
	}

	public void setMetaProcess(MetaProcess mp) {
		metaProc = mp;
	}

	/**
	 * @param block  the block to set
	 * @uml.property  name="block"
	 */
	public void setBlock(String block) {
		this.block = block; 
	}

	protected void launchWithTarget() throws BiomajBuildException,
	BuildException {
		getProject().executeTarget("execute_process_sequentialy");
	}

	/**
	 * @return the project
	 * @uml.property name="project"
	 */
	public Project getProject() {
		return bProject.getProject();
	}

	public String getProperty(String key) {
		return bProject.getProperty(key);
	}

	public void setProperty(String key,String value) {
		bProject.setProperty(key, value);
	}


	public static String getXmlFileInfoPostProcess(Bank b, String block, String metaProcess) {
		String preName = "";
		if ((block!=null)&&(block.trim().compareTo("")!=0))
		{
			preName = block+".";
		}

		return  preName+ metaProcess + "."
		+ Long.toString(b.getCurrentSession().getStart().getTime())
		+ ".xml";
	}

	/**
	 * @return the listProcessToLaunched
	 * @uml.property name="listProcessToLaunched"
	 */
	public String getListProcessToLaunched() {
		return processListToLaunch;
	}

	/**
	 * @param listProcessToLaunched
	 *            the listProcessToLaunched to set
	 * @uml.property name="listProcessToLaunched"
	 */
	public void setListProcessToLaunch(String listProcessToLaunched) {
		this.processListToLaunch = listProcessToLaunched;
	}

	public void deleteVolatilesFiles() {
		getProject().log("", Project.MSG_INFO);
		getProject().log("", Project.MSG_INFO);
		getProject().log("#===================================#", Project.MSG_INFO);
		getProject().log("End MetaProcess:" + metaProc.getName(), Project.MSG_INFO);
		getProject().log("delete volatile files:", Project.MSG_INFO);
		Vector<BiomajProcess> lp = this.metaProc.getListProcess();
		int count = 0;
		for (BiomajProcess bp : lp) {
			Vector<FileDesc> lfd = bp.getDependancesOutput();

			for (FileDesc fd : lfd) {
				if (fd.isVolatil()) {
					File f = new File(fd.getLocation());
					if (f.exists()) {
						if (!f.delete())
							getProject().log("Biomaj can't delete volatile file:["
									+ fd.getLocation() + "]", Project.MSG_WARN);
						else {
							getProject().log("Biomaj delete volatile file:["
									+ fd.getLocation() + "]", Project.MSG_INFO);
							count++;
						}
					} else {
						getProject().log("Volatile file does not existe:["
								+ fd.getLocation() + "]", Project.MSG_WARN);
					}
				}
			}

		}
		getProject().log(Integer.toString(count) + " file(s) deleted.",
				Project.MSG_INFO);
		getProject().log("", Project.MSG_INFO);
		getProject().log("#=======================#", Project.MSG_INFO);
		getProject().log("METAPROCESS FINISHED", Project.MSG_INFO);
		getProject().log("#=======================#", Project.MSG_INFO);
	}

	public void setModeConsole(boolean value) {
		withConsole = value;
	}

	private void setEnvironmentFilesList() throws BiomajBuildException {
		//Ajout de la liste complete des fichier contenu par la nouvelle release

		if (typeProcessName.equals(BiomajConst.postprocessTarget) && getProject().getProperties().containsKey(BiomajConst.listFilesAvailable)&&(Boolean.valueOf(getProject().getProperty(BiomajConst.listFilesAvailable)))) {
			try {
				getProject().setProperty(BiomajConst.listFilesAvailable, Boolean.toString(true));
				int count = 0;
				//getProject().log("download:"+xmlBank.getNbFilesDownloaded(),Project.MSG_WARN);
				//getProject().log("offline:"+xmlBank.getNbFilesLocalOffline(),Project.MSG_WARN);
				//getProject().log("online:"+xmlBank.getNbFilesLocalOnline(),Project.MSG_WARN);
				float size = xmlBank.getNbFilesDownloaded() + xmlBank.getNbFilesLocalOnline();
				getProject().log("total:"+size,Project.MSG_VERBOSE);
				String file = getProperty(BiomajConst.runtimeDirectoryProperty)+"/"+getProperty(BiomajConst.dbNameProperty)+".remote.filelist" ;
				
				//log("Complete file list:"+file,Project.MSG_ERR);
				BufferedReader listingFile = new BufferedReader(new FileReader(new File(file)));
				String line= null ;
				String resultAllDownloadedFiles = "";
				String resultAllFilesExtracted = "";

				String filtreOfflineDir = new File(getProperty(BiomajConst.dataDirProperty)+"/"+getProperty(BiomajConst.offlineDirProperty)).getAbsolutePath()+"/";
				Bank bank = BiomajSession.getInstance().getBank(getProperty(BiomajConst.dbNameProperty)) ;

				while ((line = listingFile.readLine()) != null) {
					float a = (++count/ size)*100;
					getProject().log(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE+"["+Integer.toString((int)a)+"%]",Project.MSG_INFO);
					RemoteFile rf = new RemoteFile(line);
					resultAllDownloadedFiles = resultAllDownloadedFiles + rf.getAbsolutePath() + " ";

					if (bank!=null) {
						Vector<FileDesc> lfiles = bank.getGeneratedFiles(getProject(), rf); 
						for (FileDesc f : lfiles) {
							resultAllFilesExtracted = resultAllFilesExtracted + f.getLocation().replace(filtreOfflineDir, "") + " ";						}
					}
				}
				listingFile.close();

				getProject().log("result Zipped Total :"+resultAllDownloadedFiles,Project.MSG_VERBOSE);
				getProject().log("result Unzipped Total :"+resultAllFilesExtracted,Project.MSG_VERBOSE);


				getProject().setProperty(BmajExecute.RELEASE_ALL_COMPRESSED_FILES_LIST,resultAllDownloadedFiles);

				//Attention ca fonctionne seulement si log.files=true dans le workflow !

				getProject().setProperty(BmajExecute.RELEASE_ALL_UNCOMPRESSED_FILES_LIST,resultAllFilesExtracted);
				
				file = getProperty(BiomajConst.runtimeDirectoryProperty)+"/"+getProperty(BiomajConst.dbNameProperty)+".copy.filelist" ;
				//log("Copy file list:"+file,Project.MSG_ERR);
				listingFile = new BufferedReader(new FileReader(new File(file)));
				line = null ;
				String resultCopy = "";

				count = 0;
				size = xmlBank.getNbFilesLocalOnline();

				while ((line = listingFile.readLine()) != null) {
					float a = (++count/ size)*100;
					getProject().log(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE+"["+Integer.toString((int)a)+"%]",Project.MSG_INFO);
					if (!line.startsWith("#PLA"))
						resultCopy = resultCopy + line + " ";
				}
				listingFile.close();

				getProject().log("result Copy :"+resultCopy,Project.MSG_VERBOSE);

				getProject().setProperty(BmajExecute.RELEASE_OLD_FILES_LIST,resultCopy);
				
				String[] lCopy = resultCopy.split("\\s");
				String[] lTotal = resultAllFilesExtracted.split("\\s");
				String resultDelta = "";
				count = 0;
				size = lTotal.length;
				for (String f : lTotal) {
					float a = (++count/ size)*100;
					getProject().log(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE+"["+Integer.toString((int)a)+"%]",Project.MSG_INFO);
					boolean isDownload = true;
					for (String oldFile : lCopy) {
						if (f.trim().compareTo(oldFile.trim())==0){
							isDownload = false;
							break;
						}
					}	
					if (isDownload)
					{
						resultDelta = resultDelta + f +" ";  
					}
				}
				
				getProject().log("result delta:"+resultDelta,Project.MSG_VERBOSE);
				getProject().setProperty(BmajExecute.RELEASE_NEW_FILES_LIST,resultDelta);
				
			} catch (Exception pe) {
				throw new BiomajBuildException(getProject(),pe);
			} 

		} else {
			getProject().log("Environment variables ["+BmajExecute.RELEASE_ALL_COMPRESSED_FILES_LIST+","+
					BmajExecute.RELEASE_ALL_UNCOMPRESSED_FILES_LIST+","+BmajExecute.RELEASE_NEW_FILES_LIST+","+
					BmajExecute.RELEASE_OLD_FILES_LIST+"] are not initialized. ["+BiomajConst.listFilesAvailable+"=false]",Project.MSG_INFO);
		}
	}

}
