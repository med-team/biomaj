/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.workflow;

import java.io.File;
import java.util.Enumeration;
import java.util.Properties;
import org.apache.tools.ant.BuildListener;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;



public class BiomajAntProject {

	/**
	 * @uml.property  name="project"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private Project project = null;
	/**
	 * @uml.property  name="build"
	 */
	private File build ;	
	
	public BiomajAntProject() {
		
	}
	
	
	protected void set(Properties props)  throws BiomajException {
		
		if (project == null) {
			BiomajLogger.getInstance().log("Error DEV : project not instanciate :["+build.getAbsolutePath()+"]");
			throw new BiomajException("ERREURDEV");
		}
		
		Enumeration<?> keys = props.keys();

		while (keys.hasMoreElements()) {
			String key = (String)keys.nextElement();
			project.setProperty(key,props.getProperty(key));
		}
	}
	
	/**
	 * 
	 * @param descWorkflowXml : fichier xml decrivant le workflow
	 */
	public void init(String descWorkflowXml,Properties props) throws BiomajException {
		project = new Project();
		build = new File(BiomajUtils.getBiomajRootDirectory()+BiomajConst.workflowXmlDirectory+"/"+descWorkflowXml);
		if (!build.exists()) {
			BiomajLogger.getInstance().log("Error DEV : xml file (workflow description) not find:["+build.getAbsolutePath()+"]");
			throw new BiomajException("ERREURDEV");
		}
		
		project.setUserProperty("ant.file", build.getAbsolutePath());
		set(props);
	}
	
	public void start () {
		project.init();
		ProjectHelper helper = ProjectHelper.getProjectHelper();
		project.addReference("ant.projectHelper", helper);
		helper.parse(project, build);
		project.fireBuildStarted();
	}
	
	public void updateProperties(Properties props) {
		Enumeration<?> keys = props.keys();

		while (keys.hasMoreElements()) {
			String key = (String)keys.nextElement();
			if ((project.getProperty(key)==null)||(project.getProperty(key).compareTo(props.getProperty(key))!=0)) {
				project.log("update property ["+key+":"+props.getProperty(key)+"]");
				project.setProperty(key,props.getProperty(key));
			}
		}
	}
	
	public void addBuildListener(BuildListener bl) {
		if (project != null)
			project.addBuildListener(bl);
	}	
	
	
	public String getProperty(String key) {
		if (project!=null)
			return project.getProperty(key);
		else
			return null;
	}
	
	public void setProperty(String key,String value) {
		if (project!=null)
			project.setProperty(key,value);
	}

	/**
	 * @return  the project
	 * @uml.property  name="project"
	 */
	public Project getProject() {
		return project;
	}
}
