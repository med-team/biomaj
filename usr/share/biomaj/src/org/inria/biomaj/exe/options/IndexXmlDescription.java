/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.options;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import org.apache.tools.ant.BuildException;
import org.inria.biomaj.session.bank.Bank;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.Configuration;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;
import org.jdom.Comment;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class IndexXmlDescription {

	protected class DbTypeTree {
		//Nom du type
		public String name;
		//List fo banks associated to this type
		public Vector<String> listBankName = new Vector<String>();
//		List des size associees a la liste  listBankName 
		public Vector<Long> listSizeBank = new Vector<Long>();
		//list des type fils
		public Vector<DbTypeTree> listType = new Vector<DbTypeTree>();
		//size of type sub-set
		public long size=0;
	}


	private static String nameXmlFile = "index.xml";
	//nickname for the key 
	private HashMap<String,Bank> listBank;

	/**
	 * @uml.property  name="treeType"
	 * @uml.associationEnd  multiplicity="(1 1)" inverse="this$0:org.inria.biomaj.exe.generation.infos.IndexXmlDescription$DbTypeTree"
	 */
	private DbTypeTree treeType = new DbTypeTree();

	public IndexXmlDescription() {
		listBank = new HashMap<String,Bank> ();
	}


	/**
	 * IMPORTANT: we erase and write the file in a same time because some biomaj application
	 * write and read this file!!there can have a problem of synchronization!
	 * @param p
	 */
	protected void updateXmlFile() {
		Element root = new Element("biomaj");

		root.addContent(getElementListBanks());
		root.addContent(getTreeType(treeType));

		Document document = root.getDocument();

		if (document==null)
			document=new Document(root);

		XMLOutputter out = new XMLOutputter(Format.getPrettyFormat());

		try {

			String sFile= BiomajUtils.getStateFileDirectory()+"/"+nameXmlFile;

			// An other application can read this file !!!!!
			File f = new File(sFile);
			if (f.exists())
				f.delete();
			BiomajUtils.createSubDirectories(BiomajUtils.getRelativeDirectory(sFile));
			out.output(document, new FileOutputStream(sFile,false));
		}  catch (Exception ex) {
			throw new BuildException(ex);
		}
	}

	protected Element getElementListBanks() {
		Element banks = new Element("banks");
		long sizeTotal = 0;

		for (Bank b : listBank.values()) {
			sizeTotal += b.getWorkflowInfoSizeRelease();
		}

		banks.setAttribute("creation",BiomajUtils.dateToString(new Date(), Locale.US));
		banks.setAttribute("totalSize",BiomajUtils.sizeToString(sizeTotal));

		Set<String> vs = listBank.keySet();
		for (String s : vs) {
			banks.addContent(getBank(s));
		}

		return banks;
	}

	protected Element getBank(String nickName) {
		Bank bd = listBank.get(nickName);
		Element bank = new Element("bank");
		bank.setAttribute("dbname",nickName);
		bank.setAttribute("fullname",bd.getConfig().getFullName());
		bank.setAttribute("lastUpdate",BiomajUtils.dateToString(bd.getEnd(), Locale.US));

		if ((bd.getEnd())!=null && (bd.getStart()!=null))
			bank.setAttribute("time",BiomajUtils.timeToString(bd.getEnd().getTime() - bd.getStart().getTime()));
		else
			bank.setAttribute("time","0");
		
		Element release = new Element("release");
		release.setAttribute("value",bd.getWorkflowInfoRelease());
		bank.addContent(release);

		Element d = new Element("download");
		d.setAttribute("nbfiles",Integer.toString(bd.getNbFilesDownloaded()));
		Comment comm = new Comment("band width in Ko/s");
		d.addContent(comm);
		d.setAttribute("bandwidth",Float.toString(bd.getBandWidth()));
		bank.addContent(d);


		Element proddir = new Element("productionDirectory");
		proddir.setAttribute("value",bd.getWorkflowInfoProductionDir());
		//proddir.setAttribute("nbfilesInFlat",Integer.toString(bd.getNbFilesMoves()));
		proddir.setAttribute("size",BiomajUtils.sizeToString(bd.getWorkflowInfoSizeRelease()));
		bank.addContent(proddir);

		Element formats = new Element("formats");
		for (String f : bd.getConfig().getFormats()) {
			Element format = new Element ( "format" ) ;
			format.setAttribute("name", f);
			formats.addContent(format);
		}
		bank.addContent(formats);

		try {
			Element futur = new Element("nextReleaseInDay");
			futur.addContent(new Comment("time day:hour:min:sec"));
			//futur.addContent(new Comment("Ecart type en jours"));
			futur.setAttribute("moy",BiomajUtils.timeToString(BiomajSQLQuerier.getStatMoyenneUpdate(nickName)));
			//futur.setAttribute("ecartType",Double.toString(BiomajQueryXmlStateFile.getEcartTypeUpdate(knickName)));
			bank.addContent(futur);
		} catch (Exception be) {
			BiomajLogger.getInstance().log(nickName+":"+be.getMessage());
		} 

		bd.getWorkflowInfoSizeRelease();

		return bank;
	}
	
	
	public boolean initListBank() {
		
		List<String> banks = BiomajSQLQuerier.getBanks();
		for (String bank : banks) {
			
			Configuration config = new Configuration();
			if (!BiomajUtils.fillConfig(bank, config))
				continue;

			if (listBank.containsKey(bank))
				continue;
			
			Bank b = new Bank();
			Map<String, String> info = BiomajSQLQuerier.getLatestUpdateWithProductionDirectory(bank);
			if (info == null)
				continue;
			
			b.fill(info, false);
			b.setConfig(config);
			
			listBank.put(bank, b);
		}
		
		return true;
	}


	public void createXmlGeneralFile(String file) throws BiomajException {

		if ((file!=null)&&file.compareTo("")!=0)
			nameXmlFile = file;

		if (!initListBank())
			return;

		File general = new File(BiomajUtils.getStateFileDirectory()+"/"+nameXmlFile);

		if (general.exists())
			general.delete();
		
		File parentDir = new File(general.getAbsolutePath().substring(0, general.getAbsolutePath().lastIndexOf('/')));
		if (!parentDir.exists()) {
			System.err.println(parentDir.getAbsolutePath() + " does not exist.");
		} else {
			constructDbType();
			updateXmlFile();
		}
	}


	public DbTypeTree constructDbType() {
		//System.out.println("generate a tree type");
		treeType.name="all";

		for (Bank b : listBank.values()) {
			if ((b.getConfig().getTypeBank()==null)||(b.getConfig().getTypeBank().trim().compareTo("")==0)) {
				treeType.listBankName.add(b.getConfig().getName());
				continue;
			}
			String type = b.getConfig().getTypeBank();

			while (type.contains("//"))
				type=type.replace("//", "/");

			if (type.endsWith("/"))
				type = type.replaceAll("/$", "");

			if (type.startsWith("/"))
				type = type.replaceFirst("/", "");

			constructRec(treeType,type, b.getConfig().getName());
		}

		for (DbTypeTree t : treeType.listType)
			treeType.size+=t.size;

		for (String b: treeType.listBankName)
			treeType.size+=listBank.get(b).getWorkflowInfoSizeRelease();

		return treeType;
	}


	private void constructRec(DbTypeTree tt,String type,String nameBank) {
		if (!type.contains("/")) {
			boolean isNew = true;

			DbTypeTree t2 = null;

			for (DbTypeTree t : tt.listType) {
				if (t.name.compareTo(type.trim())==0) {
					isNew = false;
					t2 = t;
					break;
				}
			}

			if (isNew)
			{
				t2 = new DbTypeTree();
				t2.name = type.trim();
				tt.listType.add(t2);
			}

			t2.listBankName.add(nameBank);
			//System.out.print("size ["+nameBank+"]:");
			//long size = BiomajUtils.computeDirectorySize(listBank.get(nameBank).getWorkflowInfoProductionDir());
			long size = listBank.get(nameBank).getWorkflowInfoSizeRelease();
			//System.out.println(BiomajUtils.sizeToString(size));
			t2.listSizeBank.add(size);
			t2.size += size;

			return;
		}

		//Sinon il reste a traiter...
		String newType = type.split("/")[0];
		boolean isNew = true;

		DbTypeTree t2 = null;

		for (DbTypeTree t : tt.listType) {
			if (t.name.compareTo(newType.trim())==0) {
				isNew = false;
				t2 = t;
				break;
			}
		}

		if (isNew)
		{
			t2 = new DbTypeTree();
			t2.name = newType.trim();
			tt.listType.add(t2);
		}

		constructRec(t2,type.replace(newType+"/", ""),nameBank);
		t2.size += listBank.get(nameBank).getWorkflowInfoSizeRelease();

	}



	private Element getTreeType(DbTypeTree t) {

		Element tree = new Element("type");
		tree.setAttribute("value",t.name);
		for (String bank : t.listBankName){
			Element l = new Element("bank");
			l.setAttribute("name",bank);
			tree.addContent(l);
		}

		for (DbTypeTree d : t.listType) {
			tree.addContent(getTreeType(d));
		}

		return tree;
	}

}
