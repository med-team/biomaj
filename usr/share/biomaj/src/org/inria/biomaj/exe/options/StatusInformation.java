/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.options;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.inria.biomaj.exe.bank.BankFactory;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.session.bank.Bank;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.Configuration;
import org.inria.biomaj.session.bank.ProductionDirectory;
import org.inria.biomaj.session.bank.Session;
import org.inria.biomaj.session.process.BiomajProcess;
import org.inria.biomaj.session.process.MetaProcess;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

public class StatusInformation {	
	
	private static String tmpDir;
	static {
		try {
			tmpDir = BiomajInformation.getInstance().getProperty(BiomajInformation.TMPDIR);
		} catch (BiomajException e) {
			tmpDir = null;
			e.printStackTrace();
		}
	}

	public static void getStatus(String bank) throws BiomajException {
		
		Map<String, String> info = BiomajSQLQuerier.getBankInfo(bank);
		if (info == null) {
			System.out.println("No update available for this bank.");
			return;
		}
		
		Configuration config = new Configuration(info);

		System.out.println("#----------------");
		System.out.println("# Properties used For the last session");
		System.out.println("#----------------");
		System.out.println("Keyname                           :"+config.getName());
		System.out.println("Description                       :"+config.getFullName());

		System.out.println("#----------------");
		System.out.println();
		System.out.println("First utilisation                 :"+config.getDate());
		System.out.println("Url                               :"+config.getProtocol()+
				"://"+config.getUrl()+"/"+config.getRemoteDirectory());
		System.out.println("Remote regular expression         :"+config.getRemoteFilesRegexp());
		System.out.println("Remote excluded regular expression:"+config.getRemoteExcludedFiles());
		System.out.println("Local regular expression          :"+config.getLocalFilesRegexp());
		System.out.println("Version directory                 :"+config.getVersionDirectory());
		System.out.println("Offline directory                 :"+config.getOfflineDirectory());
		System.out.println("Log files on state file           :"+config.getLogFiles());

		List<ProductionDirectory> lPd = BiomajSQLQuerier.getAvailableProductionDirectories(bank);

		if (lPd.size() == 0)
			return ;

		long goodSession = 0 ;
		Date t = null;
		for (ProductionDirectory pd : lPd) {
			if (t == null)
				goodSession = pd.getSession();
			else if (t.getTime()<pd.getCreationDate().getTime())
				goodSession = pd.getSession();
		}

		Bank b = new Bank();

		if (BiomajSQLQuerier.getUpdateBank(bank,goodSession,b,true)) {
			b.setConfig(config);
			System.out.println();
			System.out.println("#----------------");
			System.out.println("# Current Release");
			System.out.println("#----------------");
			printBank(b);
		}
		
		b = new Bank();
		if (BiomajSQLQuerier.getFuturReleaseBankWithoutConfiguration(bank,b,true) && b.getEnd()==null) {
			b.setConfig(config);
			System.out.println();
			System.out.println("#----------------");
			System.out.println("# Future Release");
			System.out.println("#----------------");
			printBank(b);
		}
		
		if (lPd.size()>0) {
			System.out.println("-----------------------------------");
			System.out.println("List production directories                  ");
			Collections.sort(lPd);
			for (ProductionDirectory pd : lPd) {
				System.out.println(BiomajUtils.dateToString(pd.getCreationDate(), Locale.US)+ " " + pd.getPath() + " ("+BiomajUtils.sizeToString(pd.getSize())+")");

			}
			System.out.println();
			System.out.println();
			System.out.println();
		}
	}

	public static void printBank(Bank b) throws BiomajException {

		
		System.out.println("Release                           :"+b.getWorkflowInfoRelease());
		System.out.println("Number of session                 :"+Integer.toString(b.getListOldSession().size()));
		if (b.getEnd()!=null) {
			System.out.println("Session date                      :"+BiomajUtils.dateToString(b.getEnd(), Locale.US));
			System.out.println("Duration                          :"+BiomajUtils.timeToString(b.getEnd().getTime()-b.getStart().getTime()));
		}
		if (b.getWorkflowInfoProductionDir() != null)
			System.out.println("Production directory              :"+b.getWorkflowInfoProductionDir());

		System.out.println("Num files downloaded              :"+b.getNbFilesDownloaded());
		System.out.println("Bandwidth (Mo/s)                  :"+Float.toString(b.getBandWidth()));
		System.out.println("Download size                     :"+BiomajUtils.sizeToString(b.getWorkflowInfoSizeDownload()));
		System.out.println("Bank size                         :"+BiomajUtils.sizeToString(b.getWorkflowInfoSizeRelease()));
		//System.out.println("Release files Nb                  :"+b.getNbFilesMoves());
		System.out.println();

		if (b.getListOldSession().size()>0)
			System.out.println("View Last log                     :"+b.getListOldSession().get(b.getListOldSession().size()-1).getLogfile());

		
		System.out.println();
		System.out.println("--------------");
		Collection<MetaProcess> lMpPre = b.getAvailableMetaProcessSql(Session.PREPROCESS);
		System.out.println("Pre processes by metaproc:");
		System.out.println();
		for (MetaProcess mpPre : lMpPre) {
			System.out.println("Metaproc:["+ mpPre.getName() +"] log:["+mpPre.getLogFile()+"]");
			for (BiomajProcess bp : mpPre.getListProcess()) {
				System.out.println("-->"+bp.getNameProcess()+"("+bp.getDescription()+") ");
			}

			System.out.println();
		}
		
		
		System.out.println();
		System.out.println("--------------");
		Collection<MetaProcess> lMp = b.getAvailableMetaProcessSql(Session.POSTPROCESS);
		System.out.println("Post processes by metaproc:");
		System.out.println();
		for (MetaProcess mp : lMp) {
			//System.out.println("                    **************[block:"+mp.getBlock()+" metaprocess:"+mp.getName()+"]***************");
			System.out.println("Metaproc:["+ mp.getName() +"] log:["+mp.getLogFile()+"]");
			for (BiomajProcess bp : mp.getListProcess()) {
				System.out.println("-->"+bp.getNameProcess()+"("+bp.getDescription()+") ");
				/*
				System.out.println("-------------------");
				System.out.println("   -name    :"+bp.getNameProcess());
				System.out.println("   -exe     :"+bp.getExe());
				System.out.println("   -args    :"+bp.getArgs());
				System.out.println("   -desc    :"+bp.getDescription());
				System.out.println("   -files   :"+bp.getDependancesOutput().size());
				System.out.println("   -time    :"+BiomajUtils.timeToString(bp.getEnd().getTime()-bp.getStart().getTime()));
				System.out.println("   -log     :"+mp.getLogFile());
				*/
			}

			System.out.println();
		}
		System.out.println();
	}

	public static void getStatus(String typeFilter, boolean updating, boolean online) throws BiomajException {

		final String unknown = "    ??";
		String rowDbType      = "DbType                 ";
		String rowDbName      = "DbName";

		while (rowDbName.length()<40) rowDbName+=" ";

		String rowLastRelease = "Last release        ";
		String rowSession     = "Session date ";
		String rowStatus      = "Status     ";

		String sep            = "-";
		String lineTitle = rowDbType+rowDbName+rowLastRelease+rowSession+rowStatus;//+rowDatadir;

		while (sep.length()<lineTitle.length()) sep+="-";

		System.out.println(lineTitle);
		System.out.println(sep);

		BankFactory bf = new BankFactory();
		
		List<String> bankNames = BiomajSQLQuerier.getBanks();
		Collections.sort(bankNames);
		for (String bankName : bankNames) {
			boolean init = true;
			
			
			BiomajBank bb = bf.createBank(bankName, false);
			String type = bb.getPropertiesFromBankFile().getProperty(BiomajConst.typeProperty);
			if (type == null)
				type = "";
			
			
			if (typeFilter != null && !typeFilter.trim().isEmpty() && !type.startsWith(typeFilter))
				continue;
			
			String lastSession = unknown;
			String release = unknown;
			String status = "updating";

			Map<String, String> update = BiomajSQLQuerier.getLatestUpdateWithProductionDirectory(bankName);
			
			if (update == null)
				continue;
			
			
//			List<ProductionDirectory> pds = BiomajSQLQuerier.getAvailableProductionDirectories(bankName);
			
			boolean _updating = new File(tmpDir + bankName + ".lock").exists();
			
			if (updating && !_updating)
				continue;
			
			String end = update.get(BiomajSQLQuerier.UPDATE_END);
			release = update.get(BiomajSQLQuerier.UPDATE_RELEASE);//.substring(0, 10);
			lastSession = end.substring(0, 10);
			status = _updating ? "updating" : "online";
			
			/*
			 * Formatage
			 */
			if (init) {
				while(type.length()<rowDbType.length()) type+=" ";
				init = false;
			} else {
				type = "";
				while(type.length()<rowDbType.length()) type+=" ";
			}
			while(bankName.length()<rowDbName.length()) bankName +=" ";

			if (bankName.length()>40)
				bankName = bankName.substring(0,40);

			while (release.length() < rowLastRelease.length())	release += " ";
			while (lastSession.length() < rowSession.length()) 	lastSession += " ";
			while (status.length() < rowStatus.length()) 		status += " ";

			System.out.println(type + bankName + release + lastSession + status);
					
		}
		
		System.out.println();
		System.out.println("infos : --status [dbname] ==> gives more details about releases in local repository.");
		System.out.println();
	}

	/**
	 * In dev mode, to log the application with a command like tail -f
	 * @param bank
	 */
	public static void getLog(String bank) throws BiomajException {

		class DirFilter  implements FileFilter {
			public boolean accept(File pathname) {
				return pathname.isDirectory();
			}
		}

		String log = BiomajInformation.getInstance().getProperty(BiomajInformation.WEBREPORTDIR)+"/"+bank;
		File dirLog = new File(log);

		if (! dirLog.exists()) {
			BiomajLogger.getInstance().log("Log directory not find ["+log+"]");
			return;
		}

		File[] lDir = dirLog.listFiles(new DirFilter());
		File lastLogDir = null ;

		for (File d : lDir) {
			if (d.getName().contains("runtime"))
				continue;
			if (lastLogDir==null) {
				lastLogDir = d;
				continue;
			}
			if (lastLogDir.lastModified()<d.lastModified()) {
				lastLogDir = d;
			}
		}

		if (lastLogDir==null) {
			BiomajLogger.getInstance().log("No log directory for ["+bank+"]");
			return;
		}

		/**
		 * User choice on file to log....
		 */

		try {
			FileInputStream fis  = new FileInputStream(lastLogDir.getAbsolutePath()+"/mirror.log");
			byte[] buf = new byte[1024];
			int i = 0;
			while((i=fis.read(buf))!=-1) {
				System.out.write(buf, 0, i);
			}
			fis.close();
		} catch (Exception ioe) {
			BiomajLogger.getInstance().log(ioe);
			return;
		} finally {

		}

	}
}
