/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.bank;

import java.util.Properties;
import org.inria.biomaj.utils.BiomajConst;


public class RemoteBank extends SingleBank {

	public RemoteBank(Properties props) {
		super(props);
		setServer(props.getProperty(BiomajConst.serverProperty));
		setPort(Integer.valueOf(props.getProperty(BiomajConst.portProperty)));
		setUserName(props.getProperty(BiomajConst.userNameProperty));
		setPassword(props.getProperty(BiomajConst.passwordProperty));
		setFrequencyUpdate(props.getProperty(BiomajConst.frequencyProperty));
		setReleaseFile(props.getProperty(BiomajConst.releaseFileProperty));
		setReleaseRegExp(props.getProperty(BiomajConst.releaseRegExpProperty));
		setRemoteDirectory(props.getProperty(BiomajConst.remoteDirProperty));
		setLinkCopy(Boolean.valueOf(props.getProperty(BiomajConst.copyModeProperty)));	
	}
	/**
	 * @uml.property  name="server"
	 */
	private String server = "";

	/**
	 * Getter of the property <tt>server</tt>
	 * @return  Returns the server.
	 * @uml.property  name="server"
	 */
	public String getServer() {
		return server;
	}

	/**
	 * Setter of the property <tt>server</tt>
	 * @param server  The server to set.
	 * @uml.property  name="server"
	 */
	public void setServer(String server) {
		this.server = server;
	}

	/**
	 * @uml.property  name="port"
	 */
	private Integer port = -1;

	/**
	 * Getter of the property <tt>port</tt>
	 * @return  Returns the port.
	 * @uml.property  name="port"
	 */
	public Integer getPort() {
		return port;
	}

	/**
	 * Setter of the property <tt>port</tt>
	 * @param port  The port to set.
	 * @uml.property  name="port"
	 */
	public void setPort(Integer port) {
		this.port = port;
	}

	/**
	 * @uml.property  name="userName"
	 */
	private String userName = "";

	/**
	 * Getter of the property <tt>userName</tt>
	 * @return  Returns the userName.
	 * @uml.property  name="userName"
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Setter of the property <tt>userName</tt>
	 * @param userName  The userName to set.
	 * @uml.property  name="userName"
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @uml.property  name="password"
	 */
	private String password = "";

	/**
	 * Getter of the property <tt>password</tt>
	 * @return  Returns the password.
	 * @uml.property  name="password"
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Setter of the property <tt>password</tt>
	 * @param password  The password to set.
	 * @uml.property  name="password"
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @uml.property  name="remoteDirectory"
	 */
	private String remoteDirectory = "";

	/**
	 * Getter of the property <tt>remoteDirectory</tt>
	 * @return  Returns the remoteDirectory.
	 * @uml.property  name="remoteDirectory"
	 */
	public String getRemoteDirectory() {
		return remoteDirectory;
	}

	/**
	 * Setter of the property <tt>remoteDirectory</tt>
	 * @param remoteDirectory  The remoteDirectory to set.
	 * @uml.property  name="remoteDirectory"
	 */
	public void setRemoteDirectory(String remoteDirectory) {
		this.remoteDirectory = remoteDirectory;
	}

	/**
	 * @uml.property  name="frequencyUpdate"
	 */
	private String frequencyUpdate = "";

	/**
	 * Getter of the property <tt>frequencyUpdate</tt>
	 * @return  Returns the frequencyUpdate.
	 * @uml.property  name="frequencyUpdate"
	 */
	public String getFrequencyUpdate() {
		return frequencyUpdate;
	}

	/**
	 * Setter of the property <tt>frequencyUpdate</tt>
	 * @param frequencyUpdate  The frequencyUpdate to set.
	 * @uml.property  name="frequencyUpdate"
	 */
	public void setFrequencyUpdate(String frequencyUpdate) {
		this.frequencyUpdate = frequencyUpdate;
	}

	/**
	 * @uml.property  name="releaseFile"
	 */
	private String releaseFile = "";

	/**
	 * Getter of the property <tt>releaseFile</tt>
	 * @return  Returns the releaseFile.
	 * @uml.property  name="releaseFile"
	 */
	public String getReleaseFile() {
		return releaseFile;
	}

	/**
	 * Setter of the property <tt>releaseFile</tt>
	 * @param releaseFile  The releaseFile to set.
	 * @uml.property  name="releaseFile"
	 */
	public void setReleaseFile(String releaseFile) {
		this.releaseFile = releaseFile;
	}

	/**
	 * @uml.property  name="releaseRegExp"
	 */
	private String releaseRegExp = "";

	/**
	 * Getter of the property <tt>releaseRegExp</tt>
	 * @return  Returns the releaseRegExp.
	 * @uml.property  name="releaseRegExp"
	 */
	public String getReleaseRegExp() {
		return releaseRegExp;
	}

	/**
	 * Setter of the property <tt>releaseRegExp</tt>
	 * @param releaseRegExp  The releaseRegExp to set.
	 * @uml.property  name="releaseRegExp"
	 */
	public void setReleaseRegExp(String releaseRegExp) {
		this.releaseRegExp = releaseRegExp;
	}

	/**
	 * @uml.property  name="protocol"
	 */
	private String protocol = "";

	/**
	 * Getter of the property <tt>protocol</tt>
	 * @return  Returns the protocol.
	 * @uml.property  name="protocol"
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * Setter of the property <tt>protocol</tt>
	 * @param protocol  The protocol to set.
	 * @uml.property  name="protocol"
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	/**
	 * @uml.property  name="extract"
	 */
	private Boolean extract = true;

	/**
	 * Getter of the property <tt>extract</tt>
	 * @return  Returns the extract.
	 * @uml.property  name="extract"
	 */
	public Boolean getExtract() {
		return extract;
	}

	/**
	 * Setter of the property <tt>extract</tt>
	 * @param extract  The extract to set.
	 * @uml.property  name="extract"
	 */
	public void setExtract(Boolean extract) {
		this.extract = extract;
	}

	/**
	 * If true, Biomaj create link from the online directory for files which not need to be downloaded, copy files otherwise
	 * @uml.property  name="linkCopy"
	 */
	private Boolean linkCopy = true;

	/**
	 * Getter of the property <tt>linkCopy</tt>
	 * @return  Returns the linkCopy.
	 * @uml.property  name="linkCopy"
	 */
	public Boolean getLinkCopy() {
		return linkCopy;
	}

	/**
	 * Setter of the property <tt>linkCopy</tt>
	 * @param linkCopy  The linkCopy to set.
	 * @uml.property  name="linkCopy"
	 */
	public void setLinkCopy(Boolean linkCopy) {
		this.linkCopy = linkCopy;
	}

}
