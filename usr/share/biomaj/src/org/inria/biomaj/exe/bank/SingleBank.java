/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.bank;

import java.util.Properties;
import org.inria.biomaj.utils.BiomajConst;

/**
 * @uml.stereotype  uml_id="Standard::Metaclass" 
 */
public class SingleBank extends org.inria.biomaj.exe.bank.BiomajBank {

	public SingleBank(Properties props) {
		super(props);
		setDataDirectory(props.getProperty(BiomajConst.dataDirProperty));
		setOfflineDirectory(props.getProperty(BiomajConst.offlineDirProperty));
		setLocalFilesRegExp(props.getProperty(BiomajConst.localFilesProperty));
		setDbType(props.getProperty(BiomajConst.typeProperty));
		setKeepOldVersion(Integer.valueOf(props.getProperty(BiomajConst.keepOldVersionProperty)));
	}
	@Override
	public boolean check() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @uml.property  name="dataDirectory"
	 */
	private String dataDirectory = "";

	/**
	 * Getter of the property <tt>dataDirectory</tt>
	 * @return  Returns the dataDirectory.
	 * @uml.property  name="dataDirectory"
	 */
	public String getDataDirectory() {
		return dataDirectory;
	}

	/**
	 * Setter of the property <tt>dataDirectory</tt>
	 * @param dataDirectory  The dataDirectory to set.
	 * @uml.property  name="dataDirectory"
	 */
	public void setDataDirectory(String dataDirectory) {
		this.dataDirectory = dataDirectory;
	}

	/**
	 * @uml.property  name="offlineDirectory"
	 */
	private String offlineDirectory = "";

	/**
	 * Getter of the property <tt>offlineDirectory</tt>
	 * @return  Returns the offlineDirectory.
	 * @uml.property  name="offlineDirectory"
	 */
	public String getOfflineDirectory() {
		return offlineDirectory;
	}

	/**
	 * Setter of the property <tt>offlineDirectory</tt>
	 * @param offlineDirectory  The offlineDirectory to set.
	 * @uml.property  name="offlineDirectory"
	 */
	public void setOfflineDirectory(String offlineDirectory) {
		this.offlineDirectory = offlineDirectory;
	}

	/**
	 * @uml.property  name="localFilesRegExp"
	 */
	private String localFilesRegExp = "";

	/**
	 * Getter of the property <tt>localFilesRegExp</tt>
	 * @return  Returns the localFilesRegExp.
	 * @uml.property  name="localFilesRegExp"
	 */
	public String getLocalFilesRegExp() {
		return localFilesRegExp;
	}

	/**
	 * Setter of the property <tt>localFilesRegExp</tt>
	 * @param localFilesRegExp  The localFilesRegExp to set.
	 * @uml.property  name="localFilesRegExp"
	 */
	public void setLocalFilesRegExp(String localFilesRegExp) {
		this.localFilesRegExp = localFilesRegExp;
	}

	/**
	 * @uml.property  name="dbType"
	 */
	private String dbType = "";

	/**
	 * Getter of the property <tt>dbType</tt>
	 * @return  Returns the dbType.
	 * @uml.property  name="dbType"
	 */
	public String getDbType() {
		return dbType;
	}

	/**
	 * Setter of the property <tt>dbType</tt>
	 * @param dbType  The dbType to set.
	 * @uml.property  name="dbType"
	 */
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}

	/**
	 * @uml.property  name="keepOldVersion"
	 */
	private Integer keepOldVersion = 0;

	/**
	 * Getter of the property <tt>keeOldVersion</tt>
	 * @return  Returns the keeOldVersion.
	 * @uml.property  name="keeOldVersion"
	 */
	public Integer getKeepOldVersion() {
		return keepOldVersion;
	}

	/**
	 * Setter of the property <tt>keeOldVersion</tt>
	 * @param keeOldVersion  The keeOldVersion to set.
	 * @uml.property  name="keeOldVersion"
	 */
	public void setKeepOldVersion(Integer keeOldVersion) {
		this.keepOldVersion = keeOldVersion;
	}

	private ComputedBank dependsBank;

	/**
	 * Getter of the property <tt>dependsBank</tt>
	 * @return  Returns the dependsBank.
	 * @uml.property  name="dependsBank"
	 */
	public ComputedBank getDependsBank() {
		return dependsBank;
	}

	/**
	 * Setter of the property <tt>dependsBank</tt>
	 * @param dependsBank  The dependsBank to set.
	 * @uml.property  name="dependsBank"
	 */
	public void setDependsBank(ComputedBank dependsBank) {
		this.dependsBank = dependsBank;
	}

}
