/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.bank;

import java.util.Properties;
import org.inria.biomaj.utils.BiomajConst;


/**
 * @uml.stereotype  uml_id="Standard::Metaclass" 
 */
public abstract class BiomajBank {

	public BiomajBank() {
		setPropertiesFromBankFile(new Properties());
		setDbName("MAIN SESSION CITRINA");
		setFullDbName("MAIN SESSION CITRINA");
	}

	public BiomajBank(Properties props) {
		setPropertiesFromBankFile(props);
		setDbName(props.getProperty(BiomajConst.dbNameProperty));
		setFullDbName(props.getProperty(BiomajConst.dbFullNameProperty));
	}
	/**
	 * @uml.property  name="dbName"
	 */
	private String dbName = "";

	/**
	 * Getter of the property <tt>dbName</tt>
	 * @return  Returns the dbName.
	 * @uml.property  name="dbName"
	 */
	public String getDbName() {
		return dbName;
	}

	/**
	 * Setter of the property <tt>dbName</tt>
	 * @param dbName  The dbName to set.
	 * @uml.property  name="dbName"
	 */
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	/**
	 * @uml.property  name="fullDbName"
	 */
	private String fullDbName = "";

	/**
	 * Getter of the property <tt>fullDbName</tt>
	 * @return  Returns the fullDbName.
	 * @uml.property  name="fullDbName"
	 */
	public String getFullDbName() {
		return fullDbName;
	}

	/**
	 * Setter of the property <tt>fullDbName</tt>
	 * @param fullDbName  The fullDbName to set.
	 * @uml.property  name="fullDbName"
	 */
	public void setFullDbName(String fullDbName) {
		this.fullDbName = fullDbName;
	}


	/**
	 * true if property file is well formed
	 */
	public abstract boolean check();

	/**
	 * @uml.property  name="propertiesFromBankFile"
	 */
	private Properties propertiesFromBankFile = new Properties();

	/**
	 * Getter of the property <tt>propertiesFromBankFile</tt>
	 * @return  Returns the propertiesFromBankFile.
	 * @uml.property  name="propertiesFromBankFile"
	 */
	public Properties getPropertiesFromBankFile() {
		return propertiesFromBankFile;
	}

	/**
	 * Setter of the property <tt>propertiesFromBankFile</tt>
	 * @param propertiesFromBankFile  The propertiesFromBankFile to set.
	 * @uml.property  name="propertiesFromBankFile"
	 */
	public void setPropertiesFromBankFile(Properties propertiesFromBankFile) {
		this.propertiesFromBankFile = propertiesFromBankFile;
	}
	
}
