/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.options;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.ProductionDirectory;
import org.inria.biomaj.sql.SQLConnection;
import org.inria.biomaj.sql.SQLConnectionFactory;

public class BiomajCleanDB {
	
	
	private static void deleteFromDbIfFileNotExist(ResultSet rs) {
		SQLConnection c2 = SQLConnectionFactory.getConnection();
		Statement s2 = c2.getStatement();
		try {
			
			while (rs.next()) {
				String location = rs.getString(BiomajSQLQuerier.FILE_LOCATION);
				if (!new File(location).exists()) {
					// Delete from file table
					String query = "DELETE FROM file WHERE idfile=" + rs.getInt(BiomajSQLQuerier.FILE_ID);
					c2.executeUpdate(query, s2);
				}
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		} finally {
			SQLConnectionFactory.closeConnection(s2);
		}
	}

	public static void clean() {

		SQLConnection connection = SQLConnectionFactory.getConnection();
		Statement stat = connection.getStatement();
		
		
		String query = "SELECT idproductionDirectory FROM productionDirectory WHERE state='" + ProductionDirectory.REMOVE_STR + "'";
		ResultSet rs = connection.executeQuery(query, stat);
		try {
			while (rs.next()) {
				BiomajSQLQuerier.deleteRemovedDirectoryRelatedRecords(rs.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		/*
		 * Delete downloaded files that no longer exist
		 */
		query = "SELECT idfile,location FROM file WHERE idfile IN (" +
				"SELECT ref_idfile FROM sessionTask_has_file WHERE ref_idsessionTask IN (" +
				"SELECT idsessionTask FROM sessionTask WHERE taskType='download'))";
		rs = connection.executeQuery(query, stat);
		deleteFromDbIfFileNotExist(rs);
		
		
		SQLConnectionFactory.closeConnection(stat);
		
		/*
		 * Delete extracted files that no long exist
		 */
		
//		query = "SELECT idfile,location FROM file WHERE idfile IN (" +
//				"SELECT ref_idfile FROM sessionTask_has_file WHERE ref_idsessionTask IN (" +
//				"SELECT idsessionTask FROM sessionTask WHERE taskType='extract'))";
//		rs = connection.executeQuery(query, stat);
//		deleteFromDbIfFileNotExist(rs);
		
		/*
		// Deletion of session with no error and no update.
		
		query = "SELECT idsession,logfile FROM session WHERE status=true AND idsession NOT IN " +
				"(SELECT session FROM productionDirectory)";
		
		try {
			ResultSet result = connection.executeQuery(query, stat);
			while(result.next()) {
				BiomajSQLQuerier.deleteSession(result.getLong(1), result.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		// Deletion of updates with no session with error and with no update.
		query = "DELETE FROM updateBank WHERE isUpdated=false " +
				"AND idupdateBank NOT IN (SELECT ref_idupdateBank FROM session)";
		
		stat = connection.getStatement();
		connection.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);
		*/
		
		// Suppression des fichiers obsoletes
//		BiomajSQLQuerier.deleteObsoleteFiles();
	}

}
