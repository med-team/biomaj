/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.tools.ant.Project;
import org.inria.biomaj.exe.bank.BankFactory;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.exe.options.BioMAJLogger;
import org.inria.biomaj.exe.options.BiomajCleanDB;
import org.inria.biomaj.exe.options.BiomajImport;
import org.inria.biomaj.exe.options.BiomajMoveProductionDirectories;
import org.inria.biomaj.exe.options.BiomajRemoveBank;
import org.inria.biomaj.exe.options.ChangeDbName;
import org.inria.biomaj.exe.options.IndexXmlDescription;
import org.inria.biomaj.exe.options.StatusInformation;
import org.inria.biomaj.exe.workflow.WorkflowEngine;
import org.inria.biomaj.exe.workflow.WorkflowEngineFactory;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * 
 * Biomaj main class.
 * Parses command line options and starts required workflows.
 * 
 * @author ofilangi
 */
public class Biomaj {
	/**
	 * Current version of biomaj
	 */
	public static String VERSION = "";

	/**
	 * Name of Environment variable to get the root application
	 */

	public static final String ENV_BIOMAJ = "BIOMAJ_ROOT";
	
	public static final char CONSOLE_SHORT 			= 'c';
	public static final char VERBOSE_SHORT 			= 'v';
	public static final char DEBUG_SHORT 			= 'V';
	public static final char REMOVE_SHORT 			= 'r';
	public static final char KEEP_PROD_SHORT 		= 'k';
	public static final char PATHS_SHORT 			= 'p';
	public static final char ALL_SHORT 				= 'a';
	public static final char ALL_NO_HISTORY_SHORT 	= 'A';
	public static final char UPDATE_SHORT 			= 'd';
	public static final char STAGE_SHORT 			= 's';
	public static final char NEW_SHORT 				= 'N';
	public static final char FROM_SCRATCH_SHORT		= 'z';
	public static final char STATUS_SHORT 			= 'S';
	public static final char TYPE_SHORT 			= 't';
	public static final char ONLINE_SHORT 			= 'o';
	public static final char UPDATING_SHORT 		= 'u';
	public static final char IMPORT_SHORT			= 'I';
	public static final char INDEX_SHORT 			= 'i';
	public static final char CHANGE_DB_NAME_SHORT 	= 'n';
	public static final char REBUILD_SHORT 			= 'b';
	public static final char PROCESS_SHORT 			= 'P';
	public static final char META_SHORT 			= 'M';
	public static final char BLOCK_SHORT 			= 'B';
	public static final char VERSION_SHORT 			= 'w';
	public static final char CLEAN_DB_SHORT 		= 'l';
	public static final char VIEW_LOG_SHORT 		= 'L';
	public static final char HELP_SHORT 			= 'h';
	public static final char MOVE_PROD_SHORT 		= 'e';
	public static final char CONF_LOCATION			= 'C';


	@SuppressWarnings("static-access")
	public static void main (String args []) {
		
		try {
			Class<Biomaj> clazz = Biomaj.class;
			String classContainer = clazz.getProtectionDomain().getCodeSource().getLocation().toString();
			URL manifestUrl = new URL("jar:" + classContainer + "!/META-INF/MANIFEST.MF");
			Manifest mf = new Manifest(manifestUrl.openStream());
			Attributes atts = mf.getMainAttributes();
			
	    	if(atts.getValue("Implementation-Version") != null) {
	    		Biomaj.VERSION = atts.getValue("Implementation-Version");
	    	}
	    	
		} catch (Exception e) {
			System.err.println("Notice: version is not set in the package, this is an error.");
		}
		
		List<Option> allOptions = new ArrayList<Option>();
		
		/*
		 * general.conf location
		 */
		Option optionConfLocation = OptionBuilder.withLongOpt("conf-location")
				.withDescription("Absolute path to general.conf")
				.hasArgs(1)
				.create(CONF_LOCATION);
		
		allOptions.add(optionConfLocation);
		
		
		/*
		 * MAIN OPTIONS
		 */
		Option optionConsole = OptionBuilder.withLongOpt("console")
				.withDescription("Show update progress in a window")
				.create(CONSOLE_SHORT);
		allOptions.add(optionConsole);
		
		
		Option optionVerbose = OptionBuilder.withLongOpt("verbose")
				.withDescription("Verbose mode")
				.create(VERBOSE_SHORT);
		allOptions.add(optionVerbose);
		
		
		Option optionDebug = OptionBuilder.withLongOpt("debug")
				.withDescription("Extra verbose mode\n\n")
				.create(DEBUG_SHORT);
		allOptions.add(optionDebug);
		
		
		/*
		 * REMOVE OPTIONS
		 */
		Option optionRemove = OptionBuilder.withLongOpt("remove")
			.withDescription("Delete production directories")
			.hasArgs(1)
			.withArgName("dbname")
			.create(REMOVE_SHORT);
		allOptions.add(optionRemove);
		

		Option optionKeepDir = OptionBuilder.withLongOpt("keep-dir-prod")
				.withDescription("Remove from database but keep files in repository")
				.create(KEEP_PROD_SHORT);
		allOptions.add(optionKeepDir);
		
		
		Option optionRemovePaths = OptionBuilder.withLongOpt("paths")
				.withDescription("Directly specify the directories paths to delete")
				.hasArgs()
				.withArgName("path")
				.create(PATHS_SHORT);
		allOptions.add(optionRemovePaths);
		
		
		Option optionRemoveAllDirs = OptionBuilder.withLongOpt("all")
				.withDescription("Delete all directories")
				.create(ALL_SHORT);
		allOptions.add(optionRemoveAllDirs);
		
		
		Option optionRemoveAllNoHistory = OptionBuilder.withLongOpt("all-no-history")
				.withDescription("Same as all but keeps no info about the bank in the database")
				.create(ALL_NO_HISTORY_SHORT);
		allOptions.add(optionRemoveAllNoHistory);
		
		
		/*
		 * UPDATE OPTIONS
		 */
		Option optionUpdate = OptionBuilder.withLongOpt("update")
				.withDescription("Update given banks")
				.hasArgs()
				.withArgName("dbname")
				.create(UPDATE_SHORT);
		allOptions.add(optionUpdate);
		
		
		Option optionUpdateStage = OptionBuilder.withLongOpt("stage")
				.withDescription("Stop update at given stage : preprocess|sync|postprocess|deployment")
				.hasArgs(1)
				.withArgName("stage")
				.create(STAGE_SHORT);
		allOptions.add(optionUpdateStage);
		
		
		Option optionUpdateNew = OptionBuilder.withLongOpt("new")
				.withDescription("Start a new update cycle even if a unclosed update cycle is open (ps:previous sessions are ignored)\n")
				.create(NEW_SHORT);
		allOptions.add(optionUpdateNew);
		
		
		Option optionUpdateFromScratch = OptionBuilder.withLongOpt("fromscratch")
				.withDescription("Force update from scratch of the bank by skipping the repository verification\n")
					.create(FROM_SCRATCH_SHORT);
		allOptions.add(optionUpdateFromScratch);
		
		
		/*
		 * STATUS OPTIONS
		 */
		
		Option optionStatus = OptionBuilder.withLongOpt("status")
				.withDescription("Display bank status. If dbname is specified, detailed information is displayed")
				.hasOptionalArg()
				.withArgName("dbname")
				.create(STATUS_SHORT);
		allOptions.add(optionStatus);
		
		
		Option optionStatusType = OptionBuilder.withLongOpt("dbtype")
				.withDescription("Filter bank list according given bank type")
				.hasArgs(1)
				.withArgName("dbtype")
				.create(TYPE_SHORT);
		allOptions.add(optionStatusType);
		
		
		Option optionStatusOnline = OptionBuilder.withLongOpt("online")
				.withDescription("Display only online banks")
				.create(ONLINE_SHORT);
		allOptions.add(optionStatusOnline);
		

		Option optionStatusUpdating = OptionBuilder.withLongOpt("updating")
				.withDescription("Display only currently updating banks")
				.create(UPDATING_SHORT);
		allOptions.add(optionStatusUpdating);


		/*
		 * IMPORT OPTIONS
		 */
		Option optionImport = OptionBuilder.withLongOpt("import")
				.withDescription("Populate database for a bank according to its properties file and production directory. " +
						"Data tree directory must be in compliance with the bank properties files and biomaj tree directory organisation\n")
				.hasArgs(1)
				.withArgName("bank")
				.create(IMPORT_SHORT);
		allOptions.add(optionImport);
		
		
		/*
		 * INDEX OPTIONS
		 */
		Option optionIndex = OptionBuilder.withLongOpt("index")
				.withDescription("Generate an index of all release include in repository\n" +
						"By default index filename is located in : ${statefiles.dir}/index.xml\n")
				.hasOptionalArg()
				.withArgName("file")
				.create(INDEX_SHORT);
		allOptions.add(optionIndex);
		

		/*
		 * RENAME OPTIONS
		 */
		Option optionChangeDbName = OptionBuilder.withLongOpt("change-dbname")
				.withDescription("Change the name of the bank in the database")
				.hasArgs(2)
				.withArgName("bank name")
				.create(CHANGE_DB_NAME_SHORT);
		allOptions.add(optionChangeDbName);


		/*
		 * REBUILD OPTIONS
		 */
		Option optionRebuild = OptionBuilder.withLongOpt("rebuild")
				.withDescription("Rebuild bank named dbname (downgrade the current version checks data and rerun post-processing)\n")
				.hasArgs(1)
				.withArgName("dbname")
				.create(REBUILD_SHORT);
		allOptions.add(optionRebuild);
		
		Option optionRebuildProcess = OptionBuilder.withLongOpt("process")
				.withDescription("Force the execution of process")
				.withArgName("name")
				.hasArgs(1)
				.create(PROCESS_SHORT);
		allOptions.add(optionRebuildProcess);
		
		
		Option optionRebuildMeta = OptionBuilder.withLongOpt("meta")
			.withDescription("Force the execution of metaprocess")
			.withArgName("name")
			.hasArgs(1)
			.create(META_SHORT);
		allOptions.add(optionRebuildMeta);
		
		
		Option optionRebuildBlock = OptionBuilder.withLongOpt("block")
			.withDescription("Force the execution of block")
			.withArgName("name")
			.hasArgs(1)
			.create(BLOCK_SHORT);
		allOptions.add(optionRebuildBlock);
		
		
		/*
		 * VERSION OPTIONS
		 */
		Option optionVersion = OptionBuilder.withLongOpt("version")
				.withDescription("Version information")
				.create(VERSION_SHORT);
		allOptions.add(optionVersion);


		/*
		 * CLEAN OPTIONS
		 */
		Option optionCleanDb = OptionBuilder.withLongOpt("clean-database")
				.withDescription("Delete files that no longer exist from database.\n")
				.create(CLEAN_DB_SHORT);
		allOptions.add(optionCleanDb);

		
		/*
		 * VIEW-LOG OPTIONS
		 */
		Option optionViewLog = OptionBuilder.withLongOpt("view-log")
				.withDescription("Show log viewer window")
				.create(VIEW_LOG_SHORT);
		allOptions.add(optionViewLog);
		

		/*
		 * SHOW HELP
		 */
		Option optionHelp = OptionBuilder.withLongOpt("help")
				.withDescription("Show help")
				.create(HELP_SHORT);
		allOptions.add(optionHelp);


		/*
		 * MOVE PRODUCTION DIRECTORIES
		 */
		Option optionMoveProductionDirectories = OptionBuilder.withLongOpt("move-production-directories")
				.withDescription("Export all production directories to the new version directory determined from the properties data.dir and version.dir.\n")
				.hasArgs(1)
				.withArgName("dbname")
				.create(MOVE_PROD_SHORT);
		allOptions.add(optionMoveProductionDirectories);
		
		
		/*
		 * Building group
		 */
		
		// Main options
		List<Option> mainOptions = new ArrayList<Option>();
		mainOptions.add(optionUpdate);
		mainOptions.add(optionStatus);
		mainOptions.add(optionRebuild);
		mainOptions.add(optionRemove);
		mainOptions.add(optionImport);
		mainOptions.add(optionMoveProductionDirectories);
		mainOptions.add(optionIndex);
		mainOptions.add(optionHelp);
		mainOptions.add(optionVersion);
		mainOptions.add(optionViewLog);
		mainOptions.add(optionCleanDb);
		mainOptions.add(optionChangeDbName);

		Options options = new Options();
		
		// Adding all options
		for (Option opt : allOptions) {
			options.addOption(opt);
		}
		
		// Main options are exclusive
		OptionGroup mainGrp = new OptionGroup();
		for (Option opt : mainOptions) {
			mainGrp.addOption(opt);
		}
		options.addOptionGroup(mainGrp);
		
		// Fromscratch and new are exclusive
		OptionGroup updateOptions = new OptionGroup();
		updateOptions.addOption(optionUpdateFromScratch);
		updateOptions.addOption(optionUpdateNew);
		options.addOptionGroup(updateOptions);
		
		// Some exclusive options for remove
		OptionGroup removeOptions = new OptionGroup();
		removeOptions.addOption(optionRemoveAllDirs);
		removeOptions.addOption(optionRemoveAllNoHistory);
		removeOptions.addOption(optionRemovePaths);
		options.addOptionGroup(removeOptions);

		// Rebuild sub options are exclusive
		OptionGroup rebuildOptions = new OptionGroup();
		rebuildOptions.addOption(optionRebuildBlock);
		rebuildOptions.addOption(optionRebuildMeta);
		rebuildOptions.addOption(optionRebuildProcess);
		options.addOptionGroup(rebuildOptions);

		CommandLine cl = null;
		try {
			cl = new BasicParser().parse(options, args);
		} catch (ParseException e) {
			System.err.println(e.getMessage());
			System.out.println("Run biomaj.sh --help to see syntax");
		}
		if (cl == null) {
			System.exit(-2);
		}
		
		/*
		 * VERSION
		 *********************/
		if (cl.hasOption(VERSION_SHORT)) {	
			System.out.println("BioMAJ version "+VERSION);
			System.exit(0);
		}
		
		/*
		 * HELP
		 *******************/
		if (cl.hasOption(HELP_SHORT)) {
			printHelp();
			System.exit(0);
		}

		// Test general configuration
		try {
			if (cl.hasOption(CONF_LOCATION)) {
				String path = cl.getOptionValue(CONF_LOCATION);
				if (!path.trim().isEmpty()) {
					BiomajInformation.CONF_LOCATION = path;
				}
			}
			BiomajInformation.getInstance();
		} catch (Exception e) {
			System.err.println(e.getLocalizedMessage());
			System.exit(-3);
		}
		

		try {
			/*
			 * UPDATE AND REBUILD
			 *********************/
			if (cl.hasOption(UPDATE_SHORT) || cl.hasOption(REBUILD_SHORT)) {
				String[] listP = null, listM= null,listB = null;
				String[] banks ;
				int command = WorkflowEngine.TARGET_ALL;
				if (cl.hasOption(UPDATE_SHORT)) {
					banks = cl.getOptionValues(UPDATE_SHORT);
				} else {
					command =  WorkflowEngine.TARGET_REBUILD;
					banks = cl.getOptionValues(REBUILD_SHORT);
					
					if (cl.hasOption(PROCESS_SHORT) || cl.hasOption(META_SHORT) || cl.hasOption(BLOCK_SHORT)) {
						
						listP = new String[cl.getOptionValues(PROCESS_SHORT).length];

						for (int i = 0; i < cl.getOptionValues(PROCESS_SHORT).length; i++)
							listP[i] = cl.getOptionValues(PROCESS_SHORT)[i];

						listM = new String[cl.getOptionValues(META_SHORT).length];

						for (int i = 0; i < cl.getOptionValues(META_SHORT).length; i++)
							listM[i] = cl.getOptionValues(META_SHORT)[i];

						listB = new String[cl.getOptionValues(BLOCK_SHORT).length];

						for (int i = 0; i < cl.getOptionValues(BLOCK_SHORT).length; i++)
							listB[i] = cl.getOptionValues(BLOCK_SHORT)[i];
					}
				}

				if (cl.hasOption(STAGE_SHORT)) {
					command = getCommand(cl.getOptionValue(STAGE_SHORT));
				}
				
				int mode = Project.MSG_INFO;
				if (cl.hasOption(VERBOSE_SHORT)) {
					mode = Project.MSG_VERBOSE;
				}
				if (cl.hasOption(DEBUG_SHORT)) {
					mode = Project.MSG_DEBUG;
				}

				processBanks(banks, command, cl.hasOption(CONSOLE_SHORT), mode, false, cl.hasOption(NEW_SHORT),
						cl.hasOption(FROM_SCRATCH_SHORT), listP, listM, listB);
			}
			
			/*
			 * REMOVE
			 *********************/
			else if (cl.hasOption(REMOVE_SHORT)) {
				BiomajRemoveBank brb = new BiomajRemoveBank();
				int mode = Project.MSG_INFO;
				if (cl.hasOption(VERBOSE_SHORT)) {
					mode = Project.MSG_VERBOSE;
				}
				if (cl.hasOption(DEBUG_SHORT)) {
					mode = Project.MSG_DEBUG;
				}
				brb.setShowConsole(cl.hasOption(CONSOLE_SHORT), mode);
				brb.setBankName(cl.getOptionValue(REMOVE_SHORT));
				brb.setKeepProductionDirectory(cl.hasOption(KEEP_PROD_SHORT));
				if (cl.hasOption(ALL_SHORT))
					brb.executeWithoutUserInteraction(null, true, true);
				else if (cl.hasOption(ALL_NO_HISTORY_SHORT))
					brb.executeWithoutUserInteraction(null, true, false);
				else if (cl.hasOption(PATHS_SHORT))
					brb.executeWithoutUserInteraction(cl.getOptionValues(PATHS_SHORT), false, true);
				else
					brb.execute();
			}
			
			/*
			 * STATUS
			 *********************/
			else if (cl.hasOption(STATUS_SHORT)) {


				if (cl.getOptionValue(STATUS_SHORT) == null || cl.hasOption(TYPE_SHORT) || cl.hasOption(UPDATING_SHORT) || cl.hasOption(ONLINE_SHORT)) {
					String filterType = "";
					if (cl.hasOption(TYPE_SHORT)) 
						filterType = cl.getOptionValue(TYPE_SHORT);
					StatusInformation.getStatus(filterType, cl.hasOption(UPDATING_SHORT) ,cl.hasOption(ONLINE_SHORT));
				} else {
					StatusInformation.getStatus(cl.getOptionValue(STATUS_SHORT));
				}

			}
			
			/*
			 * CLEAN-DATABASE
			 *********************/
			else if (cl.hasOption(CLEAN_DB_SHORT)) {
				BiomajCleanDB.clean();
			}
			
			/*
			 * LOG
			 *********************/
			else if (cl.hasOption(VIEW_LOG_SHORT)) {
				System.out.println("Please wait...");
				BioMAJLogger v = new BioMAJLogger();
				
				v.execute();
			}
			
			/*
			 * INDEX
			 *********************/
			else if (cl.hasOption(INDEX_SHORT)) {
				IndexXmlDescription gen = new IndexXmlDescription();
				gen.createXmlGeneralFile(cl.getOptionValue(INDEX_SHORT));
			}
			
			/*
			 * CHANGE-DBNAME
			 *********************/
			else if (cl.hasOption(CHANGE_DB_NAME_SHORT)) {
				ChangeDbName.changeDbName(cl.getOptionValues(CHANGE_DB_NAME_SHORT)[0], cl.getOptionValues(CHANGE_DB_NAME_SHORT)[1]);
			} 

			/*
			 * IMPORT
			 *********************/
			else if (cl.hasOption(IMPORT_SHORT)) {
				BiomajImport.importIntoDB(cl.getOptionValue(IMPORT_SHORT));
			}
			
			/*
			 * MOVE PRODUCTION VERSION
			 ****************************/
			else if (cl.hasOption(MOVE_PROD_SHORT)) {
				BiomajMoveProductionDirectories move = new BiomajMoveProductionDirectories();
				move.moveProductionDirectories(cl.getOptionValue(MOVE_PROD_SHORT));
			}
			
			
		} catch (Exception be) {
			System.err.println(be.getLocalizedMessage());
			System.err.println("** BioMAJ stopped. **");
			BiomajLogger.getInstance().log(be.getLocalizedMessage());
		}
	}

	private static void printHelp() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(BiomajUtils.getBiomajRootDirectory() + "/console_help.txt"));
			String line = "";
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	protected static void processBanks(String[] banks,int command,boolean withConsole,int mode,boolean checkCurrent,boolean newUpdate,boolean fromScratch,
			String[] listP,String[] listM,String[] listB) {
		if (banks.length == 0) {
			System.out.println("There are no bank to process.");
			return;
		}

		setProxyEnvironment();
		BankFactory bf = new BankFactory();
		List<Thread> running = new ArrayList<Thread>();
		for (String bank : banks) {
			try {
//				BiomajCleanStatefile.corrigeCompatibiliteVersion(bank);
				BiomajBank bb = bf.createBank(bank,true);
				// System.out.println("name:"+bb.getDbName());
				WorkflowEngineFactory wef = new WorkflowEngineFactory();
				WorkflowEngine we ;
				we = wef.createWorkflow(bb);
				we.setModeConsole(withConsole, mode);
				we.setWorkWithCurrentDirectory(checkCurrent);
				we.setForceForANewUpdate(newUpdate);
				we.setFromScratch(fromScratch);
				we.setCommand(command);
				we.setListBlockProcessToRebuild(listB);
				we.setListMetaProcessToRebuild(listM);
				we.setListProcessToRebuild(listP);
				we.start();
				running.add(we);

			} catch (BiomajException e) {
				System.err.println("Biomaj has detected an error! for bank ["+ bank+"]");
				System.err.println(e.getLocalizedMessage());
				
			} catch (BiomajBuildException bbe) {
				System.err
				.println("Biomaj has detected an error during the execution of workflow for the bank :"
						+ bank);
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println(e.getLocalizedMessage());
			}
		}
		
		// Wait for all the threads to end before closing connection
		for (Thread t : running) {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
//		BiomajSQLQuerier.closeConnection();
	}


	/**
	 * Set proxy
	 * author:ofilangi
	 * 
	 *  a tester
	 */

	private static void setProxyEnvironment() {
		final Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(new File(BiomajInformation.getInstance().getProperty(BiomajInformation.WORKFLOWSDIR)+"/"+BiomajConst.globalProperties)));
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.exit(-1);
		}
		/**
		 * AUTRE METHODE : http://floatingsun.net/articles/java-proxy.html
		 */

		/*
		if (prop.containsKey(BiomajConst.httpsProxyHost)||prop.containsKey(BiomajConst.httpsProxyPassword)||
				prop.containsKey(BiomajConst.httpsProxyPort)||prop.containsKey(BiomajConst.httpsProxyUser)) {

			System.setProperty("java.protocol.handler.pkgs","com.sun.net.ssl.internal.www.protocol");
			System.out.println("values for proxy https...");

			if (prop.containsKey(BiomajConst.httpsProxyHost))
			{
				System.setProperty("https.proxyHost",prop.getProperty(BiomajConst.httpsProxyHost));
				System.out.println("https.proxyHost:"+prop.getProperty(BiomajConst.httpsProxyHost));
			}

			if (prop.containsKey(BiomajConst.httpsProxyPort))
			{
				System.setProperty("https.proxyPort",prop.getProperty(BiomajConst.httpsProxyPort));
				System.out.println("https.proxyPort"+prop.getProperty(BiomajConst.httpsProxyPort));
			}

			if (prop.containsKey(BiomajConst.httpsProxyUser))
			{
				System.setProperty("https.proxyUser",prop.getProperty(BiomajConst.httpsProxyUser));
				System.out.println("https.proxyUser"+prop.getProperty(BiomajConst.httpsProxyUser));
			}

			if (prop.containsKey(BiomajConst.httpsProxyPassword))
			{
				System.setProperty("https.proxyPassword",prop.getProperty(BiomajConst.httpsProxyPassword));
				System.out.println("https.proxyPassword"+prop.getProperty(BiomajConst.httpsProxyPassword));
			}

			if (prop.containsKey(BiomajConst.httpsProxyUser)&&prop.containsKey(BiomajConst.httpsProxyPassword))
				Authenticator.setDefault( new HttpAuthenticateProxy(prop.getProperty(BiomajConst.httpsProxyUser),prop.getProperty(BiomajConst.httpsProxyPassword)) );

		} else */
		if (prop.containsKey(BiomajConst.proxyHost)||prop.containsKey(BiomajConst.proxyPassword)||
				prop.containsKey(BiomajConst.proxyPort)||prop.containsKey(BiomajConst.proxyUser)) {
			
			System.getProperties().put("proxySet", "true");

			if (prop.containsKey(BiomajConst.proxyHost)) {
				System.getProperties().put( "socksProxyHost" ,prop.getProperty(BiomajConst.proxyHost));
			}

			if (prop.containsKey(BiomajConst.proxyPort)) {
				System.getProperties().put( "socksProxyPort",prop.getProperty(BiomajConst.proxyPort));
			}
			if (prop.containsKey(BiomajConst.proxyUser)&&prop.containsKey(BiomajConst.proxyPassword)) {
				// Authenticate to the proxy
				Authenticator.setDefault(new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(prop.getProperty(BiomajConst.proxyUser),
								prop.getProperty(BiomajConst.proxyPassword).toCharArray());
					}
				});
				System.getProperties().put("socksProxyUser", prop.getProperty(BiomajConst.proxyUser));
				System.getProperties().put("socksProxyPassword", prop.getProperty(BiomajConst.proxyPassword));
			}
		}
	}

	protected static Integer getCommand(String c) {

		if (c == null)
			return WorkflowEngine.TARGET_ALL;

		if (c.compareTo("preprocess") == 0)
			return WorkflowEngine.TARGET_UNTIL_PREPROCESS;
		if ((c.compareTo("sync") == 0) || (c.compareTo("mirror") == 0))
			return WorkflowEngine.TARGET_UNTIL_MIRROR;
		if (c.compareTo("postprocess") == 0)
			return WorkflowEngine.TARGET_UNTIL_POSTPROCESS;
		if (c.compareTo("deploy") == 0)
			return WorkflowEngine.TARGET_UNTIL_DEPLOY;

		// System.err.println(c+" unknown option!");
		return WorkflowEngine.TARGET_ALL;
	}
}
