package org.inria.biomaj.exe.migration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.hsqldb.cmdline.SqlFile;
import org.hsqldb.cmdline.SqlToolError;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.sql.SQLConnection;
import org.inria.biomaj.sql.SQLConnectionFactory;
import org.inria.biomaj.utils.BiomajException;

/**
 * 
 * Update program from biomaj core 1.1 to biomaj core 1.2
 * 	- Moves the properties under admin_login directory
 * 	- Update db (create user tables, alter bank table, add admin records)
 * 
 * @author rsabas
 *
 */
public class CoreMigrationFrom1_1To1_2 {

private static String biomajRoot = "";
	
	public static void main(String[] args) {
		
		if (args.length != 3) {
			System.err.println("SYNTAX : CoreMigrationFrom1_1To1_2 <admin_login> <admin_passwd> <admin_mail>");
			System.exit(1);
		}
		
		biomajRoot = System.getProperty("BIOMAJ_ROOT");
		if (biomajRoot == null) {
			biomajRoot = System.getenv("BIOMAJ_ROOT");
			if (biomajRoot == null) {
				System.err.println("Could not find BIOMAJ_ROOT environment variable");
				return;
			}
		}
		
		// Get admin login
		String login = args[0];
		
		// Get admin password
		String passwd = args[1];
		String hash;
		
		// Get admin email
		String mail = args[2];
		
		
		// Generates admin auth_key
		String key = UUID.randomUUID().toString();
		
		// Generates password hash
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA1");
			digest.update(passwd.getBytes());
			byte[] hashedPasswd = digest.digest();
			hash = getHexString(hashedPasswd);
		} catch (NoSuchAlgorithmException e) {
			System.err.println(e.getMessage());
			return;
		}
		
		
		/*
		 * Update database.
		 */
		if (updateDB(login, hash, key, mail)) {
			
			/*
			 * Move properties under admin directory
			 */
			movePropertiesToAdmin(login);
		}
		
		
	}
	
	private static boolean updateDB(String login, String hash, String key, String mail) {
		SQLConnection connection = SQLConnectionFactory.getConnection();
		Statement stat = connection.getStatement();
		
		boolean isMySQL = SQLConnectionFactory.getDBType().equals(SQLConnectionFactory.MYSQL_CONNECTION);
		
		if (isMySQL) {
			if (!createNewMySQLTables(connection, stat)) {
				SQLConnectionFactory.closeConnection(stat);
				System.err.println("Migration aborted");
				return false;
			}
		} else {
			createNewHSQLTables(stat);
		}
		
		int userId;
		// Check if record exists
		String checkUser = "SELECT iduser FROM bw_user WHERE login='" + login + "'";
		ResultSet rs = connection.executeQuery(checkUser, stat);
		try {
			if (rs.next()) { // User exists
				userId = rs.getInt(1);
				String updateUser = "UPDATE bw_user SET password='" + hash + "'," +
						"auth_key='" + key + "'," +
						"mail_address='" + mail + "' WHERE iduser=" + userId;
				connection.executeUpdate(updateUser, stat);
			} else {
				
				String firstUser = "SELECT * FROM bw_user";
				rs = connection.executeQuery(firstUser, stat);
				if (rs.next()) { // Users (thus admin) already exist, do not add
					System.err.println("An admin already exists.");
					SQLConnectionFactory.closeConnection(stat);
					return false;
				} else {
					// Insert admin related records
					String insertUser = "INSERT INTO bw_user(login, password, auth_type, auth_key, mail_address) " +
							"VALUES('" + login + "','" + hash + "','local','" + key + "','" + mail + "')";
					userId = connection.executeUpdateAndGetGeneratedKey(insertUser, stat);
					
					String createGroup = "INSERT INTO bw_group(name) VALUES('admin')";
					int groupId = connection.executeUpdateAndGetGeneratedKey(createGroup, stat);
					String userToGroup = "INSERT INTO bw_user_has_group(ref_iduser, ref_idgroup) VALUES(" + userId + "," + groupId + ")";
					connection.executeUpdate(userToGroup, stat);
				}
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			SQLConnectionFactory.closeConnection(stat);
			return false;
		}
		
		boolean updated = false;
		try {
			String testTable = "SELECT visibility FROM bank";
			stat.executeQuery(testTable);
			updated = true; // No error, column already added
		} catch (SQLException e) {
			updated = false;
		}
		
		if (!updated) { // Not yet updated
			// Modify table bank to add ref_iduser foreign key
			if (isMySQL) {
				String mysql_addColumnToBank = "ALTER TABLE bank ADD COLUMN ref_iduser INT NOT NULL DEFAULT " + userId + " AFTER name;";
				connection.executeUpdate(mysql_addColumnToBank, stat);
				
				mysql_addColumnToBank = "ALTER TABLE bank ADD COLUMN visibility TINYINT(1) DEFAULT 0;";
				connection.executeUpdate(mysql_addColumnToBank, stat);
				
				String mysql_addConstraint = "ALTER TABLE bank ADD CONSTRAINT fk_bank_to_user FOREIGN KEY (ref_iduser) REFERENCES bw_user(iduser);";
				connection.executeUpdate(mysql_addConstraint, stat);
				
				String mysql_createIndex = "CREATE INDEX `fk_bank_user1` ON `biomaj_log`.`bank` (`ref_iduser` ASC);";
				connection.executeUpdate(mysql_createIndex, stat);
			} else {
				String hsql_addColumnToBank = "ALTER TABLE bank ADD COLUMN ref_iduser INT DEFAULT " + userId;
				connection.executeUpdate(hsql_addColumnToBank, stat);
				
				hsql_addColumnToBank = "ALTER TABLE bank ADD COLUMN visibility BOOLEAN DEFAULT false";
				connection.executeUpdate(hsql_addColumnToBank, stat);
				
				String hsql_addConstraint = "ALTER TABLE bank ADD CONSTRAINT fk_bank_to_user FOREIGN KEY (ref_iduser) REFERENCES bw_user(iduser)";
				connection.executeUpdate(hsql_addConstraint, stat);
			}
		}
		
		SQLConnectionFactory.closeConnection(stat);
		return true;
	}
	
	private static void movePropertiesToAdmin(String adminLogin) {
		
		try {
			String path = BiomajInformation.getInstance().getProperty(BiomajInformation.WORKFLOWSDIR);
			File dir = new File(path);
			File adminDir = new File(path + "/" + adminLogin);
			if (!adminDir.exists())
				adminDir.mkdir();
			
			for (File f : dir.listFiles()) {
				String currentPath = f.getAbsolutePath();
				if ((f.isDirectory() && f.getName().equals("include")) ||
						(f.isFile() && currentPath.endsWith(".properties") && !f.getName().equals("global.properties"))) {
					if (!f.renameTo(new File(adminDir.getAbsolutePath() + "/" + f.getName()))) {
						System.err.println("Could not move " + f.getAbsolutePath());
					}
				}
			}
			
		} catch (BiomajException e) {
			e.printStackTrace();
		}
		
	}
	
	private static void createNewHSQLTables(Statement stat) {
		File file = new File(biomajRoot + "/sql/hsql-to_1.2.sql");
		try {
			SqlFile sqlFile = new SqlFile(file);
			sqlFile.setConnection(stat.getConnection());
			
			sqlFile.execute();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (SqlToolError e) {
			e.printStackTrace();
		}
	}
	
	private static boolean createNewMySQLTables(SQLConnection connection, Statement stat) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(biomajRoot + "/sql/mysql-to_1.2.sql"));
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			return false;
		}
		
		List<String> res = new ArrayList<String>();
		String line = "";
		String query = "";
		try {
			while ((line = br.readLine()) != null) {
				if (!line.startsWith("--")) {
					query += line;
					if (line.endsWith(";")) {
						res.add(query);
						query = "";
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			try {
				br.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			return false;
		}
		
		// Create tables
		for (String s : res) {
			connection.executeUpdate(s, stat);
		}
		
		return true;
	}
	
	public static String getHexString(byte[] buf) {
		char[] TAB_BYTE_HEX = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
				'9', 'a', 'b', 'c', 'd', 'e', 'f' };

		StringBuilder sb = new StringBuilder(buf.length * 2);

		for (int i = 0; i < buf.length; i++) {
			sb.append(TAB_BYTE_HEX[(buf[i] >>> 4) & 0xf]);
			sb.append(TAB_BYTE_HEX[buf[i] & 0x0f]);
		}
		return sb.toString();
	}
}
