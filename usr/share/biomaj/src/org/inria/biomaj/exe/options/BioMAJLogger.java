package org.inria.biomaj.exe.options;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.*;

import java.io.File;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;


import javax.swing.*;
import javax.swing.border.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.apache.tools.ant.Project;
import org.inria.biomaj.exe.bank.BankFactory;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.exe.main.Biomaj;
import org.inria.biomaj.session.bank.Bank;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.Configuration;
import org.inria.biomaj.session.bank.GeneralWorkflowTask;
import org.inria.biomaj.session.bank.PostProcessTask;
import org.inria.biomaj.session.bank.PreProcessTask;
import org.inria.biomaj.session.bank.RemoveProcessTask;
import org.inria.biomaj.session.bank.Session;
import org.inria.biomaj.session.process.MetaProcess;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
/*
 * Created by JFormDesigner on Thu Mar 20 14:21:41 CET 2008
 */



/**
 * @author Olivier Filangi
 */
public class BioMAJLogger extends JPanel implements WindowListener {

	private static final long serialVersionUID = 1L;

	private StyledDocument sdoc;

	private final int REFRESH_SCREEN = 3000 ; // 3 seconds
	//private final int REFRESH_LIST   = 8000 ; // 8 seconds

	public BioMAJLogger() {
		initComponents();

		sdoc = fileLogTextArea.getStyledDocument();

		Style titleStyle = sdoc.addStyle("info", null);
		StyleConstants.setBackground(titleStyle , Color.WHITE);
		StyleConstants.setForeground(titleStyle , Color.BLACK);
		StyleConstants.setFontFamily(titleStyle , "dialoginput");
		//StyleConstants.setBold(titleStyle,true);
		StyleConstants.setFontSize(titleStyle , 12);

		Style descrStyle = sdoc.addStyle("date", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.ORANGE);

		descrStyle = sdoc.addStyle("task", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.BLUE);
		//StyleConstants.setBold(descrStyle, true);

		descrStyle = sdoc.addStyle("unknown", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.BLUE);
		//StyleConstants.setBold(descrStyle, true);

		descrStyle = sdoc.addStyle("erreur", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.RED);
		StyleConstants.setBold(descrStyle, true);

		descrStyle = sdoc.addStyle("warning", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.MAGENTA);
		StyleConstants.setBold(descrStyle, true);

		descrStyle = sdoc.addStyle("debug", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.GRAY);
		StyleConstants.setBold(descrStyle, true);

		descrStyle = sdoc.addStyle("verbose", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.DARK_GRAY);
		//StyleConstants.setBold(descrStyle, true);
		descrStyle = sdoc.addStyle("depends", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.GREEN);

		descrStyle = sdoc.addStyle("depends_volatil", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.ORANGE);


		updatingRadioButton.setSelected(true);
		errorCycleRadioButton.setSelected(false);


		refreshAll(null);
	}

	private void initListType() {
		Vector<String> listType = getListTypeSql();
		listTypeInBankLayoutComboBox.removeAllItems();
		for (String s : listType) {
			listTypeInBankLayoutComboBox.addItem(s);
		}
		try {
			sdoc.remove(0, sdoc.getLength());
		} catch (Exception e) {

		}
	}

	//private Timer timerListBank = null ;
	private void initListBankLayoutActionPerformed(ActionEvent e) {

		/*
		if (listeBankNameInBankLayoutComboBox.isPopupVisible()) {
			if (timerListBank != null)
				timerListBank.stop();

			return;
		} else {
			if (timerListBank != null)
				timerListBank.start();
		}
		 */
		String type = (String) listTypeInBankLayoutComboBox.getSelectedItem();
		/*
		if (( listTypeInBankLayoutComboBox.getItemCount()>listTypeInBankLayoutComboBox.getSelectedIndex()) &&
				(listTypeInBankLayoutComboBox.getSelectedIndex()>0))
			type = (String)listTypeInBankLayoutComboBox.getItemAt(listTypeInBankLayoutComboBox.getSelectedIndex());
		 */

		Vector<String> listBankName = getListBankSql(type);
		listeBankNameInBankLayoutComboBox.removeAllItems();
		for (String s : listBankName)
		{
			if (radioButtonAll.isSelected()) {
				listeBankNameInBankLayoutComboBox.addItem(s);
				continue;
			}
//			try {
				Bank b = new Bank();// || (updatingRadioButton.isSelected()&&(!b.getStatus()))
				Map<String, String> update = BiomajSQLQuerier.getLatestUpdate(s, false);
				b.fill(update, true);
//				if (!BiomajQueryXmlStateFile.getLastUpdateBankWithoutConfiguration(s, b, true)) {
				if (update == null) {
					if (updatingRadioButton.isSelected())
						listeBankNameInBankLayoutComboBox.addItem(s);

				} else {
					//System.out.println("getLastUpdateBankWithoutConfiguration-->true");
					//System.out.println("STATUS:"+Boolean.valueOf(b.getStatus()));
					if (updatingRadioButton.isSelected()&&(b.getEnd()==null) ) 
						listeBankNameInBankLayoutComboBox.addItem(s);
					else if (errorCycleRadioButton.isSelected()&& !b.getStatus() && (b.getEnd()!=null))
						listeBankNameInBankLayoutComboBox.addItem(s);
				}
//			} catch (BiomajException be) {
//				BiomajLogger.getInstance().log(be);
//			}
		}

		/*
		if (timerListBank == null) {
			timerListBank = new Timer(REFRESH_LIST,new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					initListBankLayoutActionPerformed(e);
				}});

			timerListBank.start();
		}
		 */
	}

	private void refreshAll(ActionEvent e) {
		if (timerCurrentfile != null) {
			timerCurrentfile.stop();
			timerCurrentfile = null ;
		}
		
		currentBank = null ;
		map_nameFileAndCompletePathFile = new HashMap<String, String>();
		currentFileLog = "";
		dateCurrentFileLog = 0;
		nbLine = 0 ;
		timerCurrentfile = null ;
		oldLevelSelected = -1 ;
		
		initListType();
		initListBankLayoutActionPerformed(null);
		initInformationSessionAndBankActionPerformed(null);
		listSessionsInSessionLayoutActionPerformed(null);
		listFileLogInLogLayoutActionPerformed(null);
	}

	private void radioButtonBankLayoutActionPerformed(ActionEvent e) {
		refreshAll(null);
	}

	private void emptyBankFill() {
		descriptionField.setText("");
		urlField.setText("");
		remoteFilesExcludedField.setText("");
		remoteFilesField.setText("");
		versionDirectoryField.setText("");
		offlineDirectoryField.setText("");

		listVersionInSessionLayoutComboBox.removeAllItems();
		listFileLogInLogLayoutComboBox.removeAllItems();
		try {
			sdoc.remove(0, sdoc.getLength());
		} catch (BadLocationException e) {
			BiomajLogger.getInstance().log(e);
		}
	}

	private Bank currentBank = null ;

	private void initInformationSessionAndBankActionPerformed(ActionEvent e) {
		String bankName = (String) listeBankNameInBankLayoutComboBox.getSelectedItem();

		if (bankName == null)
		{
			//	emptyBankFill();
			return ;
		}

		if (currentBank!=null && bankName.compareTo(currentBank.getConfig().getName())==0)
		{
			//on verifie que le panel Session a ete initialise
			if ((descriptionField.getText()!=null) && (descriptionField.getText().compareTo("")!=0))
				return ;
		}

		currentBank = new Bank();
//		try {
			/*
			if (!BiomajQueryXmlStateFile.getFuturReleaseBankWithConfiguration(bankName,currentBank,true)) {
				if (!BiomajQueryXmlStateFile.getLastUpdateBankWithNewReleaseWithConfiguration(bankName, currentBank, true)) {
					emptyBankFill();
					return ;
				}
			}
			 */
			
			Map<String, String> update = BiomajSQLQuerier.getLatestUpdate(bankName, true);
			if (update != null) {
				int id = Integer.parseInt(update.get(BiomajSQLQuerier.UPDATE_ID));
				Map<String, String> info = BiomajSQLQuerier.getBankInfoForUpdate(id);
				Configuration config = new Configuration(info);
				currentBank.setConfig(config);
				currentBank.fill(update, true);
			} else
				emptyBankFill();
			
//			BiomajQueryXmlStateFile.getLastUpdateBankWithConfiguration(bankName, currentBank, true);
			/*
		} catch (BiomajException be) {
			BiomajLogger.getInstance().log(be);
			emptyBankFill();
			return ;
		}*/
		Bank b = currentBank ;

		descriptionField.setText(b.getConfig().getFullName());
		urlField.setText(b.getConfig().getProtocol()+"://"+b.getConfig().getUrl()+"/"+b.getConfig().getRemoteDirectory());
		remoteFilesExcludedField.setText(b.getConfig().getRemoteExcludedFiles());
		remoteFilesField.setText(b.getConfig().getRemoteFilesRegexp());
		versionDirectoryField.setText(b.getConfig().getVersionDirectory());
		offlineDirectoryField.setText(b.getConfig().getOfflineDirectory());

		listVersionInSessionLayoutComboBox.removeAllItems();

		for (Session s : b.getListOldSession()) {
			listVersionInSessionLayoutComboBox.addItem(BiomajUtils.dateToString(s.getStart(), Locale.US));
		}

		if (listVersionInSessionLayoutComboBox.getItemCount()>0)
			listVersionInSessionLayoutComboBox.setSelectedIndex(listVersionInSessionLayoutComboBox.getItemCount()-1);


		BankFactory bf = new BankFactory();
		try {
			BiomajBank bb = bf.createBank(b.getConfig().getName(), false);
			String level = bb.getPropertiesFromBankFile().getProperty(BiomajConst.levelMaskProperty) ;
			if (level != null) {
				if (level.compareTo("VERBOSE")==0) {
					debugRadioButton.setEnabled(false);
				} else if (level.compareTo("INFO")==0) {
					debugRadioButton.setEnabled(false);
					verboseRadioButton.setEnabled(false);
				} else if (level.compareTo("WARNING")==0) {
					debugRadioButton.setEnabled(false);
					verboseRadioButton.setEnabled(false);
					infoRadioButton.setEnabled(false);
				} else if (level.compareTo("ERROR")==0) {
					debugRadioButton.setEnabled(false);
					verboseRadioButton.setEnabled(false);
					infoRadioButton.setEnabled(false);
					warningRadioButton.setEnabled(false);
				} 
			}

		} catch (BiomajException be) {
			BiomajLogger.getInstance().log(be);
		}
		infoRadioButton.setSelected(true);
	}

	private HashMap<String, String> map_nameFileAndCompletePathFile = new HashMap<String, String>();

	//private Timer timerFileLog = null;

	private void listSessionsInSessionLayoutActionPerformed(ActionEvent e) {
		if (currentBank == null)
			return ;
		/*
		if (listFileLogInLogLayoutComboBox.isPopupVisible()) {
			if (timerFileLog != null)
				timerFileLog.stop();

			return;
		} else {
			if (timerFileLog != null)
				timerFileLog.start();
		}
		 */
		Date start = null ;

		try {
			start = BiomajUtils.stringToDate((String) listVersionInSessionLayoutComboBox.getSelectedItem());



			Session find = null ;
			for (Session s : currentBank.getListOldSession()) {
				if (s.getStart().getTime() == start.getTime()) {
					find = s ;
					break ;
				}
			}

			if (find == null)
				return ;

			//int valueDefault = listFileLogInLogLayoutComboBox.getSelectedIndex();
			listFileLogInLogLayoutComboBox.removeAllItems();
			//System.out.println("session id:"+find.getId()+" log:"+find.getLogfile());
			String baseName = "Main activity report";
			listFileLogInLogLayoutComboBox.addItem(baseName);
			map_nameFileAndCompletePathFile.put(baseName, find.getLogfile());

			GeneralWorkflowTask gPre = find.getWorkflowTask(Session.PREPROCESS);
			if (gPre != null) {
				PreProcessTask pPre = (PreProcessTask)gPre;
				Vector<MetaProcess> mlPre = pPre.getMetaProcess(null);
				for (MetaProcess mp : mlPre) {
					baseName = "Processing activity report ["+mp.getBlock()+":"+mp.getName()+"]";
					listFileLogInLogLayoutComboBox.addItem(baseName);
					map_nameFileAndCompletePathFile.put(baseName, mp.getLogFile());
				}
			}

			GeneralWorkflowTask g = find.getWorkflowTask(Session.POSTPROCESS);
			if (g != null) {
				PostProcessTask p = (PostProcessTask)g ;
				Vector<MetaProcess> ml = p.getMetaProcess(null);
				for (MetaProcess mp : ml) {
					baseName = "Processing activity report ["+mp.getBlock()+":"+mp.getName()+"]";
					listFileLogInLogLayoutComboBox.addItem(baseName);
					map_nameFileAndCompletePathFile.put(baseName, mp.getLogFile());
				}
			}
			
			GeneralWorkflowTask gR = find.getWorkflowTask(Session.REMOVEPROCESS);
			if (gR != null) {
				RemoveProcessTask pR = (RemoveProcessTask)gR;
				Vector<MetaProcess> mlR = pR.getMetaProcess(null);
				for (MetaProcess mp : mlR) {
					baseName = "Processing activity report ["+mp.getBlock()+":"+mp.getName()+"]";
					listFileLogInLogLayoutComboBox.addItem(baseName);
					map_nameFileAndCompletePathFile.put(baseName, mp.getLogFile());
				}
			}

			String dir = BiomajInformation.getInstance().getProperty(BiomajInformation.LOGDIR)+"/"+listeBankNameInBankLayoutComboBox.getSelectedItem()+"/runtime/";

			String nameFile = currentBank.getConfig().getName()+".remote.filelist";
			String label    = "Complete remote files list (match with remote.files property)";
			if (new File(dir+nameFile).exists()) {
				listFileLogInLogLayoutComboBox.addItem(label);
				map_nameFileAndCompletePathFile.put(label, dir+nameFile);
			}


			nameFile = currentBank.getConfig().getName()+".diff.filelist";
			label    = "Downloadable remote files list";
			if (new File(dir+nameFile).exists()) {
				listFileLogInLogLayoutComboBox.addItem(label);
				map_nameFileAndCompletePathFile.put(label, dir+nameFile);
			}


			nameFile = currentBank.getConfig().getName()+".copy.filelist";
			label    = "Production files already downloaded"; 
			if (new File(dir+nameFile).exists()) {
				listFileLogInLogLayoutComboBox.addItem(label);
				map_nameFileAndCompletePathFile.put(label, dir+nameFile);
			}

			nameFile = currentBank.getConfig().getName()+".keep.offline.filelist";
			label    = "Non production files already downloaded"; 
			if (new File(dir+nameFile).exists()) {
				listFileLogInLogLayoutComboBox.addItem(label);
				map_nameFileAndCompletePathFile.put(label, dir+nameFile);
			}

			nameFile = currentBank.getConfig().getName()+".extract.filelist";
			label    = "Extractable files list"; 
			if (new File(dir+nameFile).exists()) {
				listFileLogInLogLayoutComboBox.addItem(label);
				map_nameFileAndCompletePathFile.put(label, dir+nameFile);
			}

			/*
			nameFile = currentBank.getConfig().getName()+".release";
			label    = "Release information (1)";
			if (new File(dir+nameFile).exists()) {
				listFileLogInLogLayoutComboBox.addItem(label);
				map_nameFileAndCompletePathFile.put(label, dir+nameFile);
			}
			 */

			nameFile = currentBank.getConfig().getName()+".update.properties";
			label    = "Release information";
			if (new File(dir+nameFile).exists()) {
				listFileLogInLogLayoutComboBox.addItem(label);
				map_nameFileAndCompletePathFile.put(label, dir+nameFile);
			}

			int i = 1 ;
			nameFile = currentBank.getConfig().getName()+".wget.log"+Integer.toString(i++);
			while (new File(dir+nameFile).exists()) {
				label = "Wget output ("+Integer.toString(i)+")";
				listFileLogInLogLayoutComboBox.addItem(nameFile);
				map_nameFileAndCompletePathFile.put(nameFile, dir+nameFile);
				nameFile = currentBank.getConfig().getName()+".wget.log"+Integer.toString(i++);
			}

			//On doit rafraichir si des fichiers apparaissent...
			/*
			if (timerFileLog == null) {
				timerFileLog = new Timer(REFRESH_LIST,new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						listSessionsInSessionLayoutActionPerformed(e);
					}});

				timerFileLog.start();
			}
			 */
			//	listFileLogInLogLayoutActionPerformed(null);

		} catch (Exception pe) {
			BiomajLogger.getInstance().log(pe);
			//System.out.println(pe.getLocalizedMessage());
			return ;
		}

	}


	private int getLevelSelected() {
		if (debugRadioButton.isSelected())
			return Project.MSG_DEBUG;
		if (verboseRadioButton.isSelected())
			return Project.MSG_VERBOSE;
		if (infoRadioButton.isSelected())
			return Project.MSG_INFO;
		if (warningRadioButton.isSelected())
			return Project.MSG_WARN;
		if (errorRadioButton.isSelected())
			return Project.MSG_ERR;

		return Project.MSG_INFO;
	}
	/*
	private String getLevelStyle() {
		if (debugRadioButton.isSelected())
			return "debug";
		if (verboseRadioButton.isSelected())
			return "verbose";
		if (infoRadioButton.isSelected())
			return "info";
		if (warningRadioButton.isSelected())
			return "warning";
		if (errorRadioButton.isSelected())
			return "erreur";

		return "info";
	}
	 */
	public static String getLevel(int l) {
		if (l == Project.MSG_DEBUG)
			return "debug";
		if (l == Project.MSG_VERBOSE)
			return "verbose";
		if (l == Project.MSG_INFO)
			return "info";
		if (l == Project.MSG_WARN)
			return "warning";
		if (l == Project.MSG_ERR)
			return "erreur";

		return "info";
	}

	private void handleScrollLock() {
		if (pbm!=null) {
			pbm.setActiveScrollLock(checkBox1.isSelected());
		}
	}

	private String currentFileLog = "";
	private long dateCurrentFileLog = 0;
	private int nbLine = 0 ;
	private Timer timerCurrentfile = null ;
	private int oldLevelSelected = -1 ;

	private PrintLogInBioMAJLogger pbm = null ;

	private void listFileLogInLogLayoutActionPerformed(ActionEvent e) {
		if (listFileLogInLogLayoutComboBox.getItemCount()<=0)
			return ;
		
		String fileLog = (String) listFileLogInLogLayoutComboBox.getSelectedItem() ;

		if (fileLog == null)
			return ;

		int levelSelected = getLevelSelected();
		File f = null ;
		
		if (map_nameFileAndCompletePathFile.get(fileLog) != null)
			f = new File(map_nameFileAndCompletePathFile.get(fileLog)) ;
		else
			return ;
		
		boolean isTheCurrentFile = ((currentFileLog.compareTo(f.getAbsolutePath()) == 0)&&(oldLevelSelected == levelSelected));

		//Si un nouveau fichier doit etre charge
		if (!isTheCurrentFile) {
			/**
			 * FICHIEr DIFFERENT OU LEVEL DIFF
			 */

			if (timerCurrentfile != null) {
				timerCurrentfile.stop();
				timerCurrentfile = null ;
			}

			currentFileLog = f.getAbsolutePath() ;
			//currentNameIndexLog = fileLog ;
			oldLevelSelected = levelSelected;
			nbLine = 0;
			dateCurrentFileLog = f.lastModified() ;

			if ((pbm != null) && (pbm.isAlive())) {
				pbm.setStopTraitement();
				while (pbm.isAlive()) {} ;
			}

		} else {
			/**
			 * MEME FICHIER - MEME LEVEL
			 */

			if (dateCurrentFileLog == f.lastModified()) {
				return ;
			}


			if (pbm.isAlive()) {
				//On attend que la premiere passe soit passe avant de reecrire quelquechose
				return ;
			}

			if (timerCurrentfile != null) 
				timerCurrentfile.stop();

			nbLine = pbm.getNbLineResult();
		}

		pbm = new PrintLogInBioMAJLogger(f.getAbsolutePath(),fileLogTextArea,sdoc,nbLine,levelSelected);
		handleScrollLock();
		pbm.start();



		timerCurrentfile = new Timer(REFRESH_SCREEN,new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listFileLogInLogLayoutActionPerformed(e);
			}});
		timerCurrentfile.start();

	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license - Olivier Filangi
		panel1 = new JPanel();
		label13 = new JLabel();
		listTypeInBankLayoutComboBox = new JComboBox();
		updatingRadioButton = new JRadioButton();
		errorCycleRadioButton = new JRadioButton();
		radioButtonAll = new JRadioButton();
		label1 = new JLabel();
		listeBankNameInBankLayoutComboBox = new JComboBox();
		panel2 = new JPanel();
		label6 = new JLabel();
		descriptionField = new JTextField();
		descriptionField.setEditable(false);
		label4 = new JLabel();
		urlField = new JTextField();
		urlField.setEditable(false);
		label7 = new JLabel();
		remoteFilesField = new JTextField();
		remoteFilesField.setEditable(false);
		label8 = new JLabel();
		remoteFilesExcludedField = new JTextField();
		remoteFilesExcludedField.setEditable(false);
		label9 = new JLabel();
		versionDirectoryField = new JTextField();
		versionDirectoryField.setEditable(false);
		label10 = new JLabel();
		offlineDirectoryField = new JTextField();
		offlineDirectoryField.setEditable(false);
		label11 = new JLabel();
		listVersionInSessionLayoutComboBox = new JComboBox();
		panel3 = new JPanel();
		label12 = new JLabel();
		checkBox1 = new JCheckBox();
		refreshButton = new JButton();

		listFileLogInLogLayoutComboBox = new JComboBox();
		debugRadioButton = new JRadioButton();
		debugRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listFileLogInLogLayoutActionPerformed(e);
			}
		});
		verboseRadioButton = new JRadioButton();
		verboseRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listFileLogInLogLayoutActionPerformed(e);
			}
		});

		infoRadioButton = new JRadioButton();
		infoRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listFileLogInLogLayoutActionPerformed(e);
			}
		});

		warningRadioButton = new JRadioButton();
		warningRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listFileLogInLogLayoutActionPerformed(e);
			}
		});

		errorRadioButton = new JRadioButton();
		errorRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listFileLogInLogLayoutActionPerformed(e);
			}
		});

		panel4 = new JPanel();

		scrollPane1 = new JScrollPane();
		fileLogTextArea = new JTextPane();
		fileLogTextArea.setEditable(false);
		CellConstraints cc = new CellConstraints();

		//======== this ========

		// JFormDesigner evaluation mark
		setBorder(new javax.swing.border.CompoundBorder(
				new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
						"", javax.swing.border.TitledBorder.CENTER,
						javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
						java.awt.Color.red), getBorder())); addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

						setLayout(new FormLayout(
								ColumnSpec.decodeSpecs("default:grow"),
								new RowSpec[] {
									FormFactory.DEFAULT_ROWSPEC,
									FormFactory.LINE_GAP_ROWSPEC,
									FormFactory.DEFAULT_ROWSPEC,
									FormFactory.LINE_GAP_ROWSPEC,
									FormFactory.DEFAULT_ROWSPEC,
									FormFactory.LINE_GAP_ROWSPEC,
									new RowSpec(RowSpec.CENTER, Sizes.dluY(171), FormSpec.DEFAULT_GROW)
								}));


						//======== panel1 ========
						{
							panel1.setBorder(new CompoundBorder(
									new TitledBorder("Bank"),
									Borders.DLU2_BORDER));
							panel1.setLayout(new FormLayout(
									new ColumnSpec[] {
											new ColumnSpec(Sizes.dluX(64)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(203)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(19)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(44)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(75)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(57)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(87))
									},
									new RowSpec[] {
											FormFactory.DEFAULT_ROWSPEC,
											FormFactory.LINE_GAP_ROWSPEC,
											FormFactory.DEFAULT_ROWSPEC,
											FormFactory.LINE_GAP_ROWSPEC,
											FormFactory.DEFAULT_ROWSPEC
									}));

							//---- label13 ----
							label13.setText("Type");
							panel1.add(label13, cc.xy(1, 1));

							//---- listTypeInBankLayoutComboBox ----
							listTypeInBankLayoutComboBox.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									initListBankLayoutActionPerformed(e);
								}
							});
							panel1.add(listTypeInBankLayoutComboBox, cc.xy(3, 1));

							//---- updatingRadioButton ----
							updatingRadioButton.setText("Updating");
							updatingRadioButton.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									radioButtonBankLayoutActionPerformed(e);
								}
							});
							panel1.add(updatingRadioButton, cc.xy(7, 1));

							//---- onlineRadioButton ----
							errorCycleRadioButton.setText("ErrorOnLastCycle");
							errorCycleRadioButton.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									radioButtonBankLayoutActionPerformed(e);
								}
							});
							panel1.add(errorCycleRadioButton, cc.xy(9, 1));

							//---- radioButton1 ----
							radioButtonAll.setText("All");
							radioButtonAll.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									radioButtonBankLayoutActionPerformed(e);
								}
							});
							panel1.add(radioButtonAll, cc.xy(11, 1));

							//---- label1 ----
							label1.setText("Bank Name");

							panel1.add(label1, cc.xy(1, 3));

							//---- listeBankNameInBankLayoutComboBox ----
							listeBankNameInBankLayoutComboBox.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									initInformationSessionAndBankActionPerformed(e);
								}
							});
							panel1.add(listeBankNameInBankLayoutComboBox, cc.xy(3, 3));

							//---- refreshButton ----
							refreshButton.setText("refresh");
							refreshButton.setBackground(Color.lightGray);
							refreshButton.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									refreshAll(e);
								}
							});
							panel1.add(refreshButton, cc.xy(9, 5));

//							*********************************************************************************************
							/*	panel1.setBorder(new CompoundBorder(
									new TitledBorder("Bank"),
									Borders.DLU2_BORDER));
							panel1.setLayout(new FormLayout(
									new ColumnSpec[] {
											new ColumnSpec(Sizes.dluX(64)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(203)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(19)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(44)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(40)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(20)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(87))
									},
									new RowSpec[] {
											FormFactory.DEFAULT_ROWSPEC,
											FormFactory.LINE_GAP_ROWSPEC,
											FormFactory.DEFAULT_ROWSPEC
									}));

							//---- label13 ----
							label13.setText("Type");
							panel1.add(label13, cc.xy(1, 1));

							//---- listTypeInBankLayoutComboBox ----
							listTypeInBankLayoutComboBox.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									initListBankLayoutActionPerformed(e);
								}
							});
							panel1.add(listTypeInBankLayoutComboBox, cc.xy(3, 1));

							//---- updatingRadioButton ----
							updatingRadioButton.setText("Updating");
							updatingRadioButton.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									radioButtonUpdatingInBankLayoutActionPerformed(e);
								}
							});
							panel1.add(updatingRadioButton, cc.xy(7, 1));

							//---- onlineRadioButton ----
							onlineRadioButton.setText("Online");
							onlineRadioButton.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									radioButtonOnlineInBankLayoutActionPerformed(e);

								}
							});
							panel1.add(onlineRadioButton, cc.xy(9, 1));

							//---- refreshButton ----
							refreshButton.setText("refresh");
							refreshButton.setBackground(Color.lightGray);
							refreshButton.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									refreshAll(e);
								}
							});


							panel1.add(refreshButton, cc.xy(13, 1));

							//---- label1 ----
							label1.setText("Bank Name");
							panel1.add(label1, cc.xy(1, 3));

							//---- listeBankNameInBankLayoutComboBox ----
							listeBankNameInBankLayoutComboBox.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									initInformationSessionAndBankActionPerformed(e);
								}
							});
							panel1.add(listeBankNameInBankLayoutComboBox, cc.xy(3, 3));

							//---- label2 ----
							label2.setText("filter");
							panel1.add(label2, cc.xy(7, 3));
							panel1.add(filterBankName, cc.xywh(9, 3, 5, 1));
							 */
						}
						add(panel1, cc.xy(1, 1));

						//======== panel2 ========
						{
							panel2.setBorder(new CompoundBorder(
									new TitledBorder("Session"),
									new EmptyBorder(5, 5, 5, 5)));
							panel2.setLayout(new FormLayout(
									new ColumnSpec[] {
											new ColumnSpec(Sizes.dluX(64)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(198)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											FormFactory.DEFAULT_COLSPEC,
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(195))
									},
									RowSpec.decodeSpecs("default, default, default, default, default")));

							//---- label6 ----
							label6.setText("Description");
							panel2.add(label6, cc.xy(1, 1));
							panel2.add(descriptionField, cc.xy(3, 1));

							//---- label4 ----
							label4.setText("Url");
							panel2.add(label4, cc.xy(1, 2));

							//---- urlField ----
							urlField.setText("default");
							panel2.add(urlField, cc.xy(3, 2));

							//---- label7 ----
							label7.setText("Remote files");
							panel2.add(label7, cc.xy(1, 3));
							panel2.add(remoteFilesField, cc.xy(3, 3));

							//---- label8 ----
							label8.setText("Remote files excluded");
							panel2.add(label8, cc.xy(5, 3));
							panel2.add(remoteFilesExcludedField, cc.xy(7, 3));

							//---- label9 ----
							label9.setText("Version directory");
							panel2.add(label9, cc.xy(1, 4));
							panel2.add(versionDirectoryField, cc.xy(3, 4));

							//---- label10 ----
							label10.setText("Offline directory");
							panel2.add(label10, cc.xy(5, 4));
							panel2.add(offlineDirectoryField, cc.xy(7, 4));

							//---- label11 ----
							label11.setText("Session");
							panel2.add(label11, cc.xy(1, 5));

							//---- listVersionInSessionLayoutComboBox ----
							listVersionInSessionLayoutComboBox.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									listSessionsInSessionLayoutActionPerformed(e);
								}
							});
							panel2.add(listVersionInSessionLayoutComboBox, cc.xy(3, 5));
						}
						add(panel2, cc.xy(1, 3));

						//======== panel3 ========
						{
							panel3.setBorder(new CompoundBorder(
									new TitledBorder("Log"),
									Borders.DLU2_BORDER));
							panel3.setLayout(new FormLayout(
									new ColumnSpec[] {
											new ColumnSpec(Sizes.dluX(66)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(197)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(50)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(34)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(44)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(30)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(42)),
											FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
											new ColumnSpec(Sizes.dluX(43))
									},
									RowSpec.decodeSpecs("default")));

							//---- label12 ----
							label12.setText("file");
							panel3.add(label12, cc.xy(1, 1));

							//---- listFileLogInLogLayoutComboBox ----
							listFileLogInLogLayoutComboBox.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									listFileLogInLogLayoutActionPerformed(e);
								}
							});
							panel3.add(listFileLogInLogLayoutComboBox, cc.xywh(3, 1, 1, 1, CellConstraints.FILL, CellConstraints.DEFAULT));

							//---- checkBox1 ----
							checkBox1.setText("scroll lock");
							checkBox1.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									handleScrollLock();
								}
							});

							panel3.add(checkBox1, cc.xy(5, 1));

							//---- debugRadioButton ----
							debugRadioButton.setText("DEBUG");
							panel3.add(debugRadioButton, cc.xy(7, 1));

							//---- verboseRadioButton ----
							verboseRadioButton.setText("VERBOSE");
							panel3.add(verboseRadioButton, cc.xy(9, 1));

							//---- infoRadioButton ----
							infoRadioButton.setText("INFO");
							panel3.add(infoRadioButton, cc.xy(11, 1));

							//---- warningRadioButton ----
							warningRadioButton.setText("WARNING");
							panel3.add(warningRadioButton, cc.xy(13, 1));

							//---- errorRadioButton ----
							errorRadioButton.setText("ERROR");
							panel3.add(errorRadioButton, cc.xy(15, 1));
						}
						add(panel3, cc.xy(1, 5));

						//======== panel4 ========
						{
							panel4.setBorder(new TitledBorder("Log"));
							panel4.setLayout(new FormLayout(
									"pref:grow",
							"fill:185dlu:grow"));

							//======== scrollPane1 ========
							{
								scrollPane1.setViewportView(fileLogTextArea);
							}
							panel4.add(scrollPane1, cc.xywh(1, 1, 1, 1, CellConstraints.FILL, CellConstraints.FILL));
						}
						add(panel4, cc.xywh(1, 7, 1, 1, CellConstraints.FILL, CellConstraints.FILL));
						//---- buttonGroup2 ----
						ButtonGroup buttonGroup2 = new ButtonGroup();
						buttonGroup2.add(updatingRadioButton);
						buttonGroup2.add(errorCycleRadioButton);
						buttonGroup2.add(radioButtonAll);

						//---- buttonGroup1 ----
						ButtonGroup buttonGroup1 = new ButtonGroup();
						buttonGroup1.add(debugRadioButton);
						buttonGroup1.add(verboseRadioButton);
						buttonGroup1.add(infoRadioButton);
						buttonGroup1.add(warningRadioButton);
						buttonGroup1.add(errorRadioButton);
						// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license - Olivier Filangi
	private JPanel panel1;
	private JLabel label13;
	private JButton refreshButton;
	private JComboBox listTypeInBankLayoutComboBox;
	private JRadioButton updatingRadioButton;
	private JRadioButton errorCycleRadioButton;
	private JRadioButton radioButtonAll;
	private JLabel label1;
	private JComboBox listeBankNameInBankLayoutComboBox;
	private JPanel panel2;
	private JLabel label6;
	private JTextField descriptionField;
	private JLabel label4;
	private JTextField urlField;
	private JLabel label7;
	private JTextField remoteFilesField;
	private JLabel label8;
	private JTextField remoteFilesExcludedField;
	private JLabel label9;
	private JTextField versionDirectoryField;
	private JLabel label10;
	private JTextField offlineDirectoryField;
	private JLabel label11;
	private JComboBox listVersionInSessionLayoutComboBox;
	private JPanel panel3;
	private JLabel label12;
	private JComboBox listFileLogInLogLayoutComboBox;
	private JRadioButton debugRadioButton;
	private JRadioButton verboseRadioButton;
	private JRadioButton infoRadioButton;
	private JRadioButton warningRadioButton;
	private JRadioButton errorRadioButton;
	private JScrollPane scrollPane1;
	private JTextPane fileLogTextArea;
	private JPanel panel4;
	private JCheckBox checkBox1;

	// JFormDesigner - End of variables declaration  //GEN-END:variables

	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}



	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}



	public void windowClosing(WindowEvent e) {
		fenetre.dispose();
		this.removeAll();

	}



	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}



	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}



	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}



	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	private static JFrame fenetre = null ;

	public void execute() {

		fenetre = new JFrame("View logs-BioMaj-"+Biomaj.VERSION);
		fenetre.setSize(400,400);
		fenetre.setVisible(true);

		ImageIcon icon = new ImageIcon(BiomajUtils.getBiomajRootDirectory()+"/xslt/images/icon_cycle_biomaj.png","BioMAJ - cycle update");
		fenetre.setIconImage(icon.getImage());

		Dimension screenSize=Toolkit.getDefaultToolkit().getScreenSize();
		Dimension ScreenSize=new Dimension((screenSize.width/2),(screenSize.height/2));
		Dimension frameSize=new Dimension(1200,800);
		int x=(ScreenSize.width/2);
		int y=(ScreenSize.height/2);
		fenetre.setBounds(x,y,frameSize.width,frameSize.height);


		fenetre.getContentPane().setLayout(new BorderLayout());
		fenetre.getContentPane().add(this,BorderLayout.CENTER);

		//fenetre.getContentPane().add(new JScrollPane(debug_text),BorderLayout.CENTER);
		fenetre.setVisible(true);

		fenetre.addWindowListener(this);
	}

	//private TreeMap<String,Vector<String>> listIndexTypeWithBanks = null ;

	public Vector<String> getListType() {
		Vector<String> res = new Vector<String>();
		res.add("all");
		try {
			TreeMap<String,Vector<String>> listIndexTypeWithBanks = BiomajUtils.getListBankFindInStatefile();
			for (String k : listIndexTypeWithBanks.keySet()) {
				res.add(k);
			}

		} catch (BiomajException e) {
			System.err.println(e.getMessage());
			return res;
		}


		return res;
	}
	
	public Vector<String> getListTypeSql() {
		Vector<String> res = new Vector<String>();
		res.add("all");
		List<String> types = BiomajSQLQuerier.getBankTypes();
		if (types != null)
			res.addAll(types);
		
		return res;
	}
	
	public Vector<String> getListBanks(String type) {
		Vector<String> res = new Vector<String>();
		try {
			TreeMap<String,Vector<String>> listIndexTypeWithBanks = BiomajUtils.getListBankFindInStatefile();
			for (String k : listIndexTypeWithBanks.keySet()) {
				if ( ( type == null || "all".compareTo(type)==0) || (type.compareTo(k)==0) )
					res.addAll(listIndexTypeWithBanks.get(k));
			}

		} catch (BiomajException e) {
			System.err.println(e.getMessage());
			return res;
		}
		
		Collections.sort(res);
		
		return res;
	}
	
	
	public Vector<String> getListBankSql(String type) {
		List<String> banks = BiomajSQLQuerier.getBanks();
		Vector<String> res = new Vector<String>();
		for (String bank : banks) {
			Map<String, String> info = BiomajSQLQuerier.getBankInfo(bank);
			if (type == null || info.get(BiomajSQLQuerier.DB_TYPE).equals(type) || type.equals("all"))
				res.add(bank);
		}
		
		return res;
	}
	
	
}
