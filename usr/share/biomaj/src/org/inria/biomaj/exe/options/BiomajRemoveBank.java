/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.options;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Vector;

import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.logger.DBWriter;
import org.inria.biomaj.ant.task.BmajVersionManagement;
import org.inria.biomaj.exe.bank.BankFactory;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.exe.workflow.WorkflowEngine;
import org.inria.biomaj.exe.workflow.WorkflowEngineFactory;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.ProductionDirectory;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * <p>Remove a bank from biomaj database.</p>
 * 
 * 
 * @author ofilangi
 * @version Biomaj 0.9.1.2
 * @since Biomaj 0.8 /Citrina 0.5
 */
public class BiomajRemoveBank {

	/**
	 * Keep production directory. default is false
	 * @uml.property  name="keepProductionDirectory"
	 */
	private boolean keepProductionDirectory = false;
	
	/**
	 * Show console. default is false
	 * @uml.property  name="showConsole"
	 */
	private boolean showConsole = false;
	
	/**
	 * Console mode. Default is MSG_INFO
	 * @uml.property  name="consoleMode"
	 */
	private int consoleMode = Project.MSG_INFO;

	/**
	 * Name bank to deleted
	 * @uml.property  name="bankName"
	 */
	private String bankName = "";

	public BiomajRemoveBank() {

	}

	public boolean isKeepProductionDirectory() {
		return keepProductionDirectory;
	}

	/**
	 * @param keepProductionDirectory  the keepProductionDirectory to set
	 * @uml.property  name="keepProductionDirectory"
	 */
	public void setKeepProductionDirectory(boolean keepProductionDirectory) {
		this.keepProductionDirectory = keepProductionDirectory;
	}
	
	public boolean isShowConsole() {
		return this.showConsole;
	}
	
	public int getConsoleMode() {
		return this.consoleMode;
	}

	/**
	 * @param showConsole  the showConsole to set
	 * @uml.property  name="showConsole"
	 */
	public void setShowConsole(boolean showConsole, int mode) {
		this.showConsole = showConsole;
		this.consoleMode = mode;
	}

	/**
	 * @return  the bankName
	 * @uml.property  name="bankName"
	 */
	public String getBankName() {
		return bankName;
	}

	/**
	 * @param bankName  the bankName to set
	 * @uml.property  name="bankName"
	 */
	public void setBankName(String nameBank) {
		this.bankName = nameBank;
	}


	public void execute () {

		if (bankName.compareTo("") == 0) {
			BiomajLogger.getInstance().log("dbname is empty. can't remove bank!");
			return;
		}

		/** Verification si le fichier properties exist */
		try {
			BankFactory bf = new BankFactory();
			
			BiomajBank bProperties = bf.createBank(getBankName(),false);

			
			Vector<ProductionDirectory> lpd = new Vector<ProductionDirectory>();
			lpd.addAll(BiomajSQLQuerier.getAllProductionDirectories(getBankName()));

			Vector<ProductionDirectory> available = new Vector<ProductionDirectory>();
			System.out.println("delete mode");
			int i=0;
			for (ProductionDirectory pd : lpd) {
				if (pd.getState()==ProductionDirectory.AVAILABLE) {
					available.add(pd);
					System.out.println("["+Integer.toString(i++)+"]:"+pd.getPath()+" ("+BiomajUtils.dateToString(pd.getCreationDate(), Locale.US)+")");
				}
			}
			System.out.println("["+Integer.toString(i)+"]:all");

			try {
				BufferedReader entreeClavier = new BufferedReader(new InputStreamReader(System.in));
				String saisie = entreeClavier.readLine();
				int code = -1;
				try {
					code = new Integer(saisie).intValue();
				}
				catch(NumberFormatException e) {
					return;
				}
				
				Vector<String> versions = new Vector<String>();
				if (code == i) // remove all
				{
					if (confirmeDeletion()) {
						removeAllProductionDirectory(bProperties);

						for (ProductionDirectory pd : available) {
							versions.add(BmajVersionManagement.getRelease(pd.getPath()));
						}
						runRemoveProcess(bProperties, versions);// Launch remove process workflow
					}
				} else if ((code>=0)&&(code<=i)) { // remove given version
					if (confirmeDeletion()) {
						removeProductionDirectory(bProperties.getPropertiesFromBankFile(),available.get(code),lpd);
						versions.add(BmajVersionManagement.getRelease(available.get(code).getPath()));
						runRemoveProcess(bProperties, versions);// Launch remove process workflow
					}
				}
			} catch (IOException ioe) {
				BiomajLogger.getInstance().log(ioe);
				return;
			}

		} catch (BiomajException be) {
			return;
		}

	}
	
	public WorkflowEngine executeWithoutUserInteraction(String[] dirs, boolean deleteAll, boolean keepHistory) {
		List<String> _dirs = new ArrayList<String>();
		if (dirs != null) {
			for (String s : dirs) {
				_dirs.add(s);
			}
		}
		
//		Verification si le fichier properties existe
		try {
			BankFactory bf = new BankFactory();
			BiomajBank bProperties = bf.createBank(getBankName(),false);
			
			File f = new File(BiomajInformation.getInstance().getProperty(BiomajInformation.TMPDIR) + "/" + getBankName() + WorkflowEngine.extensionLock);
			if (!f.exists()) {
				// If lock file does not exist, create it during database access.
				try {
					f.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}

				Vector<ProductionDirectory> lpd = new Vector<ProductionDirectory>();
				lpd.addAll(BiomajSQLQuerier.getAllProductionDirectories(getBankName()));
				
				if (deleteAll) {
					List<ProductionDirectory> tmp = BiomajSQLQuerier.getAvailableProductionDirectories(getBankName());
					_dirs = new ArrayList<String>();
					for (ProductionDirectory pd : tmp)
						_dirs.add(BmajVersionManagement.getRelease(pd.getPath()));
				}
				
				Vector<String> versions = new Vector<String>();
				if (deleteAll) { // remove all
					removeAllProductionDirectory(bProperties);
					versions.addAll(_dirs);
					
					
	//				for (ProductionDirectory pd : dirs) {
	//					versions.add(BmajVersionManagement.getRelease(pd.getPath()));
	//				}
				} else {
					
					for (String dir : _dirs) {
						ProductionDirectory pd = new ProductionDirectory();
						pd.setPath(dir);
						pd.setState(ProductionDirectory.AVAILABLE);
						removeProductionDirectory(bProperties.getPropertiesFromBankFile(), lpd.get(lpd.indexOf(pd)), lpd);
						versions.add(BmajVersionManagement.getRelease(dir));
					}
				}
				
				// Remove lock file as it will be created during the workflow execution
				f.delete();
				
				WorkflowEngine we = runRemoveProcess(bProperties, versions);// Launch remove process workflow
				
				if (keepHistory == false) {
					try {
						we.join();
					} catch (InterruptedException e) {
						e.printStackTrace();
					} // Wait for end of process
					BiomajSQLQuerier.deleteAllFromBank(getBankName());
				}
				
				return we;
			} else {
				System.err.println("A BioMAJ session seems to be already running for " + getBankName() + ". If that's not the case " +
						"remove " + f.getAbsolutePath());
				return null;
			}
		} catch (BiomajException be) {
			return null;
		}
	}
	
	/**
	 * Launches the remove process workflow
	 * @param bank A BiomajBank object 
	 * @param version
	 */
	public WorkflowEngine runRemoveProcess(BiomajBank bank, Vector<String> version) {
		
		try {
			WorkflowEngineFactory wef = new WorkflowEngineFactory();
			WorkflowEngine we ;
			we = wef.createWorkflow(bank);
			
			we.setModeConsole(isShowConsole(), getConsoleMode());
			we.setVersionsToRemove(version);

			we.setCommand(WorkflowEngine.TARGET_REMOVEPROCESS);
			we.start();
			
			return we;
			
		} catch (BiomajException e) {
			System.err.println("Biomaj has detected an error! for bank ["+ bank.getDbName()+"]");
			System.err.println(e.getLocalizedMessage());
			
		} catch (BiomajBuildException bbe) {
			System.err
			.println("Biomaj has detected an error during the execution of workflow for the bank :"
					+ bank.getDbName());
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
		}
		
		return null;
	}

	private boolean confirmeDeletion() {
		System.out.println("Press Yes to confirme the deletion (y/n)");
		try {
			BufferedReader entreeClavier = new BufferedReader(new InputStreamReader(System.in));
			String response = entreeClavier.readLine();
			
			//	System.out.println("commande:"+response+"$");

			if ((response.compareTo("yes")==0)||(response.compareTo("y")==0)||(response.compareTo("Y")==0))
				return true;
		
		} catch (IOException ioe) {
			BiomajLogger.getInstance().log(ioe);
			return false;
		}

		return false;
	}

	/**
	 * Delete a production directory and change statefile
	 * @param props
	 * @param lpd
	 * @param index
	 */
	private void removeProductionDirectory(Properties props,ProductionDirectory toerase, Vector<ProductionDirectory> all) {

		File current = new File(props.getProperty(BiomajConst.dataDirProperty)+"/"+props.getProperty(BiomajConst.versionDirProperty)+"/"+BiomajConst.currentLink);
		File prodDir = new File(toerase.getPath());
		boolean changeCurrent = false ;
		try {
			if (current.exists()&&(prodDir.getAbsolutePath().compareTo(current.getCanonicalPath())==0)) {
				if (!current.delete()) {
					BiomajLogger.getInstance().log("Can't delete path:"+current.getAbsolutePath());
					return;
				}
				changeCurrent = true;
				System.out.println("delete:"+current.getAbsolutePath());
			}
		} catch (IOException ioe) {
			BiomajLogger.getInstance().log(ioe);
			return;
		}

		if (!isKeepProductionDirectory()) {
			System.out.println("delete:"+prodDir.getAbsolutePath());
			BiomajUtils.deleteAll(prodDir);
		}

		toerase.setRemoveDate(new Date());
		toerase.setState(ProductionDirectory.REMOVE);
		try {
			DBWriter.setProductionDirRemoved(props.getProperty(BiomajConst.dbNameProperty), toerase);
		} catch (BiomajException be) {
			BiomajLogger.getInstance().log(be);
		}

		if (changeCurrent)
			changeCurrentLinkProductionDirectory(props,all);
	}


	private void removeAllProductionDirectory(BiomajBank b) throws BiomajException {

		String dataDir = (String) b.getPropertiesFromBankFile().get(BiomajConst.dataDirProperty);
		
		/*
		 * Bank production
		 */
		if (!isKeepProductionDirectory()) {
			String versionDir = (String) b.getPropertiesFromBankFile().get(BiomajConst.versionDirProperty);

			File dirToDel = new File (dataDir + "/" + versionDir);
			System.out.println("delete directory:" + dirToDel.getAbsolutePath());
			BiomajUtils.deleteAll(dirToDel);

		}
		
		/*
		 * Tmp dir
		 */
		File tmpDir = new File(dataDir + "/" + b.getPropertiesFromBankFile().get(BiomajConst.offlineDirProperty));
		if (tmpDir.exists())
			BiomajUtils.deleteAll(tmpDir);
		
		/*
		 * log
		 */
		File f = new File(BiomajInformation.getInstance().getProperty(BiomajInformation.LOGDIR) + "/" + getBankName());


		if (f.exists()) {
			System.out.println("delete directory:"+f.getAbsolutePath());
			BiomajUtils.deleteAll(f);
		}


		/*
		 * lock
		 */
		f = new File(BiomajUtils.getBiomajRootDirectory() + "/"
				+ BiomajConst.tmpLockDirectory + "/" + getBankName()+WorkflowEngine.extensionLock);

		if (f.exists()) {
			System.out.println("delete directory:"+f.getAbsolutePath());
			f.delete();
		}
		
		/*
		 * Removal of the bank in the database
		 */
		DBWriter.deleteBank(bankName);


	}

	/**
	 * if current link not exist, find the last production directory to apply a new link
	 * @param props
	 * @param lpd
	 * @param index
	 */

	private void changeCurrentLinkProductionDirectory(Properties props,Vector<ProductionDirectory> lpd) {

		File current = new File(props.getProperty(BiomajConst.dataDirProperty)+"/"+props.getProperty(BiomajConst.versionDirProperty)+"/"+BiomajConst.currentLink);

		if (current.exists()) {
			System.out.println("no needs to create a current link.");
			return;
		}

		ProductionDirectory candidat = null ;
		for (ProductionDirectory dir : lpd) {
			if (dir.getState()!=ProductionDirectory.AVAILABLE)
				continue;
			if ((candidat == null)&&(dir.getRemoveDate() == null))
				candidat = dir;
			else if ((candidat != null)&&(dir.getRemoveDate() == null)&&(dir.getCreationDate().getTime()>candidat.getCreationDate().getTime()))
				candidat = dir ;
		}

		try {
			if (candidat != null ) {
				File path = new File(candidat.getPath());
				BiomajUtils.createLinkOnFileSystem(path, BiomajConst.currentLink);
			}
		} catch (Exception e) {
			BiomajLogger.getInstance().log(e);
			return;
		} 
	}
}
