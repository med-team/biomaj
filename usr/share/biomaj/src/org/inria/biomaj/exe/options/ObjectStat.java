/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.exe.options;

import java.util.Date;

public class ObjectStat {

	/**
	 * @uml.property  name="release"
	 */
	private String release="";
	/**
	 * @uml.property  name="size"
	 */
	private long size =0;
	/**
	 * @uml.property  name="sizeCompressed"
	 */
	private long sizeCompressed=0;
	/**
	 * @uml.property  name="bandWidth"
	 */
	private double bandWidth=0.0;
	/**
	 * @uml.property  name="globalTime"
	 */
	private long globalTime=0;
	
	/**
	 * @uml.property  name="nbSession"
	 */
	private int nbSession=0;
	/**
	 * @uml.property  name="trueTime"
	 */
	private long trueTime=0; // aggregation du temps des sessions
	/**
	 * @uml.property  name="nbFileDownload"
	 */
	private int nbFileDownload=0;
	/**
	 * @uml.property  name="nbFileFlat"
	 */
	private int nbFileFlat=0;
	
	/**
	 * @uml.property  name="time"
	 */
	private Date time = new Date();
	
	/**
	 * @return  the bandWidth
	 * @uml.property  name="bandWidth"
	 */
	public double getBandWidth() {
		return bandWidth;
	}
	/**
	 * @param bandWidth  the bandWidth to set
	 * @uml.property  name="bandWidth"
	 */
	public void setBandWidth(double bandWidth) {
		this.bandWidth = bandWidth;
	}
	/**
	 * @return  the globalTime
	 * @uml.property  name="globalTime"
	 */
	public long getGlobalTime() {
		return globalTime;
	}
	/**
	 * @param globalTime  the globalTime to set
	 * @uml.property  name="globalTime"
	 */
	public void setGlobalTime(long globalTime) {
		this.globalTime = globalTime;
	}
	/**
	 * @return  the nbFileDownload
	 * @uml.property  name="nbFileDownload"
	 */
	public int getNbFileDownload() {
		return nbFileDownload;
	}
	/**
	 * @param nbFileDownload  the nbFileDownload to set
	 * @uml.property  name="nbFileDownload"
	 */
	public void setNbFileDownload(int nbFileDownload) {
		this.nbFileDownload = nbFileDownload;
	}
	/**
	 * @return  the nbFileFlat
	 * @uml.property  name="nbFileFlat"
	 */
	public int getNbFileFlat() {
		return nbFileFlat;
	}
	/**
	 * @param nbFileFlat  the nbFileFlat to set
	 * @uml.property  name="nbFileFlat"
	 */
	public void setNbFileFlat(int nbFileFlat) {
		this.nbFileFlat = nbFileFlat;
	}
	/**
	 * @return  the nbSession
	 * @uml.property  name="nbSession"
	 */
	public int getNbSession() {
		return nbSession;
	}
	/**
	 * @param nbSession  the nbSession to set
	 * @uml.property  name="nbSession"
	 */
	public void setNbSession(int nbSession) {
		this.nbSession = nbSession;
	}
	/**
	 * @return  the release
	 * @uml.property  name="release"
	 */
	public String getRelease() {
		return release;
	}
	/**
	 * @param release  the release to set
	 * @uml.property  name="release"
	 */
	public void setRelease(String release) {
		this.release = release;
	}
	/**
	 * @return  the size
	 * @uml.property  name="size"
	 */
	public long getSize() {
		return size;
	}
	/**
	 * @param size  the size to set
	 * @uml.property  name="size"
	 */
	public void setSize(long size) {
		this.size = size;
	}
	/**
	 * @return  the sizeCompressed
	 * @uml.property  name="sizeCompressed"
	 */
	public long getSizeCompressed() {
		return sizeCompressed;
	}
	/**
	 * @param sizeCompressed  the sizeCompressed to set
	 * @uml.property  name="sizeCompressed"
	 */
	public void setSizeCompressed(long sizeCompressed) {
		this.sizeCompressed = sizeCompressed;
	}
	/**
	 * @return  the trueTime
	 * @uml.property  name="trueTime"
	 */
	public long getTrueTime() {
		return trueTime;
	}
	/**
	 * @param trueTime  the trueTime to set
	 * @uml.property  name="trueTime"
	 */
	public void setTrueTime(long trueTime) {
		this.trueTime = trueTime;
	}
	/**
	 * @return  the time
	 * @uml.property  name="time"
	 */
	public Date getTime() {
		return time;
	}
	/**
	 * @param time  the time to set
	 * @uml.property  name="time"
	 */
	public void setTime(Date time) {
		this.time = time;
	}
}
