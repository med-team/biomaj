/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */

/**
 * 04/02/13: #15357, O. Sallou Fix path issue in computeRemoteFiles
 */

package org.inria.biomaj.exe.workflow;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Vector;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.inria.biomaj.exe.bank.BankSet;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;

public class ComputedBankWorkflowEngine extends BankSetWorkflowEngine {

	private RemoteBankWorkflowEngine remoteBankWorkflowEngine = null;

	public ComputedBankWorkflowEngine(BiomajBank bank) throws BiomajException {
		super(bank);
		/*
		 * Attention,  une instance de workflow engine est deja reference
		 * avec super (bank)
		 * Ce qui signifie que la la ligne qui suit :
		 * remoteBankWorkflowEngine = new RemoteBankWorkflowEngine(bank);
		 * ne s'enregistre pas au pres la liste static des workflow executer a cette session
		 * 
		 * voir le constructeur de WorkflowEngine()
		 */
		remoteBankWorkflowEngine = new RemoteBankWorkflowEngine(bank);
	}



	@Override
	public void setCommand(int command) {
		super.setCommand(command);
		remoteBankWorkflowEngine.setCommand(command);
	}

	/**
	 * Getter of the property <tt>remoteBankWorkflowEngine</tt>
	 * @return  Returns the remoteBankWorkflowEngine.
	 * @uml.property  name="remoteBankWorkflowEngine"
	 */
	public RemoteBankWorkflowEngine getRemoteBankWorkflowEngine() {
		return remoteBankWorkflowEngine;
	}

	/**
	 * Setter of the property <tt>remoteBankWorkflowEngine</tt>
	 * @param remoteBankWorkflowEngine  The remoteBankWorkflowEngine to set.
	 * @uml.property  name="remoteBankWorkflowEngine"
	 */
	public void setRemoteBankWorkflowEngine(RemoteBankWorkflowEngine remoteBankWorkflowEngine) {
		this.remoteBankWorkflowEngine = remoteBankWorkflowEngine;
	}


	@Override
	public void setModeConsole(boolean withConsole, int mode) {
		super.setModeConsole(withConsole, mode);
		if (remoteBankWorkflowEngine != null)
			remoteBankWorkflowEngine.setModeConsole(withConsole, mode);
		else {
			BiomajLogger.getInstance().log("mode console can't be initialized for:"+getBiomajBank().getDbName());
		}
	}



	/**
	 * Tant qu on ne met pas en production la banque dependante, on peut effectuer le reste du workflow
	 */
	@Override
	protected void runUntilMirror() throws BiomajException {
		startChildren(WorkflowEngine.TARGET_UNTIL_POSTPROCESS);
		synchronizeChildren();
		//2) on execute la banque courante
		computeRemoteDirectory();//calcul remote.dir
		remoteBankWorkflowEngine.runUntilMirror();
	}



	@Override
	protected void runUntilMakeProduction() throws BiomajException {
		startChildren(WorkflowEngine.TARGET_UNTIL_POSTPROCESS);
		synchronizeChildren();
		//2) on execute la banque courante
		computeRemoteDirectory();//calcul remote.dir
		remoteBankWorkflowEngine.runUntilMakeProduction();
	}



	@Override
	protected void runUntilPostProcess() throws BiomajException {
		//1) On lance les fils........
		//----------------------------
		startChildren(WorkflowEngine.TARGET_UNTIL_POSTPROCESS);
		synchronizeChildren();
		//2) on execute la banque courante
		computeRemoteDirectory();//calcul remote.dir
		remoteBankWorkflowEngine.runUntilPostProcess();

		/*
		if (remoteBankWorkflowEngine.linkFuturReleaseExist()) {
			//3) on execute les post_process before.all
			String listBlock = getBlockBeforeAll();
			remoteBankWorkflowEngine.log("List Block - post.process [before.all] :"+listBlock, Project.MSG_INFO);

			//4) On lance le telechargement de la banque courante
			if ((listBlock != null) && (listBlock.trim().compareTo("")!=0))
				remoteBankWorkflowEngine.runPostProcess(listBlock);
		}


		//5) Post process fils :
		startTargetFils(WorkflowEngine.TARGET_POSTPROCESS);
		synchronizeFils();


		if (remoteBankWorkflowEngine.linkFuturReleaseExist()) {
			String listBlock = getBlockAfterAll();
			remoteBankWorkflowEngine.log("List Block - post.process [after.all] :"+listBlock, Project.MSG_INFO);
			if ((listBlock != null) && (listBlock.trim().compareTo("")!=0))
				remoteBankWorkflowEngine.runPostProcess(listBlock);
		}
		 */
	}



	@Override
	protected void runUntilPreProcess() throws BiomajException {
//		1) On lance les fils........
		//----------------------------
		startChildren(WorkflowEngine.TARGET_UNTIL_POSTPROCESS);
		synchronizeChildren();
		//2) on execute la banque courante
		computeRemoteDirectory();//calcul remote.dir
		remoteBankWorkflowEngine.runUntilPreProcess();
	}


	@Override
	protected void runUntilDeployment() throws BiomajException {
		runAll();
	}

	@Override
	protected void runAll() throws BiomajException {
		
		boolean needUpdate = startChildren();
		if (isFromScratch())
			needUpdate = true;
		
		if (!needUpdate) {
			/*
			 * Children were not updated, but this doesn't mean that they were
			 * not independently updated previously.
			 * To ensure that, we verify that the latest session that produced an update
			 * for any of the children ended before the latest session of the parent bank.
			 */
			Map<String, String> latestUpdate = BiomajSQLQuerier.getLatestUpdate(getBiomajBank().getDbName(), true);
			if (latestUpdate != null) {
				String endDate = latestUpdate.get(BiomajSQLQuerier.UPDATE_END);
				for (WorkflowEngine we : getChildren()) {
					Map<String, String> childUpdate = BiomajSQLQuerier.getLatestUpdate(we.getBiomajBank().getDbName(), true);
					// Not supposed to be null
					if (childUpdate != null && 
							childUpdate.get(BiomajSQLQuerier.UPDATE_END).compareTo(endDate) > 0) {
						needUpdate = true;
						break;
					}
				}
			} else {
				needUpdate = true;
			}
			
		}
		
		//2) Execute current bank
		computeRemoteDirectory(); // calculate remote.dir
		computeRemoteFiles();
		
		remoteBankWorkflowEngine.setProperty("children.updated", String.valueOf(needUpdate));

		remoteBankWorkflowEngine.runAll();
		
	}



	@Override
	protected void runRebuild() throws BiomajException {
		remoteBankWorkflowEngine.setListBlockProcessToRebuild(getListBlockProcessToRebuild());
		remoteBankWorkflowEngine.setListMetaProcessToRebuild(getListMetaProcessToRebuild());
		remoteBankWorkflowEngine.setListProcessToRebuild(getListProcessToRebuild());
		remoteBankWorkflowEngine.runRebuild();
	}



	@Override
	protected void runDeployment() throws BiomajException {
		startChildren(WorkflowEngine.TARGET_DEPLOY);
		remoteBankWorkflowEngine.runDeployment();
	}



	@Override
	protected void runMakeProduction() throws BiomajException {
		computeRemoteDirectory();//calcul remote.dir
		remoteBankWorkflowEngine.runMakeProduction();
	}



	@Override
	protected void runMirror() throws BiomajException {
		computeRemoteDirectory();//calcul remote.dir
//		3) On lance le telechargement de la banque courante
		remoteBankWorkflowEngine.runMirror();
	}

	protected void runPostProcessBeforeAll() throws BuildException, BiomajException {
//		EXECUTION DES BLOCKS BEFORE.ALL
		//-------------------------------
		String listBlock = getBlockBeforeAll();
		
		if ((listBlock == null) || (listBlock.trim().compareTo("")==0) )
			return ;
		
		if (remoteBankWorkflowEngine.linkFuturReleaseExist()) {
			//3) on execute les post_process before.all
			
			remoteBankWorkflowEngine.log("List Block - post.process [before.all] :"+listBlock, Project.MSG_INFO);

			//4) On lance le telechargement de la banque courante
			if ((listBlock != null) && (listBlock.trim().compareTo("")!=0))
				remoteBankWorkflowEngine.runPostProcess(listBlock);
		}

	}
	
	
	protected void runPostProcessFils() throws BuildException, BiomajException {
		
	}
	
	@Override
	protected void runPostProcess() throws BiomajException {
		runPostProcessBeforeAll() ;
		//5) Post process fils :
		startChildren(WorkflowEngine.TARGET_POSTPROCESS);

		//		EXECUTION DES BLOCKS SYNCHRONISE SUR BLOCK FILS
		//     -------------------------------
		
		if (remoteBankWorkflowEngine.linkFuturReleaseExist()) {
			int i=0;
			String dep = null;
			while ((dep = getBankAndBlockChild(i))!=null) {
				String[] deps = dep.split("\\.");

				if (deps.length!=2) {
					throw new BiomajException("computedbank.workflow.error.internal.deps",deps);
				}
				remoteBankWorkflowEngine.log(getBiomajBank().getDbName()+" : attendre :"+deps[0]+"-"+ deps[1],Project.MSG_DEBUG);
				synchronizeFils(deps[0], deps[1]);
				remoteBankWorkflowEngine.runPostProcess(getBlockDependsChild(i));
				i++;
			}	
		}
		synchronizeChildren();
		
		//EXECUTION DES BLOCKS AFTER.ALL
		//-------------------------------
		
		if (remoteBankWorkflowEngine.linkFuturReleaseExist()) {
			String listBlock = getBlockAfterAll();
			remoteBankWorkflowEngine.log("List Block - post.process [after.all] :"+listBlock, Project.MSG_INFO);
			if ((listBlock != null) && (listBlock.trim().compareTo("")!=0))
				remoteBankWorkflowEngine.runPostProcess(listBlock);
		}

	}



	@Override
	protected void runPreProcess() throws BiomajException {
		remoteBankWorkflowEngine.runPreProcess();

	}
	
	@Override
	public void runOnlyRemoveProcess() throws BiomajException {
		Vector<String> versions = getVersionToRemove();
		for (String version : versions) {
			runRemoveProcess(version);
		}
	}
	
	@Override
	public void runRemoveProcess(String version) throws BiomajException {
		remoteBankWorkflowEngine.runRemoveProcess(version);
	}



	@Override
	protected void endWorkflow() throws BiomajException {
//		super.endWorkflow();
		remoteBankWorkflowEngine.endWorkflow();
	}

	@Override
	protected void initWorkflow() throws BiomajException {
//		super.initWorkflow();
		remoteBankWorkflowEngine.setFromScratch(isFromScratch());
		remoteBankWorkflowEngine.initWorkflow();
	}

	@Override
	protected void endWithErrorWorkflow(Exception e) throws BiomajException {
//		super.endWithErrorWorkflow(e);
		remoteBankWorkflowEngine.endWithErrorWorkflow(e);
	}



	@Override
	public void log(String message, int priority) {
		super.log(message,priority);
		remoteBankWorkflowEngine.log(message, priority);
	}

	protected void computeRemoteFiles() throws BiomajException {
		BankSet bs = (BankSet) getBiomajBank();
		//String dataDir = remoteBankWorkflowEngine.getBiomajBank().getPropertiesFromBankFile().getProperty("remote.dir");
		// Manage canonicalpath for all paths
		String dataDir = "";
        try {
         dataDir = new File(remoteBankWorkflowEngine.getBiomajBank().getPropertiesFromBankFile().getProperty("remote.dir")).getCanonicalPath()+"/";
        } catch (IOException e) {
                throw new BiomajException(e);
        }

		StringBuilder computedRemoteFilz = new StringBuilder();
		for (BiomajBank bb : bs.getBankList()) {
			String currentBankName = bb.getDbName();
			String dirVersion = bb.getPropertiesFromBankFile().getProperty("dir.version");
			if (dirVersion == null) // property not declared, default value is dbname
				dirVersion = currentBankName;
			
			String curData = bb.getPropertiesFromBankFile().getProperty(BiomajConst.dataDirProperty);
			
			String path = "";
			try {
				if (new File(curData + "/" + dirVersion + "/future_release").exists()) {
					path = new File(curData + "/" + dirVersion + "/future_release").getCanonicalPath();
				} else {
					path = new File(curData + "/" + dirVersion + "/current").getCanonicalPath();
				}
			} catch (IOException e) {
				throw new BiomajException(e);
			}
			
			String relativePath = path.substring(path.indexOf(dataDir) + dataDir.length());
			String bankFiles = remoteBankWorkflowEngine.getBiomajBank().getPropertiesFromBankFile().getProperty(currentBankName + ".files.move");
			if (bankFiles != null && !bankFiles.trim().isEmpty()) {
				String remoteFiles = relativePath + "/" + bankFiles;
				if (computedRemoteFilz.length() > 0)
					computedRemoteFilz.append(" ");
				computedRemoteFilz.append(remoteFiles);
			}
		}
		
		
		remoteBankWorkflowEngine.getBiomajBank().getPropertiesFromBankFile().setProperty(BiomajConst.remoteFilesProperty, computedRemoteFilz.toString());
		remoteBankWorkflowEngine.updateProperties();
		remoteBankWorkflowEngine.log("value remote.files:[" + computedRemoteFilz.toString() + "]", Project.MSG_INFO);
	}

	protected void computeRemoteDirectory() throws BiomajBuildException {
		
		String dataDir = remoteBankWorkflowEngine.getBiomajBank().getPropertiesFromBankFile().getProperty("data.dir");
		BankSet bs = (BankSet) getBiomajBank();
		for (BiomajBank bb : bs.getBankList()) {
			if (dataDir.equals("/")) {
				break;
			}
			
			String curData = bb.getPropertiesFromBankFile().getProperty(BiomajConst.dataDirProperty);
			if (!curData.startsWith(dataDir)) {
				String newRoot = "/";
				String[] split = dataDir.split("/");
				for (String dir : split) {
					if (!dir.isEmpty() && curData.startsWith(newRoot + dir)) {
						newRoot += dir + "/";
					}
				}
				dataDir = newRoot;
			}
			
		}
		
		remoteBankWorkflowEngine.getBiomajBank().getPropertiesFromBankFile().setProperty(BiomajConst.remoteDirProperty, dataDir);
		remoteBankWorkflowEngine.updateProperties();
		remoteBankWorkflowEngine.log("value remote.dir:[" + dataDir + "]", Project.MSG_INFO);
		
		/*
		BankSet bs = (BankSet)getBiomajBank();
		
		if (bs.getBankList().size() == 0)
			return;

		BiomajBank bb = bs.getBankList().get(0);

		String path = bb.getPropertiesFromBankFile().getProperty(BiomajConst.dataDirProperty)+"/"+bb.getPropertiesFromBankFile().getProperty(BiomajConst.versionDirProperty);

		File future_release = new File(path+"/"+BiomajConst.futureReleaseLink);

		String valueVersion = "";

		if (future_release.exists()) {
			try {
				remoteBankWorkflowEngine.log(BiomajConst.futureReleaseLink+" directory found!", Project.MSG_INFO);
				valueVersion = future_release.getCanonicalPath();

			} catch (IOException ioe) {
				remoteBankWorkflowEngine.log(ioe.getMessage(), Project.MSG_ERR);
				throw new BiomajBuildException(remoteBankWorkflowEngine.getProject(),"MESSAGE_ERREUR_DEPENDS",ioe);
			}
		} else {
			remoteBankWorkflowEngine.log("no detect futur release", Project.MSG_INFO);
			File current_release = new File(path+"/"+BiomajConst.currentLink);

			if (current_release.exists()) {
				try {
					remoteBankWorkflowEngine.log(BiomajConst.currentLink+" directory found!", Project.MSG_INFO);
					valueVersion = current_release.getCanonicalPath();
				} catch (IOException ioe) {
					remoteBankWorkflowEngine.log(ioe.getMessage(), Project.MSG_ERR);
					throw new BiomajBuildException(remoteBankWorkflowEngine.getProject(),"MESSAGE_ERREUR_DEPENDS",ioe);
				}
			} else 
				throw new BiomajBuildException(remoteBankWorkflowEngine.getProject(),"MESSAGE_ERRUEUR_DEPENDS_CANT_COMPUT_REMOTEDIR",null);
		}

		valueVersion = valueVersion+"/"+remoteBankWorkflowEngine.getBiomajBank().getPropertiesFromBankFile().getProperty(BiomajConst.flatRepositoryProperty);
		File file = new File (valueVersion);

		if (file.exists()) {
			remoteBankWorkflowEngine.getBiomajBank().getPropertiesFromBankFile().setProperty(BiomajConst.remoteDirProperty, valueVersion);
			remoteBankWorkflowEngine.updateProperties();
			remoteBankWorkflowEngine.log("value remote.dir:["+valueVersion+"]", Project.MSG_INFO);
		} else
			throw new BiomajBuildException(remoteBankWorkflowEngine.getProject(),"computedbank.workflow.error.datalocation",valueVersion,null);
			
			*/
	}


}
