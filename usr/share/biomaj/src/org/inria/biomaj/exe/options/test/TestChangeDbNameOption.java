package org.inria.biomaj.exe.options.test;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.apache.tools.ant.Project;
import org.inria.biomaj.exe.bank.BankFactory;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.exe.options.BiomajRemoveBank;
import org.inria.biomaj.exe.options.ChangeDbName;
import org.inria.biomaj.exe.workflow.WorkflowEngine;
import org.inria.biomaj.exe.workflow.WorkflowEngineFactory;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.Configuration;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.sql.SQLConnectionFactory;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Runs an update process then a changedbname process.
 * A remove process is launched at the end to clean up the
 * created files.
 * Therefore, update and remove options should be tested beforehand.
 * 
 * @author rsabas
 *
 */
public class TestChangeDbNameOption {
	
	private static Configuration config = new Configuration();
	private static String bankName = "testbank";
	private static String newName = "testbankbis";

	@BeforeClass
	public static void setup() {
		TestUpdateOption.createTestFiles();
		SQLConnectionFactory.setTestMode(true);
		System.out.println("Running update of testbank...");
		BankFactory bf = new BankFactory();
		
		try {
			WorkflowEngine.initWorkflowEngine();
			BiomajUtils.fillConfig(bankName, config);
			BiomajBank bb = bf.createBank(bankName,true);
			WorkflowEngineFactory wef = new WorkflowEngineFactory();
			WorkflowEngine we ;
			we = wef.createWorkflow(bb);
			we.setModeConsole(false, Project.MSG_INFO);
			we.setWorkWithCurrentDirectory(false);
			we.setForceForANewUpdate(false);
			we.setFromScratch(false);
			we.setCommand(WorkflowEngine.TARGET_ALL);
			we.start();
			
			we.join(); // wait for thread to end

		} catch (BiomajException e) {
			System.err.println("Biomaj has detected an error! for bank ["+ bankName+"]");
			System.err.println(e.getLocalizedMessage());
			
		} catch (BiomajBuildException bbe) {
			System.err.println("Biomaj has detected an error during the execution of workflow for the bank :"
					+ bankName);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
		}
	}
	
	/**
	 * 1 - Execute changedbname option
	 * 2 - Checks that the name has been modified in the db and that 
	 * queries can be made with this name
	 * 3 - Check that properties file has been renamed
	 * 4 - Check that log directory has been renamed
	 */
	@Test
	public void runTest() {
		File path = BankFactory.getBankPath(bankName);
		try {
			ChangeDbName.changeDbName(bankName, newName);
		} catch (BiomajException e) {
			e.printStackTrace();
		}
		
		
		assertTrue(BiomajSQLQuerier.getBankInfo(newName) != null);
		
		try {
			File newProperty = new File(path.getParent() + "/" + newName + ".properties");
			File newLog = new File(BiomajInformation.getInstance().getProperty(BiomajInformation.LOGDIR) + "/" + newName);
			assertTrue(newProperty.exists());
			assertTrue(newLog.exists());
		} catch (BiomajException e) {
			e.printStackTrace();
		}
	}
	
	private static void remove() {
		// Remove created files
		WorkflowEngine.initWorkflowEngine();
		BiomajRemoveBank brb = new BiomajRemoveBank();
		brb.setBankName(newName);
		try {
			brb.executeWithoutUserInteraction(null, true, true).join();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}
	
	
	@AfterClass
	public static void cleanup() {
		BiomajUtils.deleteAll(new File(BiomajUtils.getBiomajRootDirectory() + "/testbanks"));
		remove();
		// Changes the name back
		try {
			ChangeDbName.changeDbName(newName, bankName);
		} catch (BiomajException e) {
			e.printStackTrace();
		}
	}
}
