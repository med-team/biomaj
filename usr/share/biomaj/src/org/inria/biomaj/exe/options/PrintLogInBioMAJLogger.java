package org.inria.biomaj.exe.options;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;

import org.inria.biomaj.ant.logger.BiomajConsoleLogger;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajUtils;

public class PrintLogInBioMAJLogger extends Thread {

	private StyledDocument sdoc;
	private String fileLog ;
	private int lineStart = 0;
	private int levelSelected ;
	private JTextPane fileLogTextArea ;
	private boolean stop = false ;
	private boolean activeScrollLock = false  ;
	
	
	private int nbLineResult =  0;

	public PrintLogInBioMAJLogger(String fileLog,JTextPane fileLogTextArea,StyledDocument sdoc,int lineStart,int levelSelected) {
		this.fileLog =  fileLog;
		this.sdoc    = sdoc     ;
		this.lineStart = lineStart ;
		this.levelSelected = levelSelected ;
		this.fileLogTextArea=fileLogTextArea;
	}

	private Pattern patternLog = Pattern.compile("\\[(\\d{2}-\\d{2}-\\d{4}\\s\\d{2}:\\d{2}:\\d{2})\\]\\[(\\d+/\\d+)\\]\\[(\\d)\\]\\[([\\w_-]*)\\](.*)");
	private Pattern patternAffectation = Pattern.compile("(\\S+)=(\\S+)");

	@Override
	public void run() {
		String line = null ;
		String prec= "" ;
		try {
			if (lineStart ==0)
				sdoc.remove(0, sdoc.getLength());
			
			BufferedReader log = new BufferedReader( new FileReader(new File(fileLog)));

			for (int i=0;i<lineStart;i++)
				{
				nbLineResult++;
				log.readLine();
				}

			while ((line = log.readLine()) != null) {
				if (stop) {
					nbLineResult = 0;
					log.close();
					return ;
				}
				nbLineResult++;
				//fileLogTextArea.notifyAll();
				line = line.replaceFirst(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE, "");
				Matcher m = patternLog.matcher(line);
				if (m.matches()) {
					int level = Integer.valueOf(m.group(3));
					if (level > levelSelected)
						continue;

					if (m.groupCount()<5)
						sdoc.insertString(sdoc.getLength(),m.group(4)+"\n",sdoc.getStyle(BioMAJLogger.getLevel(level)));
					else {
						if (prec.compareTo(m.group(1)+m.group(4))!=0) {
							prec = m.group(1)+m.group(4);

							sdoc.insertString(sdoc.getLength(),"\n\n\t["+m.group(1)+"]",sdoc.getStyle("date"));
							sdoc.insertString(sdoc.getLength()," * "+m.group(4)+" * \n\n",sdoc.getStyle("task"));
						}

						sdoc.insertString(sdoc.getLength(),m.group(5)+"\n",sdoc.getStyle(BioMAJLogger.getLevel(level)));
					}
				} else {

					m = RemoteFile.patRemoteFile.matcher(line);
					if (m.find()) {

						RemoteFile rf = new RemoteFile(line);

						if ((rf.getBase()!=null)&&rf.getBase().length()>0)
							sdoc.insertString(sdoc.getLength(),"\t\t"+rf.getBase()+"/"+rf.getName(),sdoc.getStyle("task"));
						else
							sdoc.insertString(sdoc.getLength(),"\t\t"+rf.getName(),sdoc.getStyle("task"));

						sdoc.insertString(sdoc.getLength(),"\ttime=",sdoc.getStyle("debug"));
						sdoc.insertString(sdoc.getLength(),BiomajUtils.dateToString(rf.getDate(), Locale.US),sdoc.getStyle("task"));

						sdoc.insertString(sdoc.getLength(),"\ttimestamp=",sdoc.getStyle("debug"));
						sdoc.insertString(sdoc.getLength(),Long.toString(rf.getDate().getTime()),sdoc.getStyle("task"));
						
						sdoc.insertString(sdoc.getLength(),"\tsize=",sdoc.getStyle("debug"));
						sdoc.insertString(sdoc.getLength(),Long.toString(rf.getSize())+"\n",sdoc.getStyle("task"));
						continue;
					} 

					m = patternAffectation.matcher(line);
					if (m.matches()) {
						sdoc.insertString(sdoc.getLength(),"\t\t"+m.group(1),sdoc.getStyle("task"));
						sdoc.insertString(sdoc.getLength(),"=",sdoc.getStyle("debug"));
						sdoc.insertString(sdoc.getLength(),m.group(2)+"\n",sdoc.getStyle("erreur"));
						continue ;
					}

					if (line.compareTo("#PLACE_HOLDER_TO_PREVENT_EMPTY_FILE")==0)
						continue;
					
					sdoc.insertString(sdoc.getLength(),line+"\n",sdoc.getStyle("unknown"));
					BiomajLogger.getInstance().log(line+" : no match with :"+patternLog.pattern());
				}
				if (activeScrollLock&&(sdoc.getLength()>0))
					fileLogTextArea.setCaretPosition(sdoc.getLength()-1);
			}
			log.close();
		} catch (Exception ee) {
			BiomajLogger.getInstance().log(ee);
			//	ee.printStackTrace();
		}
	}

	
	public int getNbLineResult() {
		return nbLineResult ;
	}

	public void setStopTraitement() {
		stop = true;
	}


	public boolean isActiveScrollLock() {
		return activeScrollLock;
	}


	public void setActiveScrollLock(boolean activeScrollLock) {
		this.activeScrollLock = activeScrollLock;
		if (activeScrollLock&&(sdoc.getLength()>0)&&(fileLogTextArea!=null))
			fileLogTextArea.setCaretPosition(sdoc.getLength()-1);
	}

}
