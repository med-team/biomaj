package org.inria.biomaj.exe.options.test;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.apache.tools.ant.Project;
import org.inria.biomaj.exe.bank.BankFactory;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.exe.options.BiomajImport;
import org.inria.biomaj.exe.options.BiomajRemoveBank;
import org.inria.biomaj.exe.workflow.WorkflowEngine;
import org.inria.biomaj.exe.workflow.WorkflowEngineFactory;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.Configuration;
import org.inria.biomaj.sql.SQLConnectionFactory;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Runs an update process to have online production directories,
 * removes the records related to the bank from the database so 
 * that import command can be tested.
 * Remove process is launched afterwards to clean up the files.
 * update and remove options should be tested beforehand.
 * 
 * @author rsabas
 *
 */
public class TestImportOption {
	
	private static Configuration config = new Configuration();
	private static String bankName = "testbank";

	@BeforeClass
	public static void setup() {
		TestUpdateOption.createTestFiles();
		SQLConnectionFactory.setTestMode(true);
		System.out.println("Running update of testbank...");
		BankFactory bf = new BankFactory();
		
		try {
			WorkflowEngine.initWorkflowEngine();
			BiomajUtils.fillConfig(bankName, config);
			BiomajBank bb = bf.createBank(bankName,true);
			WorkflowEngineFactory wef = new WorkflowEngineFactory();
			WorkflowEngine we ;
			we = wef.createWorkflow(bb);
			we.setModeConsole(false, Project.MSG_INFO);
			we.setWorkWithCurrentDirectory(false);
			we.setForceForANewUpdate(false);
			we.setFromScratch(false);
			we.setCommand(WorkflowEngine.TARGET_ALL);
			we.start();
			
			we.join(); // wait for thread to end

		} catch (BiomajException e) {
			System.err.println("Biomaj has detected an error! for bank ["+ bankName+"]");
			System.err.println(e.getLocalizedMessage());
			
		} catch (BiomajBuildException bbe) {
			System.err.println("Biomaj has detected an error during the execution of workflow for the bank :"
					+ bankName);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
		}
	}
	
	/**
	 * 1 - Test that the database does not contain information about the bank
	 * that we want to import
	 * 2 - Launches the import
	 * 3 - Checks that bank general information has been inserted into the db
	 * 4 - Checks that a record for the production directory has been created
	 */
	@Test
	public void runTest() {
		
		BiomajSQLQuerier.deleteAllFromBank(bankName);
		
		assertTrue(BiomajSQLQuerier.getBankInfo(bankName) == null);
		BiomajImport.importIntoDB(bankName);
		assertTrue(BiomajSQLQuerier.getBankInfo(bankName) != null);
		assertTrue(BiomajSQLQuerier.getProductionDirectories(bankName).size() > 0);
	}
	
	/**
	 * Launches remove process to delete the created files.
	 */
	private static void remove() {
		WorkflowEngine.initWorkflowEngine();
		BiomajRemoveBank brb = new BiomajRemoveBank();
		brb.setBankName(bankName);
		try {
			brb.executeWithoutUserInteraction(null, true, false).join();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}
	
	
	@AfterClass
	public static void cleanup() {
		remove();
		BiomajUtils.deleteAll(new File(BiomajUtils.getBiomajRootDirectory() + "/testbanks"));
//		BiomajSQLQuerier.closeTestModeConnection();
	}
}
