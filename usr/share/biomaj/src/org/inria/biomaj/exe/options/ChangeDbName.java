package org.inria.biomaj.exe.options;

import java.io.File;

import org.inria.biomaj.exe.bank.BankFactory;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

public class ChangeDbName {

	public static void changeDbName(String dbNameIn,String dbNameOut) throws BiomajException {
		
		
//		File property = new File(BiomajInformation.getInstance().getProperty(BiomajInformation.WORKFLOWSDIR)+"/"+dbNameIn+".properties");
		File property = BankFactory.getBankPath(dbNameIn);
		File log      = new File(BiomajInformation.getInstance().getProperty(BiomajInformation.LOGDIR)+"/"+dbNameIn);
		
		File newProperty = new File(property.getAbsolutePath().substring(0, property.getAbsolutePath().lastIndexOf('/')) + "/" + dbNameOut + ".properties");
		File newLog      = new File(BiomajInformation.getInstance().getProperty(BiomajInformation.LOGDIR)+"/"+dbNameOut);
		
		newProperty.delete();
		BiomajUtils.deleteAll(newLog);
		
		try {
			BiomajUtils.copy(property, newProperty);
			BiomajUtils.changeKey(newProperty, BiomajConst.dbNameProperty, dbNameOut);
							
			log.renameTo(newLog);
			
			File runtimeDir = new File(newLog.getAbsolutePath()+"/runtime");
			if (runtimeDir.exists()) {
				for (File f : runtimeDir.listFiles()) {
					if (f.getName().contains(dbNameIn)) {
						f.renameTo(new File(f.getAbsolutePath().replaceFirst(dbNameIn+"\\.", dbNameOut+".")));
					}
				}
			}
			property.delete();

			BiomajSQLQuerier.renameBank(dbNameIn, dbNameOut);
			
			System.out.println(dbNameIn+" --> "+dbNameOut +" : ok");
			
		} catch (BiomajException e) {
			newProperty.delete();
			throw e;
		} catch (Exception e) {
			newProperty.delete();
			throw new BiomajException(e);
		}
		
	}
}
