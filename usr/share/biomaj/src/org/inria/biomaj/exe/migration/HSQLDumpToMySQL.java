package org.inria.biomaj.exe.migration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HSQLDumpToMySQL {

	private static String[] sqlNames = {"bank", "productionDirectory", "remoteInfo", "localInfo",
			"updateBank", "session_has_message", "message", "sessionTask", "session_has_sessionTask",
			"sessionTask_has_message", "metaprocess_has_message", "process_has_message", "process",
			"sessionTask_has_file", "file"};
	
	public static void main(String[] args) {
		try {
			Map<String, String> autoInc = new HashMap<String, String>();
			
			BufferedReader br = new BufferedReader(new FileReader(args[0]));
			PrintWriter pw = new PrintWriter(new File(args[0] + "_bis"));
			pw.println("SET foreign_key_checks = 0;");
			
			for (String name : sqlNames) {
				pw.println("ALTER TABLE " + name + " MODIFY id" + name + " INTEGER NOT NULL;");
			}
			
			
			String line;
			while ((line = br.readLine()) != null) {
				if (line.startsWith("CREATE") || line.startsWith("GRANT") || line.startsWith("SET"))
					continue;
				
				if (line.startsWith("ALTER TABLE")) {
					Matcher matcher = Pattern.compile("ALTER TABLE (\\S+) .* RESTART WITH (\\p{Digit}+)").matcher(line);
					if (matcher.find()) {
						autoInc.put(getSQLTableName(matcher.group(1)), matcher.group(2));
					}
					continue;
				}
				
				
				
				if (line.contains(" PRODUCTIONDIRECTORY "))
					line = line.replaceFirst("\\s+PRODUCTIONDIRECTORY\\s+", " productionDirectory ");
				else if (line.contains(" CONFIGURATION "))
					line = line.replaceFirst("\\s+CONFIGURATION\\s+", " configuration ");
				else if (line.contains(" LOCALINFO "))
					line = line.replaceFirst("\\s+LOCALINFO\\s+", " localInfo ");
				else if (line.contains(" REMOTEINFO "))
					line = line.replaceFirst("\\s+REMOTEINFO\\s+", " remoteInfo ");
				else if (line.contains(" UPDATEBANK "))
					line = line.replaceFirst("\\s+UPDATEBANK\\s+", " updateBank ");
				else if (line.contains(" SESSION "))
					line = line.replaceFirst("\\s+SESSION\\s+", " session ");
				else if (line.contains("SESSION_HAS_MESSAGE"))
					line = line.replaceFirst("\\s+SESSION_HAS_MESSAGE\\s+", " session_has_message ");
				else if (line.contains(" MESSAGE "))
					line = line.replaceFirst("\\s+MESSAGE\\s+", " message ");
				else if (line.contains(" SESSIONTASK "))
					line = line.replaceFirst("\\s+SESSIONTASK\\s+", " sessionTask ");
				else if (line.contains(" SESSION_HAS_SESSIONTASK "))
					line = line.replaceFirst("\\s+SESSION_HAS_SESSIONTASK\\s+", " session_has_sessionTask ");
				else if (line.contains(" SESSIONTASK_HAS_MESSAGE "))
					line = line.replaceFirst("\\s+SESSIONTASK_HAS_MESSAGE\\s+", " sessionTask_has_message ");
				else if (line.contains(" SESSIONTASK_HAS_FILE "))
					line = line.replaceFirst("\\s+SESSIONTASK_HAS_FILE\\s+", " sessionTask_has_file ");
				else if (line.contains(" FILE "))
					line = line.replaceFirst("\\s+FILE\\s+", " file ");
				else if (line.contains(" METAPROCESS "))
					line = line.replaceFirst("\\s+METAPROCESS\\s+", " metaprocess ");
				else if (line.contains(" PROCESS "))
					line = line.replaceFirst("\\s+PROCESS\\s+", " process ");
				else if (line.contains(" PROCESS_HAS_MESSAGE "))
					line = line.replaceFirst("\\s+PROCESS_HAS_MESSAGE\\s+", " process_has_message ");
				else if (line.contains(" METAPROCESS_HAS_MESSAGE "))
					line = line.replaceFirst("\\s+METAPROCESS_HAS_MESSAGE\\s+", " metaprocess_has_message ");
				else if (line.contains(" BANK "))
					line = line.replaceFirst("\\s+BANK\\s+", " bank ");
				
				pw.println(line + ";");
			}
			pw.println("SET foreign_key_checks = 1;");
			
			for (String table : autoInc.keySet()) {
				pw.println("ALTER TABLE " + table + " AUTO_INCREMENT = " + autoInc.get(table) + ";");
				pw.println("ALTER TABLE " + table + " MODIFY id" + table + " INTEGER NOT NULL AUTO_INCREMENT;");
			}
			
			pw.close();
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static String getSQLTableName(String name) {
		for (String s : sqlNames){
			if (s.equalsIgnoreCase(name))
				return s;
		}
		return null;
	}
}
