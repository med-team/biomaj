/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.utils;

public class BiomajConst {

	/**
	 * General.conf
	 */
	public final static String nameGeneralConf           = "general.conf";
	
	/**
	 * Biomaj project directory
	 */
	
	public final static String workflowXmlDirectory       = "/workflows/";
	
	/**
	 * Property File
	 * 
	 */
	public final static String tmpLockDirectory    = "tmp";
	
	public final static String globalProperties= "global.properties";
	/**
	 * Biomaj directory
	 */
	public final static String tmpDirectoryProperty        = "tmp.dir";
	
	
	public final static String propertiesDirectoryProperty = "dbprop.dir";
	
	public final static String futureReleaseLink    = "future_release";
	public final static String currentLink         = "current";
	// Repertoire temporaire dans lequel on deplace les fichiers pour provoquer la mise
	// a jour de la banque avec l'option --fromscratch
	public final static String tmpMoveDir		= "tmpmove";
	
	public final static String extensionVar         = ".var";
	public final static String runtimeDirectoryProperty        = "runtime.dir";
	/**
	 * Xml Description Workflow
	 */
	
	public final static String mirrorXmlFile        = "mirror.xml";
	public final static String processXmlFile       = "handle_process.xml";
	/**
	 * Ant Script Const : Project Name, Target, Task name,...
	 */
	public final static String mirrorProject       = "mirror";
	public final static String handleProcessProject= "handle_process";
	public final static String initMirrorTarget    = "init";
	public final static String releaseTarget       = "release";
	public final static String downloadTarget      = "download";
	public final static String extractTarget       = "extract";
	public final static String checkTarget         = "check";
	public final static String moveTarget          = "move";
	public final static String copyTarget          = "copy";
	public final static String deployTarget        = "deployment";
	
	public final static String processTarget       = "execute_process_sequentialy";
	public final static String postprocessTarget   = "postprocess";
	public final static String preprocessTarget    = "preprocess";
	public final static String removeprocessTarget   = "removeprocess";
	
	
	public final static String buildProject        = "citrina";
	public final static String callMirrorTarget    = "call-mirror";
	
	
	/**
	 * Task Name
	 */
	public final static String fileCheckTask           = "filecheck";
	public final static String releaseTask             = "bmaj-release";
	public final static String versionsmanagementTask  = "bmaj-versionsmanagement";
	
	/**
	 * XML Constantes 
	 */
	public final static String xsdBank                    = "./xslt/xmlSchemaState.xsd";
	public final static String sessions                   = "sessions";
	public final static String session                    = "session";
	public final static String configuration              = "configuration";
	public final static String virtualListTag             = "virtualBankList";
	public final static String virtualBankTag             = "virtualBank";
	
	public final static String remoteInfoTag              = "remoteInfos";
	public final static String localInfoTag               = "localInfos";
	
	public final static String serverInfoTag              = "serverInfo";
	public final static String bankTag                    = "dbName";
	public final static String protocolTag                = "protocol";
	public final static String typeBankTag                = "dbType";
	public final static String urlRemoteTag               = "server";
	public final static String fullNameTag                = "dbFullname";
	
	public final static String remoteDirTag               = "remoteDir";
	
	public final static String lastUpdateTag              = "lastUpdate";
	public final static String beginUpdateTag             = "beginUpdate";
	
	public final static String checkTag               = "check";
	public final static String downloadTag            = "download";
	public final static String extractTag             = "extract";
	public final static String moveTag                = "makeRelease";
	public final static String copyTag                = "addLocalFiles";
	public final static String preProcessTag          = "preprocess";
	public final static String postProcessTag         = "postprocess";
	public final static String removeProcessTag       = "removeprocess";
	public final static String deploymentTag          = "deployment";
	
	public final static String filesUpdatedTag            = "filesUpdatedTag";
	public final static String fileUpdatedTag             = "fileUpdated";
	public final static String bankSizeTag                = "bankSize";
	public final static String bankSizeZippedTag          = "bankSizeZipped";
	public final static String ellapsedTotalTimeTag       = "elapsedTotalTime";
	public final static String releaseTag                 = "release";
	public final static String directoryProductionTag     = "directory";
	
	
	public final static String frequencyTag               = "frequency";
	public final static String logFileTag                 = "logfile";
	public final static String doLinkCopyTag              = "dolinkcopy";
	public final static String releaseSearchTag           = "releaseInfo";
	public final static String remoteFilesTag             = "remoteFiles";
	public final static String remoteExcludedFilesTag             = "remoteExcludedFiles";
	public final static String localFilesTag              = "localFiles";
	public final static String nversionTag                = "nversions";
	public final static String stateTag                   = "state";
	
	public final static String onlineDirectoryTag         = "onlineDirectory";
	public final static String offlineDirectoryTag        = "offlineDirectory";
	public final static String versionDirectoryTag        = "versionDirectory";
	public final static String currentVersionDirTag       = "currentVersionDirName";
	public final static String oldVersionDirTag           = "oldVersionDirName";
	
	public final static String treatmentsTag              = "treatments";
	
	/**
	 * Biomaj Var
	 */
	public final static String daily                   = "daily";
	public final static String weekly                  = "weekly";
	public final static String monthly                 = "monthly";
	
	public final static String PP_WARNING             = "PP_WARNING";
	public final static String PP_DEPENDENCE          = "PP_DEPENDENCE";
	public final static String PP_DEPENDENCE_VOLATILE = "PP_DEPENDENCE_VOLATILE";
//	-------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Internal Properties
	 */
	
	public final static String noDefine                   = "noDefine";	
	public final static String noMirrorContext            ="noMirrorContext";
	public final static String newUpdateProperty          ="new_update_prop";
	public final static String countDownloadProperty      ="countDownload";
	public final static String countExtractProperty       ="countExtract";
	public final static String countLocalOnlineFileProperty     ="countLocalOnline";
	public final static String countLocalOfflineFileProperty     ="countLocalOffline";
	
	public final static String urlConcatParams = "urlconcatparams";
	
	/**
	 * Global Properties
	 */
	public final static String levelMaskProperty           = "historic.logfile.level";	
	public final static String taskMaskProperty            = "historic.logfile.task";
	public final static String targetMaskProperty          = "historic.logfile.target";	
	public final static String propertiesMaskProperty      = "historic.logfile.properties";
	
	public final static String mailSmtpHostProperty        = "mail.smtp.host";	
	public final static String mailAdminProperty           = "mail.admin";
	public final static String mailFromProperty            = "mail.from";
	
	/**
	 * Proxy properties
	 */
	/*
	public final static String httpsProxyHost              = "https.proxyHost";	
	public final static String httpsProxyPort              = "https.proxyPort";
	public final static String httpsProxyUser              = "https.proxyUser";	
	public final static String httpsProxyPassword          = "https.proxyPassword";
	*/
	public final static String proxyHost              = "proxyHost";	
	public final static String proxyPort              = "proxyPort";
	public final static String proxyUser              = "proxyUser";	
	public final static String proxyPassword          = "proxyPassword";
	
	
	//-------------------------------------------------------------------------------------------------------------------
	/**
	 * Bank Properties
	 */
	public final static String includePropertiesProperty  = "include.properties";
	public final static String dbList                = "db.list";
	public final static String bankNumThreadProperty = "bank.num.threads";
	public final static String filesNumThreadProperty = "files.num.threads";
	public final static String productionDirChmodProperty = "production.directory.chmod";
	
	public final static String databaseType = "database.type";
	public final static String databaseUrl = "database.url";
	public final static String databaseLogin  = "database.login";
	public final static String databasePassword = "database.password";
	public final static String databaseDriver = "database.driver";
	public final static String databaseTest = "database.test";
	
	public final static String dbNameProperty        = "db.name";
	public final static String frequencyProperty     = "frequency.update";
	public final static String dbFullNameProperty    = "db.fullname";
	public final static String protocolProperty      = "protocol";
	public final static String serverProperty        = "server";
	public final static String typeProperty          = "db.type";
	public final static String virtualListProperty   = "virtual.list";
	public final static String dependsListProperty   = "db.source";
	public final static String portProperty          = "port";
		
	public final static String userNameProperty      = "username";
	public final static String passwordProperty      = "password";
	public final static String remoteDirProperty     = "remote.dir";
	public final static String remoteFilesProperty   = "remote.files";
	public final static String remoteExcludedFilesProperty   = "remote.excluded.files";
	public final static String dataDirProperty       = "data.dir";
	public final static String logDirProperty        = "log.dir";
	public final static String stateFileDirProperty  = "statefiles.dir";
	public final static String onlineDirDynamicProperty     = "online.dir";
	public final static String offlineDirProperty    = "offline.dir.name";
	public final static String versionDirProperty    = "dir.version";
	public final static String localFilesProperty    = "local.files";
	public final static String localFilesExcludedProperty  = "local.files.excluded";
	
	public final static String releaseFileProperty   = "release.file";
	public final static String releaseRegExpProperty = "release.regexp";
	public final static String releaseResultProperty = "remote.release";
	public final static String keepOldVersionProperty= "keep.old.version";
	public final static String treeLevelProperty     = "tree.level";
	public final static String restructModeProperty  = "restructuration.mode";
	public final static String copyModeProperty      = "copy.mode";
	public final static String linkOneProperty       = "link1.dir";
	public final static String linkTwoProperty       = "link2.dir";
	
	public final static String extractFilelistProperty = "extract.filelist";
	public final static String optionWgetProperty    = "wget.options";
	public final static String noExtractProperty     = "no.extract";
	public final static String extractThreadProperty = "extract.threads";
	public final static String doLinkProperty        = "do.link.copy";
	public final static String commandNumberProperty = "command.number.seq";
	public final static String commandDateProperty   = "command.date.seq";
	public final static String dbPreProcessProperty  = "db.pre.process";
	public final static String dbPostProcessProperty = "db.post.process";
	public final static String dbRemoveProcessProperty = "db.remove.process";
	public final static String clOldVersionProperty  = "clean.old.version";
	
	public final static String filesDownloadNeedDynamicProperty  = "files.download.needed";
	public final static String filesExtractNeedDynamicProperty   = "files.extract.needed";
	public final static String filesCopyNeedDynamicProperty      = "files.copy.needed";
	public final static String remoteReleaseDynamicProperty      = "remote.release";
	public final static String removedReleaseProperty      		 = "removed.release";
	public final static String offlineHasFilesDynamicProperty    = "offline.has.files";
	
	public final static String listFilesAvailable                = "list.files.available";
	
	/**
	 * FTP Protocol Properties
	 */
	public final static String ftpTimeOut             = "ftp.timeout";
	public final static String ftpTriesConnexion      = "ftp.automatic.reconnect";
	
	
	/**
	 * Http protocol properties
	 */
	
	public final static String httpParseDirLineProperty      = "http.parse.dir.line";
	public final static String httpParseFileLineProperty     = "http.parse.file.line";
	public final static String httpGroupDirName              = "http.group.dir.name";
	public final static String httpGroupDirDate              = "http.group.dir.date";
	public final static String httpGroupFileName             = "http.group.file.name";
	public final static String httpGroupFileDate             = "http.group.file.date";
	public final static String httpGroupFileSize             = "http.group.file.size";
	
	public final static String ftpActiveMode                 = "ftp.active.mode";
	
	public final static String flatRepositoryProperty        = "data.location";
	
	/** Gestion des block pour les processes du workflow */
	public final static String blockPostprocessProperty                = "BLOCKS";
	public final static String currentBlockDynamicProperty             = "current_block";
	
	public final static String logFilesProperty             = "log.files";
	public final static String dbFormatsProperty             = "db.formats";
	
	
	public static final String regexpAll = "**/*";
	
	public static final String bankVisibility = "visibility.default";
	
}
