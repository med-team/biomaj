/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.utils;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.TreeMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.tools.ant.BuildEvent;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;

import org.inria.biomaj.exe.bank.BankFactory;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.exe.main.Biomaj;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;

import org.inria.biomaj.session.bank.Configuration;
import org.inria.biomaj.session.bank.FileDesc;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.singleton.BiomajLogger;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



/**
 * 
 * @author ofilangi
 *
 */
public class BiomajUtils  {
	
	
	 public static final FileFilter FILTER_DIRECTORIES = new FileFilter()
	    {
	        /*
	         * @see java.io.FileFilter#accept(java.io.File)
	         */
	        public boolean accept(java.io.File path)
	        {
	            if (path == null)
	                return false;
	            
	            return path.isDirectory();
	        }
	    };
	
	    
	    
	static public final String DATE_FORMAT_1             =   "dd-MM-yyyy HH:mm"; 
	static public final String DATE_FORMAT_2             =   "dd-MMM-yyyy HH:mm";
	//a partir de la version 0.9.2.x
	static public final String DATE_FORMAT_3             =   "dd-MM-yyyy HH:mm:ss"; 
	static public final String DATE_FORMAT_4             =   "dd-MM-yyyy";
	// US date format
	static public final String DATE_FORMAT_5			 =	 "yyyy-MM-dd HH:mm:ss";
	
	static protected final SimpleDateFormat sdf1 = new SimpleDateFormat(DATE_FORMAT_1);
	static protected final SimpleDateFormat sdf2 = new SimpleDateFormat(DATE_FORMAT_2);
	static protected final SimpleDateFormat sdf3 = new SimpleDateFormat(DATE_FORMAT_3);
	static protected final SimpleDateFormat sdf4 = new SimpleDateFormat(DATE_FORMAT_4);
	static protected final SimpleDateFormat sdf5 = new SimpleDateFormat(DATE_FORMAT_5);
	
	static public final String REGEXP_DATE_FORMAT_1      =   "[\\d]{2}-[\\d]{2}-[\\d]{4}\\s[\\d]{2}:[\\d]{2}"; 
	static public final String REGEXP_DATE_FORMAT_2      =   "[\\d]{2}-[\\w]{3}-[\\d]{4}\\s[\\d]{2}:[\\d]{2}";
	//a partir de la version 0.9.2.x
	static public final String REGEXP_DATE_FORMAT_3      =   "[\\d]{2}-[\\d]{2}-[\\d]{4}\\s[\\d]{2}:[\\d]{2}:[\\d]{2}";
	static public final String REGEXP_DATE_FORMAT_5      =   "[\\d]{4}-[\\d]{2}-[\\d]{2}\\s[\\d]{2}:[\\d]{2}:[\\d]{2}";
	
	
	static public final String DATE_FORMAT_IN_USE        =   DATE_FORMAT_3; 
	static public final String REGEXP_DATE_FORMAT_IN_USE =   REGEXP_DATE_FORMAT_5;
	
	private static List<Long> ids = new ArrayList<Long>();
	
	/**
	 * This function recover the current date and format it for the field "begin download" of the xml
	 * state file
	 * @return return the current date formatted for the field "begin download" of the xml state file
	 */
	
	static public String getCurrentDate () {
		return dateToString(new Date(), Locale.US);
	}
	
	
	/**
	 * Parses a date in a given format according to the locale.
	 * 
	 * @param date
	 * @param locale
	 * @return
	 */
	static public String dateToString(Date date, Locale locale) {
		final SimpleDateFormat sdf;
		if (locale.equals(Locale.FRANCE))
			sdf = new SimpleDateFormat(DATE_FORMAT_IN_USE);
		else
			sdf = new SimpleDateFormat(DATE_FORMAT_5);
		
		if (date==null)
			return sdf.format(new Date());

		return sdf.format(date);
	}
	
	/**
	 * Transforms a french formatted date (jj-MM-yyyy hh:mm:ss) into a
	 * US formatted date (yyyy-MM-jj hh:mm:ss)
	 * 
	 * @param date date to be transformed
	 * @return
	 */
	public static String toUSDate(String date) {
		Date d = null;
		try {
			d = stringToDate(date);
		} catch (ParseException e) {
			System.err.println("Invalid date format.");
			e.printStackTrace();
		}
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_5);
		return sdf.format(d);
		
	}
	
	/**
	 * String date has to be well formed : see DATE_FORMAT
	 * keep compatibility with old version (citrina/biomaj)
	 * @param date
	 * @return
	 */
	static synchronized public Date stringToDate (String date) throws ParseException {
		try {
			return sdf5.parse(date);
		} catch (ParseException ex) {
			try {
				return sdf3.parse(date);
			} catch (ParseException pe) {
				try {
					return sdf1.parse(date);
				} catch (ParseException pe2) {
					try {
						return sdf2.parse(date);
					} catch (ParseException pe3) {
						try {
							SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_2, Locale.US);
							return sdf.parse(date);
						} catch (ParseException pe4) {
							return sdf4.parse(date);
						}
					}
				}
			}
		}
	}

	
	/**
	 * This function returns the month string according to its number
	 * 
	 * @param numMonth The number of the month
	 * @return Returns the month string
	 */
	static public String getMonth (int numMonth) {
		switch (numMonth) {
		case 1 :
			return "Jan" ;
		case 2 :
			return "Feb" ;
		case 3 :
			return "Mar" ;
		case 4 :
			return "Apr" ;
		case 5 :
			return "May" ;
		case 6 :
			return "Jun" ;
		case 7 :
			return "Jul" ;
		case 8 :
			return "Aug" ;
		case 9 :
			return "Sep" ;
		case 10 :
			return "Oct" ;
		case 11 :
			return "Nov" ;
		case 12 :
			return "Dec" ;
		default :
			return "Jan" ;
		}
	}
	
	/**
	 * Ensures that different threads dont get the same id.
	 * 
	 * @return
	 */
	public static synchronized long getUniqueNumericId() {
		long id;
		while (ids.contains(id = new Date().getTime())) {
			id = new Date().getTime();
		}
		ids.add(id);
		return id;
	}
	
	
	/**
	 * Verif if a property is set
	 * @param p
	 * @param prop
	 * @throws BiomajException
	 */
	static private void verifProperty(Project p,String prop) throws BiomajException {	  
		
		if (p == null)
			throw new BiomajException("citrinautils.error.project");
		
		if (p.getProperty(prop)==null)
			throw new BiomajException("citrinautils.error.property",prop);
	}
	
	/**
	 * Returns the property of a project
	 * @param p
	 * @param prop
	 * @return
	 * @throws BiomajException Envoi une exception si la propriete n existe pas ou p non defini
	 */
	static public String getProperty(Project p,String prop) throws BiomajException {
		
		verifProperty(p,prop);
		return p.getProperty(prop);
	}
	
	/**
	 * Get Biomaj ROOT directory 
	 * @param arg0
	 * @return
	 */
	static public String getBiomajRootDirectory() {
		String envBmaj = System.getenv(Biomaj.ENV_BIOMAJ);
		if (envBmaj == null)
			envBmaj = System.getProperty(Biomaj.ENV_BIOMAJ);
		return envBmaj;
	}
	
	/**
	 * Get Offline directory define in build.xml
	 * @param arg0
	 * @return
	 */
	static public String getOfflineDirectory(Project arg0)  throws BiomajException {
		
		if (arg0.getProperty(BiomajConst.offlineDirProperty)==null)
			throw new BiomajException("citrinautils.error.property",BiomajConst.offlineDirProperty);
		
		
		
		return getProperty(arg0, BiomajConst.dataDirProperty)+"/"+arg0.getProperty(BiomajConst.offlineDirProperty);
	}
	
	
	static public String getLocalFileRegExpr(BuildEvent arg0)  throws BiomajException {
		
		if (arg0.getProject().getProperty(BiomajConst.localFilesProperty)==null)
			throw new BiomajException("citrinautils.error.property",BiomajConst.localFilesProperty);
		
		
		
		return getProperty(arg0.getProject(), BiomajConst.dataDirProperty)+"/"+arg0.getProject().getProperty(BiomajConst.localFilesProperty);
	}
	
	
	static public String getDirectoryVersion(BuildEvent arg0)  throws BiomajException {
		
		if (arg0.getProject().getProperty(BiomajConst.versionDirProperty)==null)
			throw new BiomajException("citrinautils.error.property",BiomajConst.versionDirProperty);
		
		
		
		return getProperty(arg0.getProject(), BiomajConst.versionDirProperty);
	}
	
	static public int computeNbLineInFile(String nameFile, Vector<String> files) {
		File f = new File (nameFile) ;
		
		if (files == null)
			files = new Vector<String>();
		
		if (!f.exists())
			return -1;
		try {
			BufferedReader br = new BufferedReader(new FileReader(f));
			int count = 0;
			String line;
			while ((line=br.readLine()) != null) {
				count++;
				files.add(line);
			}
			return count;
		} catch (Exception e) {
			return -1;
		}
	}
	
	static public String timeToString(Long time) {
		
		//return DateUtils.format(time, DateUtils.ISO8601_TIME_PATTERN);
		//return DateUtils.format(time, "MM:dd:HH");
		//return DateUtils.formatElapsedTime(time);
		//System.out.println(DateUtils.format(time, "dd:HH:mm:ss"));
		//DateUtils.ISO8601_TIME_PATTERN 
		
		time = time / 1000 ;
		//second time in second ....
		
		long sec = (time % 60) ;
		time = time / 60 ;//time in minute
		long min = time % 60 ;
		time = time / 60 ;//time in hours
		long hours = time % 24 ;
		//time = time / 24 ;//time in month
		time = time / 24 ;
		//condition) ? instruction si vrai : instruction si faux
		String secS = Long.toString(sec);
		if (secS.length()==1) secS = "0"+secS ;  
		
		String minS = Long.toString(min);
		if (minS.length()==1) minS = "0"+minS ;  
		
		String hoursS = Long.toString(hours);
		if (hoursS.length()==1) hoursS = "0"+hoursS ;  
		
		String dayS = Long.toString(time);
		if (dayS.length()==1) dayS = "0"+dayS ;  
		
		return dayS+":"+hoursS+":"+minS;//+":"+sec;
		
		/*
		String sec = new Long(time % 60).toString() ;
		if (sec.length()==1)
			sec="0"+sec;
		
		
		time = time / 60 ;
		if (time==0)
			return "00:00:"+sec;
		
		String min = new Long(time % 60).toString() ;
		
		if (min.length()==1)
			min="0"+min;
		
		time = time / 60 ;
		if (time==0)
			return "00:"+min+":"+sec;
		
		String h = new Long(time % 60).toString() ;
		
		if (h.length()==1)
			h="0"+h;
		
		time = time / 24 ;
		if (time==0)
			return h+":"+min+":"+sec;
		*/
		//return time+":"+h+":"+min;//+":"+sec;*/
	}
	
	
	public static void copy_old(File source, File dest) throws IOException {
		FileChannel in = null, out = null;
		try {          
			in = new FileInputStream(source).getChannel();
			out = new FileOutputStream(dest).getChannel();
			
			long size = in.size();
			MappedByteBuffer buf = in.map(FileChannel.MapMode.READ_ONLY, 0, size);
			
			out.write(buf);
			
		} finally {
			if (in != null)      in.close();
			if (out != null)     out.close();
		}
	}
	
	public static void copy(File source, File dest) throws IOException {
		createSubDirectories(getRelativeDirectory(dest.getAbsolutePath()));
		FileInputStream fis  = new FileInputStream(source);
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(dest));
		byte[] buf = new byte[1024];
		int i = 0;
		while ((i = fis.read(buf)) > 0) {
			bos.write(buf, 0, i);
		}
		fis.close();
		bos.close();
		
		dest.setLastModified(source.lastModified());
	}
	
	
	/**
	 * Delete all subdirectories and all sub files
	 * @param directory
	 */
	public static void deleteAll(File directory) {
		
		if (directory.isDirectory()) {
			File[] l = directory.listFiles();
			for (int i=0;i<l.length;i++)
				deleteAll(l[i]);
			directory.delete();
		} else // directory is a file ...
			directory.delete();
	}
	
	/**
	 * Delete all subdirectories and all sub files
	 * @param directory
	 */
	public static void deleteAll(File directory,Vector<File> excluded) {
		
		if (directory.isDirectory()) {
			File[] l = directory.listFiles();
			for (int i=0;i<l.length;i++)
				deleteAll(l[i],excluded);
			
			if (directory.listFiles().length==0)
				directory.delete();
		} else // directory is a file ...
			{
			for (File f : excluded)
				if (f.getAbsolutePath().compareTo(directory.getAbsolutePath())==0)
					return;
			directory.delete();
			}
	}
	
	
	public static int howManyFiles(File dir) {
		
		if (dir.isDirectory()) {
			File[] l = dir.listFiles();
			int c = 0;
			for (File f : l) {
				if (f.isFile())
					c = c+1;
				else if (f.isDirectory()) 
					c = c + howManyFiles(f);
			}
			return c;
		} else if (dir.isFile())
			return 1;
		
		return 0;
	}
	
	/**
	 * This function recover the size of the directory in parameter
	 * It's a recursive function
	 * 
	 * @param rootDirectory The directory to determine the size
	 * @return return the size of the directory
	 */
	public static long getDirectorySize(File rootDirectory)
	{
		if (!rootDirectory.exists())
			return 0;
		try {
		// Correction Bug si link on sort....
		if (rootDirectory.getAbsolutePath().compareTo(rootDirectory.getCanonicalPath())!=0)
			return 0;
		} catch (IOException ioe) {
			return 0;
		}
	
		if (rootDirectory.isDirectory()) {
			//System.out.println("Directory:"+rootDirectory.getName());
			File[] l = rootDirectory.listFiles();
			long res = 0;
			for (int i=0;i<l.length;i++)
				res+=getDirectorySize(l[i]);
			return res;
		} else //is a file !
		{
			return rootDirectory.length();
		}
	}
	
	/**
	 * Delete a file
	 * @param path
	 */
	public static boolean delete (String path) {
		File f = new File (path);
		if (f.exists()) {
			f.delete();
		} else {
			return false;
		}
		return true;
	}
	
	/**
	 * This function recover the bank size and format it for the xml state file
	 * @return return the bank size formatted
	 */
	public static long computeDirectorySize (String directory)
	{
		File fileDir = new File (directory) ;
		if (!fileDir.exists()) {
			BiomajLogger.getInstance().log("["+directory+"] does not exist");
			return 0;
		}
		try {
			return BiomajUtils.getDirectorySize(fileDir) ;
		} catch (Exception e) {
			BiomajLogger.getInstance().log(e);
			return 0;
		}	    
		
		
	}

	public static String sizeToString (Long bankSize) {
		String result="";
		
		long giga = bankSize / (1024*1024*1024);
		
		if (giga>0) {
			bankSize = bankSize % (1024*1024*1024);
			String bankDivByMo = Long.toString(bankSize/(1024*1024));
			result+=Long.toString(giga)+","+bankDivByMo.substring(0,Math.min(3,bankDivByMo.length()))+"G";
			return result;
		}
		
		long mega = 	bankSize /(1024*1024);
		
		if (mega>0)	
		{
			bankSize = bankSize % (1024*1024);
			String bankDivByKo = Long.toString(bankSize/1024);
			result+=Long.toString(mega)+","+bankDivByKo.substring(0,Math.min(3,bankDivByKo.length()))+"M";
		} else {
			result=Long.toString(bankSize/1024)+"K";
		}
		return result;
	}
	
	public static long stringToSize (String bankSize) {
		if ((bankSize==null)||(bankSize.compareTo("")==0))
			return 0;
		try {
		if (bankSize.trim().charAt(bankSize.length()-1)=='G') {
			String value = bankSize.substring(0, bankSize.length()-1);
	
			if (value.contains(".")){
				String[] t = value.split("\\.");
				return Long.valueOf(t[0])*(1024*1024*1024)+Long.valueOf(t[1])*(1024*1024);
			} else if (value.contains(",")){
				String[] t = value.split(",");
				return Long.valueOf(t[0])*(1024*1024*1024)+Long.valueOf(t[1])*(1024*1024);
			} 
			
			return Long.valueOf(value)*(1024*1024*1024);
		} 
		else if (bankSize.trim().charAt(bankSize.length()-1)=='M') {
			String value = bankSize.substring(0, bankSize.length()-1);
			
			if (value.contains(".")) {
				String[] t = value.split("\\.");
				return Long.valueOf(t[0])*(1024*1024)+Long.valueOf(t[1])*(1024);
			}
			else if (value.contains(",")) {
				String[] t = value.split(",");
				return Long.valueOf(t[0])*(1024*1024)+Long.valueOf(t[1])*(1024);
			}
				
			return Long.valueOf(value)*(1024*1024);
			
		} else if (bankSize.trim().charAt(bankSize.length()-1)=='K') {
			String value = bankSize.substring(0, bankSize.length()-1);
			
			if (value.contains(".")) {
				String[] t = value.split("\\.");
				return Long.valueOf(t[0])*(1024)+Long.valueOf(t[1]);
			}
			else if (value.contains(",")) {
				String[] t = value.split(",");
				return Long.valueOf(t[0])*(1024)+Long.valueOf(t[1]);
			}
			return Long.valueOf(value)*(1024);
		} else
			return 0;
		} catch (Exception e) {
			BiomajLogger.getInstance().log(e);
			return 0;
		}
	}
	
	
	public static String getStateFileDirectory() throws BiomajException {
		return BiomajInformation.getInstance().getProperty(BiomajInformation.STATEFILESDIR);
	}
	
	public static Vector<Node> getListChild(Node node,String nameChild) {
		
		Vector<Node> res = new Vector<Node>();
		
		if (node == null)
			return res;
		
		if (node.hasChildNodes()) {
			NodeList nl = node.getChildNodes();
			
			for (int i=0;i<nl.getLength();i++) {
				if (nl.item(i).getNodeName().compareTo(nameChild)==0)
					res.add(nl.item(i));
			}
			
		}
		
		return res;
	}
	
	public static String getAttribute(Node node,String attribut) {
		
		if (node == null)
			throw new NullPointerException("Invalid node");
		
		if (node.hasAttributes()) {
			NamedNodeMap nnm = node.getAttributes();
			Node n = nnm.getNamedItem(attribut);
			if (n == null)
				return "";
			
			return n.getTextContent();
		}
		return "";
		
	}
	
	public static void print(Node node) {
		
		if (node == null)
			BiomajLogger.getInstance().log("Invalid node");	
		BiomajLogger.getInstance().log("<"+node.getNodeName()+" ");
		if (node.hasAttributes()) {
			NamedNodeMap nnm = node.getAttributes();
			for (int i=0;i<nnm.getLength();i++) {
				if (nnm.item(i)==null)
					continue;
				if ((nnm.item(i).getTextContent()==null)||(nnm.item(i).getTextContent().trim().compareTo("")==0))
					BiomajLogger.getInstance().log(nnm.item(i).getNodeName()+"=<<EMPTY>> ");
				else
					BiomajLogger.getInstance().log(nnm.item(i).getNodeName()+"=\""+nnm.item(i).getTextContent()+"\" ");
			}
		}
		
		BiomajLogger.getInstance().log(">");		
	}
	
	
	public static String getRelativeDirectory(String filePath) {
		
		int index = filePath.lastIndexOf("/");
		if (index == -1)
			return "";
		return filePath.substring(0, index);
		
	}
	
	public static String getNameFile(String filePath) {
		int index = filePath.lastIndexOf("/");
		if (index == -1)
			return filePath;
		
		return filePath.substring(index+1);
	}
	
	public static void createSubDirectories(String path) {
		if (path.contains("/")) {
			String subDirectories = path.substring(0, path.lastIndexOf("/"));
			createSubDirectories(subDirectories);
			File f = new File(path);
			if (!f.exists())
				f.mkdir();
		} 
		//sinon rien
	}
	
	//------------------------------------- METHODS WITH HASH -----------------------------------------------------------
	
	private static final String SEPARATOR_HASH="_@_";
	
	public static String getHash(String absolFileName) throws IOException {
		//return getHashMD5File(absolFileName);
		File f = new File(absolFileName);
		//System.out.println(absolFileName+" = "+Long.toString(f.lastModified()));
		return f.getName()+SEPARATOR_HASH+Long.toString(f.lastModified())+SEPARATOR_HASH+Long.toString(f.length());
		
	}
	
	public static String getHash(String location,long time,long size) {
		String[] locs = location.split("/");
		String res;
		if (locs.length>0) 
			res = locs[locs.length-1];
		else
			res = location;
		return res+SEPARATOR_HASH+Long.toString(time)+SEPARATOR_HASH+Long.toString(size);
	}
	
	
	public static String getHashFromRemoteFile(RemoteFile rf) {
		//return getHashMD5File(absolFileName);
		return rf.getName()+SEPARATOR_HASH+Long.toString(rf.getDate().getTime())+SEPARATOR_HASH+Long.toString(rf.getSize());
		
	}
	
	public static String getNameFromHash(String hash) {
		String[] t = hash.split("_@_");
		if (t.length!=3) {
			BiomajLogger.getInstance().log("BiomajUtils::getNameFromHash  ---> error, hash bad structure["+hash+"] has to contain 2x[_@_].");
			return "";
		}
		
		return t[0];	
	}
	
	public static long getSizeFromHash(String hash) {
		String[] t = hash.split("_@_");
		if (t.length!=3) {
			BiomajLogger.getInstance().log("BiomajUtils::getSizeFromHash  ---> error, hash bad structure["+hash+"] has to contain 2x[_@_].");
			return 0;
		}
		
		return Long.valueOf(t[2]);	
	}
	
	public static long getTimeFromHash(String hash) {
		String[] t = hash.split("_@_");
		if (t.length!=3) {
			BiomajLogger.getInstance().log("BiomajUtils::getTimeFromHash  ---> error, hash bad structure["+hash+"] has to contain 2x[_@_].");
			return 0;
		}
		
		return Long.valueOf(t[1]);
		
	}
	
	//----------------------------------------------------------------------------------------------------------------------
	
	public static int createLinkOnFileSystem(File f,String nameLink) throws IOException,InterruptedException {
		
		String cmdline = "ln -s "+f.getAbsolutePath()+" "+getRelativeDirectory(f.getAbsolutePath())+"/"+nameLink;
		Process p = Runtime.getRuntime().exec(cmdline);
		return p.waitFor();
	}
	
	public static boolean move(File source,File destination) throws IOException {
		if( !destination.exists() ) {
			// On essaye avec renameTo
			boolean result = source.renameTo(destination);
			if( !result && !(source.isDirectory() || destination.isDirectory()) ) {
				// On essaye de copier
				copy(source,destination);
				source.delete();
				result = true;
			}
			return result;
		} else {
			// Si le fichier destination existe, on annule ...
			return false;
		} 				
	}
	
	public static boolean moveAllFilesToDirectory(File inDir,File outDir) throws IOException {
		
		if (!outDir.exists()) {
			BiomajLogger.getInstance().log("BiomajUtils::moveFlatFromFuturReleaseToOfflineDirectory out directory does not exist:["+outDir.getAbsolutePath()+"]");
			return false;
		}
		boolean b = true;
		
		if (inDir.exists()) {
			//Dans un premier temps on essaye de faire un move direct si ce sont des repertoires 
			//ca fonctionne si le move s effectue sur le meme disk...
			if (inDir.isDirectory()&&move(inDir,outDir))
				return true;
			File[] files = inDir.listFiles();
			for (File fi : files) {
				if (fi.isFile()) {
					File newFi = new File(outDir.getAbsolutePath()+"/"+fi.getName());
					b = move(fi,newFi) && b ;
				} else if (fi.isDirectory()) {
					File newD = new File(outDir.getAbsolutePath()+"/"+fi.getName());
					if (!newD.exists()) {
						if (!newD.mkdir())
							return false;
					}
					boolean res = moveAllFilesToDirectory(fi,newD);
					if (!res)
						return false;
				}
			}
		}
		
		return b;
	} 
	
	
	public static boolean fillConfig(String dbName, Configuration config) {
		
		try {
			BankFactory bf = new BankFactory();
			BiomajBank b = bf.createBank(dbName,false);
			
			
			/*
			File file = new File(BiomajUtils.getBiomajRootDirectory()+"/"+BiomajConst.propertiesDirectory+"/"+dbName+".properties");
			if (!file.exists())
				return false;
			
			Properties props = new Properties();
			
			FileInputStream fis = new FileInputStream(file);
			props.load(fis);
			fis.close();
			*/
			
			String dataDir = b.getPropertiesFromBankFile().getProperty(BiomajConst.dataDirProperty);
			config.setId(new Long(new Date().getTime()));
			config.setDate(BiomajUtils.getCurrentDate());
			config.setFrequencyUpdate(b.getPropertiesFromBankFile().getProperty(BiomajConst.frequencyProperty));
			config.setFullName(b.getPropertiesFromBankFile().getProperty(BiomajConst.dbFullNameProperty));
			config.setLocalFilesRegexp(b.getPropertiesFromBankFile().getProperty(BiomajConst.localFilesProperty));
			config.setName(dbName);
			config.setNbVersionManagement(b.getPropertiesFromBankFile().getProperty(BiomajConst.keepOldVersionProperty));
			config.setOfflineDirectory(dataDir+"/"+b.getPropertiesFromBankFile().getProperty(BiomajConst.offlineDirProperty));
			
//			config.setPropertyFile(new File(BiomajInformation.getInstance().getProperty(BiomajInformation.WORKFLOWSDIR)+"/"+dbName+".properties").getAbsolutePath());
			config.setPropertyFile(BankFactory.getBankPath(dbName).getAbsolutePath());
			
			
			config.setProtocol(b.getPropertiesFromBankFile().getProperty(BiomajConst.protocolProperty));
			config.setReleaseFile(b.getPropertiesFromBankFile().getProperty(BiomajConst.releaseFileProperty));
			config.setReleaseRegExp(b.getPropertiesFromBankFile().getProperty(BiomajConst.releaseRegExpProperty));
			config.setRemoteDirectory(b.getPropertiesFromBankFile().getProperty(BiomajConst.remoteDirProperty));
			config.setRemoteFilesRegexp(b.getPropertiesFromBankFile().getProperty(BiomajConst.remoteFilesProperty));
			if (b.getPropertiesFromBankFile().containsKey(BiomajConst.typeProperty))
				config.setTypeBank(b.getPropertiesFromBankFile().getProperty(BiomajConst.typeProperty));
			else
				config.setTypeBank("unknown");
			
			config.setUrl(b.getPropertiesFromBankFile().getProperty(BiomajConst.serverProperty));
			config.setVersionDirectory(dataDir+"/"+b.getPropertiesFromBankFile().getProperty(BiomajConst.versionDirProperty));
			//nouvelles donnees depuis 0.9.2
			config.setPort(b.getPropertiesFromBankFile().getProperty(BiomajConst.portProperty));
			config.setUsername(b.getPropertiesFromBankFile().getProperty(BiomajConst.userNameProperty));
			config.setDoLinkCopy(b.getPropertiesFromBankFile().getProperty(BiomajConst.doLinkProperty));
			config.setNoExtract(b.getPropertiesFromBankFile().getProperty(BiomajConst.noExtractProperty));
			config.setLogFiles(b.getPropertiesFromBankFile().getProperty(BiomajConst.logFilesProperty));
			config.setRemoteExcludedFiles(b.getPropertiesFromBankFile().getProperty(BiomajConst.remoteExcludedFilesProperty));
			if (b.getPropertiesFromBankFile().containsKey(BiomajConst.dbFormatsProperty)) {
				String[] dbFormats = b.getPropertiesFromBankFile().getProperty(BiomajConst.dbFormatsProperty).split(",");
				for (String f : dbFormats) 
					config.getFormats().add(f);
			}
			
			/*
			String l = b.getPropertiesFromBankFile().getProperty(BiomajConst.blockPostprocessProperty);
			if (l != null) {
				String[] blocks = l.split(",");
				if (blocks.length >0) {
					Vector<String> bs = new Vector<String>();
					HashMap<String, Vector<String>> lMeta  = new HashMap<String, Vector<String>>();
					HashMap<String, Vector<String>> lProcess  = new HashMap<String, Vector<String>>();
					for (String bl : blocks) {
						bs.add(bl);
						String llMeta =  b.getPropertiesFromBankFile().getProperty(bl);
						if ((llMeta != null)&&(llMeta.trim().compareTo("")!=0)) {
							String[] metas = llMeta.split(",");
							Vector<String> mp = new Vector<String>();
							for (String m : metas) {
								mp.add(m);
								String llProcess =  b.getPropertiesFromBankFile().getProperty(m);
								if ((llProcess != null)&&(llProcess.trim().compareTo("")!=0)) {
									String[] processes = llProcess.split(",");
									Vector<String> pp = new Vector<String>();
									for (String p : processes)
										pp.add(p);
									lProcess.put(m, pp);
								}
							}
							lMeta.put(bl, mp);
						}
					}
					config.setBlocks(bs);
					config.setMetaProcess(lMeta);
					config.setProcess(lProcess);
				}
			}
			 */
			return true;
		} catch (BiomajException be) {
			BiomajLogger.getInstance().log(be);
			return false;
		}
	}
	
	
	/*
	 * METHODES TO USE MD5 HASH
	 * ------------------------
	 * Use fast md5 :http://www.twmacinta.com/myjava/fast_md5.php
	 * Docs : http://www.twmacinta.com/myjava/fast_md5_javadocs/
	 
	 
	 public static String getHashMD5File(String absolFileName) throws IOException {
	 
	 String hash = MD5.asHex(MD5.getHash(new File(absolFileName)));
	 return hash;
	 
	 }
	 
	 
	 public static String getHashMD5String(String stringToHash) throws UnsupportedEncodingException {
	 MD5 md5 = new MD5();
	 md5.Update(stringToHash, null);
	 String hash = md5.asHex();
	 return hash;
	 }
	 */
	                      //bankName,type
	public static TreeMap<String,Vector<String>> getListBankFindInStatefile() throws BiomajException {
		
		TreeMap<String,Vector<String>> res = new TreeMap<String,Vector<String>>();
		
		File statefile = null ;
		try {
		statefile = new File(BiomajInformation.getInstance().getProperty(BiomajInformation.STATEFILESDIR)); 
		} catch (BiomajException ioe) {
			throw new BiomajException(ioe);
		}
		
		if (!statefile.exists())
		{
			BiomajLogger.getInstance().log("directory "+statefile.getAbsolutePath()+" does not exist.");
			return res;
		}
		
		File[] lXml = statefile.listFiles();
		for (File xml : lXml) {
			if (xml.isDirectory())
				continue;
			if (xml.getName().charAt(0)=='.')
				continue;
			if (!xml.getName().endsWith(".xml"))
				continue;
			//on verifie qu il existe dabs conf/db_properties
			String nameBank = xml.getName().split("\\.")[0];
			/*
			try {
			if (!new File(BiomajInformation.getInstance().getProperty(BiomajInformation.WORKFLOWSDIR)+"/"+nameBank+".properties").exists())
				continue;
			} catch (BiomajException io) {
				BiomajLogger.getInstance().log(io);
				continue;
			}*/
			if (BankFactory.getBankPath(nameBank) == null)
				continue;
			
			Configuration config = new Configuration();
			if (!BiomajUtils.fillConfig(nameBank, config)) {
				//System.out.println("Find xml file ["+xml.getAbsolutePath()+"] but no properties file find ["+prop+"]");
				continue;
			}
			if (config.getTypeBank()==null)
				config.setTypeBank("unknown");
			
			if (res.containsKey(config.getTypeBank()))
				res.get(config.getTypeBank()).add(nameBank);
			else {
				res.put(config.getTypeBank(),new Vector<String>());
				res.get(config.getTypeBank()).add(nameBank);
			}
		}
		return res;
	}
	
	/**
	 * 
	 * @param pathDisk
	 * @return
	 * Modif java 1.6
	 */
	public static long getFreeSize() {
		Properties p = new Properties();
		try {
			p.load(new FileInputStream(new File(BiomajInformation.getInstance().getProperty(BiomajInformation.WORKFLOWSDIR)+"/"+BiomajConst.globalProperties)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (BiomajException e) {
			e.printStackTrace();
		}
		String dataDir = p.getProperty(BiomajConst.dataDirProperty);
		File f = new File(dataDir);
		if (f.exists())
			return f.getFreeSpace();
		else
			return 0;
	}
	
	public static long getUseSize(String pathDisk) {
		
		final String PARSE_DF = "(\\S+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)\\S+";
		long size = 0;
		try {
			String[] arg = new String[2];
			arg[0] = "df";
			arg[1] = pathDisk;
			
			Process p = Runtime.getRuntime().exec(arg);
			p.waitFor();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
			//lecture de l entete
			br.readLine();
			String result = br.readLine();
			if (result != null) {
				//System.out.println(result);
				Pattern pt = Pattern.compile(PARSE_DF);
				Matcher m = pt.matcher(result);
				
				if (m.find()) {
					size = Long.valueOf(m.group(3))*1024;
				}
				
			}
			
			
			
		} catch (Exception e) {
			BiomajLogger.getInstance().log(e);
		}
		return size;
		
	}
	
	public static String currentBank (BuildEvent arg0) {
		
		if (arg0.getProject().getProperty(BiomajConst.dbNameProperty)==null)
			throw new BuildException("Internal error biomaj: property db.name not initialized!");
		
		return arg0.getProject().getProperty(BiomajConst.dbNameProperty);
	}
	
	public static String getNameDirectoryCurrentRelease(Project p) throws BiomajBuildException {
		File f = new File(p.getProperty(BiomajConst.dataDirProperty)+"/"+p.getProperty(BiomajConst.versionDirProperty)+"/"+BiomajConst.currentLink);
		
		if (!f.exists())
			throw new BiomajBuildException(p,"error.futur.release.not.exist",f.getAbsolutePath(),new Exception());
		try {
			return f.getCanonicalPath();
		} catch (IOException ioe) {
			throw new BiomajBuildException(p,"io.error",ioe.getMessage(),ioe);
		}
	}
	
	public static String getNameDirectoryFuturRelease(Project p) throws BiomajBuildException {
		File f = new File(p.getProperty(BiomajConst.dataDirProperty)+"/"+p.getProperty(BiomajConst.versionDirProperty)+"/"+BiomajConst.futureReleaseLink);
		
		if (!f.exists())
			throw new BiomajBuildException(p,"error.futur.release.not.exist",f.getAbsolutePath(),new Exception());
		try {
			return f.getCanonicalPath();
		} catch (IOException ioe) {
			throw new BiomajBuildException(p,"io.error",ioe.getMessage(),ioe);
		}
	}
	
	
	/**
	 * Recursively lists the content of the given directory.
	 * Hidden files are not taken into account.
	 * 
	 * @param dir
	 * @param result
	 * @throws BiomajException
	 */
	public static void getListFilesFromDir(String dir,Vector<FileDesc> result) throws BiomajException {
		
		if (result == null)
			return;
		
		File d = new File(dir);
		File[] l = d.listFiles();
		for (File f : l) {
			if (!f.isDirectory()) {
				if (!f.isHidden())
					result.add(new FileDesc(f,false));
			}
			else 
				getListFilesFromDir(f.getAbsolutePath(),result);
		}
		
	}
	
	/**
	 * remove string ".."
	 * @param path
	 */
	public static String getNormalizedPath(String path) {
		
		String[] dirs = path.split("/");
		
		String newValue=null;
		
		for (String dir : dirs) {
			if ((dir.compareTo("..")==0)&&(newValue!=null)) {
				int lastDir = newValue.lastIndexOf("/");
				if (lastDir==-1)
					newValue = null;
				else
					newValue = newValue.substring(0, lastDir);
			} else {
				if (newValue==null)
					newValue = dir;
				else
					newValue = newValue+"/"+dir;
			}
		}
		return newValue;
	}
	
	public static void freeMemory() {
		float percentFree = Runtime.getRuntime().freeMemory()/Runtime.getRuntime().maxMemory();
		 if (percentFree<0.2) {
			 System.out.println("call garbage....");
			 System.out.println("before["+Float.toString(percentFree*100)+"]");
			 Runtime.getRuntime().gc();
			 percentFree = Runtime.getRuntime().freeMemory()/Runtime.getRuntime().maxMemory();
			 System.out.println("next["+Float.toString(percentFree*100)+"]");
		 }
	}


	/**
	 * This function takes a filename from an ftp listing as input and returns
	 * the local filename.  It is used to transform files like gbest120.seq.gz into
	 * gbest120.seq.  It uses the local file pattern set by {@link #setFtpLocalFiles(String) setFtpLocalFiles}
	 * 
	 * @param file A string representing the remote filename.  Multiple patterns can be supplied by
	 * 			   separating them with commas ','.
	 * @return String containing the filename on the local disk.
	 */
	public static String getLocalFileName(String file) {
	
		//Trop de bug avec le math sur le localfiles....
		//simplement on enleve tous les extensions connues de compression;
		String result = file.replace(".gz", "");
		result = result.replace(".Z", "");
		result = result.replace(".gzip", "");
		result = result.replace(".zip", "");
		return result;
	}
	
	public static void changeKey(File propertyFile,String key,String value) throws BiomajException {
		
		if ((propertyFile == null) || (!propertyFile.exists()))
			{
				if (propertyFile != null)
					BiomajLogger.getInstance().log("File ["+propertyFile.getAbsolutePath()+"] does not exist !");
				else
					BiomajLogger.getInstance().log("Bad value for var File [null]");

				BiomajLogger.getInstance().log("key ["+key+"] --> can't be set with ["+value+"]");
				return ;
			}
		try {
			BufferedReader in = new BufferedReader(new FileReader(propertyFile.getAbsolutePath()));
			BufferedWriter out = new BufferedWriter(new FileWriter(propertyFile.getAbsolutePath()+".tmp"));
			String line = null ;
			Pattern patKey = Pattern.compile("^\\s*"+key+"\\s*=\\S+");
			
			while ((line = in.readLine()) != null) {
				Matcher match = patKey.matcher(line);
				if (match.find()) {
					out.write(key+"="+value+"\n");
				} else
					out.write(line+"\n");
			}
			in.close();
			out.close();
			new File (propertyFile.getAbsolutePath()+".tmp").renameTo(propertyFile);
			
		} catch (Exception e) {
			throw new BiomajException(e);
		}
	}
	
	/**
	 * Returns child bank names from the project db.source property.
	 * 
	 * @param p
	 * @return
	 */
	public static String[] getBankDependencies(Project p) {
		if (p.getProperty(BiomajConst.dependsListProperty) != null) {
			String deps = p.getProperty(BiomajConst.dependsListProperty).trim();
			if (!deps.isEmpty()) {
				return deps.split(",");
			}
		}
		return new String[0];
	}
	
	public static String getHexString(byte[] buf) {
		char[] TAB_BYTE_HEX = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
				'9', 'a', 'b', 'c', 'd', 'e', 'f' };

		StringBuilder sb = new StringBuilder(buf.length * 2);

		for (int i = 0; i < buf.length; i++) {
			sb.append(TAB_BYTE_HEX[(buf[i] >>> 4) & 0xf]);
			sb.append(TAB_BYTE_HEX[buf[i] & 0x0f]);
		}
		return sb.toString();
	}
	
}
