/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.utils;

import org.inria.biomaj.singleton.BiomajLogger;

/**
 * @author  ofilangi
 * @version  Biomaj 0.9
 * @since  Biomaj 0.8 /Citrina 0.5
 */
public class BiomajException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private static BiomajMessages bmes = new BiomajMessages();
	/**
	 * @uml.property  name="message"
	 */
	private String message = "";
	
	public BiomajException(String index_mess_property) {
		message = bmes.getMessageFromIndex(index_mess_property);

		if (message == null)
			errorIndexDefinition(index_mess_property);
		else
			BiomajLogger.getInstance().log(message);
		
	}
	
	public BiomajException(String index_mess_property,String[] values) {
		message = bmes.getMessageFromIndex(index_mess_property);

		if (message == null)
			errorIndexDefinition(index_mess_property);
		else
			{
			BiomajLogger.getInstance().log(message = getMessageWithValue(message,values));
			}
	}
	
	public BiomajException(String index_mess_property,String v0) {
		message = bmes.getMessageFromIndex(index_mess_property);

		if (message == null)
			errorIndexDefinition(index_mess_property);
		else
			{
			String[] vs = new String[1];
			vs[0] = v0;
			BiomajLogger.getInstance().log(message = getMessageWithValue(message,vs));
			}
		
	}
	
	public BiomajException(String index_mess_property,String v0, String v1) {
		message = bmes.getMessageFromIndex(index_mess_property);

		if (message == null)
			errorIndexDefinition(index_mess_property);
		else
			{
			String[] vs = new String[2];
			vs[0] = v0;
			vs[1] = v1;
			BiomajLogger.getInstance().log(message = getMessageWithValue(message,vs));
			}
		
	}
	
	public BiomajException(String index_mess_property,String v0, String v1,String v2) {
		message = bmes.getMessageFromIndex(index_mess_property);

		if (message == null)
			errorIndexDefinition(index_mess_property);
		else
			{
			String[] vs = new String[3];
			vs[0] = v0;
			vs[1] = v1;
			vs[2] = v2;
			BiomajLogger.getInstance().log(message = getMessageWithValue(message,vs));
			}
		
	}
	
	public BiomajException(Exception ex) {
		BiomajLogger.getInstance().log(ex);
		System.err.println(ex);
		message = ex.getLocalizedMessage();
	}
	
	public BiomajException(String index_mess_property,Exception ex) {
		message = bmes.getMessageFromIndex(index_mess_property);

		if (message == null)
			errorIndexDefinition(index_mess_property);
		else
			{
			BiomajLogger.getInstance().log(ex);
			BiomajLogger.getInstance().log(message);
			}
	}
	
	private void errorIndexDefinition(String index) {
		BiomajLogger.getInstance().log("******** DEVL ERROR : index message:"+index+" not defined ! *************");
	}
	
	
	protected String getMessageWithValue(String message,String[] v) {
		
		String newMess=message;
		
		for (int i=0;(i<v.length);i++) {
			String valToReplace = "$"+Integer.toString(i) ;
			newMess = newMess.replace(valToReplace,v[i]);
		}
		return newMess;
	}
	
	/**
	 * @return  the message
	 * @uml.property  name="message"
	 */
	@Override
	public String getMessage() {
		return message;
	}
	
}
