package org.inria.biomaj.sql;

import java.sql.Statement;


/**
 * Properly shuts down the hsql server by
 * issuing the sql query "SHUTDOWN".
 * 
 * @author rsabas
 *
 */
public class ShutdownHSQLDB {
	
	public static void main(String[] args) {
		HSQLDBConnection conn = new HSQLDBConnection();
		System.out.println("Shutting down database...");

		Statement stat = conn.getStatement();
		conn.executeUpdate("SHUTDOWN", stat);
		SQLConnectionFactory.closeConnection(stat);	
	}
	
//	/**
//	 * 
//	 * Shutdowns the database and modify the script
//	 * file to modify the tables type (memory or cached)
//	 * for the next startup.
//	 * 
//	 * @param tableType new table type
//	 */
//	private static void shutdownSpecial(String tableType, SQLConnection conn) {
//		String oldType = tableType.equals("MEMORY") ? "CACHED" : "MEMORY";
//		conn.executeQuery("SHUTDOWN SCRIPT", false);
//		try {
//			String path = BiomajUtils.getBiomajRootDirectory() + "/sql";
//			BufferedReader br = new BufferedReader(new FileReader(path + "/biomaj_log.script"));
//			PrintWriter pw = new PrintWriter(new File(path + "/biomaj_log2.script"));
//			String line = "";
//			while ((line = br.readLine()) != null) {
//				pw.println(line.replaceAll("CREATE " + oldType + " TABLE", "CREATE " + tableType + " TABLE"));
//			}
//			pw.close();
//			br.close();
//			new File(path + "/biomaj_log.script").delete();
//			new File(path + "/biomaj_log2.script").renameTo(new File(path + "/biomaj_log.script"));
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
}
