package org.inria.biomaj.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;


/**
 * Interface that provides methods to access a databdase.
 * 
 * @author rsabas
 * 
 */
public interface SQLConnection {
	
	public static final String SQL_DIR = "sql";
	public static final String DB_NAME = "biomaj_log";

	/**
	 * Execute the given (SELECT) query through the given statement (and logs stuff).
	 * 
	 * @param query
	 * @param stat
	 * @return ResultSet that holds the result of the query
	 */
	public ResultSet executeQuery(String query, Statement stat);
	
	
	/**
	 * Execute the given (NON SELECT) query through the given statement (and logs stuff).
	 * 
	 * @param query
	 * @param stat
	 * @return Number of updated lines.
	 */
	public int executeUpdate(String query, Statement stat);
	
	/**
	 * Creates the database.
	 * This method should be called only if you want to reset the database.
	 * 
	 * @param adminLogin 
	 * @param adminPasswd
	 * @param adminMail
	 * 
	 * @return success ?
	 */
	public boolean createDB(String adminLogin, String adminPasswd, String adminMail);
	
	/**
	 * Creates a connection to the db and return a statement object attached to it.
	 * 
	 * @return Created statement
	 */
	public Statement getStatement();
	
	/**
	 * Creates a connection to the db and return a prepared statement object attached to it.
	 * 
	 * @param query parameterized query
	 * @return Created statement
	 */
	public PreparedStatement getPreparedStatement(String query);

	
	/**
	 * Returns the id of the latest inserted row.
	 * 
	 * @return latest id
	 */
	public long getLastInsertedId();

	/**
	 * Execute update statement and return the generated id.
	 * 
	 * @param query
	 * @param stat
	 * @return generated id
	 */
	int executeUpdateAndGetGeneratedKey(String query, Statement stat);

}
