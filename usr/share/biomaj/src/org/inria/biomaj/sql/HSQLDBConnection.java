package org.inria.biomaj.sql;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeSet;
import java.util.UUID;

import org.apache.commons.dbcp.BasicDataSource;
import org.hsqldb.cmdline.SqlFile;
import org.hsqldb.cmdline.SqlToolError;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajUtils;


/**
 * HSQLDB implementation of an SQLConnection. 
 * 
 * @author rsabas
 *
 */
public class HSQLDBConnection implements SQLConnection {
	
	/**
	 * Directory where the database is stored.
	 */
	protected String dbPath;
	
	private String url;
	private String login;
	private String passwd;
	private String driver;
	

	/**
	 * Poolable datasource map that provides the connections.
	 * One datasource per database.
	 */
	private static Map<String, BasicDataSource> dataSources = new HashMap<String, BasicDataSource>();
	
	private static BiomajLogger logger = BiomajLogger.getInstance();
	
	
	public HSQLDBConnection() {
		dbPath = BiomajUtils.getBiomajRootDirectory() + "/" + SQL_DIR;
		initConnection();
//		try {
//			dbPath = BiomajUtils.getBiomajRootDirectory() + "/" + SQL_DIR;
//			Class.forName("org.hsqldb.jdbcDriver").newInstance();
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		} catch (InstantiationException e) {
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			e.printStackTrace();
//		}
	}

	private void initConnection() {
		
		Properties props = SQLConnectionFactory.loadProperties();
		
		url = props.getProperty(BiomajConst.databaseUrl);
		login = props.getProperty(BiomajConst.databaseLogin);
		passwd = props.getProperty(BiomajConst.databasePassword);
		driver = props.getProperty(BiomajConst.databaseDriver);
	}
	
	private Connection getConnection() {
		BasicDataSource bds;
		if ((bds = dataSources.get(url)) == null) {
			bds = new BasicDataSource();
			bds.setDriverClassName(driver);
			bds.setUsername(login);
			bds.setPassword(passwd);
			bds.setUrl(url);
			bds.setValidationQuery("select 1 from INFORMATION_SCHEMA.SYSTEM_USERS");
	
			dataSources.put(url, bds);
		}

		try {
			logger.log("===> " + bds.getNumActive() + " active connections // " + bds.getNumIdle() + " idle connections");
			return bds.getConnection();
		} catch (SQLException e) {
			System.err.println("No connection could be established. Please check that the server is started" +
					" and that the connection parameters are correct.");
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public Statement getStatement() {
		try {
			return getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public PreparedStatement getPreparedStatement(String query) {
		try {
			return getConnection().prepareStatement(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean createDB(String adminLogin, String adminPasswd, String adminMail) {
		
		Statement statement = getStatement();
		try {
			
			TreeSet<String> versions = new TreeSet<String>(); 
			File[] filz = new File(dbPath).listFiles(new FilenameFilter() {
				
				@Override
				public boolean accept(File dir, String name) {
					return name.startsWith("hsql-to");
				}
			});
			
			for (File fl : filz) {
				versions.add(fl.getName()); // sort versions
			}
			
			List<String> runFiles = new ArrayList<String>();
			Collections.addAll(runFiles, new File(dbPath + "/migration").list());
			
			if (!runFiles.contains("hsql.sql")) { // Run all
				System.out.println("Run all sql scripts...");
				/*
				 * Base file
				 */
				File file = new File(dbPath + "/hsql.sql");
				SqlFile sqlFile = new SqlFile(file);
				sqlFile.setConnection(statement.getConnection());
				
				sqlFile.execute();
				
				System.out.println("hsql.sql run.");
				writeFileVersion("hsql.sql");
				
				/*
				 * New versions
				 */
				for (String name : versions) {
					File nf = new File(dbPath + "/" + name);
					sqlFile = new SqlFile(nf);
					sqlFile.setConnection(statement.getConnection());
					sqlFile.execute();
					
					System.out.println(name + " run.");
					writeFileVersion(name);
				}
				
			} else {
				System.out.println("Run new sql scripts...");
				/*
				 * New versions only
				 */
				for (String name : versions) {
					if (!runFiles.contains(name)) {
						File file = new File(dbPath + "/" + name);
						SqlFile sqlFile = new SqlFile(file);
						sqlFile.setConnection(statement.getConnection());
						
						sqlFile.execute();
						
						System.out.println(name + " run.");
						writeFileVersion(name);
					}
				}
			}
			
			/*
			 * Insert admin records
			 */
			
			// Generates admin auth_key
			String key = UUID.randomUUID().toString();
			String hash = "";
			
			// Generates password hash
			MessageDigest digest;
			try {
				digest = MessageDigest.getInstance("SHA1");
				digest.update(adminPasswd.getBytes());
				byte[] hashedPasswd = digest.digest();
				hash = BiomajUtils.getHexString(hashedPasswd);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			
			String testUser = "SELECT * FROM bw_user WHERE login='" + adminLogin + "'";
			ResultSet rs = executeQuery(testUser, statement);
			if (!rs.next()) {
			
				String insertUser = "INSERT INTO bw_user(login, password, auth_type, auth_key, mail_address) " +
						"VALUES('" + adminLogin + "','" + hash + "','local','" + key + "','" + adminMail + "')";
				int userId = executeUpdateAndGetGeneratedKey(insertUser, statement);
				
				String testGroup = "SELECT idgroup FROM bw_group WHERE name='admin'";
				rs = executeQuery(testGroup, statement);
				int groupId;
				if (rs.next()) {
					System.out.println("Adding " + adminLogin + " to current admin group");
					groupId = rs.getInt(1);
				} else {
					String createGroup = "INSERT INTO bw_group(name) VALUES('admin')";
					groupId = executeUpdateAndGetGeneratedKey(createGroup, statement);
				}
				
				String userToGroup = "INSERT INTO bw_user_has_group(ref_iduser, ref_idgroup) VALUES(" + userId + "," + groupId + ")";
				executeUpdate(userToGroup, statement);
				
				
				boolean updated = false;
				try {
					String testTable = "SELECT visibility FROM bank";
					statement.executeQuery(testTable);
					updated = true; // No error, column already added
				} catch (SQLException e) {
					updated = false;
				}
				
				if (!updated) { // Not yet updated
					
					String hsql_addColumnToBank = "ALTER TABLE bank ADD COLUMN ref_iduser INT DEFAULT " + userId;
					executeUpdate(hsql_addColumnToBank, statement);
					
					hsql_addColumnToBank = "ALTER TABLE bank ADD COLUMN visibility BOOLEAN DEFAULT false";
					executeUpdate(hsql_addColumnToBank, statement);
					
					String hsql_addConstraint = "ALTER TABLE bank ADD CONSTRAINT fk_bank_to_user FOREIGN KEY (ref_iduser) REFERENCES bw_user(iduser)";
					executeUpdate(hsql_addConstraint, statement);
				}
				
			} else {
				System.err.println("User " + adminLogin + " already exists");
			}
			
			
			
			SQLConnectionFactory.closeConnection(statement);
//			statement.executeBatch();
		} catch (SQLException e) {
			SQLConnectionFactory.closeConnection(statement);
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			SQLConnectionFactory.closeConnection(statement);
			e.printStackTrace();
			return false;
		} catch (SqlToolError e) {
			SQLConnectionFactory.closeConnection(statement);
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private void writeFileVersion(String name) throws IOException {
		new File (dbPath + "/migration/" + name).createNewFile();
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized long getLastInsertedId() {
		long id = -1;
		Statement st = getStatement();
		try {
			ResultSet rs = executeQuery("CALL identity()", st);
			rs.next();
			id = Long.valueOf(rs.getObject(1).toString());
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			System.err.println("Invalid id : NaN");
		}

		SQLConnectionFactory.closeConnection(st);
		return id;
	}

	@Override
	public ResultSet executeQuery(String query, Statement stat) {
		
		long start = new Date().getTime();
		try {
			ResultSet rs = stat.executeQuery(query);
			logger.log((new Date().getTime() - start) + "ms : " + query);
			return rs;
		} catch (SQLException e) {
			logger.log(e);
			e.printStackTrace();
			if (e.getMessage().contains("Connection is closed"))
				System.exit(1);
		}
		return null;
	}

	@Override
	public int executeUpdate(String query, Statement stat) {
		long start = new Date().getTime();
		try {
			int res = stat.executeUpdate(query);
			logger.log((new Date().getTime() - start) + "ms : " + query);
			return res;
		} catch (SQLException e) {
			logger.log(e);
			e.printStackTrace();
			if (e.getMessage().contains("Connection is closed"))
				System.exit(1);
		}
		return -1;
	}
	
	@Override
	public int executeUpdateAndGetGeneratedKey(String query, Statement stat) {
		long start = new Date().getTime();
		try {
			int res = stat.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
			logger.log((new Date().getTime() - start) + "ms : " + query);
			if (res > 0) {
				ResultSet rs = stat.getGeneratedKeys();
				rs.next();
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			logger.log(e);
			e.printStackTrace();
			if (e.getMessage().contains("Connection is closed"))
				System.exit(1);
		}
		return -1;
	}

}
