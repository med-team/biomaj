package org.inria.biomaj.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajConst;

public class MySQLConnection implements SQLConnection {
	
	private static Map<String, BasicDataSource> dataSources = new HashMap<String, BasicDataSource>();
	private static BiomajLogger logger = BiomajLogger.getInstance();
	
	private String url;
	private String login;
	private String passwd;
	private String driver;
	
	public MySQLConnection() {
		initConnection();
	}

	
	private void initConnection() {
		
		Properties props = SQLConnectionFactory.loadProperties();
		
		url = props.getProperty(BiomajConst.databaseUrl);
		login = props.getProperty(BiomajConst.databaseLogin);
		passwd = props.getProperty(BiomajConst.databasePassword);
		driver = props.getProperty(BiomajConst.databaseDriver);
	}
	
	private Connection getConnection() {
		BasicDataSource bds;
		if ((bds = dataSources.get(url)) == null) {
			bds = new BasicDataSource();
			bds.setDriverClassName(driver);
			bds.setUsername(login);
			bds.setPassword(passwd);
			bds.setUrl(url);
			bds.setValidationQuery("SELECT 1");
			
			dataSources.put(url, bds);
		}

		try {
			logger.log("===> " + bds.getNumActive() + " active connections // " + bds.getNumIdle() + " idle connections");
			return bds.getConnection();
		} catch (SQLException e) {
			System.err.println("No connection could be established. Please check that the server is started" +
					" and that the connection parameters are correct.");
			return null;
		}
	}

	@Override
	public boolean createDB(String adminLogin, String adminPasswd, String adminMail) {
		/*
		 * Creation is left to the user.
		 * The script is available in sql directory.
		 */
		return false;
	}
	
	@Override
	public Statement getStatement() {
		try {
			return getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}	
	}
	
	@Override
	public PreparedStatement getPreparedStatement(String query) {
		try {
			return getConnection().prepareStatement(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public synchronized long getLastInsertedId() {
		long id = 0;
		Statement st = getStatement();
		try {
			ResultSet rs = executeQuery("SELECT LAST_INSERT_ID()", st);
			rs.next();
			id = Long.valueOf(rs.getObject(1).toString());
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			System.err.println("Invalid id : NaN");
		}

		SQLConnectionFactory.closeConnection(st);
		return id;
	}
	
	@Override
	public ResultSet executeQuery(String query, Statement stat) {
		
		long start = new Date().getTime();
		try {
			ResultSet rs = stat.executeQuery(query);
			logger.log((new Date().getTime() - start) + "ms : " + query);
			return rs;
		} catch (SQLException e) {
			logger.log(e);
			e.printStackTrace();
			if (e.getMessage().contains("Connection is closed"))
				System.exit(1);
		}
		return null;
	}

	@Override
	public int executeUpdate(String query, Statement stat) {
		long start = new Date().getTime();
		try {
			int res = stat.executeUpdate(query);
			logger.log((new Date().getTime() - start) + "ms : " + query);
			return res;
		} catch (SQLException e) {
			logger.log(e);
			e.printStackTrace();
			if (e.getMessage().contains("Connection is closed"))
				System.exit(1);
		}
		return -1;
	}


	@Override
	public int executeUpdateAndGetGeneratedKey(String query, Statement stat) {
		long start = new Date().getTime();
		try {
			int res = stat.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
			logger.log((new Date().getTime() - start) + "ms : " + query);
			if (res > 0) {
				ResultSet rs = stat.getGeneratedKeys();
				rs.next();
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			logger.log(e);
			e.printStackTrace();
			if (e.getMessage().contains("Connection is closed"))
				System.exit(1);
		}
		return -1;
	}

}
