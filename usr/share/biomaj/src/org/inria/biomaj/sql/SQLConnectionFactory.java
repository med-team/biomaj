package org.inria.biomaj.sql;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * Factory for the creation of SQL connections to a database.
 * 
 * @author rsabas
 * 
 */
public class SQLConnectionFactory {

	public static final String HSQLDB_CONNECTION = "HSQLDB";
	public static final String MYSQL_CONNECTION = "MYSQL";
	
	private static Properties dbProps = null;
	private static SQLConnection connection = null;
	private static boolean testMode = false;

	/**
	 * Returns a connection to a database as configured
	 * in global.properties file.
	 * 
	 * @return connection
	 */
	public static SQLConnection getConnection() {
		
		if (connection == null) {
			if (testMode)
				connection = new TestConnection();
			else {
				loadProperties();
				
				String type = dbProps.getProperty(BiomajConst.databaseType);
				
				if (type.equalsIgnoreCase(HSQLDB_CONNECTION)) {
					connection = new HSQLDBConnection();
				} else if (type.equalsIgnoreCase(MYSQL_CONNECTION)) 
					connection = new MySQLConnection();
				else
					return null;
			}
		}
		
		return connection;
	}
	
	public static int getDefaultAdminId() {
		if (getDBType().equals(HSQLDB_CONNECTION)) {
			return 0;
		} else {
			return 1;
		}
	}
	
	protected static Properties loadProperties() {
		if (dbProps == null) {
			dbProps = new Properties();
			FileReader fr;
			try {
				fr = new FileReader(BiomajInformation.getInstance().getProperty(BiomajInformation.WORKFLOWSDIR) + "/" + BiomajConst.globalProperties);
				dbProps.load(fr);
				fr.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (BiomajException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return dbProps;
	}
	
	public static String getDBType() {
		
		if (isTestMode()) {
			return HSQLDB_CONNECTION;
		}
		
		Properties props = loadProperties();
		String type = props.getProperty(BiomajConst.databaseType);
		if (type.equalsIgnoreCase(HSQLDB_CONNECTION))
			return HSQLDB_CONNECTION;
		else if (type.equalsIgnoreCase(MYSQL_CONNECTION))
			return MYSQL_CONNECTION;
		else
			return null;
	}
	
	/**
	 * Closes a statement and the connection attached to it.
	 * 
	 * @param st
	 */
	public static void closeConnection(Statement st) {
		try {
			Connection cnt = st.getConnection();
			st.close();
			if (cnt != null && !cnt.isClosed())
				cnt.close();
		} catch (SQLException ex) {
			BiomajLogger.getInstance().log(ex);
			ex.printStackTrace();
		} finally {
			if (st != null)
				st = null;
		}
	}
	
	/**
	 * Returns a connection to a standalone in memory HSQLDB database that is
	 * used for testing purpose.
	 * 
	 * @return
	 */
	public static Connection getTestConnection() {
		Connection cnt = null;
		try {
			cnt = DriverManager.getConnection("jdbc:hsql:mem:testdb", "sa", "");
			Statement statement = cnt.createStatement();
			
			/*
			 * Creation of the database
			 */
			String fileName = BiomajUtils.getBiomajRootDirectory() + "/sql/hsql.sql";
			try {
				BufferedReader br = new BufferedReader(new FileReader(fileName));
				String line = "";
				StringBuilder sb = new StringBuilder();
				while ((line = br.readLine()) != null) {
					if (!line.trim().isEmpty() && !line.startsWith("--")) {
						sb.append(line);
						if (line.endsWith(";")) {
							statement.executeUpdate(sb.toString());
							sb = new StringBuilder();
						}
					}
				}
				br.close();
				statement.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} catch (SQLException e) {
			System.err.println("Could not establish connection to test database.");
			e.printStackTrace();
		}
		
		return cnt;
	}
	
	public static boolean isTestMode() {
		return testMode;
	}
	
	public static void setTestMode(boolean t) {
		testMode = t;
	}
}
