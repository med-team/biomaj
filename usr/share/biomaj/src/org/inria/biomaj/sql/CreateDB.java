package org.inria.biomaj.sql;

import org.inria.biomaj.sql.SQLConnection;


/**
 * Class that creates the database.
 * 
 * @author rsabas
 *
 */
public class CreateDB {
	
	public static void main(String[] args) {
		if (args.length != 3) {
			System.err.println("SYNTAX : CreateDB <admin_login> <admin_password> <admin_mail>");
			System.exit(1);
		}
		SQLConnection conn = SQLConnectionFactory.getConnection();
		conn.createDB(args[0], args[1], args[2]);
	}

}
