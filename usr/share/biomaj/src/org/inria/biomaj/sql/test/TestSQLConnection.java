package org.inria.biomaj.sql.test;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.inria.biomaj.sql.SQLConnection;
import org.inria.biomaj.sql.SQLConnectionFactory;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test class for the database connectivity.
 * 
 * @author rsabas
 *
 */
public class TestSQLConnection {
	
	private static SQLConnection toTest;
	private static List<String> _tables = null;
	
	@BeforeClass
	public static void connect() {
		toTest = SQLConnectionFactory.getConnection();
		
		_tables = new ArrayList<String>();
		_tables.add("bank");
		_tables.add("configuration");
		_tables.add("remoteinfo");
		_tables.add("localinfo");
		_tables.add("updatebank");
		_tables.add("session");
		_tables.add("sessiontask");
		_tables.add("session_has_sessiontask");
		_tables.add("session_has_message");
		_tables.add("sessiontask_has_message");
		_tables.add("sessiontask_has_file");
		_tables.add("metaprocess");
		_tables.add("process");
		_tables.add("file");
		_tables.add("message");
		_tables.add("productiondirectory");
		_tables.add("metaprocess_has_message");
		_tables.add("process_has_message");
	}
	
	private void testCreateDb() {
		try {
			/*
			 * Checking how many tables have been created 
			 */
			Connection conn = toTest.getStatement().getConnection();
			DatabaseMetaData meta = conn.getMetaData();

			// All tables
			ResultSet tables = meta.getTables(conn.getCatalog(), null, "%", null);
			int total = 0; 
			while (tables.next()) {
				if (_tables.contains(tables.getString(3).toLowerCase()))
					total++;
			}
			
			// User tables = all - system
			assertEquals(18, total);
			
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void runTests() {
		testCreateDb();
		String testMessage = "test message";
		int testType = 12;
		long id = 0;
		
		/*
		 * INSERT of 1 row
		 */
		String query = "INSERT INTO message(message, type) values('" + testMessage + "', " + testType + ");";
		Statement stat = toTest.getStatement();
		id = toTest.executeUpdateAndGetGeneratedKey(query, stat);

		/*
		 * Retrieving the inserted row
		 */
		query = "SELECT * from message where idmessage=" + id;
		
		try {
			ResultSet result = toTest.executeQuery(query, stat);
			assertTrue(result.next());
			assertEquals(testMessage, result.getString(2)); // Same message
			assertEquals(testType, result.getInt(3)); // Same type
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		/*
		 * Deleting the inserted row
		 */
		query = "DELETE FROM message where idmessage = " + id + ";";
		assertEquals(1, toTest.executeUpdate(query, stat)); // 1 row deleted
		

		SQLConnectionFactory.closeConnection(stat);
	}

}
