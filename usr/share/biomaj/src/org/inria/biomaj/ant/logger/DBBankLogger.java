/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.logger;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.tools.ant.BuildEvent;
import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.task.BmajTask;
import org.inria.biomaj.ant.task.BmajVersionManagement;
import org.inria.biomaj.session.bank.Bank;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.CheckTask;
import org.inria.biomaj.session.bank.Configuration;
import org.inria.biomaj.session.bank.DownloadTask;
import org.inria.biomaj.session.bank.ExtractTask;
import org.inria.biomaj.session.bank.FileDesc;
import org.inria.biomaj.session.bank.GeneralWorkflowTask;
import org.inria.biomaj.session.bank.PostProcessTask;
import org.inria.biomaj.session.bank.PreProcessTask;
import org.inria.biomaj.session.bank.ProductionDirectory;
import org.inria.biomaj.session.bank.ReleaseTask;
import org.inria.biomaj.session.bank.RemoveProcessTask;
import org.inria.biomaj.session.bank.Session;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.singleton.BiomajSession;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * 
 * @author  ofilangi
 */
public class DBBankLogger {

	protected Bank bank ;
	/**
	 * @uml.property  name="generalXml"
	 * @uml.associationEnd  
	 */

	protected String listPostProcess;
	
	/**
	 * @uml.property  name="listPreProcess"
	 */
	protected String listPreProcess ;
	
	/**
	 * @uml.property  name="listRemoveProcess"
	 */
	protected String listRemoveProcess ;
	
	private int idCheckTask = -1;

	public void beginSession(BuildEvent arg0) throws BiomajException {

		//On recharge le contexte si biomaj est execute avec un point d'arret en ligne de commande
//		boolean isLoad = initContext(arg0,BiomajUtils.currentBank(arg0));
		initContext(arg0,BiomajUtils.currentBank(arg0));
		/*
		generalXml.updateBankDescription(arg0.getProject(),currentBank(arg0),
				arg0.getProject().getProperty(BiomajConst.dbFullNameProperty),
				BiomajUtils.getCurrentDate());
	*/
		listPostProcess = arg0.getProject().getProperty(BiomajConst.dbPostProcessProperty);
		listPreProcess = arg0.getProject().getProperty(BiomajConst.dbPreProcessProperty);
		listRemoveProcess = arg0.getProject().getProperty(BiomajConst.dbRemoveProcessProperty);

		DBWriter.updateStateSession(bank,true);
	}

	public void endSession(BuildEvent arg0) throws BiomajException {
		if (bank == null)
			return;
		
		bank.setEnd(new Date());
		GeneralWorkflowTask g = bank.getCurrentSession().getLastTask();
		
		if (bank.getErrorOnWorkflow()) {
			if (g != null)
				g.setStatus(GeneralWorkflowTask.STATUS_KO);
		}/* else {
			/*
			 * Ca releve du patch....
			 * 
			 * Il faut encapsuler le traitement entier du deploiement pour un cas d erreur.
			 * Pour l instant, on effectue les traitement du deploiement seulemenent si cette tache c est bien deroule.
			 */
/*			if (g instanceof DeploymentTask) {
				actionDeployement(arg0);	
			}
		}*/
		
//		if (bank.isRebuilt() && cleanMeta)
//			bank.setUpdate(false);
		
		DBWriter.updateStateSession(bank,false);
		if (bank.isRebuilt())
			DBWriter.deleteMetaprocesses();
	}

	public void workflowControlStarted(BuildEvent arg0) throws BiomajException {

	}

	public void workflowControlFinished(BuildEvent arg0) throws BiomajException{

	}


	public void mirrorFinnished(BuildEvent arg0) throws BiomajException {
	}


	/*
	 * methods called when end or start event target are caught
	 * 
	 */

	public void mirrorStartedDownloadTarget(BuildEvent arg0) throws BiomajException {
		bank.setStartProcess(BiomajConst.downloadTarget);
		DBWriter.updateStateSession(bank,false);
	}
	
	public void addFileInProduction(BuildEvent arg0, String message) throws BiomajException {
		//mode location hash
		Pattern p = Pattern.compile(BmajTask.filterAddProductionFile+BmajTask.regExpAddProductionFile);
		Matcher m = p.matcher(message);

		if (!m.find()) {
			BiomajLogger.getInstance().log("Can't add production file (can't parse:"+message+")");
			return;
		}

		String location = m.group(1);
		boolean copy = Boolean.valueOf(m.group(2));
		String refHash = ((ExtractTask) bank.getCurrentSession().getWorkflowTask(Session.EXTRACT)).getFileRefHash(location);
		bank.addFilesInProduction(location, refHash, copy);
	}



	public void mirrorFinishedDownloadTarget(BuildEvent arg0) throws BiomajException {		
			DownloadTask dt = (DownloadTask)bank.setEndProcess(BiomajConst.downloadTarget);
			
			String val = arg0.getProject().getProperty("download.needed");
			
			if (Boolean.valueOf(val)) {
				
				double timeInSec = ((double)(dt.getEnd().getTime() - dt.getStart().getTime()))/(double)(1000);
				double sizeInMo  = (double)(dt.getSizeDownloaded())/(double)(1024*1024); 
				
				//System.out.println("down size (Mo):"+Double.toString(sizeInMo));
				//System.out.println("time(s):"+Double.toString(timeInSec));
				
				dt.setBandWidth(sizeInMo/timeInSec);
				
				bank.setWorkflowInfoSizeDownload(dt.getSizeDownloaded());
			}
			
			DBWriter.updateStateSession(bank,false);
	}


	public void mirrorStartedExtractTarget(BuildEvent arg0) throws BiomajException {
		bank.setStartProcess(BiomajConst.extractTarget);
		DBWriter.updateStateSession(bank,false);
	}


	public void mirrorFinishedExtractTarget(BuildEvent arg0) throws BiomajException {
		bank.setEndProcess(BiomajConst.extractTarget);
		DBWriter.updateStateSession(bank,false);
	}


	public void mirrorStartedCheckTarget(BuildEvent arg0) throws BiomajException {
		bank.setStartProcess(BiomajConst.checkTarget);
		DBWriter.updateStateSession(bank,false);
	}


	public void mirrorFinishedCheckTarget(BuildEvent arg0) throws BiomajException {
		bank.setEndProcess(BiomajConst.checkTarget);
		CheckTask ct = (CheckTask)bank.getProcessRuntime(BiomajConst.checkTarget);
		ct.setNbFilesExtract(Integer.valueOf(arg0.getProject().getProperty(BiomajConst.countExtractProperty)));
		ct.setNbFilesDownload(Integer.valueOf(arg0.getProject().getProperty(BiomajConst.countDownloadProperty)));
		ct.setNbFilesLocalOnline(Integer.valueOf(arg0.getProject().getProperty(BiomajConst.countLocalOnlineFileProperty)));
		ct.setNbFilesLocalOffline(Integer.valueOf(arg0.getProject().getProperty(BiomajConst.countLocalOfflineFileProperty)));
		
		boolean fileCopyNeed =  Boolean.valueOf(arg0.getProject().getProperty(BiomajConst.filesCopyNeedDynamicProperty));
		if (arg0.getProject().getProperty("isComputedAndEmpty") != null &&
				arg0.getProject().getProperty("isComputedAndEmpty").equals("true") &&
				arg0.getProject().getProperty("children.updated") != null &&
				arg0.getProject().getProperty("children.updated").equals("true")) {
			bank.setUpdate(true);
		} else {
			bank.setUpdate(fileCopyNeed || (ct.getNbFilesDownload()>0) || (ct.getNbFilesLocalOffline()>0));
		}
		
		DBWriter.updateStateSession(bank,false);
	}


	public void mirrorStartedCopyTarget(BuildEvent arg0) throws BiomajException {
		bank.setStartProcess(BiomajConst.copyTarget);
		DBWriter.updateStateSession(bank,false);
	}

	public void mirrorFinishedCopyTarget(BuildEvent arg0) throws BiomajException {
		bank.setEndProcess(BiomajConst.copyTarget);
		DBWriter.updateStateSession(bank,false);
	}

	public void mirrorStartedMoveTarget(BuildEvent arg0) throws BiomajException {
		
		GeneralWorkflowTask task = null;
		if ((task = bank.getCurrentSession().getWorkflowTask(Session.COPY)) == null) {
			Collections.sort(bank.getListOldSession());
			int pos = bank.getListOldSession().size() - 1;
			while (pos >= 0 && (task = bank.getListOldSession().get(pos).getWorkflowTask(Session.COPY)) == null)
				pos--;
		}
		if (task == null)
			throw new BiomajException("No copy task could be found in previous sessions.");
		
		int taskId = task.getTaskId();
		arg0.getProject().setProperty("taskId", String.valueOf(taskId));
		
		bank.setStartProcess(BiomajConst.moveTarget);
		DBWriter.updateStateSession(bank,false);
	}

	public void mirrorFinishedMoveTarget(BuildEvent arg0) throws BiomajException {
		bank.setEndProcess(BiomajConst.moveTarget);
		if (arg0.getProject().getProperties().containsKey(BiomajConst.offlineHasFilesDynamicProperty)) {
			if (Boolean.valueOf(arg0.getProject().getProperty(BiomajConst.offlineHasFilesDynamicProperty))) {
				bank.setOnlineDirectory(BiomajUtils.getNameDirectoryFuturRelease(arg0.getProject()));
				bank.setWorkflowInfoIsDeployed(false);
			}
		}
		DBWriter.updateStateSession(bank,false);
//		DBWriter.createBackup(bank.getConfig().getName());
	}	
	
	public void mirrorStartedDeploymentTarget(BuildEvent arg0) throws BiomajException {
		bank.setStartProcess(BiomajConst.deployTarget);
		DBWriter.updateStateSession(bank,false);
		
	}

	public void mirrorFinishedDeploymentTarget(BuildEvent arg0) throws BiomajException {
		bank.setEndProcess(BiomajConst.deployTarget);
		actionDeployement(arg0);
	}
	
	public void forceDeploymentIfNeeded(BuildEvent buildEvent) throws BiomajException {
		if (BiomajSQLQuerier.getAvailableProductionDirectories(bank.getConfig().getName()).size() == 0) {
			Project project = buildEvent.getProject();
			project.log("No production directory was found in the db. Deployment will be forced with current one.");
//			bank.setWorkflowInfoProductionDir(workflowInfoProductionDir)
			File current = new File(project.getProperty("data.dir") + "/" + project.getProperty("dir.version") + "/current");
			if (current.exists()) {
				try {
					String path = current.getCanonicalPath();
					bank.setWorkflowInfoProductionDir(path);
					actionDeployement(buildEvent);
				} catch (IOException e) {
					project.log(e.getMessage(), Project.MSG_ERR);
				}
			} else {
				System.out.println(current.getAbsolutePath() + " doesnt exist");
			}
		}
	}

	public void actionDeployement(BuildEvent arg0) throws BiomajException {
		bank.setWorkflowInfoIsDeployed(true);
		String computedAndEmpty = arg0.getProject().getProperty("isComputedAndEmpty");
		if (computedAndEmpty != null && computedAndEmpty.equals("true"))
			bank.setWorkflowInfoProductionDir(BiomajUtils.getNameDirectoryCurrentRelease(arg0.getProject()));
		bank.addProductionDirectory(bank.getWorkflowInfoProductionDir());
		DBWriter.updateStateSession(bank,false);
		DBWriter.updateStateSessionWithProductionDir(bank);
		
		Vector<ProductionDirectory> pdl = bank.getBankStateListProductionDirectories();
		if (pdl.size()>=2) {
			if (pdl.get(pdl.size()-1).getSize()<pdl.get(pdl.size()-2).getSize())
				arg0.getProject().log("New production directory is smaller than the previous one [new size:"+
						Long.toString(pdl.get(pdl.size()-1).getSize())+"] [old size:"+
						Long.toString(pdl.get(pdl.size()-2).getSize())+"]",Project.MSG_WARN);
		}
	}
	
	public void versionsmanagementFinishedTask(BuildEvent arg0) throws BiomajException {
		
		String mode = arg0.getProject().getProperty(BmajVersionManagement.MODE);
		String directories = arg0.getProject().getProperty(BmajVersionManagement.RESULTS);

		arg0.getProject().log("MODE:"+mode+" RESULTS:"+directories,Project.MSG_DEBUG);
		if ((BmajVersionManagement.DELETE.compareTo(mode)==0)||(BmajVersionManagement.REBUILD.compareTo(mode)==0)) {
			String[] dirs = directories.split(",");
			for (int i=0;i<dirs.length;i++) {
				bank.removeProductionDirectory(dirs[i]);
			}
			DBWriter.updateStateSessionWithProductionDir(bank);
		}
	}

	/**
	 * INTERFACE TASKS METHODS
	 */

	public void mirrorStartedFileCheckTask(BuildEvent arg0) throws BiomajException {
		bank.setStartProcess(BiomajConst.checkTarget);
		DBWriter.updateStateSession(bank,false);
	}

	public void mirrorFinishedFileCheckTask(BuildEvent arg0) throws BiomajException {
		bank.setEndProcess(BiomajConst.checkTarget);
		DBWriter.updateStateSession(bank,false);
	}


	public void mirrorFinishedGetReleaseTask(BuildEvent arg0) throws BiomajException {
		String release = arg0.getProject().getProperty(BiomajConst.releaseResultProperty);
		bank.setRelease(release);
		ReleaseTask proc = (ReleaseTask) bank.setEndProcess(BiomajConst.releaseTarget);
		proc.setValue(release);
		DBWriter.updateStateSession(bank,false);
	}

	public void mirrorStartedGetReleaseTask(BuildEvent arg0) throws BiomajException {
		bank.setStartProcess(BiomajConst.releaseTarget);
		DBWriter.updateStateSession(bank,false);
	}

	String getProperty(BuildEvent arg0, String property) {

		//This listener handle only the mirror.xml
		if (arg0.getProject().getName().compareTo(BiomajConst.mirrorProject)==0)
		{
			return arg0.getProject().getProperty(property);
		}

		return BiomajConst.noMirrorContext;
	}

	/**
	 * Load a context find in xml file. return true if context is loaded!
	 * @param dbName
	 */

	private boolean initContext(BuildEvent arg0,String dbName) {
		try {
			//String propFile = BiomajUtils.getProperty(arg0.getProject(),BiomajConst.propertiesDirectoryProperty)+"/"+dbName+".properties";

			Configuration config = new Configuration();
			if (!BiomajUtils.fillConfig(dbName, config))
				throw new BiomajBuildException(arg0.getProject(), new Exception("Can't create bank. Check the bank properties."));

			Bank xbd = new Bank();
			xbd.setConfig(config);

			/**
			 * Test and restore of backup if a read problem occurs
			 */

			restoreDatabase();

			xbd.setBankStateListProductionDirectories(BiomajSQLQuerier.getAllProductionDirectories(dbName));
			
			bank = xbd;

			//Chargement du context si biomaj a ete lance avec une ligne de commande point d arret!
			boolean newupdate = Boolean.valueOf(arg0.getProject().getProperty(BiomajConst.newUpdateProperty));

			/****
			 * Test si il y a un current directory et pas de session avec update
			 */
//			Bank test = new Bank();
//			boolean notFirstSession = BiomajQueryXmlStateFile.getLastUpdateBankWithNewRelease(dbName,test,false);
			
			if (BiomajSQLQuerier.getLatestUpdateWithProductionDirectory(dbName) == null) {
				String dataD = arg0.getProject().getProperty(BiomajConst.dataDirProperty);
				File f = new File (dataD+"/"+config.getVersionDirectory()+"/"+BiomajConst.currentLink+"/flat");

				if (f.exists()) {
					
					arg0.getProject().log("directory ["+f.getAbsolutePath()+"] exist in version directory.",Project.MSG_ERR);
					arg0.getProject().log("Run biomaj with import option to handle a local bank with biomaj.",Project.MSG_ERR);
				//	arg0.getProject().log("Move all files from this directory to "+dataD+"/"+config.getOfflineDirectory()+" to archives them in a new version.",Project.MSG_ERR);
					throw new BiomajBuildException(arg0.getProject(),"kill.application.withoutlog",new Exception());
				}
			}
			bank.setStart(new Date());
			bank.fill(BiomajSQLQuerier.getLatestUpdate(dbName, false), true);
			
			boolean newConfig = DBWriter.thereIsANewConfig(xbd);
			
//			Debut d'une nouvelle session
			bank.setEnd(null);

			if (newConfig || newupdate || bank.needANewCycleUpdate()) {
				//New update!
				bank = new Bank();
				bank.setStart(new Date());
				bank.setConfig(config);
				bank.setBankStateListProductionDirectories(BiomajSQLQuerier.getAllProductionDirectories(dbName));
				BiomajSession.getInstance().addBank(dbName, bank);
				return false;
			} else {
				BiomajSession.getInstance().addBank(dbName, bank);
				return true;
			}
		} catch (BiomajException e) {
			throw new BiomajBuildException(arg0.getProject(),e);
		}
	}

	private void restoreDatabase() {

	}

	/**
	 * get the object associated with the bank to fill a bank's description
	 * @param dbName
	 * @return
	 */
//	Bank getXmlDescriptionBank() {
//		return bank;
//	}

	public void errorMessage(String message, BuildEvent arg0) throws BiomajException {
		//can't log error before target workflow_control
		if (bank==null)
			return;
		String nameTarget = "";
		if (arg0.getTarget()!=null)
			nameTarget = arg0.getTarget().getName();

		bank.addErrProcess(nameTarget, message);

		DBWriter.updateStateSession(bank,false);
	}

	public void warnMessage(String message, BuildEvent arg0) throws BiomajException {
//		can't log warning before target workflow_control
		if (bank==null)
			return;

		String nameTarget = "";
		if (arg0.getTarget()!=null)
			nameTarget = arg0.getTarget().getName();

		bank.addWarnProcess(nameTarget, message);
		DBWriter.updateStateSession(bank,false);
	}

	public void treatmentWhenFail(BuildEvent arg0) throws BiomajException {
		DBWriter.updateStateSession(bank,false);
	}
	

	public void addLocalOfflineFile(BuildEvent arg0, String message) throws BiomajException {
		Pattern p = Pattern.compile(BmajTask.filterAddLocalOfflineFile+BmajTask.regExpAddLocalOfflineFile);
		Matcher m = p.matcher(message);

		if (!m.find()) {
			BiomajLogger.getInstance().log("Can't add local offline file (can't parse:"+message+")");
			return;
		}
		
		FileDesc toAdd = bank.addLocalOfflineFile(m.group(1));
		
		if (idCheckTask < 0) {
			DBWriter.updateStateSession(bank,false);
			long sessionId = bank.getCurrentSession().getId();
			List<Map<String, String>> tasks = BiomajSQLQuerier.getSessionTasks(sessionId);
			for (Map<String, String> task : tasks)
				if (task.get(BiomajSQLQuerier.TASK_TYPE).equals(BiomajConst.checkTag)) {
					idCheckTask = Integer.valueOf(task.get(BiomajSQLQuerier.TASK_ID));
					break;
				}
		} else {
			DBWriter.addFileToTask(toAdd, idCheckTask, "file");
		}
	}

	public void addLocalOnlineFile(BuildEvent arg0, String message) throws BiomajException {
		Pattern p = Pattern.compile(BmajTask.filterAddLocalOnlineFile+BmajTask.regExpAddLocalOnlineFile);
		Matcher m = p.matcher(message);

		if (!m.find())
		{
			BiomajLogger.getInstance().log("Can't add local online file (can't parse:"+message+")");
			return;
		}

		bank.addLocalOnlineFile(m.group(1));
		DBWriter.updateStateSession(bank,false);
	}

	
	public void addDownloadFile(BuildEvent arg0,String message) throws BiomajException {

		Pattern p = Pattern.compile(BmajTask.filterAddDownloadedFile+BmajTask.regExpAddDownloadedFile);
		Matcher m = p.matcher(message);

		if (!m.find())
		{
			BiomajLogger.getInstance().log("Can't add download file (can't parse:"+message+")");
			return;
		}
		
		bank.getCurrentSession().setActiveTask(bank.getCurrentSession().getWorkflowTask(Session.DOWNLOAD));
		bank.addDownloadFile(m.group(1), Long.valueOf(m.group(2)), Long.valueOf(m.group(3)));
		DBWriter.updateStateSession(bank,false);
		bank.getCurrentSession().setActiveTask(null);
	}

	public void addExtractedFile(BuildEvent arg0, String message) throws BiomajException {
		//1er groupe nom compresse, 2eme hash fichier compresse,3eme groupe nom de l extract,4eme hash du fichier decompresse
		Pattern p = Pattern.compile(BmajTask.filterAddExtractedFile+BmajTask.regExpAddExtractedFile);
		Matcher m = p.matcher(message);

		if (!m.find()) {
			BiomajLogger.getInstance().log("Can't add extraction file (can't parse:"+message+")");
			return;
		}
		
		String fileUncompressed = m.group(1);
		String refHash = m.group(2);
		
		bank.addFilesInExtraction(fileUncompressed,refHash);
//		DBWriter.updateStateSession(bank,false);
	}

	public void mirrorStartedPostProcessTarget(BuildEvent arg0) throws BiomajException {
		bank.setStartProcess(BiomajConst.postprocessTarget);
		DBWriter.updateStateSession(bank,false);
	}

	public void handleOfflineEndPostProcess(Project p) throws BiomajException {
		if (bank.getProcessRuntime(BiomajConst.postprocessTarget)!=null) {
			bank.setEndProcess(BiomajConst.postprocessTarget);
			DBWriter.updateStateSession(bank,false);
		}
	}

	public void handleOfflineEndPreProcess(Project p) throws BiomajException {
		if (bank.getProcessRuntime(BiomajConst.preprocessTarget)!=null) {
			bank.setEndProcess(BiomajConst.preprocessTarget);
			DBWriter.updateStateSession(bank,false);
		}

	}
	
	public void handleOfflineEndRemoveProcess(Project p) throws BiomajException {
		if (bank.getProcessRuntime(BiomajConst.removeprocessTarget)!=null) {
			bank.setEndProcess(BiomajConst.removeprocessTarget);
			DBWriter.updateStateSession(bank,false);
		}

	}

	public void handleOfflineStartPostProcess(Project pj,String nameProcess,String block) throws BiomajException {
		if (bank.getProcessRuntime(BiomajConst.postprocessTarget)==null)
			bank.setStartProcess(BiomajConst.postprocessTarget);

		PostProcessTask p = (PostProcessTask)bank.getProcessRuntime(BiomajConst.postprocessTarget);
		if (nameProcess.compareTo("")!=0)
			p.addMetaProcess(nameProcess,block);
			
		DBWriter.updateStateSession(bank,false);
	}

	public void handleOfflineStartPreProcess(Project pj,String nameMetaProcess) throws BiomajException {
		if (bank.getProcessRuntime(BiomajConst.preprocessTarget)==null)
			bank.setStartProcess(BiomajConst.preprocessTarget);
		PreProcessTask p = (PreProcessTask)bank.getProcessRuntime(BiomajConst.preprocessTarget);
		if (nameMetaProcess.compareTo("")!=0)
			p.addMetaProcess(nameMetaProcess,"");
		DBWriter.updateStateSession(bank,false);
	}

	public void handleOfflineStartRemoveProcess(Project pj,String nameProcess,String block) throws BiomajException {
		
		if (bank.getProcessRuntime(BiomajConst.removeprocessTarget)==null)
			bank.setStartProcess(BiomajConst.removeprocessTarget);

		RemoveProcessTask p = (RemoveProcessTask)bank.getProcessRuntime(BiomajConst.removeprocessTarget);
		if (nameProcess.compareTo("")!=0)
			p.addMetaProcess(nameProcess,block);

//		bank.setStart(new Date());
		
		DBWriter.updateStateSession(bank,false);
	}
	
	public Bank getBank() {
		return bank;
	}

}
