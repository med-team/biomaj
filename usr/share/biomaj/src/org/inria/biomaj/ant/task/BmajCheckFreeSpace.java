package org.inria.biomaj.ant.task;

import java.io.File;
import java.util.List;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.ProductionDirectory;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;

/**
 * <p>This task is instanciate in mirror.xml with the name : bmaj-addlocalfile.<br/>
 * check biomaj_common.xml to see the map of task.
 * </p>
 * <p>
 * Throw an exception if there are not enough space disk.
 * 
 *  2 criteria 
 *  <ul>
 *  <li>1) if a prouction directory exist, we compare the free space disk with the size of directory.</li>
 *  <li>2) we compute the total size of file to download and we check the space disk. </li>
 * </ul>
 * </p>
 * @author ofilangi
 * @version Biomaj 0.9
 * @since Biomaj 0.8 /Citrina 0.5
 */
public class BmajCheckFreeSpace extends BmajTask {

	@Override
	public void execute() throws BuildException {

		File prod = new File(getProject().getProperty(BiomajConst.dataDirProperty));
		long freeSpace = prod.getFreeSpace();
		
		ProductionDirectory prodDirectory ;
		List<ProductionDirectory> prodDirectories = BiomajSQLQuerier.getAvailableProductionDirectories(getProject().getProperty(BiomajConst.dbNameProperty));
		
		if (prodDirectories.size()>0) {
			prodDirectory = prodDirectories.get(prodDirectories.size()-1);

			File prodBank = new File(prodDirectory.getPath());

			if (prodBank.exists()) {
				long lSizeProd = prodDirectory.getSize();
				
				if (freeSpace<lSizeProd)
					throw new BiomajBuildException(getProject(),"checkfreespace.error",Long.toString(freeSpace),Long.toString(lSizeProd),null);

				log("space ok freespace:"+Long.toString(freeSpace)+" - release["+prodDirectory.getPath()+"]:"+lSizeProd,Project.MSG_VERBOSE);
				return ;
			}
		}
		
		//2eme crite de comparaison, la taille globale a ete calcule durant le check et mis dna sle fichier update.properties (repertoire runtime) !
		if (!getProject().getProperties().containsKey("remote.files.size"))
			throw new BiomajBuildException(getProject(),"unknown.error","Please relaunche BioMAJ with option -N.",null);
		
		long sizeTotal = Long.valueOf(getProject().getProperty("remote.files.size"));
		
		if (freeSpace<sizeTotal)
			throw new BiomajBuildException(getProject(),"checkfreespace.error",Long.toString(freeSpace),Long.toString(sizeTotal),null);
		
		log("space ok freespace:"+Long.toString(freeSpace)+" - size to download:"+sizeTotal,Project.MSG_VERBOSE);
		
	}

}
