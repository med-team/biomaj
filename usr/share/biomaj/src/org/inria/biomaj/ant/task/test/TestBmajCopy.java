package org.inria.biomaj.ant.task.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static junit.framework.Assert.assertTrue;

import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.task.BmajCopy;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test class for BmajCopy.
 * 
 * @author rsabas
 *
 */
public class TestBmajCopy {
	
	private static final String FROM_DIR = BiomajUtils.getBiomajRootDirectory() + "/testfrom";
	private static final String TO_DIR = BiomajUtils.getBiomajRootDirectory() + "/testto";
	private static final String LISTING_FILE = BiomajUtils.getBiomajRootDirectory() + "/testlisting";
	private static List<File> filz = new ArrayList<File>();
	
	
	/**
	 * Creates files and directories to copy.
	 */
	@BeforeClass
	public static void setup() {
		File from = new File(FROM_DIR);
		File to = new File(TO_DIR);
		File list = new File(LISTING_FILE);
		
		BiomajUtils.deleteAll(from);
		BiomajUtils.deleteAll(to);
		list.delete();
		
		from.mkdir();
		populateFromDir();
		createListing(list, filz);
	}
	
	/**
	 * Write the file listing to be copied
	 * 
	 * @param list file name
	 * @param filz file list to write
	 */
	private static void createListing(File list, List<File> filz) {
		PrintWriter pw;
		try {
			pw = new PrintWriter(list);
			for (File f : filz) {
				pw.println("name=" + f.getName() +
						",base=" + "" +
						",link=" + "" +
						",date=" + BiomajUtils.dateToString(new Date(), Locale.US) +
						",size=" + f.length() +
						",isDir=" + f.isDirectory());
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private static void populateFromDir() {
		File f1 = new File(FROM_DIR + "/test1");
		File f2 = new File(FROM_DIR + "/test2");
		File f3 = new File(FROM_DIR + "/test3");
		try {
			f1.createNewFile();
			f2.createNewFile();
			f3.mkdir();
			
			filz.add(f1);
			filz.add(f2);
			filz.add(f3);
			
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	
	@AfterClass
	public static void cleanup() {
		BiomajUtils.deleteAll(new File(FROM_DIR));
		BiomajUtils.deleteAll(new File(TO_DIR));
		new File(LISTING_FILE).delete();
	}

	/**
	 * Executes task and checks that files have been
	 * copied but not directories.
	 */
	@Test
	public void testFilesAreCopied() {
		
		BmajCopy copy = new BmajCopy();
		Project p = new Project();
		p.setProperty(BiomajConst.logFilesProperty, "false");
		copy.setProject(p);
		copy.setFrom(FROM_DIR);
		copy.setToDir(TO_DIR);
		copy.setListing(LISTING_FILE);
		
		copy.execute();
		
		for (File f : filz) {
			assertTrue(f.exists());
			if (f.isDirectory())
				assertTrue(new File(TO_DIR + "/" + f.getName()).exists() == false);
			else
				assertTrue(new File(TO_DIR + "/" + f.getName()).exists());
		}
		
	}
}
