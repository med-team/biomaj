/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;


public class BmajMove extends BmajTask {

	public static final String MODE_COPY = "copy";
	public static final String MODE_MOVE = "move";
	
	private int moveCount = 0;

	private Vector<FileSet> vFilesSet = new Vector<FileSet>();
	/**
	 * @uml.property  name="mode"
	 */
	private String mode ;
	/**
	 * @uml.property  name="toDir"
	 */
	private String toDir;
	
	private String taskId;
	
	private int numThread = 1;
	
	private List<String> filesToMove = new ArrayList<String>();

	@Override
	public void execute() throws BuildException {
		
		/*
		 * Retrieving number of threads to run
		 * Based on extract.threads
		 */
		try {
			String filesNumThread = BiomajUtils.getProperty(this.getProject(), BiomajConst.extractThreadProperty);
			numThread = new Integer(filesNumThread);
		} catch (BiomajException e) {
			numThread = 1;
			log("extract.threads is not defined ! Default value is : 1", Project.MSG_WARN);
		}

		if (numThread <= 0) {
			log("Bad value for extract.threads (" + numThread + "). New value is : 1", Project.MSG_WARN);
			numThread = 1;
		}
		
		File targetDirectory = new File(toDir);

		if (!targetDirectory.exists())
			throw new BiomajBuildException(getProject(),"bmajmove.todir.not.exist",toDir,new Exception());
		
		for (FileSet fileset : vFilesSet) {
			if (MODE_COPY.equals(mode))
				move(fileset, true);
			else if (MODE_MOVE.equals(mode))
				move(fileset, false);
			else
				throw new BiomajBuildException(getProject(),"bmajmove.unknow.mode",mode,new Exception());
		}
		
	}
	
	public void move(FileSet fileset, boolean hardCopy) throws BuildException {
		String[] files = fileset.getDirectoryScanner(getProject()).getIncludedFiles();
		boolean logFiles = Boolean.valueOf(getProject().getProperty(BiomajConst.logFilesProperty));
		
		List<MoveThread> runningThreads = new ArrayList<MoveThread>();
		
		int currentThreads = files.length < numThread ? files.length : numThread;
		
		filesToMove.clear();
		Collections.addAll(filesToMove, files);
		
		
		log("Starting " + currentThreads + " thread(s) to move " + files.length + " file(s).", Project.MSG_INFO);
		// Starting the threads
		for (int i = 0; i < currentThreads; i++) {
			MoveThread thread = new MoveThread(this, files.length, hardCopy, logFiles, toDir);
			runningThreads.add(thread);
			thread.start();
		}
		
		// Waiting for the threads to end
		for (MoveThread th : runningThreads) {
			try {
				th.join();
				/* 
				 * If an exception occured for any of the threads, throw that buildexception
				 * which will result in workflow being stopped.
				 */
				if (th.getError() != null)
					throw new BiomajBuildException(getProject(), th.getError());
			} catch (InterruptedException e) {
				log("Thread synchronization error : " + e.getMessage());
			}
		}
	}


	/**
	 * @param mode  the mode to set
	 * @uml.property  name="mode"
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	public void addConfiguredFileSet(FileSet fileset) {
		vFilesSet.add(fileset);
	}

	/**
	 * @return  the toDir
	 * @uml.property  name="toDir"
	 */
	public String getToDir() {
		return toDir;
	}

	/**
	 * @param toDir  the toDir to set
	 * @uml.property  name="toDir"
	 */
	public void setToDir(String toDir) {
		this.toDir = toDir;
	}
	
	public String getTaskId() {
		return taskId;
	}
	
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	protected synchronized String[] getFile() {
		if (filesToMove.size() > 0) {
			String name = filesToMove.get(0);
			filesToMove.remove(0);
			
			String[] res = {name, String.valueOf(moveCount++)};
			
			return res;
		}
		
		return null;
	}
}
