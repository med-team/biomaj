package org.inria.biomaj.ant.task.test;

import java.io.File;
import java.io.IOException;
import java.sql.Statement;
import java.util.Date;
import java.util.Locale;

import static junit.framework.Assert.assertTrue;

import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.task.BmajVersionManagement;
import org.inria.biomaj.sql.SQLConnection;
import org.inria.biomaj.sql.SQLConnectionFactory;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for BmajVersionManagement.
 * 
 * @author rsabas
 *
 */
public class TestBmajVersionManagement {
	
	private final String DIR = BiomajUtils.getBiomajRootDirectory() + "/testversionmanagement";
	private final String bankName = "test";
	private final String release = "x1";
	
	/**
	 * Creates the production directories, links and the related records in the test database.
	 * The creation date is modified so that it is different for each directory. 
	 */
	@Before
	public void setup() {
		
		
		SQLConnectionFactory.setTestMode(true);
		SQLConnection conn = SQLConnectionFactory.getConnection();
		conn.createDB("toto", "toto", "mail");
		
		
		
		Statement stat = conn.getStatement();
		
		// Clean
		conn.executeUpdate("DELETE FROM productionDirectory", stat);
		conn.executeUpdate("DELETE FROM bank", stat);
		
		String query = "INSERT INTO bank(name, ref_iduser) values('" + bankName + "',0)";
		int bankId = conn.executeUpdateAndGetGeneratedKey(query, stat);
		
		new File(DIR + "/dir1").mkdirs();
		query = "INSERT INTO productionDirectory(remove,creation,size,state,session,path,ref_idbank) " +
				"VALUES('" + BiomajUtils.dateToString(new Date(), Locale.US) + "','" + BiomajUtils.dateToString(new Date(new Date().getTime() - 10000), Locale.US) +
				"','1M','deleted',12121,'" + DIR + "/dir1'," + bankId + ")";
		conn.executeUpdate(query, stat);


		new File(DIR + "/dir2").mkdirs();
		query = "INSERT INTO productionDirectory(remove,creation,size,state,session,path,ref_idbank) " +
				"VALUES('" + BiomajUtils.dateToString(new Date(), Locale.US) + "','" + BiomajUtils.dateToString(new Date(new Date().getTime() - 5000), Locale.US) +
				"','1M','available',12121,'" + DIR + "/dir2'," + bankId + ")";
		conn.executeUpdate(query, stat);


		File current = new File(DIR + "/dir3");
		current.mkdirs();
		query = "INSERT INTO productionDirectory(remove,creation,size,state,session,path,ref_idbank) " +
				"VALUES(null,'" + BiomajUtils.dateToString(new Date(), Locale.US) +
				"','1M','available',12121,'" +DIR + "/dir3'," + bankId + ")";
		conn.executeUpdate(query, stat);
		
		try {
			SQLConnectionFactory.closeConnection(stat);
			BiomajUtils.createLinkOnFileSystem(current, BiomajConst.currentLink);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void cleanup() {
		BiomajUtils.deleteAll(new File(DIR));
	}
	
	/**
	 * Checks that the latest directory is retrieved.
	 */
	@Test
	public void testGet() {
		
		BmajVersionManagement v = getBmajVersionManagement();
		Project p = new Project();
		v.setProject(p);
		v.setMode(BmajVersionManagement.GET);
		v.execute();
		
		assertTrue(p.getProperty("prod.dir").equals(DIR + "/dir3"));
	}
	
	/**
	 * Checks that the next directory is created and the associated future link.
	 */
	@Test
	public void testCreate() {
		
		BmajVersionManagement v = getBmajVersionManagement();
		Project p = new Project();
		v.setProject(p);
		v.setMode(BmajVersionManagement.CREATE);
		v.execute();
		
		File f = new File(DIR + "/" + bankName + "_" + release);
		assertTrue(f.exists());
		File link = new File(DIR + "/" + BiomajConst.futureReleaseLink);
		assertTrue(link.exists());
		try {
			assertTrue(link.getCanonicalPath().equals(f.getAbsolutePath()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Checks that the links are correctly set.
	 */
	@Test
	public void testRebuild() {
		
		BmajVersionManagement v = getBmajVersionManagement();
		Project p = new Project();
		v.setProject(p);
		v.setMode(BmajVersionManagement.REBUILD);
		v.execute();
		
		File current = new File(DIR + "/" + BiomajConst.currentLink);
		File future = new File(DIR + "/" + BiomajConst.futureReleaseLink);
		
		try {
			assertTrue(current.getCanonicalPath().equals(DIR + "/dir2"));
			assertTrue(future.getCanonicalPath().equals(DIR + "/dir3"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private BmajVersionManagement getBmajVersionManagement() {
		BmajVersionManagement v = new BmajVersionManagement();
		v.setNbVersion("2");
		v.setBank(bankName);
		v.setRoot(DIR);
		v.setFlatDirectory("flat.dir");
		v.setProdDirectory("prod.dir");
		v.setRelease(release);
		
		return v;
	}
	
}
