/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task;

import java.io.File;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.inria.biomaj.utils.BiomajBuildException;

public class BmajGetRelativePath extends Task {
	
	/**
	 * @uml.property  name="absolutePath"
	 */
	private String absolutePath;
	/**
	 * @uml.property  name="fromPath"
	 */
	private String fromPath;
	/**
	 * @uml.property  name="relativPath"
	 */
	private String relativPath;
	/**
	 * Find a relativ path from a reference directory,
	 * otherwise get the absoltue path from origine
	 */
	@Override
	public void execute() throws BuildException {
		
		File from = new File(fromPath);
		File abs  = new File(absolutePath);
		
		if (!from.exists())
			throw new BiomajBuildException(getProject(),"bmajGetRelativePath.error.file",from.getAbsolutePath(),new Exception());
		if (!abs.exists())
			throw new BiomajBuildException(getProject(),"bmajGetRelativePath.error.file",abs.getAbsolutePath(),new Exception());
		
		log("reference directory to calculate relative path:"+from.getAbsolutePath(),Project.MSG_VERBOSE);
		log("absolute directory to transforme:"+abs.getAbsolutePath(),Project.MSG_VERBOSE);
		
		String[] base = from.getAbsolutePath().split("/");
		String[] baseAbs = abs.getAbsolutePath().split("/");
		
		int i=0;
		while ((i<base.length)&&(i<baseAbs.length)&&(base[i].compareTo(baseAbs[i])==0))
			i++;
		
		String res="";
		//ex : from:/toto/tata     abs:/toto/tata/titi   
		if (i>=base.length) {
			res = "./";
			for(int j=i;j<baseAbs.length;j++)
				res+=baseAbs[j]+"/";
		} else {
			res="";
			//sinon ex from:/toto/tata/titi  abs:/toto/tata/tutu/tete
			for (int j=i;j<base.length;j++){
				res+="../";
			}
			
			for (int j=i;j<baseAbs.length;j++){
				res+=baseAbs[j]+"/";
			}
		}
		log("relative path:"+res,Project.MSG_VERBOSE);
		getProject().setProperty(relativPath,res);
		return;
	}
	
	/**
	 * @return  the absolutePath
	 * @uml.property  name="absolutePath"
	 */
	public String getAbsolutePath() {
		return absolutePath;
	}
	
	/**
	 * @param absolutePath  the absolutePath to set
	 * @uml.property  name="absolutePath"
	 */
	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}
	
	/**
	 * @return  the fromPath
	 * @uml.property  name="fromPath"
	 */
	public String getFromPath() {
		return fromPath;
	}
	
	/**
	 * @param fromPath  the fromPath to set
	 * @uml.property  name="fromPath"
	 */
	public void setFromPath(String fromPath) {
		this.fromPath = fromPath;
	}
	
	/**
	 * @return  the relativPath
	 * @uml.property  name="relativPath"
	 */
	public String getRelativPath() {
		return relativPath;
	}
	
	/**
	 * @param relativPath  the relativPath to set
	 * @uml.property  name="relativPath"
	 */
	public void setRelativPath(String relativPath) {
		this.relativPath = relativPath;
	}
	
}
