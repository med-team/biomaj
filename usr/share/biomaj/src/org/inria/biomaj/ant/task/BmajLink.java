/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task;


import java.io.File;
import java.util.Vector;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
import org.inria.biomaj.utils.BiomajBuildException;

public class BmajLink extends Task {
	
	/**
	 * Target directory where link have to generate.
	 * @uml.property  name="todir"
	 */
	private String todir ;
	
	/** FileSet to filter files*/
	
	private Vector<FileSet> fileSets;
	
	/**
	 * Constructor
	 *
	 */
	public BmajLink() {
		fileSets = new Vector<FileSet>();
	}
	/**
	 * <p>Main task methode. This methode generate links and subdirectories from a directory specified by todir</p>
	 * @throws org.apache.tools.ant.BuildException If an Ant build exception is encountered.
	 * @see org.apache.tools.ant.Task#execute()
	 */
	@Override
	public void execute () throws BuildException
	{
		try
		{
			// each line is read
			for (FileSet fs : fileSets)
			{
				DirectoryScanner ds = fs.getDirectoryScanner(getProject());
				//file to link
				String[] f = ds.getIncludedFiles();
				log("remotedirectory:"+ds.getBasedir().toString(),Project.MSG_VERBOSE);
				String remoteDirectory = ds.getBasedir().toString();
				for (int i=0;i<f.length;i++) {
					log("file "+Integer.toString(i)+":"+f[i],Project.MSG_VERBOSE);
					createDirectoryNeeded(f[i]);
					Runtime.getRuntime().exec("ln -s "+remoteDirectory+"/"+f[i]+" "+todir+"/"+"/"+f[i]);
				}
			}
		}
		catch (Exception e)
		{
			throw new BiomajBuildException(getProject(),e);
		}
	}
	
	/**
	 * Generate directory inside pathAndFileName in target directory
	 * @param pathAndFileName relative path and filename
	 */
	protected void createDirectoryNeeded (String pathAndFileName) {
		//If there are no directory to create
		if (!pathAndFileName.contains("/"))
			return;
		log("Create directories to link :"+pathAndFileName,Project.MSG_VERBOSE);
		String[] directorySet  = pathAndFileName.split("/");
		String currentDirectoryToCreate=todir+"/";
		//The last element is a file name
		for (int i=0;i<directorySet.length-1;i++) {
			currentDirectoryToCreate = currentDirectoryToCreate+directorySet[i]+"/";
			log("mkdir directory:"+currentDirectoryToCreate,Project.MSG_VERBOSE);
			File directoryToCreate = new File(currentDirectoryToCreate);
			directoryToCreate.mkdir();
			
		}
		
	}
	
	/**
	 * Add a set of files (nested fileset attribute)
	 */
	public void addFileSet(FileSet set) {
		// Add the fileSet Object to the instance variable
		// fileSets, a vector of FileSets Object
		fileSets.add(set);
	}
	
	
	/**
	 * @return  the todir
	 * @uml.property  name="todir"
	 */
	public String getTodir() {
		return todir;
	}
	
	
	/**
	 * @param todir  the todir to set
	 * @uml.property  name="todir"
	 */
	public void setTodir(String todir) {
		this.todir = todir;
	}
}
