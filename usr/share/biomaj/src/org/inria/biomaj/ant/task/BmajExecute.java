/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task;

import java.io.File;
import java.io.IOException;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Execute;
import org.apache.tools.ant.taskdefs.ExecuteWatchdog;
import org.apache.tools.ant.taskdefs.LogStreamHandler;
import org.apache.tools.ant.types.Commandline;
import org.inria.biomaj.exe.bank.BankFactory;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * Execute a process in phase : pre and post process
 * 
 * @author ofilangi
 * @version Biomaj 0.9
 * @since Biomaj 0.8 /Citrina 0.5
 */
public class BmajExecute extends BmajTask {

	public static final String WARNING_FILTER = "__biomaj_warning___";
	public static final String END_FILTER     = "__end___";
	public static final String DEPENDANCE_FILTER     = "__depends___";
	public static final String DEPENDANCE_VOLATILE__FILTER     = "__depends___volatile___";

	public static final String PROPERTY_ERROR = "biomaj.error.during.script";
	public static final String MESSAGE_PROPERTY_ERROR = "biomaj.error.message.during.script";


	public static final String RELEASE_ALL_COMPRESSED_FILES_LIST = "RELEASE_ALL_COMPRESSED_FILES_LIST";
	public static final String RELEASE_ALL_UNCOMPRESSED_FILES_LIST = "RELEASE_ALL_UNCOMPRESSED_FILES_LIST";
	public static final String RELEASE_OLD_FILES_LIST ="RELEASE_OLD_FILES_LIST"; 
	public static final String RELEASE_NEW_FILES_LIST = "RELEASE_NEW_FILES_LIST";
	
	public static final String CLUSTER_EXE = "biomaj_drmaa.pl";

	/* Attributs Task*/
	/* --------------*/
	/**
	 * Name of key process (Required)
	 * @uml.property  name="keyName"
	 */
	private String keyName;
	/**
	 * Process name (Required)
	 * @uml.property  name="name"
	 */
	private String name;
	/**
	 * Executable Name (Required)
	 * @uml.property  name="exe"
	 */
	private String exe;
	/**
	 * Arguments executable (Not Required)
	 * @uml.property  name="args"
	 */
	private String args;
	/**
	 * Process description (Required)
	 * @uml.property  name="description"
	 */
	private String description;
	/**
	 * Process Type (Required)
	 * @uml.property  name="type"
	 */
	private String type;
	/**
	 * Kill all processes (Not Required)
	 * @uml.property  name="killAllOnError"
	 */
	private String killAllOnError;

	private String cluster; // If true, execute process on cluster
	
	private String timeOut;

	long valueTimeOut = -1 ;
	
	/* Internal data */
	/* ------------- */

	/**
	 * Kill all process
	 * @uml.property  name="killAll"
	 */
	Boolean killAll = false;

	@Override
	public void execute() throws BuildException {
		verifAttributs();
		
		String clustValue;
		boolean runOnCluster = false;
		if ((clustValue = this.getProject().getProperty(cluster)) != null) {
			runOnCluster = clustValue.equals("true");
		}

		Commandline cmd = new Commandline();

//		Execute execute = new Execute(new LogStreamHandler(this, Project.MSG_INFO,Project.MSG_ERR),null);
		
		ExecuteWatchdog watchTimeout = null;
		if (valueTimeOut > 0)
			watchTimeout = new ExecuteWatchdog(valueTimeOut * 1000); //ms to s
		
		Execute execute = new Execute(new LogStreamHandler(this, Project.MSG_INFO,Project.MSG_ERR), watchTimeout);

		String executable = "";
		try {
			
			Vector<String> listEnvironmentVariables = new Vector<String>();
			listEnvironmentVariables.add("dbname="+getProject().getProperty(BiomajConst.dbNameProperty));
			listEnvironmentVariables.add("datadir="+getProject().getProperty(BiomajConst.dataDirProperty));
			listEnvironmentVariables.add("offlinedir="+getProject().getProperty(BiomajConst.offlineDirProperty));
			listEnvironmentVariables.add("dirversion="+getProject().getProperty(BiomajConst.versionDirProperty));
			listEnvironmentVariables.add("remotedir="+getProject().getProperty(BiomajConst.remoteDirProperty));
			listEnvironmentVariables.add("noextract="+getProject().getProperty(BiomajConst.noExtractProperty));
			listEnvironmentVariables.add("localfiles="+getProject().getProperty(BiomajConst.localFilesProperty));
			listEnvironmentVariables.add("remotefiles="+getProject().getProperty(BiomajConst.remoteFilesProperty));
			listEnvironmentVariables.add("mailadmin="+getProject().getProperty(BiomajConst.mailAdminProperty));
			listEnvironmentVariables.add("mailsmtp="+getProject().getProperty(BiomajConst.mailSmtpHostProperty));
			listEnvironmentVariables.add("mailfrom="+getProject().getProperty(BiomajConst.mailFromProperty));
			
			// Bank dependencies
			if (getProject().getProperty("db.source") != null) {
				String[] dependencies = BiomajUtils.getBankDependencies(getProject());
				for (String dependency : dependencies) {
					BankFactory factory = new BankFactory();
					BiomajBank bb = factory.createBank(dependency, false);
					String version = bb.getPropertiesFromBankFile().getProperty(BiomajConst.versionDirProperty);
					String dataDir = bb.getPropertiesFromBankFile().getProperty(BiomajConst.dataDirProperty);
					File prod = new File(dataDir + "/" + version + "/" + BiomajConst.futureReleaseLink);
					if (!prod.exists()) {
						prod = new File(dataDir + "/" + version + "/" + BiomajConst.currentLink);
					}
					String prodPath = prod.getCanonicalPath();
					listEnvironmentVariables.add(dependency + "source=" + prodPath);
					this.getProject().setProperty(dependency + "source",prodPath);
					log("Computed bank specific variable : " + dependency + "source=" + prodPath, Project.MSG_DEBUG);
				}
			}
			
			String newArgs = replacePropertiesValues(this.getProject().getProperty(args));
			String pathToExe = getPathExe(getProject(), this.getProject().getProperty(exe).trim());
			
			if (runOnCluster) {
				executable = BiomajInformation.getInstance().getProperty(BiomajInformation.PROCESSDIR) + "/" + CLUSTER_EXE;
				newArgs = (pathToExe + " " + newArgs).trim();
			} else {
				executable = pathToExe;
			}
			
			
			cmd.setExecutable(executable);
			cmd.createArgument().setLine(newArgs);
			execute.setCommandline(cmd.getCommandline());
			
			//Nouvelles Variables d'environnements

			listEnvironmentVariables.add("PATH_PROCESS_BIOMAJ="+BiomajInformation.getInstance().getProperty(BiomajInformation.PROCESSDIR));
			listEnvironmentVariables.add("PATH_LOG_BIOMAJ="+BiomajInformation.getInstance().getProperty(BiomajInformation.LOGDIR));
			listEnvironmentVariables.add("PATH_WORKFLOW_BIOMAJ="+BiomajInformation.getInstance().getProperty(BiomajInformation.WORKFLOWSDIR));

			String listFilesValue = null;
			if ((listFilesValue = getProject().getProperty(BiomajConst.listFilesAvailable)) != null && Boolean.valueOf(listFilesValue)) {
				
				listEnvironmentVariables.add("RELEASE_ALL_COMPRESSED_FILES_LIST="+getProject().getProperty(RELEASE_ALL_COMPRESSED_FILES_LIST));
				listEnvironmentVariables.add("RELEASE_ALL_UNCOMPRESSED_FILES_LIST="+getProject().getProperty(RELEASE_ALL_UNCOMPRESSED_FILES_LIST));
				listEnvironmentVariables.add("RELEASE_OLD_FILES_LIST="+getProject().getProperty(RELEASE_OLD_FILES_LIST));
				listEnvironmentVariables.add("RELEASE_NEW_FILES_LIST="+getProject().getProperty(RELEASE_NEW_FILES_LIST));
			}

			//Ajout de la liste des fichiers telecharges pour la release correspondant au delta entre 2 release


			log("------------------------- ENVIRONMENT VARIABLE FOR POST-PROCESS ------------------------",Project.MSG_DEBUG);

			log("dbname="+getProject().getProperty(BiomajConst.dbNameProperty),Project.MSG_DEBUG);
			log("datadir="+getProject().getProperty(BiomajConst.dataDirProperty),Project.MSG_DEBUG);
			log("offlinedir="+getProject().getProperty(BiomajConst.offlineDirProperty),Project.MSG_DEBUG);
			log("dirversion="+getProject().getProperty(BiomajConst.versionDirProperty),Project.MSG_DEBUG);
			log("remotedir="+getProject().getProperty(BiomajConst.remoteDirProperty),Project.MSG_DEBUG);
			log("noextract="+getProject().getProperty(BiomajConst.noExtractProperty),Project.MSG_DEBUG);
			log("localfiles="+getProject().getProperty(BiomajConst.localFilesProperty),Project.MSG_DEBUG);
			log("remotefiles="+getProject().getProperty(BiomajConst.remoteFilesProperty),Project.MSG_DEBUG);
			log("mailadmin="+getProject().getProperty(BiomajConst.mailAdminProperty),Project.MSG_DEBUG);
			log("mailsmtp="+getProject().getProperty(BiomajConst.mailSmtpHostProperty),Project.MSG_DEBUG);
			log("mailfrom="+getProject().getProperty(BiomajConst.mailFromProperty),Project.MSG_DEBUG);

			//Dynamics property....
			listEnvironmentVariables.add("remoterelease="+getProject().getProperty(BiomajConst.remoteReleaseDynamicProperty));
			listEnvironmentVariables.add("removedrelease="+getProject().getProperty(BiomajConst.removedReleaseProperty));
			log("remoterelease="+getProject().getProperty(BiomajConst.remoteReleaseDynamicProperty),Project.MSG_DEBUG);
			log("removedrelease="+getProject().getProperty(BiomajConst.removedReleaseProperty),Project.MSG_DEBUG);
			log("-------------------------------------------------",Project.MSG_DEBUG);
			listEnvironmentVariables.add(BiomajConst.PP_WARNING+"="+WARNING_FILTER);
			listEnvironmentVariables.add(BiomajConst.PP_DEPENDENCE_VOLATILE+"="+DEPENDANCE_VOLATILE__FILTER);
			listEnvironmentVariables.add(BiomajConst.PP_DEPENDENCE+"="+DEPENDANCE_FILTER);

			//String onlineDirDynamicProperty = getProject().getProperty(CitrinaConst.onlineDirDynamicProperty);

			/*
			if ((onlineDirDynamicProperty == null)||(onlineDirDynamicProperty.compareTo("")==0))
				throw new BiomajBuildException(getProject(),"citrinautils.error.property",CitrinaConst.onlineDirDynamicProperty);
			 */

			String type = getProject().getProperty("type_process");

			if (type == null)
				throw new BiomajBuildException(getProject(),"biomaj.property.define.error","type_process",new Exception());
			File workDirectory ;
			if (type.compareTo("removeprocess")==0)
				workDirectory = new File(getProject().getProperty(BiomajConst.dataDirProperty)+"/");
			else if (type.compareTo("preprocess")==0)
				workDirectory = new File(getProject().getProperty(BiomajConst.dataDirProperty)+"/"+getProject().getProperty(BiomajConst.versionDirProperty));
			else
				workDirectory = new File(getProject().getProperty(BiomajConst.dataDirProperty)+"/"+getProject().getProperty(BiomajConst.versionDirProperty)+"/"+BiomajConst.futureReleaseLink);

			if (!workDirectory.isDirectory()) {
				throw new BiomajBuildException(getProject(),"directory.not.exist",workDirectory.getAbsolutePath(),new Exception());
			}

			if (type.compareTo("preprocess")!=0)
				listEnvironmentVariables.add("newrelease="+workDirectory.getCanonicalPath());

			log("                                 #===================================#",Project.MSG_INFO);
			log("                                 [Process]",Project.MSG_INFO);
			log("                                 KEYNAME     :"+getKeyName(),Project.MSG_INFO);
			log("                                 NAME        :"+getProject().getProperty(getName()),Project.MSG_INFO);
			log("                                 EXE         :"+getProject().getProperty(getExe()),Project.MSG_INFO);
			log("                                 ARGS        :"+newArgs,Project.MSG_INFO);
			log("                                 TIMEOUT     :"+getProject().getProperty(getTimeOut()),Project.MSG_INFO);
			log("                                 DESCRIPTION :"+getProject().getProperty(getDescription()),Project.MSG_INFO);
			log("                                 #===================================#",Project.MSG_INFO);

			execute.setWorkingDirectory(workDirectory);
			execute.setEnvironment(listEnvironmentVariables.toArray(new String[listEnvironmentVariables.size()]));

			getProject().setProperty(PROPERTY_ERROR, Boolean.toString(false));
			execute.execute();

			if (Boolean.valueOf(getProject().getProperty(PROPERTY_ERROR))) {
				//Il y a eu une erreur qui a ete initialiser par BiomajProcessListener
				throw new BiomajBuildException(getProject(),"unknown.error",getProject().getProperty(MESSAGE_PROPERTY_ERROR),null);
			}

		} catch (IOException ioe) {
			//	ioe.printStackTrace();
			throw new BiomajBuildException(getProject(),"io.error",ioe.getMessage(),ioe);
		} catch (BiomajException be) {
			throw new BiomajBuildException(getProject(),be);
		}

		int code_retour  = execute.getExitValue();
		getProject().setProperty("returnValue", Integer.toString(code_retour));
		if (code_retour!=0) {
			if (watchTimeout != null && watchTimeout.killedProcess())
				log("Process timeout", Project.MSG_ERR);
			throw new BiomajBuildException(getProject(),"process.error",keyName,exe,new Exception());
		}
	}

	private String getProperty(String name) {
		return this.getProject().getProperty(name);
	}

	private void verifAttributs() {

		if (!this.getProject().getProperties().containsKey(name)) {
			throw new BiomajBuildException(getProject(),"process.param.undefined",keyName,"name",new Exception());
		} 

		String test = getProperty(name);
		if (test.contains(" "))
			throw new BiomajBuildException(getProject(),"process.not.blank",name,getProperty(name),new Exception());

		if (!this.getProject().getProperties().containsKey(exe)) {
			throw new BiomajBuildException(getProject(),"process.param.undefined",keyName,"exe",new Exception());
		} 

		test = getProperty(exe);
		if (test.contains(" "))
			throw new BiomajBuildException(getProject(),"process.not.blank",exe,getProperty(exe),new Exception());

		if (!this.getProject().getProperties().containsKey(description)) {
			throw new BiomajBuildException(getProject(),"process.param.undefined",keyName,"desc",new Exception());
		} 

		if (!this.getProject().getProperties().containsKey(type)) {
			throw new BiomajBuildException(getProject(),"process.param.undefined",keyName,"type",new Exception());
		} 
		
		if (!this.getProject().getProperties().containsKey(timeOut)) {
			valueTimeOut = -1;
		} else {
			try {
			valueTimeOut = Long.valueOf(getProperty(timeOut));
			} catch (NumberFormatException e) {
				log("Value ["+getProperty(timeOut)+"] for timeout is not valid ",Project.MSG_WARN);
				valueTimeOut = -1 ;
			}
		}
		
		if (this.getProject().getProperties().containsKey(killAllOnError)) {
			killAll = Boolean.valueOf(this.getProject().getProperty(killAllOnError));
		} else
			this.getProject().setProperty(killAllOnError, "false");


	}

	static public String getPathExe(Project p,String executable) throws BiomajException {
		if (executable.charAt(0)!='/') {
			String scriptsDirectory = BiomajInformation.getInstance().getProperty(BiomajInformation.PROCESSDIR)+"/";
			//	Si le chemin n'est pas absolue, on cherche le script dans le repertoire par defaut
			File theExe = new File(scriptsDirectory+executable);
			if (theExe.exists()) {
				executable = theExe.getAbsolutePath();
			} else {
				if (p!=null)
					p.log("["+theExe.getAbsolutePath()+"] does not exist.", Project.MSG_WARN);
			}
		}

		return executable;
	}


	static public long getTimeStampExe(String pathexe) {
		File f = new File(pathexe);

		if (f.exists())
			return f.lastModified();

		return 0;
	}

	/**
	 * @return  the description
	 * @uml.property  name="description"
	 */
	@Override
	public String getDescription() {
		return description;
	}


	/**
	 * @param description  the description to set
	 * @uml.property  name="description"
	 */
	@Override
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return  the exe
	 * @uml.property  name="exe"
	 */
	public String getExe() {
		return exe;
	}


	/**
	 * @param exe  the exe to set
	 * @uml.property  name="exe"
	 */
	public void setExe(String exe) {
		this.exe = exe;
	}


	/**
	 * @return  the killAllOnError
	 * @uml.property  name="killAllOnError"
	 */
	public String getKillAllOnError() {
		return killAllOnError;
	}


	/**
	 * @param killAllOnError  the killAllOnError to set
	 * @uml.property  name="killAllOnError"
	 */
	public void setKillAllOnError(String killAllOnError) {
		this.killAllOnError = killAllOnError;
	}


	/**
	 * @return  the name
	 * @uml.property  name="name"
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name  the name to set
	 * @uml.property  name="name"
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return  the type
	 * @uml.property  name="type"
	 */
	public String getType() {
		return type;
	}


	/**
	 * @param type  the type to set
	 * @uml.property  name="type"
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	public void setCluster(String cluster) {
		this.cluster = cluster;
	}
	
	public String getCluster() {
		return cluster;
	}


	/**
	 * @return  the keyName
	 * @uml.property  name="keyName"
	 */
	public String getKeyName() {
		return keyName;
	}


	/**
	 * @param keyName  the keyName to set
	 * @uml.property  name="keyName"
	 */
	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}


	/**
	 * @return  the args
	 * @uml.property  name="args"
	 */
	public String getArgs() {
		return args;
	}


	/**
	 * @param args  the args to set
	 * @uml.property  name="args"
	 */
	public void setArgs(String args) {
		this.args = args;
	}

	public String getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(String timeOut) {
		this.timeOut = timeOut;
	}

	public String replacePropertiesValues(String args) throws BiomajBuildException{

		//System.out.println("ok");
		if ((args==null)||args.compareTo("")==0)
			return "";
		Pattern patternProperty = Pattern.compile("\\$\\{([\\w\\.]+)\\}");
		Matcher match = patternProperty.matcher(args);
		try {
			while (match.find()) {
				String key = match.group(1);
				String res = BankFactory.getRecursiveProperty(getProject().getProperties(), key);
				args = args.replaceAll("\\$\\{"+key+"\\}", res);				
				match = patternProperty.matcher(args);
			}
		} catch (Exception e) {
			throw new BiomajBuildException(getProject(),e);
		}

		return args ;
	}
}
