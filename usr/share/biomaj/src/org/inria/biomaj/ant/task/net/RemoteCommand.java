/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task.net;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.gmod.biomaj.ant.task.InputValidation;
import org.inria.biomaj.internal.ant.task.net.FtpImpl;
import org.inria.biomaj.internal.ant.task.net.HttpImpl;
import org.inria.biomaj.internal.ant.task.net.LocalImpl;
import org.inria.biomaj.internal.ant.task.net.RemoteCommandImpl;
import org.inria.biomaj.internal.ant.task.net.RsyncImpl;
import org.inria.biomaj.internal.ant.task.net.S3Impl;
import org.inria.biomaj.internal.ant.task.net.SftpImpl;
import org.inria.biomaj.utils.BiomajBuildException;

public class RemoteCommand extends Task {
	

	/**
	 * @uml.property  name="server"
	 */
	private String server;
	
	/**
	 * @uml.property  name="port"
	 */
	private int port;
	
	/**
	 * @uml.property  name="username"
	 */
	private String username;
	
	/**
	 * @uml.property  name="password"
	 */
	private String password;

	public static final String FTP_PROTOCOL  = "ftp";
	public static final String HTTP_PROTOCOL = "http";
	public static final String LOCAL_PROTOCOL = "local";
	public static final String RSYNC_PROTOCOL = "rsync";
	public static final String SFTP_PROTOCOL = "sftp";
	public static final String S3_PROTOCOL = "s3";
	
	/**
	 * @uml.property  name="protocol"
	 */
	private String protocol;
	
	/**
	 * @uml.property  name="impl"
	 * @uml.associationEnd  
	 */
	protected RemoteCommandImpl impl = null;
   
    public void initRemoteCommand() throws BuildException {
    	//Implementation
    	getProject().log("**** Start Remote Session ****",Project.MSG_VERBOSE);
    	
    	impl = getImpl();
    	
    }
    
    public RemoteCommandImpl getImpl() throws BuildException {
    	RemoteCommandImpl res = null;
    	log("Implementation:"+protocol, Project.MSG_VERBOSE);
    	if (protocol.equals(FTP_PROTOCOL))
    		res = new FtpImpl(this);
    	else if (protocol.equals(HTTP_PROTOCOL))
    		res = new HttpImpl(this);
    	else if (protocol.equals(LOCAL_PROTOCOL))
    		res = new LocalImpl(this);
    	else if (protocol.equals(RSYNC_PROTOCOL))
    		res = new RsyncImpl(this);
    	else if (protocol.equals(S3_PROTOCOL))
    		res = new S3Impl(this);
    	else if (protocol.equals(SFTP_PROTOCOL)) {
    		res = new SftpImpl(this);
    	}
    	else
    		throw new BiomajBuildException(getProject(),"error.implementation.find",protocol,new Exception());
    	
    	res.init(server,port,username,password);
    	
    	return res;
    }
    
    public void closeRemoteCommand() throws BuildException {
    	if (impl!=null) {
    		impl.disconnect();
    	}
    	getProject().log("**** End Remote Session ****",Project.MSG_VERBOSE);
    }
    
    /**
     * @return  the password
	 * @uml.property  name="password"
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password  the password to set
	 * @uml.property  name="password"
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return  the port
	 * @uml.property  name="port"
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port  the port to set
	 * @uml.property  name="port"
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return  the server
	 * @uml.property  name="server"
	 */
	public String getServer() {
		return server;
	}

	/**
	 * @param server  the server to set
	 * @uml.property  name="server"
	 */
	public void setServer(String server) {
		this.server = server;
	}

	/**
	 * @return  the username
	 * @uml.property  name="username"
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username  the username to set
	 * @uml.property  name="username"
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return  the protocol
	 * @uml.property  name="protocol"
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * @param protocol  the protocol to set
	 * @uml.property  name="protocol"
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	
	/**
     * Checks the attributes passed to ftplisting task.
     *
     * @throws org.apache.tools.ant.BuildException If required attributes are not set.
     */
	  protected void checkInputInit() {
	        InputValidation.checkString(getProject(),server, "server name");
	        InputValidation.checkString(getProject(),Integer.toString(port), "port number");
	        InputValidation.checkString(getProject(),username, "username");
	        InputValidation.checkString(getProject(),password, "password");
	  }

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return super.getDescription();
	} 
}
