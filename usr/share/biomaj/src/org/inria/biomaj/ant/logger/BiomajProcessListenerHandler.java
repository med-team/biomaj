/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.logger;

import org.apache.tools.ant.BuildEvent;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.SubBuildListener;
import org.inria.biomaj.ant.task.BmajExecute;
import org.inria.biomaj.session.bank.Bank;
import org.inria.biomaj.session.process.MetaProcess;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
/**
 * <p>This class implements a SubBuildListener of Ant to write the process state 
 * file with update information on each step of Ant.
 * This listener is attached with the handle_process.xml file.</p>
 * 
 * @author ofilangi
 * @version Biomaj 0.9
 * @since Biomaj 0.8 /Citrina 0.5
 */
public class BiomajProcessListenerHandler implements SubBuildListener {

	/**
	 * @uml.property  name="pl"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private DBProcessLogger pl ;
	/**
	 * @uml.property  name="historic"
	 * @uml.associationEnd  
	 */
//	private SimpleLoggerHistoric historic = null;
	/**
	 * @uml.property  name="bank"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private Bank bank;
	
	public BiomajProcessListenerHandler(Bank bank) throws BiomajException {
		pl = new DBProcessLogger(bank);
		this.bank = bank;
	}

	public void subBuildFinished(BuildEvent arg0) {
		try {
		pl.endMetaProcessProcess(arg0);
		} catch (BiomajException be) {
			throwException(arg0,be);
		}
	}

	public void subBuildStarted(BuildEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void buildFinished(BuildEvent arg0) {
	//	historic.stop();
	}

	public void buildStarted(BuildEvent arg0) {
		//historic = new SimpleLoggerHistoric(arg0.getProject().getProperty("type_process")+"."+pl.getMetaProcess().getName()+".log",bank,arg0.getProject());
		SimpleLoggerHistoric slh = BiomajLogger.getInstance().getLogger(bank.getConfig().getName()+"Block:"+pl.getMetaProcess().getBlock()+"."+pl.getMetaProcess().getName());
		if (slh != null)
			pl.getMetaProcess().setLogFile(slh.getNameFile());
	}

	public void messageLogged(BuildEvent arg0) {
		if (arg0==null) {
			return;
		}
		try {
		String value = arg0.getMessage();
//		BiomajLogger.getInstance().log("### Message : " + value);
		/*
		if (historic != null)
			historic.write(arg0);
		*/
		//Tests de Warning
		
		if (value.contains(BmajExecute.WARNING_FILTER)) {
			value = value.replaceFirst(BmajExecute.WARNING_FILTER,"");
			pl.warnMessage(value,arg0);
			return;
		}
		
		if ((value.contains(BmajExecute.DEPENDANCE_FILTER))||(value.contains(BmajExecute.DEPENDANCE_VOLATILE__FILTER))) {
			
			boolean volatil = value.contains(BmajExecute.DEPENDANCE_VOLATILE__FILTER);
			String filter = BmajExecute.DEPENDANCE_FILTER;
			if (volatil)
				filter = BmajExecute.DEPENDANCE_VOLATILE__FILTER;
			
			String[] l = value.split(filter);
			if (l.length!=2) {
				arg0.getProject().log("Dependance files is bad specified in script!:", Project.MSG_ERR);
				if (l.length<=0)
					{
					arg0.getProject().log("no origine files are specified", Project.MSG_ERR);
					return;
					}
				int i = 0;
				arg0.getProject().log("origine files:"+l[i++], Project.MSG_ERR);
				
				if (l.length<2) {
					arg0.getProject().log("no new files are specified", Project.MSG_ERR);
					return;
				}
				arg0.getProject().log("new files:"+l[i++], Project.MSG_ERR);
				return;
			}
			if (!volatil) {
				pl.addDependanceFiles(arg0.getProject().getProperty(BiomajConst.dbNameProperty),l[0], l[1],arg0);
			}
			else
				pl.addVolatileDependanceFiles(arg0.getProject().getProperty(BiomajConst.dbNameProperty),l[0], l[1],arg0);
		}
		
		if (arg0.getPriority()==Project.MSG_WARN)
			pl.warnMessage(arg0.getMessage(),arg0);
		if (arg0.getPriority()==Project.MSG_ERR)
			pl.errorMessage(arg0.getMessage(),arg0);
		} catch (BiomajException e) {
			throwException(arg0,e);
		}
	}

	public void targetFinished(BuildEvent arg0) {
		if (handleProcessContext(arg0)&&arg0.getTarget().getName().compareTo(BiomajConst.processTarget)==0)
		{
			if (arg0.getProject().getProperty("type_process")==null) {
				BiomajLogger.getInstance().log("Internal error: property type_process not defined (process management)");
				return;
			}
		}

	}

	public void targetStarted(BuildEvent arg0) {
		if (handleProcessContext(arg0)&&arg0.getTarget().getName().compareTo(BiomajConst.processTarget)==0)
		{
			if (arg0.getProject().getProperty("type_process")==null) {
				BiomajLogger.getInstance().log("Internal error: property type_process not defined (process management)");
				return;
			}
		}

	}

	public void taskFinished(BuildEvent arg0) {
		if (handleProcessContext(arg0)&&arg0.getTask().getTaskName().compareTo("bmaj-execute")==0)
		{
			if (arg0.getProject().getProperty("type_process")==null) {
				BiomajLogger.getInstance().log("Internal error: property type_process not defined (process management)");
				return;
			}
			try {
				if (arg0.getProject().getProperty("type_process").compareTo(BiomajConst.postprocessTarget)==0)
					pl.endPostProcess(arg0);
				else if (arg0.getProject().getProperty("type_process").compareTo(BiomajConst.preprocessTarget)==0)
					pl.endPreProcess(arg0);
				else if (arg0.getProject().getProperty("type_process").compareTo(BiomajConst.removeprocessTarget)==0)
					pl.endRemoveProcess(arg0);
				else {
					BiomajLogger.getInstance().log("Internal error: property type_process bad definition ["+arg0.getProject().getProperty("type_process")+"] (process management)");
					return;
				}
			} catch (BiomajException be) {
				throwException(arg0,be);
			}
		}
		
	}

	public void taskStarted(BuildEvent arg0) {
		if (handleProcessContext(arg0)&&arg0.getTask().getTaskName().compareTo("bmaj-execute")==0)
		{
			if (arg0.getProject().getProperty("type_process")==null) {
				BiomajLogger.getInstance().log("Internal error: property type_process not defined (process management)");
				return;
			}
			try {
			if (arg0.getProject().getProperty("type_process").compareTo(BiomajConst.postprocessTarget)==0)
				pl.addPostProcess(arg0);
			else if (arg0.getProject().getProperty("type_process").compareTo(BiomajConst.preprocessTarget)==0)
				pl.addPreProcess(arg0);
			else if (arg0.getProject().getProperty("type_process").compareTo(BiomajConst.removeprocessTarget)==0)
				pl.addRemoveProcess(arg0);
			else {
				BiomajLogger.getInstance().log("Internal error: property type_process bad definition ["+arg0.getProject().getProperty("type_process")+"] (process management)");
				return;
			}
			} catch (BiomajException be) {
				throwException(arg0,be);
			}
		}
	}

	//******************************************** FIN INTERFACE ******************************************************

	protected Boolean handleProcessContext(BuildEvent arg0) {

		if (BiomajConst.handleProcessProject.compareTo(arg0.getProject().getName())==0)
			return true;

		return false;

	}
	
	
	public MetaProcess getMetaProcess() {
		return pl.getMetaProcess();
	}


	public void setMetaProcess(MetaProcess mp) {
		pl.setMetaProcess(mp);
	}
	
	public void setErrorOnCurrentProcess() throws BiomajException {
		pl.setErrorOnCurrentProcess();
	}

	public void throwException(BuildEvent arg0,BiomajException e) throws BuildException {
		arg0.getProject().setProperty(BmajExecute.PROPERTY_ERROR, Boolean.toString(true));
		arg0.getProject().setProperty(BmajExecute.MESSAGE_PROPERTY_ERROR, e.getMessage());
		throw new BuildException();
		//throw new BiomajBuildException(arg0.getProject(),e);
	}
	
}
