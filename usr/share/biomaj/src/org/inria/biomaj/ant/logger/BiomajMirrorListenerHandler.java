/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.logger;

import org.apache.tools.ant.BuildEvent;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.SubBuildListener;
import org.inria.biomaj.ant.task.BmajTask;
import org.inria.biomaj.session.bank.Bank;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.singleton.BiomajSession;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;

/**
 *<p>This class implements a SubBuildListener of Ant to write the session state 
 * file with update information on each step of Ant.
 * This listener is attached with the mirror file.</p>
 * @author ofilangi<br>
 * @since Biomaj 0.8.0.0
 * 
 */
public class BiomajMirrorListenerHandler implements SubBuildListener{

	/**
	 * Listeners to use while citrina process
	 * @uml.property  name="bankLogger"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private DBBankLogger bankLogger = new DBBankLogger();
	private boolean moved = false;
	private boolean deployed = false;

	public BiomajMirrorListenerHandler() throws BiomajException {
	}

	/**
	 * Retourne la session courante!
	 * @return
	 */
/*	public Bank getXmlBank(Project p) {
		if (bankLogger == null ) {
			throw new BiomajBuildException(p,"session.not.find",null); 
		}
		return bankLogger.getXmlDescriptionBank();
	}
*/
	/**
	 * This method is called when ant script begin.
	 */

	public void buildStarted(BuildEvent arg0) {
		if (arg0!=null) {
			try {
				String dbName = arg0.getProject().getProperty(BiomajConst.dbNameProperty);
				bankLogger.beginSession(arg0);
				Bank bank = BiomajSession.getInstance().getBank(dbName);//getXmlBank(arg0.getProject());
				if (BiomajLogger.getInstance().getLogger(dbName)!=null)
					bank.setLogFile(BiomajLogger.getInstance().getLogger(dbName).getNameFile());
				else {
					BiomajLogger.getInstance().log("Impossible d initialiser le log de "+arg0.getProject().getProperty(BiomajConst.dbNameProperty)+" logger de cette banque n est pas init!");
				}
			} catch (BiomajException be) {
				throw new BiomajBuildException(arg0.getProject(),be);
			}
		}
	}

	/**
	 * 
	 */
	public void buildFinished(BuildEvent arg0) {
		
		if (arg0!=null)
		{
			try {
				// We're probably running an update with an existing production directory
				// but none logged in the db. Deploy will be skipped because filecheck will return
				// no file to download, and so no proddir record will be added in the db even though
				// the session is successful.
				if (moved && !deployed) {
					bankLogger.forceDeploymentIfNeeded(arg0);
				}
				
				bankLogger.endSession(arg0);
			} catch (BiomajException be) {
				System.err.println(be.getLocalizedMessage());
				//throw new BiomajBuildException(arg0.getProject(),be);
			}
		}

	}




	public void messageLogged(BuildEvent arg0) {
		
		if (arg0==null)
			return;
		try {
			// if an exception is caught!
			String bio_error = arg0.getProject().getProperty("biomaj.error");
			if ((bio_error!=null)&&(bio_error.compareTo(arg0.getMessage())==0)) {
				bankLogger.treatmentWhenFail(arg0);
				return;
			}
			try {
				// Message to add a file in xml tree!
				String prefix = BmajTask.filterAddDownloadedFile;
				if (arg0.getMessage().startsWith(prefix)) {
					bankLogger.addDownloadFile(arg0,arg0.getMessage());
					return;
				}
				prefix = BmajTask.filterAddExtractedFile;
				if (arg0.getMessage().startsWith(prefix)) {
					bankLogger.addExtractedFile(arg0,arg0.getMessage());
					return;
				}
				prefix = BmajTask.filterAddProductionFile;
				if (arg0.getMessage().startsWith(prefix)) {
					bankLogger.addFileInProduction(arg0,arg0.getMessage());
					return;
				}
				prefix = BmajTask.filterAddLocalOfflineFile;
				if (arg0.getMessage().startsWith(prefix)) {
					bankLogger.addLocalOfflineFile(arg0,arg0.getMessage());
					return;
				}
				
				prefix = BmajTask.filterAddLocalOnlineFile;
				if (arg0.getMessage().startsWith(prefix)) {
					bankLogger.addLocalOnlineFile(arg0,arg0.getMessage());
					return;
				}
			} catch (BiomajException be) {
				throw new BuildException(be);
			}
			
			if (arg0.getPriority()==Project.MSG_WARN)
				bankLogger.warnMessage(arg0.getMessage(),arg0);
			if (arg0.getPriority()==Project.MSG_ERR)
				bankLogger.errorMessage(arg0.getMessage(),arg0);
			
		} catch (BiomajException be) {
			throw new BiomajBuildException(arg0.getProject(),be);
		}
		
	}

	public void targetStarted(BuildEvent arg0) {
		//Write bank log  
		
		if (arg0.getProject().getProperty(BiomajConst.dbNameProperty)==null)
		{
			String info = "db name property unknow! value of db.name:"+arg0.getProject().getProperty(BiomajConst.dbNameProperty);
			info+="\n (You have to remove \"ftp.\" if you use an old version of citrina!)";
			throw new BuildException(info);
		}		
		try {
			String taskName = arg0.getTarget().getName();
			// File Mirror.xml - target : init
			if (mirrorContext(arg0)&&BiomajConst.initMirrorTarget.equals(arg0.getTarget().getName()))
			{
			} 
			else if (mirrorContext(arg0)&&(taskName.compareTo(BiomajConst.checkTarget)==0 ||
					taskName.equals("directCheck")))
			{
				// If Citrine comes into this target, checkfrequency is passed (first step next checkfrequency)
				bankLogger.mirrorStartedCheckTarget(arg0);
			}
			// 	File Mirror.xml - target : download
			else if (mirrorContext(arg0)&&taskName.compareTo(BiomajConst.downloadTarget)==0)
			{
				bankLogger.mirrorStartedDownloadTarget(arg0);
			}
			//	File Mirror.xml - target : extract
			else if (mirrorContext(arg0)&&taskName.compareTo(BiomajConst.extractTarget)==0)
			{
				bankLogger.mirrorStartedExtractTarget(arg0);
			}
			else if (mirrorContext(arg0)&&taskName.compareTo(BiomajConst.copyTarget)==0)
			{
				bankLogger.mirrorStartedCopyTarget(arg0);
			}
			else if (mirrorContext(arg0)&&taskName.compareTo(BiomajConst.moveTarget)==0)
			{
				bankLogger.mirrorStartedMoveTarget(arg0);
			} else if (mirrorContext(arg0)&&taskName.compareTo(BiomajConst.deployTarget)==0)
			{
				bankLogger.mirrorStartedDeploymentTarget(arg0);
			} else if (mirrorContext(arg0)&&(taskName.compareTo("workflow_control")==0))
			{
				bankLogger.workflowControlStarted(arg0);
			}
		} catch (BiomajException be) {
			throw new BiomajBuildException(arg0.getProject(),be);
		}
	}


	public void targetFinished(BuildEvent arg0) {
		try {
			String taskName = arg0.getTarget().getName();
			if (mirrorContext(arg0)&& (taskName.compareTo(BiomajConst.checkTarget)==0 ||
					taskName.equals("directCheck"))) {
				bankLogger.mirrorFinishedCheckTarget(arg0);
			}
//			File Mirror.xml - target : download
			else if (mirrorContext(arg0)&&taskName.compareTo(BiomajConst.downloadTarget)==0) {
				bankLogger.mirrorFinishedDownloadTarget(arg0);
			}
			//	 	File Mirror.xml - target : extract
			else if (mirrorContext(arg0)&&taskName.compareTo(BiomajConst.extractTarget)==0) {
				bankLogger.mirrorFinishedExtractTarget(arg0);
			} else if (mirrorContext(arg0)&&taskName.compareTo(BiomajConst.copyTarget)==0) {
				bankLogger.mirrorFinishedCopyTarget(arg0);
			} else if (mirrorContext(arg0)&&taskName.compareTo(BiomajConst.moveTarget)==0) {
				bankLogger.mirrorFinishedMoveTarget(arg0);
				moved = true;
			} else if (mirrorContext(arg0)&&taskName.compareTo(BiomajConst.deployTarget)==0) {
				bankLogger.mirrorFinishedDeploymentTarget(arg0);
				deployed = true;
			} else if (mirrorContext(arg0)&&(taskName.compareTo("workflow_control")==0)) {
				bankLogger.workflowControlFinished(arg0);
			}	
		} catch (BiomajException be) {
			throw new BiomajBuildException(arg0.getProject(),be);
		}
	}


	public void subBuildFinished(BuildEvent arg0) {
		
		if (mirrorContext(arg0))
		{
			try {
				bankLogger.mirrorFinnished(arg0);
			} catch (BiomajException be) {
				throw new BiomajBuildException(arg0.getProject(),be);
			}
		}
	}


	public void subBuildStarted(BuildEvent arg0) {
	}
	
	
	
	public void taskStarted(BuildEvent arg0) {
		
		//elementary test
		if ((arg0==null)||(arg0.getTarget()==null)||(arg0.getTarget().getName().trim().compareTo("")==0))
			return;
		try {
			String taskName = arg0.getTask().getTaskName();
			if (BiomajConst.fileCheckTask.compareTo(taskName)==0)
				bankLogger.mirrorStartedFileCheckTask(arg0);
			else if (BiomajConst.releaseTask.compareTo(taskName)==0 ||
					taskName.equals("bmaj-directfilecheck"))
				bankLogger.mirrorStartedGetReleaseTask(arg0);
		} catch (BiomajException be) {
			throw new BiomajBuildException(arg0.getProject(),be);
		}
	}
	
	public void taskFinished(BuildEvent arg0) {
		
		if ((arg0==null)||(arg0.getTarget()==null)||(arg0.getTarget().getName().trim().compareTo("")==0))
			return;
		try {
			String taskName = arg0.getTask().getTaskName();
			if (BiomajConst.fileCheckTask.compareTo(taskName)==0)
				bankLogger.mirrorFinishedFileCheckTask(arg0);
			else if (BiomajConst.releaseTask.compareTo(taskName)==0 ||
					taskName.equals("bmaj-directfilecheck"))
				bankLogger.mirrorFinishedGetReleaseTask(arg0);
			else if (BiomajConst.versionsmanagementTask.compareTo(taskName)==0)
				bankLogger.versionsmanagementFinishedTask(arg0);
		} catch (BiomajException be) {
			throw new BiomajBuildException(arg0.getProject(),be);
		}

		//else 
		//arg0.getProject().log("NO MATCH:"+arg0.getTask().getTaskName(),Project.MSG_WARN);
	}


	/**
	 * True if the event is in a mirror context (mirror.xml)
	 * @param arg0
	 * @return
	 */
	protected Boolean mirrorContext(BuildEvent arg0) {

		if (BiomajConst.mirrorProject.compareTo(arg0.getProject().getName())==0)
			return true;

		return false;

	}

	/****************************************/
	/* Modifier le statefile offline        */

	
	public void startPostProcess(Project pj,String nameMetaProcess,String block) {
		try {
			bankLogger.handleOfflineStartPostProcess(pj,nameMetaProcess,block);
		} catch (BiomajException be) {
			throw new BiomajBuildException(pj,be);
		}
	}
	
	
	public void finishedPostProcess(Project pj, boolean wasLaunched) {
		try {
			bankLogger.handleOfflineEndPostProcess(pj);
//			bankLogger.setCleanMeta(wasLaunched);
		} catch (BiomajException be) {
			throw new BiomajBuildException(pj,be);
		}
	}
	
	public void startPreProcess(Project pj,String nameMetaProcess) {
		try {
			bankLogger.handleOfflineStartPreProcess(pj,nameMetaProcess);
		} catch (BiomajException be) {
			throw new BiomajBuildException(pj,be);
		}
	}
	
	
	public void finishedPreProcess(Project pj) {
		try {
			bankLogger.handleOfflineEndPreProcess(pj);
		} catch (BiomajException be) {
			throw new BiomajBuildException(pj,be);
		}
	}

	public void startRemoveProcess(Project pj,String nameMetaProcess,String block) {
		try {
			bankLogger.handleOfflineStartRemoveProcess(pj,nameMetaProcess,block);
		} catch (BiomajException be) {
			throw new BiomajBuildException(pj,be);
		}
	}
	
	
	public void finishedRemoveProcess(Project pj) {
		try {
			bankLogger.handleOfflineEndRemoveProcess(pj);
		} catch (BiomajException be) {
			throw new BiomajBuildException(pj,be);
		}
	}
	
	public DBBankLogger getLogger() {
		return bankLogger;
	}
}
