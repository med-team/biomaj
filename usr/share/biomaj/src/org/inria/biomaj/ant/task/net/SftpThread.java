package org.inria.biomaj.ant.task.net;

import java.util.Date;

import org.apache.tools.ant.Project;
import org.inria.biomaj.internal.ant.task.net.RemoteCommandImpl;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.internal.ant.task.net.SftpImpl;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajException;

/**
 * Thread that runs a sftp download task.
 * 
 * @author rsabas
 *
 */
public class SftpThread extends Thread {

	private BmajSftp task;
	private String threadName;
	
	public SftpThread(BmajSftp task, int pos) {
		this.task = task;
		threadName = "sftpthead" + pos;
	}
	
	/**
	 * Runs the parallel download task.
	 * Asks Bmajsftp for files to download and stops once there's no left.
	 */
	@Override
	public void run() {
		
		RemoteCommandImpl pro = new SftpImpl(task);
		try {
			long totaltime = 0;
			int fileCount = 0;
			pro.init(task.getServer(), task.getPort(), task.getUserId(), task.getPassword());
			Date m_chrono = new Date();
			RemoteFile rf = null;
			while ((rf = task.getFile()) != null) {
				int j = 0;
				task.log(threadName + " downloading:"+rf.getAbsolutePath(),Project.MSG_VERBOSE);
				while ((!pro.getFile(task.getRemoteDir(), rf.getAbsolutePath(), task.getToDir(),rf.getName())) && (j<WgetThread.NB_TRY)) {   	
					j++;
					task.log(j+" try downloading:"+rf.getAbsolutePath(),Project.MSG_INFO);
				}
				if (j > WgetThread.NB_TRY)
					throw new BiomajException("Could not download " + rf.getAbsolutePath() + ", " + WgetThread.NB_TRY + " tries failed");
				fileCount++;
				Date end = new Date();
				Long l = new Long(end.getTime() - m_chrono.getTime());
				totaltime+=l;
				task.addDownloadFile(rf.getAbsolutePath(), Long.toString(rf.getDate().getTime()), Long.toString(rf.getSize()));
			}
			task.log(threadName + " : " + fileCount + " files downloaded in " + totaltime,Project.MSG_VERBOSE);
		} catch (BiomajException e) {
			throw new BiomajBuildException(task.getProject(),e);
		} catch (Exception e) {
			throw new BiomajBuildException(task.getProject(),e);
		} finally {
			if (pro != null)
				pro.disconnect();
		}

	}

}
