/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task.net;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Semaphore;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Execute;
import org.apache.tools.ant.taskdefs.LogStreamHandler;
import org.apache.tools.ant.types.Commandline;
import org.inria.biomaj.ant.task.BmajExtract;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * Tache ANt pour le telechargements des fichiers ftp et http :
 * Principe: 
 * Classe principale BmajWget qui execute N (files.num.thread) thread wget
 * 
 * Les threads Wget definissent NB_TRY tentative pour le telachargement de fichier,
 * ci celui n est tjs pas telecharger, la classe principale BmajWegt remte le fichier dans la liste de fichier a telecharger
 * (NB_TENTATIV_GLOBAL fois) si celui ci n est tjs pas telecharger on l ajoute a une liste de fichier Erreur !
 * 
 * En fin de telechargement (tous les fichiers valide qui sont telecharger), une erreur stop l application si la liste de fichier erreur est non vide ! 
 * 
 * @author ofilangi
 * @version Biomaj 0.9
 * @since Biomaj 0.8 /Citrina 0.5
 */
public class WgetThread extends Thread {

	private final static String OPTIONS_WGET_DEFAULT_STATIC =  "--tries=inf --wait=5 --random-wait --passive-ftp --timeout=50" ;

	/**
	 * @uml.property  name="oPTIONS_WGET_DEFAULT"
	 */
	private String OPTIONS_WGET_DEFAULT = OPTIONS_WGET_DEFAULT_STATIC ;

	/**
	 * The workflow is lock by default!
	 * @uml.property  name="lockWorkflow"
	 */
	private Semaphore lockWorkflow = new Semaphore(0);

	/**
	 * @uml.property  name="nb_try"
	 */
	public static final int NB_TRY = 10;

	/**
	 * @uml.property  name="numThread"
	 */
	private int numThread;
	/**
	 * @uml.property  name="threadName"
	 */
	private String threadName;
	/**
	 * @uml.property  name="wget"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private BmajWget wget;

	public WgetThread(int numThread,BmajWget wget) {

		this.numThread = numThread+1;
		this.threadName = "WgetThread"+this.numThread;

		setThreadName(threadName);
		this.wget = wget;
	}


	public void run() throws BiomajBuildException {

		String decal = "Thread "+this.numThread+" : ";
		//throw new NullPointerException();
		//String optionWgetProxy ="";


		if (wget.getProject().getProperties().containsKey(BiomajConst.optionWgetProperty)) {
			String opt = wget.getProject().getProperty(BiomajConst.optionWgetProperty);
			if (opt.contains("--cut-dirs=")||opt.contains("--directory-prefix=")||opt.contains("-a")) {
				wget.log("Thread["+numThread+"] "+BiomajConst.optionWgetProperty+" can't redefine theses options [--cut-dirs --directory-prefix -a]", Project.MSG_WARN);
			} else {
				OPTIONS_WGET_DEFAULT = opt;
			}
		} 


		/**
		 * Add proxy func : 29/06/2007 O.Filangi
		 */
		/*
		if (wget.getProject().getProperties().containsKey(BiomajConst.proxyUser)&&wget.getProject().getProperties().containsKey(BiomajConst.proxyPassword)) {
			String userProxy=wget.getProject().getProperty(BiomajConst.proxyUser);
			String passwordProxy=wget.getProject().getProperty(BiomajConst.proxyPassword);

			optionWgetProxy = " --proxy-user="+userProxy+" --proxy-password="+passwordProxy+" ";
		}

		String optionWgetAuth ="";
		 */ 
		//	try {

		Commandline cmd = new Commandline();

		String pathWget="";
		try {
			pathWget = BiomajInformation.getInstance().getProperty(BiomajInformation.WGET);
		} catch (BiomajException ioe) {
			throw new BiomajBuildException(wget.getProject(),ioe);
		}
		cmd.setExecutable(pathWget);

		String offlineDirectory = wget.getProject().getProperty("offline.dir");

		//delete log file
		File logFileWget = new File(wget.getProject().getProperty("wget.out.log"));
		if (logFileWget.exists())
			logFileWget.delete();


		Execute execute = new Execute(new LogStreamHandler(wget, Project.MSG_INFO,Project.MSG_ERR),null);

		/*while(wget.thereAreDownloadFile()) {
			RemoteFile rf = wget.getFileToDownload();

			if (rf == null)
				continue;
		*/
		RemoteFile rf = null;
		while((rf = wget.getFileToDownload()) != null) {
			int numberTentative=0;
//			wget.log("[Thread " + numThread + "] Downloading : "+rf.getName() + " [" + wget.getDownloaded() + "/" + wget.getNbFileDownload() + "]",Project.MSG_INFO);
			wget.log("downloading:"+rf.getName(),Project.MSG_VERBOSE);
			boolean isOk = false;
			int code_retour = -1;

			while ((!isOk)&&(numberTentative<NB_TRY)) {
				isOk = false;
				int cutdir=0;
				Commandline newcmd = (Commandline) cmd.clone();

				String wherePutFile = offlineDirectory;

				if (wget.getProtocol().compareTo(RemoteCommand.HTTP_PROTOCOL)==0)
				{
					wherePutFile = offlineDirectory ;// + "/" + BiomajUtils.getRelativeDirectory(rf.getAbsolutePath());
					cutdir = wget.computCuteDir(wget.getRemotedir());

					/*
					//Ajout ofilangi pour authentification 29/06/2007
					if (wget.getProject().getProperties().containsKey(BiomajConst.userNameProperty)) {
						//If anonymous, don't need to add options to wget
						String user = wget.getProject().getProperty(BiomajConst.userNameProperty);
						String password = wget.getProject().getProperty(BiomajConst.passwordProperty);
						if (user.compareTo("")!=0) {
							optionWgetAuth = " --http-user="+user+" --http-password="+password;
						}
					}
					 */
				}
				else {
					if (rf.isLink()) {
						String absPath = BiomajUtils.getRelativeDirectory(rf.getAbsolutePath());
						wherePutFile = offlineDirectory + "/" + absPath;
						/*Correction erreur de link 19 juin 2007
							-------------------------------------
							 Exemple : link = ../../MapView/Mus_musculus/sequence/current/initial_release/bes_seq.md.gz

						 * on se retroure avec un lien $repdir/initial_release/bes_seq.md.gz
						 * 
						 *  on doit donc prendre en compte les repertoire qui ne sont pas coupe par cut_dir de wget!
						 *  
						 */
						//Calcul du cut-dir avec le linkName
						String stringForCutDir = wget.getRemotedir()+"/"+rf.getLinkName() ;
						//remove les ../..etc...
						String trueCutDir = BiomajUtils.getNormalizedPath(stringForCutDir);
						cutdir = wget.computCuteDir(trueCutDir);
					} else
						cutdir = wget.computCuteDir(wget.getRemotedir());

					//Ajout ofilangi pour authentification 29/06/2007
					/*
					if (wget.getProject().getProperties().containsKey(BiomajConst.userNameProperty)) {
						//If anonymous, don't need to add options to wget
						String user = wget.getProject().getProperty(BiomajConst.userNameProperty);
						String password = wget.getProject().getProperty(BiomajConst.passwordProperty);
						if (user.compareTo("")!=0) {
							optionWgetAuth = " --ftp-user="+user+" --ftp-password="+password+" ";
						}
					}
					 */
				}

				String options_line= OPTIONS_WGET_DEFAULT+" -N -c -x -nH --cut-dirs="+Integer.toString(cutdir);
				//Fatal errors like ''connection refused'' or ''not found'' (404)
				options_line +=" --directory-prefix=" + wherePutFile + " -a "+ wget.getProject().getProperty("wget.out.log")+this.numThread;
				//options_line +=optionWgetAuth+" "+optionWgetProxy;

				String serverAndPort = wget.getServer();
				if (wget.getProtocol().equals(RemoteCommand.FTP_PROTOCOL)) {
					if (serverAndPort.endsWith("/"))
						serverAndPort = serverAndPort.substring(0, serverAndPort.lastIndexOf("/")) + ":" + wget.getPort() + "/";
					else
						serverAndPort += ":" + wget.getPort();
				}
				
				if (!rf.isLink())
					options_line = options_line + " " + wget.getProtocol()+"://"+serverAndPort+wget.getRemotedir()+"/"+rf.getAbsolutePath(); 
				else
					options_line = options_line + " " + wget.getProtocol()+"://"+serverAndPort+wget.getRemotedir()+"/"+rf.getLinkName();

				wget.log("wget options:"+options_line,Project.MSG_VERBOSE);
				newcmd.createArgument().setLine(options_line);

				//creation des sous repertoires si il y a besoin!
				String targetDirectory = offlineDirectory+"/"+BiomajUtils.getRelativeDirectory(rf.getAbsolutePath());
				BiomajUtils.createSubDirectories(targetDirectory);

				execute.setCommandline(newcmd.getCommandline());

				String message = "";
				for (int k=0;k<execute.getCommandline().length;k++)
					message = message+execute.getCommandline()[k]+" ";

				wget.log("Thread["+numThread+"]"+message,Project.MSG_DEBUG);

				try {
					execute.execute();
				} 
				catch (Throwable b) {
					BiomajLogger.getInstance().log(b.getLocalizedMessage());
					wget.log(b.getLocalizedMessage(),Project.MSG_ERR);
					code_retour  = 1;
				}
				
				code_retour  = execute.getExitValue();

				if ((code_retour==0)&&(rf.isLink())) {

					String nameLink = BiomajUtils.getNameFile(rf.getLinkName());
					if (nameLink.compareTo(rf.getName())!=0) {
						wget.log(decal+"Change name link....["+nameLink+"]---->["+rf.getName()+"]",Project.MSG_VERBOSE);
						File s = new File(targetDirectory+"/"+nameLink);
						File d = new File(targetDirectory+"/"+rf.getName());
						try {
							if (BiomajUtils.move(s, d))
								wget.log("["+threadName+"]"+"Ok",Project.MSG_VERBOSE);
							else 
							{
								wget.log("["+threadName+"]"+"KO, Can't change name link..["+nameLink+"]---->["+rf.getName()+"]",Project.MSG_ERR);
								wget.setErrorOnChild(rf);
								break;
							}
						} catch (IOException ioe) {
							numberTentative++;
							wget.log(decal+"problem to rename:"+rf.getName()+"-->"+nameLink+" ("+numberTentative+"/"+NB_TRY+") :"+ioe.getMessage(),Project.MSG_WARN);
							continue;
						}
					}
				}

				boolean notExiste = !new File(targetDirectory+"/"+rf.getName()).exists();

				if ((code_retour!=0)||(notExiste)) {
					if (code_retour!=0)
						wget.log(decal+"problem to download file:"+rf.getAbsolutePath()+" code retour:"+code_retour+" ("+numberTentative+"/"+NB_TRY+")",Project.MSG_VERBOSE);
					else 
						wget.log(decal+"problem to download file:"+rf.getAbsolutePath()+" not find."+" ("+numberTentative+"/"+NB_TRY+")",Project.MSG_WARN);

					numberTentative++;

					if (numberTentative>=NB_TRY)
					{
						wget.setErrorOnChild(rf);
						wget.log("["+threadName+"]"+"Can't download :["+rf.getAbsolutePath()+"]",Project.MSG_DEBUG);
						//throw new BiomajBuildException(wget.getProject(),"wget.error.download",rf.getAbsolutePath(),new Exception());
					}


					wget.log("["+threadName+"]"+numberTentative+" try downloading:"+rf.getAbsolutePath(),Project.MSG_VERBOSE);
					if (BiomajUtils.delete(offlineDirectory+"/"+rf.getAbsolutePath()))
						wget.log ("["+threadName+"]"+"delete file:"+offlineDirectory+"/"+rf.getAbsolutePath(),Project.MSG_DEBUG);
					// Add a test to check if remote file is the same
					/**
					 * 
					 * On enleve ce test qui est source d erreur et enleve des perf...
					 * --> cout d une connection au serveur distant pour verifier les attributs du fichiers !
					wget.diffRemoteLocalFile(rf.getAbsolutePath(),rf.getDate().getTime(),rf.getSize());
					 */
					//Attente entre 0 et qq sec pour retelecharger
					long t = (long)((double)5000*Math.random());
					//wget.log("["+threadName+"]"+"[t="+t+"]", Project.MSG_DEBUG);
					long i= 0;
					while ((i++)<t) {}
					continue;
				}

				//Download is ok!
				wget.log("["+threadName+"]"+"["+rf.getAbsolutePath()+"] file downloaded! crc verification if need ... ",Project.MSG_VERBOSE);
				isOk = true;
				//if the file is a compressed file we have to verify the crc
				File f = new File(offlineDirectory+"/"+rf.getAbsolutePath());
				try {
				if (BmajExtract.containsCompressedFormat(rf.getAbsolutePath()))
				{
					wget.log("["+threadName+"]"+rf.getAbsolutePath()+" : this file is a compressed file :",Project.MSG_VERBOSE);
					try {
						isOk=BmajExtract.check(f,wget);
					} catch (BiomajException be) {
						BiomajLogger.getInstance().log(be);
						throw new BiomajBuildException(wget.getProject(),be);
					}
				} else
					wget.log("["+threadName+"]"+"no need!",Project.MSG_VERBOSE);
				} catch (BiomajException e) {
					throw new BiomajBuildException(wget.getProject(),e);
				}
				if (isOk)
				{
					//wget.log(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE+decal + Integer.toString(i+1)+"/"+Integer.toString(lFiles.size())+" file(s)",Project.MSG_INFO);
					wget.logDownload(numThread);
					wget.addDownloadFile(rf.getAbsolutePath(), Long.toString(rf.getDate().getTime()),Long.toString(rf.getSize()));
					wget.downloadIsOk(rf);
					wget.log("["+threadName+"]"+"ok, next download",Project.MSG_VERBOSE);
				}
				else
				{
					wget.log("["+threadName+"]"+"bad crc....",Project.MSG_VERBOSE);
					if (!BiomajUtils.delete(offlineDirectory+"/"+rf.getAbsolutePath()))
						wget.log (decal+"File d'ont exist (can't delete this):"+offlineDirectory+"/"+rf.getAbsolutePath(),Project.MSG_WARN);
					else
						wget.log (decal+"File delete:"+offlineDirectory+"/"+rf.getAbsolutePath(),Project.MSG_WARN);

					//Ajout O.F en cas d erreur de tentative on arrete!
					if (numberTentative>=NB_TRY) {
						wget.setErrorOnChild(rf);
						wget.log("["+threadName+"]"+"Can't download :["+rf.getAbsolutePath()+"]",Project.MSG_DEBUG);
						break;
						//throw new BiomajBuildException(wget.getProject(),"wget.error.download",rf.getAbsolutePath(),new Exception());
					}
					numberTentative++;
				}
			}
		}

		wget.log("Thread ["+threadName+"] dead",Project.MSG_INFO);

		/*	} catch (Exception e) {
			wget.setErrorOnChild(e);
			//throw new BiomajBuildException(wget.getProject(),e);
		} finally {
			release();
		}
		 */
		release();
	}

	protected void synchr() {
		try {
			wget.log("acquire:"+getThreadName()+" nb semaphore:"+lockWorkflow.availablePermits(),Project.MSG_VERBOSE);
			lockWorkflow.acquire();
			wget.log("acquire ok:"+getThreadName()+" nb semaphore:"+lockWorkflow.availablePermits(),Project.MSG_VERBOSE);
		} catch (InterruptedException ex) {
			BiomajLogger.getInstance().log(ex);
			System.exit(-1);
		}
	}

	protected void release() {
		wget.log("release:"+getThreadName(),Project.MSG_VERBOSE);
		lockWorkflow.release();
	}

	/**
	 * @return  the threadName
	 * @uml.property  name="threadName"
	 */
	public String getThreadName() {
		return threadName;
	}

	/**
	 * @param threadName  the threadName to set
	 * @uml.property  name="threadName"
	 */
	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}
}

