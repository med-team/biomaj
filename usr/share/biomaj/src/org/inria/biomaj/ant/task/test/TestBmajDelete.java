package org.inria.biomaj.ant.task.test;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.task.BmajDelete;
import org.inria.biomaj.utils.BiomajUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;

/**
 * Test class for BmajDelete
 * 
 * @author rsabas
 *
 */
public class TestBmajDelete {

	private static final String DIR = BiomajUtils.getBiomajRootDirectory() + "/testdir";
	private static final String EX_FILE = BiomajUtils.getBiomajRootDirectory() + "/testexclude";
	private static List<File> excludz = new ArrayList<File>();
	
	@BeforeClass
	public static void setup() {
		File dir = new File(DIR);
		BiomajUtils.deleteAll(new File(DIR));
		dir.mkdir();
		
		File ex = new File(EX_FILE);
		ex.delete();
		
		for (int i = 0; i < 5; i++) {
			File f = new File(DIR + "/test" + i);
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		for (int i = 0; i < 3; i++) {
			File f = new File(DIR + "/ex" + i);
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			excludz.add(f);
		}
		
		createListing(ex, excludz);
		
	}
	
	private static void createListing(File list, List<File> filz) {
		PrintWriter pw;
		try {
			pw = new PrintWriter(list);
			for (File f : filz) {
				pw.println(f.getAbsolutePath());
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Executes the tasks and checks that only the specified
	 * files in excludz list remain.
	 */
	@Test
	public void testFilesAreDeleted() {
		BmajDelete del = new BmajDelete();
		del.setProject(new Project());
		del.setDir(DIR);
		del.setListingExclude(EX_FILE);
		del.execute();
		
		File f = new File(DIR);
		assertTrue(f.list().length == excludz.size());
		for (File fl : excludz)
			assertTrue(fl.exists());
	}
	
	@AfterClass
	public static void cleanup() {
		BiomajUtils.deleteAll(new File(DIR));
		new File(EX_FILE).delete();
	}
}
