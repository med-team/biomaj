/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task.net;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.gmod.biomaj.ant.task.InputValidation;
import org.inria.biomaj.ant.logger.BiomajConsoleLogger;
import org.inria.biomaj.ant.task.BmajTask;
import org.inria.biomaj.internal.ant.task.net.FtpImpl;
import org.inria.biomaj.internal.ant.task.net.HttpImpl;
import org.inria.biomaj.internal.ant.task.net.RemoteCommandImpl;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * Tache ANt pour le telechargements des fichiers ftp et http :
 * Principe: 
 * Classe principale BmajWget qui execute N (files.num.thread) thread wget
 * 
 * Les threads Wget definissent NB_TRY tentative pour le telachargement de fichier,
 * ci celui n est tjs pas telecharger, la classe principale BmajWegt remte le fichier dans la liste de fichier a telecharger
 * (NB_TENTATIV_GLOBAL fois) si celui ci n est tjs pas telecharger on l ajoute a une liste de fichier Erreur !
 * 
 * En fin de telechargement (tous les fichiers valide qui sont telecharger), une erreur stop l application si la liste de fichier erreur est non vide ! 
 * 
 * @author ofilangi
 * @version Biomaj 0.9
 * @since Biomaj 0.8 /Citrina 0.5
 */
public class BmajWget extends BmajTask {

	/**
	 * numbers of file to download
	 * @uml.property  name="nbFileDownload"
	 */
	private int nbFileDownload = 0;
	
	private int downloaded = 0;

	/**
	 * Liste pour les sous listes des fichiers a telecharger
	 */
	//private Vector<Vector> listFileByThread = new Vector<Vector>();
	//valeur : Nbre de tentative de telechargement / nom du fichier a telecharger
	
	public class TwoValue {
		public int nbTentative  = 0;
		public boolean isTreat = false;
	} 
	
	/**
	 * @uml.property  name="attributFilesDownload"
	 * @uml.associationEnd  inverse="this$0:org.inria.biomaj.ant.task.net.BmajWget$TwoValue" qualifier="rf:org.inria.biomaj.internal.ant.task.net.RemoteFile org.inria.biomaj.ant.task.net.BmajWget$TwoValue"
	 */
	private Map<RemoteFile,TwoValue> attributFilesDownload = new HashMap<RemoteFile,TwoValue>();
	public static final int NB_TENTATIV_GLOBAL = 5;
	
	/**
	 * @uml.property  name="protocol"
	 */
	private String protocol ;
	/**
	 * @uml.property  name="server"
	 */
	private String server ;
	/**
	 * @uml.property  name="port"
	 */
	private Integer port ;
	/**
	 * @uml.property  name="userid"
	 */
	private String userid ;
	/**
	 * @uml.property  name="password"
	 */
	private String password;
	/**
	 * @uml.property  name="remotedir"
	 */
	private String remotedir;
	/**
	 * @uml.property  name="listing"
	 */
	private String listing;
	/**
	 * @uml.property  name="toDir"
	 */
	private String toDir;

	private Vector<String> treeMapErrorDownload = new Vector<String>();

	/**
	 * @uml.property  name="useWget"
	 */
	private boolean useWget = false;

	@Override
	public void execute() throws BuildException {
		InputValidation.checkString(getProject(),server, "");
		if (port == null)
			throw new BiomajBuildException(getProject(),"error.remote.port.malformed",new Exception());

		InputValidation.checkString(getProject(),userid, "");
		InputValidation.checkString(getProject(),password, "");
		//InputValidation.checkString(getProject(),remotedir, "");
		InputValidation.checkString(getProject(),listing, "");
		InputValidation.checkString(getProject(),toDir, "");

		if (useWget)
			executeWithWget();
		else
			executeWithoutWget();

	}

	protected void executeWithoutWget() {
		log("FTPClient method",Project.MSG_INFO);
		BufferedReader br;
		Vector<RemoteFile> files      = new Vector<RemoteFile>();
		try {
			br = new BufferedReader(new FileReader(listing));
			String line;
			while ((line=br.readLine())!= null) {
				files.add(new RemoteFile(line));
			}
		} catch (IOException e) {
			throw new BiomajBuildException(getProject(),e);
		} catch (BiomajException be) {
			throw new BuildException(be);
		} catch (ParseException e) {
			throw new BiomajBuildException(getProject(),e);
		} 
		RemoteCommandImpl pro = null ;
		try {
			if (protocol.compareTo(RemoteCommand.FTP_PROTOCOL)==0)
				pro = new FtpImpl(this);
			else if (protocol.compareTo(RemoteCommand.HTTP_PROTOCOL)==0)
				pro = new HttpImpl(this);
			else
				throw new BiomajBuildException(getProject(),"error.implementation.find",protocol,new Exception());
			long totaltime = 0;
			pro.init(server,port,userid,password);
			Date m_chrono = new Date();
			for (int i=0;i<files.size();i++) {
				RemoteFile rf = files.get(i);
				int j=0;
				log("downloading:"+rf.getAbsolutePath(),Project.MSG_VERBOSE);
				while ((!pro.getFile(remotedir,rf.getAbsolutePath(),toDir,rf.getAbsolutePath()))&&(j<WgetThread.NB_TRY)) {   	
					j++;
					log(j+" try downloading:"+rf.getAbsolutePath(),Project.MSG_INFO);
				}
				Date end = new Date();
				Long l = new Long(end.getTime() - m_chrono.getTime());
				totaltime+=l;
				addDownloadFile(rf.getAbsolutePath(), Long.toString(rf.getDate().getTime()), Long.toString(rf.getSize()));
				log(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE+Integer.toString(i+1)+"/"+Integer.toString(files.size())+" file(s) downloaded!",Project.MSG_INFO);
			} 
			log(Integer.toString(files.size())+"/"+Integer.toString(files.size())+" file(s) downloaded! time:"+BiomajUtils.timeToString(totaltime),Project.MSG_INFO);
		} catch (BiomajException e) {
			throw new BiomajBuildException(getProject(),e);
		} catch (Exception e) {
			throw new BiomajBuildException(getProject(),e);
		} finally {
			if (pro != null)
				pro.disconnect();
		}

	}

	protected void executeWithWget() {
		log("Wget method",Project.MSG_INFO);
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(listing));
			String line;
			while ((line=br.readLine())!= null) {
				attributFilesDownload.put(new RemoteFile(line),new TwoValue());
			}
		} catch (IOException e) {
			throw new BiomajBuildException(getProject(),e);
		} catch (BiomajException be) {
			throw new BuildException(be);
		} catch (ParseException e) {
			throw new BiomajBuildException(getProject(),e);
		} 

		this.setNbFileDownload(attributFilesDownload.size());

		InputValidation.checkString(getProject(),this.getProject().getProperty("offline.dir"), "offline directory is not specified!");
		InputValidation.checkString(getProject(),this.getProject().getProperty("wget.out.log"), "file for log messages from wget is not specified!");   	

		int numThread = 1;
		try {
			String filesNumThread = BiomajUtils.getProperty(this.getProject(),BiomajConst.filesNumThreadProperty);
			numThread = new Integer(filesNumThread);
		}
		catch (BiomajException e) {
			log("files.num.thread is not defined ! New value is : 1",Project.MSG_WARN);
		}

		if ( numThread <= 0 ){
			log("Bad value for files.num.thread ("+numThread+"). New value is : 1",Project.MSG_WARN);
			numThread = 1;
		}

		if (nbFileDownload < numThread) {
			numThread = nbFileDownload;
		}
		log("Starting " + numThread + " thread(s) to download " + nbFileDownload + " file(s)",Project.MSG_INFO);
		

		int cptWget = 0;
		Vector<WgetThread> lWget = new Vector<WgetThread>();
		for (int i = 0;i<numThread;i++) {

			WgetThread wt = new WgetThread(cptWget,this);
			lWget.add(wt);
			wt.start();
			cptWget++;
		}

		for (int i=0;i<lWget.size();i++) {
			lWget.get(i).synchr();
		}
		
		if (errorDetected()) {
			log("Biomaj haves errors for files:",Project.MSG_ERR);
			for (String file : treeMapErrorDownload) {
				log(file,Project.MSG_ERR);
			}
			throw new BiomajBuildException(getProject(),"wget.error.download",new Exception());
		}
	}

//	Yo ajout de methode pour check 2
	protected void diffRemoteLocalFile(String downloadFile, long downloadTime, long downloadSize){

		Boolean boolDiff = false;
		FTPClient client = new FTPClient();
		try {
			client.connect(server, port);
			int nbTent = 0 ;
			while ((!client.login(userid, password))&&(nbTent<WgetThread.NB_TRY))
				nbTent++ ;
			if (nbTent>=WgetThread.NB_TRY) {
				throw new BiomajBuildException(getProject(),"ftp.loggin.error",client.getReplyString(),new Exception());
			}	
			client.changeWorkingDirectory(remotedir);

			FTPFile[] remoteFiles = client.listFiles(downloadFile);

			// Ligne barbare pour pouvoir comparer time de Biomaj et du remoteFile dans les meme conditions
			Date d ;
			if (remoteFiles.length>0) {
				//d = BiomajUtils.stringToDate(BiomajUtils.dateToString(new Date(remoteFiles[0].getTimestamp().getTime().getTime())));
				d = remoteFiles[0].getTimestamp().getTime() ;
			}
			else {
				getProject().log("Can't compare files listfiles :get 0 files on remote dir.",Project.MSG_VERBOSE);
				getProject().log("client code:"+client.getReplyCode()+" string:"+client.getReplyString(),Project.MSG_VERBOSE);
				return;
			}

			if ( downloadFile.compareTo(remoteFiles[0].getName())!=0)
			{
				getProject().log("Name file change: [local:"+downloadFile+"] [server:"+remoteFiles[0].getName()+"]",Project.MSG_WARN);
				boolDiff = true;
			}

			if ( new Long(downloadSize).longValue() != remoteFiles[0].getSize())
			{
				getProject().log("["+downloadFile+"] Size file change: [local:"+Long.toString(downloadSize)+"] [server:"+Long.toString(remoteFiles[0].getSize())+"]",Project.MSG_WARN);
				boolDiff = true;
			}

			if ( new Long(downloadTime).longValue() != d.getTime())
			{
				getProject().log("["+downloadFile+"] Time file change: [local:"+Long.toString(downloadTime)+"] [server:"+Long.toString(d.getTime())+"]",Project.MSG_WARN);
				boolDiff = true;
			}

			if ( boolDiff.booleanValue() )
			{
				getProject().log("Launch a new update cycle",Project.MSG_ERR);
				throw new BiomajBuildException(getProject(),"wget.error.newRemoteFile",downloadFile,new Exception());
			} 
			if(client.isConnected()) {
				try {
					client.disconnect();
				} catch(IOException ioe) {
					getProject().log("Cant disconnect from server :"+ioe.getMessage(),Project.MSG_VERBOSE);
				}
			}	
		} catch (Exception se) {
			throw new BiomajBuildException(getProject(),"ftp.loggin.error",se.getMessage(),new Exception());
		}
	}
//	Fin Yo

	protected int computCuteDir(String dir) {
		String t = dir.trim();
		t = t.replaceAll("\\.\\./", "");
		String[] res = t.split("/");
		int cutdir=0;
		for (int i=0;i<res.length;i++)
			if (res[i].trim().compareTo("")!=0)
				cutdir++;	
		return cutdir;
	}

	protected String computePathLinkFile(String pathLink,int cutdir) {
		String[] t = pathLink.split("/");
		String res="";

		for (int i=cutdir-1;i<t.length;i++)
			res=res+"/"+t[i];

		if (res.compareTo("")==0)
			res = pathLink;
		else
			res.replaceFirst("/", "");
		return res;
	}


	/**
	 * Les fils wgetThread appele cette methode pour prevenir d'une erreur
	 *
	 */
	protected synchronized void setErrorOnChild(RemoteFile rf) {
		
		if (attributFilesDownload.containsKey(rf)) {
			if (attributFilesDownload.get(rf).nbTentative>=BmajWget.NB_TENTATIV_GLOBAL) {
				treeMapErrorDownload.add(rf.getAbsolutePath());
				attributFilesDownload.remove(rf);
				return;
			}
			log("ONE MORE TIME TENTATIV GLOBAL TO DOWNLOAD:"+rf.getAbsolutePath()+" ("+attributFilesDownload.get(rf).nbTentative+" tries)",Project.MSG_VERBOSE);
			attributFilesDownload.get(rf).isTreat=false;
		} else
			treeMapErrorDownload.add(rf.getAbsolutePath());
	}

	public boolean errorDetected() {
		return treeMapErrorDownload.size()!=0;
	}

	/**
	 * @return  the listing
	 * @uml.property  name="listing"
	 */
	public String getListing() {
		return listing;
	}

	/**
	 * @param listing  the listing to set
	 * @uml.property  name="listing"
	 */
	public void setListing(String listing) {
		this.listing = listing;
	}

	/**
	 * @return  the password
	 * @uml.property  name="password"
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password  the password to set
	 * @uml.property  name="password"
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return  the remotedir
	 * @uml.property  name="remotedir"
	 */
	public String getRemotedir() {
		return remotedir;
	}

	/**
	 * @param remotedir  the remotedir to set
	 * @uml.property  name="remotedir"
	 */
	public void setRemotedir(String remotedir) {
		this.remotedir = remotedir;
	}

	/**
	 * @return  the server
	 * @uml.property  name="server"
	 */
	public String getServer() {
		return this.server;
	}

	/**
	 * @param server  the server to set
	 * @uml.property  name="server"
	 */
	public void setServer(String server) {
		this.server = server;
	}

	/**
	 * @return the protocol
	 * @uml.property  name="protocol"
	 */
	public String getProtocol() {
		return this.protocol;
	}

	/**
	 * @return  the toDir
	 * @uml.property  name="toDir"
	 */
	public String getToDir() {
		return toDir;
	}

	/**
	 * @param toDir  the toDir to set
	 * @uml.property  name="toDir"
	 */
	public void setToDir(String toDir) {
		this.toDir = toDir;
	}

	/**
	 * @return  the userid
	 * @uml.property  name="userid"
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * @param userid  the userid to set
	 * @uml.property  name="userid"
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}

	public void setPort(String port) {
		this.port = Integer.valueOf(port);
	}
	
	public int getPort() {
		return this.port;
	}

	/**
	 * @param protocol  the protocol to set
	 * @uml.property  name="protocol"
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	/**
	 * @return  the nbFileDownload
	 * @uml.property  name="nbFileDownload"
	 */
	public int getNbFileDownload() {
		return this.nbFileDownload;
	}

	/**
	 * @param nbFileDownload  the nbFileDownload to set
	 * @uml.property  name="nbFileDownload"
	 */
	public void setNbFileDownload(int nbFileDownload) {
		this.nbFileDownload = nbFileDownload;
	}
	public void setUseWget(String useWget) {
		if ((useWget.trim().compareTo("true")==0)||
				(useWget.trim().compareTo("on")==0)||
				(useWget.trim().compareTo("yes")==0))
			this.useWget = true;
		else
			this.useWget = false;
	}

	/**
	 * @uml.property  name="nThread"
	 * @uml.associationEnd  qualifier="valueOf:java.lang.Integer java.lang.Integer"
	 */
	private HashMap<Integer,Integer> nThread = new HashMap<Integer,Integer>();

	public synchronized void logDownload(int thread) {

		if (nThread.containsKey(thread)){
			Integer v = nThread.get(thread);
			v++;
			nThread.put(thread,v);
		} else 
			nThread.put(thread,1);

		String message = "";
		int totalD = 0;

		for (Integer k : nThread.keySet()) {
			message+="Thread "+k+"=>["+nThread.get(k)+"] ";
			totalD += nThread.get(k);	
		}
		message+=" [" + totalD+ "/" + this.getNbFileDownload() +"]";
		log(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE+message,Project.MSG_INFO);
	}

	@Override
	public synchronized void log(String msg,int mode) {
		super.log(msg,mode);
	}

	/**
	 * Methode d accession pour un fonctionnement Consomateur en groupe sur une liste de fichier a telecharger 
	 * 
	 */

	public boolean thereAreDownloadFile() {
		return (attributFilesDownload.size() != 0);
	}
	
	public synchronized int getDownloaded() {
		return downloaded;
	}

	public synchronized RemoteFile getFileToDownload() {
		if (attributFilesDownload.size()==0)
			return null;
		
		for (RemoteFile rf : attributFilesDownload.keySet()) {
			if (!attributFilesDownload.get(rf).isTreat) {
				attributFilesDownload.get(rf).isTreat = true ;
				attributFilesDownload.get(rf).nbTentative++;
				getProject().log("Get file ["+rf.getAbsolutePath()+" num_tent="+attributFilesDownload.get(rf).nbTentative+"] to download",Project.MSG_VERBOSE);
				downloaded++;
				return rf;
			}
		}
		return null;
	}
	//Free the map
	public synchronized void downloadIsOk(RemoteFile rf) {
		attributFilesDownload.remove(rf);
	}
}
