/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task.net;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.utils.BiomajUtils;

public class HttpListingParser implements ListingParser {

	/**
	 * @uml.property  name="patternLineDirectory"
	 */
	private Pattern patternLineDirectory ;
	/**
	 * @uml.property  name="patternLineFile"
	 */
	private Pattern patternLineFile      ;
	/**
	 * @uml.property  name="groupDateDir"
	 */
	private int groupDateDir             ;
	/**
	 * @uml.property  name="groupDateFile"
	 */
	private int groupDateFile            ;
	/**
	 * @uml.property  name="groupNameDir"
	 */
	private int groupNameDir             ;
	/**
	 * @uml.property  name="groupNameFile"
	 */
	private int groupNameFile            ;
	/**
	 * @uml.property  name="groupeSizeFile"
	 */
	private int groupeSizeFile           ;
	
	
	
	/**
	 * @uml.property  name="lineToParse"
	 */
	private String lineToParse="";
	/**
	 * @uml.property  name="rf"
	 * @uml.associationEnd  
	 */
	private RemoteFile rf ;
	
	/**
	 * Parse a html line to detecet a directory or a file specified by biomaj admin
	 * Put -1 of groupe if you don't know the group in pattern
	 * 
	 * @param patternDirectory pattern directory with groups 
	 * @param groupeDirName  name of directory in pattern directory
	 * @param groupeDirDate date directiry in pattern directory
	 * @param patternFile     pattern file with group
	 * @param groupeFileName  group in patternfile
	 * @param groupeFileDate  date of file in pattern file
	 * @param groupeFileSize size of file in pattern size
	 */
	public HttpListingParser(String patternDirectory,int groupDirName,int groupDirDate,String patternDateDir,
			String patternFile,int groupFileName,int groupFileDate,int groupFileSize,String patternDateFile) {
		
		patternLineDirectory = Pattern.compile(patternDirectory);
		patternLineFile      = Pattern.compile(patternFile);
		groupDateDir         = groupDirDate;
		groupDateFile        = groupFileDate;
		groupNameDir         = groupDirName;
		groupNameFile        = groupFileName;
		groupeSizeFile       = groupFileSize;
		
	}
	
	public String getFileName() {
		// TODO Auto-generated method stub
		return null;
	}

	public long getFileSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getLinkName() {
		// TODO Auto-generated method stub
		return null;
	}

	public long getTime() {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean isLink() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean parse() throws ParseException {
		//Line have to begin by a tag <a
		rf = new RemoteFile();
		/*
		if (!lineToParse.startsWith("<a", 0)) {
			//System.out.println("\""+lineToParse+"\" can't be parsed!");
			return ;
		}
		*/
		Matcher mDir = patternLineDirectory.matcher(lineToParse);
		
		if (mDir.find()) {
			rf.setName(mDir.group(groupNameDir));
			rf.setDir(true);
			rf.setDate(BiomajUtils.stringToDate(mDir.group(groupDateDir)));
			rf.setSize(new Long(0));
			return true;
		}
		
		Matcher mFile = patternLineFile.matcher(lineToParse);
		
		if (mFile.find()) {
			//System.out.println(lineToParse);
			rf.setName(mFile.group(groupNameFile));
			rf.setDir(false);
			rf.setDate(BiomajUtils.stringToDate(mFile.group(groupDateFile)));
			rf.setSize(BiomajUtils.stringToSize(mFile.group(groupeSizeFile)));
			return true;
		} 
		
		return false ;
		//System.out.println(lineToParse+" not matche!");
	}

	public void setLine(String s) {
		lineToParse = s;
	}

	
	public boolean matches(String line) {
		return (patternLineDirectory.matcher(lineToParse).find()
				||patternLineFile.matcher(lineToParse).find());
	}

	public RemoteFile getRemoteFile() {
		return rf;
	}
	
}
