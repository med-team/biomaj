package org.inria.biomaj.ant.task.net;

import java.io.File;
import java.util.Date;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.task.BmajTask;
import org.inria.biomaj.internal.ant.task.net.DirectHttpImpl;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;

/**
 * Class for downloading a file from the given direct URL.
 * 
 * @author rsabas
 *
 */
public class BmajDirectHttp extends BmajTask {

	private String url;
	private String toDir;
	private String targetName;
	private String method;
	
	@Override
	public void execute() throws BuildException {
		DirectHttpImpl impl = new DirectHttpImpl(this, url, method, getProject().getProperty(BiomajConst.urlConcatParams));
		log("Parameters for direct download : " +  impl.getParameters(), Project.MSG_INFO);
		impl.init("", 80, "", "");
		
//		String remoteDirectory = url.substring(0, url.lastIndexOf('/'));
//		String nameFile = url.substring(url.lastIndexOf('/') + 1);
		
		try {
			impl.getFile("", targetName, toDir, targetName);
		} catch (BiomajException e) {
			e.printStackTrace();
		}
		addDownloadFile(targetName, String.valueOf(new Date().getTime()), String.valueOf(new File(toDir + "/" + targetName).length()));
		impl.disconnect();
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getUrl() {
		return url;
	}

	public String getToDir() {
		return toDir;
	}

	public void setToDir(String toDir) {
		this.toDir = toDir;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
}
