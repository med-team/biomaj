package org.inria.biomaj.ant.task.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.task.BmajExtract;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;


/**
 * Test class for BmajExtract class.
 * 
 * @author rsabas
 *
 */
public class TestBmajExtract {
	
	private static List<File> toZip = new ArrayList<File>();
	private static final String LISTING = BiomajUtils.getBiomajRootDirectory() + "/listing";
	private static final String DIR = BiomajUtils.getBiomajRootDirectory() + "/testextract";
	private static final String ARCHIVE = DIR + "/out.zip";
	
	/**
	 * Creates a zip archive.
	 */
	@BeforeClass
	public static void setup() {
		File dir = new File(DIR);
		BiomajUtils.deleteAll(dir);
		dir.mkdir();
		
		for (int i = 0; i < 5; i++) {
			File f = new File(DIR  + "/extract" + i);
			writeRandom(f);
			toZip.add(f);
		}
		createZip();
		List<File> filz = new ArrayList<File>();
		filz.add(new File(ARCHIVE));
		createListing(filz);
		
		for (File f : toZip)
			f.delete();
		
		
		BmajExtract extract = new BmajExtract();
		Project p = new Project();
		p.setProperty(BiomajConst.logFilesProperty, "false");
		extract.setProject(p);
		extract.setDir(DIR);
		extract.setIncludesfile(LISTING);
		extract.execute();
	}
	
	/**
	 * Creates the listing file that contains the archive(s) location.
	 * 
	 * @param filz
	 */
	private static void createListing(List<File> filz) {
		PrintWriter pw;
		try {
			pw = new PrintWriter(new File(LISTING));
			for (File f : filz) {
				pw.println(f.getName());
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Removes the directory that contained the test files.
	 */
	@AfterClass
	public static void cleanup() {
		BiomajUtils.deleteAll(new File(DIR));
	}

	@Test
	public void testFilesExist() {
		String threadDir = "";
		String[] children = new File(DIR).list();
		if (children.length > 0) {
			threadDir = children[0];
		}
		for (File f : toZip) {
			File check = new File(DIR + "/" + threadDir + "/" + f.getName());
			
			assertTrue(check.exists());
		}
	}
	
	@Test
	public void testFilesNotEmpty() {
		String threadDir = "";
		String[] children = new File(DIR).list();
		if (children.length > 0) {
			threadDir = children[0];
		}
		for (File f : toZip) {
			File check = new File(DIR + "/" + threadDir + "/" + f.getName());
			
			assertTrue(check.length() > 0);
		}
	}
	
	/**
	 * Writes a random content in a file.
	 * 
	 * @param file
	 */
	private static void writeRandom(File file) {
		file.delete();
		String rnd = "alezkj lkj^ij mk	j ifj mkjqsmkj	m kjsqmdlkj qmskj " +
				"szd kqjm kj mqskj mkfjmri mlzkrjzm lkj mlkj zoiapodjk kj zklj " +
				"zevdmlkjb iJmZRIJMQKLJ eROBJoMllmkj mkj mlkjdm lkjm";
		
		PrintWriter pw;
		try {
			pw = new PrintWriter(file);
			pw.println(rnd);
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method that creates the zip archive with the previously created files.
	 */
	private static void createZip() {
		
		byte[] buf = new byte[1024];
		
		try {
			ZipOutputStream zip = new ZipOutputStream(new FileOutputStream(ARCHIVE));
			for (File f : toZip) {
				FileInputStream uncompressed = new FileInputStream(f);
				zip.putNextEntry(new ZipEntry(f.getName()));
				int count;
				while ((count = uncompressed.read(buf)) > 0) {
					zip.write(buf, 0, count);
				}
				zip.closeEntry();
				uncompressed.close();
			}
			zip.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
}
