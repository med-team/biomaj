/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.Execute;
import org.apache.tools.ant.taskdefs.LogStreamHandler;
import org.apache.tools.ant.types.Commandline;
import org.inria.biomaj.session.bank.FileDesc;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

public class BmajExtract extends BmajTask {
	/**
	 * Task interface
	 * @uml.property  name="includesfile"
	 */
	private String includesfile ;
	/**
	 * @uml.property  name="dir"
	 */
	private String dir;
	
	/**
	 * @uml.property  name="continueOnError"
	 */
	private boolean continueOnError = true;
	
	private List<String> namesFile = new ArrayList<String>();
	
	private int extractCount = 0;
	
	public static final String TMP_DIR = "bmj_tmp_extract";

	@Override
	public void execute() throws BuildException {
		//checkExecutable();
		//String currentUncompressedFile = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(includesfile)));
			String nf; 
//			LogStreamHandler os = new LogStreamHandler(this, Project.MSG_INFO,Project.MSG_ERR);

			while ((nf = br.readLine()) != null) {
				if (!nf.trim().isEmpty() && !nf.equals("#PLACE_HOLDER_TO_PREVENT_EMPTY_FILE")) {
					namesFile.add(nf);
				}
			}
			br.close();
			
			int total = namesFile.size();

			List<ExtractThread> runningThreads = new ArrayList<ExtractThread>();
			
			/*
			 * Retrieving number of threads to run
			 */
			int numThread;
			try {
				String filesNumThread = BiomajUtils.getProperty(this.getProject(), BiomajConst.extractThreadProperty);
				numThread = new Integer(filesNumThread);
			} catch (BiomajException e) {
				numThread = 1;
				log("extract.threads is not defined ! Default value is : 1",Project.MSG_WARN);
			}

			if (numThread <= 0) {
				log("Bad value for extract.threads ("+numThread+"). New value is : 1",Project.MSG_WARN);
				numThread = 1;
			}
			
			numThread = total < numThread ? total : numThread;
			
			log("Starting " + numThread + " thread(s) to extract " + total + " file(s).", Project.MSG_INFO);
			// Starting the threads
			for (int i = 0; i < numThread; i++) {
				ExtractThread thread = new ExtractThread(this, continueOnError, total);
				runningThreads.add(thread);
				thread.start();
			}
			
			// Waiting for the threads to end
			for (ExtractThread th : runningThreads) {
				try {
					th.join();
					/* 
					 * If an exception occured for any of the threads, throw that buildexception
					 * which will result in workflow being stopped.
					 */
					if (th.getError() != null)
						throw new BiomajBuildException(getProject(), th.getError());
				} catch (InterruptedException e) {
					log("Thread synchronization error : " + e.getMessage());
				}
			}
			
			log("ok",Project.MSG_INFO);

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			throw new BiomajBuildException(getProject(),e);
		} catch (Exception e) {
			throw new BiomajBuildException(getProject(),e);
		}
	}
	
	/**
	 * Return next file name to extract and number of files already extracted.
	 * 
	 * @return
	 */
	protected synchronized String[] getFileToExtract() {
		if (namesFile.size() > 0) {
			String name = namesFile.get(0);
			namesFile.remove(0);
			
			String[] res = {name, String.valueOf(extractCount++)};
			
			return res;
		}
		
		return null;
	}
	
	/**
	 * @param dir  the dir to set
	 * @uml.property  name="dir"
	 */
	public void setDir(String dir) {
		this.dir = dir;
	}
	
	public String getDir() {
		return dir;
	}



	public void setFailonerror(String failonerror) {
		if ((failonerror.trim().compareTo("true")==0)||
				(failonerror.trim().compareTo("on")==0)||
				(failonerror.trim().compareTo("yes")==0))
			continueOnError=false;
	}




	/**
	 * @param includesfile  the includesfile to set
	 * @uml.property  name="includesfile"
	 */
	public void setIncludesfile(String includesfile) {
		this.includesfile = includesfile;
	}

	/**
//	 * get a name file without extension if this has been uncompressed
	 * @param dir
	 * @param nameFile
	 * @param t
	 * @return
	 */

	public static String uncompressedFile(String dir, String nameFile,Task t) throws BiomajException {
		String exe;
		try {
			exe = BiomajInformation.getInstance().getUncompressedOptionWithFile(nameFile,BiomajInformation.OPTION_BIN);
			if (exe==null || exe.compareTo("")==0)
				{
				t.log("Unable to find binary file extraction for ["+nameFile+"]",Project.MSG_WARN);
				t.log("Check the general.conf or define a new case of extraction.",Project.MSG_WARN);
				return nameFile ;
				}
		} catch (BiomajException e) {
			//Pas reconnu en tant que fichier compresse
			return nameFile;
		}

		String option_decomp = BiomajInformation.getInstance().getUncompressedOptionWithFile(nameFile,BiomajInformation.OPTION_DECOMP);
		String option_output = BiomajInformation.getInstance().getUncompressedOptionWithFile(nameFile,BiomajInformation.OPTION_OUTPUT);
		
		
		Commandline cmd = new Commandline();
		cmd.setExecutable(exe);
		if (option_decomp != null)
			cmd.createArgument().setValue(option_decomp);
		cmd.createArgument().setValue(dir+"/"+nameFile);
		
		if (option_output!=null) {
			cmd.createArgument().setValue(option_output);
			cmd.createArgument().setValue(getTargetDirectory(dir,nameFile));
		}
		
		Execute execute = new Execute(new LogStreamHandler(t, Project.MSG_VERBOSE,Project.MSG_ERR),null);

		t.log("extracting:"+dir+"/"+nameFile,Project.MSG_VERBOSE);
		execute.setCommandline(cmd.getCommandline());
		try {
			execute.execute();
		} catch (IOException ioe) {
			t.log("i/o error:"+ioe.getMessage(),Project.MSG_ERR);
			return nameFile;
		}
		int code_retour  = execute.getExitValue();

		if (code_retour!=0) {
			for (String s : execute.getCommandline())
				t.log(s,Project.MSG_ERR);
			
			t.log("file:"+dir+"/"+nameFile,Project.MSG_ERR);
			t.log("code retour:"+code_retour,Project.MSG_ERR);
			throw new BiomajException("bmajExtract.error.file",nameFile);
		}

		String newName = removeExtension(nameFile);	

		if (nameFile.compareTo(newName)==0) {
			throw new BiomajBuildException(t.getProject(),"bmajExtract.error.format",nameFile,new Exception());
		}

		t.log("uncompressed file ok :"+nameFile,Project.MSG_DEBUG);

		return newName;
	}

	public static boolean containsCompressedFormat(String nameFile) throws BiomajException {
		if (nameFile == null)
			return false;

		return nameFile.compareTo(BiomajInformation.getInstance().getUncompressedName(nameFile)) != 0;
	}

	public static boolean check(File f,Task t) throws BiomajException {
		try {
			Commandline cmd = new Commandline();
			cmd.setExecutable(BiomajInformation.getInstance().getUncompressedOptionWithFile(f.getName(),BiomajInformation.OPTION_BIN));
			String option_test =  BiomajInformation.getInstance().getUncompressedOptionWithFile(f.getName(),BiomajInformation.OPTION_TEST) ;
			
			if (option_test == null) {
				t.log("Test archive:"+f.getAbsolutePath(),Project.MSG_VERBOSE);
				t.log("Option test is not define for binary :"+cmd.getExecutable(),Project.MSG_VERBOSE);
				t.log("No test have been executed! --->true",Project.MSG_VERBOSE);
				return true;
			}
			
			cmd.createArgument().setValue(option_test);
			cmd.createArgument().setValue(f.getAbsolutePath());

			Execute execute = new Execute(new LogStreamHandler(t, Project.MSG_VERBOSE,Project.MSG_ERR),null);

			t.log("Test archive:"+f.getAbsolutePath(),Project.MSG_VERBOSE);
			execute.setCommandline(cmd.getCommandline());
			execute.execute();
			int code_retour  = execute.getExitValue();
			//not in gzip format code=1!
			if (code_retour!=0) {
				t.log("exec:"+cmd.getExecutable(),Project.MSG_WARN);
				t.log("opt:",Project.MSG_WARN);
				for (String opt : cmd.getArguments())
					t.log("    "+opt,Project.MSG_WARN);
				t.log("file:"+f.getAbsolutePath(),Project.MSG_WARN);
				t.log("code:"+code_retour,Project.MSG_WARN);
				return false;
			}

		} catch (FileNotFoundException fnfe) {
			BiomajLogger.getInstance().log(fnfe);
			throw new BiomajBuildException(t.getProject(),fnfe);
		} catch (IOException ioe) {
			throw new BiomajBuildException(t.getProject(),ioe);
		}
		
		return true;
	}

	public static String removeExtension(String nameFile) throws BiomajException {
		return BiomajInformation.getInstance().getUncompressedName(nameFile);

	}


	protected synchronized Vector<String> getDiffListing(Vector<FileDesc> lsBefore,Vector<FileDesc> lsNext) {
		Vector<String> result = new Vector<String>();

		for (int i=0 ; i<lsBefore.size() ; i++) {
			boolean estPresent = false;
			for (int j=0 ; j<lsNext.size() ; j++) {
				estPresent = estPresent || (lsBefore.get(i).getLocation().compareTo(lsNext.get(j).getLocation())==0);
			}
			if (!estPresent)
				result.add(lsBefore.get(i).getLocation());
		}

		for (int i=0 ; i<lsNext.size(); i++) {
			boolean estPresent = false;
			for (int j=0 ; j<lsBefore.size() ; j++) {
				estPresent = estPresent || (lsNext.get(i).getLocation().compareTo(lsBefore.get(j).getLocation())==0);
			}

			if ((!result.contains(lsNext.get(i).getLocation()))&&(!estPresent))
				result.add(lsNext.get(i).getLocation());
		}
		return result;
	}


	protected static synchronized String getTargetDirectory(String base, String absName) {

		if (!absName.contains("/")) {
			return base;
		} else {
			int index = absName.lastIndexOf("/");
			return base+"/"+absName.substring(0, index);
		}

	}


}
