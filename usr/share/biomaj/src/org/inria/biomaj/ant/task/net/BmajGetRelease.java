/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task.net;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.gmod.biomaj.ant.task.InputValidation;
import org.inria.biomaj.internal.ant.task.net.RemoteCommandImpl;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
/**
 * <p>GetRelease is a Biomaj Task Ant to get the bank release.
 * Two method are implemented:
 * <li>
 * <ul>Search the release in a name file</ul>
 * <ul>Search a release in a file</ul>
 * </li>
 * <p/>
 * @author ofilangi
 *
 */
public class BmajGetRelease extends RemoteCommand {

	/**
	 * Directory to search release info
	 * @uml.property  name="remoteDir"
	 */
	private String remoteDir;
	/**
	 * Name file that contents the release (2nd method)
	 * @uml.property  name="nameFile"
	 */
	private String nameFile;
	/**
	 * Regexp to find the release
	 * @uml.property  name="regexp"
	 */
	private String regexp;
	/**
	 * release date format (regexp is not specified!)
	 * @uml.property  name="dateFormat"
	 */
	private String dateFormat;
	/**
	 * result
	 * @uml.property  name="release"
	 */
	private String release;
	/**
	 * Property name to set the release
	 * @uml.property  name="releaseProperty"
	 */
	private String releaseProperty;
	
	private String refRelease;
	
	private String computed;

	@Override
	public void execute() throws BuildException {
		
		// If it is a computed bank, special stuff required to determine bank release
		if (computed.equals("true")) {
		
			// Default : release is current date
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			release = sdf.format(new Date());
			
			if (!refRelease.trim().isEmpty()) { // Release is that of an other bank
				log("Get release from bank : " + refRelease, Project.MSG_DEBUG);
				Map<String, String> latestUpdate = BiomajSQLQuerier.getLatestUpdate(refRelease, true);
				if (latestUpdate != null && latestUpdate.get(BiomajSQLQuerier.UPDATE_RELEASE) != null) {
					release = latestUpdate.get(BiomajSQLQuerier.UPDATE_RELEASE);
				}
			}
			log("Release found : " + release, Project.MSG_DEBUG);
			getProject().setProperty(releaseProperty, release);
			
		} else {
		
			checkInputInit();

			InputValidation.checkString(getProject(),dateFormat, "release.dateFormat");
	
			//File name must be initialized!
			/*if (remoteDir.trim().compareTo("")==0)
				throw new BiomajBuildException(getProject(),"getRelease.error.directory.mis",new Exception());
			else*/
			if ((remoteDir!=null)&&(remoteDir.length()>1)&&remoteDir.trim().charAt(0)!='/')
				throw new BiomajBuildException(getProject(),"error.remote.directory.malformed",remoteDir,new Exception());
	
			boolean find = false;
	
			try {
				//if a regexp is defined!
				if (regexp.trim().compareTo("")!=0) {
					if ((nameFile.trim().compareTo("")==0)) {//Find a release with name only if nameFile is set!
						log("NAMEFILE:"+nameFile,Project.MSG_DEBUG);
						log("Find Release in the name file:"+release,Project.MSG_VERBOSE);
						find = findReleaseWithFileName();
	
					} else {
						log("Find Release in the file:"+release,Project.MSG_VERBOSE);
						find = findReleaseInsideFile();
					} 
	
					if (!find) {
						log("A regexp was defined for a remote release but it did not give any results!",Project.MSG_WARN);
					}
				}
	
				if (!find){
					log("Biomaj will try to create a remote release.",Project.MSG_INFO);
					log("Creation date of the most recent file on the remote server : "+nameFile,Project.MSG_INFO);
					find = findReleaseWithDateOfRemoteFile(); 
				}
				
				if (!find)
					throw new BiomajBuildException(getProject(),"getRelease.error.remote.attributes.malformed",new Exception());
	
				//Correction bug : on ne laisse pas d'espace/tab etc... dans l'expression de la release
				release = release.replaceAll("\\s", "-");
	
				getProject().setProperty(releaseProperty,release);
			} finally {
				closeRemoteCommand();
			}
		}
	}

	/**
	 * First method to find release. The release may be contents in the name file
	 * @param file
	 * @return
	 */
	public boolean findReleaseWithFileName() {
		log("1srt METHODE find Release with File Name",Project.MSG_VERBOSE);
		try {
			initRemoteCommand();
			Collection<RemoteFile> files = impl.getListingFiles(remoteDir,regexp,"");
			closeRemoteCommand();
			if (files==null) 
				throw new BiomajBuildException(getProject(),new NullPointerException("files"));

			if (files.size()<=0)
				return false;

			Pattern pattern = Pattern.compile(regexp);
			Matcher releaseMatcher;

			String currentRelease = "";
			for (RemoteFile file : files) {
				log("name file:"+file.getName(),Project.MSG_DEBUG);
				releaseMatcher = pattern.matcher(file.getName());
				//if the release is found 
				if(releaseMatcher.find()) {
					if (releaseMatcher.groupCount() >= 1) {
						if (releaseMatcher.groupCount() > 1) {
							log("More than one group is defined, BioMAJ takes the first group:["+releaseMatcher.group(1)+"]",Project.MSG_WARN);
						}
						String grp = releaseMatcher.group(1);
						currentRelease = currentRelease.trim().isEmpty() ? grp : currentRelease.compareTo(grp) < 0 ?
									grp : currentRelease;
								
//						release = releaseMatcher.group(1);
						
					} else {
						log("No group defined for release",Project.MSG_VERBOSE);
						
						String grp = releaseMatcher.group();
						currentRelease = currentRelease.trim().isEmpty() ? grp : currentRelease.compareTo(grp) < 0 ?
									grp : currentRelease;
						
//						release = releaseMatcher.group() ;
					}
				}
			}
			if (!currentRelease.trim().isEmpty()) {
				release = currentRelease;
				log("RELEASE:"+release,Project.MSG_VERBOSE);
				return true;
			}
		}  catch (Exception e) {
			throw new BiomajBuildException(getProject(),e);
		}

		//no release found!
		return false;
	}

	/**
	 * Second method to find release. The release may be contents in the file
	 * and can be find with a regexp!
	 * @param file
	 * @return
	 */
	public boolean findReleaseInsideFile() {
		log("2nd METHODE find Release inside File",Project.MSG_VERBOSE);
		String targetDirectory = "/var/tmp/";
		String targetFile      = System.currentTimeMillis()+nameFile;


		String[] list = targetFile.split("/");
		targetFile = list[list.length-1];

		log("targetFile:"+targetFile+" (ne doit pas contenir de repertoire!)",Project.MSG_DEBUG);
		log("directory :"+targetDirectory,Project.MSG_DEBUG);
		log("compressed file :"+targetDirectory,Project.MSG_DEBUG);

		try {
			String isCompressed = getProject().getProperty("release.file.compressed");
			log("compressed file :"+isCompressed,Project.MSG_DEBUG);

			if ((isCompressed==null)||(isCompressed.trim().compareTo("true")!=0)) {
				//Modif O.F pour recuperer un fichier avec une url entiere
				if ((nameFile != null)&&(nameFile.contains("://"))) 
				{
					if (!RemoteCommandImpl.getFile(this, getUsername(),getPassword(),nameFile, targetDirectory, targetFile))
					{
						log("Release no find with url:"+nameFile,Project.MSG_VERBOSE);
						return false;
					}
				}
				else 
				{
					initRemoteCommand();
					if (!impl.getFile(remoteDir,nameFile,targetDirectory,targetFile)) {
						log("Release no find by findReleaseInsideFile",Project.MSG_VERBOSE);
						return false;
					}
					closeRemoteCommand();
				}
			} else {
				String newName = "";

				if ((nameFile != null)&&(nameFile.contains("://"))) 
					newName = RemoteCommandImpl.getFileUncompressed(this,getUsername(),getPassword(),nameFile,targetDirectory);
				else {
					initRemoteCommand();
					newName = impl.getFileUncompressed(remoteDir,nameFile,targetDirectory);
					closeRemoteCommand();
				}
				if ((newName.compareTo("")==0)||(newName.compareTo(nameFile)==0))
				{
					log("Can't uncompressed file:"+nameFile,Project.MSG_VERBOSE);
					return false;
				} else {
					targetFile = newName;
				}
			}
			log("File find!",Project.MSG_DEBUG);

		} 	catch (BiomajException e) {
			throw new BiomajBuildException(getProject(),e);
		} 

		File file = new File(targetDirectory+targetFile);
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
		} catch (FileNotFoundException ex) {
			log("File not found:"+ex.getMessage(),Project.MSG_WARN);
			return false;
		}

		try {		
			Pattern pattern = Pattern.compile(regexp);
			Matcher releaseMatcher;
			String line = "";
			log("REGEXP:"+regexp,Project.MSG_DEBUG);

			while ((line = reader.readLine()) != null) 
			{
				log("Line to parse:"+line,Project.MSG_DEBUG);
				releaseMatcher = pattern.matcher(line);
				if(releaseMatcher.find())
				{
					if (releaseMatcher.groupCount()>=1) {
						if (releaseMatcher.groupCount()>1) {
							log("More than one group is defined, BioMAJ takes the first group:["+releaseMatcher.group(1)+"]",Project.MSG_WARN);
						}
						release = releaseMatcher.group(1);
						
					} else {
						log("Aucun group definit pour la release",Project.MSG_VERBOSE);
						release = releaseMatcher.group() ;
					}
					log("RELEASE:"+release,Project.MSG_VERBOSE);
					reader.close();
					return true;
				}
			}

			reader.close();

		} catch (IOException e) {
			throw new BiomajBuildException(getProject(),e);
		} finally {
			file.delete();
		}

		return false;
	}

	/**
	 * 3rd method to find release. The release is the date of the earlest file
	 * and can be found with a regexp!
	 * @param file
	 * @return
	 */
	public boolean findReleaseWithDateOfRemoteFile() {
		log("3rd METHODE find Release with date of Remote Files",Project.MSG_VERBOSE);
		try {

			String regExpressionOnRemoteFiles = getProject().getProperty(BiomajConst.remoteFilesProperty);

			String regExcludedRemoteFiles = "";
			if (getProject().getProperties().containsKey(BiomajConst.remoteExcludedFilesProperty))
				regExcludedRemoteFiles = getProject().getProperty(BiomajConst.remoteExcludedFilesProperty);

			InputValidation.checkString(getProject(), regExpressionOnRemoteFiles,"check remote.files");

			initRemoteCommand();
			Collection<RemoteFile> files = impl.getListingFiles(remoteDir,regExpressionOnRemoteFiles,regExcludedRemoteFiles);
			closeRemoteCommand();
			
			log("nb file on remote server:"+remoteDir+"  nb:"+files.size(),Project.MSG_DEBUG);
			if (files.size()<=0)
				return false;

			Date releaseDate= new Date();
			releaseDate.setTime(0);

			for (RemoteFile file : files) {
				log("name file:"+file.getName(),Project.MSG_DEBUG);
				log("date file:"+file.getDate().toString(),Project.MSG_DEBUG);

				if (releaseDate.getTime()<file.getDate().getTime())
					releaseDate.setTime(file.getDate().getTime()); 
			}

			log("dateFormat:"+dateFormat,Project.MSG_INFO);
			try {
				release = new SimpleDateFormat(dateFormat).format(releaseDate);
			} catch (IllegalArgumentException ex) {
				throw new BiomajBuildException(getProject(),"getRelease.error.date.format.malformed",dateFormat,ex);
			}

			//	log("Release date:"+release);

		} catch (BiomajBuildException e) {
			throw e;
		} catch (Throwable e) {
			throw new BiomajBuildException(getProject(),e);
		}

		return true;


	}	

	/**
	 * @return  the regexp
	 * @uml.property  name="regexp"
	 */
	public String getRegexp() {
		return regexp;
	}

	/**
	 * @param regexp  the regexp to set
	 * @uml.property  name="regexp"
	 */
	public void setRegexp(String regexp) {
		this.regexp = regexp;
	}

	/**
	 * @return  the nameFile
	 * @uml.property  name="nameFile"
	 */
	public String getNameFile() {
		return nameFile;
	}

	/**
	 * @param nameFile  the nameFile to set
	 * @uml.property  name="nameFile"
	 */
	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}

	/**
	 * @return  the remoteDir
	 * @uml.property  name="remoteDir"
	 */
	public String getRemoteDir() {
		return remoteDir;
	}

	/**
	 * @param remoteDir  the remoteDir to set
	 * @uml.property  name="remoteDir"
	 */
	public void setRemoteDir(String remoteDir) {
		this.remoteDir = remoteDir;
	}

	/**
	 * @return  the releaseProperty
	 * @uml.property  name="releaseProperty"
	 */
	public String getReleaseProperty() {
		return releaseProperty;
	}

	/**
	 * @param releaseProperty  the releaseProperty to set
	 * @uml.property  name="releaseProperty"
	 */
	public void setReleaseProperty(String releaseProperty) {
		this.releaseProperty = releaseProperty;
	}

	/**
	 * @return  the dateFormat
	 * @uml.property  name="dateFormat"
	 */
	public String getDateFormat() {
		return dateFormat;
	}

	/**
	 * @param dateFormat  the dateFormat to set
	 * @uml.property  name="dateFormat"
	 */
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getRefRelease() {
		return refRelease;
	}

	public void setRefRelease(String bankRefRelase) {
		this.refRelease = bankRefRelase;
	}

	public String getComputed() {
		return computed;
	}

	public void setComputed(String computed) {
		this.computed = computed;
	}

}
