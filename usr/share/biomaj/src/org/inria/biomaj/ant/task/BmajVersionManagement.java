/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.gmod.biomaj.ant.task.InputValidation;
import org.inria.biomaj.exe.bank.BankFactory;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.exe.workflow.WorkflowEngine;
import org.inria.biomaj.exe.workflow.WorkflowEngineFactory;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.ProductionDirectory;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

public class BmajVersionManagement extends Task {

	/** separator to create several directory with the same release*/
	public final static  String SEP_NUMBER_OF_RELEASE_VERSION  = "__";
	/** separator between bank name et release number */
	public final static  String SEP_RELEASE                    = "_";	
	/** Directory name where flat file are stored */
	public final static  String FLAT_DIRECTORY                 = "flat";

	/** Internal Biomaj properties */
	public final static String MODE                              = "mode_version_management";
	public final static String RESULTS                           = "result_version_management";

	/**
	 * Number of version to keep on database
	 * @uml.property  name="nbVersion"
	 */
	private String nbVersion;
	/**
	 * Use in interne
	 * @uml.property  name="numberVersion"
	 */
	private int numberVersion;
	/**
	 * Root of the specified database (where version are stored)
	 * @uml.property  name="root"
	 */
	private String root;
	/**
	 * Remote release
	 * @uml.property  name="release"
	 */
	private String release;
	/**
	 * Bank Name
	 * @uml.property  name="bank"
	 */
	private String bank;
	
	/**
	 * directory where raw data are
	 * @uml.property  name="flatDirectory"
	 */
	private String flatDirectory;
	/**
	 * directory where compute for post process
	 * @uml.property  name="prodDirectory"
	 */
	private String prodDirectory;
	
	private String override = "true";

	public final static String DELETE                =   "delete";
	public final static String GET                   =   "get";
	public final static String CREATE                =   "create";
	public final static String REBUILD               =   "rebuild";

	/**
	 * CREATE or GET a directory version
	 * @uml.property  name="mode"
	 */
	private String mode;

	@Override
	public void execute() throws BuildException {

		InputValidation.checkString(getProject(),bank, "name of bank.");
		InputValidation.checkString(getProject(),mode, "create or get a directory version.");
		InputValidation.checkString(getProject(),root, "root where version are stored.");

		//Init properties
		getProject().setProperty(MODE,mode);
		getProject().setProperty(RESULTS,"NOT INITIALIZED");
		if (mode.compareTo(GET)==0) { 
			InputValidation.checkString(getProject(),flatDirectory, "raw data directory");

			String path =  getLastDirectoryCreated() ;

			log("online directory:"+path,Project.MSG_INFO);
			getProject().setProperty(flatDirectory,path+"/"+FLAT_DIRECTORY);
			getProject().setProperty(prodDirectory,path);

		} else if (mode.compareTo(CREATE)==0) { 
			InputValidation.checkString(getProject(),flatDirectory, "raw date directory");
			InputValidation.checkString(getProject(),release, "need release to create a directory!");

			File newDirectory = createNextDirectoryVersion();
			File flatDir = new File(newDirectory.getAbsolutePath()+"/"+FLAT_DIRECTORY);
			
			if (!flatDir.exists())
				flatDir.mkdir();
			else if (override.equals("true"))
				flatDir.mkdir();
			
			getProject().setProperty(flatDirectory,newDirectory.getAbsolutePath()+"/"+FLAT_DIRECTORY);
			getProject().setProperty(prodDirectory,newDirectory.getAbsolutePath());
			getProject().setProperty(RESULTS,newDirectory.getAbsolutePath());
			log("new online directory:"+newDirectory.getAbsolutePath()+"/"+FLAT_DIRECTORY,Project.MSG_INFO);

		} else if (mode.compareTo(DELETE)==0) {
			InputValidation.checkString(getProject(),nbVersion, "number of version to keep on database.");	
			try {
				numberVersion = Integer.valueOf(nbVersion) ;
//				Correction Bug OFI, nbVersion ne peut etre negatif !
				if (numberVersion<0)
					throw new BiomajBuildException(getProject(),"bmajVersionManagement.error.number",nbVersion,new Exception());

			} catch (NumberFormatException ex) {
				throw new BiomajBuildException(getProject(),"bmajVersionManagement.error.number",nbVersion,ex);
			}
			deleteOldVersion();
		} else if (mode.compareTo(REBUILD)==0) {

			/*
			 * On doit supprimer le lien current et le mettre sur une ancienne version (si celle ci existe)
			 * le nouveau lien de la version en prod la plus recente devient future_release
			 */
			rebuild();
		} else {
			throw new BiomajBuildException(getProject(),"bmajVersionManagement.error.devl",new Exception());
		}

	}

	/**
	 * Get a list of Version bank directory (WARNING if they are a current version (in construction from this session),this version is not include). 
	 * This list is order with the date of directories
	 * @return
	 */
	protected Vector<File> getDirectoriesVersions() {
//		try {
//			Vector<ProductionDirectory> lDir = BiomajQueryXmlStateFile.getAvailableDirectoriesProduction(bank);
			List<ProductionDirectory> lDir = BiomajSQLQuerier.getAvailableProductionDirectories(bank);

			if (lDir == null)
				throw new BuildException("Can't get the last online directory from "+bank+".xml");


			Vector<File> result = new Vector<File>();

			while (lDir.size()>0) {
				int youngest = 0;
				for(int i=1;i<lDir.size();i++) {
					if (lDir.get(i).getCreationDate().getTime()<lDir.get(youngest).getCreationDate().getTime())
						youngest = i;
				}
				File dir = new File(lDir.get(youngest).getPath());
				lDir.remove(youngest);
				if (dir.exists()) {
					result.add(dir);
				}
			}

			return result;
//		} catch (BiomajException be) {
//			throw new BiomajBuildException(getProject(),be);
//		}
	}

	/**
	 * 
	 * @return
	 */
	protected String getNameNewDirectory() {
		return (root+"/"+bank+SEP_RELEASE+release).replaceAll("/{2,}", "/");
	}

	/**
	 * True if a directory with the name of remote release exist!
	 * @param listVersionDirectories
	 * @return
	 */
	protected boolean remoteReleaseExistOnLocalDatabase(Vector<File> listVersionDirectories) {

		if (listVersionDirectories == null)
			throw new BiomajBuildException(getProject(),new NullPointerException("listVersionDirectories"));

		for (int i=0;i<listVersionDirectories.size();i++) {
			String path = listVersionDirectories.get(i).getAbsolutePath();
			Matcher m = Pattern.compile("(.*)(__\\d+)$").matcher(path);
			if (m.find()) { // Ends with __<digit>
				if (m.group(1).replaceAll("/{2,}", "/").equals(getNameNewDirectory()))
					return true;
			} else {
				if (path.replaceAll("/{2,}", "/").equals(getNameNewDirectory()))
					return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param directory  local name of directory
	 */
	protected void createDirectory(String directory) {
		File dir = new File(root+"/"+directory);
		if (dir.exists())
			throw new BiomajBuildException(getProject(),"bmajVersionManagement.error.create.directory",dir.getAbsolutePath(),new Exception());

		dir.mkdir();
	}

	/**
	 * Copy files and subdirectories from online citrina directory to a directory destination 
	 * 
	 */
	protected void copy(String from, String to) {
		log("copy from:"+from+" to:"+to,Project.MSG_INFO);
		File fromDir = new File(from);

		if (!fromDir.exists())
			throw new BiomajBuildException(getProject(),"bmajVersionManagement.error.copy",from,new Exception());

		File toDir = new File(to);

		if (!toDir.exists())
			toDir.mkdir();

		File[] lFiles = fromDir.listFiles();
		for (int i=0;i<lFiles.length;i++) {
			if (lFiles[i].isFile()) {
				File f = new File(to+"/"+lFiles[i].getName());
				try {
					f.createNewFile();
					log("copy file:"+lFiles[i].getName(),Project.MSG_INFO);
					BiomajUtils.copy(lFiles[i],f);
				} catch (IOException e) {
					throw new BiomajBuildException(getProject(),e);
				}

			} else {
				copy(lFiles[i].getAbsolutePath(),to+"/"+lFiles[i].getName());
			}
		}

	}

	/**
	 * Get the last number of release subdirectory creation
	 * if result is 0, there are only one directory created 
	 * @param directories
	 * @return
	 */
	protected Integer getLastNumberFromVersion(Vector<File> directories) {
		Integer result = -1;
		for (int i = 0; i < directories.size(); i++) {
			String path = directories.get(i).getAbsolutePath().replaceAll("/{2,}", "/");
			Matcher m = Pattern.compile("(.*)(__\\d+)$").matcher(path);
			if (m.find()) {
				int value = Integer.valueOf(m.group(2).substring(2));
				if (result < value)
					result = value;
			} else if (result < 0) {
				// Not found and was not found in another directory
				result = 0;
			}
		}
		return result;
	}

	/**
	 * Create the new subdirectory for a version :
	 * if the remote release exists, we create a subdirectory : BANK_NUMBER-RELEASE_N
	 * otherwise BANK_NUMBER-RELEASE is created!
	 */
	protected File createNextDirectoryVersion() {
		Vector<File> directoriesVersions = getDirectoriesVersions();
		File f;
		if (remoteReleaseExistOnLocalDatabase(directoriesVersions)){
			Integer num = getLastNumberFromVersion(directoriesVersions);
			if (num == -1)
				throw new BiomajBuildException(getProject(),"bmajVersionManagement.error.directory.find",new Exception());

			num++;
			f = new File(getNameNewDirectory()+SEP_NUMBER_OF_RELEASE_VERSION+num.toString());
		} else {
			f = new File(getNameNewDirectory());
		}
		
		if (f.exists() && override.equals("true")) {
			BiomajUtils.deleteAll(f);
		}
		f.mkdir();
		createNextReleaseLink(f);
		return f;
	}

	/**
	 * nbVersion = 0 ==> keep only one directory
	 * 1 ==> 2 directories etc...
	 *
	 */
	protected void deleteOldVersion() {
		String results = "";
		Vector<File> lDir = getDirectoriesVersions();

		getProject().log("versions found in prod dir ["+lDir.size()+"]",Project.MSG_VERBOSE);

		for (File d : lDir) {
			getProject().log("version ["+d.getAbsolutePath()+"]",Project.MSG_VERBOSE);
		}

		try {
			BankFactory bf = new BankFactory();
			BiomajBank bankToDelete = bf.createBank(bank, true);
		
			while (lDir.size()>numberVersion+1) {
	
				BiomajUtils.deleteAll(lDir.get(0));
				log(lDir.get(0).getAbsolutePath()+" is deleted",Project.MSG_INFO);
				results+=lDir.get(0).getAbsolutePath()+",";
				lDir.remove(0);
				if ( bankToDelete != null ) {
					WorkflowEngineFactory wef = new WorkflowEngineFactory();
					WorkflowEngine currentWe ;
					currentWe = wef.createWorkflow(bankToDelete);
					currentWe.runRemoveProcess(BmajVersionManagement.getRelease(lDir.get(0).getAbsolutePath()));
				}
			}
		} catch (BiomajException e) {
			//A biomajException is displayed  on error output by default!
			System.err.println("Biomaj has detected an error while trying to launch remove process for bank ["+ bank+"]");
			System.err.println(e.getLocalizedMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
		}
		getProject().setProperty(RESULTS, results);
	}

	protected String getLastDirectoryCreated() {
		String res;
//		try {
//			ProductionDirectory pd = BiomajQueryXmlStateFile.getLastOnlineDirectory(bank);
			ProductionDirectory pd = BiomajSQLQuerier.getLatestProductionDirectory(bank);
			
			if (pd != null)
				res = pd.getPath();
			else {
				log("First execution for bank:"+bank,Project.MSG_WARN);
				return "";
			}
//		} catch (BiomajException be) {
//			throw new BiomajBuildException(getProject(),be);
//		}
		File dirOnline = new File(res);

		if (!dirOnline.exists()){
			log("Last online directory has been delete:"+dirOnline.getAbsolutePath(),Project.MSG_WARN);
			return "";
		}

		return res;

	}

	protected void createNextReleaseLink(File f){

		try {
			File futurrelease = new File(getRoot()+"/"+BiomajConst.futureReleaseLink);
			if (futurrelease.exists())
				futurrelease.delete();
			String cmdline_chmod = "chmod 744 "+f.getAbsolutePath();
			Runtime.getRuntime().exec(cmdline_chmod);

			if (BiomajUtils.createLinkOnFileSystem(f, BiomajConst.futureReleaseLink)!=0) {
				throw new BiomajBuildException(getProject(),"bmajVersionManagement.error.link",futurrelease.getAbsolutePath(),new Exception());
			}

		} catch (Exception e) {
			throw new BiomajBuildException(getProject(),e);
		}
	}

	/**
	 * @return  the bank
	 * @uml.property  name="bank"
	 */
	public String getBank() {
		return bank;
	}

	/**
	 * @param bank  the bank to set
	 * @uml.property  name="bank"
	 */
	public void setBank(String bank) {
		this.bank = bank;
	}

	/**
	 * @return  the nbVersion
	 * @uml.property  name="nbVersion"
	 */
	public String getNbVersion() {
		return nbVersion;
	}

	/**
	 * @param nbVersion  the nbVersion to set
	 * @uml.property  name="nbVersion"
	 */
	public void setNbVersion(String nbVersion) {
		this.nbVersion = nbVersion;
	}

	/**
	 * @return  the numberVersion
	 * @uml.property  name="numberVersion"
	 */
	public int getNumberVersion() {
		return numberVersion;
	}

	/**
	 * @param numberVersion  the numberVersion to set
	 * @uml.property  name="numberVersion"
	 */
	public void setNumberVersion(int numberVersion) {
		this.numberVersion = numberVersion;
	}

	/**
	 * @return  the release
	 * @uml.property  name="release"
	 */
	public String getRelease() {
		return release;
	}

	/**
	 * @param release  the release to set
	 * @uml.property  name="release"
	 */
	public void setRelease(String release) {
		this.release = release;
	}

	/**
	 * @return  the root
	 * @uml.property  name="root"
	 */
	public String getRoot() {
		return root;
	}

	/**
	 * @param root  the root to set
	 * @uml.property  name="root"
	 */
	public void setRoot(String root) {
		this.root = root;
	}

	/**
	 * @return  the flatDirectory
	 * @uml.property  name="flatDirectory"
	 */
	public String getFlatDirectory() {
		return flatDirectory;
	}

	/**
	 * @param flatDirectory  the flatDirectory to set
	 * @uml.property  name="flatDirectory"
	 */
	public void setFlatDirectory(String propertyDirectoryResult) {
		this.flatDirectory = propertyDirectoryResult;
	}

	/**
	 * @param mode  the mode to set
	 * @uml.property  name="mode"
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 * @return  the prodDirectory
	 * @uml.property  name="prodDirectory"
	 */
	public String getProdDirectory() {
		return prodDirectory;
	}

	/**
	 * @param prodDirectory  the prodDirectory to set
	 * @uml.property  name="prodDirectory"
	 */
	public void setProdDirectory(String prodDirectory) {
		this.prodDirectory = prodDirectory;
	}

	/**
	 * @return  the mode
	 * @uml.property  name="mode"
	 */
	public String getMode() {
		return mode;
	}

	public static String getRelease(String dirName) throws BiomajException {

		String[] splitDirName = dirName.split(SEP_NUMBER_OF_RELEASE_VERSION);
		String value = "";
		if (splitDirName.length==0)
			value = splitDirName[0];
		else {
			for (int i=0;i<=splitDirName.length-2;i++)
				value = value + splitDirName[i];

			//Pour finir on verifie que c est un chiffre, si ce n est pas le cas
			//on ne tronque pas...
			try {
				Integer.parseInt(splitDirName[splitDirName.length-1]);

			} catch (NumberFormatException e) {
				//pas ok donc revient a la valeur initiale
				value = dirName;
			}
		}

		String [] l = value.split(SEP_RELEASE);

		if (l.length<2) {
			throw new BiomajException("bmajVersionManagement.error.releasename",dirName);
		}

		String rel = l[l.length-1];
		return rel;		
	}


	public void rebuild() {

		try {
			File current = new File (root+"/"+BiomajConst.currentLink);

			if (!current.exists()) {
				log("current n existe pas....",Project.MSG_ERR);
				return;
			}

			List<ProductionDirectory> lDir = BiomajSQLQuerier.getAvailableProductionDirectories(bank);

			if (lDir.size()>0) {

				if ((lDir.get(lDir.size()-1).getPath().compareTo(current.getCanonicalPath())==0)) {
					String result = current.getCanonicalPath();
					getProject().log("current release:"+result+" -->delete link...",Project.MSG_INFO);
					if (!current.delete())//efface le lien...
						throw new BiomajBuildException(getProject(),"bmajVersionManagement.error.delete.link.current",new Exception());

					getProject().setProperty(RESULTS, result);
					if (lDir.size()>1) {
						if (BiomajUtils.createLinkOnFileSystem(new File(lDir.get(lDir.size()-2).getPath()), BiomajConst.currentLink)!=0) {
							throw new BiomajBuildException(getProject(),"bmajVersionManagement.create.link",BiomajConst.currentLink,lDir.get(lDir.size()-2).getPath(),new Exception());
						} else
							getProject().log("new current release:"+lDir.get(lDir.size()-2).getPath(),Project.MSG_INFO);
					}
					if (BiomajUtils.createLinkOnFileSystem(new File(lDir.get(lDir.size()-1).getPath()), BiomajConst.futureReleaseLink)!=0) {
						throw new BiomajBuildException(getProject(),"bmajVersionManagement.create.link",BiomajConst.futureReleaseLink,lDir.get(lDir.size()-1).getPath(),new Exception());
					} else 
						getProject().log("new futur release:"+lDir.get(lDir.size()-1).getPath(),Project.MSG_INFO);
				} else 
					throw new BiomajBuildException(getProject(),"bmajVersionManagement.error.rebuild.statefile",lDir.get(lDir.size()-1).getPath(),current.getCanonicalPath(),new Exception());

			} else {
				throw new BiomajBuildException(getProject(),"bmajVersionManagement.error.current.not.exist",current.getCanonicalPath(),new Exception());
			}
		} catch (IOException ioe) {
			throw new BiomajBuildException(getProject(),"io.error",ioe.getMessage(),ioe);
		} catch (InterruptedException ie) {
			throw new BiomajBuildException(getProject(),"unknown.error",ie.getMessage(),ie);
		}// catch (BiomajException e) {
//			throw new BiomajBuildException(getProject(),e);
//		}

	}

	public String getOverride() {
		return override;
	}

	public void setOverride(String override) {
		this.override = override;
	}

}

/**
 * 
 * @author ofilangi
 *
 */
class DirectoryVersionManagementFilter implements FilenameFilter {
	/**
	 * @uml.property  name="bankName"
	 */
	String bankName;

	DirectoryVersionManagementFilter(String bankName) { 
		this.bankName = bankName; 
	}

	public boolean accept(File dir, String name) {
		return name.matches(name);
	}
}

