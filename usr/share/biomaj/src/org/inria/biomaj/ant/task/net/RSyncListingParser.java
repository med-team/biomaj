/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task.net;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import org.apache.tools.ant.Project;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.utils.BiomajBuildException;

public class RSyncListingParser implements ListingParser {

	private static final String DATE_FORMAT_RSYNC="yyyy/MM/dd HH:mm:ss"; 
	/**
	 * Line to parse
	 * @uml.property  name="line"
	 */
	private String line;
	/**
	 * Current Ant Project
	 * @uml.property  name="project"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private Project project ;
	/**
	 * result file parsed
	 * @uml.property  name="remoteFile"
	 * @uml.associationEnd  
	 */
	private RemoteFile remoteFile;
	
	/**
	 * Constructeur
	 * @param s
	 * @param p
	 */
	public RSyncListingParser(Project p) {
		project = p;
	}

	public String getFileName() {
		return remoteFile.getName();
	}

	public long getFileSize() {
		return remoteFile.getSize();
	}

	public String getLinkName() {
		return remoteFile.getLinkName();
	}

	public long getTime() {
		return remoteFile.getDate().getTime();
	}

	public boolean isLink() {
		return remoteFile.isLink();
	}
	
	public boolean parse() throws ParseException {
		project.log("Rsync mode: parsing line:"+line,Project.MSG_DEBUG);
		if (line==null)
			throw new BiomajBuildException(project,"rsyncListingParser.error.init",new Exception());
		
		String[] tabAtt = line.split("[\\s]");
		Vector<String> tabAttribut = new Vector<String>();
		
		//Remove blanks!
		for (int i=0;i<tabAtt.length;i++) {
			if (tabAtt[i].trim().compareTo("")!=0)
				tabAttribut.add(tabAtt[i]);
		}
		
		// there are a minimum of 5 index : permission-size-date-date-nameFile
		if (tabAttribut.size()<5)
			throw new BiomajBuildException(project,"rsyncListingParser.error.parse",line,new Exception());
		
		
		try {
			//For instance, link are not managed!!!!
			/*if (line.charAt(0)=='l')
				throw new BiomajBuildException(project,"rsyncListingParser.error.link",line);
			*/
			if (line.charAt(0)=='l')
				{
				project.log("Find a link:"+line,Project.MSG_VERBOSE);
				
				}
			remoteFile = new RemoteFile();
			//remoteFile.setValueString(line);
			//if is directory
			remoteFile.setDir(tabAttribut.get(0).trim().charAt(0)=='d');			
			
			remoteFile.setSize(Long.valueOf(tabAttribut.get(1).trim()));
			project.log("Rsync mode - value of size:"+remoteFile.getSize(),Project.MSG_DEBUG);
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_RSYNC);
			
			Date date = sdf.parse(tabAttribut.get(2).trim()+" "+tabAttribut.get(3).trim());
			
			remoteFile.setDate(date);
			project.log("Rsync mode - value of date:"+sdf.format(remoteFile.getDate()),Project.MSG_DEBUG);
			remoteFile.setName(tabAttribut.get(tabAttribut.size()-1));
			project.log("Rsync mode - name file:"+remoteFile.getName(),Project.MSG_DEBUG);
			return true;
		} catch (Exception e) {
			throw new BiomajBuildException(project,e);
		}
	}

	/**
	 * @return  the remoteFile
	 * @uml.property  name="remoteFile"
	 */
	public RemoteFile getRemoteFile() {
		return remoteFile;
	}
	
	/**
	 * @param line  the line to set
	 * @uml.property  name="line"
	 */
	public void setLine(String s) {
		line = s;
	}

}
