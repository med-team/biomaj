/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task;


import java.util.regex.Pattern;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.inria.biomaj.utils.BiomajConst;

public class BmajTask extends Task {
	
	/** STATIC VARIABLES DEFINED */
	public static final String filterAddDownloadedFile = "__biomaj:download:";
	public static final String regExpAddDownloadedFile = "(\\S+),(\\d+),(\\d+)";
	
	public static final String filterAddLocalOfflineFile = "__biomaj:localOffline:";
	public static final String regExpAddLocalOfflineFile ="(\\S+)";
	
	public static final String filterAddLocalOnlineFile = "__biomaj:localOnline:";
	public static final String regExpAddLocalOnlineFile ="(\\S+)";
	
	public static final String filterAddExtractedFile = "__biomaj:extract:";
	public static final String regExpAddExtractedFile = "(\\S+),(\\S+)";
	
	public static final String filterAddProductionFile = "__biomaj:production:";
	public static final String regExpAddProductionFile = "(\\S+),(\\S+)";
	
	public static final String filterAddProcess = "__biomaj:addProcess:";
	public static final String filterProcessTerminatedIsOk = "__biomaj:processTerminatedIsOk:";
	                                                       
	/**
	 * Add production file in the log4j flow
	 * @param mode
	 * @param prodDir
	 * @param location
	 */
	protected synchronized void addProductionFile(String location,boolean copy) {
		log(filterAddProductionFile+location+","+Boolean.toString(copy),Project.MSG_DEBUG);
	}
		
	/**
	 * Add an extract file in log4j flow
	 * @param nameCompress
	 * @param hashCompressedFile
	 * @param nameUncompressed
	 */
	protected synchronized void addExtractFile(String nameUncompressed,String refHash) {

		String patternsExcluded = getProject().getProperty(BiomajConst.localFilesExcludedProperty) ;
		String[] listPatternExcluded = patternsExcluded.split("\\s");
		for (int i = 0; i<listPatternExcluded.length;i++) {
			Pattern p = Pattern.compile(listPatternExcluded[i]);
			if (p.matcher(nameUncompressed).find()) {
				getProject().log("file ["+nameUncompressed+"] matches with ["+listPatternExcluded[i]+"] (from property :"+BiomajConst.localFilesExcludedProperty+")",Project.MSG_WARN);
				getProject().log("file ["+nameUncompressed+"] is excluding from the extract list.",Project.MSG_WARN);
				return;
			} 
		}
		
		String valUncompressed = nameUncompressed;
		log(filterAddExtractedFile+valUncompressed+","+refHash,Project.MSG_DEBUG);
	}

	/**
	 * Add a downloaded file in the log4j flow
	 * @param location
	 * @param timeOnServer
	 * @param sizeOnServer
	 */
	public synchronized void addDownloadFile(String location,String timeOnServer,String sizeOnServer) {
		String locationAbs = getProject().getProperty("offline.dir")+"/"+location;
		log(filterAddDownloadedFile+locationAbs+","+timeOnServer+","+sizeOnServer,Project.MSG_DEBUG);
	}

	/**
	 * Add File find in offline directory
	 * @param location
	 * @param timeOnServer
	 * @param sizeOnServer
	 */
	protected void addLocalOfflineFile(String location) {
		log(filterAddLocalOfflineFile+location,Project.MSG_DEBUG);
	}
	
	/**
	 * Add File find in online directory
	 * @param location
	 * @param timeOnServer
	 * @param sizeOnServer
	 */
	protected void addLocalOnlineFile(String location) {
		log(filterAddLocalOnlineFile+location,Project.MSG_DEBUG);
	}
	
	@Override
	public synchronized void log(String msg, int mode){
		super.log(msg,mode);
	}
	

}
