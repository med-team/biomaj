/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.gmod.biomaj.ant.task.InputValidation;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajUtils;

public class BmajDelete extends BmajTask {

	/**
	 * @uml.property  name="listingExclude"
	 */
	private String listingExclude;
	/**
	 * @uml.property  name="dir"
	 */
	private String dir;
	
	@Override
	public void execute() throws BuildException {
		InputValidation.checkString(getProject(),listingExclude, "listing exclud has to be set");
		InputValidation.checkString(getProject(),dir, "directory has to be set");
		BufferedReader br;
		Vector<File> files      = new Vector<File>();
    	try {
    		br = new BufferedReader(new FileReader(listingExclude));
    		String line;
    		while ((line=br.readLine())!= null) {
    			if (line.compareTo("#PLACE_HOLDER_TO_PREVENT_EMPTY_FILE")==0)
    				break;
    			File f = new File(line);
    			if (!f.exists()) {
    				log("file does not exist :"+line,Project.MSG_WARN); 
    				continue;
    			}
    			files.add(f);
    		}
		} catch (IOException e) {
			throw new BiomajBuildException(getProject(),e);
		}
		
		File dirD = new File(dir);
		
		if (! dirD.exists()) {
			log("${dir} does not exist :"+dirD.getAbsolutePath(),Project.MSG_ERR);
			throw new BiomajBuildException(getProject(),null);
		}
		
		
		if (! dirD.isDirectory()) {
			log("${dir} is not a directory :"+dirD.getAbsolutePath(),Project.MSG_ERR);
			throw new BiomajBuildException(getProject(),null);
		}
		
		for (File f : dirD.listFiles())
			BiomajUtils.deleteAll(f,files);
	}

	/**
	 * @return  the dir
	 * @uml.property  name="dir"
	 */
	public String getDir() {
		return dir;
	}

	/**
	 * @param dir  the dir to set
	 * @uml.property  name="dir"
	 */
	public void setDir(String dir) {
		this.dir = dir;
	}

	/**
	 * @return  the listingExclude
	 * @uml.property  name="listingExclude"
	 */
	public String getListingExclude() {
		return listingExclude;
	}

	/**
	 * @param listingExclude  the listingExclude to set
	 * @uml.property  name="listingExclude"
	 */
	public void setListingExclude(String listingExclude) {
		this.listingExclude = listingExclude;
	}
	
}
