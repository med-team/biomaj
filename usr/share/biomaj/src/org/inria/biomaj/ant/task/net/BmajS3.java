package org.inria.biomaj.ant.task.net;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.task.BmajTask;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.internal.ant.task.net.S3Impl;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;


/**
 * Ant task called in mirror.xml for S3 download.
 * 
 * @author rsabas
 *
 */
public class BmajS3 extends BmajTask {
	
	private String server;
	private Integer port;
	private String userId;
	private String password;
	private String remoteDir;
	private String listing;
	private String toDir;
	
	@Override
	public void execute() throws BuildException {
		log("S3 download ant task", Project.MSG_INFO);
		
		BufferedReader br;
		Map<String, List<RemoteFile>> files = new HashMap<String, List<RemoteFile>>();
		try {
			br = new BufferedReader(new FileReader(listing));
			String line;
			while ((line=br.readLine()) != null) {
				RemoteFile rf = new RemoteFile(line);
				if (files.get(rf.getBase()) == null) {
					List<RemoteFile> lst = new ArrayList<RemoteFile>();
					lst.add(rf);
					files.put(rf.getBase(), lst);
				} else {
					files.get(rf.getBase()).add(new RemoteFile(line));
				}
			}
		} catch (IOException e) {
			throw new BiomajBuildException(getProject(),e);
		} catch (BiomajException be) {
			throw new BuildException(be);
		} catch (ParseException e) {
			throw new BiomajBuildException(getProject(),e);
		}
		
		
		/*
		 * Retrieving number of threads to run
		 */
		int numThread = 1;
		try {
			String filesNumThread = BiomajUtils.getProperty(this.getProject(),BiomajConst.filesNumThreadProperty);
			numThread = new Integer(filesNumThread);
		} catch (BiomajException e) {
			log("files.num.thread is not defined ! New value is : 1",Project.MSG_WARN);
		}

		if (numThread <= 0){
			log("Bad value for files.num.thread ("+numThread+"). New value is : 1",Project.MSG_WARN);
			numThread = 1;
		}
		
		S3Impl s3 = new S3Impl(this);
		s3.init(server, port, userId, password);
		
		int i = 0;
		List<String> dlQueue = new ArrayList<String>();
		for (String bucket : files.keySet()) {
			for (RemoteFile rf : files.get(bucket)) {
				dlQueue.add(rf.getName());
				if (i++ == numThread) {
					s3.downloadFiles(bucket, dlQueue, toDir);
					dlQueue.clear();
					i = 0;
				}
			}
			// Less files than threads in that bucket. Force download.
			if (i != 0 && i <= numThread) {
				s3.downloadFiles(bucket, dlQueue, toDir);
				dlQueue.clear();
				i = 0;
			}
		}
		
	}


	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRemoteDir() {
		return remoteDir;
	}

	public void setRemoteDir(String remoteDir) {
		this.remoteDir = remoteDir;
	}

	public String getListing() {
		return listing;
	}

	public void setListing(String listing) {
		this.listing = listing;
	}

	public String getToDir() {
		return toDir;
	}

	public void setToDir(String toDir) {
		this.toDir = toDir;
	}

	
	
}
