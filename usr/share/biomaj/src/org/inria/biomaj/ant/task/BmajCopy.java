/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Execute;
import org.apache.tools.ant.taskdefs.LogStreamHandler;
import org.apache.tools.ant.types.Commandline;
import org.gmod.biomaj.ant.task.InputValidation;
import org.inria.biomaj.ant.logger.BiomajConsoleLogger;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * <p>This task is instanciate in mirror.xml with the name : bmaj-addlocalfile.<br/>
 * check biomaj_common.xml to see the map of task.
 * </p>
 * 
 * @author  ofilangi
 * @version  Biomaj 0.9
 * @since  Biomaj 0.8 /Citrina 0.5
 */
public class BmajCopy extends BmajTask {

	/**
	 * @uml.property  name="remotedir"
	 */
	private String from;

	/**
	 * @uml.property  name="listing"
	 */
	private String listing;
	/**
	 * @uml.property  name="toDir"
	 */
	private String toDir;

	/**
	 * @uml.property  name="continueOnError"
	 */
	private boolean failOnError = true;

	/**
	 * @uml.property  name="cmd"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private Commandline cmd = new Commandline();
	
	private String computed;


	public BmajCopy() {
		cmd.setExecutable("cp");
		cmd.createArgument().setValue("-pf");
	}


	@Override
	public void execute() throws BuildException {
		InputValidation.checkString(getProject(),from, "");
		InputValidation.checkString(getProject(),listing, "");
		InputValidation.checkString(getProject(),toDir, "");
		BufferedReader br;
		Vector<RemoteFile> files      = new Vector<RemoteFile>();
    	try {
    		br = new BufferedReader(new FileReader(listing));
    		String line;
    		while ((line=br.readLine())!= null) {
    			files.add(new RemoteFile(line));
    		}
		} catch (IOException e) {
			throw new BiomajBuildException(getProject(),e);
		} catch (BiomajException be) {
			throw new BuildException(be);
		} catch (ParseException pe) {
			throw new BiomajBuildException(getProject(),pe);
		}
		boolean logFiles = Boolean.valueOf(getProject().getProperty(BiomajConst.logFilesProperty));
		int count = 0;
		log("["+from+"] => ["+toDir+"]",Project.MSG_INFO);	
		for (int i=0;i<files.size();i++) {
			try {
				Commandline c = (Commandline) cmd.clone();
				c.createArgument().setValue(from+"/"+files.get(i).getAbsolutePath());
				
				String filePath = toDir+"/"+getTargetDirectory(files.get(i).getAbsolutePath());
				BiomajUtils.createSubDirectories(filePath);
				c.createArgument().setValue(filePath);
				Execute exe = new Execute(new LogStreamHandler(this, 
						Project.MSG_INFO,
						Project.MSG_ERR),
						null);
				exe.setCommandline(c.getCommandline());
				exe.execute();

				int code_retour = exe.getExitValue();

				if (code_retour!=0) {
					if (failOnError)
						throw new BiomajBuildException(getProject(),"copy.error",files.get(i).getAbsolutePath(),new Exception());
					continue;
				}

				float a = ((float)++count/ (float)files.size())*100;
				log(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE+"["+Integer.toString((int)a)+"%]",Project.MSG_INFO);	
				
				if (logFiles) {
					addDownloadFile(getTargetDirectory(files.get(i).getAbsolutePath()) + "/" + getFileName(files.get(i).getAbsolutePath()),
							Long.toString(files.get(i).getDate().getTime()), Long.toString(files.get(i).getSize()));
//					addDownloadFile(files.get(i).getAbsolutePath(), Long.toString(files.get(i).getDate().getTime()), Long.toString(files.get(i).getSize()));
				}

			} catch (Exception e) {
				log("I/O Exception :"+e.getMessage(),Project.MSG_WARN);
				if (!failOnError)
					throw new BiomajBuildException(getProject(),e);
			}
		}
		log("ok",Project.MSG_INFO);	
	}


	protected String getTargetDirectory(String pathFile) {
		int index = pathFile.lastIndexOf('/');
		if (index == -1)
			return "";
		
		/*
		 * If we get here and bank is computed, it means that remote.files is not empty.
		 * remote.files is computed from bankName.files and contains the bank path relative to data.dir.
		 * Bank production dir will look like : data.dir/dir.version/release/flat/[child]dir.version/release/flat/...
		 * We will transform that into : data.dir/dir.version/release/flat/childBank/flat/...
		 */
		if (computed.equals("true")) {
			String[] deps = BiomajUtils.getBankDependencies(getProject());
			for (String dependency : deps) {
				// 1. Looking for smthg like .../bankName_release[__xx]/...
				// 2. substring from bank_release end
				// 3. add at the beginning bank name
				Pattern p = Pattern.compile(dependency + "_[\\w\\-\\.]+(__\\d+)?");
				Matcher m = p.matcher(pathFile);
				if (m.find()) {
					pathFile = dependency  + "/" + pathFile.substring(m.end() + 1);
				}
			}
		}
		index = pathFile.lastIndexOf('/');
		return pathFile.substring(0, index);
	}

	protected String getFileName(String pathFile) {
		int index = pathFile.lastIndexOf('/');
		if (index == -1)
			return pathFile;
		
		return pathFile.substring(index+1);
	}
	
	/**
	 * @return  the failOnError
	 * @uml.property  name="failOnError"
	 */
	public boolean isFailOnError() {
		return failOnError;
	}


	/**
	 * @param failOnError  the failOnError to set
	 * @uml.property  name="failOnError"
	 */
	public void setFailOnError(boolean continueOnError) {
		this.failOnError = continueOnError;
	}


	/**
	 * @return  the from
	 * @uml.property  name="from"
	 */
	public String getFrom() {
		return from;
	}


	/**
	 * @param from  the from to set
	 * @uml.property  name="from"
	 */
	public void setFrom(String from) {
		this.from = from;
	}


	/**
	 * @return  the listing
	 * @uml.property  name="listing"
	 */
	public String getListing() {
		return listing;
	}


	/**
	 * @param listing  the listing to set
	 * @uml.property  name="listing"
	 */
	public void setListing(String listing) {
		this.listing = listing;
	}


	/**
	 * @return  the toDir
	 * @uml.property  name="toDir"
	 */
	public String getToDir() {
		return toDir;
	}


	/**
	 * @param toDir  the toDir to set
	 * @uml.property  name="toDir"
	 */
	public void setToDir(String toDir) {
		this.toDir = toDir;
	}


	public String getComputed() {
		return computed;
	}


	public void setComputed(String computed) {
		this.computed = computed;
	}

}
