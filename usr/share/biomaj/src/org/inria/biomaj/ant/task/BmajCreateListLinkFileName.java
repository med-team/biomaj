/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task;

import java.io.File;
import java.io.FilenameFilter;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

public class BmajCreateListLinkFileName extends Task {

	/**
	 * @uml.property  name="propertyfilelist"
	 */
	private String propertyfilelist;
	/**
	 * @uml.property  name="filelist"
	 */
	private String filelist ="";

	@Override
	public void execute() throws BuildException {
			
		try {
			File dirOffline = new File(BiomajUtils.getOfflineDirectory(getProject()));
			findAnsWriteFileLinkName(dirOffline);
			filelist = filelist.replaceAll(",$", "");
			getProject().setProperty(propertyfilelist, filelist);
			
		} catch (BiomajException be) {
			throw new BiomajBuildException(getProject(),be);
		} 
		
	}

	private void findAnsWriteFileLinkName(File dir) {
		FileFilterString ff = new FileFilterString(getProject().getProperty("link.file.name"));
		File[] lFile = dir.listFiles(ff);
		
		if (lFile.length>1)
			throw new BiomajBuildException(getProject(),"",new Exception("Internal error (BmajCreateListLinkFileName::findAnsWriteFileLinkName) )"));
		
		if (lFile.length==1) {
				filelist+=lFile[0].getAbsolutePath()+",";
		}
		File[] lDir = dir.listFiles() ;
		for (int i=0;i<lDir.length;i++)
			if (lDir[i].isDirectory())
				findAnsWriteFileLinkName(lDir[i]);
		
	}
	
	private class FileFilterString implements FilenameFilter {

		private String filter ; 
		
		public FileFilterString(String f) {
			filter = f;
		}
			
		public boolean accept(File dir, String name) {
			if (name.compareTo(filter)==0)
				return true;
			
			return false;
		}
		
	}

	/**
	 * @return  the propertyfilelist
	 * @uml.property  name="propertyfilelist"
	 */
	public String getPropertyfilelist() {
		return propertyfilelist;
	}

	/**
	 * @param propertyfilelist  the propertyfilelist to set
	 * @uml.property  name="propertyfilelist"
	 */
	public void setPropertyfilelist(String propertyfilelist) {
		this.propertyfilelist = propertyfilelist;
	}
}
