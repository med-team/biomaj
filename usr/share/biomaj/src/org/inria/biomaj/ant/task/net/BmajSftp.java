package org.inria.biomaj.ant.task.net;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Vector;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.task.BmajTask;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * Ant task that does sftp stuff.
 * It runs n (files.num.thread) SftpThread instances that concurrently
 * handle the downloading stuff.
 *  
 * @author rsabas
 *
 */
public class BmajSftp extends BmajTask {
	
	private String protocol;
	private String server;
	private Integer port;
	private String userId;
	private String password;
	private String remoteDir;
	private String listing;
	private String toDir;
	private Vector<RemoteFile> files;
	
	@Override
	public void execute() throws BuildException {
		log("SFTPClient method", Project.MSG_INFO);
		BufferedReader br;
		files = new Vector<RemoteFile>();
		try {
			br = new BufferedReader(new FileReader(listing));
			String line;
			while ((line=br.readLine())!= null) {
				files.add(new RemoteFile(line));
			}
		} catch (IOException e) {
			throw new BiomajBuildException(getProject(),e);
		} catch (BiomajException be) {
			throw new BuildException(be);
		} catch (ParseException e) {
			throw new BiomajBuildException(getProject(),e);
		}
		
		
		/*
		 * Retrieving number of threads to run
		 */
		int numThread = 1;
		try {
			String filesNumThread = BiomajUtils.getProperty(this.getProject(),BiomajConst.filesNumThreadProperty);
			numThread = new Integer(filesNumThread);
		}
		catch (BiomajException e) {
			log("files.num.thread is not defined ! New value is : 1",Project.MSG_WARN);
		}

		if (numThread <= 0){
			log("Bad value for files.num.thread ("+numThread+"). New value is : 1",Project.MSG_WARN);
			numThread = 1;
		}
		
		
		Vector<SftpThread> runningThreads = new Vector<SftpThread>();
		
		// Starting the threads
		for (int i = 0; i < numThread; i++) {
			SftpThread thread = new SftpThread(this, i);
			runningThreads.add(thread);
			thread.start();
		}
		// Waiting for the threads to end
		for (SftpThread th : runningThreads) {
			try {
				th.join();
			} catch (InterruptedException e) {
				log("Thread synchronization error : " + e.getMessage());
				e.printStackTrace();
			}
		}
		
		/*
		RemoteCommandImpl pro = new SftpImpl(this);
		try {
			long totaltime = 0;
			pro.init(server,port,userId,password);
			Date m_chrono = new Date();
			for (int i=0;i<files.size();i++) {
				RemoteFile rf = files.get(i);
				int j=0;
				log("downloading:"+rf.getAbsolutePath(),Project.MSG_VERBOSE);
				while ((!pro.getFile(remoteDir,rf.getAbsolutePath(),toDir,rf.getName()))&&(j<WgetThread.NB_TRY)) {   	
					j++;
					log(j+" try downloading:"+rf.getAbsolutePath(),Project.MSG_INFO);
				}
				Date end = new Date();
				Long l = new Long(end.getTime() - m_chrono.getTime());
				totaltime+=l;
				addDownloadFile(rf.getAbsolutePath(), Long.toString(rf.getDate().getTime()), Long.toString(rf.getSize()));
				log(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE+Integer.toString(i+1)+"/"+Integer.toString(files.size())+" file(s) downloaded!",Project.MSG_INFO);
			} 
			log(Integer.toString(files.size())+"/"+Integer.toString(files.size())+" file(s) downloaded! time:"+BiomajUtils.timeToString(totaltime),Project.MSG_INFO);
		} catch (BiomajException e) {
			throw new BiomajBuildException(getProject(),e);
		} catch (Exception e) {
			throw new BiomajBuildException(getProject(),e);
		} finally {
			if (pro != null)
				pro.disconnect();
		}*/

	}
	
	/**
	 * Synchronized method thats returns to the threads asking it
	 * the file to download.
	 * 
	 * @return File to download or null there isn't any
	 */
	public synchronized RemoteFile getFile() {
		if (files.size() > 0) {
			RemoteFile rf = files.get(0);
			files.remove(0);
			return rf;
		}
		
		return null;
	}
	
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRemoteDir() {
		return remoteDir;
	}
	public void setRemoteDir(String remoteDir) {
		this.remoteDir = remoteDir;
	}
	public String getListing() {
		return listing;
	}
	public void setListing(String listing) {
		this.listing = listing;
	}
	public String getToDir() {
		return toDir;
	}
	public void setToDir(String toDir) {
		this.toDir = toDir;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
