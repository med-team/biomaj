/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.logger;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import org.apache.tools.ant.BuildEvent;
import org.inria.biomaj.ant.task.BmajExecute;
import org.inria.biomaj.session.bank.Bank;
import org.inria.biomaj.session.bank.FileDesc;
import org.inria.biomaj.session.process.BiomajProcess;
import org.inria.biomaj.session.process.MetaProcess;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.singleton.BiomajSession;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;

/**
 * <p>Handle the mapping of Process object and database</p>
 * 
 * @author ofilangi
 * @version Biomaj 0.9
 * @since Biomaj 0.8 /Citrina 0.5
 */
public class DBProcessLogger {

	/**
	 * @uml.property  name="bank"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private Bank bank = null;
	/**
	 * @uml.property  name="metaProcess"
	 * @uml.associationEnd  
	 */
	private MetaProcess metaProcess = null;
	/**
	 * @uml.property  name="current"
	 * @uml.associationEnd  
	 */
	private BiomajProcess current;

	public DBProcessLogger(Bank bank) throws BiomajException {

		if (bank == null) {
			BiomajLogger.getInstance().log("Error internal: not find session in XmlProcessLogger");
			throw new BiomajException("");
		}

		this.bank = bank;
	}


	public void addPostProcess(BuildEvent arg0) throws BiomajException {

		String keyname     = arg0.getProject().getProperty("name_process");
		String name        = arg0.getProject().getProperty(keyname+".name");
		String exe         = arg0.getProject().getProperty(keyname+".exe");
		String args        = arg0.getProject().getProperty(keyname+".args");
		String desc        = arg0.getProject().getProperty(keyname+".desc");
		String type        = arg0.getProject().getProperty(keyname+".type");

		current = new BiomajProcess();
		current.setKeyName(keyname);
		current.setArgs(args);
		current.setExe(exe);
		current.setNameProcess(name);
		current.setDescription(desc);
		current.setType(type);
		current.setStart(new Date());
		current.setTimeStampExe(BmajExecute.getTimeStampExe(BmajExecute.getPathExe(null,exe)));
		metaProcess.setStart(current.getStart());
		metaProcess.setEnd(new Date());
		metaProcess.getListProcess().add(current);

		updateInDB();

	}




	public void endPostProcess(BuildEvent arg0) throws BiomajException{
		if (current==null) {
			BiomajLogger.getInstance().log("Can't initialized end date for process!(XmlProcessLogger)");
			return;
		}
		current.setEnd(new Date());
		String value = arg0.getProject().getProperty("returnValue");
		if (value!=null)
			current.setReturnValue(Integer.valueOf(value));
		else {
			BiomajLogger.getInstance().log("Error Biomaj :Can't get return value from script!");
		}
		updateInDB();
		current=null;
	}


	public void endMetaProcessProcess(BuildEvent arg0) throws BiomajException{
		metaProcess.setEnd(new Date());
		updateInDB();
	}


	public void errorMessage(String message, BuildEvent arg0) throws BiomajException{

		if (current==null)
			metaProcess.getErr().add(message);
		else
			current.getErr().add(message);
		updateInDB();
	}


	public void warnMessage(String message, BuildEvent arg0) throws BiomajException{
		if (current==null)
			metaProcess.getWarn().add(message);
		else
			current.getWarn().add(message);
		updateInDB();

	}	


	private void updateInDB() throws BiomajException {

		if ((metaProcess.getName()==null)||(metaProcess.getName().trim().compareTo("")==0))	{
			BiomajLogger.getInstance().log("Object MetaProcess initialized without name!");
			return;
		}
		DBWriter.createMetaprocess(bank.getConfig().getName(),bank.getCurrentSession(), metaProcess);
	}


//	private Vector<FileDesc> getFilesOnSystem(String path,boolean volatil) {
//		Vector<FileDesc> list = new Vector<FileDesc>();
//		String[] all = path.split("/");
//
//		if (all.length<=0)
//		{
//			System.err.println("Path for file dependence has been absolute");
//			return list;
//		}
//
//		try {
//			Pattern filePattern = Pattern.compile(all[all.length-1]);
//
//			String directory = path.replace("/"+all[all.length-1], "");
//
//			File workDir = new File(directory);
//
//			if (!workDir.exists()) {
//				BiomajLogger.getInstance().log(directory+" does not exist!");
//				current.setErrorDetected(true);
//				return list;
//			}
//
//			if (workDir.isDirectory()) {
//				File[] l = workDir.listFiles();
//				for (File f : l) {
//					if (filePattern.matcher(f.getName()).find()) {
//						FileDesc fd = new FileDesc(f,volatil);
//						list.add(fd);
//					}
//				}
//			}
//		} catch (Exception e) {
//			BiomajLogger.getInstance().log(all[all.length-1] + " bad java regular expression! (use [\\w]+ and not *)");
//		}
//
//		return list;
//
//	}

	private void addDependance(String dbname,String origineFiles, String newFiles,BuildEvent arg0,boolean volatil) throws BiomajException {

		String[] news = newFiles.split("\\s");
		for (String i : news) {
			//On ne gere pas les expressions reguliere....
			//current.getDependancesOutput().addAll(getFilesOnSystem(i,volatil));
			
			if (i.contains(BiomajConst.futureReleaseLink)) { 
				Bank b = BiomajSession.getInstance().getBank(dbname);
				if (b != null) {
//					String[] dirs =  b.getWorkflowInfoProductionDir().split("/");
//					i = i.replaceFirst(BiomajConst.futureReleaseLink, dirs[dirs.length-1]);
					File f = new File(i);
					if (f.exists()) {
						try {
							i = f.getCanonicalPath(); // true path without link
						} catch (IOException e) {
							BiomajLogger.getInstance().log(e);
						}
					}
				} else {
 					i = i.replaceFirst(BiomajConst.futureReleaseLink, BiomajConst.currentLink);
				}
			}
			try {
				current.getDependancesOutput().add(new FileDesc(new File(i),volatil));
			} catch (BiomajException be) {
				BiomajLogger.getInstance().log(be.getMessage());
			}
		}
	}


	public void addDependanceFiles(String dbname,String origineFiles, String newFiles,BuildEvent arg0) throws BiomajException {
		addDependance(dbname,origineFiles,newFiles,arg0,false);
	}





	public void addVolatileDependanceFiles(String dbname,String origineFiles, String newFiles, BuildEvent arg0) throws BiomajException {
		addDependance(dbname,origineFiles,newFiles,arg0,true);
	}


	public void addPreProcess(BuildEvent arg0) throws BiomajException {
		addPostProcess(arg0);
		// Same stuff as for a post process
	}


	public void endPreProcess(BuildEvent arg0) throws BiomajException {
		endPostProcess(arg0);
		// Same stuff as for a post process
	}


	public void addRemoveProcess(BuildEvent arg0) throws BiomajException {
		addPostProcess(arg0);
		// Same stuff as for a post process
	}


	public void endRemoveProcess(BuildEvent arg0) throws BiomajException {
		endPostProcess(arg0);
		// Same stuff as for a post process
	}

	/**
	 * @return  the metaProcess
	 * @uml.property  name="metaProcess"
	 */
	public MetaProcess getMetaProcess() {
		return metaProcess;
	}


	/**
	 * @param metaProcess  the metaProcess to set
	 * @uml.property  name="metaProcess"
	 */
	public void setMetaProcess(MetaProcess mp) {
		metaProcess = mp;

	}


	public void setErrorOnCurrentProcess() throws BiomajException {
		if (metaProcess.getListProcess().size()>0) {
			metaProcess.getListProcess().get(metaProcess.getListProcess().size()-1).setErrorDetected(true);
			updateInDB();
		}
	}



}
