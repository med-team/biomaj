package org.inria.biomaj.ant.task.net;

import java.util.Date;

import org.apache.tools.ant.Project;
import org.inria.biomaj.internal.ant.task.net.FtpImpl;
import org.inria.biomaj.internal.ant.task.net.HttpImpl;
import org.inria.biomaj.internal.ant.task.net.RemoteCommandImpl;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.internal.ant.task.net.SftpImpl;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajException;

/**
 * Thread that that handles downloading with a specified protocol.
 * 
 * @author rsabas
 *
 */
public class DownloadThread extends Thread {

	private BmajDownload task;
	private String threadName;
	private int threadId;
	
	private Exception error = null;
	
	public DownloadThread(BmajDownload task, int pos) {
		this.task = task;
		threadName = "downloadthead" + pos;
		threadId = pos;
	}
	
	/**
	 * Runs the parallel download task.
	 * Asks Bmajsftp for files to download and stops once there's no left.
	 */
	@Override
	public void run() {
	
		String protocol = task.getProtocol();
		RemoteCommandImpl pro;
		if (protocol.equals(RemoteCommand.FTP_PROTOCOL))
			pro = new FtpImpl(task);
		else if (protocol.equals(RemoteCommand.HTTP_PROTOCOL))
			pro = new HttpImpl(task);
		else
			pro = new SftpImpl(task);
		
		try {
			long totaltime = 0;
			int fileCount = 0;
			pro.init(task.getServer(), task.getPort(), task.getUserId(), task.getPassword());
			Date m_chrono = new Date();
			RemoteFile rf = null;
			while ((rf = task.getFile()) != null) {
				int j = 0;
				task.log(threadName + " downloading:"+rf.getAbsolutePath(),Project.MSG_VERBOSE);
				while ((!pro.getFile(task.getRemoteDir(), rf.getAbsolutePath(), task.getToDir(),rf.getAbsolutePath())) && (j<WgetThread.NB_TRY)) {   	
					j++;
					task.log(j+" attempt for : "+rf.getAbsolutePath(),Project.MSG_INFO);
				}
				if (j > WgetThread.NB_TRY)
					throw new BiomajException("Could not download " + rf.getAbsolutePath() + ", " + WgetThread.NB_TRY + " tries failed");
				fileCount++;
				Date end = new Date();
				Long l = new Long(end.getTime() - m_chrono.getTime());
				totaltime += l;
				task.logDownload(threadId);
//				task.log(rf.getAbsolutePath() + " downloaded");
				task.addDownloadFile(rf.getAbsolutePath(), Long.toString(rf.getDate().getTime()), Long.toString(rf.getSize()));
			}
			task.log(threadName + " : " + fileCount + " files downloaded in " + totaltime,Project.MSG_VERBOSE);
		} catch (BiomajException e) {
			// Throwing exception here is useless as they won't be treated by ant
			// and so won't stop the workflow.
			// At the end of the thread the master will retrieve that exception
			// and throw it.
			error = new BiomajBuildException(task.getProject(),e);
		} catch (Exception e) {
			error = new BiomajBuildException(task.getProject(),e);
		} finally {
			if (pro != null)
				pro.disconnect();
		}

	}
	
	public Exception getError() {
		return error;
	}

}
