package org.inria.biomaj.ant.task.test;

import static junit.framework.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;
import org.inria.biomaj.ant.task.BmajLink;
import org.inria.biomaj.utils.BiomajUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for BmajLink.
 * 
 * @author rsabas
 *
 */
public class TestBmajLink {
	
	private static final String FROM_DIR = BiomajUtils.getBiomajRootDirectory() + "/fromtestlink";
	private static final String TO_DIR = BiomajUtils.getBiomajRootDirectory() + "/totestlink";
	private List<File> toLink;
	private FileSet fileSet;
	
	/**
	 * Creates source and destination directories and files to move.
	 */
	@Before
	public void setup() {
		toLink = new ArrayList<File>();
		fileSet = new FileSet();
		File fFrom = new File(FROM_DIR);
		File fTo = new File(TO_DIR);
		if (fFrom.exists()) {
			BiomajUtils.deleteAll(fFrom);
		}
		if (fTo.exists()) {
			BiomajUtils.deleteAll(fTo);
		}
		fFrom.mkdir();
		fTo.mkdir();
		
		for (int i = 0; i < 5; i++) {
			File f = new File(FROM_DIR + "/testmove" + i);
			writeRandom(f);
			toLink.add(f);
		}

		
		fileSet.setDir(new File(FROM_DIR));
		
		BmajLink link = new BmajLink();
		link.setTodir(TO_DIR);
		link.setProject(new Project());
		link.addFileSet(fileSet);
		
		try {
			TimeUnit.SECONDS.sleep(2); // Wait for files to be created
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		link.execute();
		
		try {
			TimeUnit.SECONDS.sleep(2); // Wait for files to be created
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Removes the directories that contained the test files.
	 */
	@After
	public void cleanup() {
		BiomajUtils.deleteAll(new File(FROM_DIR));
		BiomajUtils.deleteAll(new File(TO_DIR));
	}

	/**
	 * Test the move method via file copy.
	 */
	@Test
	public void testLinksExist() {
		
		for (File f : toLink) {
			File check = new File(TO_DIR + "/" + f.getName());
			assertTrue(check.exists());
			
		}
	}
	
	public void testPathsAreDifferent() {
		for (File f : toLink) {
			File check = new File(TO_DIR + "/" + f.getName());
			try {
				// It is a link if canonical and absolute path are different.
				assertTrue(!check.getCanonicalPath().equals(check.getAbsolutePath()));
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * Writes a random content in a file.
	 * 
	 * @param file
	 */
	private static void writeRandom(File file) {
		file.delete();
		String rnd = "alezkj lkj-ij mk	j ifj mkjqsmkj	m kjsqmdlkj qmskj " +
				"szd kqjm kj mqskj mkfjmri mlzkrjzm lkj mlkj zoiapodjk kj zklj " +
				"zevdmlkjb iJmZRIJMQKLJ eROBJoMllmkj mkj mlkjdm lkjm";
		
		PrintWriter pw;
		try {
			pw = new PrintWriter(file);
			pw.println(rnd);
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
}
