/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task.net;

import java.io.BufferedReader;
import java.io.FileReader;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;



public class BmajRemoteListing extends RemoteCommand {
	
	/**
	 * @uml.property  name="remoteDir"
	 */
	private String remoteDir;
	
	/**
	 * @uml.property  name="remoteFiles"
	 */
	private String remoteFiles;
	
	/**
	 * @uml.property  name="excludedFiles"
	 */
	private String excludedFiles="";
	
	/**
	 * @uml.property  name="listingFile"
	 */
	private String listingFile;
	
	
	@Override
	public void execute() throws BuildException {
		checkInputInit();
		initRemoteCommand();
		
		if ((remoteDir.length()>0)&&(remoteDir.trim().charAt(0)!='/')) 
			throw new BiomajBuildException(getProject(),"error.remote.directory.malformed",remoteDir,new Exception());
		
		try {
			log(getProject().getProperty(BiomajConst.dbNameProperty)+": Checking files at "+this.getProtocol()+"://"+this.getServer()+":"+this.getRemoteDir()+".",Project.MSG_INFO);
			
			impl.getListingFilesWithBufferFile(remoteDir,listingFile,remoteFiles,excludedFiles);
			BufferedReader br = new BufferedReader(new FileReader(listingFile));
			int count = 0;
			while (br.readLine()!= null)
				count++;
			
			log(Integer.toString(count)+" file(s) found on the server with remote-files["+remoteFiles+"] and excluded-files["+excludedFiles+"]",Project.MSG_INFO);
			
		} catch (BiomajBuildException e) {
			throw e;
		} catch (Throwable e) {
			BiomajLogger.getInstance().log(e.getMessage());
			throw new BiomajBuildException(getProject(),e);
		} finally {
			closeRemoteCommand();
		}
	}
	
	/**
	 * Sets the file name to store the listing information in.
	 * @param listingFile  The listingFile to set.
	 * @uml.property  name="listingFile"
	 */
	public void setListingFile(String listingFile) {
		this.listingFile = listingFile;
		
	}
	
	/**
	 * @return  the remoteDir
	 * @uml.property  name="remoteDir"
	 */
	public String getRemoteDir() {
		return remoteDir;
	}
	
	/**
	 * @param remoteDir  the remoteDir to set
	 * @uml.property  name="remoteDir"
	 */
	public void setRemoteDir(String remoteDir) {
		this.remoteDir = remoteDir;
	}
	
	/**
	 * @return  the remoteFiles
	 * @uml.property  name="remoteFiles"
	 */
	public String getRemoteFiles() {
		return remoteFiles;
	}
	
	/**
	 * @param remoteFiles  the remoteFiles to set
	 * @uml.property  name="remoteFiles"
	 */
	public void setRemoteFiles(String remoteFiles) {
		this.remoteFiles = remoteFiles;
	}
	
	/**
	 * @return  the listingFile
	 * @uml.property  name="listingFile"
	 */
	public String getListingFile() {
		return listingFile;
	}
	
	/**
	 * @return  the excludedFiles
	 * @uml.property  name="excludedFiles"
	 */
	public String getExcludedFiles() {
		return excludedFiles;
	}
	
	/**
	 * @param excludedFiles  the excludedFiles to set
	 * @uml.property  name="excludedFiles"
	 */
	public void setExcludedFiles(String excludedFiles) {
		this.excludedFiles = excludedFiles;
	}
	
}
