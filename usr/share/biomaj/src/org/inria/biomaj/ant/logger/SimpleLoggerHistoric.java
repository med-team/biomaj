/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.logger;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import org.apache.tools.ant.BuildEvent;
import org.apache.tools.ant.BuildLogger;
import org.apache.tools.ant.Project;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * <p>This class implements a SubBuildListener of Ant
 * to write log bank according to the level define by the user.
 * This listener is attached to the mirror.xml and handle_process.xml files.</p>
 * @author ofilangi
 *
 */
public class SimpleLoggerHistoric implements BuildLogger {

	/** Constance state of workflow ERROR_WF */
	public static final String START = "START";
	/** Constance state of workflow OK_WF */
	public static final String END = "END";
	/** log directory */
	/**
	 * Property format Output logfile
	 * @uml.property  name="patternLayout"
	 */
	private String patternLayout ;
	/**
	 * Property level info
	 * @uml.property  name="level"
	 */
	private String level ;
	/**
	 * Target mask
	 * @uml.property  name="targetMask"
	 */
	private boolean targetMask;
	/**
	 * Task mask
	 * @uml.property  name="taskMask"
	 */
	private boolean taskMask;


	/**
	 * @uml.property  name="propertiesMask"
	 */
	private boolean propertiesMask;

	/**
	 * @uml.property  name="file"
	 */
	private String file;

	/**
	 * @uml.property  name="logBuffWriter"
	 */
	private BufferedWriter logBuffWriter ; 

	/**
	 * @uml.property  name="nameFile"
	 */
	private String nameFile = "";

	/**
	 * @uml.property  name="nameDirectory"
	 */
	private String nameDirectory = ""; 
	
	/**
	 * @uml.property  name="isInit"
	 */
	private boolean isInit = false ;

	/** Constructeur */
	public SimpleLoggerHistoric(String nameFile) {
		this.nameFile = nameFile ;
	}

	
	public void init(Project p) throws BiomajException {
		setLevel(p.getProperty(BiomajConst.levelMaskProperty));
		setTaskMask(p.getProperty(BiomajConst.taskMaskProperty));
		setTargetMask(p.getProperty(BiomajConst.targetMaskProperty));
		setPropertiesMask(p.getProperty(BiomajConst.propertiesMaskProperty));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String session = sdf.format(new Date()); 

		String dbName = p.getProperty(BiomajConst.dbNameProperty);

		try {
			if (nameDirectory.trim().compareTo("")==0)
				nameDirectory = dbName+"/"+session;
				

			String dir = BiomajInformation.getInstance().getProperty(BiomajInformation.LOGDIR)+"/"+nameDirectory+"/" ;
			
			
			file = dir + this.nameFile;
			BiomajUtils.createSubDirectories(BiomajUtils.getRelativeDirectory(file));
			
			logBuffWriter = new BufferedWriter (new FileWriter(file));
			isInit = true;

			if (propertiesMask) {
				Enumeration<?> e = p.getProperties().keys();
				while (e.hasMoreElements()) {
					String key = (String)e.nextElement();
					write("**** "+key+" :  "+p.getProperty(key));
				}
			}
		} catch (IOException ioe) {
			BiomajLogger.getInstance().log(ioe);
			throw new BiomajBuildException(p,"io.error",ioe.getMessage(),ioe);
		}
	}

	public void close() throws IOException {
		if (logBuffWriter != null)
			logBuffWriter.close();
	}
	
	public String getNameFile() {
		return file;
	}

	protected static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	public static String prefixMessage () {
		String prec = "["+sdf.format(new Date())+"]";
		prec+="["+Runtime.getRuntime().freeMemory()+"/"+Runtime.getRuntime().maxMemory()+"]";

		return prec ;
	}

	public void write(String message) {
	
	}

	/**
	 * Write message logged
	 * @param bankName
	 * @param target
	 * @param message
	 */
	public void write(BuildEvent arg0) {
		
		final int head = 30;

		if (arg0 == null)
			return;

		if ((level!=null)&&(level.trim().compareTo("")!=0)&&(arg0.getPriority()>getTypeLevel(level)))
			return;


		String prec=prefixMessage();

		prec+="["+arg0.getPriority()+"]";
		
		if ((arg0.getTarget()==null)||(arg0.getTask()==null))
			prec+="["+arg0.getProject().getName()+"]";

		if (targetMask&&(arg0.getTarget()!=null))
			prec+="["+arg0.getTarget().getName()+"]";


		if (taskMask&&(arg0.getTask()!=null))
			prec+="["+arg0.getTask().getTaskName()+"]";

		prec = prec.toUpperCase();
		while (prec.length()<head)
			prec += " ";

		if (arg0.getMessage()!=null)
			prec+=arg0.getMessage();

		try {
			logBuffWriter.write(prec);
			logBuffWriter.newLine();
			logBuffWriter.flush();
		} catch (IOException ioe) {
			BiomajLogger.getInstance().log(prec);
			BiomajLogger.getInstance().log(ioe);
		}
	}

	/**
	 * @return  the level
	 * @uml.property  name="level"
	 */
	public String getLevel() {
		return level;
	}

	/**
	 * @param level  the level to set
	 * @uml.property  name="level"
	 */
	public void setLevel(String level) {
		this.level = level;
	}

	/**
	 * @return  the patternLayout
	 * @uml.property  name="patternLayout"
	 */
	public String getPatternLayout() {
		return patternLayout;
	}

	/**
	 * @param patternLayout  the patternLayout to set
	 * @uml.property  name="patternLayout"
	 */
	public void setPatternLayout(String patternLayout) {
		this.patternLayout = patternLayout;
	}

	public void setTargetMask(String value) {
		if (value!=null) {
			if (value.compareTo("true")==0||value.compareTo("ok")==0)
				targetMask = true;
		} else
			targetMask = false;
	}

	public void setTaskMask(String value) {
		if (value!=null) {
			if (value.compareTo("true")==0||value.compareTo("ok")==0)
				taskMask = true;
		} else
			taskMask = false;
	}

	public void setPropertiesMask(String value) {
		if (value!=null) {
			if (value.compareTo("true")==0||value.compareTo("ok")==0)
				propertiesMask = true;
		} else
			propertiesMask = false;
	}

	public int getTypeLevel(String level) {

		if (this.level.equals("VERBOSE"))
			return Project.MSG_VERBOSE; 
		else if (this.level.equals("INFO"))
			return Project.MSG_INFO; 
		else if (this.level.equals("ERR"))
			return Project.MSG_ERR; 
		else if (this.level.equals("DEBUG"))
			return Project.MSG_DEBUG; 
		else if (this.level.equals("WARN"))
			return Project.MSG_WARN; 

		return Project.MSG_DEBUG; 
	}

	public static String getStringPriority(int priority) {
		switch (priority) {
		case Project.MSG_DEBUG : return "DEBUG";
		case Project.MSG_ERR   : return "ERR";
		case Project.MSG_INFO : return "INFO";
		case Project.MSG_VERBOSE   : return "VERBOSE";
		case Project.MSG_WARN   : return "WARN";
		default : return Integer.toString(priority);
		}

	}

	public void setEmacsMode(boolean arg0) {
		// TODO Auto-generated method stub

	}

	public void setErrorPrintStream(PrintStream arg0) {
		// TODO Auto-generated method stub

	}

	public void setMessageOutputLevel(int arg0) {
		// TODO Auto-generated method stub

	}

	public void setOutputPrintStream(PrintStream arg0) {
		// TODO Auto-generated method stub

	}

	public void buildFinished(BuildEvent arg0) {
		try {
			logBuffWriter.close();
		} catch (IOException ioe) {
			
		}
	}

	public void buildStarted(BuildEvent arg0) {
	}

	public void messageLogged(BuildEvent arg0) {
		if (arg0 == null)
			return ;
		try {
		if (!isInit)
			init(arg0.getProject());
		} catch (BiomajException be) {
			throw new BiomajBuildException(arg0.getProject(),be);
		}
		write(arg0);

	}

	public void targetFinished(BuildEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void targetStarted(BuildEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void taskFinished(BuildEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void taskStarted(BuildEvent arg0) {
		// TODO Auto-generated method stub

	}


	/**
	 * @return  the nameDirectory
	 * @uml.property  name="nameDirectory"
	 */
	public String getNameDirectory() {
		return nameDirectory;
	}


	/**
	 * @param nameDirectory  the nameDirectory to set
	 * @uml.property  name="nameDirectory"
	 */
	public void setNameDirectory(String nameDirectory) {
		this.nameDirectory = nameDirectory;
	}
}
