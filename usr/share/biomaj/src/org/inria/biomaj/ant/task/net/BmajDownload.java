package org.inria.biomaj.ant.task.net;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.logger.BiomajConsoleLogger;
import org.inria.biomaj.ant.task.BmajTask;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * Ant task for downloading files with ftp/http/sftp protocols.
 * It runs n (files.num.thread) downloadthreads that concurrently
 * handle the downloading.
 *  
 * @author rsabas
 *
 */
public class BmajDownload extends BmajTask {
	
	private String protocol;
	private String server;
	private Integer port;
	private String userId;
	private String password;
	private String remoteDir;
	private String listing;
	private String toDir;
	private Vector<RemoteFile> files;
	private int totalFiles;
	
	private Map<Integer,Integer> downloadCount = new HashMap<Integer,Integer>();
	
	@Override
	public void execute() throws BuildException {
		if (!protocol.equals(RemoteCommand.HTTP_PROTOCOL) && !protocol.equals(RemoteCommand.FTP_PROTOCOL) && !protocol.equals(RemoteCommand.SFTP_PROTOCOL))
			throw new BiomajBuildException(getProject(),"error.implementation.find",protocol,new Exception());
		
		log("Download client method for protocol : " + protocol,Project.MSG_INFO);
		BufferedReader br;
		files = new Vector<RemoteFile>();
		try {
			br = new BufferedReader(new FileReader(listing));
			String line;
			while ((line = br.readLine()) != null) {
				files.add(new RemoteFile(line));
			}
		} catch (IOException e) {
			throw new BiomajBuildException(getProject(),e);
		} catch (BiomajException be) {
			throw new BuildException(be);
		} catch (ParseException e) {
			throw new BiomajBuildException(getProject(),e);
		}
		
		totalFiles = files.size();
		
		/*
		 * Retrieving number of threads to run
		 */
		int numThread = 1;
		try {
			String filesNumThread = BiomajUtils.getProperty(this.getProject(),BiomajConst.filesNumThreadProperty);
			numThread = new Integer(filesNumThread);
		}
		catch (BiomajException e) {
			log("files.num.thread is not defined ! New value is : 1",Project.MSG_WARN);
		}

		if (numThread <= 0){
			log("Bad value for files.num.thread ("+numThread+"). New value is : 1",Project.MSG_WARN);
			numThread = 1;
		}
		
		
		Vector<DownloadThread> runningThreads = new Vector<DownloadThread>();
		
		//Don't start more threads that needed
		numThread = files.size() < numThread ? files.size() : numThread;
		
		log("Starting " + numThread + " thread(s) to download " + files.size() + " file(s).",Project.MSG_INFO);
		// Starting the threads
		for (int i = 0; i < numThread; i++) {
			DownloadThread thread = new DownloadThread(this, i);
			runningThreads.add(thread);
			thread.start();
		}
		// Waiting for the threads to end
		for (DownloadThread th : runningThreads) {
			try {
				th.join();
				/* 
				 * If an exception occured for any of the threads, throw that buildexception
				 * which will result in workflow being stopped.
				 */
				if (th.getError() != null)
					throw new BiomajBuildException(getProject(), th.getError());
			} catch (InterruptedException e) {
				log("Thread synchronization error : " + e.getMessage());
			}
		}
	}

	public synchronized void logDownload(int thread) {

		Integer dlFilesCount = 1;
		if ((dlFilesCount = downloadCount.get(thread)) != null) {
			downloadCount.put(thread, dlFilesCount + 1);
		} else
			downloadCount.put(thread, 1);

		String message = "";
		int totalD = 0;

		for (Integer k : downloadCount.keySet()) {
			message += "Thread " + k + "=>[" + downloadCount.get(k) + "] ";
			totalD += downloadCount.get(k);	
		}
		message += " [" + totalD+ "/" + totalFiles +"]";
		log(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE+message,Project.MSG_INFO);
	}
	
	/**
	 * Synchronized method thats returns to the threads asking it
	 * the file to download.
	 * 
	 * @return File to download or null there isn't any
	 */
	public synchronized RemoteFile getFile() {
		if (files.size() > 0) {
			RemoteFile rf = files.get(0);
			files.remove(0);
			return rf;
		}
		
		return null;
	}
	
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRemoteDir() {
		return remoteDir;
	}
	public void setRemoteDir(String remoteDir) {
		this.remoteDir = remoteDir;
	}
	public String getListing() {
		return listing;
	}
	public void setListing(String listing) {
		this.listing = listing;
	}
	public String getToDir() {
		return toDir;
	}
	public void setToDir(String toDir) {
		this.toDir = toDir;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
