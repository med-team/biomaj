package org.inria.biomaj.ant.task;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Execute;
import org.apache.tools.ant.taskdefs.LogStreamHandler;
import org.apache.tools.ant.types.Commandline;
import org.inria.biomaj.ant.logger.BiomajConsoleLogger;
import org.inria.biomaj.session.bank.FileDesc;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

public class ExtractThread extends Thread {

	private BmajExtract extract;
	private boolean continueOnError = false;
	private int total = 0;
	private Exception error = null;
	
	public ExtractThread(BmajExtract extract, boolean continueOnError, int total) {
		this.extract = extract;
		this.continueOnError = continueOnError;
		this.total = total;
	}
	
	public Exception getError() {
		return error;
	}
	
	@Override
	public void run() {
		
		String[] content = null;
		
		String tmpOutputDir = BmajExtract.TMP_DIR + getName();
		

		String inputDir = extract.getDir();
		String outputDir = inputDir;
		if (outputDir.trim().isEmpty() || outputDir.endsWith("/"))
			outputDir += tmpOutputDir;
		else
			outputDir += "/" + tmpOutputDir;
		
		// Create output for thread
		new File(outputDir).mkdirs();
		
		while ((content = extract.getFileToExtract()) != null) {
			
			// Initialize input dir for each file
			inputDir = extract.getDir();
			
			String nameFile = content[0];
			int count = new Integer(content[1]);
			// Extracted file is a single file. No need to do a listing
			boolean isGz = (nameFile.endsWith(".gz") || nameFile.endsWith(".Z") || nameFile.endsWith(".bz2"))
					&& (!nameFile.endsWith(".tar.gz") && !nameFile.endsWith(".tar.Z") && !nameFile.endsWith(".tar.bz2"));
			
			if (isGz) {
				// Output dir cant be specified for gz files. Need to move the file to the output dir before extracting it
				try {
					BiomajUtils.move(new File(inputDir + "/" + nameFile), new File(outputDir + "/" + nameFile));
					inputDir = outputDir;
				} catch (IOException e1) {
					error = e1;
					break;
				}
			}
			
			LogStreamHandler os = new LogStreamHandler(extract, Project.MSG_INFO,Project.MSG_ERR);
	
			float a = ((float)count/ (float) total) * 100;
			extract.log(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE+"["+Integer.toString((int)a)+"%]",Project.MSG_INFO);
	
			Commandline cmd = new Commandline();
	
			if (nameFile.trim().compareTo("") == 0)
				continue;
	
			if (nameFile.contains("PLACE_HOLDER_TO_PREVENT_EMPTY_FILE"))
				continue;
	
			try {
	
				cmd.setExecutable(BiomajInformation.getInstance().getUncompressedOptionWithFile(nameFile,BiomajInformation.OPTION_BIN));
				String option_decomp = BiomajInformation.getInstance().getUncompressedOptionWithFile(nameFile,BiomajInformation.OPTION_DECOMP);
				if (option_decomp!=null) {
					cmd.createArgument().setValue(option_decomp);
					
				}
				cmd.createArgument().setValue(inputDir+"/"+nameFile);
	
				String option_output = BiomajInformation.getInstance().getUncompressedOptionWithFile(nameFile,BiomajInformation.OPTION_OUTPUT);
				
				if (option_output!=null) {
					cmd.createArgument().setValue(option_output);
					File uncompressDir = new File(BmajExtract.getTargetDirectory(outputDir,nameFile));
					if(! uncompressDir.exists()) {
						uncompressDir.mkdirs();
					}
					cmd.createArgument().setValue(BmajExtract.getTargetDirectory(outputDir,nameFile));
				}
				
				
			} catch (BiomajException e) {
				extract.log("["+nameFile +"] format unknown !",Project.MSG_ERR);
				extract.log(e.getLocalizedMessage(),Project.MSG_ERR);
				throw new BiomajBuildException(extract.getProject(),"bmajExtract.error.file",inputDir+"/"+nameFile,new Exception());
			}
	
			Execute execute = new Execute(os,null);
	
			extract.log("extracting:"+inputDir+"/"+nameFile,Project.MSG_VERBOSE);
			
			execute.setCommandline(cmd.getCommandline());
	
			File fileArchive = new File(inputDir + "/" + nameFile);
	
			try {
			
				boolean logExtract = Boolean.valueOf(extract.getProject().getProperty(BiomajConst.logFilesProperty));
				
				File dirT = null;
				Vector<FileDesc> lsBefore = null;
				FileDesc fd = null;
				String hash = null;
		
				// Listing before extracting archive
				if (logExtract) {
					dirT = new File(outputDir);
					
					if (!isGz) {
						lsBefore = new Vector<FileDesc>();
						BiomajUtils.getListFilesFromDir(dirT.getAbsolutePath(),lsBefore);
					}
					fd = new FileDesc(fileArchive,false);
					hash = fd.getHash();
				}

				execute.execute();
				
		
				int code_retour  = execute.getExitValue();
		
				//not in gzip format code=1!
				if (code_retour != 0) {
		
					for (String line : execute.getCommandline())
						extract.log(line,Project.MSG_ERR);
					extract.log(Integer.valueOf(count)+":"+nameFile +" error",Project.MSG_ERR);
					for (int k=0;k<execute.getCommandline().length;k++)
						extract.log (execute.getCommandline()[k]+" ",Project.MSG_DEBUG);
					extract.log("file:"+inputDir+"/"+nameFile,Project.MSG_DEBUG);
					extract.log("code retour:"+code_retour,Project.MSG_DEBUG);
					if (!continueOnError)
						throw new BiomajBuildException(extract.getProject(),"bmajExtract.error.file",inputDir+"/"+nameFile,new Exception());
				} else {
					//	Ca s est bien passe, On efface l'archive
					if (fileArchive.exists()) {
						extract.log("delete :"+inputDir+"/"+nameFile,Project.MSG_VERBOSE);
						fileArchive.delete();
					}
		
					if (logExtract) {
						if (!isGz) {
							// Listing after extraction to get generated files
							Vector<FileDesc> lsAfter = new Vector<FileDesc>();
							BiomajUtils.getListFilesFromDir(dirT.getAbsolutePath(),lsAfter);
							extract.log(Integer.valueOf(count)+":"+nameFile +" ok",Project.MSG_VERBOSE);
							Vector<String> listNewFile = extract.getDiffListing(lsBefore,lsAfter);
							listNewFile.remove(new File(dirT.getAbsolutePath()+"/"+nameFile).getAbsolutePath());
			
							for (int i = 0; i < listNewFile.size(); i++) {
								extract.addExtractFile(listNewFile.get(i),hash);
							}
						} else { // Single file, no need to list, just remove the extension
							extract.addExtractFile(dirT.getAbsolutePath() + "/" + nameFile.substring(0, nameFile.lastIndexOf('.')), hash);
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
				error = e;
				break;
			} catch (BiomajException e) {
				e.printStackTrace();
				error = e;
				break;
			}
		}
		
	}
}
