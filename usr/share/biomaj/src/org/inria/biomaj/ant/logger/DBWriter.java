/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.logger;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import org.inria.biomaj.exe.bank.BankFactory;
import org.inria.biomaj.exe.bank.BiomajBank;
import org.inria.biomaj.session.bank.Bank;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.Configuration;
import org.inria.biomaj.session.bank.FileDesc;
import org.inria.biomaj.session.bank.GeneralWorkflowTask;
import org.inria.biomaj.session.bank.ProductionDirectory;
import org.inria.biomaj.session.bank.Session;
import org.inria.biomaj.session.process.BiomajProcess;
import org.inria.biomaj.session.process.MetaProcess;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.sql.SQLConnection;
import org.inria.biomaj.sql.SQLConnectionFactory;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * Static methods for database population.
 * 
 * @author ofilangi
 * @version Biomaj 0.9
 * @since Biomaj 0.8 /Citrina 0.5
 */
public class DBWriter {
	
	private static List<String> metaIdToDelete = new ArrayList<String>();
	private static SQLConnection connection = SQLConnectionFactory.getConnection();
	
	
	/**
	 * True if a new config is detected
	 * @param xmlBank
	 * @return
	 */
	public static synchronized boolean thereIsANewConfig(Bank xmlBank) throws BiomajException {

		if (!wellFormed(xmlBank))
			return true;
		
		boolean newConfig = createContentConfig(xmlBank.getConfig(), getBankId(xmlBank.getConfig().getName()));

		return newConfig;
	}

	/**
	 * 
	 * @param bank
	 * @param newSession
	 * @param deleteOldUpdateBank
	 * @return Id of created update record
	 * @throws BiomajException
	 */
	public synchronized static void updateStateSession(Bank bank, boolean newSession) throws BiomajException {
		
		if (!wellFormed(bank))
			return;
		
		String query = "";
		long confId = BiomajSQLQuerier.getLatestConfigurationWithSession((int) getBankId(bank.getConfig().getName()), false);
		Session session = bank.getCurrentSession();
		long updateId = -1;
		
		if (newSession) { // Create a new session element
			GeneralWorkflowTask isDeployed = null;
			int oldSessionListSize = bank.getListOldSession().size();
			if(oldSessionListSize>0) {
				/*
				 *  We have some previous update sessions
				 *  Did last session made a deployment ? If yes we can switch to a new update session
				 *  else we should keep the same update session id
				 */
				
				isDeployed = bank.getListOldSession().get(oldSessionListSize - 1).getWorkflowTask(Session.DEPLOYMENT);
				
			}
			if (oldSessionListSize == 0 || // If the session is ok (deployed) and is not a removeprocess
				(
				  (bank.getListOldSession().get(oldSessionListSize - 1).getStatus() && isDeployed!=null)
				&&
				  !(oldSessionListSize == 1 && bank.getListOldSession().get(0).getWorkflowTask(Session.REMOVEPROCESS) != null))
				) { // If the latest session is ok, create a new update element
				/*
				 * NEW UPDATE
				 */
				query = "INSERT INTO updateBank(ref_idconfiguration, productionDirectoryPath, productionDirectoryDeployed," +
						"sizeDownload, sizeRelease, startTime, endTime, isUpdated, nbSessions, updateRelease, idLastSession) " +
						"values(" + confId + ",'" + bank.getWorkflowInfoProductionDir() + "'," + bank.getWorkflowInfoIsDeployed() +
						",'" + BiomajUtils.sizeToString(bank.getWorkflowInfoSizeDownload()) + "','" + BiomajUtils.sizeToString(bank.getWorkflowInfoSizeRelease()) + "','" +
						BiomajUtils.dateToString(bank.getStart(), Locale.US) + "','" + BiomajUtils.dateToString(bank.getEnd(), Locale.US) +
						"'," + bank.isUpdate() + ",1";
				if (bank.getWorkflowInfoRelease() != null) {
					query += ",'" + bank.getWorkflowInfoRelease() + "'";
				} else
					query += ",null";
				// Idlastsession
				query += "," + bank.getCurrentSession().getId() + ");";
				Statement stat = connection.getStatement();
				updateId = connection.executeUpdateAndGetGeneratedKey(query, stat);
				SQLConnectionFactory.closeConnection(stat);
				
				
			} else {
				updateId = Long.valueOf(BiomajSQLQuerier.getLatestUpdate(bank.getConfig().getName(), false).get(BiomajSQLQuerier.UPDATE_ID));
				updateUpdateBank(bank, confId, updateId);
			}
			
			/*
			 * NEW SESSION
			 */
			query = "INSERT INTO session(idsession, ref_idupdateBank, parse, status, startTime, endTime, elapsedTime, logfile) " +
					"values(" + session.getId() + "," + updateId + ",'xml'," + session.getStatus() + ",'" + BiomajUtils.dateToString(session.getStart(), Locale.US) +
					"','" + BiomajUtils.dateToString(session.getEnd(), Locale.US) + "','";
			if (session.getEnd() == null) // elapsed time
				query += BiomajUtils.timeToString(new Date().getTime() - session.getStart().getTime()) +
					"','" + session.getLogfile() + "');";
			else
				query += (session.getEnd().getTime() - session.getStart().getTime()) +
				"','" + session.getLogfile() + "');";
			
			Statement stat = connection.getStatement();
			connection.executeUpdate(query, stat);
			SQLConnectionFactory.closeConnection(stat);
			
			
		} else { // Update an existing session
			updateId = Long.valueOf(BiomajSQLQuerier.getLatestUpdate(bank.getConfig().getName(), false).get(BiomajSQLQuerier.UPDATE_ID));
			updateUpdateBank(bank, confId, updateId);
			
			query = "UPDATE session SET " +
					"status=" + session.getStatus() + "," +
					"startTime='" + BiomajUtils.dateToString(session.getStart(), Locale.US) + "'," +
					"endTime='" + BiomajUtils.dateToString(session.getEnd(), Locale.US) + "',";
			if (session.getEnd() == null)
				query += "elapsedTime='" + BiomajUtils.timeToString(new Date().getTime() - session.getStart().getTime()) + "',";
			else
				query += "elapsedTime='" + BiomajUtils.timeToString(session.getEnd().getTime() - session.getStart().getTime()) + "',";
		
			query += "logfile='" + session.getLogfile() + "' WHERE idsession=" + session.getId();

			Statement stat = connection.getStatement();
			connection.executeUpdate(query, stat);
			SQLConnectionFactory.closeConnection(stat);
			
			
//			BiomajSQLQuerier.deleteSessionMessages(session.getId());
		}
		
		GeneralWorkflowTask p1 = session.getLastTask();
		GeneralWorkflowTask p2 = session.getActiveTask();
		
		if (p1 != null) {
			setElementTaskWorkflow(p1);
		} else if (p2 != p1 && p2 != null) {
			setElementTaskWorkflow(p2);
		}
		

		addMessages("warning",session.getNewWarn(),session.getId().toString(),"session");
		addMessages("error",session.getNewErr(), session.getId().toString(), "session");
		
	}
	
	/**
	 * Modify an updateBank record.
	 * 
	 * @param bank
	 * @param confId
	 * @param updateId
	 * @param nbSessions
	 */
	public static synchronized void updateUpdateBank(Bank bank, long confId, long updateId) {
		String query = "UPDATE updateBank set " +
				"ref_idconfiguration=" + confId + "," +
				"productionDirectoryPath='" + bank.getWorkflowInfoProductionDir() + "'," +
				"productionDirectoryDeployed=" + bank.getWorkflowInfoIsDeployed() + "," +
				"sizeDownload='" + BiomajUtils.sizeToString(bank.getWorkflowInfoSizeDownload()) + "'," +
				"sizeRelease='" + BiomajUtils.sizeToString(bank.getWorkflowInfoSizeRelease()) + "'," +
				"startTime='" + BiomajUtils.dateToString(bank.getStart(), Locale.US) + "'," +
				"endTime='" + BiomajUtils.dateToString(bank.getEnd(), Locale.US) + "'," +
				"isUpdated=" + bank.isUpdate() + "," +
				"nbSessions='" + (bank.getListOldSession().size() + 1) + "',";
		
		if (bank.getWorkflowInfoRelease() != null) {
			query += "updateRelease='" + bank.getWorkflowInfoRelease() + "',";
		} else
			query += "updateRelease=null,";
		
		// Idlastsession
		query += "idLastSession=" + bank.getCurrentSession().getId();
		query += " WHERE idupdateBank=" + updateId;
		
		Statement stat = connection.getStatement();
		connection.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);

	}
	
	public static synchronized void addFileToTask(FileDesc fd, long taskId, String fileType) {
		String query = "INSERT INTO file(location, size, time, link, is_extract, volatile, refHash, fileType) " +
				"values('" + fd.getLocation() + "','" + fd.getSize() + "','" + fd.getTime() + "'," + fd.isLink() +
				"," + fd.isExtract() + "," + fd.isVolatil() + ",";
		if (fd.getRefHash() == null)
			query += "null";
		else
			query += "'" + fd.getRefHash() + "'";
		query += ",'" + fileType + "');";
		
		Statement stat = connection.getStatement();
		long id = connection.executeUpdateAndGetGeneratedKey(query, stat);
		SQLConnectionFactory.closeConnection(stat);
		fd.setFileId(id);
		
		query = "INSERT INTO sessionTask_has_file(ref_idsessionTask,ref_idfile) " +
			"VALUES(" + taskId + "," + id + ");";
		
		stat = connection.getStatement();
		connection.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);
	}

	/**
	 * 
	 * @param bank
	 * @param confId
	 * @param updateId
	 * @return Update Id
	 * @throws BiomajException
	 */
	public static synchronized long createUpdateBank(Bank bank, long confId, long updateId) throws BiomajException {
		
		if (bank==null) {
			throw new BiomajException("unknown.error","bad definition of bank object.");
		}		
		
		if (bank.getStart() == null) {
			throw new BiomajException("unknown.error","Start date is not defined.");
		}
		
		//L'info du status est trouve par la derniere tache status=ok ou ko
		//updateBank.setAttribute("status", Boolean.toString(bank.getStatus()));
		int nbSessions;
		
		if (bank.getListOldSession() != null && bank.getListOldSession().size() > 0) {
			nbSessions = bank.getListOldSession().size();
			if (bank.getCurrentSession()!= null)
				nbSessions++;

			// If the last sessiontask was a remove process, retrieve the id of the corresponding
			// updatebank to modify it.
			
			String query = "SELECT idupdateBank FROM updateBank WHERE ref_idconfiguration=" + confId + " AND idlastSession IN (" +
		            "SELECT idsession FROM session WHERE startTime=(SELECT max(startTime) FROM session WHERE idsession IN" +
		            "(SELECT ref_idsession FROM session_has_sessionTask WHERE ref_idsessionTask IN (" +
		            "SELECT idsessionTask FROM sessionTask WHERE taskType='"+ BiomajConst.removeProcessTag + "'))) AND idsession IN (" +
		            "SELECT ref_idsession FROM session_has_sessionTask WHERE ref_idsessionTask IN (" +
		            "SELECT idsessionTask FROM sessionTask WHERE taskType='"+ BiomajConst.removeProcessTag + "')));";
			
			Statement stat = connection.getStatement();
			try {
				ResultSet rs = connection.executeQuery(query, stat);
				if (rs.next()) {
					updateId = rs.getInt(1);
				}
				SQLConnectionFactory.closeConnection(stat);
			} catch (SQLException e) {
				SQLConnectionFactory.closeConnection(stat);
				e.printStackTrace();
			}
			
			
			if (updateId < 0) {
				query = "INSERT INTO updateBank(ref_idconfiguration, productionDirectoryPath, productionDirectoryDeployed," +
						"sizeDownload, sizeRelease, startTime, endTime, isUpdated, nbSessions, updateRelease, idLastSession) " +
						"values(" + confId + ",'" + bank.getWorkflowInfoProductionDir() + "'," + bank.getWorkflowInfoIsDeployed() +
						",'" + BiomajUtils.sizeToString(bank.getWorkflowInfoSizeDownload()) + "','" + BiomajUtils.sizeToString(bank.getWorkflowInfoSizeRelease()) + "','" +
						BiomajUtils.dateToString(bank.getStart(), Locale.US) + "','" + BiomajUtils.dateToString(bank.getEnd(), Locale.US) +
						"'," + bank.isUpdate() + "," + nbSessions;
				if (bank.getWorkflowInfoRelease() != null) {
					query += ",'" + bank.getWorkflowInfoRelease() + "'";
				} else
					query += ",null";
				
				// Idlastsession
				if (bank.getListOldSession() != null) {
					if (bank.getCurrentSession()!= null)
						query += "," + bank.getCurrentSession().getId() + ");";
					else if (bank.getListOldSession().size() > 0)
						query += "," + Long.toString(bank.getListOldSession().get(bank.getListOldSession().size()-1).getId()) + ");";
					else
						query += ",null);";
				} else
					query += ",null);";
				
				Statement stat2 = connection.getStatement();
				updateId = connection.executeUpdateAndGetGeneratedKey(query, stat2);
				SQLConnectionFactory.closeConnection(stat2);
				
			} else {
				query = "UPDATE updateBank set " +
						"ref_idconfiguration=" + confId + "," +
						"productionDirectoryPath='" + bank.getWorkflowInfoProductionDir() + "'," +
						"productionDirectoryDeployed=" + bank.getWorkflowInfoIsDeployed() + "," +
						"sizeDownload='" + BiomajUtils.sizeToString(bank.getWorkflowInfoSizeDownload()) + "'," +
						"sizeRelease='" + BiomajUtils.sizeToString(bank.getWorkflowInfoSizeRelease()) + "'," +
						"startTime='" + BiomajUtils.dateToString(bank.getStart(), Locale.US) + "'," +
						"endTime='" + BiomajUtils.dateToString(bank.getEnd(), Locale.US) + "'," +
						"isUpdated=" + bank.isUpdate() + "," +
						"nbSessions='" + nbSessions + "',";
				
				if (bank.getWorkflowInfoRelease() != null) {
					query += "updateRelease='" + bank.getWorkflowInfoRelease() + "',";
				} else
					query += "updateRelease=null,";
				
				// Idlastsession
				if (bank.getListOldSession() != null) {
					if (bank.getCurrentSession()!= null)
						query += "idLastSession=" + bank.getCurrentSession().getId();
					else if (bank.getListOldSession().size() > 0)
						query += "idLastSession=" + Long.toString(bank.getListOldSession().get(bank.getListOldSession().size() - 1).getId());
					else
						query += "idLastSession=null";
				} else
					query += "idLastSession=null";
				
				query += " WHERE idupdateBank=" + updateId;
				
				Statement stat2 = connection.getStatement();
				connection.executeUpdate(query, stat2);
				SQLConnectionFactory.closeConnection(stat2);
				
			}
			
			for (Session s : bank.getListOldSession()) {
				setSessionAttributes(s, updateId);
			}
			
		} else {
			nbSessions = 1;
			
			String query = "";
			
			if (updateId < 0) {
				query = "INSERT INTO updateBank(ref_idconfiguration, updateRelease, productionDirectoryPath, productionDirectoryDeployed," +
						"sizeDownload, sizeRelease, startTime, endTime, isUpdated, nbSessions, idLastSession) " +
						"values(" + confId + ",'" + bank.getWorkflowInfoRelease() + "','" + bank.getWorkflowInfoProductionDir() + "'," + bank.getWorkflowInfoIsDeployed() +
						",'" + BiomajUtils.sizeToString(bank.getWorkflowInfoSizeDownload()) + "','" + BiomajUtils.sizeToString(bank.getWorkflowInfoSizeRelease()) + "','" +
						BiomajUtils.dateToString(bank.getStart(), Locale.US) + "','" + BiomajUtils.dateToString(bank.getEnd(), Locale.US) +
						"'," + bank.isUpdate() + "," + nbSessions;
				// Idlastsession
				if (bank.getListOldSession() != null) {
					if (bank.getCurrentSession()!= null) 
						query += "," + bank.getCurrentSession().getId() + ");";
					else if (bank.getListOldSession().size()>0)
						query += "," + Long.toString(bank.getListOldSession().get(bank.getListOldSession().size() - 1).getId()) + ");";
				} else
					query += ",null);";
				
				Statement stat = connection.getStatement();
				updateId = connection.executeUpdateAndGetGeneratedKey(query, stat);
				
				SQLConnectionFactory.closeConnection(stat);
			} else {
				query = "UPDATE updateBank set " +
						"ref_idconfiguration=" + confId + "," +
						"productionDirectoryPath='" + bank.getWorkflowInfoProductionDir() + "'," +
						"productionDirectoryDeployed=" + bank.getWorkflowInfoIsDeployed() + "," +
						"sizeDownload='" + BiomajUtils.sizeToString(bank.getWorkflowInfoSizeDownload()) + "'," +
						"sizeRelease='" + BiomajUtils.sizeToString(bank.getWorkflowInfoSizeRelease()) + "'," +
						"startTime='" + BiomajUtils.dateToString(bank.getStart(), Locale.US) + "'," +
						"endTime='" + BiomajUtils.dateToString(bank.getEnd(), Locale.US) + "'," +
						"isUpdated=" + bank.isUpdate() + "," +
						"nbSessions='" + nbSessions + "',";
		
				if (bank.getWorkflowInfoRelease() !=null) {
					query += "updateRelease='" + bank.getWorkflowInfoRelease() + "',";
				} else
					query += "updateRelease=null,";
				
				// Idlastsession
				if (bank.getListOldSession() != null) {
					if (bank.getCurrentSession()!= null)
						query += "idLastSession=" + bank.getCurrentSession().getId();
					else if (bank.getListOldSession().size() > 0)
						query += "idLastSession=" + Long.toString(bank.getListOldSession().get(bank.getListOldSession().size() - 1).getId());
					else
						query += "idLastSession=null";
				} else
					query += "idLastSession=null";
				
				query += " WHERE idupdateBank=" + updateId;
				
				Statement stat = connection.getStatement();
				connection.executeUpdate(query, stat);
				SQLConnectionFactory.closeConnection(stat);
			}
			
		}
		
		if (bank.getCurrentSession() != null) {
			setSessionAttributes(bank.getCurrentSession(), updateId);
		}
		
		return updateId;
		
	}

	public synchronized static void updateStateSessionWithProductionDir(Bank bank) throws BiomajException {
		if (bank==null)
			return ;

		if (bank.getConfig() == null)
			throw new NullPointerException("Config bank can't be null");

		createContentProductionDirectories(bank.getBankStateListProductionDirectories(), getBankId(bank.getConfig().getName()));
	}
	
	public static synchronized long getBankId(String bankName) {

		
		long bankId = BiomajSQLQuerier.getBankId(bankName);
		if (bankId < 0) {
			int userId = getBankOwnerId(bankName);
			
			String visibility = "false";
			BankFactory bf = new BankFactory();
			try {
				BiomajBank bb = bf.createBank(bankName, false);
				String tmp = bb.getPropertiesFromBankFile().getProperty(BiomajConst.bankVisibility);
				if (tmp != null) {
					visibility = tmp.equals("public") ? "true" : "false";
				}
			} catch (BiomajException e) {
				e.printStackTrace();
			}
			
			String query = "INSERT INTO bank(name, ref_iduser, visibility) values('" + bankName + "'," + userId + "," + visibility + ");";
			Statement stat = connection.getStatement();
			bankId = connection.executeUpdateAndGetGeneratedKey(query, stat);
			SQLConnectionFactory.closeConnection(stat);
		}
		return bankId;
	}

	public synchronized static void setProductionDirRemoved(String bankName,ProductionDirectory pd) throws BiomajException {

		if (bankName==null)
			return ;

		long id = getBankId(bankName);
		String query = "SELECT idproductionDirectory FROM productionDirectory WHERE ref_idbank=" + id + " AND path='" + pd.getPath() + "' AND " +
				"state='" + ProductionDirectory.AVAILABLE_STR + "'";
		
		Statement stat = connection.getStatement();
		ResultSet rs = connection.executeQuery(query, stat);
		int pdId = -1;
		try {
			if (rs.next()) {
				pdId = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			SQLConnectionFactory.closeConnection(stat);
			return;
		}
		
		query = "UPDATE productionDirectory SET remove='" + BiomajUtils.dateToString(pd.getRemoveDate(), Locale.US) + "'," +
				"state='" + ProductionDirectory.REMOVE_STR + "' WHERE idproductionDirectory=" + pdId;

		connection.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);
		
		BiomajSQLQuerier.deleteRemovedDirectoryRelatedRecords(pdId);
	}
	
	public static synchronized void deleteBank(String bankName) {
		BiomajSQLQuerier.deleteBank(bankName);
	}


	private synchronized static void setSessionAttributes(Session session, long updateId) {
		
		String query = "SELECT * FROM session WHERE idsession = " + session.getId();
		Statement stat = connection.getStatement();
		
		try {
			ResultSet rs = connection.executeQuery(query, stat);
			if (!rs.next()) { // We create a new session
				SQLConnectionFactory.closeConnection(stat);
				
				query = "INSERT INTO session(idsession, ref_idupdateBank, parse, status, startTime, endTime, elapsedTime, logfile) " +
						"values(" + session.getId() + "," + updateId + ",'xml'," + session.getStatus() + ",'" + BiomajUtils.dateToString(session.getStart(), Locale.US) +
						"','" + BiomajUtils.dateToString(session.getEnd(), Locale.US) + "','";
				if (session.getEnd() == null) // elapsed time
					query += BiomajUtils.timeToString(new Date().getTime() - session.getStart().getTime()) +
						"','" + session.getLogfile() + "');";
				else
					query += (session.getEnd().getTime() - session.getStart().getTime()) +
					"','" + session.getLogfile() + "');";
			
			} else { // We update an existing session
				SQLConnectionFactory.closeConnection(stat);
				
				query = "UPDATE session SET " +
						"status=" + session.getStatus() + "," +
						"startTime='" + BiomajUtils.dateToString(session.getStart(), Locale.US) + "'," +
						"endTime='" + BiomajUtils.dateToString(session.getEnd(), Locale.US) + "',";
				if (session.getEnd() == null)
					query += "elapsedTime='" + BiomajUtils.timeToString(new Date().getTime() - session.getStart().getTime()) + "',";
				else
					query += "elapsedTime='" + BiomajUtils.timeToString(session.getEnd().getTime() - session.getStart().getTime()) + "',";

				query += "logfile='" + session.getLogfile() + "' WHERE idsession=" + session.getId();
				
				// Deletion of the old messages
//				BiomajSQLQuerier.deleteSessionMessages(session.getId());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Statement stat2 = connection.getStatement();
		connection.executeUpdate(query, stat2);
		SQLConnectionFactory.closeConnection(stat2);


		/*
		for (int i = Session.PREPROCESS;i<=Session.REMOVEPROCESS; i++) {
			if (session.taskAreSaveInWorkflow(i)) {
				GeneralWorkflowTask p = session.getWorkflowTask(i);
				if (p!=null) {
					setElementTaskWorkflow(p);
				}
			}
		}*/
		
		GeneralWorkflowTask p = session.getLastTask();
		if (p != null)
			setElementTaskWorkflow(p);
		

		addMessages("warning",session.getNewWarn(),session.getId().toString(),"session");
		addMessages("error",session.getNewErr(), session.getId().toString(), "session");

	}
	
	public static synchronized void deleteMetaprocesses() {
		for (String s : metaIdToDelete) {
			String query = "DELETE FROM metaprocess WHERE idmetaprocess='" + s + "'";
			Statement stat = connection.getStatement();
			connection.executeUpdate(query, stat);
			SQLConnectionFactory.closeConnection(stat);
		}
	}

	private static synchronized void setElementTaskWorkflow(GeneralWorkflowTask p) {
		if (p == null)
			return;

		String query = "SELECT idsessionTask from sessionTask WHERE taskType='" + p.getProcessName() + "' AND " +
				"idsessionTask IN (SELECT ref_idsessionTask FROM session_has_sessionTask WHERE " +
				"ref_idsession=" + p.getSession().getId() + ");";
		Statement stat = connection.getStatement();
		
		long taskId = -1;
		try {
			ResultSet rs = connection.executeQuery(query, stat);
			// S'il y a deja une tache
			if (rs.next()) {
				taskId = rs.getLong(1);
				
				query = "UPDATE sessionTask SET " +
						"startTime='" + BiomajUtils.dateToString(p.getStart(), Locale.US) + "'," +
						"endTime='" + BiomajUtils.dateToString(p.getEnd(), Locale.US) + "'," +
						"elapsedTime='" + BiomajUtils.timeToString(p.getElapsedTime()) + "'," +
						"status='" + p.getStatusStr() + "' " +
						"WHERE idsessionTask=" + taskId;
				
				Statement stat2 = connection.getStatement();
				connection.executeUpdate(query, stat2);
				SQLConnectionFactory.closeConnection(stat2);
				
//				BiomajSQLQuerier.deleteTaskMessages(taskId);
//				BiomajSQLQuerier.deleteTaskFiles(taskId);
				
			} else {
				query = "INSERT INTO sessionTask(startTime, endTime, elapsedTime, status) " +
						"VALUES ('" + BiomajUtils.dateToString(p.getStart(), Locale.US) + "','" + BiomajUtils.dateToString(p.getEnd(), Locale.US) +
						"','" + BiomajUtils.timeToString(p.getElapsedTime()) + "','" + p.getStatusStr() + "');";
				
				Statement stat2 = connection.getStatement();
				taskId = connection.executeUpdateAndGetGeneratedKey(query, stat2);
				SQLConnectionFactory.closeConnection(stat2);
				
				query = "INSERT INTO session_has_sessionTask(ref_idsession, ref_idsessionTask) values(" + p.getSession().getId() + "," + taskId + ");";

				Statement stat3 = connection.getStatement();
				connection.executeUpdate(query, stat3);
				SQLConnectionFactory.closeConnection(stat3);
			}
			SQLConnectionFactory.closeConnection(stat);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		p.setTaskId((int) taskId);
		
		p.fillElement(taskId);

		addMessages("warning",p.getNewWarn(), String.valueOf(taskId), "task");
		addMessages("error",p.getNewErr(), String.valueOf(taskId), "task");

//		return d;
	}

	private static void addMessages(String tag,Vector<String> messages, String ownerId, String ownerType){
		for (String mess : messages) {

			String queryInsert = "INSERT INTO message(message, type) values('" + mess.replaceAll("'", "''") + "', '" + tag + "');";
			Statement stat = connection.getStatement();
			long messageId = connection.executeUpdateAndGetGeneratedKey(queryInsert, stat);
			SQLConnectionFactory.closeConnection(stat);
			
			String finalQuery = "";
			
			if (ownerType.equals("task"))
				finalQuery = "INSERT INTO sessionTask_has_message(ref_idsessionTask, ref_idmessage) " +
						"VALUES(" + ownerId + "," + messageId + ");";
			else if (ownerType.equals("metaprocess"))
				finalQuery = "INSERT INTO metaprocess_has_message(ref_idmetaprocess, ref_idmessage) " +
						"VALUES('" + ownerId + "'," + messageId + ");";
			else if (ownerType.equals("process"))
				finalQuery = "INSERT INTO process_has_message(ref_idprocess, ref_idmessage) " +
						"VALUES(" + ownerId + "," + messageId + ");";
			else if (ownerType.equals("session"))
				finalQuery = "INSERT INTO session_has_message(ref_idsession, ref_idmessage) " +
						"VALUES(" + ownerId + "," + messageId + ");";
			
			stat = connection.getStatement();
			connection.executeUpdate(finalQuery, stat);
			SQLConnectionFactory.closeConnection(stat);
		}

	}


	/*================================== PROCESS ==================================*/


	private static void addDepsFile(Vector<FileDesc> dependsFiles, long idProcess) {

		for (FileDesc nFile : dependsFiles) {
			
			String query = "";
//			String query = "SELECT idfile FROM file WHERE location='" + nFile.getLocation() + "' AND " +
//					"ref_idprocess=" + idProcess;
//			connection.executeQuery(query, true);
			if (nFile.getFileId() >= 0) {
				// A file is not likely to be modified once it has been created.
				
				/*
				query = "UPDATE file SET " +
						"size='" + nFile.getSize() + "'," +
						"time='" + nFile.getTime() + "'," +
						"is_extract=" + nFile.isExtract() + "," +
						"volatile=" + nFile.isVolatil() + ",";
				if (nFile.getRefHash() == null)
					query += "refHash=null ";
				else
					query += "refHash='" + nFile.getRefHash() + "' ";
				query += "WHERE idfile=" + nFile.getFileId();
				
				Statement stat = connection.getStatement();
				connection.executeUpdate(query, stat);
				stat.close();
				Connection cnt = stat.getConnection();
					stat.close();
					cnt.close();
				connection.closeStatement();*/
			} else {
				query = "INSERT INTO file(location, size, time, link, is_extract, volatile, refHash, ref_idprocess)" +
						"values('" + nFile.getLocation() + "','" + nFile.getSize() + "','" + nFile.getTime() +
						"'," + nFile.isLink() + "," + nFile.isExtract() + "," + nFile.isVolatil() +
						",";
				if (nFile.getRefHash() == null)
					query += "null,";
				else
					query += "'" + nFile.getRefHash() + "',";
				query += idProcess + ");";
				Statement stat = connection.getStatement();
				nFile.setFileId(connection.executeUpdateAndGetGeneratedKey(query, stat));
				SQLConnectionFactory.closeConnection(stat);
			}
		}
	}

	private static void createProcess(BiomajProcess process, String metaId) {
		long processId = process.getProcessId();
		
		String query = "";
//		String query = "SELECT idprocess FROM process WHERE " +
//				"name='" + process.getNameProcess() + "' AND " +
//				"keyname='" + process.getKeyName() + "' AND " +
//				"exe='" + process.getExe() + "' AND " +
//				"args='" + process.getArgs().replaceAll("'", "''") + "' AND " +
//				"ref_idmetaprocess='" + metaId + "';";
//		connection.executeQuery(query, true);
		
		if (processId >= 0) {
			
			query = "UPDATE process SET " +
					"name='" + process.getNameProcess() + "'," +
					"keyname='" + process.getKeyName() + "'," +
					"exe='" + process.getExe() + "'," +
					"args='" + process.getArgs().replaceAll("'", "''") + "'," +
					"description='" + process.getDescription().replaceAll("'", "''") + "'," +
					"type='" + process.getType() + "'," +
					"startTime='" + BiomajUtils.dateToString(process.getStart(), Locale.US) + "',";

			if (process.getEnd() != null)
				query += "endTime='" + BiomajUtils.dateToString(process.getEnd(), Locale.US) + "'," +
						"elapsedTime='" + BiomajUtils.timeToString(process.getEnd().getTime() - process.getStart().getTime()) + "'," +
						"value='" + process.getReturnValue() + "',";
			else
				query += "endTime='" + BiomajUtils.dateToString(new Date(), Locale.US) + "'," +
						"elapsedTime='" + BiomajUtils.timeToString(new Date().getTime() - process.getStart().getTime()) + "'," +
						"value='-1',";
			
			 
			 query += "biomaj_error=" + process.isErrorDetected() + "," +
					"timestamp='" + process.getTimeStampExe() + "'," +
					"ref_idmetaprocess='" + metaId + "' " +
					"WHERE idprocess=" + processId;
			 
			Statement stat = connection.getStatement();
			connection.executeUpdate(query, stat);
			SQLConnectionFactory.closeConnection(stat);
			 
			 // Deletion of the old process related messages if there are new messages
			 if (process.getWarn().size() > 0 || process.getErr().size() > 0) {
				 BiomajSQLQuerier.deleteProcessMessages(processId);
			 }
			
		} else {
			
			query = "INSERT INTO process(name, keyname, exe, args, description, type, startTime, endTime, elapsedTime, value, biomaj_error," +
					"timestamp, ref_idmetaprocess) values('" + process.getNameProcess() + "','" + process.getKeyName() + "','" + process.getExe() + "','" +
					process.getArgs().replaceAll("'", "''") + "','" + process.getDescription().replaceAll("'", "''") + "','" + process.getType() + "','" +
					BiomajUtils.dateToString(process.getStart(), Locale.US) + "','";
			
			if (process.getEnd() != null)
				query += BiomajUtils.dateToString(process.getEnd(), Locale.US) + "','" + BiomajUtils.timeToString(process.getEnd().getTime() - process.getStart().getTime()) +
					"','" + process.getReturnValue();
			else
				query += BiomajUtils.dateToString(new Date(), Locale.US) + "','" + BiomajUtils.timeToString(new Date().getTime() - process.getStart().getTime()) +
					"','-1";
			
			query += "'," + process.isErrorDetected() + ",'" + process.getTimeStampExe() + "','" + metaId + "');";
			
			Statement stat = connection.getStatement();
			processId = connection.executeUpdateAndGetGeneratedKey(query, stat);
			SQLConnectionFactory.closeConnection(stat);
			
			
			process.setProcessId(processId);
		}
		
		addDepsFile(process.getDependancesOutput(), processId);
		addMessages("warning",process.getWarn(), String.valueOf(processId), "process");
		addMessages("error",process.getErr(), String.valueOf(processId), "process");
		
	}

	public static synchronized void createMetaprocess(String bankName, Session session, MetaProcess metaProcess) throws BiomajException {
		
		String metaId = session.getId() + "." + metaProcess.getName();
		
		/* */
		
		String query = "SELECT * FROM metaprocess WHERE idmetaprocess='" + metaId + "';";
		Statement stat = connection.getStatement();
		
		try {
			ResultSet rs = connection.executeQuery(query, stat);
			if (!rs.next()) {
				query = "INSERT INTO metaprocess(idmetaprocess,ref_idsessionTask,name,startTime,endTime,elapsedTime,status,logfile,block) " +
						"values ('" + metaId + "'," + session.getLastTask().getTaskId() + ",'" + metaProcess.getName() + "','" + BiomajUtils.dateToString(metaProcess.getStart(), Locale.US) +
						"','" + BiomajUtils.dateToString(metaProcess.getEnd(), Locale.US) + "','" + BiomajUtils.timeToString(metaProcess.getEnd().getTime() - metaProcess.getStart().getTime()) +
						"','" + metaProcess.getStatusStr() + "','" + metaProcess.getLogFile() + "','" + metaProcess.getBlock() + "');";
				
				Statement stat2 = connection.getStatement();
				
				connection.executeUpdate(query, stat2);
				SQLConnectionFactory.closeConnection(stat2);
				addMetaId(metaId);
			} else {
				query = "UPDATE metaprocess set name='" + metaProcess.getName() + "', startTime='" + BiomajUtils.dateToString(metaProcess.getStart(), Locale.US) +
		 		"',endTime='" + BiomajUtils.dateToString(metaProcess.getEnd(), Locale.US) + "', elapsedTime='" +
		 		BiomajUtils.timeToString(metaProcess.getEnd().getTime() - metaProcess.getStart().getTime()) + "',status='" + metaProcess.getStatusStr() + "',logfile='" +
		 		metaProcess.getLogFile() + "',block='" + metaProcess.getBlock() + "' WHERE idmetaprocess='"  + metaId + "';";
		
//				Statement stat = connection.getStatement();
				if (connection.executeUpdate(query, stat) >= 0) {
					metaIdToDelete.remove(metaId);
				}
			}
		} catch (SQLException e) {
			BiomajLogger.getInstance().log(e);
		}
		SQLConnectionFactory.closeConnection(stat);
		
		/* */
		
		for (BiomajProcess p : metaProcess.getListProcess())
			createProcess(p, metaId);
		
		addMessages("warning",metaProcess.getWarn(), metaId, "metaprocess");
		addMessages("error",metaProcess.getErr(), metaId, "metaprocess");

	}

	/**
	 * 
	 * @param config
	 * @param idBank
	 */
	public static synchronized boolean createContentConfig(Configuration config, long idBank) {
		/*
		 * A configuration consists in remoteinfo + localinfo.
		 * Before adding a new config we check that we dont already
		 * have a pair remote+local with same info.
		 * If either localinfo or remoteinfo is different, we create
		 * a new record for both. Thus, each pair should be referenced
		 * by only one config record (i.e. local or remote cant be shared
		 * by several configs). This was done to simplify modification
		 * and deletion issues.
		 * 
		 */
		
		boolean newConfig = false;
		
		String queryRemote = "SELECT max(idremoteInfo) FROM remoteInfo WHERE " +
				"protocol='" + config.getProtocol() + "' AND ";
		if (config.getPort().isEmpty())
			queryRemote += "port=NULL AND ";
		else
			queryRemote += "port=" + config.getPort() + " AND ";
		
		queryRemote += "dbName='" + config.getName().replaceAll("'", "''") + "' AND " +
				"dbFullname='" + config.getFullName().replaceAll("'", "''") + "' AND " +
				"dbType='" + config.getTypeBank() + "' AND " +
				"server='" + config.getUrl() + "' AND " +
				"remoteDir='" + config.getRemoteDirectory() + "'";
		
		String queryLocal = "SELECT max(idlocalInfo) FROM localInfo WHERE " +
				"offlineDirectory='" + config.getOfflineDirectory() + "' AND " +
				"versionDirectory='" + config.getVersionDirectory() + "' AND " +
				"frequency=" + config.getFrequencyUpdate() + " AND " +
				"dolinkcopy=" + config.getDoLinkCopy() + " AND " +
				"logfile=" + config.getLogFiles() + " AND " +
				"releaseFile='" + config.getReleaseFile() + "' AND " +
				"releaseRegexp='" + config.getReleaseRegExp() + "' AND " +
				"remoteFiles='" + config.getRemoteFilesRegexp() + "' AND " +
				"localFiles='" + config.getLocalFilesRegexp() + "' AND " +
				"nversions=" + config.getNbVersionManagement();
		
		String queryConfig = "SELECT idconfiguration FROM configuration WHERE ref_idremoteInfo =(" + queryRemote + ") AND " +
				"ref_idlocalInfo =(" + queryLocal + ");";
		
		Statement stat = connection.getStatement();
		try {
			ResultSet rs = connection.executeQuery(queryConfig, stat);
			if (rs.next()) {
				newConfig = false;
				SQLConnectionFactory.closeConnection(stat);
			}
			else {
				SQLConnectionFactory.closeConnection(stat);
				queryRemote = "INSERT INTO remoteInfo(protocol, port, dbName, dbFullname, dbType, server, remoteDir) " +
						"values('" + config.getProtocol() + "'," + config.getPort() + ",'" + config.getName().replaceAll("'", "''") + "','" +
						config.getFullName().replaceAll("'", "''") + "','" + config.getTypeBank() + "','" + config.getUrl() + "','" +
						config.getRemoteDirectory() + "');";
				Statement stat2 = connection.getStatement();
				long remoteId = connection.executeUpdateAndGetGeneratedKey(queryRemote, stat2);
				SQLConnectionFactory.closeConnection(stat2);
				
				queryLocal = "INSERT INTO localInfo(offlineDirectory, versionDirectory, frequency, dolinkcopy, logfile," +
						"releaseFile, releaseRegexp, remoteFiles, remoteExcludedFiles, localFiles, nversions) " +
						"values('" + config.getOfflineDirectory() + "','" + config.getVersionDirectory() + "','" +
						config.getFrequencyUpdate()  + "'," + config.getDoLinkCopy() + "," + config.getLogFiles() + ",'" +
						config.getReleaseFile() + "','" + config.getReleaseRegExp() + "','" + config.getRemoteFilesRegexp() + "','" +
						config.getRemoteExcludedFiles() + "','" + config.getLocalFilesRegexp() + "','" + config.getNbVersionManagement() + "');";
				stat2 = connection.getStatement();
				long localId = connection.executeUpdateAndGetGeneratedKey(queryLocal, stat2);
				SQLConnectionFactory.closeConnection(stat2);
				
				String query = "INSERT INTO configuration(idconfiguration, date, file, ref_idbank, ref_idremoteInfo, ref_idlocalInfo)" +
						"values(" + config.getId() + ",'" + BiomajUtils.toUSDate(config.getDate()) + "','" + config.getPropertyFile() +
						"'," + idBank +	"," + remoteId + "," + localId + ");";
				
				stat = connection.getStatement();
				connection.executeUpdate(query, stat);
				SQLConnectionFactory.closeConnection(stat);

				newConfig = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return newConfig;
	}

	private static void createContentProductionDirectories(Vector<ProductionDirectory> listDir, long idBank) {
		for (ProductionDirectory pd : listDir) {
			
			String query = "SELECT idproductionDirectory,state FROM productionDirectory WHERE path='" + pd.getPath() + "'" +
					" AND session=" + pd.getSession() + ";";
			
			Statement stat = connection.getStatement();
			try {
				ResultSet rs = connection.executeQuery(query, stat);
				if (rs.next()) {
					int id = rs.getInt(1);
					String oldState = rs.getString(2);
					SQLConnectionFactory.closeConnection(stat);
					
					
					
					query = "UPDATE productionDirectory SET " +
							"remove=" + (pd.getRemoveDate() == null ? "null," : "'" + BiomajUtils.dateToString(pd.getRemoveDate(), Locale.US) + "',") +
							"creation='" + BiomajUtils.dateToString(pd.getCreationDate(), Locale.US) + "',";
					
					if (pd.getStateStr().equals(ProductionDirectory.AVAILABLE_STR)) {
						query += "size='" + BiomajUtils.sizeToString(pd.getSize()) + "',";
					}
					query += "state='" + pd.getStateStr() + "'," +
							"session='" + pd.getSession() + "'," +
							"path='" + pd.getPath() + "'," +
							"ref_idbank=" + idBank + " WHERE idproductionDirectory=" + id;
					
					stat = connection.getStatement();
					connection.executeUpdate(query, stat);
					SQLConnectionFactory.closeConnection(stat);
					
					if (oldState.equals(ProductionDirectory.AVAILABLE_STR) &&
							pd.getStateStr().equals(ProductionDirectory.REMOVE_STR)) { // Directory just deleted
						BiomajSQLQuerier.deleteRemovedDirectoryRelatedRecords(id);
					}
					
				} else {
					SQLConnectionFactory.closeConnection(stat);
					query = "INSERT INTO productionDirectory(remove, creation, size, state, session, path, ref_idbank)" +
							"values (" + (pd.getRemoveDate() == null ? "null" : "'" + BiomajUtils.dateToString(pd.getRemoveDate(), Locale.US) + "'") +
							",'" + BiomajUtils.dateToString(pd.getCreationDate(), Locale.US) + "','" + BiomajUtils.sizeToString(pd.getSize()) + "','" +
							pd.getStateStr() + "','" + pd.getSession() + "','" + pd.getPath() + "'," + idBank + ");";
		
					stat = connection.getStatement();
					connection.executeUpdate(query, stat);
					SQLConnectionFactory.closeConnection(stat);
				}
			} catch (SQLException e) {
				SQLConnectionFactory.closeConnection(stat);
				e.printStackTrace();
			}
		}
	}
	
	public static int getBankOwnerId(String bankName) {
		File file = BankFactory.getBankPath(bankName);
		int id = SQLConnectionFactory.getDefaultAdminId();
		
		if (file == null)
			return id;
		
		String path = file.getAbsolutePath();
		String np = path.substring(0, path.lastIndexOf('/'));
		String user = np.substring(np.lastIndexOf('/') + 1);
		
		if (!user.equals("db_properties")) {
			String query = "SELECT iduser FROM bw_user WHERE login='" + user + "'";
			Statement stat = connection.getStatement();
			try {
				ResultSet rs = connection.executeQuery(query, stat);
				if (rs.next()) {
					id = rs.getInt(1);
				}
			} catch (SQLException ex) {
				SQLConnectionFactory.closeConnection(stat);
				ex.printStackTrace();
			}
			SQLConnectionFactory.closeConnection(stat);
		}
		
		return id;
		
	}
	
	/**
	 * Id of metaprocess that may need to be deleted
	 * in case of rebuild with no rerun of metaprocess.
	 *  
	 * @param id
	 */
	public static synchronized void addMetaId(String id) {
		metaIdToDelete.add(id);
	}

	private static boolean wellFormed(Bank xmlBank) {
		if (xmlBank==null)
			return false;

		if (xmlBank.getConfig() == null)
			throw new NullPointerException("Config bank can't be null");

		return true;
	}
}
