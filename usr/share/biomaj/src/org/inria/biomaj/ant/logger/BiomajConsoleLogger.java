/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.logger;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.PrintStream;
import java.util.Random;
import java.util.concurrent.Semaphore;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import org.apache.tools.ant.BuildEvent;
import org.apache.tools.ant.BuildLogger;
import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.task.BmajExecute;
import org.inria.biomaj.exe.main.Biomaj;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajUtils;
import org.inria.biomaj.exe.workflow.*;

/**
 * Implementation of BuildLogger and WindowsListener to print information about current session
 * in a BioMAJ console.
 * 
 * @author ofilangi
 * @version Biomaj 0.9
 * @since Biomaj 0.8 /Citrina 0.5
 */
public class BiomajConsoleLogger implements BuildLogger , WindowListener {

	public static final int PRE       = 0;
	public static final int SYNC      = 1;
	public static final int POST      = 2;
	public static final int DEP       = 3;
	public static final int SYNC_DEP  = 4;
	public static final int REMOVE    = 5;
	
	public static final Color OK_COLOR = new Color(153,204,51);
	public static final Color ERROR_COLOR = Color.RED;
	
	public static final String NOT_KEEP_LINE_ON_CONSOLE="__BiomajConsoleLogger__NOT_KEEP_LINE::"; 
	
	public static final int SIZE_W=800;
	public static final int SIZE_H=600;
	
	/**
	 * @uml.property  name="fenetre"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private static JFrame fenetre = null ;
	/**
	 * @uml.property  name="text"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private JTextPane text;
	
	private static JTabbedPane jtp = null ;
	
	private static Semaphore mySema = new Semaphore(1);
	
	/**
	 * @uml.property  name="sdoc"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private StyledDocument sdoc;
	/**
	 * @uml.property  name="level"
	 */
	private int level = Project.MSG_INFO;
	/**
	 * @uml.property  name="we"
	 * @uml.associationEnd  multiplicity="(0 -1)" elementType="java.lang.Object" qualifier="key:java.lang.String java.lang.Object"
	 */
	private Thread we;
	/**
	 * @uml.property  name="lastTask"
	 * @uml.associationEnd  multiplicity="(0 -1)" elementType="java.lang.Object" qualifier="key:java.lang.String java.lang.Object"
	 */
	private String lastTask="";	
	private static int instance = 0;
	/**
	 * @uml.property  name="scroll"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private JScrollPane scroll  = null;
	/**
	 * @uml.property  name="position"
	 */
	private int position =0;
	/**
	 * @uml.property  name="currentTaskName"
	 * @uml.associationEnd  multiplicity="(0 -1)" elementType="java.lang.Object" qualifier="key:java.lang.String java.lang.Object"
	 */
	private String currentTaskName="";
	/**
	 * Constructor
	 * @param titleConsole title of console
	 * @param we           the workflow engine which is executed in the console
	 */
	
	// Position of tab related to this logger
	private int tabIndex = 0;
	 
	public BiomajConsoleLogger(String titleConsole,Thread we,int type) {

		if (we == null) {
			BiomajLogger.getInstance().log("Error: Can't define a BiomajConsoleLogger with workflow engine null!");
			System.exit(-1);
		}
		
		// Threads messing up and causing exceptions when several tab are
		// opened at the same time.
		Random rand = new Random();
		int randomWait = rand.nextInt(500);
		try {
			Thread.sleep(randomWait);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		createWindows(titleConsole,we); 
		
		addTextPane(titleConsole,we,type); 
	}

	
	public synchronized void createWindows(String titleConsole,Thread we) {
		
		try {
			mySema.acquire();

			if ((fenetre != null)||(jtp != null))
				{
				mySema.release();
				return;
				}

			this.we = we;
			fenetre = new JFrame("BioMaj-"+Biomaj.VERSION);
			fenetre.setSize(400,400);
			fenetre.setVisible(true);
			
			ImageIcon icon = new ImageIcon(BiomajUtils.getBiomajRootDirectory()+"/xslt/images/icon_cycle_biomaj.png","BioMAJ - cycle update");
			fenetre.setIconImage(icon.getImage());
			
			Dimension screenSize=Toolkit.getDefaultToolkit().getScreenSize();
			Dimension ScreenSize=new Dimension((screenSize.width/2),(screenSize.height/2));
			Dimension frameSize=new Dimension(SIZE_W,SIZE_H);
			int x=(ScreenSize.width/2);
			int y=(ScreenSize.height/2);
			fenetre.setBounds(x+instance,y+instance,frameSize.width,frameSize.height);

			jtp = new JTabbedPane(SwingConstants.TOP,JTabbedPane.WRAP_TAB_LAYOUT);

			fenetre.getContentPane().setLayout(new BorderLayout());
			fenetre.getContentPane().add(jtp,BorderLayout.CENTER);

			//fenetre.getContentPane().add(new JScrollPane(debug_text),BorderLayout.CENTER);
			fenetre.setVisible(true);

			fenetre.addWindowListener(this);
			
			mySema.release();
		} catch (InterruptedException ie) {
			BiomajLogger.getInstance().log(ie);
		}
	}
	
	
	public synchronized void addTextPane(String titleConsole,Thread we,int type) {

		instance = instance+50;
		text=new JTextPane();
		text.setEditable(false);
		this.we = we;


		sdoc = text.getStyledDocument();

		Style titleStyle = sdoc.addStyle("info", null);
		StyleConstants.setBackground(titleStyle , Color.WHITE);
		StyleConstants.setForeground(titleStyle , Color.BLACK);
		StyleConstants.setFontFamily(titleStyle , "dialoginput");
		//StyleConstants.setBold(titleStyle,true);
		StyleConstants.setFontSize(titleStyle , 12);

		Style descrStyle = sdoc.addStyle("task", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.BLUE);
		//StyleConstants.setBold(descrStyle, true);

		descrStyle = sdoc.addStyle("target", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.BLACK);
		StyleConstants.setBold(descrStyle, true);

		descrStyle = sdoc.addStyle("erreur", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.RED);
		StyleConstants.setBold(descrStyle, true);

		descrStyle = sdoc.addStyle("warning", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.MAGENTA);
		StyleConstants.setBold(descrStyle, true);

		descrStyle = sdoc.addStyle("debug", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.GRAY);
		//StyleConstants.setBold(descrStyle, true);

		descrStyle = sdoc.addStyle("verbose", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.DARK_GRAY);
		//StyleConstants.setBold(descrStyle, true);
		descrStyle = sdoc.addStyle("depends", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.GREEN);
		
		descrStyle = sdoc.addStyle("depends_volatil", titleStyle);
		StyleConstants.setForeground(descrStyle, Color.ORANGE);



		//Font font = new Font("Arial", Font.BOLD, 10);
		//textArea.setFont(font);

		//fenetre.getContentPane().setLayout(new BorderLayout());
		scroll = new JScrollPane(text);
		scroll.setAutoscrolls(true);
		ImageIcon icon = null;
		scroll.setName(titleConsole);
		
		if (type==BiomajConsoleLogger.PRE) {
			icon = new ImageIcon(BiomajUtils.getBiomajRootDirectory()+"/xslt/images/icon_cycle_biomaj_pre.png","Preprocess");
		}
		else if (type==BiomajConsoleLogger.SYNC) {
			icon = new ImageIcon(BiomajUtils.getBiomajRootDirectory()+"/xslt/images/icon_cycle_biomaj_sync.png","Synchronize");
		}
		else if (type==BiomajConsoleLogger.POST) {
			icon = new ImageIcon(BiomajUtils.getBiomajRootDirectory()+"/xslt/images/icon_cycle_biomaj_post.png","Postprocess");
		} 
		else if (type==BiomajConsoleLogger.DEP) {
			icon = new ImageIcon(BiomajUtils.getBiomajRootDirectory()+"/xslt/images/icon_cycle_biomaj_dep.png","Deployement");
		}
		else if (type==BiomajConsoleLogger.SYNC_DEP) {
			icon = new ImageIcon(BiomajUtils.getBiomajRootDirectory()+"/xslt/images/icon_cycle_biomaj_sync_dep.png","Synchronize/Deployement");
		}
		else if (type==BiomajConsoleLogger.REMOVE) {
			icon = new ImageIcon(BiomajUtils.getBiomajRootDirectory()+"/xslt/images/icon_cycle_biomaj_remove.gif","Postprocess");
		}
		
		try {
			// Semaphore not to have two identical indexes
			mySema.acquire();
			jtp.addTab(titleConsole,icon,scroll);
			tabIndex = jtp.getTabCount() - 1;
			mySema.release();
		} catch (InterruptedException ex) {
			BiomajLogger.getInstance().log(ex);
		}

		//fenetre.getContentPane().add(new JScrollPane(debug_text),BorderLayout.CENTER);
		//fenetre.setVisible(true);

		//fenetre.addWindowListener(this);
	}
	
	public void setEmacsMode(boolean arg0) {
		// TODO Auto-generated method stub

	}

	public void setErrorPrintStream(PrintStream arg0) {
	}

	public void setMessageOutputLevel(int arg0) {
		// TODO Auto-generated method stub
		level = arg0;
	}

	public void setOutputPrintStream(PrintStream arg0) {

	}

	public void buildFinished(BuildEvent arg0) {
		try {
			mySema.acquire();
			if (checkArg(arg0)) {
				RemoteBankWorkflowEngine rbwe = null;
				if (we instanceof RemoteBankWorkflowEngine)
					rbwe = (RemoteBankWorkflowEngine) we;
				else if (we instanceof ComputedBankWorkflowEngine)
					rbwe = ((ComputedBankWorkflowEngine) we).getRemoteBankWorkflowEngine();
				
				if (rbwe != null && rbwe.getBank() != null) {// Tests sur le workflow
					if (rbwe.getBank().getCurrentSession().getStatus())
						jtp.setBackgroundAt(tabIndex, OK_COLOR);
					else
						jtp.setBackgroundAt(tabIndex, ERROR_COLOR);
				} else { // Tests sur les process
					if (arg0.getException() != null)
						jtp.setBackgroundAt(tabIndex, ERROR_COLOR);
					else
						jtp.setBackgroundAt(tabIndex, OK_COLOR);
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			mySema.release();
		}
	}

	public void buildStarted(BuildEvent arg0) {
		if (checkArg(arg0)) {

		}
	}

	public void messageLogged(BuildEvent arg0) {
		try {
			if (checkArg(arg0)) {
				if (arg0.getPriority() <= level)
				{
					String message = arg0.getMessage();
					int newPosition;
					boolean withReturn = true;
					if (message.contains(NOT_KEEP_LINE_ON_CONSOLE)) {
						
						if (position==0)
							newPosition = 	sdoc.getLength();
						else
							newPosition = position;
						
						message = message.replaceFirst(NOT_KEEP_LINE_ON_CONSOLE, "");
						withReturn = false;
					} else {
						newPosition = 0;
					}
					
					Style s = sdoc.getStyle("info");
					
					if (message.contains(BmajExecute.WARNING_FILTER)) {
						message = message.replace(BmajExecute.WARNING_FILTER, "");
						s = sdoc.getStyle("warning");
					} else if (message.contains(BmajExecute.DEPENDANCE_VOLATILE__FILTER)) {
						message = message.replace(BmajExecute.DEPENDANCE_VOLATILE__FILTER, "");
						s = sdoc.getStyle("depends_volatil");
					} else if (message.contains(BmajExecute.DEPENDANCE_FILTER)) {
						message = message.replace(BmajExecute.DEPENDANCE_FILTER, "");
						s = sdoc.getStyle("depends");
					} 
					else if (arg0.getPriority() == Project.MSG_ERR) 
						s = sdoc.getStyle("erreur");
					else if (arg0.getPriority() == Project.MSG_WARN)
						s = sdoc.getStyle("warning");
					else if (arg0.getPriority() == Project.MSG_DEBUG)
						s = sdoc.getStyle("debug");
					else if (arg0.getPriority() == Project.MSG_VERBOSE)
						s = sdoc.getStyle("verbose");

					
					
					if (position==0)
						position = sdoc.getLength();
					else {
						
						String value = sdoc.getText(sdoc.getLength()-message.length(), message.length());
						if (message.compareTo(value)==0) {
							return;
						} 
						sdoc.remove(position, sdoc.getLength()-position);
					}
					if (withReturn)
						sdoc.insertString(position,message+"\n",s);
					else {	
						sdoc.insertString(position,message,s);
					}
					text.setCaretPosition(text.getDocument().getLength ());
					
					position = newPosition;
					/*
					try {
						l = text.getDocument().getLength ();
						
						if (l<0)
							return;
						
						Rectangle rec = text.modelToView(l);
						
						text.scrollRectToVisible(rec);
						} catch (javax.swing.text.BadLocationException err) {
						} catch (Exception e) {
							e.printStackTrace();	
							System.out.println("***************************************["+Integer.toString(l)+"]*******************");
						}*/
					
				}
			} 
			/*
			if (arg0.getPriority() == Project.MSG_ERR) {
				sdoc.insertString(sdoc.getLength(),"***"+arg0.getMessage()+"****\n",sdoc.getStyle("erreur"));
			}

			if (arg0.getPriority() == Project.MSG_WARN) {
				sdoc.insertString(sdoc.getLength(),"***"+arg0.getMessage()+"****\n",sdoc.getStyle("warning"));
			}
			 */
		} catch (BadLocationException ex) {
			BiomajLogger.getInstance().log(ex.getMessage());
		}
	}

	public void targetFinished(BuildEvent arg0) {/*
		if (checkArg(arg0)) {
			
			System.out.println("\nTarget : "+arg0.getTarget().getName());
			System.out.println("Exception : "+arg0.getException());
		}*/
	}

	public void targetStarted(BuildEvent arg0) {
	}

	public void taskFinished(BuildEvent arg0) {
		if (checkArg(arg0)) {
		}

	}

	public void taskStarted(BuildEvent arg0) {
		try {
			if (checkArg(arg0)) {
				if ((arg0.getTask() != null)&&(lastTask.compareTo(arg0.getTask().getTaskName())!=0)) {
					
					String taskName = arg0.getTask().getTaskName();
					
					if (currentTaskName.compareTo(taskName)==0)
						return;
					currentTaskName = taskName;
					
					if (taskToNotWrite(taskName))
						return;
					
					sdoc.insertString(sdoc.getLength(),"   ===  ["+arg0.getTask().getTaskName()+"]   === ".toUpperCase()+"\n",sdoc.getStyle("task"));
					//lastTask = arg0.getTask().getTaskName();
				}
			}
		} catch (BadLocationException ex) {
			BiomajLogger.getInstance().log(ex);
		}
	}

	public boolean checkArg(BuildEvent arg0) {
		if (arg0 == null)
		{
			return false;
		}
		return true;
	}
	
	
	protected boolean taskToNotWrite(String taskName) {
		
		if (taskName.contains("bmaj"))
			return false;
		
		return true;
	}

	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	public void windowClosed(WindowEvent e) {
		//System.out.println("Close:"+fenetre.getTitle());
		//we.interrupt();
		//while (!we.isInterrupted()) {}
		//System.out.println("Very very Close:"+fenetre.getTitle());
	}

	public void windowClosing(WindowEvent e) {
		//System.out.println("Close:"+fenetre.getTitle());
		//	we.interrupt();
		jtp.removeAll();
		fenetre.dispose();
		we.interrupt();
		/*
		try {
			this.finalize();
		} catch(Throwable t) {
			System.err.println(t.getMessage());
		}
		 */
		//while (!we.isInterrupted()) {}
		//System.out.println("Very very Close:"+fenetre.getTitle());

	}

	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}



}
