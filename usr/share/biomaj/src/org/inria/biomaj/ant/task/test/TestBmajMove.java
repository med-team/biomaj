package org.inria.biomaj.ant.task.test;

import static junit.framework.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;
import org.inria.biomaj.ant.task.BmajMove;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for BmajMove class.
 * 
 * @author rsabas
 *
 */
public class TestBmajMove {
	
	private static final String FROM_DIR = BiomajUtils.getBiomajRootDirectory() + "/fromtestmove";
	private static final String TO_DIR = BiomajUtils.getBiomajRootDirectory() + "/totestmove";
	private List<File> toMove;
	private FileSet fileSet;
	private long size = 0;
	
	/**
	 * Creates source and destination directories and files to move.
	 */
	@Before
	public void setup() {
		toMove = new ArrayList<File>();
		fileSet = new FileSet();
		new File(FROM_DIR).mkdir();
		new File(TO_DIR).mkdir();
		
		for (int i = 0; i < 5; i++) {
			File f = new File(FROM_DIR + "/testmove" + i);
			writeRandom(f);
			toMove.add(f);
			size = f.length();
		}
		
		fileSet.setDir(new File(FROM_DIR));
	}
	
	/**
	 * Removes the directories that contained the test files.
	 */
	@After
	public void cleanup() {
		BiomajUtils.deleteAll(new File(FROM_DIR));
		BiomajUtils.deleteAll(new File(TO_DIR));
	}

	/**
	 * Test the move method via file copy.
	 */
	@Test
	public void runTestCopy() {
		
		BmajMove move = new BmajMove();
		move.setMode(BmajMove.MODE_COPY);
		move.setToDir(TO_DIR);
		move.addConfiguredFileSet(fileSet);
		move.setProject(getProject());
		move.execute();
		
		checkFiles();
	}
	
	/**
	 * Test the move method via file move (rename).
	 */
	@Test
	public void runTestMove() {
		BmajMove move = new BmajMove();
		move.setMode(BmajMove.MODE_MOVE);
		move.setToDir(TO_DIR);
		move.setProject(getProject());
		move.addConfiguredFileSet(fileSet);
		move.execute();
		
		checkFiles();
	}
	
	/**
	 * Initializes a project with the required properties.
	 * 
	 * @return
	 */
	private Project getProject() {
		Project p = new Project();
		p.setProperty(BiomajConst.dataDirProperty, BiomajUtils.getBiomajRootDirectory());
		p.setProperty(BiomajConst.offlineDirProperty, "fromtestmove");
		
		return p;
	}
	
	/**
	 * Writes a random content in a file.
	 * 
	 * @param file
	 */
	private static void writeRandom(File file) {
		file.delete();
		String rnd = "alezkj lkj-ij mk	j ifj mkjqsmkj	m kjsqmdlkj qmskj " +
				"szd kqjm kj mqskj mkfjmri mlzkrjzm lkj mlkj zoiapodjk kj zklj " +
				"zevdmlkjb iJmZRIJMQKLJ eROBJoMllmkj mkj mlkjdm lkjm";
		
		PrintWriter pw;
		try {
			pw = new PrintWriter(file);
			pw.println(rnd);
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Checks existence and size.
	 */
	private void checkFiles() {
		for (File f : toMove) {
			File check = new File(TO_DIR + "/" + f.getName());
			assertTrue(check.exists());
			assertTrue(size == check.length());
			assertTrue(!f.exists());
		}
	}
	
}
