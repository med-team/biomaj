/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task.net;

import java.text.ParseException;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;

/**
 * Interface to parse line from a specific protocol
 * @author   ofilangi
 */
public interface ListingParser {
	/**
	 * Get file name
	 * @uml.property  name="fileName"
	 */
	public String getFileName() ;
	/**
	 * Get link name
	 * @uml.property  name="linkName"
	 */
	public String getLinkName() ;
	/** Get size */
	public long getFileSize() ;
	/** True if the file is a link */
	public boolean isLink() ;
	/** Creation Time */
	public long getTime() ;
	/** Set a line to parsed */
	public void setLine(String s) ;
	/** Process the parsing 
	 * @throws ParseException TODO*/
	public boolean parse() throws ParseException ;
	
	/**
	 * @uml.property  name="remoteFile"
	 * @uml.associationEnd  
	 */
	public RemoteFile getRemoteFile();
}
