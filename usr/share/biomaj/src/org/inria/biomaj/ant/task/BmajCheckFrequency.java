/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task;

import java.util.Date;
import java.util.Locale;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.ProductionDirectory;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * <p>This task is instanciate in mirror.xml with the name : bmaj-addlocalfile.<br/>
 * check biomaj_common.xml to see the map of task.
 * </p>
 * <p>
 * TaskBiomaj to decide if the workflow can be executed. Administrator tune this
 * property in the .properties.
 * 
 * This task take the oldest file in the production directory and compare with
 * current date
 * </p>
 * 
 * @deprecated
 * @author ofilangi
 * 
 */
public class BmajCheckFrequency extends Task {

	/**
	 * Frequency to update the bank (0, n, daily, weekly, monthly)
	 * @uml.property  name="frequency"
	 */
	private String frequency;
	/**
	 * Value property to set a result true/false true if the bank can be up to date otherwise false
	 * @uml.property  name="value"
	 */
	private String value = "";

	/**
	 * 
	 */
	@Override
	public void execute() throws BuildException {

		try {
			getProject().setProperty(value, Boolean.toString(frequencyIsOk()));
		} catch (BiomajException e) {
			throw new BiomajBuildException(getProject(), e);
		}
	}

	/**
	 * @return the frequency
	 * @uml.property name="frequency"
	 */
	public String getFrequency() {
		return frequency;
	}

	/**
	 * @param frequency
	 *            the frequency to set
	 * @uml.property name="frequency"
	 */
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public Long getNumberFrequency() {

		if (frequency.equalsIgnoreCase("daily"))
			return new Long(1);
		else if (frequency.equalsIgnoreCase("weekly"))
			return new Long(6);
		else if (frequency.equalsIgnoreCase("monthly"))
			return new Long(30);
		else {
			try {
				return Long.valueOf(frequency);
			} catch (Exception e) {
				BiomajLogger.getInstance().log("Problem with frequency update:"
						+ e.getMessage());
				BiomajLogger.getInstance().log(e);
				throw new BiomajBuildException(getProject(),
						"bmajCheckFrequency.error.frequency", frequency, e);
			}
		}
	}

	/**
	 * @return the value
	 * @uml.property name="value"
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 * @uml.property name="value"
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * 
	 * @return
	 * @throws BiomajException
	 */
	
	
	public boolean frequencyIsOk() throws BiomajException {
		
		ProductionDirectory pd = BiomajSQLQuerier.getLatestProductionDirectory(BiomajUtils.getProperty(getProject(),BiomajConst.dbNameProperty));

		if (pd == null) {
			log("No production directory has been created.");
			getProject().setProperty(value, Boolean.toString(true));
			return true;
		}
		Date date = pd.getCreationDate();
		if (date == null)
			return true;

		Date today = new Date();

		Long freq = getNumberFrequency() * 24 * 60 * 60 * 1000;

		log("frequency update check:[session update:"
				+ BiomajUtils.dateToString(date, Locale.US) + "][today:"
				+ BiomajUtils.dateToString(today, Locale.US) + "][freq prop:"
				+ Long.toString(freq) + "]", Project.MSG_INFO);
		Long elapsedTime = today.getTime() - date.getTime();

		if (elapsedTime < 0)
			throw new BiomajBuildException(getProject(),
					"bmajcheckfrequency.error.date", BiomajUtils
							.dateToString(date, Locale.US), BiomajUtils
							.dateToString(today, Locale.US), Long.toString(freq),
					new Exception());

		log("elapsed time :" + Long.toString(elapsedTime), Project.MSG_INFO);
		log("frequency prop:" + Long.toString(freq), Project.MSG_INFO);

		return (elapsedTime >= freq);
	}

}
