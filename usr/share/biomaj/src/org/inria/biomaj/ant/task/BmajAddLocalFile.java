/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajConst;
/**
 * <p>This task is instanciate in mirror.xml with the name : bmaj-addlocalfile.<br/>
 * check biomaj_common.xml to see the map of task.
 * </p>
 * @author ofilangi
 * @version Biomaj 0.9
 * @since Biomaj 0.8 /Citrina 0.5
 */
public class BmajAddLocalFile extends BmajTask {

	/**
	 * @uml.property  name="file"
	 */
	private String file = null ;

	/**
	 * @uml.property  name="fileList"
	 */
	private String fileList = null ;

	@Override
	public void execute() throws BuildException {
		
		boolean logFiles = Boolean.valueOf(getProject().getProperty(BiomajConst.logFilesProperty));
		if (!logFiles)
			return;

		if ((file != null)&&(file.trim().compareTo("")!=0))
		{
			addLocalOnlineFile(file);
			log("file ["+file+"] is add as a local online file.",Project.MSG_VERBOSE);
			return;
		}

		if (fileList==null) {
			log("None attribut (file or filelist) are set!",Project.MSG_ERR);
			return;
		}

		try {
			BufferedReader br = new BufferedReader(new FileReader(fileList));
			String line;

			while ((line=br.readLine())!= null) {

				if (line.compareTo("#PLACE_HOLDER_TO_PREVENT_EMPTY_FILE")==0)
					continue;

				String myfile = getProject().getProperty(BiomajConst.dataDirProperty)+"/"+getProject().getProperty(BiomajConst.offlineDirProperty)+"/"+line;

				log("file ["+myfile+"] is add as a local online file.",Project.MSG_VERBOSE);

				addLocalOnlineFile(myfile);

			}

		} catch (IOException e) {
			throw new BiomajBuildException(getProject(),e);
		} 


	}

	@Override
	public String getTaskName() {
		return "BmajAddLocalFile";
	}

	/**
	 * @return  the file
	 * @uml.property  name="file"
	 */
	public String getFile() {
		return file;
	}

	/**
	 * @param file  the file to set
	 * @uml.property  name="file"
	 */
	public void setFile(String file) {
		this.file = file;
	}

	/**
	 * @return  the fileList
	 * @uml.property  name="fileList"
	 */
	public String getFileList() {
		return fileList;
	}

	/**
	 * @param fileList  the fileList to set
	 * @uml.property  name="fileList"
	 */
	public void setFileList(String fileList) {
		this.fileList = fileList;
	}
}
