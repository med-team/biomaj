package org.inria.biomaj.ant.task;

import java.io.File;
import java.io.IOException;

import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.logger.BiomajConsoleLogger;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

public class MoveThread extends Thread {

	private BmajMove move;
	private Exception error = null;
	private int total = 0;
	private boolean hardCopy = false;
	private boolean logFiles = false;
	private String toDir;
	
	public MoveThread(BmajMove move, int total, boolean hardCopy, boolean logFiles, String toDir) {
		this.move = move;
		this.total = total;
		this.hardCopy = hardCopy;
		this.logFiles = logFiles;
		this.toDir = toDir;
	}
	
	public Exception getError() {
		return error;
	}
	
	@Override
	public void run() {
		String[] content = null;
		
		while ((content = move.getFile()) != null) {

			String fileName = content[0];
			int count = Integer.valueOf(content[1]);
			
			try {
				float a = ((float) count / (float) total) * 100;
				move.log(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE+"["+Integer.toString((int)a)+"%]",Project.MSG_INFO);
				if (hardCopy) {
					move.log("COPY:"+BiomajUtils.getOfflineDirectory(move.getProject())+"/"+fileName+" to "+toDir,Project.MSG_VERBOSE);
				} else { // Just move
					move.log("MOVE:"+BiomajUtils.getOfflineDirectory(move.getProject())+"/"+fileName+" to "+toDir,Project.MSG_VERBOSE);
				}
				File tempFile = new File(BiomajUtils.getOfflineDirectory(move.getProject())+"/"+fileName);
				File targetFile = new File(toDir + "/" + stripTmpExtractDir(fileName));
				
				if (hardCopy) {
					BiomajUtils.copy(tempFile, targetFile);
					tempFile.delete();
				} else {
					BiomajUtils.createSubDirectories(BiomajUtils.getRelativeDirectory(targetFile.getAbsolutePath()));
					tempFile.renameTo(targetFile);
				}
				
				if (logFiles)
					move.addProductionFile(targetFile.getCanonicalPath(), hardCopy);
			} catch (IOException ioe) {
				System.out.println("######"+ioe);
				throw new BiomajBuildException(move.getProject(),"io.error",ioe.getMessage(),ioe);
			} catch (BiomajException be) {
				throw new BiomajBuildException(move.getProject(),be);
			}
		}
	}
	
	private String stripTmpExtractDir(String file) {
		if (file.startsWith(BmajExtract.TMP_DIR)) {
			return file.substring(file.indexOf('/') + 1);
		}
		return file;
		
	}
}
