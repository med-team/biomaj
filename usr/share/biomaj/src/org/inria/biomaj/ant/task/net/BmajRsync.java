/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.ant.task.net;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Vector;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Execute;
import org.apache.tools.ant.taskdefs.LogStreamHandler;
import org.apache.tools.ant.types.Commandline;
import org.gmod.biomaj.ant.task.InputValidation;
import org.inria.biomaj.ant.logger.BiomajConsoleLogger;
import org.inria.biomaj.ant.task.BmajTask;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.singleton.BiomajInformation;
import org.inria.biomaj.utils.BiomajBuildException;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

public class BmajRsync extends BmajTask {

	/**
	 * @uml.property  name="server"
	 */
	private String server ;
	/**
	 * @uml.property  name="userid"
	 */
	private String userid ;
	/**
	 * @uml.property  name="password"
	 */
	private String password;
	/**
	 * @uml.property  name="remotedir"
	 */
	private String remotedir;
	
	/**
	 * @uml.property  name="failonerror"
	 */
	private String failonerror;
	/**
	 * @uml.property  name="listing"
	 */
	private String listing;
	/**
	 * @uml.property  name="toDir"
	 */
	private String toDir;
	
	/**
	 * @uml.property  name="continueOnError"
	 */
	private boolean continueOnError = true;
	
	/**
	 * @uml.property  name="cmd"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private Commandline cmd = new Commandline();
  

    public BmajRsync() {
        //cmd.createArgument().setValue("--delete"); 
    }

    @Override
	public void execute() throws BuildException {
    	InputValidation.checkString(getProject(),server, "");
		InputValidation.checkString(getProject(),userid, "");
		InputValidation.checkString(getProject(),password, "");
		InputValidation.checkString(getProject(),remotedir, "");
		InputValidation.checkString(getProject(),listing, "");
		InputValidation.checkString(getProject(),toDir, "");
    	BufferedReader br;
    	Vector<RemoteFile> files      = new Vector<RemoteFile>();
    	try {
    		
    		String rsync = BiomajInformation.getInstance().getProperty(BiomajInformation.RSYNC);
            cmd.setExecutable(rsync);
            cmd.createArgument().setValue("-t");
            
    		br = new BufferedReader(new FileReader(listing));
    		String line;
    		while ((line=br.readLine())!= null) {
    			files.add(new RemoteFile(line));
    		}
    	} catch (IOException e) {
        		throw new BiomajBuildException(getProject(),e);
    	} catch (BiomajException be) {
        		throw new BuildException(be);
    	} catch (ParseException pe) {
    		throw new BiomajBuildException(getProject(),pe);
    	}
    	
    	for (int i=0;i<files.size();i++) {
    		try {
    			RemoteFile rf = files.get(i);
    			Commandline c = (Commandline) cmd.clone();
    			c.createArgument().setValue("rsync://"+server+remotedir+"/"+rf.getAbsolutePath());
    			
    			String subDirectory = BiomajUtils.getRelativeDirectory(rf.getAbsolutePath());
    			String newToDir = toDir+"/"+subDirectory;
    			BiomajUtils.createSubDirectories(newToDir);
    			c.createArgument().setValue(newToDir);
    			Execute exe = new Execute(new LogStreamHandler(this, 
    				Project.MSG_INFO,
    				Project.MSG_ERR),
    				null);
    			exe.setCommandline(c.getCommandline());
    			exe.execute();
    			
    			int code_retour = exe.getExitValue();
    			
    			if (code_retour!=0) {
	    			if (!continueOnError)
	    				throw new BiomajBuildException(getProject(),"rsync.error.download",rf.getAbsolutePath(),new Exception());
	    			continue;
    			}
    			
    			log(BiomajConsoleLogger.NOT_KEEP_LINE_ON_CONSOLE+Integer.toString(i+1)+"/"+Integer.toString(files.size())+" file(s) downloaded!");
    			addDownloadFile(rf.getAbsolutePath(), Long.toString(rf.getDate().getTime()), Long.toString(rf.getSize()));
       	
    		} catch (Exception e) {
    			log("I/O Exception :"+e.getMessage(),Project.MSG_WARN);
    			if (!continueOnError)
    				throw new BiomajBuildException(getProject(),e);
    		}
    	}
    	log(Integer.toString(files.size())+"/"+Integer.toString(files.size())+" file(s) downloaded!");
    }
    
	/**
	 * @return  the toDir
	 * @uml.property  name="toDir"
	 */
	public String getToDir() {
		return toDir;
	}



	/**
	 * @param toDir  the toDir to set
	 * @uml.property  name="toDir"
	 */
	public void setToDir(String toDir) {
		this.toDir = toDir;
	}



	/**
	 * @return  the listing
	 * @uml.property  name="listing"
	 */
	public String getListing() {
		return listing;
	}



	/**
	 * @param listing  the listing to set
	 * @uml.property  name="listing"
	 */
	public void setListing(String listing) {
		this.listing = listing;
	}



	/**
	 * @return  the password
	 * @uml.property  name="password"
	 */
	public String getPassword() {
		return password;
	}



	/**
	 * @param password  the password to set
	 * @uml.property  name="password"
	 */
	public void setPassword(String password) {
		this.password = password;
	}



	/**
	 * @return  the remotedir
	 * @uml.property  name="remotedir"
	 */
	public String getRemotedir() {
		return remotedir;
	}



	/**
	 * @param remotedir  the remotedir to set
	 * @uml.property  name="remotedir"
	 */
	public void setRemotedir(String remotedir) {
		this.remotedir = remotedir;
	}



	/**
	 * @return  the server
	 * @uml.property  name="server"
	 */
	public String getServer() {
		return server;
	}



	/**
	 * @param server  the server to set
	 * @uml.property  name="server"
	 */
	public void setServer(String server) {
		this.server = server;
	}



	/**
	 * @return  the userid
	 * @uml.property  name="userid"
	 */
	public String getUserid() {
		return userid;
	}



	/**
	 * @param userid  the userid to set
	 * @uml.property  name="userid"
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}



	/**
	 * @return  the failonerror
	 * @uml.property  name="failonerror"
	 */
	public String getFailonerror() {
		return failonerror;
	}



	/**
	 * @param failonerror  the failonerror to set
	 * @uml.property  name="failonerror"
	 */
	public void setFailonerror(String failonerror) {
		this.failonerror = failonerror;
		  if (failonerror!=null)
			  if ((failonerror.trim().compareTo("true")==0)||
						(failonerror.trim().compareTo("on")==0)||
						(failonerror.trim().compareTo("yes")==0))
					continueOnError=false;
	}
}
