package org.inria.biomaj.singleton;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.InvalidPropertiesFormatException;
import java.util.StringTokenizer;

import org.inria.biomaj.exe.main.Biomaj;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * Classe Singleton utilise pour donner les informations de configuration de l'application  
 * 
 * @author ofilangi
 * @version Biomaj 0.9
 * @since Biomaj 0.8 /Citrina 0.5
 */
public class BiomajInformation {

	/**
	 * Directories section
	 */
	protected final static String DIRECTORIES_SECTION="DIRECTORIES";


	public final static String LOGDIR="log.dir";
	public final static String LOGBIOMAJDIR="log-biomaj.dir";
	public final static String STATEFILESDIR="statefiles.dir";
	public final static String WORKFLOWSDIR="workflows.dir";
	public final static String PROCESSDIR="process.dir";
	public final static String WEBREPORTDIR="webreport.dir";
	public final static String TMPDIR="tmp.dir";

	protected final static String APPLICATIONS_SECTION="APPLICATIONS";
	/*
	public static final String  TAR = "tar.bin";
	public static final String  GUNZIP  = "gunzip.bin";
	public static final String  BUNZIP  = "bunzip.bin";
	public static final String  UNZIP  = "unzip.bin";
	 */
	public static final String  WGET  = "wget.bin"; 
	public static final String  RSYNC = "rsync.bin"; 

	public static final String  LIST_BIN_UNCOMPRESS  = "uncompress.bin";

	public static final String  OPTION_BIN     = ".bin";
	public static final String  BIN_CASE       = ".case";
	public static final String  OPTION_TEST    = ".option.test";
	public static final String  OPTION_DECOMP  = ".option.uncomp";
	public static final String  OPTION_OUTPUT  = ".option.output";
	
	public static String CONF_LOCATION = BiomajUtils.getBiomajRootDirectory();


	protected final static String ENGINE_SECTION="ENGINE";

	public static final String  DEBUG    = "debug";
	public static final String  LOG_FLAG = "bank.log";

	/**
	 * Applications section
	 */
	private static Hashtable<String, Hashtable<String,String>> _sections; 

	private String envBmaj;


	/** 
	 * Implementation du singleton
	 */
	private static BiomajInformation uniqueInstance = null;

	/**
	 * Chargement du fichier de configuration general
	 *
	 */
	private BiomajInformation () {

		_sections = new Hashtable<String, Hashtable<String,String>>(); 
		envBmaj = BiomajUtils.getBiomajRootDirectory();

		File f = new File(CONF_LOCATION+"/"+BiomajConst.nameGeneralConf);
		try {
			if (!f.exists()) {
				System.out.println("The file ["+CONF_LOCATION+"/"+BiomajConst.nameGeneralConf+"] does not exist!");
				System.out.println("BioMAJ create a new file. Edit the file to change input/output directories.");
				BufferedWriter bw = new BufferedWriter (new FileWriter(f));
				bw.write("# Created by BioMAJ "+Biomaj.VERSION);bw.newLine();
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				bw.write("# Date : "+sdf.format(new Date()));bw.newLine();
				bw.write("# File : General configuration");bw.newLine();
				bw.write("[DIRECTORIES]");bw.newLine();
				bw.write("log.dir        ="+envBmaj+"/log");bw.newLine();
				bw.write("log-biomaj.dir ="+envBmaj+"/log/biomaj-runtime");bw.newLine();
				bw.write("statefiles.dir ="+envBmaj+"/statefiles");bw.newLine();
				bw.write("workflows.dir  ="+envBmaj+"/conf/db_properties");bw.newLine();
				bw.write("process.dir    ="+envBmaj+"/conf/process");bw.newLine();
				bw.write("webreport.dir  ="+envBmaj+"/rapport");bw.newLine();
				bw.write("tmp.dir        ="+envBmaj+"/tmp");bw.newLine();
				bw.write("");bw.newLine();
				bw.write("[APPLICATIONS]");bw.newLine();

				bw.write("uncompress.bin=tar,tar2,tar3,gunzip,bunzip,unzip");bw.newLine();
				
				bw.newLine();
				
				bw.write("tar.bin="+which("tar"));bw.newLine();
				bw.write("tar.case=.tar.gz,.tgz,.tar.Z");bw.newLine();
				bw.write("tar.option.uncomp=-zxf");bw.newLine();
				bw.write("tar.option.output=-C");bw.newLine();
				bw.write("tar.option.test=-tzf");bw.newLine();
			
				bw.newLine();

				bw.write("gunzip.bin="+which("gunzip"));bw.newLine();
				bw.write("gunzip.case=.gz,.Z");bw.newLine();
				bw.write("gunzip.option.uncomp=-f");bw.newLine();
				bw.write("gunzip.option.test=-t");bw.newLine();
				
				bw.newLine();
				
				bw.write("tar2.bin="+which("tar"));bw.newLine();
				bw.write("tar2.case=.tar.bz2");bw.newLine();
				bw.write("tar2.option.uncomp=-jxf");bw.newLine();
				bw.write("tar2.option.output=-C");bw.newLine();
				bw.write("tar2.option.test=-tjf");bw.newLine();

				bw.newLine();

				bw.write("bunzip.bin="+which("bunzip2"));bw.newLine();
				bw.write("bunzip.case=.bz2");bw.newLine();
				bw.write("bunzip.option.uncomp=-f");bw.newLine();
				bw.write("bunzip.option.test=-t");bw.newLine();

				bw.newLine();

				bw.write("tar3.bin="+which("tar"));bw.newLine();
				bw.write("tar3.case=.tar");bw.newLine();
				bw.write("tar3.option.uncomp=-xf");bw.newLine();
				bw.write("tar3.option.output=-C");bw.newLine();
				bw.write("tar3.option.test=-tf");bw.newLine();

				bw.newLine();

				bw.write("unzip.bin="+which("unzip"));bw.newLine();
				bw.write("unzip.case=.zip");bw.newLine();
				bw.write("unzip.option.uncomp=-q");bw.newLine();
				bw.write("unzip.option.output=-d");bw.newLine();
				bw.write("unzip.option.test=-t");bw.newLine();

				bw.newLine();

				bw.write("wget.bin="+which("wget"));bw.newLine();

				bw.write("rsync.bin="+which("rsync"));bw.newLine();

				bw.write("[ENGINE]");bw.newLine();

				bw.write("debug=false");bw.newLine();
				bw.write("bank.log=true");bw.newLine();

				bw.close();
			}

			load(new FileInputStream(f));


			if (!checkSections()||!checkProperty())
				throw new InvalidPropertiesFormatException("");
		} catch (IOException ioe) {
			System.err.println("** "+BiomajConst.nameGeneralConf+" is not valid ! **");
			System.err.println(ioe.getLocalizedMessage());
			System.exit(1);
		}
	}

	protected String which(String nameExe) {
		try {
			Process p = Runtime.getRuntime().exec("which "+nameExe);
			int res = p.waitFor();
			if (res==0) {
				BufferedReader entree = new BufferedReader( new InputStreamReader(p.getInputStream()) );
				String result = entree.readLine();
				//System.out.println(nameExe+":"+result);
				return result;
			} else
				return "";
		} catch (Exception ie) {
			System.err.println(ie.getLocalizedMessage());
			return "";
		}
	}

	public static synchronized BiomajInformation getInstance() throws BiomajException
	{
		if(uniqueInstance==null)
		{
			uniqueInstance = new BiomajInformation();
		}
		return uniqueInstance;
	}


	/**

	 * @param prop
	 * @return
	 */
	public String getProperty(String prop) {

		assert _sections != null : "_sections has to be defined!" ;


		/**
		 * Les informations contenues sont des repertoires. les repertoires commencant par un '/' sont des chemins absolues
		 * dans le cas contraire il sont relatif a la variable d'environnement $BIOMAJ_ROOT
		 */
		if (_sections.get(DIRECTORIES_SECTION).containsKey(prop)) {

			String dir = _sections.get(DIRECTORIES_SECTION).get(prop) ;

			if (dir.startsWith("/"))
				return dir;

			return envBmaj+"/"+dir;
		}

		for (String h : _sections.keySet()) {
			if (_sections.get(h).containsKey(prop))
				return _sections.get(h).get(prop);
		}

		return null;
	}


	private boolean checkProperty() {

		assert _sections != null : "_sections has to be defined!" ;

		if (!_sections.get(DIRECTORIES_SECTION).containsKey(BiomajInformation.LOGDIR)) {
			System.err.println("Property ["+BiomajInformation.LOGDIR+"] is not defined in "+BiomajConst.nameGeneralConf);
			return false;
		} else if (!_sections.get(DIRECTORIES_SECTION).containsKey(BiomajInformation.LOGBIOMAJDIR)) {
			System.err.println("Property ["+BiomajInformation.LOGBIOMAJDIR+"] is not defined in "+BiomajConst.nameGeneralConf);
			return false;
		} else if (!_sections.get(DIRECTORIES_SECTION).containsKey(BiomajInformation.WORKFLOWSDIR)) {
			System.err.println("Property ["+BiomajInformation.WORKFLOWSDIR+"] is not defined in "+BiomajConst.nameGeneralConf);
			return false;
		} else if (!_sections.get(DIRECTORIES_SECTION).containsKey(BiomajInformation.PROCESSDIR)) {
			System.err.println("Property ["+BiomajInformation.PROCESSDIR+"] is not defined in "+BiomajConst.nameGeneralConf);
			return false;
		} else if (!_sections.get(DIRECTORIES_SECTION).containsKey(BiomajInformation.STATEFILESDIR)) {
			System.err.println("Property ["+BiomajInformation.STATEFILESDIR+"] is not defined in "+BiomajConst.nameGeneralConf);
			return false;
		} else if (!_sections.get(DIRECTORIES_SECTION).containsKey(BiomajInformation.WEBREPORTDIR)) {
			System.err.println("Property ["+BiomajInformation.WEBREPORTDIR+"] is not defined in "+BiomajConst.nameGeneralConf);
			return false;
		} else if (!_sections.get(DIRECTORIES_SECTION).containsKey(BiomajInformation.TMPDIR)) {
			System.err.println("Property ["+BiomajInformation.TMPDIR+"] is not defined in "+BiomajConst.nameGeneralConf);
			return false;
		}/*  else if (!_sections.get(APPLICATIONS_SECTION).containsKey(BiomajInformation.WGET)) {
			System.err.println("Property ["+BiomajInformation.WGET+"] is not defined in "+BiomajConst.nameGeneralConf);
			return false;
		}*/

		/*if ("".compareTo(which(getProperty(BiomajInformation.WGET)))==0)
		{
			System.err.println("BioMAJ does not find executable :"+ getProperty(BiomajInformation.WGET));
			return false;
		}*/

		if (_sections.get(APPLICATIONS_SECTION).containsKey(BiomajInformation.LIST_BIN_UNCOMPRESS)) {
			String[] listBinUncompress = _sections.get(APPLICATIONS_SECTION).get(BiomajInformation.LIST_BIN_UNCOMPRESS).split(",");

			for (String s : listBinUncompress) {
				if (s.compareTo("")==0)
					continue;
				if (!_sections.get(APPLICATIONS_SECTION).containsKey(s+BiomajInformation.OPTION_BIN)) {
					System.err.println("Property ["+s+BiomajInformation.OPTION_BIN+"] is not defined in "+BiomajConst.nameGeneralConf);
					return false;
				}
				if (!_sections.get(APPLICATIONS_SECTION).containsKey(s+BiomajInformation.BIN_CASE)) {
					System.err.println("Property ["+s+BiomajInformation.BIN_CASE+"] is not defined in "+BiomajConst.nameGeneralConf);
					return false;
				}
			}
		} else {
			System.err.println("Property ["+BiomajInformation.LIST_BIN_UNCOMPRESS+"] is not defined in "+BiomajConst.nameGeneralConf);
			return false;
		}


		if (!_sections.containsKey(ENGINE_SECTION)) {
			_sections.put(ENGINE_SECTION, new Hashtable<String, String>());
			_sections.get(ENGINE_SECTION).put(DEBUG, Boolean.toString(false));
			_sections.get(ENGINE_SECTION).put(LOG_FLAG, Boolean.toString(true));
		} else {
			if (!_sections.get(ENGINE_SECTION).containsKey(DEBUG)) {
				_sections.get(ENGINE_SECTION).put(DEBUG, Boolean.toString(false));
			}
			if (!_sections.get(ENGINE_SECTION).containsKey(LOG_FLAG)) {
				_sections.get(ENGINE_SECTION).put(LOG_FLAG, Boolean.toString(true));
			}
		}

		return true;
	}




	private boolean checkSections() {

		assert _sections != null : "_sections has to be defined!" ;

		if (!_sections.containsKey(DIRECTORIES_SECTION)) {
			System.err.println("Section ["+BiomajInformation.DIRECTORIES_SECTION+"] is not defined in "+BiomajConst.nameGeneralConf);
			return false;
		}
		if (!_sections.containsKey(APPLICATIONS_SECTION)) {
			System.err.println("Section ["+BiomajInformation.APPLICATIONS_SECTION+"] is not defined in "+BiomajConst.nameGeneralConf);
			return false;
		}

		return true;
	}

	/** 
	 * Load the current objet with the data found in the given stream 
	 * @param aStream the stream that represent the INI file. 
	 * @throws Exception in case of problems. 
	 */ 
	public void load(InputStream aStream) throws IOException
	{ 
		if (null == aStream) 
		{ 
			return; 
		} 

		BufferedReader reader = new BufferedReader(new InputStreamReader(aStream)); 
		String line = null; 
		String sectionName = null; 
		Hashtable<String,String> section = null; 
		while ((line = reader.readLine()) != null) 
		{ 
			line = line.trim();
			//comments
			if (line.startsWith("#")||line.compareTo("")==0)
				continue;

			// All the data should be in a section 
			if (null == sectionName) 
			{ 
				if ((!line.startsWith("[")) || (!line.endsWith("]"))) 
				{ 
					throw new InvalidPropertiesFormatException("Invalid format: data found outside section:"+line); 
				} 

				sectionName = line.substring(1, line.length() - 1).trim(); 
				addSection(sectionName); 
				section = getSection(sectionName); 
			} 
			else 
			{ 
				if (line.startsWith("[")) 
				{ 
					if (!line.endsWith("]")) 
					{ 
						throw new InvalidPropertiesFormatException("Invalid format: no ending ] for section name:"+line); 
					} 
					sectionName = line.substring(1, line.length() - 1).trim(); 
					addSection(sectionName); 
					section = getSection(sectionName); 
				} 
				else 
				{ 
					addLineToSection(line, section); 
				} 
			} 
		} 
	} 

	/** 
	 * Return the value of the given key in the given section 
	 * @param aSectionName the name of the section 
	 * @param aKey the key 
	 * @return the value if found or null. 
	 */ 
	public String getProperty(String aSectionName, String aKey) 
	{ 
		Hashtable<String, String> section = getSection(aSectionName); 
		if (null == section) 
		{ 
			return null; 
		} 

		return section.get(aKey); 
	} 

	private void addLineToSection(String aLine, Hashtable<String,String> aSection) throws IOException
	{ 
		if (null == aLine) 
		{ 
			return; 
		} 

		if (null == aSection) 
		{ 
			throw new InvalidPropertiesFormatException("No section found to add data"); 
		} 

		aLine = aLine.trim(); 

		// lines that starts with ; are comments 
		if (aLine.startsWith(";")) 
		{ 
			return; 
		} 

		//lines that starts with # are comments 
		if (aLine.startsWith("#")) 
		{ 
			return; 
		} 

		// Avoid the empty lines 
		if (aLine.length() == 0) 
		{ 
			return; 
		} 

		// The format of a line of data is: key = value 
		StringTokenizer st = new StringTokenizer(aLine, "="); 
		if ((st.countTokens() > 2) || (st.countTokens() < 1)) 
		{ 
			throw new InvalidPropertiesFormatException("Invalid format of data: " + aLine); 
		} 

		String key = st.nextToken().trim(); 
		// a key should not contain spaces 
		for (int index = 0; index < key.length(); index++) 
		{ 
			if (Character.isWhitespace(key.charAt(index))) 
			{ 
				throw new InvalidPropertiesFormatException("Invalid format of data: " + aLine); 
			} 
		} 

		if (st.countTokens()==0) {
			aSection.put(key, ""); 
		} else {
			String value = st.nextToken().trim();
			aSection.put(key, value);
		}
	} 

	private void addSection(String aSectionName) 
	{ 
		if (null == aSectionName) 
		{ 
			return; 
		} 

		Hashtable<String,String> section = getSection(aSectionName); 
		if (null == section) 
		{ 
			section = new Hashtable<String,String>(); 
			_sections.put(aSectionName, section); 
		} 
	} 

	public Hashtable<String,String> getSection(String aSectionName) 
	{ 
		return _sections.get(aSectionName); 
	} 


	public String getUncompressedOptionWithFile(String nameFile,String option) throws BiomajException {

		if (_sections.get(APPLICATIONS_SECTION).containsKey(BiomajInformation.LIST_BIN_UNCOMPRESS)) {
			String[] listBinUncompress = _sections.get(APPLICATIONS_SECTION).get(BiomajInformation.LIST_BIN_UNCOMPRESS).split(",");
			ArrayList<String> resultsCase = new ArrayList<String>();
			ArrayList<String> resultsBin = new ArrayList<String>();

			for (String s : listBinUncompress) {
				if (s.compareTo("")==0)
					continue;
				String[] casePattern =  _sections.get(APPLICATIONS_SECTION).get(s+BiomajInformation.BIN_CASE).split(",");

				for (String thecase : casePattern ) {
					if (nameFile.endsWith(thecase)) {
						resultsCase.add(thecase);
						resultsBin.add(s);
					}
				}
			}

			String choice = "";
			String bin = "";
			//On prend le plus grand suffix trouve et on l'enleve au nom de fichier 
			for (int i=0;i<resultsCase.size();i++) {
				String suff = resultsCase.get(i);
				if (suff.length()>choice.length()) {
					choice = suff ;
					bin = resultsBin.get(i);
				}
			}

			if (bin.compareTo("")!=0)
					return _sections.get(APPLICATIONS_SECTION).get(bin+option);

			return "";

		}
		throw new BiomajException("error.dev","bad option ["+option+"] give in argument to BiomajInformation::getUncompressedOptionWithFile");
	}


	public String getUncompressedName(String nameFile) {

		if (_sections.get(APPLICATIONS_SECTION).containsKey(BiomajInformation.LIST_BIN_UNCOMPRESS)) {
			String[] listBinUncompress = _sections.get(APPLICATIONS_SECTION).get(BiomajInformation.LIST_BIN_UNCOMPRESS).split(",");

			ArrayList<String> results = new ArrayList<String>();
			for (String s : listBinUncompress) {
				if (s.compareTo("")==0)
					continue;
				String[] casePattern =  _sections.get(APPLICATIONS_SECTION).get(s+BiomajInformation.BIN_CASE).split(",");

				for (String thecase : casePattern ) {
					if (nameFile.endsWith(thecase)) {
						results.add(thecase);
					}
				}
			}
			String choice = "";
			//On prend le plus grand suffix trouve et on l'enleve au nom de fichier 
			for (String suff : results) {
				if (suff.length()>choice.length())
					choice = suff ;
			}

			return nameFile.replace(choice, "");

		}
		return nameFile ;
	}


}
