package org.inria.biomaj.singleton;

import java.util.Hashtable;
import java.util.Set;

import org.inria.biomaj.session.bank.Bank;
import org.inria.biomaj.utils.BiomajException;

/**
 * <p>
 * Classe de type Singleton permettant de donner des informations sur la Session.
 * 
 * </p>
 * 
 * @author ofilangi
 * @version Biomaj 0.9
 * @since Biomaj 0.8 /Citrina 0.5
 */
public class BiomajSession {

	/** 
	 * Implementation du singleton
	 */
	private static BiomajSession uniqueInstance = null;
	
	/**
	 * Association dbName/Bank object
	 */
	private Hashtable<String, Bank > _banks; 
	
	private BiomajSession () {
		_banks= new Hashtable<String, Bank>();
	}
	
	/**
	 * Methode pour manipuler les singleton
	 * @return
	 * @throws BiomajException
	 */
	public static synchronized BiomajSession getInstance() 
    {
            if(uniqueInstance==null)
            {
                    uniqueInstance = new BiomajSession();
            }
            return uniqueInstance;
    }
	
	
	public synchronized void addBank(String dbName,Bank bank) throws BiomajException {
	BiomajLogger.getInstance().log(dbName+"-BiomajSession:addBank");
		if (_banks.containsKey(dbName))
			throw new BiomajException("banksession.error.addbank",dbName);
		
		if (bank == null)
			throw new BiomajException("banksession.error.nullobject",dbName);
		
		_banks.put(dbName, bank);
	}
	
	public synchronized Bank removeBank(String dbName) {
		BiomajLogger.getInstance().log(dbName+"-BiomajSession:removeBank");
		if (_banks.containsKey(dbName)){
			return _banks.remove(dbName);
		}
		return null;
	}
	
	
	public Bank getBank(String dbName) throws BiomajException {
		BiomajLogger.getInstance().log(dbName+"-BiomajSession:getBank-size:"+_banks.size());
		if (! _banks.containsKey(dbName))
			{
			throw new BiomajException("banksession.error.getbank",dbName);
			}
		
		return _banks.get(dbName);
	}
	
	public String[] getListBank() {
		Set<String> set = _banks.keySet() ;
		String[] res = new String[set.toArray().length];
		res = set.toArray(res);
		return res;
	}
	
}
