package org.inria.biomaj.singleton;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import org.inria.biomaj.ant.logger.SimpleLoggerHistoric;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * Cette classe singleton a deux fonctions :
 * - un logger pour les message d erreur qui ne sont pas lie au context d'un workflow particulier
 * - retourne le logger specifique d'un workflow
 * 
 * @author ofilangi
 * @version Biomaj 0.9
 * @since Biomaj 0.8 /Citrina 0.5
 */
public class BiomajLogger {
	
	public static String DEBUG_LEVEL = "DEBUG";
	public static String INFO_LEVEL = "INFO";
	public static String ERROR_LEVEL = "ERROR";

	private static final String BIOMAJLOG = "biomaj";

	private static HashMap<String, SimpleLoggerHistoric> m_loggerWorkflow = new HashMap<String, SimpleLoggerHistoric>();

	private static BiomajLogger _instance = null ;

	/**
	 * @uml.property  name="logBuffWriter"
	 */
	private BufferedWriter logBuffWriter ; 

	private String fileNameGeneralLog = "";

	private boolean available = false;

	private static boolean generate_log_flag = true;
	
	private BiomajLogger() {
		try {
			//SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
			//File file = new File(BiomajInformation.getInstance().getProperty(BiomajInformation.LOGBIOMAJDIR)+"/"+BiomajLogger.BIOMAJLOG+"."+sdf.format(new Date())+".log");
			available = Boolean.valueOf(BiomajInformation.getInstance().getProperty(BiomajInformation.DEBUG));
			generate_log_flag = Boolean.valueOf(BiomajInformation.getInstance().getProperty(BiomajInformation.LOG_FLAG));
			
			if (available) {
				fileNameGeneralLog = BiomajInformation.getInstance().getProperty(BiomajInformation.LOGBIOMAJDIR)+"/"+BiomajLogger.BIOMAJLOG+"."+new Date().getTime()+".log";
				//System.out.println("[GENERAL] :"+fileNameGeneralLog);
				File file = new File(fileNameGeneralLog);
				BiomajUtils.createSubDirectories(BiomajUtils.getRelativeDirectory(file.getAbsolutePath()));
				logBuffWriter = new BufferedWriter (new FileWriter(file,true));
			}
		} catch (Exception ioe) {
			ioe.printStackTrace();
			System.err.println(ioe.getMessage());
		}
	}

	/**
	 * Retourne l'instance du singleton
	 * @return
	 */
	public static synchronized BiomajLogger getInstance () {
		if (_instance == null ) {
			_instance = new BiomajLogger();
		}
		return _instance ;
	}

	public SimpleLoggerHistoric getLogger(String dbName) {
		if (m_loggerWorkflow.containsKey(dbName))
			return m_loggerWorkflow.get(dbName);
		else 
			return null;
	}

	/**
	 * Initialisation d'un logger
	 * @param dbName
	 * @param directory
	 * @param nameFile
	 * @return
	 */
	public SimpleLoggerHistoric initLogger(String dbName,String directory, String nameFile) {

		SimpleLoggerHistoric slh = null ;

		if (!generate_log_flag)
			return slh;
		
		if (!m_loggerWorkflow.containsKey(dbName)) {
			//System.out.println("["+dbName.toUpperCase()+"]");
			slh = new SimpleLoggerHistoric(nameFile);
			slh.setNameDirectory(directory);
			m_loggerWorkflow.put(dbName, slh);
		}

		return m_loggerWorkflow.get(dbName) ;
	}


	public SimpleLoggerHistoric initLogger(String dbName, String nameFile) {
		log("-- START:"+dbName+" --");
		SimpleLoggerHistoric slh = null ;
		
		if (!generate_log_flag)
			return slh;
		
		if (!m_loggerWorkflow.containsKey(dbName)) {
			//System.out.println("["+dbName.toUpperCase()+"]");
			slh = new SimpleLoggerHistoric(nameFile);
			m_loggerWorkflow.put(dbName, slh);
		}

		return slh ;
	}

	public SimpleLoggerHistoric removeLogger(String dbName) {

		if (!m_loggerWorkflow.containsKey(dbName)) {
			return null;
		} 

		return m_loggerWorkflow.remove(dbName);

	}
	
	/**
	 * Logs a message with information on the level.
	 * 
	 * @param message
	 * @param level
	 */
	public void log(String message, String level) {
		if (level.equals(DEBUG_LEVEL) || 
				level.equals(INFO_LEVEL) ||
				level.equals(ERROR_LEVEL)) {
			
			log("[" + level + "] " + message);
		}
	}

	public void log(String message) {
		if (! available || ! generate_log_flag)
			return ;
		String prec = "["+new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date())+"]";

		//Savoir dans quelle methode on se trouve
		Throwable throwable = new Throwable();
		StackTraceElement[] trace = throwable.getStackTrace();

		int i = 2 ;
		boolean isOk = false;

		while (!isOk) {
			if ((trace.length>i)&&(trace[i].getFileName()!=null)) {
				prec += "["+trace[i].getFileName()+"]";
				prec += "["+trace[i].getMethodName()+"]";
				prec += "["+trace[i].getLineNumber()+"]";
				isOk = true;
			} else if (trace.length<i) {
				isOk = true;
			} else
				i++;
		}

		try {
			open();
			logBuffWriter.write(prec+message);
			logBuffWriter.newLine();
			logBuffWriter.flush();
		} catch (IOException ioe) {
			System.err.println(prec+message);
			System.err.println(ioe.getMessage());
		}
	}

	public void log(Exception e) {
		if (!available || ! generate_log_flag)
			return ;

		if (e != null) {
			for (StackTraceElement st : e.getStackTrace())
				log(st.getFileName()+":"+st.getMethodName()+":"+st.getLineNumber());
			log(e.getMessage());
		}
	}

	private void close() {
		if (!available || ! generate_log_flag)
			return ;
		log("-- END --");
		
		try {
			if ((logBuffWriter!= null)) {
				//System.out.println("[GENERAL]-->close");
				logBuffWriter.close();
				logBuffWriter = null ;
			}

		} catch (IOException e) {
			System.err.println(e.getLocalizedMessage());
		}
	}

	public void close(String dbName) {
		log("-- END:"+dbName+" --");
		//System.out.println("["+dbName.toUpperCase()+"]-->close");
		SimpleLoggerHistoric slh = getLogger(dbName);
		try {
			if (slh != null)
				slh.close();
			removeLogger(dbName);

			if (m_loggerWorkflow.size() == 0)
				close();

		} catch (IOException e) {
			System.err.println(e.getLocalizedMessage());
		}
	}

	public void open() throws IOException {
		if (logBuffWriter == null) {
			File file = new File(fileNameGeneralLog);
			logBuffWriter = new BufferedWriter (new FileWriter(file,true));
		}
	}
	
	public String getFileNameGeneralLog() {
		return fileNameGeneralLog;
	}
}
