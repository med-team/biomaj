package org.inria.biomaj.session.dependency;

import java.util.ArrayList;
import java.util.List;

/**
 * Verifies dependencies validity.
 * 
 * @author rsabas
 *
 */
public class GraphEvaluator {
	
	private GraphElement _root;
	private List<GraphElement> graphAsList = new ArrayList<GraphElement>();
	private List<String> completed = new ArrayList<String>();
	
	/**
	 * 
	 * @param root
	 */
	public GraphEvaluator(GraphElement root) {
		this._root = root;
		buildList(root);
	}
	
	private void buildList(GraphElement root) {
		
		if (!graphAsList.contains(root)) {
			graphAsList.add(root);
			for (GraphElement child : root.getChildren()) {
				buildList(child);
			}
		}
	}
	
	
	public boolean isValid() {
		List<GraphElement> elements = new ArrayList<GraphElement>();
		return findDuplicate(_root, elements);
	}
	
	/**
	 * Look for elements with same value that are not both leaves.
	 * This is the characteristic for mutual dependencies that we dont want:
	 * A -> B -> A
	 * 
	 * @param element current element
	 * @param values already scanned
	 */
	private boolean findDuplicate(GraphElement element, List<GraphElement> elements) {
		int index;
		if ((index = elements.indexOf(element)) >= 0) {
			return element.isLeaf() && elements.get(index).isLeaf();
		}
		elements.add(element);
		for (GraphElement child : element.getChildren()) {
			if (!findDuplicate(child, elements)) {
				return false;
			}
		}
		return true;
	}
	
	public void displayGraph() {
		printValues(_root);
	}
	
	private void printValues(GraphElement e) {
		System.out.println(e.getValue());
		for (GraphElement child : e.getChildren()) {
			printValues(child);
		}
	}
	
	/*
	 * Not used.
	 * BankFactory.createDependsBank() used instead to get execution order.
	 * 
	 */
	public synchronized GraphElement getNextLeafToExecute() {
		GraphElement g;		
		
		while ((g = getLeaf()) == null) {
			
			if (_root.isLeaf())
				return null;
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
		String value = g.getValue();
		completed.add(value);
		
		return g;
		
	}
	
	public String getExecutionOrder() {
		String res = "";
		for (String s : completed)
			res += s + ",";
		
		if (res.length() > 0) {
			return res.substring(0, res.length() - 1);
		}
		
		return res;
	}

	
	private GraphElement getLeaf() {

		for (GraphElement g : graphAsList) {
			if (g.isLeaf() && !g.isProcessed())
				return g;
		}
		
		return null;
		
		/*
		if (root == null)
			return null;
		
		if (root.isLeaf() && !root.isProcessed()) {
			return root;
		} else {
			for (GraphElement child : root.getChildren()) {
				return getLeaf(child);
			}
		}
		
		return null;*/
		
		
	}
	
}
