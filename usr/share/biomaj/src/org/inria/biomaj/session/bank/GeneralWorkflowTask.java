/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.session.bank;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.inria.biomaj.ant.logger.DBWriter;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajUtils;


/**
 * Tache generique du workflow. Une tache est definie par sa date de commencement,sa date de fin, 
 * son temps d execution et son status une liste d'erreur et de warning y est egalement attachee!
 * @author   ofilangi
 */

public abstract class GeneralWorkflowTask {

	public static final int STATUS_OK = 0;
	public static final int STATUS_KO = 1;

	/**
	 * @uml.property  name="_session"
	 * @uml.associationEnd  multiplicity="(1 1)" inverse="workflowtasks:org.inria.biomaj.session.bank.Session"
	 */
	private Session _session ; 
	
	// Task id in the database
	private int taskId;
	
	/**
	 * If disabled speed up the loading.
	 */
	protected boolean loadMessages = true;
	

	public GeneralWorkflowTask(Session s) {
		_session = s;
	}

	/**
	 * @uml.property  name="processName"
	 */
	public abstract String getProcessName() ;

	/**
	 * @uml.property  name="start"
	 */
	private Date start = new Date();

	/**
	 * @uml.property  name="end"
	 */
	private Date end = new Date();

	/**
	 * @uml.property  name="elapsedTime"
	 */
//	private Long elapsedTime = new Long(0);

	/**
	 * @uml.property  name="status"
	 */
	private int status = STATUS_KO;

	/**
	 * @uml.property  name="warn"
	 */
	private Vector<String> warn = new Vector<String>();

	/**
	 * @uml.property  name="err"
	 */
	private Vector<String> err = new Vector<String>();
	
	private Vector<String> oldErr = null;
	private Vector<String> oldWarn = null;

	/**
	 * @return  the err
	 * @uml.property  name="err"
	 */
	public Vector<String> getErr() {
		return err;
	}
	
	public Vector<String> getNewErr() {
		if (oldErr == null) {
			oldErr = new Vector<String>();
			oldErr.addAll(err);
			
			return err;
		}
		
		Vector<String> res = new Vector<String>();
		
		if (oldErr.size() == err.size()) // Nothing new
			return res;
		else if (err.size() > oldErr.size()) {
			for (String s : err) {
				boolean found = false;
				for (String sOld : oldErr) {
					if (s == sOld) {
						found = true;
						break;
					}
				}
				if (!found) {
					oldErr.add(s);
					res.add(s);
				}
			}
		} else {
			throw new RuntimeException("Somehow, some message have been deleted since the previous call.");
		}
		
		return res;
	}

	/**
	 * @param err  the err to set
	 * @uml.property  name="err"
	 */
	public void setErr(Vector<String> err) {
		this.err = err;
	}

	/**
	 * @return  the warn
	 * @uml.property  name="warn"
	 */
	public Vector<String> getWarn() {
		return warn;
	}
	
	public Vector<String> getNewWarn() {
		if (oldWarn == null) {
			oldWarn = new Vector<String>();
			oldWarn.addAll(warn);
			
			return warn;
		}
		
		Vector<String> res = new Vector<String>();
		
		if (oldWarn.size() == warn.size()) // Nothing new
			return res;
		else if (warn.size() > oldWarn.size()) {
			for (String s : warn) {
				boolean found = false;
				for (String sOld : oldWarn) {
					if (s == sOld) {
						found = true;
						break;
					}
				}
				if (!found) {
					oldWarn.add(s);
					res.add(s);
				}
			}
		} else {
			throw new RuntimeException("Somehow, some message have been deleted since the previous call.");
		}
		
		return res;
	}

	/**
	 * @param warn  the warn to set
	 * @uml.property  name="warn"
	 */
	public void setWarn(Vector<String> warn) {
		this.warn = warn;
	}

	
	public void setLoadMessages(boolean load) {
		loadMessages = load;
	}

	/**
	 * @return  the elapsedTime
	 * @uml.property  name="elapsedTime"
	 */
	public long getElapsedTime() {
		if ( (end != null) && (start != null) )
			return end.getTime() - start.getTime();
		return 0;
	}


	/**
	 * @return  the end
	 * @uml.property  name="end"
	 */
	public Date getEnd() {
		return end;
	}


	/**
	 * @param end  the end to set
	 * @uml.property  name="end"
	 */
	public void setEnd(Date end) {
		this.end = end;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	/**
	 * @return  the start
	 * @uml.property  name="start"
	 */
	public Date getStart() {
		return start;
	}


	/**
	 * @param start  the start to set
	 * @uml.property  name="start"
	 */
	public void setStart(Date start) {
		this.start = start;
	}


	/**
	 * @return  the status
	 * @uml.property  name="status"
	 */
	public int getStatus() {
		return status;
	}


	public String getStatusStr() {
		switch (status) {
		case GeneralWorkflowTask.STATUS_KO :return "ko";
		case GeneralWorkflowTask.STATUS_OK:return "ok";
		default:
			return "no_value";
		}
	}

	/**
	 * @param status  the status to set
	 * @uml.property  name="status"
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	public void setStatusStr(String status) {
		if ("ok".compareTo(status)==0)
			this.status = GeneralWorkflowTask.STATUS_OK;
		else if ("ko".compareTo(status)==0)
			this.status = GeneralWorkflowTask.STATUS_KO;
		else {
			BiomajLogger.getInstance().log("internal error, can't set status in GeneralWorkflowTask :"+status);
		}
	}

	
	public void fill(Map<String, String> task) {
		try {
			setStart(BiomajUtils.stringToDate(task.get(BiomajSQLQuerier.TASK_START)));
			String end = task.get(BiomajSQLQuerier.TASK_END);

			if (end != null && end.trim().isEmpty())
				setEnd(BiomajUtils.stringToDate(end));
		} catch (ParseException pe) {
			BiomajLogger.getInstance().log(pe);
			return ;
		}
		
		setTaskId(Integer.valueOf(task.get(BiomajSQLQuerier.TASK_ID)));
		setStatusStr(task.get(BiomajSQLQuerier.TASK_STATUS));
		
		if (loadMessages) {
			List<List<String>> messages = BiomajSQLQuerier.getTaskMessages(Integer.valueOf(task.get(BiomajSQLQuerier.TASK_ID)));
			getWarn().addAll(messages.get(0));
			getErr().addAll(messages.get(1));
		}
	}

	public int compareTo(String nameTag) {
		return getProcessName().compareTo(nameTag);
	}

	/**
	 * Two type tag file:
	 * file download         : attributs name,hash,time,size
	 * file extract and prod : attributs name,hash,refhash
	 * refhash is the hash of the origine file (downloaded)
	 * 
	 * @param files
	 * @param taskId 
	 */
	protected static void addFilesInDB(Vector<FileDesc> files, long taskId) {
		if (files.size() == 0)
			return;

		for (FileDesc file : files) {
			setLocalFile(file, taskId, "file");
		}

	}

	protected static void setLocalFile(FileDesc fd, long taskId, String fileType) {
		
		if (fd.getFileId() < 0) // If file has not been inserted yet
			DBWriter.addFileToTask(fd, taskId, fileType);
	}


	protected static void addFilesInVector(List<Map<String, String>> files, Vector<FileDesc> destination) {
		
		for (Map<String, String> file : files) {
			
			String type = file.get(BiomajSQLQuerier.FILE_TYPE);

			String location, refHash=null;
			long time, size;
			boolean extract, link, volatil;

			String value = file.get(BiomajSQLQuerier.FILE_LOCATION);

			if (isInitialized(value))
				location = value;
			else
				continue;

			value = file.get(BiomajSQLQuerier.FILE_SIZE);
			if (isInitialized(value))
				size = Long.valueOf(value);
			else
				continue;

			value = file.get(BiomajSQLQuerier.FILE_TIME);
			if (isInitialized(value))
				time = Long.valueOf(value);
			else
				continue;

			value = file.get(BiomajSQLQuerier.IS_LINK);
			if (isInitialized(value))
				link = Boolean.valueOf(value);
			else
				continue;

			value = file.get(BiomajSQLQuerier.IS_EXTRACT);
			if (isInitialized(value))
				extract = Boolean.valueOf(value);
			else
				continue;

			value = file.get(BiomajSQLQuerier.IS_VOLATILE);
			if (isInitialized(value))
				volatil = Boolean.valueOf(value);
			else
				continue;

			value = file.get(BiomajSQLQuerier.REF_HASH);
			if ((value!=null)&&(value.compareTo("") != 0))
				refHash = value;

			FileDesc fd ;
			if (type != null) {
				if (type.compareTo("remote") == 0)
					fd = new FileDesc(location, size, time, link, extract);
				else
					fd = new FileDesc(location, size, time, link, extract, volatil, refHash);
			} else {
				BiomajLogger.getInstance().log("GeneralWorkflowTask::addFileInVector Can't find type of file!");
				continue;
			}

			destination.add(fd);
			
		}
	}

	private static boolean isInitialized(String value) {
		return (value!=null)&&(value.compareTo("")!=0);
	}

	public abstract void fillElement(long taskId);

	public Session getSession() {
		return _session;
	}
}
