/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.session.bank;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.inria.biomaj.sql.SQLConnection;
import org.inria.biomaj.sql.SQLConnectionFactory;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;


/**
 * @author  ofilangi
 * @version  Biomaj 0.9
 * @since  Biomaj 0.8 /Citrina 0.5
 */
public class MakeReleaseTask extends GeneralWorkflowTask {

	private Vector<FileDesc> filesCopy = new Vector<FileDesc>();
	private Vector<FileDesc> filesMove = new Vector<FileDesc>();
	
	public MakeReleaseTask(Session s) {
		super(s);
	}

	public void addFile(String location,String refHash,boolean copy) throws BiomajException {
		FileDesc f = new FileDesc(location,false,refHash);
		if (copy)
			this.filesCopy.add(f);
		else
			this.filesMove.add(f);
	}

	@Override
	public void fill(Map<String, String> task) {
		super.fill(task);
		
		List<Map<String, String>> filz = BiomajSQLQuerier.getTaskFiles(Integer.valueOf(task.get(BiomajSQLQuerier.TASK_ID)));
		List<Map<String, String>> filzCopy = new ArrayList<Map<String,String>>();
		List<Map<String, String>> filzMove = new ArrayList<Map<String,String>>();

		for (Map<String, String> file : filz) {
			String type = file.get(BiomajSQLQuerier.FILE_TYPE);
			if (type.equals("copy"))
				filzCopy.add(file);
			else if (type.equals("move"))
				filzMove.add(file);
		}
		addFilesInVector(filzCopy, filesCopy);
		addFilesInVector(filzMove, filesMove);
		
	}

	@Override
	public String getProcessName() {
		return BiomajConst.moveTag;
	}

	@Override
	public void fillElement(long taskId) {
		String query = "UPDATE sessionTask SET taskType='" + getProcessName() + "',nbFilesMoved=" +
				filesMove.size() + ",nbFilesCopied=" + filesCopy.size() + " WHERE idsessionTask=" + taskId + ";";

		SQLConnection conn = SQLConnectionFactory.getConnection();
		Statement stat = conn.getStatement();
		conn.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);
		
		addFilesInElement("copy",filesCopy, taskId);
		addFilesInElement("move",filesMove, taskId);
		
	}
	
	protected static void addFilesInElement(String nameTag, Vector<FileDesc> files, long taskId){
		if (files.size() == 0)
			return;
		
		for (FileDesc file : files) {
			setLocalFile(file, taskId, nameTag);
		}
	}

	/**
	 * @return  the filesCopy
	 * @uml.property  name="filesCopy"
	 */
	public Vector<FileDesc> getFilesCopy() {
		return filesCopy;
	}

	/**
	 * @param filesCopy  the filesCopy to set
	 * @uml.property  name="filesCopy"
	 */
	public void setFilesCopy(Vector<FileDesc> filesCopy) {
		this.filesCopy = filesCopy;
	}

	/**
	 * @return  the filesMove
	 * @uml.property  name="filesMove"
	 */
	public Vector<FileDesc> getFilesMove() {
		return filesMove;
	}

	/**
	 * @param filesMove  the filesMove to set
	 * @uml.property  name="filesMove"
	 */
	public void setFilesMove(Vector<FileDesc> filesMove) {
		this.filesMove = filesMove;
	}
	
	
	
	
}
