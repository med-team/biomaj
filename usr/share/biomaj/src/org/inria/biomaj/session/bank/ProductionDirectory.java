/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.session.bank;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajUtils;

public class ProductionDirectory implements Comparable<ProductionDirectory> {

	/** Const definition */
	public static final int REMOVE     = 0;
	public static final int AVAILABLE  = 1;
	public static final String REMOVE_STR = "deleted";
	public static final String AVAILABLE_STR = "available";
	
	/**
	 * REMOVE or AVAILABLE
	 * @uml.property  name="state"
	 */ 
	private int state = -1;
	/**
	 * Date of creation
	 * @uml.property  name="creationDate"
	 */
	private Date creationDate ;
	/**
	 * Date of destruction
	 * @uml.property  name="removeDate"
	 */
	private Date removeDate ;
	/**
	 * Absolute path
	 * @uml.property  name="path"
	 */
	private String path;
	/**
	 * Session id that create the prod dir
	 * @uml.property  name="session"
	 */
	private long session;
	
	/**
	 * Total size of prod dir
	 * @uml.property  name="size"
	 */
	private long size;
	
	public ProductionDirectory() {
		
	}
	
	public ProductionDirectory(Map<String, String> directory) {
		setStateStr(directory.get(BiomajSQLQuerier.DIR_STATE));
		if (getState() == ProductionDirectory.REMOVE) {
			try {
				setRemoveDate(BiomajUtils.stringToDate(directory.get(BiomajSQLQuerier.DIR_REMOVE)));
			} catch (ParseException pe) {
				BiomajLogger.getInstance().log(pe);
			}
		}
		setPath(directory.get(BiomajSQLQuerier.DIR_PATH));
		setSession(Long.valueOf(directory.get(BiomajSQLQuerier.DIR_SESSION)));
		setSize(BiomajUtils.stringToSize(directory.get(BiomajSQLQuerier.DIR_SIZE)));
		
		try {
			setCreationDate(BiomajUtils.stringToDate(directory.get(BiomajSQLQuerier.DIR_CREATION)));
		} catch (ParseException pe) {
			BiomajLogger.getInstance().log(pe);
		}
	}
	
	
	/**
	 * @return  the size
	 * @uml.property  name="size"
	 */
	public long getSize() {
		return size;
	}
	/**
	 * @param size  the size to set
	 * @uml.property  name="size"
	 */
	public void setSize(long size) {
		this.size = size;
	}
	/**
	 * @return  the creationDate
	 * @uml.property  name="creationDate"
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	/**
	 * @param creationDate  the creationDate to set
	 * @uml.property  name="creationDate"
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	/**
	 * @return  the path
	 * @uml.property  name="path"
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @param path  the path to set
	 * @uml.property  name="path"
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * @return  the removeDate
	 * @uml.property  name="removeDate"
	 */
	public Date getRemoveDate() {
		return removeDate;
	}
	/**
	 * @param removeDate  the removeDate to set
	 * @uml.property  name="removeDate"
	 */
	public void setRemoveDate(Date removeDate) {
		this.removeDate = removeDate;
	}
	/**
	 * @return  the state
	 * @uml.property  name="state"
	 */
	public int getState() {
		return state;
	}
	/**
	 * @param state  the state to set
	 * @uml.property  name="state"
	 */
	public void setState(int state) {
		this.state = state;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		
		ProductionDirectory pd;
		if (obj instanceof ProductionDirectory)
			pd = (ProductionDirectory)obj;
		else
			return false;
		
		if (pd == null)
			return false;
		
		if ((pd.path==null)||(this.path==null))
			return false;
		
		return ((pd.path.compareTo(this.path)==0)&&(pd.state==this.state));
		
	}
	
	public String getStateStr() {
		switch (state) {
		case AVAILABLE : return AVAILABLE_STR;
		case REMOVE    : return REMOVE_STR;
		}
		return "";
	}
	
	public void setStateStr(String state) {
		if (AVAILABLE_STR.compareTo(state)==0)
			this.state = AVAILABLE;
		else if (REMOVE_STR.compareTo(state)==0)
			this.state = REMOVE;
	}
	
	public int compareTo(ProductionDirectory o) {
		if (creationDate.getTime()<o.getCreationDate().getTime())
			return -1;
		else if (creationDate.getTime()==o.getCreationDate().getTime())
			return 0;
		return 1;
	}
	/**
	 * @return  the session
	 * @uml.property  name="session"
	 */
	public long getSession() {
		return session;
	}
	/**
	 * @param session  the session to set
	 * @uml.property  name="session"
	 */
	public void setSession(long session) {
		this.session = session;
	}	
	
}
