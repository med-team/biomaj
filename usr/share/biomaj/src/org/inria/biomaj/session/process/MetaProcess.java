/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.session.process;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * @author  ofilangi
 * @version  Biomaj 0.9
 * @since  Biomaj 0.8 /Citrina 0.5
 */
public class MetaProcess {

	/**
	 * @uml.property  name="name"
	 */
	private String name;

	private Vector<BiomajProcess> listProcess = new Vector<BiomajProcess>();

//	Dynamics data
	/**
	 * @uml.property  name="start"
	 */
	private Date start = null;

	/**
	 * @uml.property  name="end"
	 */
	private Date end  = null;

	/**
	 * @uml.property  name="logFile"
	 */
	private String logFile = "";

	private Vector<String> warn = new Vector<String>();

	private Vector<String> err = new Vector<String>();


	/**
	 * @uml.property  name="block"
	 */
	private String block="";

	/**
	 * @return  the err
	 * @uml.property  name="err"
	 */
	public Vector<String> getErr() {
		return err;
	}

	/**
	 * @param err  the err to set
	 * @uml.property  name="err"
	 */
	public void setErr(Vector<String> err) {
		this.err = err;
	}

	/**
	 * @return  the warn
	 * @uml.property  name="warn"
	 */
	public Vector<String> getWarn() {
		return warn;
	}

	/**
	 * @param warn  the warn to set
	 * @uml.property  name="warn"
	 */
	public void setWarn(Vector<String> warn) {
		this.warn = warn;
	}

	/**
	 * @return  the end
	 * @uml.property  name="end"
	 */
	public Date getEnd() {
		return end;
	}

	/**
	 * @param end  the end to set
	 * @uml.property  name="end"
	 */
	public void setEnd(Date end) {
		this.end = end;
	}

	/**
	 * @return  the start
	 * @uml.property  name="start"
	 */
	public Date getStart() {
		return start;
	}

	/**
	 * @param start  the start to set
	 * @uml.property  name="start"
	 */
	public void setStart(Date start) {
		this.start = start;
	}

	/**
	 * @return  the listProcess
	 * @uml.property  name="listProcess"
	 */
	public Vector<BiomajProcess> getListProcess() {
		return listProcess;
	}

	/**
	 * @param listProcess  the listProcess to set
	 * @uml.property  name="listProcess"
	 */
	public void setListProcess(Vector<BiomajProcess> listProcess) {
		this.listProcess = listProcess;
	}

	/**
	 * @return  the name
	 * @uml.property  name="name"
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name  the name to set
	 * @uml.property  name="name"
	 */
	public void setName(String name) {
		this.name = name;
	}

	public String getStatusStr() {
		for (BiomajProcess p: getListProcess()) {
			if ((p.getReturnValue()!=0)||(p.isErrorDetected()))
				return "ko";
		}
		return "ok";
	}
	
	public void fill(Map<String, String> metaprocess) {
		if (metaprocess == null) return;
		
		setName(metaprocess.get(BiomajSQLQuerier.META_NAME));
		try {
			setStart(BiomajUtils.stringToDate(metaprocess.get(BiomajSQLQuerier.META_START)));
			setStart(BiomajUtils.stringToDate(metaprocess.get(BiomajSQLQuerier.META_END)));
		} catch (ParseException pe) {
			BiomajLogger.getInstance().log(pe);
			if (getStart() == null) {
				setStart(new Date());
			}
			if (getEnd() == null) {
				setEnd(new Date());
			}
		}
		
		setBlock(metaprocess.get(BiomajSQLQuerier.BLOCK));
		setLogFile(metaprocess.get(BiomajSQLQuerier.META_LOGFILE));
		
		List<List<String>> messages = BiomajSQLQuerier.getMetaprocessMessages(metaprocess.get(BiomajSQLQuerier.META_ID));
		warn.addAll(messages.get(0));
		err.addAll(messages.get(1));
		
		List<Map<String, String>> processes = BiomajSQLQuerier.getMetaprocessProcesses(metaprocess.get(BiomajSQLQuerier.META_ID));
		for (Map<String, String> process : processes) {
			BiomajProcess p  =  new BiomajProcess() ;
			p.fill(process);
			listProcess.add(p);
		}
		
	}

	/**
	 * @return  the logFile
	 * @uml.property  name="logFile"
	 */
	public String getLogFile() {
		return logFile;
	}

	/**
	 * @param logFile  the logFile to set
	 * @uml.property  name="logFile"
	 */
	public void setLogFile(String logFile) {
		this.logFile = logFile;
	}

	/**
	 * @return  the block
	 * @uml.property  name="block"
	 */
	public String getBlock() {
		return block;
	}

	/**
	 * @param block  the block to set
	 * @uml.property  name="block"
	 */
	public void setBlock(String block) {
		this.block = block;
	}
}
