/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.session.bank;

import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.inria.biomaj.sql.SQLConnection;
import org.inria.biomaj.sql.SQLConnectionFactory;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;

public class CheckTask extends GeneralWorkflowTask {

	public CheckTask(Session s) {
		super(s);
	}
	/**
	 * @uml.property  name="nbFilesExtract"
	 */
	private int nbFilesExtract      ;
	/**
	 * @uml.property  name="nbFilesLocalOnline"
	 */
	private int nbFilesLocalOnline  ;
	/**
	 * @uml.property  name="nbFilesLocalOffline"
	 */
	private int nbFilesLocalOffline ;
	/**
	 * @uml.property  name="nbFilesDownload"
	 */
	private int nbFilesDownload      ;

	private Vector<FileDesc> files = new Vector<FileDesc>();
	
	/**
	 * 
	 * @param location
	 * @return Added file
	 * @throws BiomajException
	 */
	public FileDesc addFile(String location) throws BiomajException {
		FileDesc f = new FileDesc(location,false);
		this.files.add(f);
		return f;
	}
	
	
	/**
	 * @return  the nbFilesDownload
	 * @uml.property  name="nbFilesDownload"
	 */
	public int getNbFilesDownload() {
		return nbFilesDownload;
	}
	/**
	 * @param nbFilesDownload  the nbFilesDownload to set
	 * @uml.property  name="nbFilesDownload"
	 */
	public void setNbFilesDownload(int nbFileDownload) {
		this.nbFilesDownload = nbFileDownload;
	}
	/**
	 * @return  the nbFilesExtract
	 * @uml.property  name="nbFilesExtract"
	 */
	public int getNbFilesExtract() {
		return nbFilesExtract;
	}
	/**
	 * @param nbFilesExtract  the nbFilesExtract to set
	 * @uml.property  name="nbFilesExtract"
	 */
	public void setNbFilesExtract(int nbFilesExtract) {
		this.nbFilesExtract = nbFilesExtract;
	}
	/**
	 * @return  the nbFilesLocalOffline
	 * @uml.property  name="nbFilesLocalOffline"
	 */
	public int getNbFilesLocalOffline() {
		return nbFilesLocalOffline;
	}
	/**
	 * @param nbFilesLocalOffline  the nbFilesLocalOffline to set
	 * @uml.property  name="nbFilesLocalOffline"
	 */
	public void setNbFilesLocalOffline(int nbFilesLocalOffline) {
		this.nbFilesLocalOffline = nbFilesLocalOffline;
	}
	/**
	 * @return  the nbFilesLocalOnline
	 * @uml.property  name="nbFilesLocalOnline"
	 */
	public int getNbFilesLocalOnline() {
		return nbFilesLocalOnline;
	}
	/**
	 * @param nbFilesLocalOnline  the nbFilesLocalOnline to set
	 * @uml.property  name="nbFilesLocalOnline"
	 */
	public void setNbFilesLocalOnline(int nbFilesLocalOnline) {
		this.nbFilesLocalOnline = nbFilesLocalOnline;
	}
	
	@Override
	public void fill(Map<String, String> task) {
		super.fill(task);
		nbFilesDownload = Integer.valueOf(task.get(BiomajSQLQuerier.NB_DOWNLOADED_FILES));
		nbFilesExtract = Integer.valueOf(task.get(BiomajSQLQuerier.NB_EXTRACT));
		nbFilesLocalOffline = Integer.valueOf(task.get(BiomajSQLQuerier.NB_LOCAL_OFFLINE_FILES));
		
		if (loadMessages) { // If we dont look for performance then we can load the files.
			List<Map<String, String>> filz = BiomajSQLQuerier.getTaskFiles(Integer.valueOf(task.get(BiomajSQLQuerier.TASK_ID)));
			addFilesInVector(filz, files);
		}
	}
	
	@Override
	public String getProcessName() {
		return BiomajConst.checkTag;
	}
	
	@Override
	public void fillElement(long taskId) {

		String query = "UPDATE sessionTask SET nbExtract=" + nbFilesExtract + ",nbLocalOnlineFiles=" +
				nbFilesLocalOnline + ",nbLocalOfflineFiles=" + nbFilesLocalOffline + ",nbDownloadFiles=" +
				nbFilesDownload + ",taskType='" + getProcessName() +  "' WHERE idsessionTask=" + taskId + ";";
		
		SQLConnection conn = SQLConnectionFactory.getConnection();
		Statement stat = conn.getStatement();
		conn.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);
		
		addFilesInDB(files, taskId);
	}


}
