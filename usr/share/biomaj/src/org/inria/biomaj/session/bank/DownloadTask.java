/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.session.bank;

import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.inria.biomaj.sql.SQLConnection;
import org.inria.biomaj.sql.SQLConnectionFactory;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;

/**
 * @author  ofilangi
 * @version  Biomaj 0.9
 * @since  Biomaj 0.8 /Citrina 0.5
 */
public class DownloadTask extends GeneralWorkflowTask {
	
	public DownloadTask(Session s) {
		super(s);
	}

	private Vector<FileDesc> files = new Vector<FileDesc>();
	
	/**
	 * @uml.property  name="bandWidth"
	 */
	private double bandWidth = 0;
	
	public void addFile(String location) throws BiomajException {
		FileDesc f = new FileDesc(location,true);
		this.files.add(f);
	}
	
	@Override
	public void fill(Map<String, String> task) {
		super.fill(task);
		String val = task.get(BiomajSQLQuerier.BANDWIDTH);
		
		if (!val.isEmpty())
			bandWidth = Double.valueOf(val);
		
		if (loadMessages) { // If we dont look for performance then we can load the files.
			List<Map<String, String>> filz = BiomajSQLQuerier.getTaskFiles(Integer.valueOf(task.get(BiomajSQLQuerier.TASK_ID)));
			addFilesInVector(filz, files);
		}
	}


	@Override
	public String getProcessName() {
		return BiomajConst.downloadTag;
	}


	@Override
	public void fillElement(long taskId) {
		String query = "UPDATE sessionTask SET taskType='" + getProcessName() + "',bandwidth=" + bandWidth + " WHERE " +
				"idsessionTask=" + taskId + ";";

		
		SQLConnection conn = SQLConnectionFactory.getConnection();
		Statement stat = conn.getStatement();
		conn.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);
		
		addFilesInDB(files, taskId);
	}


	/**
	 * @return  the bandWidth
	 * @uml.property  name="bandWidth"
	 */
	public double getBandWidth() {
		return bandWidth;
	}
	
	/**
	 * @param bandWidth  the bandWidth to set
	 * @uml.property  name="bandWidth"
	 */
	public void setBandWidth(double bandWidth) {
		this.bandWidth = bandWidth;
	}


	/**
	 * @return  the files
	 * @uml.property  name="files"
	 */
	public Vector<FileDesc> getFiles() {
		return files;
	}
	
	public long getSizeDownloaded() {
		long count = 0;
		for (FileDesc f : files) 
			count+=f.getSize();
		return count;
	}
	
}
