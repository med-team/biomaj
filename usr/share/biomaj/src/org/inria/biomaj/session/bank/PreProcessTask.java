/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.session.bank;

import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.inria.biomaj.session.process.MetaProcess;
import org.inria.biomaj.sql.SQLConnection;
import org.inria.biomaj.sql.SQLConnectionFactory;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;

public class PreProcessTask extends GeneralWorkflowTask {

	protected Vector<String>  metaProcessName         = new Vector<String> ();
	protected Vector<String>  metaProcessblock        = new Vector<String> ();

	public PreProcessTask(Session s) {
		super(s);
	}

	public void addMetaProcess(String name,String block) {
		metaProcessName.add(name);
		metaProcessblock.add(block);
	}
	
	@Override
	public void fill(Map<String, String> task) {
		super.fill(task);

		List<Map<String, String>> metas = BiomajSQLQuerier.getTaskMetaprocesses(Integer.valueOf(task.get(BiomajSQLQuerier.TASK_ID)));
		for (Map<String, String> meta : metas) {
			metaProcessName.add(meta.get(BiomajSQLQuerier.META_NAME));
			metaProcessblock.add(meta.get(BiomajSQLQuerier.BLOCK));
		}
	}

	@Override
	public String getProcessName() {
		return BiomajConst.preProcessTag;
	}

	@Override
	public void fillElement(long taskId) {
		
		String query = "UPDATE sessionTask SET nbreMetaProcess=" + metaProcessName.size() +
				",taskType='" + getProcessName() + "' WHERE idsessionTask=" + taskId;
		
		SQLConnection conn = SQLConnectionFactory.getConnection();

		Statement stat = conn.getStatement();
		conn.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);
		
		/*
		for (int i=0; i< metaProcessName.size(); i++) {

			String id = getSession().getId() + "." + metaProcessName.get(i);
			query = "SELECT * FROM metaprocess WHERE idmetaprocess='" + id + "';";
			stat = conn.getStatement();
			
			try {
				ResultSet rs = conn.executeQuery(query, stat);
				if (!rs.next()) {
					query = "INSERT INTO metaprocess(idmetaprocess, ref_idsessionTask, startTime, endTime) " +
							"values ('" + id + "'," + taskId + ",'" + BiomajUtils.dateToString(new Date(), Locale.US) +
							"','" + BiomajUtils.dateToString(new Date(), Locale.US) + "');";
					
					Statement stat2 = conn.getStatement();
					conn.executeUpdate(query, stat2);
					SQLConnectionFactory.closeConnection(stat2);
					DBWriter.addMetaId(id);
				}
			} catch (SQLException e) {
				BiomajLogger.getInstance().log(e);
			}
			SQLConnectionFactory.closeConnection(stat);
		}*/
		
	}


	public String getNameProcess(int i) {
		if (i<metaProcessName.size())
			return metaProcessName.get(i);

		return null;
	}

	public int getNbMetaProcess() {
		return metaProcessName.size();
	}

	public Vector<MetaProcess> getMetaProcess(String block) throws BiomajException {
		Vector<MetaProcess> res = new Vector<MetaProcess>();

		if (getSession().getBank().getConfig()== null)
			System.err.println("error dev: config is not set : PreProcessTask::getMetaProcess");
		
		List<Map<String, String>> metas = BiomajSQLQuerier.getTaskMetaprocesses(getTaskId());
		if (block == null || block.trim().isEmpty()) {
			for (Map<String, String> meta : metas) {
				MetaProcess mp = new MetaProcess();
				mp.fill(meta);
				res.add(mp);
			}
		} else {
			for (Map<String, String> meta : metas) {
				if (meta.get(BiomajSQLQuerier.BLOCK).equals(block)) {
					MetaProcess mp = new MetaProcess();
					mp.fill(meta);
					res.add(mp);
				}
			}
		}
		
		return res;
	}
}
