/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.session.bank;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;
import org.inria.biomaj.session.process.BiomajProcess;
import org.inria.biomaj.session.process.MetaProcess;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;


/**
 * @author  ofilangi
 * @version  Biomaj 0.9
 * @since  Biomaj 0.8 /Citrina 0.5
 */
public class Session implements Comparable<Session>{

	public static final int PREPROCESS = 0 ;
	public static final int RELEASE    = 1 ;
	public static final int CHECK      = 2 ;
	public static final int DOWNLOAD   = 3 ;
	public static final int EXTRACT    = 4 ;
	public static final int COPY       = 5 ;
	public static final int MOVE       = 6 ;
	public static final int POSTPROCESS= 7 ;
	public static final int DEPLOYMENT = 8 ;
	public static final int REMOVEPROCESS= 9 ;

	/**
	 * @uml.property  name="_bank"
	 * @uml.associationEnd  multiplicity="(1 1)" inverse="currentSession:org.inria.biomaj.session.bank.Bank"
	 */
	private Bank _bank ;

	public Session(Bank bank) {
		_bank = bank;
	}


	public static GeneralWorkflowTask createWorkflowTask(int proc,Session s) {

		switch(proc) {
		case Session.PREPROCESS :
			return new PreProcessTask(s);
		case Session.RELEASE:
			return new ReleaseTask(s);
		case Session.CHECK:
			return new CheckTask(s);
		case Session.COPY:
			return new AddLocalFilesTask(s);
		case Session.DOWNLOAD:
			return new DownloadTask(s);
		case Session.EXTRACT:
			return new ExtractTask(s);
		case Session.MOVE:
			return new MakeReleaseTask(s);
		case Session.POSTPROCESS:
			return new PostProcessTask(s);
		case Session.REMOVEPROCESS:
			return new RemoveProcessTask(s);
		case Session.DEPLOYMENT:
			return new DeploymentTask(s);
		default:
		{
			BiomajLogger.getInstance().log(String.valueOf(proc)+" unknown value for create a task...");
			//Arret brutale, car erreur de dev!
			System.exit(-1);
			return null;
		}
		}

	}

	public static String getNameTagWorkflowTask(int proc) {

		switch(proc) {
		case Session.PREPROCESS :
			return BiomajConst.preProcessTag;
		case Session.CHECK:
			return BiomajConst.checkTag;
		case Session.COPY:
			return BiomajConst.copyTag;
		case Session.DOWNLOAD:
			return BiomajConst.downloadTag;
		case Session.EXTRACT:
			return BiomajConst.extractTag;
		case Session.MOVE:
			return BiomajConst.moveTag;
		case Session.POSTPROCESS:
			return BiomajConst.postProcessTag;
		case Session.REMOVEPROCESS:
			return BiomajConst.removeProcessTag;
		case Session.RELEASE:
			return BiomajConst.releaseTag;
		case Session.DEPLOYMENT:
			return BiomajConst.deploymentTag;
		default:
			return "unknown tag:"+Integer.toString(proc);
		}

	}

	/**
	 * @uml.property  name="workflowtasks"
	 * @uml.associationEnd  qualifier="valueOf:java.lang.Integer org.inria.biomaj.session.bank.GeneralWorkflowTask"
	 */
	private HashMap<Integer,GeneralWorkflowTask> workflowtasks = new HashMap<Integer,GeneralWorkflowTask>();

	private GeneralWorkflowTask activeTask = null;
	
	
	/**
	 * id session
	 * @uml.property  name="id"
	 */
	private Long id = new Long(-1);

	/**
	 * @uml.property  name="logfile"
	 */
	private String logfile = "";


	/**
	 * @uml.property  name="start"
	 */
	private Date start = new Date();

	/**
	 * @uml.property  name="end"
	 */
	private Date end   = null ;

	/**
	 * @uml.property  name="warn"
	 */
	private Vector<String> warn = new Vector<String>();
	
	private Vector<String> oldWarn = null;
	private Vector<String> oldErr = null;

	/**
	 * @uml.property  name="err"
	 */
	private Vector<String> err = new Vector<String>();

	public void fill(Map<String, String> session, boolean loadMessages) {

		setId(Long.valueOf(session.get(BiomajSQLQuerier.SESSION_ID)));

		try {
			setStart(BiomajUtils.stringToDate(session.get(BiomajSQLQuerier.SESSION_START)));
			String end = session.get(BiomajSQLQuerier.SESSION_END);
			if (end != null && !end.isEmpty())
				setEnd(BiomajUtils.stringToDate(end));
		} catch (ParseException pe) {
			BiomajLogger.getInstance().log(pe);
		}
		setLogfile(session.get(BiomajSQLQuerier.LOG_FILE));
		
		long sessionId = Long.valueOf(session.get(BiomajSQLQuerier.SESSION_ID));
		
		loadWorkflowTasks(sessionId, loadMessages);
		
		if (loadMessages) {
			List<List<String>> messages = BiomajSQLQuerier.getSessionMessages(sessionId);
			
			getWarn().addAll(messages.get(0));
			getErr().addAll(messages.get(1));
		}
	}

	public void fillWithDirectoryVersion(String dir) throws BiomajException {
		setId(BiomajUtils.getUniqueNumericId());
		setStart(new Date());
		setEnd(new Date());
		setLogfile("");

		workflowtasks.put(PREPROCESS, new PreProcessTask(this));
		workflowtasks.get(PREPROCESS).setStart(new Date());
		workflowtasks.get(PREPROCESS).setEnd(new Date());
		workflowtasks.get(PREPROCESS).setStatus(GeneralWorkflowTask.STATUS_OK);

		workflowtasks.put(RELEASE, new ReleaseTask(this));
		workflowtasks.get(RELEASE).setStart(new Date());
		workflowtasks.get(RELEASE).setEnd(new Date());
		workflowtasks.get(RELEASE).setStatus(GeneralWorkflowTask.STATUS_OK);

		workflowtasks.put(CHECK, new CheckTask(this));
		workflowtasks.get(CHECK).setStart(new Date());
		workflowtasks.get(CHECK).setEnd(new Date());
		workflowtasks.get(CHECK).setStatus(GeneralWorkflowTask.STATUS_OK);

		workflowtasks.put(DOWNLOAD, new DownloadTask(this));
		workflowtasks.get(DOWNLOAD).setStart(new Date());
		workflowtasks.get(DOWNLOAD).setEnd(new Date());
		workflowtasks.get(DOWNLOAD).setStatus(GeneralWorkflowTask.STATUS_OK);

		workflowtasks.put(COPY, new AddLocalFilesTask(this));
		workflowtasks.get(COPY).setStart(new Date());
		workflowtasks.get(COPY).setEnd(new Date());
		workflowtasks.get(COPY).setStatus(GeneralWorkflowTask.STATUS_OK);

		workflowtasks.put(EXTRACT, new ExtractTask(this));
		workflowtasks.get(EXTRACT).setStart(new Date());
		workflowtasks.get(EXTRACT).setEnd(new Date());
		workflowtasks.get(EXTRACT).setStatus(GeneralWorkflowTask.STATUS_OK);

		workflowtasks.put(POSTPROCESS, new PostProcessTask(this));
		workflowtasks.get(POSTPROCESS).setStart(new Date());
		workflowtasks.get(POSTPROCESS).setEnd(new Date());
		workflowtasks.get(POSTPROCESS).setStatus(GeneralWorkflowTask.STATUS_OK);

//		workflowtasks.put(REMOVEPROCESS, new RemoveProcessTask(this));
//		workflowtasks.get(REMOVEPROCESS).setStart(new Date());
//		workflowtasks.get(REMOVEPROCESS).setEnd(new Date());
//		workflowtasks.get(REMOVEPROCESS).setStatus(GeneralWorkflowTask.STATUS_OK);

		workflowtasks.put(MOVE, new MakeReleaseTask(this));
		workflowtasks.get(MOVE).setStart(new Date());
		workflowtasks.get(MOVE).setEnd(new Date());
		workflowtasks.get(MOVE).setStatus(GeneralWorkflowTask.STATUS_OK);
		Vector<FileDesc> result = new Vector<FileDesc>();
		BiomajUtils.getListFilesFromDir(dir,result);
		((MakeReleaseTask)workflowtasks.get(MOVE)).setFilesCopy(result);

		workflowtasks.put(DEPLOYMENT, new DeploymentTask(this));
		workflowtasks.get(DEPLOYMENT).setStart(new Date());
		workflowtasks.get(DEPLOYMENT).setEnd(new Date());
		workflowtasks.get(DEPLOYMENT).setStatus(GeneralWorkflowTask.STATUS_OK);

	}

	/**
	 * @return  the err
	 * @uml.property  name="err"
	 */
	public Vector<String> getErr() {
		return err;
	}
	
	public Vector<String> getNewErr() {
		if (oldErr == null) {
			oldErr = new Vector<String>();
			oldErr.addAll(err);
			
			return err;
		}
		
		Vector<String> res = new Vector<String>();
		
		if (oldErr.size() == err.size()) // Nothing new
			return res;
		else if (err.size() > oldErr.size()) {
			for (String s : err) {
				boolean found = false;
				for (String sOld : oldErr) {
					if (s == sOld) {
						found = true;
						break;
					}
				}
				if (!found) {
					oldErr.add(s);
					res.add(s);
				}
			}
		} else {
			throw new RuntimeException("Somehow, some message have been deleted since the previous call.");
		}
		
		return res;
	}

	/**
	 * @param err  the err to set
	 * @uml.property  name="err"
	 */
	public void setErr(Vector<String> err) {
		this.err = err;
	}

	/**
	 * @return  the warn
	 * @uml.property  name="warn"
	 */
	public Vector<String> getWarn() {
		return warn;
	}
	
	/**
	 * Returns the new messages since the last call to this method.
	 * 
	 * @return
	 */
	public Vector<String> getNewWarn() {
		if (oldWarn == null) {
			oldWarn = new Vector<String>();
			oldWarn.addAll(warn);
			
			return warn;
		}
		
		Vector<String> res = new Vector<String>();
		
		if (oldWarn.size() == warn.size()) // Nothing new
			return res;
		else if (warn.size() > oldWarn.size()) {
			for (String s : warn) {
				boolean found = false;
				for (String sOld : oldWarn) {
					if (s == sOld) {
						found = true;
						break;
					}
				}
				if (!found) {
					oldWarn.add(s);
					res.add(s);
				}
			}
		} else {
			throw new RuntimeException("Somehow, some message have been deleted since the previous call.");
		}
		
		return res;
	}

	/**
	 * @param warn  the warn to set
	 * @uml.property  name="warn"
	 */
	public void setWarn(Vector<String> warn) {
		this.warn = warn;
	}


	/**
	 * Getter of the property <tt>id</tt>
	 * @return  Returns the id.
	 * @uml.property  name="id"
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Getter of the property <tt>logfile</tt>
	 * @return  Returns the logfile.
	 * @uml.property  name="logfile"
	 */
	public String getLogfile() {
		return logfile;
	}


	public boolean getStatus() {

		if (workflowtasks.size()==0)
			return false;

		Collection<GeneralWorkflowTask> l = workflowtasks.values();

		for (GeneralWorkflowTask t : l) 
			if ((t.getStatus()==GeneralWorkflowTask.STATUS_KO))
				return false;

		return true;
	}

	public boolean taskAreSaveInWorkflow(int proc) {
		return workflowtasks.containsKey(proc);
	}

	public GeneralWorkflowTask getWorkflowTask(int process) {
		if (workflowtasks.containsKey(process))
			return workflowtasks.get(process);

		return null;
	}

	public GeneralWorkflowTask getLastTask() {
		for (int i=Session.REMOVEPROCESS;i>=Session.PREPROCESS;i--) {
			if (workflowtasks.containsKey(i)) {
				return workflowtasks.get(i);
			}
		}
		return null;
	}

	public HashMap<Integer,GeneralWorkflowTask> getWorkflowTasks() {
		return workflowtasks;
	}
	
	public void setWorkflowTasks(HashMap<Integer,GeneralWorkflowTask> in) {
		workflowtasks = in;
	}
	
	/**
	 * Retrieves and initializes the tasks of a given session.
	 * 
	 * @param sessionId
	 */
	protected void loadWorkflowTasks(long sessionId, boolean loadMessages) {

		List<Map<String, String>> tasks = BiomajSQLQuerier.getSessionTasks(sessionId);
		for (Map<String, String> task : tasks) {
			String taskType = task.get(BiomajSQLQuerier.TASK_TYPE);
			GeneralWorkflowTask wt = null;
			
			if (taskType.equals(BiomajConst.preProcessTag)) {
				wt = new PreProcessTask(this);
				workflowtasks.put(PREPROCESS, wt);
			} else if (taskType.equals(BiomajConst.checkTag)) {
				wt = new CheckTask(this);
				workflowtasks.put(CHECK, wt);
			} else if (taskType.equals(BiomajConst.copyTag)) {
				wt = new AddLocalFilesTask(this);
				workflowtasks.put(COPY, wt);
			} else if (taskType.equals(BiomajConst.downloadTag)) {
				wt = new DownloadTask(this);
				workflowtasks.put(DOWNLOAD, wt);
			} else if (taskType.equals(BiomajConst.extractTag)) {
				wt = new ExtractTask(this);
				workflowtasks.put(EXTRACT, wt);
			} else if (taskType.equals(BiomajConst.moveTag)) {
				wt = new MakeReleaseTask(this);
				workflowtasks.put(MOVE, wt);
			} else if (taskType.equals(BiomajConst.postProcessTag)) {
				wt = new PostProcessTask(this);
				workflowtasks.put(POSTPROCESS, wt);
			} else if (taskType.equals(BiomajConst.removeProcessTag)) {
				wt = new RemoveProcessTask(this);
				workflowtasks.put(REMOVEPROCESS, wt);
			} else if (taskType.equals(BiomajConst.releaseTag)) {
				wt = new ReleaseTask(this);
				workflowtasks.put(RELEASE, wt);
			} else if (taskType.equals(BiomajConst.deploymentTag)) {
				wt = new DeploymentTask(this);
				workflowtasks.put(DEPLOYMENT, wt);
			}
			wt.setLoadMessages(loadMessages);
			wt.fill(task);
		}
	}


	/**
	 * Setter of the property <tt>id</tt>
	 * @param id  The id to set.
	 * @uml.property  name="id"
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * Setter of the property <tt>logfile</tt>
	 * @param logfile  The logfile to set.
	 * @uml.property  name="logfile"
	 */
	public void setLogfile(String logfile) {
		this.logfile = logfile;
	}


	public GeneralWorkflowTask setWorkflowTask(int process) {
		//Ajout le 16/01/2007 
		if (workflowtasks.containsKey(process))
			return workflowtasks.get(process);


		GeneralWorkflowTask p =  createWorkflowTask(process,this);

		workflowtasks.put(new Integer(process), p);
		return p;
	}

	public boolean workflowTaskExist(int task) {

		if (workflowtasks.containsKey(task))
			return true;

		return false;
	}

	/**
	 * @return  the end
	 * @uml.property  name="end"
	 */
	public Date getEnd() {
		return end;
	}

	/**
	 * @param end  the end to set
	 * @uml.property  name="end"
	 */
	public void setEnd(Date end) {
		this.end = end;
	}

	/**
	 * @return  the start
	 * @uml.property  name="start"
	 */
	public Date getStart() {
		return start;
	}

	/**
	 * @param start  the start to set
	 * @uml.property  name="start"
	 */
	public void setStart(Date start) {
		this.start = start;
	}

	public int compareTo(Session o) {
		if (getStart()==null)
			return -1;

		if (o.getStart()==null)
			return -1;

		if (getStart().getTime()<o.getStart().getTime())
			return -1;
		if (getStart().getTime()==o.getStart().getTime())
			return 0;

		return 1;
	}


	public Vector<String> getMessageErrorOnSession() throws BiomajException,ParseException {
		Vector<String> res = new Vector<String>();
		
		if (getEnd() == null)
			setEnd(new Date());
		
		res.add("SESSION (start:"+BiomajUtils.dateToString(getStart(), Locale.US)+" - end:"+BiomajUtils.dateToString(getEnd(), Locale.US)+")\n time:"+BiomajUtils.timeToString(getEnd().getTime() - getStart().getTime()));		
		res.add("--------");
		res.add("LOG:"+getLogfile());
		res.add("----------- GLOBAL ERROR  -----------------------");
		for (String s : getErr()) {
			res.add("ERROR : "+s);
		}
		if ( getErr().size()==0)
			res.add("* NONE *");
		res.add("----------------------------------------");
		res.add("");
		res.add("----------- GLOBAL WARNING --------------------");
		for (String s : getWarn()) {
			res.add("WARNING : "+s);
		}
		if ( getWarn().size()==0)
			res.add("* NONE *");
		res.add("");
		res.add("----------- ERROR ON SUB-TASK -----------------------");
		for ( Integer i : workflowtasks.keySet() ) {
			GeneralWorkflowTask g = workflowtasks.get(i);
			if (g.getEnd() == null)
				g.setEnd(new Date());
			
			 /*"[start:"+BiomajUtils.dateToString(g.getStart())+" - end:"+BiomajUtils.dateToString(g.getEnd())+*/
			String d ="[time:"+BiomajUtils.timeToString(g.getEnd().getTime() - g.getStart().getTime())+"]";
			
			res.add("            *** "+g.getProcessName()+" *** "+d);
			
			for (String s : g.getErr())
				res.add("ERROR:    "+getNameTagWorkflowTask(i)+":"+s);
			//PostProcess est un descendant de PreProcess, donc cette ligne suffit....
			if (g instanceof PreProcessTask) {
				PreProcessTask pp = (PreProcessTask) g;
				Vector<MetaProcess> lmp = null;
				lmp = pp.getMetaProcess(null);

				for (MetaProcess mp : lmp) {
					for (BiomajProcess bp : mp.getListProcess()) {
						for (String e : bp.getErr())
							res.add("ERROR:    "+mp.getName()+"::"+bp.getKeyName()+" : " + e);
					}
					for (String s : mp.getErr()) {
						res.add("ERROR:    "+mp.getName()+":"+s);
					}
				}
			}

			for (String s : g.getWarn())
				res.add("WARNING:  "+getNameTagWorkflowTask(i)+":"+s);
			//PostProcess est un descendant de PreProcess, donc cette ligne suffit....
			if (g instanceof PreProcessTask) {
				PreProcessTask pp = (PreProcessTask) g;
				Vector<MetaProcess> lmp = null;
				lmp = pp.getMetaProcess(null);
				for (MetaProcess mp : lmp) {
					for (BiomajProcess bp : mp.getListProcess()) {
						for (String e : bp.getWarn())
							res.add("WARNING:  "+mp.getName()+"::"+bp.getKeyName()+" : " + e);
					}
					for (String s : mp.getWarn()) {
						res.add("WARNING:  "+mp.getName()+":"+s);
					}
				}
			}
		}
		/*
		res.add("----------------------------------------");
		res.add("");
		res.add("----------- WARNING ON SUB-TASK --------------------");
		for ( Integer i : workflowtasks.keySet() ) {
			GeneralWorkflowTask g = workflowtasks.get(i);
			res.add("            *** "+g.getProcessName()+" ***");
			for (String s : g.getWarn())
				res.add(getNameTagWorkflowTask(i)+":"+s);
			//PostProcess est un descendant de PreProcess, donc cette ligne suffit....
			if (g instanceof PreProcessTask) {
				PreProcessTask pp = (PreProcessTask) g;
				Vector<MetaProcess> lmp = null;

				if (g instanceof PostProcessTask) 
					lmp = pp.getMetaProcess(true);
				else 
					lmp = pp.getMetaProcess(false);
				for (MetaProcess mp : lmp) {
					for (BiomajProcess bp : mp.getListProcess()) {
						for (String e : bp.getWarn())
						res.add(mp.getName()+"::"+bp.getKeyName()+" : " + e);
					}
					for (String s : mp.getWarn()) {
						res.add(mp.getName()+":"+s);
					}
				}
			}
		}*/
		res.add("----------------------------------------");
		return res;
	}


	public Bank getBank() {
		return _bank;
	}


	public GeneralWorkflowTask getActiveTask() {
		return activeTask;
	}
	
	public void setActiveTask(GeneralWorkflowTask task) {
		activeTask = task;
	}

}

