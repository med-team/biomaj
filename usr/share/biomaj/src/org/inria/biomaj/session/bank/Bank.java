/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.session.bank;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.task.BmajVersionManagement;
import org.inria.biomaj.internal.ant.task.net.RemoteFile;
import org.inria.biomaj.session.process.MetaProcess;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajConst;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * @author  ofilangi  12/03/2007 : Bug vider la liste listeSession avant de la remplir dans l methode fill
 */
public class Bank {

	/**
	 * @uml.property  name="id"
	 */
	private Long id ;

	/**
	 * @uml.property  name="config"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private Configuration config  = new Configuration();

	/**
	 * @uml.property  name="start"
	 */
	private Date start = null ;

	/**
	 * @uml.property  name="end"
	 */
	private Date end   = null ;

	private Vector<Session> listOldSession = new Vector<Session>();

	/**
	 * @uml.property  name="currentSession"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private Session currentSession = new Session(this);

	/**
	 * @uml.property  name="update"
	 */
	private boolean update = false ;
	
	/**
	 * @uml.property  name="remove"
	 */
	private boolean remove = false ;
	
	/**
	 * @uml.property  name="removeOk"
	 */
	private boolean removeOk = true ;

	//Resultat du workflow
	//--------------------
	/**
	 * @uml.property  name="workflowInfoProductionDir"
	 */
	private String workflowInfoProductionDir;
	/**
	 * @uml.property  name="workflowInfoIsDeployed"
	 */
	private Boolean workflowInfoIsDeployed = false;
	/**
	 * @uml.property  name="workflowInfoRelease"
	 */
	private String workflowInfoRelease;
	/**
	 * @uml.property  name="workflowInfoSizeRelease"
	 */
	private long workflowInfoSizeRelease;
	/**
	 * @uml.property  name="workflowInfoSizeDownload"
	 */
	private long workflowInfoSizeDownload;
	/**
	 * @uml.property  name="workflowInfoRemovedRelease"
	 */
	private String workflowInfoRemovedRelease;
	
	private boolean isRebuilt = false;

	//Etat de la banque en prod
	//-------------------------
	private Vector<ProductionDirectory> bankStateListProductionDirectories = new Vector<ProductionDirectory>();
	
	private List<Map<String, String>> allExtractedFiles = null;
	
	/**
	 * Speed up the bank loading if disabled.
	 */
	private boolean loadMessages = true;


	/**
	 * @uml.property  name="errorOnWorkflow"
	 */
	private Boolean errorOnWorkflow = false;

	public Bank() {	
		setId(BiomajUtils.getUniqueNumericId());
		currentSession.setId(BiomajUtils.getUniqueNumericId());
		//listSession.add(session);
	}

	public void setRelease(String release) {
		setWorkflowInfoRelease(release);
	}

	public void setOnlineDirectory(String online) {
		setWorkflowInfoProductionDir(online);
	}


	public void setLogFile(String logfile) {
		currentSession.setLogfile(logfile);
	}

	public String getLogFile() {
		return currentSession.getLogfile();
	}

	public void setBankSize(Long id) {
		currentSession.setId(id);
	}

	public void setStartProcess(String process) {
		currentSession.setWorkflowTask(getIdProcess(process));
		GeneralWorkflowTask proc = currentSession.getWorkflowTask(getIdProcess(process));
		proc.setStart(new Date());
		proc.setStatus(GeneralWorkflowTask.STATUS_KO);
	}

	public GeneralWorkflowTask setEndProcess(String process) throws BuildException {
		GeneralWorkflowTask proc = currentSession.getWorkflowTask(getIdProcess(process));
		if (proc == null)
			throw new BuildException ("Can't initialize end process:"+process+" (Process not initialized)");

		proc.setEnd(new Date());
		proc.setStatus(GeneralWorkflowTask.STATUS_OK);
		return proc;
	}

	public GeneralWorkflowTask getProcessRuntime(String process) throws BuildException {
		return currentSession.getWorkflowTask(getIdProcess(process));
	}

	public void addWarnProcess(String process,String warn) {
		GeneralWorkflowTask proc = currentSession.getWorkflowTask(getIdProcess(process));
		if (proc == null)
			currentSession.getWarn().add(warn);
		else
			proc.getWarn().add(warn);
	}

	public void addErrProcess(String process,String err) {
		GeneralWorkflowTask proc = currentSession.getWorkflowTask(getIdProcess(process));
		if (proc == null)
			currentSession.getErr().add(err);
		else
			proc.getErr().add(err);
	}

	public void addFilesInExtraction(String locationUncomp,String refHash) throws BiomajException {
		ExtractTask proc = (ExtractTask)currentSession.getWorkflowTask(Session.EXTRACT);
		proc.addFile(locationUncomp,refHash);
	}

	public void addFilesInProduction(String location,String refHash,boolean copy) throws BiomajException {
		MakeReleaseTask proc = (MakeReleaseTask) currentSession.getWorkflowTask(Session.MOVE);
		proc.addFile(location,refHash,copy);
	}

	public void addDownloadFile(String location,long timeOnRemoteServer,long sizeOnRemoteServer) throws BiomajException {
		DownloadTask proc = (DownloadTask) currentSession.getWorkflowTask(Session.DOWNLOAD);
		proc.setEnd(new Date());
		proc.addFile(location);
	}

	public FileDesc addLocalOfflineFile(String location) throws BiomajException {
		CheckTask proc = (CheckTask) currentSession.getWorkflowTask(Session.CHECK);
		return proc.addFile(location);
	}

	public void addLocalOnlineFile(String location) throws BiomajException {
		AddLocalFilesTask proc = (AddLocalFilesTask) currentSession.getWorkflowTask(Session.COPY);
		proc.addFile(location);
	}

	protected int getIdProcess(String process) {
		if (BiomajConst.preprocessTarget.compareTo(process)==0)
			return Session.PREPROCESS;
		if (BiomajConst.releaseTarget.compareTo(process)==0)
			return Session.RELEASE;
		if (BiomajConst.checkTarget.compareTo(process)==0)
			return Session.CHECK;
		if (BiomajConst.downloadTarget.compareTo(process)==0)
			return Session.DOWNLOAD;
		if (BiomajConst.extractTarget.compareTo(process)==0)
			return Session.EXTRACT;
		if (BiomajConst.copyTarget.compareTo(process)==0)
			return Session.COPY;
		if (BiomajConst.postprocessTarget.compareTo(process)==0)
			return Session.POSTPROCESS;
		if (BiomajConst.moveTarget.compareTo(process)==0)
			return Session.MOVE;
		if (BiomajConst.deployTarget.compareTo(process)==0)
			return Session.DEPLOYMENT;
		if (BiomajConst.removeprocessTarget.compareTo(process)==0)
			return Session.REMOVEPROCESS;

		return -1;
	}

	/**
	 * @return  the currentSession
	 * @uml.property  name="currentSession"
	 */
	public Session getCurrentSession() {
		return currentSession;
	}

	/**
	 * @return  the config
	 * @uml.property  name="config"
	 */
	public Configuration getConfig() {
		return config;
	}

	/**
	 * @param config  the config to set
	 * @uml.property  name="config"
	 */
	public void setConfig(Configuration config) {
		this.config = config;
	}

	public void addProductionDirectory(String path) {
		ProductionDirectory pd = new ProductionDirectory();
		pd.setPath(path);
		pd.setSession(getCurrentSession().getId());
		pd.setState(ProductionDirectory.AVAILABLE);
		pd.setCreationDate(new Date());
		long size = BiomajUtils.computeDirectorySize(path);
		setWorkflowInfoSizeRelease(size);
		pd.setSize(size);
		bankStateListProductionDirectories.add(pd);
		Collections.sort(bankStateListProductionDirectories);



	}

	public void removeProductionDirectory(String path) {

		if (path==null||path.compareTo("")==0)
			return;

		ProductionDirectory temp = new ProductionDirectory();
		temp.setPath(path);
		temp.setState(ProductionDirectory.AVAILABLE);

		int i = bankStateListProductionDirectories.indexOf(temp);

		if (i == -1) {
			BiomajLogger.getInstance().log("Can't remove directory in statefile:["+path+"] (not present inside)");
			return;
		}
		ProductionDirectory pd = bankStateListProductionDirectories.get(i);
		pd.setState(ProductionDirectory.REMOVE);
		pd.setRemoveDate(new Date());
	}

	/**
	 * @return  the bankStateListProductionDirectories
	 * @uml.property  name="bankStateListProductionDirectories"
	 */
	public Vector<ProductionDirectory> getBankStateListProductionDirectories() {
		return bankStateListProductionDirectories;
	}

	/**
	 * @param bankStateListProductionDirectories  the bankStateListProductionDirectories to set
	 * @uml.property  name="bankStateListProductionDirectories"
	 */
	public void setBankStateListProductionDirectories(
			List<ProductionDirectory> listProductionDirectories) {
		this.bankStateListProductionDirectories = new Vector<ProductionDirectory>();
		bankStateListProductionDirectories.addAll(listProductionDirectories);
		Collections.sort(this.bankStateListProductionDirectories);
	}

	/**
	 * Get for oinformation, subtask available for this update
	 * @param process Constante from class Session
	 */
	public GeneralWorkflowTask getAvailableProcess(int process) {
		GeneralWorkflowTask res = null ;
		for (int i=listOldSession.size()-1;i>=0;i--) {
			res = listOldSession.get(i).getWorkflowTask(process);
			if ( res!= null)
				return res;
		}
		return null;
	}


	//pour les pre et post process, le status est contenu a l interieur des sous fichiers xml...
	//la tache globale peut etre a status=ko mais un metaprocess peut s etre bien passe!
	//Cette methode rend l'ensemble des metaProcess qui on un status ok!

	public Collection<MetaProcess> getAvailableMetaProcess(int process) throws BiomajException {
		HashMap<String, MetaProcess> inter = new HashMap<String, MetaProcess>();
		if ((process != Session.POSTPROCESS)&&(process != Session.PREPROCESS)&&(process != Session.REMOVEPROCESS) ) {
			BiomajLogger.getInstance().log("Internal error : Bank::getAllAvailableProcess Can't get MetaProcess from process preprocess/postprocess/removeprocess");
			return new Vector<MetaProcess>();
		}

		listOldSession.add(currentSession);

		for (int i=listOldSession.size()-1;i>=0;i--) {
			GeneralWorkflowTask res = listOldSession.get(i).getWorkflowTask(process);

			if (res == null)
				continue;
			
			if (res instanceof PreProcessTask) {
				PreProcessTask pre = (PreProcessTask) res;
				
				Vector<MetaProcess> value = pre.getMetaProcess(null);

				for (MetaProcess mp : value) {
					
					//System.out.print(mp.getBlock()+":"+mp.getName());
					if (mp.getStatusStr().trim().compareTo("ok")!=0)
						continue;
					//Si deja inserer alors pas besoin de traiter (car on parcours la liste de session a l envers donc le premier entre et le dernier "ok".....)
					if (!inter.containsKey(mp.getBlock()+mp.getName())) {
						inter.put(mp.getBlock()+mp.getName(), mp);
						//System.out.println(" --> ok");
					} else {
						//System.out.println("");
					}
				}


			} else {
				BiomajLogger.getInstance().log("Internal error : Bank::getAllAvailableProcess process ("+process+") derived not from PreProcess Task!");
			}

		}

		listOldSession.remove(listOldSession.size()-1);

		return inter.values();
	}
	
	public Collection<MetaProcess> getAvailableMetaProcessSql(int process) throws BiomajException {
		HashMap<String, MetaProcess> inter = new HashMap<String, MetaProcess>();
		if ((process != Session.POSTPROCESS)&&(process != Session.PREPROCESS)&&(process != Session.REMOVEPROCESS) ) {
			BiomajLogger.getInstance().log("Internal error : Bank::getAllAvailableProcess Can't get MetaProcess from process preprocess/postprocess/removeprocess");
			return new Vector<MetaProcess>();
		}

		listOldSession.add(currentSession);

		for (int i=listOldSession.size()-1;i>=0;i--) {
			GeneralWorkflowTask res = listOldSession.get(i).getWorkflowTask(process);

			if (res == null)
				continue;
			
			if (res instanceof PreProcessTask) {
				PreProcessTask pre = (PreProcessTask) res;
				
				Vector<MetaProcess> value = pre.getMetaProcess(null);

				for (MetaProcess mp : value) {
					
					//System.out.print(mp.getBlock()+":"+mp.getName());
					if (mp.getStatusStr().trim().compareTo("ok")!=0)
						continue;
					//Si deja inserer alors pas besoin de traiter (car on parcours la liste de session a l envers donc le premier entre et le dernier "ok".....)
					if (!inter.containsKey(mp.getBlock()+mp.getName())) {
						inter.put(mp.getBlock()+mp.getName(), mp);
						//System.out.println(" --> ok");
					} else {
						//System.out.println("");
					}
				}


			} else {
				BiomajLogger.getInstance().log("Internal error : Bank::getAllAvailableProcess process ("+process+") derived not from PreProcess Task!");
			}

		}

		listOldSession.remove(listOldSession.size()-1);

		return inter.values();
	}
	

	/**
	 * @return  the end
	 * @uml.property  name="end"
	 */
	public Date getEnd() {
		return end;
	}

	/**
	 * @param end  the end to set
	 * @uml.property  name="end"
	 */
	public void setEnd(Date end) {
		currentSession.setEnd(end);
		this.end = end;
	}

	/**
	 * @return  the listOldSession
	 * @uml.property  name="listOldSession"
	 */
	public Vector<Session> getListOldSession() {
		return listOldSession;
	}

	/**
	 * @param listOldSession  the listOldSession to set
	 * @uml.property  name="listOldSession"
	 */
	public void setListOldSession(Vector<Session> listSession) {
		this.listOldSession = listSession;
	}

	/**
	 * @return  the start
	 * @uml.property  name="start"
	 */
	public Date getStart() {
		return start;
	}

	/**
	 * @param start  the start to set
	 * @uml.property  name="start"
	 */
	public void setStart(Date start) {
		this.start = start;
	}
	
	public void setLoadMessages(boolean load) {
		loadMessages = load;
	}

	/**
	 * @return  the workflowInfoIsDeployed
	 * @uml.property  name="workflowInfoIsDeployed"
	 */
	public Boolean getWorkflowInfoIsDeployed() {
		return workflowInfoIsDeployed;
	}

	/**
	 * @param workflowInfoIsDeployed  the workflowInfoIsDeployed to set
	 * @uml.property  name="workflowInfoIsDeployed"
	 */
	public void setWorkflowInfoIsDeployed(Boolean workflowInfoIsDeployed) {
		this.workflowInfoIsDeployed = workflowInfoIsDeployed;
	}

	/**
	 * @return  the workflowInfoProductionDir
	 * @uml.property  name="workflowInfoProductionDir"
	 */
	public String getWorkflowInfoProductionDir() {
		return workflowInfoProductionDir;
	}

	/**
	 * @param workflowInfoProductionDir  the workflowInfoProductionDir to set
	 * @uml.property  name="workflowInfoProductionDir"
	 */
	public void setWorkflowInfoProductionDir(String workflowInfoProductionDir) {
		this.workflowInfoProductionDir = workflowInfoProductionDir;
	}

	/**
	 * @return  the workflowInfoRelease
	 * @uml.property  name="workflowInfoRelease"
	 */
	public String getWorkflowInfoRelease() {
		return workflowInfoRelease;
	}

	/**
	 * @param workflowInfoRelease  the workflowInfoRelease to set
	 * @uml.property  name="workflowInfoRelease"
	 */
	public void setWorkflowInfoRelease(String workflowInfoRelease) {
		this.workflowInfoRelease = workflowInfoRelease;
	}

	/**
	 * @return  the workflowInfoRemovedRelease
	 * @uml.property  name="workflowInfoRemovedRelease"
	 */
	public String getWorkflowInfoRemovedRelease() {
		return workflowInfoRemovedRelease;
	}

	/**
	 * @param workflowInfoRemovedRelease  the workflowInfoRemovedRelease to set
	 * @uml.property  name="workflowInfoRemovedRelease"
	 */
	public void setWorkflowInfoRemovedRelease(String workflowInfoRemovedRelease) {
		this.workflowInfoRemovedRelease = workflowInfoRemovedRelease;
	}
	
	public void fill(Map<String, String> update, boolean loadSession) {
		if (update != null) {
			try {
				setStart(BiomajUtils.stringToDate(update.get(BiomajSQLQuerier.UPDATE_START)));
				String endDate = update.get(BiomajSQLQuerier.UPDATE_END);
				if (endDate != null && !endDate.trim().isEmpty())
					setEnd(BiomajUtils.stringToDate(endDate));
			} catch (ParseException pe) {
				BiomajLogger.getInstance().log(pe);
			}
			String updateString = update.get(BiomajSQLQuerier.UPDATED);
			if (updateString.equalsIgnoreCase("true") || updateString.equals("1"))
				setUpdate(true);
			else
				setUpdate(false);
			
			if (loadSession) {
				List<Map<String, String>> sessions = BiomajSQLQuerier.getUpdateSessions(Integer.valueOf(update.get(BiomajSQLQuerier.UPDATE_ID)));
				//O.Filangi Attention, Correction Bug, on efface la session initialisee dans le constructeur
				if (listOldSession.size() > 0)
					listOldSession.removeAllElements();
	
				
				for (Map<String, String> session : sessions) {
					Session s = new Session(this);
					s.fill(session, loadMessages);
					listOldSession.add(s);
				}
	
				Collections.sort(listOldSession);
			}
	
			setWorkflowInfoIsDeployed(Boolean.valueOf(update.get(BiomajSQLQuerier.PRODUCTION_DIR_DEPLOYED)));
			setWorkflowInfoProductionDir(update.get(BiomajSQLQuerier.PRODUCTION_DIR_PATH));
			setWorkflowInfoSizeRelease(BiomajUtils.stringToSize(update.get(BiomajSQLQuerier.SIZE_RELEASE)));
			setWorkflowInfoRelease(update.get(BiomajSQLQuerier.UPDATE_RELEASE));
			setWorkflowInfoSizeDownload(BiomajUtils.stringToSize(update.get(BiomajSQLQuerier.SIZE_DOWNLOAD)));
			List<Map<String, String>> directories = BiomajSQLQuerier.getProductionDirectories(getConfig().getName());
			for (Map<String, String> directory : directories) {
				ProductionDirectory pd = new ProductionDirectory();
				pd.setPath(directory.get(BiomajSQLQuerier.DIR_PATH));
				pd.setSession(Long.valueOf(directory.get(BiomajSQLQuerier.DIR_SESSION)));
				pd.setStateStr(directory.get(BiomajSQLQuerier.DIR_STATE));
				try {
					pd.setCreationDate(BiomajUtils.stringToDate(directory.get(BiomajSQLQuerier.DIR_CREATION)));
					if (pd.getState() == ProductionDirectory.REMOVE) {
						pd.setRemoveDate(BiomajUtils.stringToDate(directory.get(BiomajSQLQuerier.DIR_REMOVE)));
					}
				} catch (ParseException pe) {
					BiomajLogger.getInstance().log(pe);
				}
				bankStateListProductionDirectories.add(pd);
			}
		}
		
	}

	public void fillWithDirectoryVersion(String dir) throws BiomajException {
		//System.out.println(BiomajUtils.getAttribute(node,"idLastSession"));
		setStart(new Date());
		setEnd(new Date());
		setUpdate(true);

		currentSession.fillWithDirectoryVersion(dir);

		File d = new File(dir);

		if (!d.exists())
			throw new BiomajException("directory.not.exist");
		try {
			setWorkflowInfoIsDeployed(true);
			setWorkflowInfoProductionDir(d.getCanonicalPath());
			setWorkflowInfoSizeRelease(BiomajUtils.computeDirectorySize(d.getCanonicalPath()));

			//File current = new File(d.getCanonicalPath()+"/current");
			String[] l = d.getCanonicalPath().split("/");
			String nameDir = l[l.length-1];
			setWorkflowInfoRelease(BmajVersionManagement.getRelease(nameDir));
			setWorkflowInfoSizeDownload(getWorkflowInfoSizeRelease());


			ProductionDirectory pd = new ProductionDirectory();
			pd.setPath(d.getCanonicalPath());
			pd.setSession(currentSession.getId());
			pd.setState(ProductionDirectory.AVAILABLE);
			pd.setCreationDate(new Date());

			bankStateListProductionDirectories.add(pd);
		} catch (IOException ioe) {
			throw new BiomajException("io.error",ioe.getMessage());
		}
	}


	/**
	 * @return  the id
	 * @uml.property  name="id"
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id  the id to set
	 * @uml.property  name="id"
	 */
	public void setId(Long id) {
		this.id = id;
	}


	//To take decision, we need information about workflow task

	/**
	 * Return true if task is ever executed and the status of this task is ok!
	 */
	public boolean workflowTaskEverPast(int task) {
		for (Session s : listOldSession) {
//			System.out.println("Session  : "+s.getStart());
			GeneralWorkflowTask wt = s.getWorkflowTask(task);
			if (wt != null)
				if (wt.getStatus() == GeneralWorkflowTask.STATUS_OK)
					return true;
		}

		if (currentSession == null)
			return false;

		GeneralWorkflowTask wt = currentSession.getWorkflowTask(task);
		if (wt!=null)
			if (wt.getStatus()==GeneralWorkflowTask.STATUS_OK)
				return true;

		return false;
	}

	/**
	 * @return  the update
	 * @uml.property  name="update"
	 */
	public boolean isUpdate() {
		return update;
	}

	/**
	 * @param update  the update to set
	 * @uml.property  name="update"
	 */
	public void setUpdate(boolean update) {
		this.update = update;
	}

	/**
	 * @return  has remove process been launched?
	 * @uml.property  name="remove"
	 */
	public boolean isRemove() {
		return remove;
	}

	/**
	 * @param remove  the remove to set
	 * @uml.property  name="remove"
	 */
	public void setRemove(boolean remove) {
		this.remove = remove;
	}

	/**
	 * @return  has remove process been launched successfully?
	 * @uml.property  name="remove"
	 */
	public boolean isRemoveOk() {
		return removeOk;
	}

	/**
	 * @param remove  the removeOk to set
	 * @uml.property  name="removeOk"
	 */
	public void setRemoveOk(boolean removeOk) {
		this.removeOk = removeOk;
	}
	
	public boolean getStatus() {
		//Si la derniere session s est deroule correctemment, alors la bank c'est ok!
		if (listOldSession.size()<=0)
			return false;
		Collections.sort(listOldSession);
		//System.out.println(BiomajUtils.dateToString(listSession.get(listSession.size()-2).getStart()));
		return listOldSession.get(listOldSession.size()-1).getStatus();
	}

	/**
	 * @return  the workflowInfoSizeRelease
	 * @uml.property  name="workflowInfoSizeRelease"
	 */
	public long getWorkflowInfoSizeRelease() {
		return workflowInfoSizeRelease;
	}

	/**
	 * @param workflowInfoSizeRelease  the workflowInfoSizeRelease to set
	 * @uml.property  name="workflowInfoSizeRelease"
	 */
	public void setWorkflowInfoSizeRelease(long workflowInfoSizeRelease) {
		this.workflowInfoSizeRelease = workflowInfoSizeRelease;
	}


	/******************************************
	 * Methode d 'information general sur le cycle update
	 */

	public int getNbFilesDownloaded() {
		int nb = 0;

		//Lors de la premiere session on obtient les infos exact du nbre de telechargement !

		CheckTask dt = (CheckTask)currentSession.getWorkflowTask(Session.CHECK);
		if (dt != null)
		{
			return dt.getNbFilesDownload();
		}
		
		for (Session s: listOldSession) {
			CheckTask d = (CheckTask)s.getWorkflowTask(Session.CHECK);
			if (d !=null)
				return d.getNbFilesDownload();
		}

		return nb;
	}

	public int getNbFilesLocalOnline() {
		int nb = 0;

		CheckTask dt = (CheckTask)currentSession.getWorkflowTask(Session.CHECK);
		if (dt != null)
		{
			return dt.getNbFilesLocalOnline();
		}

		for (Session s: listOldSession) {
			CheckTask d = (CheckTask)s.getWorkflowTask(Session.CHECK);
			if (d !=null)
				return d.getNbFilesLocalOnline();
		}	

		return nb;
	}	

	
	public int getNbFilesLocalOffline() {
		int nb = 0;

		CheckTask dt = (CheckTask)currentSession.getWorkflowTask(Session.CHECK);
		if (dt != null)
		{
			return dt.getNbFilesLocalOffline();
		}

		for (Session s: listOldSession) {
			CheckTask d = (CheckTask)s.getWorkflowTask(Session.CHECK);
			if (d !=null)
				return d.getNbFilesLocalOffline();
		}	

		return nb;
	}	

	
	public float getBandWidth() {
		float nb = 0;
		int t = 0;
		for (Session s: listOldSession) {
			DownloadTask d = (DownloadTask)s.getWorkflowTask(Session.DOWNLOAD);
			if (d !=null)
			{
				t++;
				nb+=d.getBandWidth();
			}
		}
		DownloadTask dt = (DownloadTask)currentSession.getWorkflowTask(Session.DOWNLOAD);
		if (dt != null)
		{
			t++;
			nb+=dt.getBandWidth();
		}

		return (nb/t);
	}

	/**
	 * Cas d un nouveau cycle :
	 *    1) update=true (une nouvelle version a traiter ) && le stage DEPLOYMENT est passe && DEPLOYMENT.status = ok
	 *    2) update=false et le stage CHECK est passe && CHECK.status = ok
	 *    
	 * @return
	 */
	public boolean needANewCycleUpdate() {
		boolean isUpdate = isUpdate();
		
		if (isUpdate) {
			//le stage deployement n est pas encore passe
			if (getAvailableProcess(Session.DEPLOYMENT)==null) {
				return false;
			}
			
			if (getAvailableProcess(Session.DEPLOYMENT).getStatus()==GeneralWorkflowTask.STATUS_OK)
				return true;
			else
				return false;
		} else {
//			le stage deployement n est pas encore passe
			if (getAvailableProcess(Session.CHECK)==null)
				return false;

			if (getAvailableProcess(Session.CHECK).getStatus()==GeneralWorkflowTask.STATUS_OK)
				return true;
			else
				return false;

		}

		/*
		arg0.getProject().log("Need A new Cycle Update : ",Project.MSG_DEBUG);
		boolean avail = getAvailableProcess(Session.DEPLOYMENT)!=null;
		arg0.getProject().log("A)Find Deployement : "+Boolean.toString(avail),Project.MSG_DEBUG);
		boolean status = getStatus();
		arg0.getProject().log("B)Status : "+Boolean.toString(status),Project.MSG_DEBUG);

		arg0.getProject().log("(A&&B) : "+Boolean.toString(status&&status),Project.MSG_DEBUG);


		boolean isUpdate = isUpdate();
		arg0.getProject().log("C)isUpdate : "+Boolean.toString(isUpdate),Project.MSG_DEBUG);

		arg0.getProject().log("(!C&&B) : "+Boolean.toString(!isUpdate&&status),Project.MSG_DEBUG);

		boolean ret = ((avail)&&(status));//||(!isUpdate&&status));
		arg0.getProject().log("(A&&B) || (!C&&B) : "+Boolean.toString(ret),Project.MSG_DEBUG);

		return ret;
		 */
	}

	/**
	 * @return  the errorOnWorkflow
	 * @uml.property  name="errorOnWorkflow"
	 */
	public Boolean getErrorOnWorkflow() {
		return errorOnWorkflow;
	}

	/**
	 * @param errorOnWorkflow  the errorOnWorkflow to set
	 * @uml.property  name="errorOnWorkflow"
	 */
	public void setErrorOnWorkflow(Boolean errorOnWorkflow) {
		this.errorOnWorkflow = errorOnWorkflow;
	}

	/**
	 * @return  the workflowInfoSizeDownload
	 * @uml.property  name="workflowInfoSizeDownload"
	 */
	public long getWorkflowInfoSizeDownload() {
		return workflowInfoSizeDownload;
	}

	/**
	 * @param workflowInfoSizeDownload  the workflowInfoSizeDownload to set
	 * @uml.property  name="workflowInfoSizeDownload"
	 */
	public void setWorkflowInfoSizeDownload(long workflowInfoSizeDownload) {
		this.workflowInfoSizeDownload = workflowInfoSizeDownload;
	}

	public Vector<MetaProcess> getAllMetaProcessFromPhasePreprocess()  throws BiomajException {
		Vector<MetaProcess> res = new Vector<MetaProcess>();

		for (Session s : listOldSession) {
			PreProcessTask pp = (PreProcessTask)s.getWorkflowTask(Session.PREPROCESS);
			if (pp!=null)
				res.addAll(pp.getMetaProcess(null));
		}

		if (currentSession != null) {
			PreProcessTask pp = (PreProcessTask)currentSession.getWorkflowTask(Session.PREPROCESS);
			if (pp!=null)
				res.addAll(pp.getMetaProcess(null));
		}
		return res;
	}

	public Vector<MetaProcess> getAllMetaProcessFromPhasePostprocess(String block) throws BiomajException {
		Vector<MetaProcess> res = new Vector<MetaProcess>();
		for (Session s : listOldSession) {
			PostProcessTask pp = (PostProcessTask)(s.getWorkflowTask(Session.POSTPROCESS));
			if (pp!=null)
			{
				/*for (MetaProcess mp : pp.getMetaProcess(block))
					System.out.println("add:"+mp.getName()+" date_start:"+BiomajUtils.dateToString(mp.getStart()));*/
				res.addAll(pp.getMetaProcess(block));
			}
		}

		if (currentSession != null) {
			PostProcessTask pp = (PostProcessTask)currentSession.getWorkflowTask(Session.POSTPROCESS);
			if (pp!=null)
				res.addAll(pp.getMetaProcess(block));
		}

		return res;
	}

	public Vector<MetaProcess> getAllMetaProcessFromPhaseRemoveprocess(String block) throws BiomajException {
		Vector<MetaProcess> res = new Vector<MetaProcess>();
		for (Session s : listOldSession) {
			RemoveProcessTask pp = (RemoveProcessTask)(s.getWorkflowTask(Session.REMOVEPROCESS));
			if (pp!=null)
				res.addAll(pp.getMetaProcess(block));
		}

		if (currentSession != null) {
			RemoveProcessTask pp = (RemoveProcessTask)currentSession.getWorkflowTask(Session.REMOVEPROCESS);
			if (pp!=null)
				res.addAll(pp.getMetaProcess(block));
		}

		return res;
	}

	public FileDesc findFile(String name,long time,long size,int typeSession) {

		for (Session s : listOldSession) {
			GeneralWorkflowTask task = s.getWorkflowTask(typeSession);
			if (task == null)
				continue;
			if (typeSession == Session.DOWNLOAD) {
				DownloadTask d = (DownloadTask)task;
				for (FileDesc fd : d.getFiles()) {
					if ((time == fd.getTime())&&(size == fd.getSize())&&(fd.getName().compareTo(name)==0))
						return fd ;
				}
			}
			else if (typeSession == Session.EXTRACT) {
				ExtractTask d = (ExtractTask)task;
				for (FileDesc fd : d.getFiles()) {
					if ((time == fd.getTime())&&(size == fd.getSize())&&(fd.getName().compareTo(name)==0))
						return fd ;
				}
			}
		}
		return null ;
	}

	public Vector<FileDesc> findDependancesFilesFromExtraction(String refHash) {
		Vector<FileDesc> l = new Vector<FileDesc>();

		for (Session s : listOldSession) {
			ExtractTask task = (ExtractTask)s.getWorkflowTask(Session.EXTRACT);
			if (task == null)
				continue;
			for (FileDesc fd : task.getFiles()) {
				if (refHash.compareTo(fd.getRefHash())==0)
					l.add(fd);
			}
		}
/*
		if (currentSession != null) {
			ExtractTask task = (ExtractTask)currentSession.getWorkflowTask(Session.EXTRACT);
			if (task != null)
				for (FileDesc fd : task.getFiles()) {
					if (refHash.compareTo(fd.getRefHash())==0)
						l.add(fd);
				}
		}
*/
		return l;
	}


	public void setBankToRebuild(Bank b) throws BiomajException {
		BiomajLogger.getInstance().log("******** setBankToRebuild *************** ");
		config = b.config ;	
		end = null;

		errorOnWorkflow = b.errorOnWorkflow;
		id = b.id;

		listOldSession.removeAllElements();
		for (Session s : b.listOldSession) {
			
			if (s.getWorkflowTask(Session.DEPLOYMENT)!=null) {
				s.getWorkflowTasks().remove(Session.DEPLOYMENT);
			}
			if (s.getWorkflowTask(Session.POSTPROCESS)!=null) {
				s.getWorkflowTask(Session.POSTPROCESS).setStatus(GeneralWorkflowTask.STATUS_KO);
			}
			listOldSession.add(s);
		}

		start = new Date();
		update = true;
		isRebuilt = true;
		workflowInfoIsDeployed = false;
		workflowInfoProductionDir = b.workflowInfoProductionDir;
		workflowInfoRelease       = b.workflowInfoRelease;
		workflowInfoSizeDownload  = b.workflowInfoSizeDownload;
		workflowInfoSizeRelease   = b.workflowInfoSizeRelease;

		//if (bankStateListProductionDirectories.size()>0)
		//bankStateListProductionDirectories.remove(bankStateListProductionDirectories.size()-1) ;

	}

	/**
	 * 
	 * @param block   peut etre null, dans ce cas cherche le metaprocess dans tous les blocks
	 * @param metaprocess peu etre null 
	 * @return
	 */
	public MetaProcess getLastMetaProcessExecuted(String block,String metaprocess) throws BiomajException {

		Vector<PostProcessTask> listPostProcess = new Vector<PostProcessTask>();

		for (Session s : listOldSession) {
			if (s.getWorkflowTask(Session.POSTPROCESS)!=null)
				listPostProcess.add(0, (PostProcessTask)s.getWorkflowTask(Session.POSTPROCESS));
		}

		for (PostProcessTask p : listPostProcess) {
			Vector<MetaProcess> mps = p.getMetaProcess(block);

			if (mps != null)
				for (MetaProcess mp : mps)
					if (mp.getName().compareTo(metaprocess)==0)
						return mp;
		}

		return null;	
	}

	/**
	 * Retourn les dernier metaProcess executer
	 * @return
	 * @throws BiomajException
	 */
	public Vector<MetaProcess> getAllMetaprocess() throws BiomajException {
		Vector<PostProcessTask> listPostProcess = new Vector<PostProcessTask>();

		for (Session s : listOldSession) {
			if (s.getWorkflowTask(Session.POSTPROCESS)!=null)
				listPostProcess.add(0, (PostProcessTask)s.getWorkflowTask(Session.POSTPROCESS));
		}

		Vector<MetaProcess> res = new Vector<MetaProcess>();
		for (PostProcessTask p : listPostProcess) {
			res.addAll(p.getMetaProcess(null));	
		}

		Vector<MetaProcess> trueRes = new Vector<MetaProcess>();

		for (MetaProcess mp1 : res) {
			boolean ok = true;
			for (MetaProcess mp2 : trueRes)
				if ((mp2.getName().compareTo(mp1.getName())==0)&&(mp2.getBlock().compareTo(mp1.getBlock())==0))
				{
					ok = false;
					break;
				}
			if (ok)
				trueRes.add(mp1);
		}


		return trueRes;	
	}


	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Vector<FileDesc> getGeneratedFiles(Project project, RemoteFile rf) {
		if (project == null)
			return null;
		//FileDesc res;
		//res = lastUpdateSession.findFile(rf.getAbsolutePath(), rf.getDate().getTime(), rf.getSize(), Session.DOWNLOAD);
		String refHash = BiomajUtils.getHashFromRemoteFile(rf);
		//if (res == null)
		//return null;
		Vector<FileDesc> lFd = findDependancesFilesFromExtraction(refHash);

		if (lFd == null || lFd.size() == 0) {
			// Il se peut que le fichier provienne d'une mise a jour ancienne (a la courante....)
			project.log("Search generated file in old session for ["+rf.toString()+"]....",Project.MSG_VERBOSE);
			
			if (allExtractedFiles == null)
				allExtractedFiles = BiomajSQLQuerier.getAllSessionsExtractedFiles(project.getProperty(BiomajConst.dbNameProperty));
				
			Vector<FileDesc> res = new Vector<FileDesc>();
			for (Map<String, String> file : allExtractedFiles) {
				if (hashAreEquals(file.get(BiomajSQLQuerier.REF_HASH),refHash)) {
					FileDesc f = new FileDesc(file.get(BiomajSQLQuerier.FILE_LOCATION),
							Long.valueOf(file.get(BiomajSQLQuerier.FILE_SIZE)),
							Long.valueOf(file.get(BiomajSQLQuerier.FILE_TIME)),
							Boolean.valueOf(file.get(BiomajSQLQuerier.IS_LINK)),
							Boolean.valueOf(file.get(BiomajSQLQuerier.IS_EXTRACT)),
							Boolean.valueOf(file.get(BiomajSQLQuerier.IS_VOLATILE)),
							file.get(BiomajSQLQuerier.REF_HASH));
					res.add(f);
					
					project.log("Generated files find!",Project.MSG_VERBOSE);
					project.log(f.toString(),Project.MSG_VERBOSE);
				}
			}
			
//			Vector<FileDesc> res = BiomajSQLQuerier.getUncompressedFilesWithArchive(refHash, project.getProperty(BiomajConst.dbNameProperty), "extract");
			
//			if (res != null && res.size() > 0) {
//				project.log("Generated files find!",Project.MSG_VERBOSE);
//				for (FileDesc f : res)
//					project.log(f.toString(),Project.MSG_VERBOSE);
//			}
			
			return res;
		}

		return lFd ;
	}
	
	private boolean hashAreEquals(String localHash, String remoteHash) {
		/*
		 * Compare a hash from a remote file and a hash from this remote file once downloaded.
		 * As the two can differ whithout being actually different, we have to :
		 * 
		 * 1 -Ignore the last field of the hash which is the size because the announced size on the remote server
		 * can be different from the actual size of the file once downloaded.
		 * 2 - Make sure that the second field which is the date of the file is smaller for the remote file.
		 * 
		 */
		if (localHash == null)
			return false;
		
		String[] splitLocal = localHash.split("_@_");
		String[] splitRemote = remoteHash.split("_@_");
		
		return splitLocal[0].equals(splitRemote[0]) && Long.valueOf(splitLocal[1]) >= Long.valueOf(splitRemote[1]);
	}
	
	public boolean isRebuilt() {
		return isRebuilt;
	}
}
