package org.inria.biomaj.session.dependency.test;

import java.util.ArrayList;
import java.util.List;

import org.inria.biomaj.session.dependency.GraphElement;
import org.inria.biomaj.session.dependency.GraphEvaluator;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestDependencies {
	
	
	class FakeTask extends Thread {
		
		private GraphEvaluator _eval;
		private int wait;
		
		public FakeTask(GraphEvaluator eval, int wait) {
			_eval = eval;
			this.wait = wait;
		}
		
		@Override
		public void run() {
			GraphElement value = null;
			while ((value = _eval.getNextLeafToExecute()) != null) {
				value.setProcessed(true);
//				System.out.println("Starting : " + value.getValue() + " (" + wait + ")");
				try {
					Thread.sleep(wait);
					value.removeFromParent();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Test scheduling.
	 */
	@SuppressWarnings("unused")
	@Test
	public void testScheduling() {
		
		GraphElement a = new GraphElement(null, "a");
		GraphElement b = new GraphElement(a, "b");
		GraphElement d = new GraphElement(a, "d");
		GraphElement c = new GraphElement(d, "c");
		GraphElement e = new GraphElement(c, "e");
		GraphElement f = new GraphElement(d, "f");
		GraphElement g = new GraphElement(f, "g");
		
		
		GraphEvaluator eval = new GraphEvaluator(a);
		
		int max = 3;
		List<Thread> tasks = new ArrayList<Thread>();
		for (int i = 1; i < max; i++) {
			TestDependencies dp = new TestDependencies();
			FakeTask t = dp.new FakeTask(eval, (i * 100));
			tasks.add(t);
			t.start();
		}
		for (Thread t : tasks) {
			try {
				t.join();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}
		
		String exe = eval.getExecutionOrder();
		
		assertTrue(exe.equals("b,e,g,f,c,d,a") || exe.equals("b,e,g,c,f,d,a"));
		
	}
	
	
	/**
	 * Tests with 2 different parents. 
	 */
	@SuppressWarnings("unused")
	@Test
	public void testValidationDifferentBranches() {
		GraphElement a = new GraphElement(null, "a");
		GraphElement b = new GraphElement(a, "b");
		GraphElement d = new GraphElement(b, "d");
		GraphElement c = new GraphElement(d, "c");
		GraphElement dd = new GraphElement(b, "d");
		
		GraphEvaluator eval = new GraphEvaluator(a);
		assertTrue(!eval.isValid());
	}
	
	/**
	 * Tests A -> B -> A
	 */
	@SuppressWarnings("unused")
	@Test
	public void testValidationSameBranch() {
		
		GraphElement a = new GraphElement(null, "a");
		GraphElement b = new GraphElement(a, "b");
		GraphElement d = new GraphElement(b, "a");		
		
		GraphEvaluator eval = new GraphEvaluator(a);
		
		assertTrue(!eval.isValid());
		
	}
	
}
