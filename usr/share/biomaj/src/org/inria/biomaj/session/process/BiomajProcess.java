/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.session.process;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.FileDesc;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * Process description corresponding to the xml process file
 * @author  ofilangi
 */
public class BiomajProcess {

	/**
	 * @uml.property  name="keyName"
	 */
	private String keyName;
	/**
	 * @uml.property  name="nameProcess"
	 */
	private String nameProcess;
	/**
	 * @uml.property  name="exe"
	 */
	private String exe;
	/**
	 * @uml.property  name="args"
	 */
	private String args;
	/**
	 * @uml.property  name="description"
	 */
	private String description;
	/**
	 * @uml.property  name="type"
	 */
	private String type;

	/**
	 * @uml.property  name="returnValue"
	 */
	private int returnValue ;
	/**
	 * @uml.property  name="errorDetected"
	 */
	private boolean errorDetected ;

	//Dynamics data
	/**
	 * @uml.property  name="start"
	 */
	private Date start;
	/**
	 * @uml.property  name="end"
	 */
	private Date end;

	/**
	 * @uml.property  name="timeStampExe"
	 */
	private long timeStampExe;
	
	// Process id in the db
	private long processId = -1;

	private Vector<String> warn = new Vector<String>();

	private Vector<String> err = new Vector<String>();

	private Vector<FileDesc> dependancesOutput = new Vector<FileDesc>();

	public BiomajProcess() {

	}
	
	public void setProcessId(long id) {
		processId = id;
	}
	
	public long getProcessId() {
		return processId;
	}

	/**
	 * @return  the args
	 * @uml.property  name="args"
	 */
	public String getArgs() {
		return args;
	}

	/**
	 * @param args  the args to set
	 * @uml.property  name="args"
	 */
	public void setArgs(String args) {
		this.args = args;
	}

	/**
	 * @return  the description
	 * @uml.property  name="description"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description  the description to set
	 * @uml.property  name="description"
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return  the exe
	 * @uml.property  name="exe"
	 */
	public String getExe() {
		return exe;
	}

	/**
	 * @param exe  the exe to set
	 * @uml.property  name="exe"
	 */
	public void setExe(String exe) {
		this.exe = exe;
	}

	/**
	 * @return  the keyName
	 * @uml.property  name="keyName"
	 */
	public String getKeyName() {
		return keyName;
	}

	/**
	 * @param keyName  the keyName to set
	 * @uml.property  name="keyName"
	 */
	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	/**
	 * @return  the nameProcess
	 * @uml.property  name="nameProcess"
	 */
	public String getNameProcess() {
		return nameProcess;
	}

	/**
	 * @param nameProcess  the nameProcess to set
	 * @uml.property  name="nameProcess"
	 */
	public void setNameProcess(String nameProcess) {
		this.nameProcess = nameProcess;
	}

	/**
	 * @return  the type
	 * @uml.property  name="type"
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type  the type to set
	 * @uml.property  name="type"
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return  the err
	 * @uml.property  name="err"
	 */
	public Vector<String> getErr() {
		return err;
	}

	/**
	 * @param err  the err to set
	 * @uml.property  name="err"
	 */
	public void setErr(Vector<String> err) {
		this.err = err;
	}

	/**
	 * @return  the warn
	 * @uml.property  name="warn"
	 */
	public Vector<String> getWarn() {
		return warn;
	}

	/**
	 * @param warn  the warn to set
	 * @uml.property  name="warn"
	 */
	public void setWarn(Vector<String> warn) {
		this.warn = warn;
	}

	/**
	 * @return  the end
	 * @uml.property  name="end"
	 */
	public Date getEnd() {
		return end;
	}

	/**
	 * @param end  the end to set
	 * @uml.property  name="end"
	 */
	public void setEnd(Date end) {
		this.end = end;
	}

	/**
	 * @return  the start
	 * @uml.property  name="start"
	 */
	public Date getStart() {
		return start;
	}

	/**
	 * @param start  the start to set
	 * @uml.property  name="start"
	 */
	public void setStart(Date start) {
		this.start = start;
	}

	/**
	 * @return  the returnValue
	 * @uml.property  name="returnValue"
	 */
	public int getReturnValue() {
		return returnValue;
	}

	/**
	 * @param returnValue  the returnValue to set
	 * @uml.property  name="returnValue"
	 */
	public void setReturnValue(int returnValue) {
		this.returnValue = returnValue;
	}

	/**
	 * @return  the dependancesOutput
	 * @uml.property  name="dependancesOutput"
	 */
	public Vector<FileDesc> getDependancesOutput() {
		return dependancesOutput;
	}

	/**
	 * @param dependancesOutput  the dependancesOutput to set
	 * @uml.property  name="dependancesOutput"
	 */
	public void setDependancesOutput(Vector<FileDesc> dependancesOutput) {
		this.dependancesOutput = dependancesOutput;
	}
	
	public void fill(Map<String, String> process) {
		
		processId = Long.valueOf(process.get(BiomajSQLQuerier.PROC_ID));
		
		setNameProcess(process.get(BiomajSQLQuerier.PROC_NAME));
		try {
			setStart(BiomajUtils.stringToDate(process.get(BiomajSQLQuerier.PROC_START)));
			setEnd(BiomajUtils.stringToDate(process.get(BiomajSQLQuerier.PROC_END)));
		} catch (ParseException pe) {
			BiomajLogger.getInstance().log(pe.getLocalizedMessage());
			return;
		}
		String val = process.get(BiomajSQLQuerier.PROC_VALUE);
		if (val != null && !val.isEmpty())
			setReturnValue(Integer.valueOf(val));
		else
			setReturnValue(-1);

		setKeyName(process.get(BiomajSQLQuerier.PROC_KEYNAME));
		setExe(process.get(BiomajSQLQuerier.PROC_EXE));
		setArgs(process.get(BiomajSQLQuerier.PROC_ARGS));
		setDescription(process.get(BiomajSQLQuerier.PROC_DESC));
		setType(process.get(BiomajSQLQuerier.PROC_TYPE));
		setErrorDetected(Boolean.valueOf(process.get(BiomajSQLQuerier.PROC_ERROR)));

		String timeStamp = process.get(BiomajSQLQuerier.PROC_TIMESTAMP);
		if (timeStamp != null && !timeStamp.trim().isEmpty())
			setTimeStampExe(Long.valueOf(timeStamp));
		
		List<Map<String, String>> files = BiomajSQLQuerier.getProcessFiles(Integer.valueOf(process.get(BiomajSQLQuerier.PROC_ID)));
		for (Map<String, String> file : files) {
			
			String location = file.get(BiomajSQLQuerier.FILE_LOCATION);
			long size = Long.valueOf(file.get(BiomajSQLQuerier.FILE_SIZE));
			long time = Long.valueOf(file.get(BiomajSQLQuerier.FILE_TIME));
			String refHash = file.get(BiomajSQLQuerier.REF_HASH);
			boolean link = Boolean.valueOf(file.get(BiomajSQLQuerier.IS_LINK));
			boolean extract = Boolean.valueOf(file.get(BiomajSQLQuerier.IS_EXTRACT));
			boolean volatil = Boolean.valueOf(file.get(BiomajSQLQuerier.IS_VOLATILE));

			FileDesc f = new FileDesc(location,size,time,link,extract,volatil,refHash);
			f.setFileId(Long.valueOf(file.get(BiomajSQLQuerier.FILE_ID)));
			getDependancesOutput().add(f);
		}
		
		List<List<String>> messages = BiomajSQLQuerier.getProcessMessages(Integer.valueOf(process.get(BiomajSQLQuerier.PROC_ID)));
		warn.addAll(messages.get(0));
		err.addAll(messages.get(1));
		
	}

	/**
	 * @return  the errorDetected
	 * @uml.property  name="errorDetected"
	 */
	public boolean isErrorDetected() {
		return errorDetected;
	}

	/**
	 * @param errorDetected  the errorDetected to set
	 * @uml.property  name="errorDetected"
	 */
	public void setErrorDetected(boolean errorDetected) {
		this.errorDetected = errorDetected;
	}

	/**
	 * @return  the timeStampExe
	 * @uml.property  name="timeStampExe"
	 */
	public long getTimeStampExe() {
		return timeStampExe;
	}

	/**
	 * @param timeStampExe  the timeStampExe to set
	 * @uml.property  name="timeStampExe"
	 */
	public void setTimeStampExe(long timeStampExe) {
		this.timeStampExe = timeStampExe;
	}
}
