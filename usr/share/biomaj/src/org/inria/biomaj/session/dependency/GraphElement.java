package org.inria.biomaj.session.dependency;

import java.util.ArrayList;
import java.util.List;


/**
 * Tree structure for evaluating the bank update order.
 * 
 * @author rsabas
 *
 */
public class GraphElement {

	private String value = "";
	private List<GraphElement> children = new ArrayList<GraphElement>();
	private GraphElement parent = null;
	private boolean processed = false;
	
	public GraphElement(GraphElement parent, String value) {
		this.parent = parent;
		this.value = value;
		if (parent != null)
			parent.addChild(this);
	}
	
	public void addChild(GraphElement e) {
		children.add(e);
		e.parent = this;
	}
	
	public String getValue() {
		return value;
	}
	
	public boolean isRoot() {
		return parent == null;
	}
	
	public boolean isLeaf() {
		return children.size() == 0;
	}
	
	public void removeFromParent() {
		if (parent != null) {
			parent.getChildren().remove(this);
		}
	}
	
	public List<GraphElement> getChildren() {
		return children;
	}
	
	public GraphElement getParent() {
		return parent;
	}
	
	public void setProcessed(boolean p) {
		processed = p;
	}
	
	public boolean isProcessed() {
		return processed;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GraphElement) {
			return ((GraphElement) obj).getValue().equals(value);
		}
		return false;
	}
	
}
