package org.inria.biomaj.session.bank;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.sql.SQLConnection;
import org.inria.biomaj.sql.SQLConnectionFactory;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * Class that requests various information from the database.
 * 
 * @author rsabas
 * @author tuco (Emmanuel Quevillon) SQL queries updates
 */
public class BiomajSQLQuerier {
	
	
	//Bank
	public static final String BANK_ID					= "idbank";
	public static final String BANK_NAME				= "name";
	
	//Configuration
	public static final String CONFIGURATION_ID 		= "idconfiguration";
	public static final String DATE 					= "date";
	public static final String FILE 					= "file";
	
	//LocalInfo
	public static final String OFFLINE_DIRECTORY		= "offlineDirectory";
	public static final String VERSION_DIRECTORY		= "versionDirectory";
	public static final String FREQUENCY 				= "frequency";
	public static final String DO_LINKCOPY				= "dolinkcopy";
	public static final String HAS_LOG_FILE				= "logFile";
	public static final String RELEASE_FILE				= "releaseFile";
	public static final String RELEASE_REGEXP			= "releaseRegexp";
	public static final String REMOTE_FILES				= "remoteFiles";
	public static final String REMOTE_EXCLUDED_FILES	= "remoteExcludedFiles";
	public static final String LOCAL_FILES				= "localFiles";
	public static final String N_VERSIONS				= "nversions";
	
	//RemoteInfo
	public static final String PROTOCOL					= "protocol";
	public static final String PORT						= "port";
	public static final String DB_NAME					= "dbName";
	public static final String DB_FULLNAME				= "dbFullName";
	public static final String DB_TYPE					= "dbType";
	public static final String SERVER					= "server";
	public static final String REMOTE_DIR				= "remoteDir";
	
	//Updatebank
	public static final String UPDATE_ID				= "idupdateBank";
	public static final String UPDATE_RELEASE			= "updateRelease";
	public static final String PRODUCTION_DIR_PATH		= "productionDirectoryPath";
	public static final String PRODUCTION_DIR_DEPLOYED	= "productionDirectoryDeployed";
	public static final String SIZE_DOWNLOAD			= "sizeDownload";
	public static final String SIZE_RELEASE				= "sizeRelease";
	public static final String UPDATE_START				= "startTime";
	public static final String UPDATE_END				= "endTime";
	public static final String UPDATE_ELAPSED			= "elapsedTime";
	public static final String UPDATED					= "isUpdated";
	public static final String NB_SESSIONS				= "nbSessions";
	public static final String ID_LAST_SESSION			= "idLastSession";
	
	//Session
	public static final String SESSION_ID				= "idsession";
	public static final String HREF						= "href";
	public static final String PARSE					= "parse";
	public static final String SESSION_STATUS			= "status";
	public static final String SESSION_START			= "startTime";
	public static final String SESSION_END				= "endTime";
	public static final String SESSION_ELAPSED			= "elapsedTime";
	public static final String LOG_FILE					= "logfile";
	
	//SessionTask
	public static final String TASK_ID					= "idsessionTask";
	public static final String TASK_START				= "startTime";
	public static final String TASK_END					= "endTime";
	public static final String TASK_ELAPSED				= "elapsedTime";
	public static final String TASK_STATUS				= "status";
	public static final String VALUE					= "value";
	public static final String NB_EXTRACT				= "nbExtract";
	public static final String NB_LOCAL_ONLINE_FILES	= "nbLocalOnlineFiles";
	public static final String NB_LOCAL_OFFLINE_FILES	= "nbLocalOfflineFiles";
	public static final String NB_DOWNLOADED_FILES		= "nbDownloadFiles";
	public static final String BANDWIDTH				= "bandwidth";
	public static final String NB_FILES_MOVED			= "nbFilesMoved";
	public static final String NB_FILES_COPIED			= "nbFilesCopied";
	public static final String NBRE_METAPROCESS			= "nbreMetaProcess";
	public static final String TASK_TYPE				= "taskType";
	
	//Message
	public static final String MESSAGE_ID				= "idmessage";
	public static final String MESSAGE					= "message";
	public static final String MESSAGE_TYPE				= "type";
	
	//Metaprocess
	public static final String META_ID					= "idmetaprocess";
	public static final String META_NAME				= "name";
	public static final String META_START				= "startTime";
	public static final String META_END					= "endTime";
	public static final String META_ELAPSED				= "elapsedTime";
	public static final String META_STATUS				= "status";
	public static final String META_LOGFILE				= "logfile";
	public static final String BLOCK					= "block";
	
	//File
	public static final String FILE_ID					= "idfile";
	public static final String FILE_LOCATION			= "location";
	public static final String FILE_SIZE				= "size";
	public static final String FILE_TIME				= "time";
	public static final String IS_LINK					= "link";
	public static final String IS_EXTRACT				= "is_extract";
	public static final String IS_VOLATILE				= "volatile";
	public static final String REF_HASH					= "refHash";
	public static final String FILE_TYPE				= "fileType";
	
	//ProductionDirectory
	public static final String DIR_ID					= "idproductionDirectory";
	public static final String DIR_REMOVE				= "remove";
	public static final String DIR_CREATION				= "creation";
	public static final String DIR_SIZE					= "size";
	public static final String DIR_STATE				= "state";
	public static final String DIR_SESSION				= "session";
	public static final String DIR_PATH					= "path";
	
	//Process
	public static final String PROC_ID					= "idprocess";
	public static final String PROC_NAME				= "name";
	public static final String PROC_KEYNAME				= "keyname";
	public static final String PROC_EXE					= "exe";
	public static final String PROC_ARGS				= "args";
	public static final String PROC_DESC				= "description";
	public static final String PROC_TYPE				= "type";
	public static final String PROC_START				= "startTime";
	public static final String PROC_END					= "endTime";
	public static final String PROC_ELAPSED				= "elapsedTime";
	public static final String PROC_ERROR				= "biomaj_error";
	public static final String PROC_TIMESTAMP			= "timestamp";
	public static final String PROC_VALUE				= "value";
	
	
	private static SQLConnection connection = SQLConnectionFactory.getConnection();
//	private static BiomajLogger logger = BiomajLogger.getInstance();
	
//	private static boolean connectionInit = false;
//	private static boolean testMode = false;
	
	
	/*
	 * Methods from former BiomajQueryXMLStateFile class...
	 */
	
	public static synchronized boolean getUpdateBank(String bankName, long idSession, Bank bank,boolean loadSession) throws BiomajException {
		BiomajLogger.getInstance().log("["+bankName+"]"+"START sql request : getUpdateBank "+new SimpleDateFormat("HH:mm:ss").format(new Date()));
		
		Map<String, String> update = getUpdateFromId(idSession);
		if (update == null)
			return false;

		bank.fill(update, loadSession);
		BiomajLogger.getInstance().log("END sql request : getUpdateBank "+new SimpleDateFormat("HH:mm:ss").format(new Date()));
		return true;
	}
	
	public static synchronized boolean getFuturReleaseBankWithoutConfiguration(String bankName, Bank bank,boolean loadSession) throws BiomajException {

		BiomajLogger.getInstance().log("START sql query : getFuturReleaseBankWithoutConfiguration "+new SimpleDateFormat("HH:mm:ss").format(new Date()));

		Map<String, String> update = BiomajSQLQuerier.getLatestUpdate(bankName, true);
		if (update == null)
			return false;
		
		bank.fill(update,loadSession);		
		BiomajLogger.getInstance().log("END sql query : getFuturReleaseBankWithoutConfiguration "+new SimpleDateFormat("HH:mm:ss").format(new Date()));
		
		return true;
	}
	
	public static synchronized Vector<FileDesc> getUncompressedFilesWithArchive(String refHash,String bankName,String fromTag) throws BiomajException {
		
		List<Map<String, String>> filz = BiomajSQLQuerier.getAllSessionsExtractedFiles(bankName);
		if (filz == null)
			return null;
		
		Vector<FileDesc> res = new Vector<FileDesc>();
		for (Map<String, String> file : filz) {
			if (file.get(BiomajSQLQuerier.REF_HASH).equals(refHash)) {
				res.add(new FileDesc(file.get(BiomajSQLQuerier.FILE_LOCATION),
						Long.valueOf(file.get(BiomajSQLQuerier.FILE_SIZE)),
						Long.valueOf(file.get(BiomajSQLQuerier.FILE_TIME)),
						Boolean.valueOf(file.get(BiomajSQLQuerier.IS_LINK)),
						Boolean.valueOf(file.get(BiomajSQLQuerier.IS_EXTRACT)),
						Boolean.valueOf(file.get(BiomajSQLQuerier.IS_VOLATILE)),
						file.get(BiomajSQLQuerier.REF_HASH)));
			}
		}
		
		return res;
	}
	
	public static synchronized long getStatMoyenneUpdate(String bankName) throws BiomajException {
		
		List<Map<String, String>> updates = getBankUpdates(bankName, true);
		
		if (updates == null || updates.size() == 0)
			return 0;

		double avg = 0; 
		long[] data = new long[updates.size() - 1];

		for (int i = 0; i < updates.size()-1; i++) {
			Map<String, String> u1 = updates.get(i);
			Map<String, String> u2 = updates.get(i+1);
			long t1 = 0;
			long t2 = 0;
			try {
				t1 = BiomajUtils.stringToDate(u1.get(UPDATE_END)).getTime();
			} catch (ParseException pe) {
				BiomajLogger.getInstance().log(pe);
			}
			try {
				t2 = BiomajUtils.stringToDate(u2.get(UPDATE_END)).getTime();
			} catch (ParseException pe) {
				BiomajLogger.getInstance().log(pe);
			}
			data[i] = t2 - t1;
			avg += (t2-t1);
		}
		avg = (double)(avg / (updates.size() - 1));
		
		return (long)avg;
	}
	/*
	 * END
	 */

	/**
	 * Returns the X latest messages.
	 * 
	 * @param messageCount number of message to retrieve
	 * @return
	 */
	public static synchronized List<Map<String, String>> getLastMessages(int messageCount) {
		String query = "SELECT * FROM message ORDER BY idmessage DESC LIMIT " + messageCount;
		
		Statement stat = connection.getStatement();
		
		List<Map<String, String>> messages = new ArrayList<Map<String,String>>();
		try {

			ResultSet rs = connection.executeQuery(query, stat);
			while (rs.next()) {
				Map<String, String> message = new HashMap<String, String>();
				message.put(MESSAGE_ID, String.valueOf(rs.getInt(MESSAGE_ID)));
				message.put(MESSAGE_TYPE, rs.getString(MESSAGE_TYPE));
				message.put(MESSAGE, rs.getString(MESSAGE));
				
				messages.add(message);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		SQLConnectionFactory.closeConnection(stat);
		return messages;
	}
	
	
	/**
	 * Returns the bank corresponding to this process.
	 * 
	 * @param idProcess
	 * @return
	 */
	public static synchronized List<String> getProcessBankAndDate(int idProcess) {
		String query = "SELECT ref_idmetaprocess FROM process WHERE idprocess=" + idProcess;
		Statement stat = connection.getStatement();
		try {
			ResultSet rs = connection.executeQuery(query, stat);
			if (rs.next()) {
				String metaId = rs.getString(1);
				SQLConnectionFactory.closeConnection(stat);
				List<String> res = new ArrayList<String>();
				res.add(getMetaprocessBankAndDate(metaId).get(0));
				res.add(getDate("SELECT endTime FROM process WHERE idprocess=" + idProcess));
				return res;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		SQLConnectionFactory.closeConnection(stat);
		return null;
		
	}
	
	/**
	 * Returns the bank corresponding to this metaprocess.
	 * 
	 * @param idMeta
	 * @return
	 */
	public static synchronized List<String> getMetaprocessBankAndDate(String idMeta) {
		String query = "SELECT ref_idsessionTask FROM metaprocess WHERE idmetaprocess='" + idMeta + "'";
		Statement stat = connection.getStatement();
		try {
			ResultSet rs = connection.executeQuery(query, stat);
			if (rs.next()) {
				int taskId = rs.getInt(1);
				SQLConnectionFactory.closeConnection(stat);
				List<String> res = new ArrayList<String>();
				res.add(getTaskBankAndDate(taskId).get(0));
				res.add(getDate("SELECT endTime FROM metaprocess WHERE idmetaprocess='" + idMeta + "'"));
				return res;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		SQLConnectionFactory.closeConnection(stat);
		return null;
	}
	
	/**
	 * Returns the bank corresponding to this session.
	 * 
	 * @param idSession
	 * @return
	 */
	public static synchronized List<String> getSessionBankAndDate(long idSession) {
		String query = "SELECT name FROM bank WHERE idbank=" +
			"(SELECT ref_idbank FROM configuration WHERE idconfiguration=" +
			"(SELECT ref_idconfiguration FROM updateBank WHERE idupdateBank=" +
			"(SELECT ref_idupdateBank FROM session WHERE idsession=" + idSession + ")));";
		
		Statement stat = connection.getStatement();
		try {
			ResultSet rs = connection.executeQuery(query, stat);
			if (rs.next()) {
				String bank = rs.getString(1);
				SQLConnectionFactory.closeConnection(stat);
				List<String> res = new ArrayList<String>();
				res.add(bank);
				res.add(getDate("SELECT endTime FROM session where idsession=" + idSession));
				return res;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		SQLConnectionFactory.closeConnection(stat);
		return null;
	}
	
	/**
	 * Returns the bank corresponding to this task.
	 * 
	 * @param idTask
	 * @return
	 */
	public static synchronized List<String> getTaskBankAndDate(int idTask) {
		String query = "SELECT ref_idsession FROM session_has_sessionTask WHERE " +
				"ref_idsessionTask=" + idTask;
		Statement stat = connection.getStatement();
		try {
			ResultSet rs = connection.executeQuery(query, stat);
			if (rs.next()) {
				long sessionId = rs.getLong(1);
				SQLConnectionFactory.closeConnection(stat);
				List<String> res = new ArrayList<String>();
				res.add(getSessionBankAndDate(sessionId).get(0));
				res.add(getDate("SELECT endTime FROM sessionTask WHERE idsessionTask=" + idTask));
				return res;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		SQLConnectionFactory.closeConnection(stat);
		return null;
	}
	
	private static String getDate(String query) {
		Statement stat = connection.getStatement();
		ResultSet rs = connection.executeQuery(query, stat);
		String date = "";
		try {
			if (rs.next()) {
				date = BiomajUtils.dateToString(new Date(rs.getTimestamp(1).getTime()), Locale.US);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		SQLConnectionFactory.closeConnection(stat);
		return date;
	}
	
	
	/**
	 * Returns a map that contains the latest configuratoin
	 * information about the given bank.
	 * 
	 * @param bankName bank whose configuration is retrieved
	 * @return
	 */
	public static synchronized Map<String, String> getBankInfo(String bankName) {
		/*
		String query = "SELECT * FROM bank,configuration,localinfo,remoteinfo WHERE idbank= " +
				"(SELECT idbank FROM bank WHERE name='" + bankName + "') AND idconfiguration=" +
				"(SELECT idconfiguration FROM configuration WHERE configuration.ref_idbank=idbank " +
				"AND configuration.date=(SELECT max(date) FROM configuration WHERE ref_idbank=idbank)) " +
				"AND idlocalinfo=(SELECT ref_idlocalinfo FROM configuration where ref_idbank=idbank) " +
				"AND idremoteinfo=(SELECT ref_idremoteInfo FROM configuration WHERE ref_idbank=idbank)";*/
		
		
		/*
		 * Information related to the latest update
		 */
		
		/*
		query = "SELECT max(startTime) FROM updateBank WHERE ref_idconfiguration IN " +
				"(SELECT idconfiguration FROM configuration WHERE ref_idbank=" + bankId + ")";
		Statement stat = connection.getStatement();
		ResultSet rs = connection.executeQuery(query, stat);
		rs = connection.getResult();
		String maxStart = null;
		try {
			if (rs.next())
				maxStart = rs.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		query = "SELECT * FROM bank,configuration,localinfo,remoteinfo WHERE idconfiguration IN " +
				"(SELECT ref_idconfiguration FROM updateBank WHERE ref_idconfiguration IN " +
				"(SELECT idconfiguration FROM configuration WHERE ref_idbank=" + bankId + ") AND " +
				"startTime='" + maxStart + "') " +
				"AND idremoteinfo=ref_idremoteinfo " +
				"AND idlocalinfo=ref_idlocalinfo " +
				"AND idbank = ref_idbank";
		*/
		
		int bankId = getBankId(bankName);
		if (bankId < 0)
			return null;
		// Varies between 0.28 sec and 2 sec
		// New query between 0.01 and 0.12 sec
		/*
		String query = "SELECT * FROM bank,configuration,localInfo,remoteInfo WHERE idconfiguration IN " +
				"(SELECT ref_idconfiguration FROM updateBank WHERE ref_idconfiguration IN " +
				"(SELECT idconfiguration FROM configuration WHERE ref_idbank=" + bankId + ") AND " +
				"startTime=(SELECT max(startTime) FROM updateBank WHERE ref_idconfiguration IN " +
				"(SELECT idconfiguration FROM configuration WHERE ref_idbank=" + bankId + "))) " +
				"AND idremoteinfo=ref_idremoteinfo " +
				"AND idlocalinfo=ref_idlocalinfo " +
				"AND idbank = ref_idbank";
		*/
		// We only want one result (getBankInfo(rs))
		  String query = "SELECT b.*,c.*,lI.*,rI.* FROM bank b " +
		      "JOIN configuration c ON c.ref_idbank = b.idbank " +
		      "JOIN localInfo lI ON lI.idlocalInfo = c.ref_idlocalInfo " +
		      "JOIN remoteInfo rI ON rI.idremoteInfo = c.ref_idremoteInfo " +
		      "JOIN updateBank uB ON uB.ref_idconfiguration = c.idconfiguration " +
		      "WHERE b.idbank = " + bankId + " " +
		      "ORDER BY uB.startTime DESC LIMIT 1";


		Statement stat = connection.getStatement();
		ResultSet rs = connection.executeQuery(query, stat);
		Map<String, String> res = getBankInfo(rs);
		SQLConnectionFactory.closeConnection(stat);
		return res;
	}
	
	/**
	 * Gets a bank's configurations.
	 * 
	 * @param bankId
	 * @return
	 */
	public static List<Map<String, String>> getConfigurations(int bankId) {
		String query = "SELECT * FROM configuration, localInfo, remoteInfo WHERE ref_idbank=" + bankId +
				" AND ref_idremoteInfo=idremoteInfo AND ref_idlocalInfo=idlocalInfo";
		Statement stat = connection.getStatement();
		List<Map<String, String>> configs = new ArrayList<Map<String,String>>();
		try {
			ResultSet result = connection.executeQuery(query, stat);
			while (result.next()) {
				Map<String, String> map = new HashMap<String, String>();
				map.put(CONFIGURATION_ID, String.valueOf(result.getLong(CONFIGURATION_ID)));
				map.put(DATE, BiomajUtils.dateToString(new Date(result.getTimestamp(DATE).getTime()), Locale.US));
				map.put(FILE, result.getString(FILE));
				map.put(OFFLINE_DIRECTORY, result.getString(OFFLINE_DIRECTORY));
				map.put(VERSION_DIRECTORY, result.getString(VERSION_DIRECTORY));
				map.put(FREQUENCY, String.valueOf(result.getInt(FREQUENCY)));
				map.put(DO_LINKCOPY, String.valueOf(result.getBoolean(DO_LINKCOPY)));
				map.put(HAS_LOG_FILE, String.valueOf(result.getBoolean(HAS_LOG_FILE)));
				map.put(RELEASE_FILE, result.getString(RELEASE_FILE));
				map.put(RELEASE_REGEXP, result.getString(RELEASE_REGEXP));
				map.put(REMOTE_FILES, result.getString(REMOTE_FILES));
				map.put(REMOTE_EXCLUDED_FILES, result.getString(REMOTE_EXCLUDED_FILES));
				map.put(LOCAL_FILES, result.getString(LOCAL_FILES));
				map.put(N_VERSIONS, String.valueOf(result.getInt(N_VERSIONS)));
				map.put(PROTOCOL, result.getString(PROTOCOL));
				map.put(PORT, String.valueOf(result.getInt(PORT)));
				map.put(DB_NAME, result.getString(DB_NAME));
				map.put(DB_FULLNAME, result.getString(DB_FULLNAME));
				map.put(DB_TYPE, result.getString(DB_TYPE));
				map.put(SERVER, result.getString(SERVER));
				map.put(REMOTE_DIR, result.getString(REMOTE_DIR));
				
				configs.add(map);
			}
		} catch (SQLException e) {
			SQLConnectionFactory.closeConnection(stat);
			e.printStackTrace();
			return null;
		}

		SQLConnectionFactory.closeConnection(stat);
		return configs;
	}
	
	/**
	 * Gets a configuration's updates.
	 * 
	 * @param configId
	 * @return
	 */
	public static List<Map<String, String>> getConfigUpdates(long configId) {
		String query = "SELECT * FROM updateBank WHERE ref_idconfiguration=" + configId;
		Statement stat = connection.getStatement();
		List<Map<String, String>> updates = new ArrayList<Map<String,String>>();
		try {
			ResultSet result = connection.executeQuery(query, stat);
			while (result.next()) {
				updates.add(getUpdate(result));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		SQLConnectionFactory.closeConnection(stat);
		return updates;
	}
	
	/**
	 * Gets bank configuration for a given update.
	 * 
	 * @param bankName
	 * @return
	 */
	public static synchronized Map<String, String> getBankInfoForUpdate(int updateId) {
		
		String query = "SELECT * FROM bank, configuration, localInfo, remoteInfo WHERE " +
				"idlocalInfo=ref_idlocalInfo AND idremoteInfo=ref_idremoteInfo AND " +
				"idbank=ref_idbank AND idconfiguration=(SELECT ref_idconfiguration FROM updateBank WHERE " +
				"idupdateBank=" + updateId + ")";
		
		Statement stat = connection.getStatement();
		
		Map<String, String> res = null;
		ResultSet result = connection.executeQuery(query, stat);
		res = getBankInfo(result);

		SQLConnectionFactory.closeConnection(stat);
		return res;
	}
	
	/**
	 * Returns bank id from its name.
	 * 
	 * @param bankName
	 * @return bankId, -1 if bank was not found
	 */
	public static synchronized int getBankId(String bankName) {
		String query = "SELECT idbank FROM bank WHERE name='" + bankName + "'";
		Statement stat = connection.getStatement();
		
		int bankId = -1;
		try {
			ResultSet rs = connection.executeQuery(query, stat);
			if (rs.next())
				bankId = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		SQLConnectionFactory.closeConnection(stat);
		return bankId;
	}
	
	private static Map<String, String> getBankInfo(ResultSet result) {
		
		Map<String, String> map = new HashMap<String, String>();
		try {
			if (result.next()) {
				map.put(BANK_ID, String.valueOf(result.getInt(BANK_ID)));
				map.put(BANK_NAME, result.getString(BANK_NAME));
				map.put(CONFIGURATION_ID, String.valueOf(result.getLong(CONFIGURATION_ID)));
				map.put(DATE, BiomajUtils.dateToString(new Date(result.getTimestamp(DATE).getTime()), Locale.US));
				map.put(FILE, result.getString(FILE));
				map.put(OFFLINE_DIRECTORY, result.getString(OFFLINE_DIRECTORY));
				map.put(VERSION_DIRECTORY, result.getString(VERSION_DIRECTORY));
				map.put(FREQUENCY, String.valueOf(result.getInt(FREQUENCY)));
				map.put(DO_LINKCOPY, String.valueOf(result.getBoolean(DO_LINKCOPY)));
				map.put(HAS_LOG_FILE, String.valueOf(result.getBoolean(HAS_LOG_FILE)));
				map.put(RELEASE_FILE, result.getString(RELEASE_FILE));
				map.put(RELEASE_REGEXP, result.getString(RELEASE_REGEXP));
				map.put(REMOTE_FILES, result.getString(REMOTE_FILES));
				map.put(REMOTE_EXCLUDED_FILES, result.getString(REMOTE_EXCLUDED_FILES));
				map.put(LOCAL_FILES, result.getString(LOCAL_FILES));
				map.put(N_VERSIONS, String.valueOf(result.getInt(N_VERSIONS)));
				map.put(PROTOCOL, result.getString(PROTOCOL));
				map.put(PORT, String.valueOf(result.getInt(PORT)));
				map.put(DB_NAME, result.getString(DB_NAME));
				map.put(DB_FULLNAME, result.getString(DB_FULLNAME));
				map.put(DB_TYPE, result.getString(DB_TYPE));
				map.put(SERVER, result.getString(SERVER));
				map.put(REMOTE_DIR, result.getString(REMOTE_DIR));
			} else
				return null;
			// We expect only 1 row
			if (result.next())
				throw new SQLException("More than 1 row was returned");
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return map;
	}
	
	
	/**
	 * Returns information about the latest update for the given bank.
	 * 
	 * @param bankName
	 * @param checkUpdate check whether the process produced an update or not
	 */
	public static synchronized Map<String, String> getLatestUpdate(String bankName, boolean checkUpdate) {
		
		
		int bankId = getBankId(bankName);
		if (bankId < 0)
			return null;
		
		long configId = getLatestConfigurationWithSession(bankId, checkUpdate);
		
		if (configId < 0)
			return null;
		
		String query;
		if (checkUpdate) {
			query = "SELECT * FROM updateBank WHERE ref_idconfiguration=" + configId + " AND " +
					"endTime = (SELECT max(endTime) FROM updateBank WHERE " +
					"ref_idconfiguration=" + configId + " AND isUpdated=true)";
		} else
			query = "SELECT * FROM updateBank WHERE ref_idconfiguration=" + configId + " AND " +
					"endTime = (SELECT max(endTime) FROM updateBank WHERE " +
					"ref_idconfiguration=" + configId + ")";
		Statement stat = connection.getStatement();
		
		Map<String, String> info = null;
		try {
			ResultSet result = connection.executeQuery(query, stat);
			if (result.next()) {
				info = getUpdate(result);
			
				if (result.next()) {
					SQLConnectionFactory.closeConnection(stat);
					throw new SQLException("More than 1 row was returned");
				}
			}
		} catch (SQLException ex) {
			SQLConnectionFactory.closeConnection(stat);
			ex.printStackTrace();
			return null;
		}
		SQLConnectionFactory.closeConnection(stat);
		return info;
	}
	
	/**
	 * Returns the id of the latest configuration that contains sessions.
	 * 
	 * @param bankId
	 * @param withUpdate if true, search for sessions that updated the repository
	 * 
	 * @return
	 */
	public static synchronized long getLatestConfigurationWithSession(int bankId, boolean withUpdate) {
		String query;
		if (withUpdate)
			query = "SELECT idconfiguration FROM configuration WHERE " +
					"ref_idbank=" + bankId + " AND idconfiguration IN " +
					"(SELECT ref_idconfiguration FROM updateBank WHERE isUpdated=true) " +
					"AND date=(SELECT max(date) FROM configuration WHERE " +
					"ref_idbank=" + bankId + " AND idconfiguration IN " +
					"(SELECT ref_idconfiguration FROM updateBank WHERE isUpdated=true));";
		else
			query = "SELECT idconfiguration FROM configuration WHERE " +
					"ref_idbank=" + bankId + " AND date=" +
					"(SELECT max(date) FROM configuration WHERE " +
					"ref_idbank=" + bankId + ");";
		
		Statement stat = connection.getStatement();
		try {
			ResultSet rs = connection.executeQuery(query, stat);
			if (rs.next()) {
				long l = rs.getLong(1);
				SQLConnectionFactory.closeConnection(stat);
				return  l;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		SQLConnectionFactory.closeConnection(stat);
		return -1;
	}
	
	/**
	 * Returns information about the latest update for the given bank that produced
	 * a production directory.
	 * 
	 * @param bankName
	 */
	public static synchronized Map<String, String> getLatestUpdateWithProductionDirectory(String bankName) {
//		Map<String, String> bankInfo = getBankInfo(bankName);
//		if (bankInfo == null)
//			return null;
//		
//		String configId = bankInfo.get(CONFIGURATION_ID);
		int bankId = getBankId(bankName);
		if (bankId < 0)
			return null;
		
		long configId = getLatestConfigurationWithSession(bankId, true);
		if (configId < 0)
			return null;
		
//		String query = "SELECT * FROM updateBank WHERE ref_idconfiguration=" + configId + " AND endTime = " +
//				"(SELECT max(endTime) FROM updateBank WHERE ref_idconfiguration=" + configId + " AND isUpdated=true) " +
//				"AND isUpdated=true AND idlastSession IN (SELECT session FROM productionDirectory);";
		
		String query = "SELECT * FROM updateBank WHERE ref_idconfiguration=" + configId + " AND endTime = " +
				"(SELECT max(endTime) FROM updateBank WHERE ref_idconfiguration=" + configId + " AND isUpdated=true AND productionDirectoryDeployed=true) " +
				"AND isUpdated=true AND productionDirectoryDeployed=true;";

		Statement stat = connection.getStatement();
		
		Map<String, String> info = null;
		try {
			ResultSet result = connection.executeQuery(query, stat);
			if (result.next())
				info = getUpdate(result);
			
			if (result.next()) {
				SQLConnectionFactory.closeConnection(stat);
				throw new SQLException("More than 1 row was returned");
			}
		} catch (SQLException ex) {
			SQLConnectionFactory.closeConnection(stat);
			ex.printStackTrace();
			return null;
		}
		SQLConnectionFactory.closeConnection(stat);
		return info;
	}
	
	/**
	 * Returns all the updates for a bank.
	 * 
	 * @param bankName
	 * @param withUpdate if true, retrieve only sessions that produced an update
	 * @return
	 */
	public static synchronized List<Map<String, String>> getBankUpdates(String bankName, boolean withUpdate) {
		
		/* Better query performance : tuco 24.10.2013
		String query;
		if (withUpdate)
			query = "SELECT * FROM updateBank WHERE isUpdated=true AND ";
		else
			query = "SELECT * FROM updateBank WHERE ";
		query += "ref_idconfiguration IN " +
				"(SELECT idconfiguration FROM configuration WHERE ref_idbank=" +
				"(SELECT idbank FROM bank WHERE name='" + bankName + "')) ORDER BY startTime DESC;";
		*/
		String query = "SELECT uB.* FROM updateBank uB " +
		    "JOIN configuration c ON c.idconfiguration = uB.ref_idconfiguration " +
		    "JOIN bank b ON b.idbank = c.ref_idbank " +
		    "WHERE b.name = '" + bankName + "' ";
		if (withUpdate)
		    query += "AND isUpdated = true ";
		query += "ORDER BY uB.startTime DESC";

		Statement stat = connection.getStatement();
		
		List<Map<String, String>> info = new ArrayList<Map<String,String>>();
		try {
			ResultSet result = connection.executeQuery(query, stat);
			while (result.next())
				info.add(getUpdate(result));
		} catch (SQLException ex) {
			SQLConnectionFactory.closeConnection(stat);
			ex.printStackTrace();
			return null;
		}

		SQLConnectionFactory.closeConnection(stat);
		return info;
	}
	
	
	private static Map<String, String> getUpdate(ResultSet result) {
		
		Map<String, String> map = new HashMap<String, String>();
		try {
			map.put(UPDATE_ID, String.valueOf(result.getInt(UPDATE_ID)));
			map.put(UPDATE_RELEASE, result.getString(UPDATE_RELEASE));
			map.put(PRODUCTION_DIR_PATH, result.getString(PRODUCTION_DIR_PATH));
			map.put(PRODUCTION_DIR_DEPLOYED, String.valueOf(result.getBoolean(PRODUCTION_DIR_DEPLOYED)));
			map.put(SIZE_DOWNLOAD, result.getString(SIZE_DOWNLOAD));
			map.put(SIZE_RELEASE, result.getString(SIZE_RELEASE));
			map.put(UPDATE_START, BiomajUtils.dateToString(new Date(result.getTimestamp(UPDATE_START).getTime()), Locale.US));
			if (result.getTimestamp(UPDATE_END) != null)
				map.put(UPDATE_END, BiomajUtils.dateToString(new Date(result.getTimestamp(UPDATE_END).getTime()), Locale.US));
			else
				map.put(UPDATE_END, "null");
			if (result.getString(UPDATE_ELAPSED) != null)
				map.put(UPDATE_ELAPSED,result.getString(UPDATE_ELAPSED));
			else
				map.put(UPDATE_ELAPSED, "null");
			map.put(UPDATED, result.getString(UPDATED));
			map.put(NB_SESSIONS, String.valueOf(result.getInt(NB_SESSIONS)));
			map.put(ID_LAST_SESSION, String.valueOf(result.getLong(ID_LAST_SESSION)));
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

		return map;
	}
	
	/**
	 * Returns the update according to the given lastSession.
	 * 
	 * @param lastSession
	 * @return
	 */
	public static synchronized Map<String, String> getUpdateFromId(long lastSession) {
		
		String query = "SELECT * FROM updateBank WHERE idLastSession=" + lastSession + ";";
		Statement stat = connection.getStatement();
		
		Map<String, String> info = null;
		try {
			ResultSet result = connection.executeQuery(query, stat);
			if (result.next())
				info = getUpdate(result);
			
			if (result.next()) {
				SQLConnectionFactory.closeConnection(stat);
				throw new SQLException("More than 1 row was returned");
			}
			
		} catch (SQLException ex) {
			SQLConnectionFactory.closeConnection(stat);
			ex.printStackTrace();
			return null;
		}

		SQLConnectionFactory.closeConnection(stat);
		return info;
	}
	
	
	/**
	 * Returns the session list for a given update.
	 * 
	 * @param updateBankId updateBank whose sessions are searched
	 * @return list of sessions stored as maps
	 */
	public static synchronized List<Map<String, String>> getUpdateSessions(int updateBankId) {

		List<Map<String, String>> sessions = new ArrayList<Map<String,String>>();
		
		String query = "SELECT * FROM session WHERE ref_idupdateBank=" + updateBankId + " ORDER BY session.startTime DESC";
		Statement stat = connection.getStatement();
		
		try {
			ResultSet result = connection.executeQuery(query, stat);
			while (result.next()) {
				Map<String, String> session = new HashMap<String, String>();
				
				session.put(SESSION_ID, String.valueOf(result.getLong(SESSION_ID)));
				session.put(HREF, result.getString(HREF));
				session.put(PARSE, result.getString(PARSE));
				session.put(SESSION_STATUS, String.valueOf(result.getBoolean(SESSION_STATUS)));
				session.put(SESSION_START, BiomajUtils.dateToString(new Date(result.getTimestamp(SESSION_START).getTime()), Locale.US));
				if (result.getTimestamp(SESSION_END) != null)
					session.put(SESSION_END, BiomajUtils.dateToString(new Date(result.getTimestamp(SESSION_END).getTime()), Locale.US));
				else
					session.put(SESSION_END, null);
//					session.put(SESSION_ELAPSED, BiomajUtils.dateToString(new Date(result.getTime(SESSION_ELAPSED).getTime()), Locale.FRANCE));
				session.put(SESSION_ELAPSED, result.getString(SESSION_ELAPSED));
				session.put(LOG_FILE, result.getString(LOG_FILE));
				
				sessions.add(session);
			}
		} catch (SQLException e) {
			SQLConnectionFactory.closeConnection(stat);
			e.printStackTrace();
			return null;
		}

		SQLConnectionFactory.closeConnection(stat);
		return sessions;
	}
	
	/**
	 * Returns the latest session date for a given bank.
	 * 
	 * @param bankId
	 * @return
	 */
	public static synchronized String getLatestSessionDate(int bankId) {
	    /* SQL Query improvment 
	       Tuco 23.10.2013
		String query = "SELECT max(startTime) FROM session WHERE ref_idupdateBank IN (" +
				"SELECT idupdateBank FROM updateBank WHERE ref_idconfiguration IN (" +
				"SELECT idconfiguration FROM configuration WHERE ref_idbank=" + bankId + "));";
	    */
	    String query = "SELECT max(s.startTime) FROM session s " +
		"JOIN updateBank uB ON uB.idupdateBank = s.ref_idupdateBank " +
		"JOIN configuration c ON c.idconfiguration = uB.ref_idconfiguration " +
		"WHERE c.ref_idbank = " + bankId;
		Statement stat = connection.getStatement();
		try {
			ResultSet rs = connection.executeQuery(query, stat);
			if (rs.next()) {
				String res = BiomajUtils.dateToString(new Date(rs.getTimestamp(1).getTime()), Locale.US);
				SQLConnectionFactory.closeConnection(stat);
				return res;
			}
		} catch (SQLException e) {
			SQLConnectionFactory.closeConnection(stat);
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Returns the lists of tasks for a given session.
	 * 
	 * @param sessionId
	 * @return list of tasks stored as maps
	 */
	public static synchronized List<Map<String, String>> getSessionTasks(long sessionId) {
		
		List<Map<String, String>> sessionTasks = new ArrayList<Map<String,String>>();
		/* SQL query improvment
		   Tuco  23.10.2013
		String query = "SELECT * from sessionTask WHERE idsessionTask IN " +
				"(SELECT ref_idsessionTask FROM session_has_sessionTask WHERE " +
				"ref_idsession =" + sessionId + ");";
		*/
		String query = "SELECT * FROM sessionTask sT " +
		    "JOIN session_has_sessionTask shsT ON shsT.ref_idsessionTask = sT.idsessionTask " +
		    "WHERE shsT.ref_idsession = " + sessionId;
		Statement stat = connection.getStatement();
		try {
			ResultSet result = connection.executeQuery(query, stat);
			while (result.next()) {
				Map<String, String> sessionTask = new HashMap<String, String>();
				
				sessionTask.put(TASK_ID, String.valueOf(result.getInt(TASK_ID)));
				sessionTask.put(TASK_START, BiomajUtils.dateToString(new Date(result.getTimestamp(TASK_START).getTime()), Locale.US));
				sessionTask.put(TASK_END, BiomajUtils.dateToString(new Date(result.getTimestamp(TASK_END).getTime()), Locale.US));
				sessionTask.put(TASK_ELAPSED, result.getString(TASK_ELAPSED));
				sessionTask.put(TASK_STATUS, result.getString(TASK_STATUS));
				sessionTask.put(VALUE, result.getString(VALUE));
				sessionTask.put(NB_EXTRACT, String.valueOf(result.getInt(NB_EXTRACT)));
				sessionTask.put(NB_LOCAL_OFFLINE_FILES, String.valueOf(result.getInt(NB_LOCAL_OFFLINE_FILES)));
				sessionTask.put(NB_LOCAL_ONLINE_FILES, String.valueOf(result.getInt(NB_LOCAL_ONLINE_FILES)));
				sessionTask.put(NB_DOWNLOADED_FILES, String.valueOf(result.getInt(NB_DOWNLOADED_FILES)));
				sessionTask.put(BANDWIDTH, String.valueOf(result.getFloat(BANDWIDTH)));
				sessionTask.put(NB_FILES_MOVED, String.valueOf(result.getInt(NB_FILES_MOVED)));
				sessionTask.put(NB_FILES_COPIED, String.valueOf(result.getInt(NB_FILES_COPIED)));
				sessionTask.put(NBRE_METAPROCESS, String.valueOf(result.getInt(NBRE_METAPROCESS)));
				sessionTask.put(TASK_TYPE, result.getString(TASK_TYPE));
				
				sessionTasks.add(sessionTask);
			}
			
		} catch (SQLException e) {
			SQLConnectionFactory.closeConnection(stat);
			e.printStackTrace();
			return null;
		}
		
		SQLConnectionFactory.closeConnection(stat);
		return sessionTasks;
	}
	
	/**
	 * Returns the files of a process.
	 * 
	 * @param processId
	 * @return
	 */
	public static synchronized List<Map<String, String>> getProcessFiles(int processId) {
		
		String query = "SELECT * from file WHERE ref_idprocess=" + processId;
		Statement stat = connection.getStatement();
		
		ResultSet result = connection.executeQuery(query, stat);
		List<Map<String, String>> res = getFiles(result);
		SQLConnectionFactory.closeConnection(stat);
		return res;
	}
	
	/**
	 * Returns the files of a task.
	 * 
	 * @param taskId
	 * @return
	 */
	public static synchronized List<Map<String, String>> getTaskFiles(int taskId) {
		
		List<Map<String, String>> res = new ArrayList<Map<String,String>>();
		
		String query = "SELECT ref_idfile FROM sessionTask_has_file WHERE " +
				"ref_idsessionTask=" + taskId;
		Statement stat = connection.getStatement();
		
		/*
		 * Some sessiontask can return a resultset tens of thousands rows large,
		 * which hsqldb seems to poorly handle.
		 * To speedup the thing we try to build a request in the form : SELECT ... WHERE idfile between x AND y
		 * instead of running several times the same SELECT ... WHERE idfile = z.
		 * To do so, we have to ensure that each file id is such as id(i+1) = id(i) + 1
		 */
		int first = -1;
		try {
			ResultSet rs = connection.executeQuery(query, stat);
			if (!rs.next()) {
				SQLConnectionFactory.closeConnection(stat);
				return res;
			}
			
			first = rs.getInt(1);
			
			int last = first;
			while (rs.next() && rs.getInt(1) == (last++) + 1);
			
			
			if (rs.next()) { // Not good, we have to do it the old way
				rs.first();
				try {
					Statement st = connection.getStatement();
					while (rs.next()) {
						res.add(getFile(connection.executeQuery("SELECT * FROM file WHERE idfile=" + rs.getInt(1), st)));
					}
					SQLConnectionFactory.closeConnection(st);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			} else { // All the ids are consecutive
				query = "SELECT * FROM file WHERE idfile>=" + first + " AND idfile<=" + last;
				Statement stat2 = connection.getStatement();
				List<Map<String, String>> filz = getFiles(connection.executeQuery(query, stat2));
				SQLConnectionFactory.closeConnection(stat2);
				SQLConnectionFactory.closeConnection(stat);
				return filz;
			}
		} catch (SQLException e1) {
			SQLConnectionFactory.closeConnection(stat);
			e1.printStackTrace();
		}
		SQLConnectionFactory.closeConnection(stat);
		return res;
	}
	
	
	/**
	 * Returns the files that were processed during the extract task
	 * of all the sessions.
	 *  
	 * @param bankName
	 * @return
	 */
	public static synchronized List<Map<String, String>> getAllSessionsExtractedFiles(String bankName) {
//		Map<String, String> info = getLatestUpdate(bankName, true);
//		
//		if (info == null)
//			return null;
//		
//		int updateId = Integer.valueOf(info.get(UPDATE_ID));
//		
//		String query = "SELECT * FROM file WHERE idfile IN (SELECT ref_idfile FROM sessionTask_has_file WHERE " +
//				"ref_idsessionTask=(SELECT idsessionTask FROM sessionTask WHERE taskType='extract' AND idsessionTask IN " +
//				"(SELECT ref_idsessionTask FROM session_has_sessionTask WHERE ref_idsession=" +
//				"(SELECT idsession FROM session WHERE startTime=(SELECT max(startTime) FROM session WHERE " +
//				"ref_idupdateBank=" + updateId + ") AND ref_idupdateBank = " + updateId + "))))";
//		
//		Statement stat = connection.getStatement();
//		ResultSet rs = connection.executeQuery(query, stat);
//		ResultSet result = stat.getResultSet();
//		
//		return getFiles(result);
		
		List<Map<String, String>> updates = getBankUpdates(bankName, true);
		List<Map<String, String>> filz = new ArrayList<Map<String,String>>();
		
		for (Map<String, String> update : updates) {
//			Map<String, String> update = getLatestUpdate(bankName, true);
		    /* SQL qeury improvment
		       Original query time : 1min23sec
		       New query time      : 0.12 sec
		       Tuco  24.10.2013
			String query = "SELECT * FROM file WHERE idfile IN (SELECT ref_idfile FROM sessionTask_has_file WHERE " +
					"ref_idsessionTask IN (SELECT idsessionTask FROM sessionTask WHERE taskType='makeRelease' AND idsessionTask IN " +
					"(SELECT ref_idsessionTask FROM session_has_sessionTask WHERE ref_idsession IN " +
					"(SELECT idsession FROM session WHERE ref_idupdateBank=" + update.get(UPDATE_ID) + "))))";
		    */
		    String query = "SELECT f.* FROM file f " +
			"JOIN sessionTask_has_file sThf ON sThf.ref_idfile = f.idfile " +
			"JOIN sessionTask sT ON sT.idsessionTask = sThf.ref_idsessionTask " +
			"JOIN session_has_sessionTask shsT ON shsT.ref_idsessionTask = sT.idsessionTask " +
			"JOIN session s on s.idsession = shsT.ref_idsession " +
			"WHERE sT.taskType='makeRelease' AND  s.ref_idupdateBank = " + update.get(UPDATE_ID);
			Statement stat = connection.getStatement();
			ResultSet rs = connection.executeQuery(query, stat);
			filz = getFiles(rs);
			SQLConnectionFactory.closeConnection(stat);
			if (filz.size() > 0)
				return filz;
		}
		
		return filz;
		
	}
	
	/**
	 * Returns the files from the resultset.
	 * 
	 * @param result
	 * @return
	 */
	public static synchronized List<Map<String, String>> getFiles(ResultSet result) {
		List<Map<String, String>> files = new ArrayList<Map<String,String>>();
		
		if (result != null) {
			try {
				while (result.next()) {
					Map<String, String> file = new HashMap<String, String>();
					
					file.put(FILE_ID, String.valueOf(result.getInt(FILE_ID)));
					file.put(FILE_LOCATION, result.getString(FILE_LOCATION));
					file.put(FILE_SIZE, String.valueOf(result.getLong(FILE_SIZE)));
					file.put(FILE_TIME, String.valueOf(result.getLong(FILE_TIME)));
					file.put(IS_LINK, String.valueOf(result.getBoolean(IS_LINK)));
					file.put(IS_EXTRACT, String.valueOf(result.getBoolean(IS_EXTRACT)));
					file.put(IS_VOLATILE, String.valueOf(result.getBoolean(IS_VOLATILE)));
					file.put(REF_HASH, result.getString(REF_HASH));
					file.put(FILE_TYPE, result.getString(FILE_TYPE));
					
					files.add(file);
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		} else		
			return null;
		
		return files;
	}
	
	private static Map<String, String> getFile(ResultSet result) {
		Map<String, String> file = null;
		
		try {
			if (result.next()) {
				file = new HashMap<String, String>();
				file.put(FILE_ID, String.valueOf(result.getInt(FILE_ID)));
				file.put(FILE_LOCATION, result.getString(FILE_LOCATION));
				file.put(FILE_SIZE, String.valueOf(result.getLong(FILE_SIZE)));
				file.put(FILE_TIME, String.valueOf(result.getLong(FILE_TIME)));
				file.put(IS_LINK, String.valueOf(result.getBoolean(IS_LINK)));
				file.put(IS_EXTRACT, String.valueOf(result.getBoolean(IS_EXTRACT)));
				file.put(IS_VOLATILE, String.valueOf(result.getBoolean(IS_VOLATILE)));
				file.put(REF_HASH, result.getString(REF_HASH));
				file.put(FILE_TYPE, result.getString(FILE_TYPE));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return file;
	}
	
	
	/**
	 * Returns the messages for a given task.
	 * Can be either ERROR message or WARNING message.
	 * 
	 * @param taskId
	 * @return list that contains first the warnings then the errors. 
	 */
	public static synchronized List<List<String>> getTaskMessages(int taskId) {
	    /* SQL query improvment
	       Original query time : 0.24sec
	       New query time      : 0.06
	       Tuco 24.10.2013
		String query = "SELECT * from message WHERE idmessage IN " +
				"(SELECT ref_idmessage FROM sessionTask_has_message WHERE " +
				"ref_idsessionTask =" + taskId + ");";
	    */
		String query = "SELECT m.* FROM message m " +
		    "JOIN sessionTask_has_message sThm ON sThm.ref_idmessage = m.idmessage " +
		    "WHERE sThm.ref_idsessionTask = " + taskId;
		Statement stat = connection.getStatement();

		ResultSet result = connection.executeQuery(query, stat);
		List<List<String>> mess = getMessages(result);
		SQLConnectionFactory.closeConnection(stat);
		return mess;
	
	}
	
	
	/**
	 * Returns the messages for a given process.
	 * Can be either ERROR message or WARNING message.
	 * 
	 * @param processId
	 * @return list that contains first the warnings then the errors. 
	 */
	public static synchronized List<List<String>> getProcessMessages(int processId) {
	    /* SQL query improvment
	       Original query time :
	       New query time      :
	       Tuco 24.10.2013
		String query = "SELECT * from message WHERE idmessage IN " +
				"(SELECT ref_idmessage FROM process_has_message WHERE " +
				"ref_idprocess =" + processId + ");";

	    */
	    String query = "SELECT m.* FROM message m " +
		"JOIN process_has_message phm ON phm.ref_idmessage = m.idmessage " +
		"WHERE phm.ref_idprocess = " + processId;
		Statement stat = connection.getStatement();
		ResultSet result = connection.executeQuery(query, stat);
		List<List<String>> mess = getMessages(result);
		SQLConnectionFactory.closeConnection(stat);
		return mess;

	}	
	
	
	/**
	 * Returns the metaprocesses for the given task.
	 * 
	 * @param taskId
	 * @return
	 */
	public static synchronized List<Map<String, String>> getTaskMetaprocesses(int taskId) {
		
		List<Map<String, String>> metaprocesses = new ArrayList<Map<String,String>>();
		
		String query = "SELECT * from metaprocess WHERE ref_idsessionTask=" + taskId;
		Statement stat = connection.getStatement();
		try {
			ResultSet result = connection.executeQuery(query, stat);
			while (result.next()) {
				Map<String, String> metaprocess = new HashMap<String, String>();
				
				metaprocess.put(META_ID, result.getString(META_ID));
				metaprocess.put(META_NAME, result.getString(META_NAME));
				metaprocess.put(META_START, BiomajUtils.dateToString(new Date(result.getTimestamp(META_START).getTime()), Locale.US));
				metaprocess.put(META_END, BiomajUtils.dateToString(new Date(result.getTimestamp(META_END).getTime()), Locale.US));
				metaprocess.put(META_ELAPSED, result.getString(META_ELAPSED));
				metaprocess.put(META_STATUS, result.getString(META_STATUS));
				metaprocess.put(META_LOGFILE, result.getString(META_LOGFILE));
				metaprocess.put(BLOCK, result.getString(BLOCK));
				
				metaprocesses.add(metaprocess);
			}
		} catch (SQLException e) {
			SQLConnectionFactory.closeConnection(stat);
			e.printStackTrace();
			return null;
		}
		
		SQLConnectionFactory.closeConnection(stat);
		return metaprocesses;
	}
	
	
	/**
	 * Returns the messages for a given session.
	 * Can be either ERROR message or WARNING message.
	 * 
	 * @param sessionId
	 * @return list that contains first the warnings then the errors. 
	 */
	public static synchronized List<List<String>> getSessionMessages(long sessionId) {
	    /* SQL query improvment 
	       Tuco 24.10.2013
		String query = "SELECT * from message WHERE idmessage IN " +
				"(SELECT ref_idmessage FROM session_has_message WHERE " +
				"ref_idsession =" + sessionId + ");";
	    */
	        String query = "SELECT m.* FROM message m " +
		    "JOIN session_has_message shm ON shm.ref_idmessage = m.idmessage " +
		    "WHERE shm.ref_idsession = " + sessionId;
		Statement stat = connection.getStatement();

		ResultSet result = connection.executeQuery(query, stat);
		List<List<String>> mess = getMessages(result);
		SQLConnectionFactory.closeConnection(stat);
		return mess;

	}
	
	
	/**
	 * Gets the messages from a resultset.
	 * 
	 * @param result sql object that contains the messages
	 * @return
	 */
	private static List<List<String>> getMessages(ResultSet result) {
		
		List<List<String>> messages = new ArrayList<List<String>>();
		List<String> errors = new ArrayList<String>();
		List<String> warnings = new ArrayList<String>();
		
		if (result != null) {
			try {
				while (result.next()) {
					String type = result.getString(MESSAGE_TYPE);
					if (type.equals("error"))
						errors.add(result.getString(MESSAGE));
					else if (type.equals("warning"))
						warnings.add(result.getString(MESSAGE));
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		} else		
			return null;
		
		messages.add(warnings);
		messages.add(errors);
		
		return messages;
		
	}
	
	/**
	 * Returns the messages for a given metaprocess.
	 * Can be either ERROR message or WARNING message.
	 * 
	 * @param metaId
	 * @return list that contains first the warnings then the errors. 
	 */
	public static synchronized List<List<String>> getMetaprocessMessages(String metaId) {
	    /* SQL query improvment
	       Tuco 24.10.2013
		String query = "SELECT * from message WHERE idmessage IN " +
				"(SELECT ref_idmessage FROM metaprocess_has_message WHERE " +
				"ref_idmetaprocess='" + metaId + "');";
	    */
	        String query = "SELECT m.* FROM message m " +
		    "JOIN metaprocess_has_message mphm ON mphm.ref_idmessage = m.idmessage " +
		    "WHERE mphm.ref_idmetaprocess = '" + metaId + "'";

		Statement stat = connection.getStatement();
		ResultSet result = connection.executeQuery(query, stat);
		List<List<String>> mess = getMessages(result);
		SQLConnectionFactory.closeConnection(stat);
		return mess;
	
	}
	
	
	/**
	 * Returns the processes of a given metaprocess.
	 * 
	 * @param metaId
	 * @return
	 */
	public static synchronized List<Map<String, String>> getMetaprocessProcesses(String metaId) {
		
		List<Map<String, String>> directories = new ArrayList<Map<String,String>>();
		
		String query = "SELECT * from process WHERE ref_idmetaprocess='" + metaId + "';";
		Statement stat = connection.getStatement();
		try {
			ResultSet result = connection.executeQuery(query, stat);
			while (result.next()) {
				Map<String, String> process = new HashMap<String, String>();
				
				process.put(PROC_ID, result.getString(PROC_ID));
				process.put(PROC_NAME, result.getString(PROC_NAME));
				process.put(PROC_KEYNAME, result.getString(PROC_KEYNAME));
				process.put(PROC_EXE, result.getString(PROC_EXE));
				process.put(PROC_ARGS, result.getString(PROC_ARGS));
				process.put(PROC_DESC, result.getString(PROC_DESC));
				process.put(PROC_TYPE, result.getString(PROC_TYPE));
				process.put(PROC_START, BiomajUtils.dateToString(new Date(result.getTimestamp(PROC_START).getTime()), Locale.US));
				if (result.getTimestamp(PROC_END) != null)
					process.put(PROC_END, BiomajUtils.dateToString(new Date(result.getTimestamp(PROC_END).getTime()), Locale.US));
				else
					process.put(PROC_END, "null");
				if (result.getString(PROC_ELAPSED) != null)
					process.put(PROC_ELAPSED, "null");
//					process.put(PROC_ELAPSED, BiomajUtils.timeToString(result.getTime(PROC_ELAPSED).getTime()));
				process.put(PROC_ELAPSED, result.getString(PROC_ELAPSED));
				process.put(PROC_ERROR, String.valueOf(result.getBoolean(PROC_ERROR)));
				process.put(PROC_TIMESTAMP, String.valueOf(result.getLong(PROC_TIMESTAMP)));
				process.put(PROC_VALUE, result.getString(PROC_VALUE));
				
				directories.add(process);
			}
		} catch (SQLException e) {
			SQLConnectionFactory.closeConnection(stat);
			e.printStackTrace();
			return null;
		}
		
		SQLConnectionFactory.closeConnection(stat);
		return directories;
	}
	
	
	/**
	 * Returns the list of production directories for the given bank.
	 * 
	 * @param bankName
	 * @return
	 */
	public static synchronized List<Map<String, String>> getProductionDirectories(String bankName) {
		
		List<Map<String, String>> directories = new ArrayList<Map<String,String>>();
		
		String query = "SELECT * from productionDirectory WHERE ref_idbank=" +
				"(SELECT idBank FROM bank WHERE name='" + bankName + "');";
		Statement stat = connection.getStatement();
		try {
			ResultSet result = connection.executeQuery(query, stat);
			while (result.next()) {
				Map<String, String> directory = new HashMap<String, String>();
				
				directory.put(DIR_ID, String.valueOf(result.getInt(DIR_ID)));
				if (result.getTimestamp(DIR_REMOVE) != null)
					directory.put(DIR_REMOVE, BiomajUtils.dateToString(new Date(result.getTimestamp(DIR_REMOVE).getTime()), Locale.US));
				else
					directory.put(DIR_REMOVE, "null");
				directory.put(DIR_CREATION, BiomajUtils.dateToString(new Date(result.getTimestamp(DIR_CREATION).getTime()), Locale.US));
				directory.put(DIR_SIZE, result.getString(DIR_SIZE));
				directory.put(DIR_STATE, result.getString(DIR_STATE));
				directory.put(DIR_SESSION, String.valueOf(result.getLong(DIR_SESSION)));
				directory.put(DIR_PATH, result.getString(DIR_PATH));
				
				directories.add(directory);
			}
		} catch (SQLException e) {
			SQLConnectionFactory.closeConnection(stat);
			e.printStackTrace();
			return null;
		}
		
		SQLConnectionFactory.closeConnection(stat);
		return directories;
	}
	
	public static synchronized List<ProductionDirectory> getAvailableProductionDirectories(String bankName) {
		List<Map<String, String>> directories = getProductionDirectories(bankName);
		List<ProductionDirectory> res = new ArrayList<ProductionDirectory>();
		
		for (Map<String, String> directory : directories) {
			ProductionDirectory pd = new ProductionDirectory(directory);
			
			if (pd.getState() != ProductionDirectory.REMOVE)
				res.add(pd);
		}
		
		Collections.sort(res);
		return res;
		
	}
	
	/**
	 * Returns all the directories as a list of production directories.
	 * Same as getProductionDirectories method.
	 * 
	 * @param bankName
	 * @return
	 */
	public static synchronized List<ProductionDirectory> getAllProductionDirectories(String bankName) {
		
		List<Map<String, String>> directories = getProductionDirectories(bankName);
		
		List<ProductionDirectory> res = new ArrayList<ProductionDirectory>();
		
		for (Map<String, String> directory : directories)
			res.add(new ProductionDirectory(directory));
		
		Collections.sort(res);
		
		return res;
	}
	
	
	/**
	 * Returns the names of the banks in the database.
	 * 
	 * @return
	 */
	public static synchronized List<String> getBanks() {
		List<String> banks = new ArrayList<String>();
		String query = "SELECT name FROM bank ORDER BY name";
		Statement stat = connection.getStatement();
		try {
			ResultSet rs = connection.executeQuery(query, stat);
			while (rs.next())
				banks.add(rs.getString(1));
		} catch (SQLException e) {
			SQLConnectionFactory.closeConnection(stat);
			e.printStackTrace();
			return null;
		}

		SQLConnectionFactory.closeConnection(stat);
		return banks;
	}
	
	/**
	 * Returns the number of configuration for the given bank.
	 * 
	 * @param bankName
	 * @return
	 */
	public static synchronized int getConfigCount(String bankName) {
		String query = "SELECT count(idconfiguration) FROM configuration WHERE ref_idbank=" +
				"(SELECT idbank FROM bank WHERE name='" + bankName + "')";
		Statement stat = connection.getStatement();
		try {
			ResultSet rs = connection.executeQuery(query, stat); 
			if (rs.next()) {
				int i = rs.getInt(1);
				SQLConnectionFactory.closeConnection(stat);
				return i;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		SQLConnectionFactory.closeConnection(stat);
		return -1;
	}
	
	/**
	 * Returns lastest production directory for the given bank based on its
	 * creation date.
	 * 
	 * @param bankName
	 * @return
	 */
	public static synchronized ProductionDirectory getLatestProductionDirectory(String bankName) {
		List<ProductionDirectory> pds = getAvailableProductionDirectories(bankName);
		ProductionDirectory ref = null;
		if (pds.size() > 0)
			ref = pds.get(0);
		for (ProductionDirectory pd : pds) {
			if (ref.getCreationDate().compareTo(pd.getCreationDate()) < 0)
				ref = pd;
		}
		
		return ref;
	}
	
	
	/**
	 * Renames the bank
	 * 
	 * @param oldName bank to rename
	 * @param newName new name
	 */
	public static synchronized void renameBank(String oldName, String newName) {
		String query = "UPDATE bank SET name='" + newName + "' WHERE name='" + oldName + "';";
		Statement stat = connection.getStatement();
		connection.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);
		
	}
	
	
	/**
	 * Updates the production directory path.
	 * 
	 * @param bankName
	 * @param oldPath path to change
	 * @param dir
	 */
	public static synchronized void updateProductionDirectory(String bankName, String oldPath, ProductionDirectory dir) {
		String query = "UPDATE productionDirectory " +
				"SET path='" + dir.getPath() + 
				"' WHERE path='" + oldPath +
				"' AND ref_idBank=(SELECT idbank FROM bank WHERE name='" + bankName + "');";
		
		Statement stat = connection.getStatement();
		connection.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);
		
	}
	
	/**
	 * Updates the versiondirectory path.
	 * 
	 * @param bankName
	 * @param oldPath
	 * @param newPath
	 */
	public static synchronized void updateVersionDirectory(String bankName, String oldPath, String newPath) {
		String query = "UPDATE localInfo SET versionDirectory='" + newPath + "' WHERE " +
				"idlocalInfo IN (SELECT ref_idlocalInfo FROM configuration WHERE ref_idbank=" +
				"(SELECT idbank FROM bank WHERE name='" + bankName + "')) AND versionDirectory='" +
				oldPath + "';";

		Statement stat = connection.getStatement();
		connection.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);
	}
	
	public static synchronized void moveFiles(String oldDir, String newDir) {
		
		/*
		String query = "UPDATE file set path='...' WHERE " +
				"idfile = (SELECT idfile FROM file WHERE ref_idprocess=" +
				"(SELECT idprocess FROM process WHERE ref_idmetaprocess=" +
				"(SELECT idmetaprocess FROM metaprocess WHERE ref_idsessionTask=" +
				"(SELECT ref_idsessionTask FROM session_has_sessionTask WHERE ref_idsession=" +
				"(SELECT idsession FROM session WHERE ref_idupdateBank=" +
				"(SELECT idupdateBank FROM updateBank WHERE ref_idconfiguration=" +
				"(SELECT idconfiguration FROM configuration WHERE ref_idbank=" +
				"(SELECT idbank FROM bank WHERE name='" + bankName + "'))))))));";*/
		
		
		// Retrieval of the files according to location
		String query = "SELECT location FROM file WHERE location like '" + oldDir + "%'";
		Statement stat = connection.getStatement();
		List<List<String>> paths = new ArrayList<List<String>>();
		
		// Building new locations
		try {
			ResultSet result = connection.executeQuery(query, stat);
			while (result.next()) {
				List<String> list = new ArrayList<String>();
				String old = result.getString(1);
				list.add(old);
				list.add(newDir + old.substring(oldDir.length()));
				paths.add(list);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		SQLConnectionFactory.closeConnection(stat);
		
		// Setting new locations
		for (List<String> path : paths) {
			query = "UPDATE file SET location='" + path.get(1) + "' WHERE location='" + path.get(0) + "'";
			stat = connection.getStatement();
			connection.executeUpdate(query, stat);
			SQLConnectionFactory.closeConnection(stat);
		}
	}
	
	public static synchronized int setDirectoryStateToDeleted(ProductionDirectory pd) {
		
		String query = "UPDATE productionDirectory SET state='" + ProductionDirectory.REMOVE_STR + "'," +
				"remove='" + BiomajUtils.dateToString(new Date(), Locale.US) + "' WHERE path='" +
				pd.getPath() + "' AND session=" + pd.getSession();
		
		Statement stat = connection.getStatement();
		int i = connection.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);
		return i;
	}
	
	
	/*
	 * 
	 * DELETION METHODS
	 * 
	 * 
	 */
	
	/**
	 * Deletes files that no longer exist.
	 */
	public static synchronized void deleteObsoleteFiles() {
		
		String query = "SELECT idfile, location FROM file";
		Statement stat = connection.getStatement();
		try {
			ResultSet result = connection.executeQuery(query, stat);
			while (result.next()) {
				String path = result.getString(2);
				if (!(new File(path).exists())) {
					query = "DELETE FROM file WHERE idfile=" + result.getInt(1);
					Statement stat2 = connection.getStatement();
					connection.executeUpdate(query, stat2);
					
					query = "DELETE FROM sessionTask_has_file WHERE ref_idfile=" + result.getInt(1);
					connection.executeUpdate(query, stat2);
					SQLConnectionFactory.closeConnection(stat2);
				}
			}
		} catch (SQLException e) {
			new BiomajException(e);
			e.printStackTrace();
		}
		SQLConnectionFactory.closeConnection(stat);
	}
	
	public static synchronized List<String> getBankTypes() {
		
		List<String> types = new ArrayList<String>();
		String query = "SELECT DISTINCT dbType FROM remoteInfo";
		Statement stat = connection.getStatement();
		try {
			ResultSet result = connection.executeQuery(query, stat);
			while (result.next())
				types.add(result.getString(1));
		} catch (SQLException ex) {
			SQLConnectionFactory.closeConnection(stat);
			ex.printStackTrace();
			return null;
		}
		SQLConnectionFactory.closeConnection(stat);
		return types;
		
	}
	
	/**
	 * Completely removes a bank from the database.
	 * 
	 * @param bankName
	 */
	public static synchronized void deleteAllFromBank(String bankName) {
		deleteBank(bankName);
		
		int bankId = getBankId(bankName);
		Statement stat = connection.getStatement();
		
		String query = "DELETE FROM productionDirectory WHERE ref_idbank=" + bankId;
		connection.executeUpdate(query, stat);
		
		
		/*
		 * Remoteinfo or localinfo cascade deletes configurations, so we need
		 * to retrieved all remoteinfo and localinfo ids
		 */
		query = "SELECT ref_idlocalInfo, ref_idremoteInfo FROM configuration WHERE ref_idbank=" + bankId;
		List<List<Long>> configIds = new ArrayList<List<Long>>(); 
		try {
			ResultSet rs = connection.executeQuery(query, stat);
			while (rs.next()) {
				List<Long> lst = new ArrayList<Long>();
				lst.add(rs.getLong(1));
				lst.add(rs.getLong(2));
				configIds.add(lst);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (configIds.size() > 0) {
			for (List<Long> ids : configIds) {
				String queryLocal = "DELETE FROM localInfo WHERE idlocalInfo=" + ids.get(0);
				String queryRemote = "DELETE FROM remoteInfo WHERE idremoteInfo=" + ids.get(1);
				
				connection.executeUpdate(queryLocal, stat);
				connection.executeUpdate(queryRemote, stat);
			}
			
		}

		// Delete configuration to double check
		query = "DELETE FROM configuration WHERE ref_idbank=" + bankId;
		connection.executeUpdate(query, stat);
		
		query = "DELETE FROM bank WHERE name='" + bankName + "'";
		connection.executeUpdate(query, stat);
		
		SQLConnectionFactory.closeConnection(stat);
		
	}
	
	
	/**
	 * Removes a bank from the database (all its updates and production directories).
	 * Bank, configuration, remoteinfo, localinfo records are kept.
	 * Production directories state are changed to 'deleted'.
	 * 
	 * @param bankName name of the bank to remove
	 */
	public static synchronized void deleteBank(String bankName) {
		
		/*
		 * - supprimer updatebank > cascade session
		 * - supprimer sessiontasks > cascade metaprocess > process > file
		 * - supprimer files sessiontasks
		 * - supprimer messages metaprocess
		 * - supprimer messages process
		 * - supprimer messages sessiontasks
		 * - supprimer messages session
		 * - supprimer productiondirectories
		 */
		
		String query = "SELECT idupdateBank FROM updateBank WHERE ref_idconfiguration IN " +
				"(SELECT idconfiguration FROM configuration WHERE ref_idbank = " +
				"(SELECT idbank FROM bank WHERE name = '" + bankName + "'));";
		
		Statement stat = connection.getStatement();
		try {
			ResultSet rsUpdateBank = connection.executeQuery(query, stat);
			while (rsUpdateBank.next()) { // UPDATEBANK
				int idUpdate = rsUpdateBank.getInt(1);
				deleteUpdate(idUpdate);
			}
			SQLConnectionFactory.closeConnection(stat);
			// Delete productionDirectories
//			query = "DELETE FROM productionDirectory WHERE ref_idbank=(" +
//					"SELECT idbank FROM bank WHERE name='" + bankName + "');";
			query = "UPDATE productionDirectory SET remove='" + BiomajUtils.dateToString(new Date(), Locale.US) + "'," +
					"state='" + ProductionDirectory.REMOVE_STR + "' WHERE ref_idbank=(" +
					"SELECT idbank FROM bank WHERE name='" + bankName + "') AND state='" + ProductionDirectory.AVAILABLE_STR + "';";
			stat = connection.getStatement();
			connection.executeUpdate(query, stat);
			SQLConnectionFactory.closeConnection(stat);
			
		} catch (SQLException e) {
			SQLConnectionFactory.closeConnection(stat);
			e.printStackTrace();
		}
	}
	
	/**
	 * Deletes an update and all its related elements : session, sessiontask,
	 * metaprocess, process, file, message.
	 * 
	 * @param updateId
	 */
	public static synchronized void deleteUpdate(int updateId) {
		
		String query = "SELECT idsession,logfile FROM session WHERE ref_idupdateBank = " + updateId;
		Statement stat = connection.getStatement();
		try {
			ResultSet rsSession = connection.executeQuery(query, stat);
			while (rsSession.next()) { // SESSION
				long idSession = rsSession.getLong(1);
				deleteSession(idSession, rsSession.getString(2));
			}
			SQLConnectionFactory.closeConnection(stat);
			// Delete updateBank > cascade session
			query = "DELETE FROM updateBank WHERE idupdateBank = " + updateId;
			stat = connection.getStatement();
			connection.executeUpdate(query, stat);
		} catch (SQLException e) {
			SQLConnectionFactory.closeConnection(stat);
			new BiomajException(e);
			e.printStackTrace();
		}
		SQLConnectionFactory.closeConnection(stat);
	}
	
	/**
	 * Delete session identified by its sessionId.
	 * 
	 * @param sessionId
	 */
	public static synchronized int deleteSession(long sessionId, String logFile) {
		
		String query = "SELECT ref_idsessionTask FROM session_has_sessionTask WHERE " +
				"ref_idsession = " + sessionId;
		Statement stat = connection.getStatement();
		try {
			ResultSet rsSessionTask = connection.executeQuery(query, stat);
			while (rsSessionTask.next()) { // SESSIONTASK
				int idTask = rsSessionTask.getInt(1);
				query = "SELECT logfile,idmetaprocess FROM metaprocess WHERE " +
						"ref_idsessionTask=" + idTask ;
				Statement stat2 = connection.getStatement();
				ResultSet rsMetaprocess = connection.executeQuery(query, stat2);
				while (rsMetaprocess.next()) { // METAPROCESS
					String idMeta = rsMetaprocess.getString(2);
					query = "SELECT idprocess FROM process WHERE " +
							"ref_idmetaprocess='" + idMeta + "'";
					Statement stat3 = connection.getStatement();
					ResultSet rsProcess = connection.executeQuery(query, stat3);
					while (rsProcess.next()) { // PROCESS
						int idProcess = rsProcess.getInt(1);
						// Messages process
						deleteProcessMessages(idProcess);
						deleteProcessFiles(idProcess);
					}
					SQLConnectionFactory.closeConnection(stat3);
					// Messages metaprocess
					deleteMetaprocessMessages(idMeta);
					
					if (rsMetaprocess.getString(1) != null) {
						File file = new File(rsMetaprocess.getString(1));
						if (file.exists())
							file.delete();
					}
				}
				SQLConnectionFactory.closeConnection(stat2);
				
				// Messages sessiontask
				deleteTaskMessages(idTask);
				// Delete files sessiontask on db
				deleteTaskFiles(idTask);
				// Delete files on disk
				/*
				  Tuco increase time execution
				query = "SELECT location FROM file WHERE idfile IN " +
						"(SELECT ref_idfile FROM sessionTask_has_file WHERE " +
						"ref_idsessionTask=" + idTask + ")";
				*/
				query = "SELECT location FROM file f " +
				    "JOIN sessionTask_has_file sThf ON f.idfile = sThf.ref_idfile " +
				    "WHERE sThf.ref_idsessionTask = " + idTask;
				Statement stat4 = connection.getStatement();
				deleteFiles(connection.executeQuery(query, stat4));
				SQLConnectionFactory.closeConnection(stat4);
				
				// Delete sessionTask
				query = "DELETE FROM sessionTask WHERE idsessionTask=" + idTask + ";";
				stat4 = connection.getStatement();
				connection.executeUpdate(query, stat4);
				SQLConnectionFactory.closeConnection(stat4);
			}
			SQLConnectionFactory.closeConnection(stat);
		} catch (SQLException ex) {
			new BiomajException(ex);
			ex.printStackTrace();
		}
		// Delete messages session
		deleteSessionMessages(sessionId);
		
		// Delete log
		if (logFile != null) {
			File log = new File(logFile);
			if (log.exists()) {
				log.delete();
				// Delete directory
				new File(log.getParent()).delete();
			}
		}
		
		query = "DELETE FROM session WHERE idsession=" + sessionId;
		Statement st = connection.getStatement();
		int count = connection.executeUpdate(query, st);
		SQLConnectionFactory.closeConnection(st);
		return count;
		
	}
	
	/**
	 * Deletes  the files related to the given process.
	 * 
	 * @param processId
	 */
	private static void deleteProcessFiles(int processId) {
		String query = "SELECT location FROM file WHERE ref_idprocess=" + processId;
		Statement stat = connection.getStatement();
		deleteFiles(connection.executeQuery(query, stat));
		SQLConnectionFactory.closeConnection(stat);
	}
	
	/**
	 * Deletes the files related to the task identified by
	 * the given id.
	 * 
	 * @param idTask
	 */
	public static synchronized void deleteTaskFiles(long idTask) {		
	    /* SQL query improvment, run much faster
	       Tuco : 23.10.2013
		String query = "DELETE FROM file WHERE idfile IN " +
				"(SELECT ref_idfile FROM sessionTask_has_file " +
				"WHERE ref_idsessionTask=" + idTask + ");";
	    */
	    String query = "DELETE f FROM file f " +
		"JOIN sessionTask_has_file sThf ON sThf.ref_idfile = f.idfile " +
		"WHERE sThf.ref_idsessionTask = " + idTask;
		Statement stat = connection.getStatement();
		connection.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);
	}
	
	/**
	 * Deletes file from the file system.
	 * 
	 * @param result
	 */
	private static void deleteFiles(ResultSet result) {
		try {
			while (result.next()) {
				File file = new File(result.getString(1));
				if (file.exists())
					file.delete();
			}
		} catch (SQLException ex) {
			new BiomajException(ex);
			ex.printStackTrace();
		}
	}
	
	/**
	 * Deletes the messages that are related to the metaprocess identified by 
	 * the given id and that are not related to any other table.
	 * 
	 * @param idMeta
	 */
	private static void deleteMetaprocessMessages(String idMeta) {
	    /* SQL query improvment
	       Tuco 23.10.2013
		String query = "DELETE FROM message WHERE idmessage IN " +
				"(SELECT ref_idmessage FROM metaprocess_has_message " +
				"WHERE ref_idmetaprocess='" + idMeta + "');";
	    */
	    String query = "DELETE m FROM message m " + 
		"JOIN metaprocess_has_message mphm on mphm.ref_idmessage = m.idmessage " +
		"WHERE mphm.ref_idmetaprocess = '" + idMeta + "'";
		Statement stat = connection.getStatement();
		connection.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);
	}
	
	
	/**
	 * Deletes the messages that are related to the process identified by 
	 * the given id and that are not related to any other table.
	 * 
	 * @param idMeta
	 */
	public static synchronized void deleteProcessMessages(long idProcess) {
	    /* SQL query improvment
	       Tuco 24.10.2013
		String query = "DELETE FROM message WHERE idmessage IN " +
				"(SELECT ref_idmessage FROM process_has_message " +
				"WHERE ref_idprocess=" + idProcess + ");";
	    */
	        String query = "DELETE m FROM message m " + 
		    "JOIN process_has_message phm ON phm.ref_idmessage = m.idmessage " +
		    "WHERE phm.ref_idprocess = " + idProcess;
		Statement stat = connection.getStatement();
		connection.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);		
	}
	
	/**
	 * Deletes the messages that are related to the sessionTask identified by 
	 * the given id and that are not related to any other table.
	 * 
	 * @param idMeta
	 */
	public static synchronized void deleteTaskMessages(long idTask) {
	    /* SQL query improvment 
	       Tuco 24.10.2013
		String query = "DELETE FROM message WHERE idmessage IN " +
				"(SELECT ref_idmessage FROM sessionTask_has_message " +
				"WHERE ref_idsessionTask=" + idTask + ");";
	    */
	        String query = "DELETE m FROM message m " +
		    "JOIN sessionTask_has_message sThm ON sThm.ref_idmessage = m.idmessage " +
		    "WHERE sThm.ref_idsessionTask = " + idTask;
		Statement stat = connection.getStatement();
		connection.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);
	}
	
	public static synchronized void deleteSessionMessages(long idSession) {
	    /* SQL query improvment
	       Tuco 24.10.2013
		String query = "DELETE FROM message WHERE idmessage IN " +
				"(SELECT ref_idmessage FROM session_has_message " +
				"WHERE ref_idsession=" + idSession + ");";
	    */
	        String query = "DELETE m FROM message m  " +
		    "JOIN session_has_message shm ON shm.ref_idmessage = m.idmessage " +
		    "WHERE shm.ref_idsession = " + idSession;
		Statement stat = connection.getStatement();
		connection.executeUpdate(query, stat);
		SQLConnectionFactory.closeConnection(stat);
	}
	
	public static synchronized void deleteRemovedDirectoryRelatedRecords(int productionDirectoryId) {
		
		BiomajLogger.getInstance().log("Deletion of removed directory related records", BiomajLogger.INFO_LEVEL);
		
		SQLConnection connection = SQLConnectionFactory.getConnection();
		Statement stat = connection.getStatement();
		// Delete sessionTask files
		
		/* Tuco 23/10.2013 : Improved SQL query to increase execution time
		   This long time execution caused 'Lock wait timeout exceeded' error
		   if more than one bank tried to execute the query to delete files
		   row from the database.

		             Original query time execution : > 55 sec
			     New query time execution      : 0.10 sec

		String query = "DELETE FROM file WHERE idfile IN " +
				"(SELECT ref_idfile FROM sessionTask_has_file WHERE ref_idsessionTask IN " +
				"(SELECT ref_idsessionTask FROM session_has_sessionTask WHERE ref_idsession=" +
				"(SELECT session FROM productionDirectory WHERE idproductionDirectory=" + productionDirectoryId +
				")))";

		*/
		String query = "DELETE f FROM file f " + 
		    "JOIN sessionTask_has_file sThf ON sThf.ref_idfile = f.idfile " +
		    "JOIN sessionTask sT ON sT.idsessionTask = sThf.ref_idsessionTask " +
		    "JOIN session_has_sessionTask shsT ON shsT.ref_idsessionTask = sT.idsessionTask " +
		    "JOIN productionDirectory pD ON pD.session = shsT.ref_idsession " +
		    "WHERE idproductionDirectory = " + productionDirectoryId;

		BiomajLogger.getInstance().log("Deleting sessionTask files", BiomajLogger.INFO_LEVEL);
		connection.executeUpdate(query, stat);
		
		// Delete process files
		/* Original query Time execution : 12 < time > 16 sec
		   New query time execution      : 0.17 sec
		query = "DELETE FROM file WHERE ref_idprocess IN " +
				"(SELECT idprocess FROM process WHERE ref_idmetaprocess IN " +
				"(SELECT idmetaprocess FROM metaprocess WHERE ref_idsessionTask IN " +
				"(SELECT ref_idsessionTask FROM session_has_sessionTask WHERE ref_idsession=" +
				"(SELECT session FROM productionDirectory WHERE idproductionDirectory=" + productionDirectoryId +
				"))))";
		*/
		query = "DELETE f FROM file f " +
		    "JOIN process p ON p.idprocess = f.ref_idprocess " +
		    "JOIN metaprocess mp ON mp.idmetaprocess = p.ref_idmetaprocess " +
		    "JOIN session_has_sessionTask shsT ON shsT.ref_idsessionTask = mp.ref_idsessionTask " +
		    "JOIN productionDirectory pD ON pD.session = shsT.ref_idsession " +
		    "WHERE pD.idproductionDirectory = " + productionDirectoryId;

		BiomajLogger.getInstance().log("Deleting process files", BiomajLogger.INFO_LEVEL);
		connection.executeUpdate(query, stat);
		
		// Process messages
		/* Original query Time execution : 0.08 sec
		   New query time execution      : 0.0  sec
		query = "DELETE FROM message WHERE idmessage IN " +
				"(SELECT ref_idmessage FROM process_has_message WHERE ref_idprocess IN " +
				"(SELECT idprocess FROM process WHERE ref_idmetaprocess IN " +
				"(SELECT idmetaprocess FROM metaprocess WHERE ref_idsessionTask IN " +
				"(SELECT ref_idsessionTask FROM session_has_sessionTask WHERE ref_idsession=" +
				"(SELECT session FROM productionDirectory WHERE idproductionDirectory=" + productionDirectoryId +
				")))))";
		*/
		query = "DELETE m FROM message m " +
		    "JOIN process_has_message phm ON phm.ref_idmessage = m.idmessage " +
		    "JOIN process p ON p.idprocess = phm.ref_idprocess " +
		    "JOIN metaprocess mp ON mp.idmetaprocess = p.ref_idmetaprocess " +
		    "JOIN session_has_sessionTask shsT ON shsT.ref_idsessionTask = mp.ref_idsessionTask " +
		    "JOIN productionDirectory pD ON pD.session = shsT.ref_idsessionTask " +
		    "WHERE pD.idproductionDirectory = " + productionDirectoryId;
		BiomajLogger.getInstance().log("Deleting process messages", BiomajLogger.INFO_LEVEL);
		connection.executeUpdate(query, stat);
		
		// Metaprocess messages
		/*
		  Original query : 0.02 sec (empty set)
		  New query      : 0.02 sec (empty set)
		query = "DELETE FROM message WHERE idmessage IN " +
				"(SELECT ref_idmessage FROM metaprocess_has_message WHERE ref_idmetaprocess IN " +
				"(SELECT idmetaprocess FROM metaprocess WHERE ref_idsessionTask IN " +
				"(SELECT ref_idsessionTask FROM session_has_sessionTask WHERE ref_idsession=" +
				"(SELECT session FROM productionDirectory WHERE idproductionDirectory=" + productionDirectoryId +
				"))))";
		*/
		query = "DELETE m FROM message m " +
		    "JOIN metaprocess_has_message mphm ON mphm.ref_idmessage = m.idmessage " +
		    "JOIN metaprocess mp ON mp.idmetaprocess = mphm.ref_idmetaprocess " +
		    "JOIN session_has_sessionTask shsT ON shsT.ref_idsessionTask = mp.ref_idsessionTask " +
		    "JOIN productionDirectory pD ON pD.session = shsT.ref_idsession " +
		    "WHERE pD.idproductionDirectory = " + productionDirectoryId;

		BiomajLogger.getInstance().log("Deleting metaprocess messages", BiomajLogger.INFO_LEVEL);
		connection.executeUpdate(query, stat);
		
		// SessionTask messages
		/*
		  Original query : 0.04 sec (1 row)
		  New query      : 0.00 sec (1 row)
		query = "DELETE FROM message WHERE idmessage IN " +
				"(SELECT ref_idmessage FROM sessionTask_has_message WHERE ref_idsessionTask IN " +
				"(SELECT ref_idsessionTask FROM session_has_sessionTask WHERE ref_idsession=" +
				"(SELECT session FROM productionDirectory WHERE idproductionDirectory=" + productionDirectoryId +
				")))";
		*/
		query = "DELETE m FROM message m " +
		    "JOIN sessionTask_has_message sThm ON sThm.ref_idmessage = m.idmessage " +
		    "JOIN session_has_sessionTask shsT ON shsT.ref_idsessionTask = sThm.ref_idsessionTask " +
		    "JOIN productionDirectory pD ON pD.session = shsT.ref_idsession " +
		    "WHERE pD.idproductionDirectory = " + productionDirectoryId;

		BiomajLogger.getInstance().log("Deleting sessionTask messages", BiomajLogger.INFO_LEVEL);
		connection.executeUpdate(query, stat);
		
		// Session messages
		/*
		  Original query : 0.12 sec (1 row)
		  New query      : 0.00 sec (1 row)
		query = "DELETE FROM message WHERE idmessage IN " +
				"(SELECT ref_idmessage FROM session_has_message WHERE ref_idsession=" +
				"(SELECT session FROM productionDirectory WHERE idproductionDirectory=" + productionDirectoryId +
				"))";
		*/
		query = "DELETE m FROM message m " +
		    "JOIN session_has_message shm ON shm.ref_idmessage = m.idmessage " +
		    "JOIN productionDirectory pD ON pD.session = shm.ref_idsession " +
		    "WHERE pD.idproductionDirectory = " + productionDirectoryId;
		BiomajLogger.getInstance().log("Deleting session messages", BiomajLogger.INFO_LEVEL);
		connection.executeUpdate(query, stat);
		
		SQLConnectionFactory.closeConnection(stat);
	}

}
