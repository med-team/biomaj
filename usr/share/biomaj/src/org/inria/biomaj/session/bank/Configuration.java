/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.session.bank;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


public class Configuration {

	/**
	 * Initializes a configuration from a map.
	 * 
	 * @param info
	 */
	public Configuration(Map<String, String> info) {
		
		setName(info.get(BiomajSQLQuerier.BANK_NAME));
		setFullName(info.get(BiomajSQLQuerier.DB_FULLNAME)); 
		setDate(info.get(BiomajSQLQuerier.DATE));
		setUrl(info.get(BiomajSQLQuerier.SERVER));
		setProtocol(info.get(BiomajSQLQuerier.PROTOCOL));
		setRemoteDirectory(info.get(BiomajSQLQuerier.REMOTE_DIR));
		setRemoteFilesRegexp(info.get(BiomajSQLQuerier.REMOTE_FILES));
		setRemoteExcludedFiles(info.get(BiomajSQLQuerier.REMOTE_EXCLUDED_FILES));
		setLocalFilesRegexp(info.get(BiomajSQLQuerier.LOCAL_FILES));
		setVersionDirectory(info.get(BiomajSQLQuerier.VERSION_DIRECTORY));
		setOfflineDirectory(info.get(BiomajSQLQuerier.OFFLINE_DIRECTORY));
		setLogFiles(info.get(BiomajSQLQuerier.LOG_FILE));
		setFrequencyUpdate(info.get(BiomajSQLQuerier.FREQUENCY));
		setDoLinkCopy(info.get(BiomajSQLQuerier.DO_LINKCOPY));
		setPort(info.get(BiomajSQLQuerier.PORT));
		setTypeBank(info.get(BiomajSQLQuerier.DB_TYPE));
		setReleaseRegExp(info.get(BiomajSQLQuerier.RELEASE_REGEXP));
		setReleaseFile(info.get(BiomajSQLQuerier.RELEASE_FILE));
		setNbVersionManagement(info.get(BiomajSQLQuerier.N_VERSIONS));
		setId(Long.valueOf(info.get(BiomajSQLQuerier.CONFIGURATION_ID)));
		setPropertyFile(info.get(BiomajSQLQuerier.FILE));
		setLogFiles(info.get(BiomajSQLQuerier.HAS_LOG_FILE));
	}
	
	public Configuration() {
	}
	
	
	/**
	 * @uml.property  name="id"
	 */
	private Long id = new Long(-1);

	/**
	 * Getter of the property <tt>id</tt>
	 * @return  Returns the id.
	 * @uml.property  name="id"
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Setter of the property <tt>id</tt>
	 * @param id  The id to set.
	 * @uml.property  name="id"
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @uml.property  name="date"
	 */
	private String date = "";

	/**
	 * Getter of the property <tt>date</tt>
	 * @return  Returns the date.
	 * @uml.property  name="date"
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Setter of the property <tt>date</tt>
	 * @param date  The date to set.
	 * @uml.property  name="date"
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @uml.property  name="propertyFile"
	 */
	private String propertyFile = "";

	/**
	 * Getter of the property <tt>propertyFile</tt>
	 * @return  Returns the propertyFile.
	 * @uml.property  name="propertyFile"
	 */
	public String getPropertyFile() {
		return propertyFile;
	}

	/**
	 * Setter of the property <tt>propertyFile</tt>
	 * @param propertyFile  The propertyFile to set.
	 * @uml.property  name="propertyFile"
	 */
	public void setPropertyFile(String propertyFile) {
		this.propertyFile = propertyFile;
	}

	/**
	 * @uml.property  name="fullName"
	 */
	private String fullName = "";

	/**
	 * Getter of the property <tt>fullName</tt>
	 * @return  Returns the fullName.
	 * @uml.property  name="fullName"
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * Setter of the property <tt>fullName</tt>
	 * @param fullName  The fullName to set.
	 * @uml.property  name="fullName"
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @uml.property  name="protocol"
	 */
	private String protocol = "";

	/**
	 * Getter of the property <tt>protocol</tt>
	 * @return  Returns the protocol.
	 * @uml.property  name="protocol"
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * Setter of the property <tt>protocol</tt>
	 * @param protocol  The protocol to set.
	 * @uml.property  name="protocol"
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	/**
	 * @uml.property  name="port"
	 */
	private String port = "";
	
	/**
	 * @uml.property  name="username"
	 */
	private String username = "";
	
	/**
	 * @uml.property  name="password"
	 */
	private String password = "";
	
	/**
	 * @uml.property  name="fileNumThread"
	 */
	private String fileNumThread = "";
	
	/**
	 * @uml.property  name="doLinkCopy"
	 */
	private String doLinkCopy = "";
	
	/**
	 * @uml.property  name="logFiles"
	 */
	private String logFiles = "";
	
	/**
	 * @uml.property  name="noExtract"
	 */
	private String noExtract = "";
	
	/**
	 * @uml.property  name="remoteExcludedFiles"
	 */
	private String remoteExcludedFiles = "";
	
	private Vector<String> blocks = new Vector<String>();
	
	private HashMap<String,Vector<String>> metaProcess = new HashMap<String,Vector<String>>();
	
	private HashMap<String,Vector<String>> process = new HashMap<String,Vector<String>>();
	
	private Vector<String> formats = new Vector<String>();
	
	/**
	 * @uml.property  name="url"
	 */
	private String url = "";

	/**
	 * Getter of the property <tt>url</tt>
	 * @return  Returns the url.
	 * @uml.property  name="url"
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Setter of the property <tt>url</tt>
	 * @param url  The url to set.
	 * @uml.property  name="url"
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @uml.property  name="typeBank"
	 */
	private String typeBank = "";

	/**
	 * Getter of the property <tt>typeBank</tt>
	 * @return  Returns the typeBank.
	 * @uml.property  name="typeBank"
	 */
	public String getTypeBank() {
		return typeBank;
	}

	/**
	 * Setter of the property <tt>typeBank</tt>
	 * @param typeBank  The typeBank to set.
	 * @uml.property  name="typeBank"
	 */
	public void setTypeBank(String typeBank) {
		this.typeBank = typeBank;
	}

	/**
	 * @uml.property  name="remoteDirectory"
	 */
	private String remoteDirectory = "";

	/**
	 * Getter of the property <tt>remoteDirectory</tt>
	 * @return  Returns the remoteDirectory.
	 * @uml.property  name="remoteDirectory"
	 */
	public String getRemoteDirectory() {
		return remoteDirectory;
	}

	/**
	 * Setter of the property <tt>remoteDirectory</tt>
	 * @param remoteDirectory  The remoteDirectory to set.
	 * @uml.property  name="remoteDirectory"
	 */
	public void setRemoteDirectory(String remoteDirectory) {
		this.remoteDirectory = remoteDirectory;
	}

	/**
	 * @uml.property  name="offlineDirectory"
	 */
	private String offlineDirectory = "";

	/**
	 * Getter of the property <tt>offlineDirectory</tt>
	 * @return  Returns the offlineDirectory.
	 * @uml.property  name="offlineDirectory"
	 */
	public String getOfflineDirectory() {
		return offlineDirectory;
	}

	/**
	 * Setter of the property <tt>offlineDirectory</tt>
	 * @param offlineDirectory  The offlineDirectory to set.
	 * @uml.property  name="offlineDirectory"
	 */
	public void setOfflineDirectory(String offlineDirectory) {
		this.offlineDirectory = offlineDirectory;
	}

	/**
	 * @uml.property  name="versionDirectory"
	 */
	private String versionDirectory = "";

	/**
	 * Getter of the property <tt>versionDirectory</tt>
	 * @return  Returns the versionDirectory.
	 * @uml.property  name="versionDirectory"
	 */
	public String getVersionDirectory() {
		return versionDirectory;
	}

	/**
	 * Setter of the property <tt>versionDirectory</tt>
	 * @param versionDirectory  The versionDirectory to set.
	 * @uml.property  name="versionDirectory"
	 */
	public void setVersionDirectory(String versionDirectory) {
		this.versionDirectory = versionDirectory;
	}

	/**
	 * @uml.property  name="frequencyUpdate"
	 */
	private String frequencyUpdate = "";

	/**
	 * Getter of the property <tt>frequencyUpdate</tt>
	 * @return  Returns the frequencyUpdate.
	 * @uml.property  name="frequencyUpdate"
	 */
	public String getFrequencyUpdate() {
		return frequencyUpdate;
	}

	/**
	 * Setter of the property <tt>frequencyUpdate</tt>
	 * @param frequencyUpdate  The frequencyUpdate to set.
	 * @uml.property  name="frequencyUpdate"
	 */
	public void setFrequencyUpdate(String frequencyUpdate) {
		this.frequencyUpdate = frequencyUpdate;
	}

	/**
	 * @uml.property  name="releaseFile"
	 */
	private String releaseFile = "";

	/**
	 * Getter of the property <tt>releaseFile</tt>
	 * @return  Returns the releaseFile.
	 * @uml.property  name="releaseFile"
	 */
	public String getReleaseFile() {
		return releaseFile;
	}

	/**
	 * Setter of the property <tt>releaseFile</tt>
	 * @param releaseFile  The releaseFile to set.
	 * @uml.property  name="releaseFile"
	 */
	public void setReleaseFile(String releaseFile) {
		this.releaseFile = releaseFile;
	}

	/**
	 * @uml.property  name="releaseRegExp"
	 */
	private String releaseRegExp = "";

	/**
	 * Getter of the property <tt>releaseRegExp</tt>
	 * @return  Returns the releaseRegExp.
	 * @uml.property  name="releaseRegExp"
	 */
	public String getReleaseRegExp() {
		return releaseRegExp;
	}

	/**
	 * Setter of the property <tt>releaseRegExp</tt>
	 * @param releaseRegExp  The releaseRegExp to set.
	 * @uml.property  name="releaseRegExp"
	 */
	public void setReleaseRegExp(String releaseRegExp) {
		this.releaseRegExp = releaseRegExp;
	}

	/**
	 * @uml.property  name="nbVersionManagement"
	 */
	private String nbVersionManagement = "";

	/**
	 * Getter of the property <tt>nbVersionManagement</tt>
	 * @return  Returns the nbVersionManagement.
	 * @uml.property  name="nbVersionManagement"
	 */
	public String getNbVersionManagement() {
		return nbVersionManagement;
	}

	/**
	 * Setter of the property <tt>nbVersionManagement</tt>
	 * @param nbVersionManagement  The nbVersionManagement to set.
	 * @uml.property  name="nbVersionManagement"
	 */
	public void setNbVersionManagement(String nbVersionManagement) {
		this.nbVersionManagement = nbVersionManagement;
	}

	/**
	 * @uml.property  name="name"
	 */
	private String name = "";

	/**
	 * Getter of the property <tt>name</tt>
	 * @return  Returns the name.
	 * @uml.property  name="name"
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter of the property <tt>name</tt>
	 * @param name  The name to set.
	 * @uml.property  name="name"
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @uml.property  name="remoteFilesRegexp"
	 */
	private String remoteFilesRegexp = "";

	/**
	 * Getter of the property <tt>remoteFilesRegexp</tt>
	 * @return  Returns the remoteFilesRegexp.
	 * @uml.property  name="remoteFilesRegexp"
	 */
	public String getRemoteFilesRegexp() {
		return remoteFilesRegexp;
	}

	/**
	 * Setter of the property <tt>remoteFilesRegexp</tt>
	 * @param remoteFilesRegexp  The remoteFilesRegexp to set.
	 * @uml.property  name="remoteFilesRegexp"
	 */
	public void setRemoteFilesRegexp(String remoteFilesRegexp) {
		this.remoteFilesRegexp = remoteFilesRegexp;
	}

	/**
	 * @uml.property  name="localFilesRegexp"
	 */
	private String localFilesRegexp = "";

	/**
	 * Getter of the property <tt>localFilesRegexp</tt>
	 * @return  Returns the localFilesRegexp.
	 * @uml.property  name="localFilesRegexp"
	 */
	public String getLocalFilesRegexp() {
		return localFilesRegexp;
	}

	/**
	 * Setter of the property <tt>localFilesRegexp</tt>
	 * @param localFilesRegexp  The localFilesRegexp to set.
	 * @uml.property  name="localFilesRegexp"
	 */
	public void setLocalFilesRegexp(String localFilesRegexp) {
		this.localFilesRegexp = localFilesRegexp;
	}

	/**
	 * @return  the port
	 * @uml.property  name="port"
	 */
	public String getPort() {
		return port;
	}

	/**
	 * @param port  the port to set
	 * @uml.property  name="port"
	 */
	public void setPort(String port) {
		this.port = port;
	}

	public Vector<String> getBlocks() {
		return blocks;
	}

	public void setBlocks(Vector<String> blocks) {
		this.blocks = blocks;
	}

	/**
	 * @return  the doLinkCopy
	 * @uml.property  name="doLinkCopy"
	 */
	public String getDoLinkCopy() {
		return doLinkCopy;
	}

	/**
	 * @param doLinkCopy  the doLinkCopy to set
	 * @uml.property  name="doLinkCopy"
	 */
	public void setDoLinkCopy(String doLinkCopy) {
		this.doLinkCopy = doLinkCopy;
	}

	/**
	 * @return  the fileNumThread
	 * @uml.property  name="fileNumThread"
	 */
	public String getFileNumThread() {
		return fileNumThread;
	}

	/**
	 * @param fileNumThread  the fileNumThread to set
	 * @uml.property  name="fileNumThread"
	 */
	public void setFileNumThread(String fileNumThread) {
		this.fileNumThread = fileNumThread;
	}

	/**
	 * @return  the logFiles
	 * @uml.property  name="logFiles"
	 */
	public String getLogFiles() {
		return logFiles;
	}

	/**
	 * @param logFiles  the logFiles to set
	 * @uml.property  name="logFiles"
	 */
	public void setLogFiles(String logFiles) {
		this.logFiles = logFiles;
	}

	public HashMap<String, Vector<String>> getMetaProcess() {
		return metaProcess;
	}

	public void setMetaProcess(HashMap<String, Vector<String>> metaProcess) {
		this.metaProcess = metaProcess;
	}

	/**
	 * @return  the noExtract
	 * @uml.property  name="noExtract"
	 */
	public String getNoExtract() {
		return noExtract;
	}

	/**
	 * @param noExtract  the noExtract to set
	 * @uml.property  name="noExtract"
	 */
	public void setNoExtract(String noExtract) {
		this.noExtract = noExtract;
	}

	/**
	 * @return  the password
	 * @uml.property  name="password"
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password  the password to set
	 * @uml.property  name="password"
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	public HashMap<String, Vector<String>> getProcess() {
		return process;
	}

	public void setProcess(HashMap<String, Vector<String>> process) {
		this.process = process;
	}

	/**
	 * @return  the remoteExcludedFiles
	 * @uml.property  name="remoteExcludedFiles"
	 */
	public String getRemoteExcludedFiles() {
		return remoteExcludedFiles;
	}

	/**
	 * @param remoteExcludedFiles  the remoteExcludedFiles to set
	 * @uml.property  name="remoteExcludedFiles"
	 */
	public void setRemoteExcludedFiles(String remoteExcludedFiles) {
		if (remoteExcludedFiles != null && remoteExcludedFiles.equals("null"))
			this.remoteExcludedFiles = "";
		else
			this.remoteExcludedFiles = remoteExcludedFiles;
	}

	/**
	 * @return  the username
	 * @uml.property  name="username"
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username  the username to set
	 * @uml.property  name="username"
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	public Vector<String> getFormats() {
		return formats;
	}

	public void setFormats(Vector<String> formats) {
		this.formats = formats;
	}

}
