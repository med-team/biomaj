/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.session.process;

import java.io.File;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;
import org.apache.tools.ant.Project;
import org.inria.biomaj.ant.task.BmajExecute;
import org.inria.biomaj.session.bank.Bank;
import org.inria.biomaj.session.bank.FileDesc;
import org.inria.biomaj.singleton.BiomajSession;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * Manage decision about workflow post process.
 * This class verify and determines which postprocess launch 
 * @author ofilangi
 *
 */
public class DecisionWorkflowPostProcess {
	
	//Mode when there are no change in the remote server but verify current directory
	public static final int MODE_VERIF = 0;
	//Mode when a new bank ha been download
	public static final int MODE_NEW_RELEASE = 1;
	
	/**
	 * @uml.property  name="project"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	private Project project ;
	/**
	 * @uml.property  name="metaProcess"
	 * @uml.associationEnd  multiplicity="(0 -1)" ordering="true" elementType="org.inria.biomaj.session.process.BiomajProcess" qualifier="name:java.lang.String org.inria.biomaj.session.process.MetaProcess"
	 */
	private Hashtable<String,MetaProcess> metaProcess = new Hashtable<String,MetaProcess>();
	private Vector<String> listMetaProcess =  new Vector<String>();
	
	public DecisionWorkflowPostProcess(
								String block,
								String nameBank, 
								String propertyListMetaProcess, 
								Project project,
								int mode,boolean isPostProcess) throws BiomajException {
		this.project = project;
		
		//Si c'est une nouvelle release : 
		//- Recuperer les post process deja execute (avec contrainte postprosess = ko)
		//- on travaille sur future_release
		
		if (project.getProperty(propertyListMetaProcess)==null)
		{
			project.log("Can't interprete ["+project.getProperty(propertyListMetaProcess)+"]");
		}
		
		String[] lMeta = project.getProperty(propertyListMetaProcess).split(",");
		for (String n : lMeta)
			listMetaProcess.add(block+":"+n);

		Bank bank = BiomajSession.getInstance().getBank(nameBank);
		
		if ((mode != MODE_NEW_RELEASE)&&isPostProcess) {
			//try {
				for (int i=0;i<lMeta.length;i++) {
					//MetaProcess p = BiomajQueryXmlStateFile.getLastMetaProcess(block,lMeta[i],nameBank,isPostProcess);
					MetaProcess p = bank.getLastMetaProcessExecuted(block,lMeta[i]);
					if (p!=null) {
						metaProcess.put(block+":"+p.getName(), p);
					}
				}
	/*		} catch (FileNotFoundException fe) {
				project.log(fe.getMessage(),Project.MSG_ERR);
			} catch (BiomajException be) {
				project.log(be.getMessage(),Project.MSG_ERR);
			} */
		} else {
			//
		}
	}
	
	public MetaProcess getMetaProcess(String block,String name) {
		if (metaProcess.containsKey(block+":"+name))
			return metaProcess.get(block+":"+name);
		
		MetaProcess p = new MetaProcess();
		p.setName(name);
		p.setStart(new Date());
		return p;
		
	}
	

	public String getListSubProcess(String block,String name) throws BiomajException {
		
		if (!metaProcess.containsKey(block+":"+name))
			return project.getProperty(name);
		//Sinon il faut voir quels sont les subprocess qui ont besoin d etre relancee!
		
		if (project.getProperty(name)==null) 
			return null; 	
		
		String listProcess = project.getProperty(name);
		
		project.log("List process:"+listProcess,Project.MSG_INFO);
		MetaProcess mp = metaProcess.get(block+":"+name);
		boolean lookVolatile = (mp.getStatusStr().compareTo("ok")!=0);
		//${data.dir}/${dir.version}
		//String root = project.getProperty(CitrinaConst.dataDirProperty)+"/"+project.getProperty(CitrinaConst.versionDirProperty);
		
		Vector<BiomajProcess> lp = mp.getListProcess();
		
		//Il y a jamais eu de process lancee...
		if (lp.size()==0)
			return listProcess;
		
		//Si le metaProcess s est bien deroule la derniere fois, nous somme dans une phase de verif!
		if (!lookVolatile) {
			//Premiere verif, si le metaprocess est ok, on verifie que la liste de process dans le properties correspond a celle lance la derniere fois
			//si changement (ajout de process), on relance le tout!
			String[] l = listProcess.split(",");
			if (l.length!=lp.size()) {
				project.log("Number of processes defined inside metaprocess ["+mp.getName()+"] has changed. old["+
						Integer.toString(lp.size())+"] new [" +Integer.toString(l.length)+"] All process from ["+mp.getBlock()+":"+mp.getName()+"]"
							+" have to be launch!",Project.MSG_WARN);
				metaProcess.remove(block+":"+mp.getName());
				return listProcess;
			}
			
			for (int i=0;i<l.length;i++) {
				//OFI 29/01/2008  : ajout si le script a execute a changer on relance egalement
				project.log("li.exe:"+project.getProperty(l[i].trim()+".exe"));
				project.log("exe logged:"+lp.get(i).getExe());
				
				boolean exeChanged = project.getProperty(l[i].trim()+".exe").compareTo(lp.get(i).getExe())!=0;
				
				project.log("li.args:"+project.getProperty(l[i].trim()+".args"));
				project.log("args logged:"+lp.get(i).getArgs());

				boolean argsChanged = project.getProperty(l[i].trim()+".args").compareTo(lp.get(i).getArgs())!=0;
				
				if ((l[i].trim().compareTo(lp.get(i).getKeyName().trim())!=0)|| exeChanged || argsChanged)  {
					project.log("Processes defined inside metaprocess ["+l[i].trim()+"] has changed. ["+l[i].trim()+"] not match with [" +
							lp.get(i).getKeyName().trim()+"] All process from ["+mp.getBlock()+":"+mp.getName()+"]"
							+" have to be launch!",Project.MSG_WARN);
					metaProcess.remove(block+":"+mp.getName());
					return listProcess;
				}
				//ver 0.9.2 08/2007 ajout TEST, si date de l executable a changer alors on rexecute le metaprocess
				long timeLocal = BmajExecute.getTimeStampExe(BmajExecute.getPathExe(null,lp.get(i).getExe()));
				long timeSav   = lp.get(i).getTimeStampExe();
				if (timeLocal != timeSav) {
					project.log("Executable time [old:"+BiomajUtils.dateToString(new Date(timeSav), Locale.US)+"] " +
							"[new:"+BiomajUtils.dateToString(new Date(timeLocal), Locale.US)+"]",Project.MSG_WARN);
					project.log("exec process ["+BmajExecute.getPathExe(null,lp.get(i).getExe())+"] has been modified. All process from ["+mp.getBlock()+":"+mp.getName()+"]"
							+" have to be launch!",Project.MSG_WARN);
					metaProcess.remove(block+":"+mp.getName());
					return listProcess;
				}
				/*
				if (!dependenceIsOk(lp.get(i),lookVolatile)) {
					project.log("["+lp.get(i).getNameProcess()+"] is not ok...All Process from ["+mp.getBlock()+":"+mp.getName()+"]"
							+" have to be launch!",Project.MSG_WARN);
					return listProcess;
				}
				*/
			}
			
		}
		
		int i=0;
		for (i=0;i<lp.size();i++) {
			BiomajProcess p = lp.get(i);
			if ((p.getReturnValue()==0)&&(!p.isErrorDetected())) {
//				ver 0.9.2 08/2007 ajout TEST, si date de l executable a changer alors on rexecute le metaprocess
				long timeLocal = BmajExecute.getTimeStampExe(BmajExecute.getPathExe(null,p.getExe()));
				long timeSav   = p.getTimeStampExe();
				if (timeLocal != timeSav) {
					project.log("Executable time [old:"+BiomajUtils.dateToString(new Date(timeSav), Locale.US)+"] " +
							"[new:"+BiomajUtils.dateToString(new Date(timeLocal), Locale.US)+"]",Project.MSG_WARN);
					project.log("exec process ["+BmajExecute.getPathExe(null,lp.get(i).getExe())+"] has been modified.",Project.MSG_WARN);
					break;
				}
				//tout va bien pour l instant , il faut verifier l existence des fichiers generes!
				if (!dependenceIsOk(p,lookVolatile)) {
					project.log("["+p.getNameProcess()+"] is not ok...Process need to be launched.",Project.MSG_WARN);
					break;
				} else {
					project.log("["+p.getNameProcess()+"] is ok...",Project.MSG_INFO);
					
					listProcess = listProcess.replaceFirst(p.getKeyName(), "").trim();
					if (listProcess.startsWith(","))
						listProcess = listProcess.replaceFirst(",", "");
					else //C est donc une chaine vide, il y a rien a faire!
						return "";
					
					project.log("new list process:"+listProcess,Project.MSG_INFO);
				}
				
			} else {
				//Le process a retourne une erreur!
				break;
			}
		}
		//On efface tous les process a partir du process qui est considere comme ko (status ko ou fichier generer manquant)
		while (i!=lp.size()) {
			lp.remove(i);
		}
		return listProcess;
	}
	
	protected boolean dependenceIsOk(BiomajProcess p,boolean lookVolatile) {
		Vector<FileDesc> lFd = p.getDependancesOutput();
		
		for (FileDesc fd : lFd) {
			if (lookVolatile||(!fd.isVolatil())) {
				File f = new File(fd.getLocation());
				if (!f.exists()) {
					project.log("The dependence file ["+fd.getLocation()+"] is not find ! Process ["+p.getNameProcess()+"] need to be launched.",Project.MSG_WARN);
					return false;
				}
				if (f.lastModified()!=fd.getTime()) {
					project.log("Detect modification for the dependence file ["+fd.getLocation()+"] time stored:["+BiomajUtils.dateToString(new Date(fd.getTime()), Locale.US)+
							"], time on system ["+BiomajUtils.dateToString(new Date(f.lastModified()), Locale.US)+"]",Project.MSG_WARN);
					return false;
				}
				
				if (f.length()!=fd.getSize()) {
					project.log("Detect modification for the dependence file ["+fd.getLocation()+"] length stored:["+
							Long.toString(fd.getSize())+"], length on system ["+Long.toString(f.length())+"]",Project.MSG_WARN);
					return false;
				}
				project.log("** The dependence file ["+fd.getLocation()+"] [volatil:"+Boolean.toString(fd.isVolatil())+"] is ok!",Project.MSG_INFO);
			}
		}
		
		return true;
	}
	
}
