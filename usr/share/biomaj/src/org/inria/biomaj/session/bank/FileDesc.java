/**
 * Copyright Copr. INRIA/INRA
 * Contact :  biomaj_AT_genouest.org
 * 
 * BioMAJ is a workflow engine dedicated to biological bank management. 
 * The Software automates the update cycle and the supervision of the locally 
 * mirrored bank repository. The project is a collaborative effort between two 
 * French Research Institutes INRIA (Institut National de Recherche en
 * Informatique 
 * et en Automatique) & INRA (Institut National de la Recherche Agronomique).
 *
 * Inter Deposit Digital Number : IDDN ...................................
 *
 * This software is governed by the CeCILL-A license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-A license and that you accept its terms.
 */


package org.inria.biomaj.session.bank;

import java.io.File;
import java.io.IOException;
import org.inria.biomaj.ant.task.BmajExtract;
import org.inria.biomaj.singleton.BiomajLogger;
import org.inria.biomaj.utils.BiomajException;
import org.inria.biomaj.utils.BiomajUtils;

/**
 * This class is used for created a File Description (location and hash) ans insert into a xml file or to initialized a FileDesc from xml File. Behaviour of this classe change according the contexte (create to xml file or initialize from xml file)
 * @author  ofilangi
 */

public class FileDesc {
	/** Localtion of file 
	 * 
	 * local file  : location is the absolute
	 * */
	/**
	 * Path of file
	 * @uml.property  name="location"
	 */
	private String location;
	/**
	 * hash value
	 * @uml.property  name="hash"
	 */
	private String hash=null;
	/**
	 * Local size
	 * @uml.property  name="size"
	 */
	private long size=-1;
	/**
	 * Local tile
	 * @uml.property  name="time"
	 */
	private long time=-1;
	/**
	 * Reference to the file father
	 * @uml.property  name="refHash"
	 */
	private String refHash =null;
	/**
	 * Tag if file is an extract file (gz,tar,etc...)
	 * @uml.property  name="extract"
	 */
	private boolean extract = false;
	/**
	 * Tag if file is a link
	 * @uml.property  name="link"
	 */
	private boolean link = false;
	/**
	 * Tag if file is a volatile file
	 * @uml.property  name="volatil"
	 */
	private boolean volatil = false;
	
	/**
	 * File id in db
	 */
	private long fileId = -1;

//	---------------------------------------- Constructor for CREATE A FILEDESC DURING THE WORKFLOW -------------------------------------------
	/**
	 * Create a file desc with a local file
	 * Use when a FileDesc is created 
	 * @param f
	 * @param volatil
	 */
	public FileDesc(File f,boolean volatil) throws BiomajException {
		set(f,volatil);
		
	}
	
	public FileDesc(String location,boolean volatil) throws BiomajException {
		File f = new File(location);
		set(f,volatil);
	}

	public FileDesc(String location,boolean volatil, String refHash) throws BiomajException {
		File f = new File(location);
		set(f,volatil);
		this.refHash = refHash;
	}

	/*
	 * Initialized a Remote File Desc
	 */
	public FileDesc(String location,long size, long time, boolean link,boolean extract) {
		this.location = location;
		this.hash = BiomajUtils.getHash(location, time, size);
		this.size = size;
		this.time = time;
		this.extract = extract;
		this.link = link;
	}
	
	/*
	 * Initialized a Remote File Desc
	 */
	public FileDesc(String location,long size, long time, boolean link,boolean extract,boolean volatil,String refHash) {
		this.location = location;
		this.hash = BiomajUtils.getHash(location, time, size);
		this.size = size;
		this.time = time;
		this.extract = extract;
		this.link = link;
		this.volatil = volatil;
		this.refHash = refHash;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * True if hash is same that the calcul of hash in this instant!
	 * False otherwise
	 *
	 */
	public boolean isIntegre() {
		try {
			String h = BiomajUtils.getHash(location);
			if (h!=null) 
				return (h.compareTo(hash)==0);
		} catch (IOException ioe) {
			return false;
		}
		
		return false;
	}
	
	private void set(File f,boolean volatil) throws BiomajException {
		if (!f.exists()) {
			BiomajLogger.getInstance().log("FileDesc::set(File f,boolean volatil) fichier:"+f.getAbsolutePath()+" n'existe pas...");
			throw new BiomajException("filedesc.error","file:"+f.getAbsolutePath()+" does not exist !");
		}
		try {
			//location = f.getCanonicalPath();
			location = f.getAbsolutePath() ;
			size = f.length();
			time = f.lastModified();
			hash = BiomajUtils.getHash(f.getAbsolutePath());
			link = false; // forcement car on a pris la forme canonique du chemin
			String r = BmajExtract.removeExtension(location);
			this.extract = location.compareTo(r)!=0;
		} catch (IOException ioe) {
			BiomajLogger.getInstance().log(ioe);
			throw new BiomajException("filedesc.error",ioe.getMessage());
		}
		this.volatil = volatil;
	}
	
	/**
	 * @return  the extract
	 * @uml.property  name="extract"
	 */
	public boolean isExtract() {
		return extract;
	}

	/**
	 * @return  the hash
	 * @uml.property  name="hash"
	 */
	public String getHash() {
		return hash;
	}

	/**
	 * @return  the link
	 * @uml.property  name="link"
	 */
	public boolean isLink() {
		return link;
	}

	/**
	 * @return  the location
	 * @uml.property  name="location"
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return  the refHash
	 * @uml.property  name="refHash"
	 */
	public String getRefHash() {
		return refHash;
	}

	/**
	 * @return  the size
	 * @uml.property  name="size"
	 */
	public long getSize() {
		return size;
	}

	/**
	 * @return  the time
	 * @uml.property  name="time"
	 */
	public long getTime() {
		return time;
	}

	/**
	 * @return  the volatil
	 * @uml.property  name="volatil"
	 */
	public boolean isVolatil() {
		return volatil;
	}

	/**
	 * @param extract  the extract to set
	 * @uml.property  name="extract"
	 */
	public void setExtract(boolean extract) {
		this.extract = extract;
	}

	/**
	 * @param link  the link to set
	 * @uml.property  name="link"
	 */
	public void setLink(boolean link) {
		this.link = link;
	}
	
	public long getFileId() {
		return fileId;
	}
	
	public void setFileId(long id) {
		fileId = id;
	}

	/**
	 * @param volatil  the volatil to set
	 * @uml.property  name="volatil"
	 */
	public void setVolatil(boolean volatil) {
		this.volatil = volatil;
	}

	public String getName() {
		String[] t = location.split("/");
		if (t.length>0)
			return t[t.length-1];
		
		return "";
	}

	@Override
	public String toString() {
		return location;
	}

	

}
