#!/bin/sh

test_var_env()
{
    if  [ -z "$2" ]
    then
        echo "The environment variable $1 is not defined!"
        echo "You have to set correctly $1 in order to run biomaj."
        exit 1
    fi
}

test_var_env "BIOMAJ_ROOT" "${BIOMAJ_ROOT}"

java -cp $BIOMAJ_ROOT/lib/biomaj.jar -Dlog4j.configuration=file://$BIOMAJ_ROOT/src/log4j.properties org.inria.biomaj.exe.migration.ParserOld $@
