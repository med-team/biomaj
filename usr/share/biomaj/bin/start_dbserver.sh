#!/bin/bash

test_var_env()
{
    if  [ -z "$2" ]
    then
        echo "The environment variable $1 is not defined!"
        echo "You have to set correctly $1 in order to run biomaj."
        exit 1
    fi
}

SCRIPT_PATH="${BASH_SOURCE[0]}";
if([ -h "${SCRIPT_PATH}" ]) then
  while([ -h "${SCRIPT_PATH}" ]) do SCRIPT_PATH=`readlink "${SCRIPT_PATH}"`; done
fi
pushd . > /dev/null
cd `dirname ${SCRIPT_PATH}` > /dev/null
SCRIPT_PATH=`pwd`;
popd  > /dev/null
source $SCRIPT_PATH/env.sh

test_var_env "BIOMAJ_ROOT" "${BIOMAJ_ROOT}"

if [ ! -d $BIOMAJ_ROOT/tmp ]; then
	mkdir $BIOMAJ_ROOT/tmp
fi

if [ -f $BIOMAJ_ROOT/tmp/db.pid ]; then
	PID=`head -1 $BIOMAJ_ROOT/tmp/db.pid`
	if ! ps $PID | grep bmajdb > /dev/null
	then
		echo "Starting server..."
		java -Xmx1024m -cp $BIOMAJ_ROOT/lib/biomaj.jar org.hsqldb.server.Server -database.0 file:$BIOMAJ_ROOT/sql/biomaj_log -dbname.0 bmajdb > $BIOMAJ_ROOT/dbserver.log &
		
		while [ ! -f $BIOMAJ_ROOT/sql/biomaj_log.lck ]; do
			sleep 0.5
		done
		pgrep -f bmajdb > $BIOMAJ_ROOT/tmp/db.pid
	else
		echo "Server already running."
	fi
else
	echo "Starting server..."
	java -Xmx1024m -cp $BIOMAJ_ROOT/lib/biomaj.jar org.hsqldb.server.Server -database.0 file:$BIOMAJ_ROOT/sql/biomaj_log -dbname.0 bmajdb > $BIOMAJ_ROOT/dbserver.log &
	
	while [ ! -f $BIOMAJ_ROOT/sql/biomaj_log.lck ]; do
		sleep 0.5
	done
	pgrep -f bmajdb > $BIOMAJ_ROOT/tmp/db.pid
fi
