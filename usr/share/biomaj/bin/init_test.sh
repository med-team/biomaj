#!/bin/sh

# Get BIOMAJ_ROOT directory
cd `dirname $0`
cd ..
ROOT=`pwd`

BANK_NAME="testbank.properties"
DATA_DIR=$ROOT"/testdatadir"

if [ ! -e $DATA_DIR ]; then
	mkdir $DATA_DIR
fi

# env.sh
echo "#!/bin/sh" > $ROOT/bin/env.sh
echo "export ANT_HOME=/local/ant/apache-ant-1.7.0" >> $ROOT/bin/env.sh
echo "export JAVA_HOME=/local/java/jdk1.6.0_05" >> $ROOT/bin/env.sh
echo "export BIOMAJ_ROOT=$ROOT" >> $ROOT/bin/env.sh

# create admin dir
if [ ! -e $ROOT/conf/db_properties/admin ]; then
	mkdir $ROOT/conf/db_properties/admin
fi

# Create testbank with home directory as remote dir
echo "db.fullname=testbank" > $ROOT/conf/db_properties/admin/$BANK_NAME
echo "remote.dir=$ROOT/testbanks" >> $ROOT/conf/db_properties/admin/$BANK_NAME
echo "server=localhost" >> $ROOT/conf/db_properties/admin/$BANK_NAME
echo "frequency.update=0" >> $ROOT/conf/db_properties/admin/$BANK_NAME
echo "db.name=testbank" >> $ROOT/conf/db_properties/admin/$BANK_NAME
echo "remote.files=.*" >> $ROOT/conf/db_properties/admin/$BANK_NAME
echo "local.files=.*" >> $ROOT/conf/db_properties/admin/$BANK_NAME
echo "dir.version=testbank" >> $ROOT/conf/db_properties/admin/$BANK_NAME
echo "offline.dir.name=testbank_tmp" >> $ROOT/conf/db_properties/admin/$BANK_NAME
echo "protocol=local" >> $ROOT/conf/db_properties/admin/$BANK_NAME


# Create global.properties with data.dir
echo "data.dir=$DATA_DIR" > $ROOT/conf/db_properties/global.properties
echo "port=21" >> $ROOT/conf/db_properties/global.properties
echo "username=anonymous" >> $ROOT/conf/db_properties/global.properties
echo "password=anonymous@nowhere.com" >> $ROOT/conf/db_properties/global.properties
echo "production.directory.chmod=775" >> $ROOT/conf/db_properties/global.properties
echo "bank.num.threads=4" >> $ROOT/conf/db_properties/global.properties
echo "files.num.threads=4" >> $ROOT/conf/db_properties/global.properties
echo "keep.old.version=0" >> $ROOT/conf/db_properties/global.properties
echo "do.link.copy=true" >> $ROOT/conf/db_properties/global.properties
echo "release.dateformat=yyyy-MM-dd" >> $ROOT/conf/db_properties/global.properties
echo "historic.logfile.level=VERBOSE" >> $ROOT/conf/db_properties/global.properties
echo "historic.logfile.properties=false" >> $ROOT/conf/db_properties/global.properties
echo "historic.logfile.task=true" >> $ROOT/conf/db_properties/global.properties
echo "historic.logfile.target=true" >> $ROOT/conf/db_properties/global.properties
echo "http.parse.dir.line=<a[\\s]+href=\"([\\S]+)/\".*alt=\"\\[DIR\\]\">.*([\\d]{2}-[\\w\\d]{2,5}-[\\d]{4}\\s[\\d]{2}:[\\d]{2})" >> $ROOT/conf/db_properties/global.properties
echo "http.parse.file.line=<a[\\s]+href=\"([\\S]+)\".*([\\d]{2}-[\\w\\d]{2,5}-[\\d]{4}\\s[\\d]{2}:[\\d]{2})[\\s]+([\\d\\.]+[MKG]{0,1})" >> $ROOT/conf/db_properties/global.properties
echo "http.group.dir.name=1" >> $ROOT/conf/db_properties/global.properties
echo "http.group.dir.date=2" >> $ROOT/conf/db_properties/global.properties
echo "http.group.file.name=1" >> $ROOT/conf/db_properties/global.properties
echo "http.group.file.date=2" >> $ROOT/conf/db_properties/global.properties
echo "http.group.file.size=3" >> $ROOT/conf/db_properties/global.properties
echo "log.files=false" >> $ROOT/conf/db_properties/global.properties
echo "local.files.excluded=\\.panfs.*" >> $ROOT/conf/db_properties/global.properties
echo "ftp.timeout=2000000" >> $ROOT/conf/db_properties/global.properties
echo "ftp.automatic.reconnect=5" >> $ROOT/conf/db_properties/global.properties
echo "ftp.active.mode=false" >> $ROOT/conf/db_properties/global.properties


