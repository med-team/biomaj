#!/bin/bash

echo "Migration script from Biomaj 1.1 to 1.2"

SCRIPT_PATH="${BASH_SOURCE[0]}";
if([ -h "${SCRIPT_PATH}" ]) then
  while([ -h "${SCRIPT_PATH}" ]) do SCRIPT_PATH=`readlink "${SCRIPT_PATH}"`; done
fi
pushd . > /dev/null
cd `dirname ${SCRIPT_PATH}` > /dev/null
SCRIPT_PATH=`pwd`;
popd  > /dev/null

cd $SCRIPT_PATH/..

echo "Enter currently installed BioMAJ root directory (default value is /usr/share/biomaj) :"
read ROOT

if [ -z "$ROOT" ]; then
	ROOT="/usr/share/biomaj"
fi

#echo "Are you using the provided Tomcat server ? (y/n) "
#read -n 1 ANS

ANS="n"

echo ""

USE_TOMCAT="false"

if [[ "$ANS" == "y" || "$ANS" == "Y" ]]; then
	USE_TOMCAT="true"
else
	echo ""
	echo "Read the instructions in README => MIGRATION => Biomaj watcher to update Biomaj watcher."
	echo ""
fi

source $ROOT/bin/env.sh

TOMCAT=$BIOMAJ_ROOT/apache-tomcat-6.0.24


echo "Updating source code..."

# Source
cp -R src $BIOMAJ_ROOT

# build.xml
cp -f build.xml $BIOMAJ_ROOT

cd $SCRIPT_PATH/..

rm -f $BIOMAJ_ROOT/lib/*

# Copying new libraries
cp -f lib/* $BIOMAJ_ROOT/lib

echo "Compiling source code..."
ant -f $BIOMAJ_ROOT/build.xml

cp sql/hsql*.sql $BIOMAJ_ROOT/sql
cp sql/mysql*.sql $BIOMAJ_ROOT/sql

cp -f workflows/* $BIOMAJ_ROOT/workflows

cp -f RELEASE $BIOMAJ_ROOT
cp -f INSTALL $BIOMAJ_ROOT
cp -f README $BIOMAJ_ROOT
cp -f console_help.txt $BIOMAJ_ROOT


if [ "$USE_TOMCAT" == "true" ]; then
	echo "Stopping apache..."

	sh $BIOMAJ_ROOT/apache-tomcat-6.0.24/bin/shutdown.sh 2>/dev/null

	# Copying jobs.xml
	cp $TOMCAT/webapps/BmajWatcher/jobs.xml $BIOMAJ_ROOT
	cp $TOMCAT/webapps/BmajWatcher/jobs.xsd $BIOMAJ_ROOT
																						
	# Cleaning webapps directory
	rm -rf $TOMCAT/webapps/BmajWatcher/
	rm -f $TOMCAT/webapps/BmajWatcher.war

	# Removing old libraries
	rm -rf $TOMCAT/lib/gwt-2.0.0

	# Copying new BmajWatcher
	cp apache-tomcat-6.0.24/webapps/BmajWatcher.war $TOMCAT/webapps

	# Link to new biomaj lib
	cd $TOMCAT/lib
	ln -s ../../lib/biomaj.jar biomaj.jar
	cd $SCRIPT_PATH/..

	echo ""
	echo "*** Update Note ***"
	echo ""
	echo ""
	echo "Add to $TOMCAT/conf/Catalina/localhost/BmajWatcher.xml the following parameters :"
	echo ""
	echo "<Parameter name=\"ADMIN_LOGIN\" value=\"admin\" override=\"false\"/>"
	echo "<Parameter name=\"JOBS_LOCATION\" value=\"$BIOMAJ_ROOT\" override=\"false\"/>"
	echo ""
	echo "Then start the server"
	

fi

# Run java
java -cp $BIOMAJ_ROOT/lib/biomaj.jar -Xmx512m org.inria.biomaj.exe.migration.CoreMigrationFrom1_1To1_2 admin admin mail@admin

echo ""
echo "Admin login/password are admin/admin. Log in to BmajWatcher interface to change the password."

