#!/bin/bash

# Copyright Copr. INRIA/INRA
# Contact :  biomaj_AT_genouest.org
# 
# BioMAJ is a workflow engine dedicated to biological bank management. 
# The Software automates the update cycle and the supervision of the locally 
# mirrored bank repository. The project is a collaborative effort between two 
# French Research Institutes INRIA (Institut National de Recherche en
# Informatique 
# et en Automatique) & INRA (Institut National de la Recherche Agronomique).
#
# This software is governed by the CeCILL-A license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-A license and that you accept its terms.

test_var_env()
{
    if  [ -z "$2" ]
    then
        echo "The environment variable $1 is not defined!"
        echo "You have to set correctly $1 in order to run biomaj."
        exit 1
    fi
}

test_ant_version()
{
ANTVER=`ant -version | awk '{print $4}' | cut -d "." -f1-3 | sed -e 's/\./ /g' -e 's/[a-z]/ /g' -e 's/_/ /g' `
#The ANT minimum Requirement is 1.6.5
PreRequis=(1 6 5)

count=-1
    for i in $ANTVER; do
        count=`expr $count + 1`
        if [ $i -eq ${PreRequis[$count]} ]; then
        continue
        elif [ $i -gt ${PreRequis[$count]} ]; then
        break
        else
        Prerequis=`echo ${PreRequis[*]} | sed -e 's/ /\./g'`
        echo "WARNING: You have a Version of Ant(`ant -version | awk '{print $4}' | cut -d "." -f1-3`) older then $Prerequis "
        echo "Biomaj has been tested with Ant version $Prerequis"
        echo "Please setup the prerequired Ant Environment (or above), and re-run Biomaj"
        exit 1
        fi
    done
}

test_java_version()
{
JAVAVER=`java -version 2>&1 | grep version | awk '{ print $3}' | sed -e 's/"//g' | cut -d "." -f1-3 | cut -d "_" -f1 | sed -e 's/\./ /g' -e 's/[a-z]/ /g' -e 's/_/ /g' `
#The JAVA minimum Requirement is 1.6.0
PreRequis=(1 6 0)

count=-1
       for i in $JAVAVER; do
               count=`expr $count + 1`
               if [ $i -eq ${PreRequis[$count]} ]; then
               continue
               elif [ $i -gt ${PreRequis[$count]} ]; then
               break
               else
               Prerequis=`echo ${PreRequis[*]} | sed -e 's/ /\./g'`
               echo "WARNING: You have a Version of JAVA(`java -version 2>&1 | grep version | awk '{ print $3}'`) older then $Prerequis "
               echo "Biomaj has been tested with Java version $Prerequis"
               echo "Please setup the prerequired Java Environment (or above), and re-run Biomaj"
               exit 1
               fi
       done
}

#echo "Loading variables environment"
SCRIPT_PATH="${BASH_SOURCE[0]}";
if([ -h "${SCRIPT_PATH}" ]) then
  while([ -h "${SCRIPT_PATH}" ]) do SCRIPT_PATH=`readlink "${SCRIPT_PATH}"`; done
fi
pushd . > /dev/null
cd `dirname ${SCRIPT_PATH}` > /dev/null
SCRIPT_PATH=`pwd`;
popd  > /dev/null
source $SCRIPT_PATH/env.sh

test_var_env "ANT_HOME"  "${ANT_HOME}"
test_var_env "JAVA_HOME" "${JAVA_HOME}"
test_var_env "BIOMAJ_ROOT" "${BIOMAJ_ROOT}"

ANT=${ANT_HOME}/bin/ant

#echo "Creating database..."
#source $BIOMAJ_ROOT/bin/create_database.sh
#echo "Verifying Pre-requisists"
test_ant_version 
test_java_version


#echo "Executing biomaj ..."
java -Xmx1024m -jar $BIOMAJ_ROOT/lib/biomaj.jar $@
