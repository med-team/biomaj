#!/bin/bash


test_var_env()
{
    if  [ -z "$2" ]
    then
        echo "The environment variable $1 is not defined!"
        echo "You have to set correctly $1 in order to run biomaj."
        exit 1
    fi
}

SCRIPT_PATH="${BASH_SOURCE[0]}";
if([ -h "${SCRIPT_PATH}" ]) then
  while([ -h "${SCRIPT_PATH}" ]) do SCRIPT_PATH=`readlink "${SCRIPT_PATH}"`; done
fi
pushd . > /dev/null
cd `dirname ${SCRIPT_PATH}` > /dev/null
SCRIPT_PATH=`pwd`;
popd  > /dev/null
source $SCRIPT_PATH/env.sh


read -p "Enter admin login: " LOGIN
read -s -p "Enter admin password: " PASSWORD
echo "" # new line
read -p "Enter admin mail: " MAIL

test_var_env "BIOMAJ_ROOT" "${BIOMAJ_ROOT}"


#if  [ -f $BIOMAJ_ROOT/sql/biomaj_log.lck ]; then
	#echo 'Database already exists.'
#	true
#else
	echo 'Creating database...'
	java -cp $BIOMAJ_ROOT/lib/biomaj.jar org.inria.biomaj.sql.CreateDB $LOGIN $PASSWORD $MAIL
	echo 'Done.'
#fi
