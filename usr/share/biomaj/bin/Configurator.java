import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.regex.Matcher;


/**
 * Initializes database parameters in global.properties
 * and env.sh
 * 
 * @author rsabas
 *
 */
public class Configurator {
	
	private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	
	private static final String HSQLDB_DRIVER = "org.hsqldb.jdbcDriver";
	private static final String HSQLDB_LOGIN = "sa";
	private static final String HSQLDB_PASSWD = "";
	private static final String HSQLDB_URL = "jdbc:hsqldb:hsql://localhost/bmajdb";
	
	private static final String TOMCAT_DIR = "apache-tomcat-6.0.24";
	private static final int MESSAGE_MAX = 30;
	
	private static String biomajRoot = "";
	
	private static PrintWriter logger = null;
	
	public static void main(String[] args) {

		Map<String, String> params = new HashMap<String, String>();
		
		for (String arg : args) {
			String[] pair = arg.split(":=");
			if (pair.length > 1)
				params.put(pair[0], pair[1]);
			else
				params.put(pair[0], "");
		}

		biomajRoot = params.get("biomaj_root");
		
		try {
			logger = new PrintWriter(biomajRoot + "/install_log.log");
		} catch (FileNotFoundException e1) {
			log(e1.toString());
		}
		
		if (params.get("java_home").trim().isEmpty())
			params.put("java_home","/usr/lib/jvm/java-6-openjdk");

		/*
		 * Global.properties
		 */
		log("Updating global.properties...");
		String globalLocation = "/etc/biomaj/db_properties/global.properties";
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader br = new BufferedReader(new FileReader(globalLocation));
			String line = "";
			while ((line = br.readLine()) != null) {
				if (line.startsWith("database."))
					continue;
				sb.append(line + "\n");
			}
		} catch (FileNotFoundException e) {
			log(e.toString());
		} catch (IOException e) {
			log(e.toString());
		}
		
		if (params.get("db_type").equals("MySQL")) {
			log("Using MySQL");
			sb.append("database.type=mysql\n");
			sb.append("database.driver=" + MYSQL_DRIVER + "\n");
			sb.append("database.url=" + params.get("db_url") + "\n");
			sb.append("database.login=" + params.get("db_login") + "\n");
			sb.append("database.password=" + params.get("db_passwd") + "\n");
		} else {
			log("Using HSQLDB");
			sb.append("database.type=hsqldb\n");
			sb.append("database.driver=" + HSQLDB_DRIVER + "\n");
			sb.append("database.url=" + HSQLDB_URL + "\n");
			sb.append("database.login=" + HSQLDB_LOGIN + "\n");
			sb.append("database.password=" + HSQLDB_PASSWD + "\n");
		}
		
		try {
			PrintWriter pw = new PrintWriter(globalLocation);
			pw.println(sb.toString());
			pw.close();
		} catch (FileNotFoundException ex) {
			log(ex.toString());
		}
		
		/*
		 * Creer env.sh
		 */
		createEnv(params.get("ant_home"), params.get("java_home"));
		
				
		log("Configuration complete");
		logger.close();
	}
	
	private static void createEnv(String antHome, String javaHome) {

		try {
			PrintWriter pw = new PrintWriter(biomajRoot + "/bin/env.sh");
			pw.println("#!/bin/sh");
			pw.println("export JAVA_HOME=" + javaHome);
			pw.println("export ANT_HOME=" + antHome);
			pw.println("export BIOMAJ_ROOT=" + biomajRoot);
			pw.close();
		} catch (FileNotFoundException ex) {
			log(ex.toString());
		}
		
	}
	
	
	private static void log(String message) {
		SimpleDateFormat sdf = new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss]");
		logger.println(sdf.format(new Date()) + ": " + message);
		logger.flush();
	}
}

